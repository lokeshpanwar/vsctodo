﻿using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
//using Doconut.Config;
//using Doconut.Viewer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data;
using static CoreStructure_APP.Utility.Standard;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using System.Text;
using System.Net.NetworkInformation;
using System.Management;
using CoreStructure_API.Utility;

namespace CoreStructure_APP.Controllers
{


    public class AuthController : Controller
    {

        private IHttpContextAccessor _accessor;


        Auth_Repository objAuthRepo = new Auth_Repository();
        Employee_Repository objEmpRepo = new Employee_Repository();
        DropDown_Repository objDropRepo = new DropDown_Repository();
        SubCompany_Repository objSCRepo = new SubCompany_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        string baseUrl = APIHelper.BaseURL();

        public AuthController(IHttpContextAccessor accessor, IHostingEnvironment hostingEnvironment)
        {
            _accessor = accessor;
            _hostingEnvironment = hostingEnvironment;
        }

        public string GetIP()
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            return ip;
        }


        private string GetMacAddress()
        {
            //string macAddresses = string.Empty;
            //foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            //{
            //    if (nic.OperationalStatus == OperationalStatus.Up)
            //    {
            //        macAddresses += nic.GetPhysicalAddress().ToString();
            //        break;
            //    }
            //}
            //return macAddresses;

            var mbs = new ManagementObjectSearcher("Select ProcessorId From Win32_processor");
            ManagementObjectCollection mbsList = mbs.Get();
            string id = "";
            foreach (ManagementObject mo in mbsList)
            {
                id = mo["ProcessorId"].ToString();
                break;
            }
            return id;
        }

        #region Login (GET)

        [HttpGet]
        public IActionResult Login()
        {
            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string Returnurl)
        {
            Login_Model pLoginModel = new Login_Model();
            pLoginModel.HDReturnurl = Returnurl;
            return View(pLoginModel);
        }


        [AllowAnonymous]
        public ActionResult TodoLogin(string loginkey)
        {
            string tokenkey = CryptorEngine.Decrypt(loginkey, true);
            string[] cipherString = tokenkey.Split(',');
            
            
            Login_Model objModel = new Login_Model();
            objModel.UserId = Convert.ToInt32(cipherString[0].ToString());
            objModel.Vertical = Convert.ToInt32(cipherString[1].ToString());
            objModel.UserName = "username";
            objModel.Role = 1;
            objModel.Name = "nameofuser";
            //objModel.UserName = cipherString[2].ToString();
            //objModel.Vertical = Convert.ToInt32(cipherString[3].ToString());
            //objModel.Role = Convert.ToInt32(cipherString[4].ToString());
            
            var data = objAuthRepo.CheckLogin(objModel);
            if (data.results.LoginStatus == "Success")
            {
                string logintoken = data.results.UserId.ToString() + "," + data.results.Name.ToString() + "," + data.results.UserName.ToString() + "," + data.results.Vertical.ToString() + "," + data.results.Role.ToString() + "," + "";
                string strCookieValue = CryptorEngine.Encrypt(logintoken, true);
                ViewBag.Tokenkey = strCookieValue;

                CookieOptions option = new CookieOptions();
                option.Expires = DateTime.Now.AddMinutes(60);
                option.IsEssential = true;
                Response.Cookies.Append("Token", strCookieValue, option);
                Response.Cookies.Append("UserId", data.results.UserId.ToString(), option);
                Response.Cookies.Append("UserName", data.results.UserName.ToString(), option);
                Response.Cookies.Append("Role", data.results.Role.ToString(), option);
                Response.Cookies.Append("OfficeId", data.results.OfficeId.ToString(), option);
                Response.Cookies.Append("EmpId", data.results.EmpId.ToString(), option);
                Response.Cookies.Append("Name", data.results.Name.ToString(), option);
                Response.Cookies.Append("Vertical", data.results.Vertical.ToString(), option);
                Response.Cookies.Append("UserMobile", data.results.UserMobile.ToString(), option);
                Response.Cookies.Append("UserEmail", data.results.UserEmail.ToString(), option);
              return  RedirectToAction("Index", "ToDo");

            }
            else
            {

            }

            return View("Login");
        }

        [AllowAnonymous]
        public ActionResult createkey(string Userid, string Name, string Username, string Vertical, string Role)
        {
        
            string token = Userid + "," + Name + "," + Username + "," + Vertical + "," + Role + "," + StandardDateTime.GetDateTime();
            string strCookieValue = CryptorEngine.Encrypt(token, true);
            ViewBag.Tokenkey = strCookieValue;
            return View();
        
        }


        [AllowAnonymous]
        public ActionResult logout()
        {
            try { 

            int vertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());


            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
            }

            if (vertical == 1)
            {
                
                return Redirect("http://erp.vsc.co.in/");
            }
            else if (vertical == 2)
            {

                return Redirect("http://erp.vscconsulting.in/");

            }
            else if (vertical == 3)
            {
                return Redirect("http://erp.pearlinsurancebroker.com/");
            }
            else if (vertical == 4)
            {
                return Redirect("http://erp.pearltechnosolutions.com/");
            }
            else if (vertical == 5)
            {
                return Redirect("http://test.pearlattorneys.in/");
            }
            else
            {
                return Redirect("https://www.vinodsinghal.com/");
            }
            }
            catch(Exception ex)
            {
                

            }

            return View();
        }




        #endregion

        #region Login (POST)

        [HttpPost]
        public JsonResult GetOTPForLogIn(string uName, string pWord)
        {
            string macAdd = GetMacAddress();
            Login_Model objModel = new Login_Model();
            objModel.UserName = uName;
            objModel.Password = pWord;
            objModel.IPAddress = GetIP();
            objModel.MacAddress = GetMacAddress();
            OTP_Return_Value_API modal = new OTP_Return_Value_API();
            modal = objAuthRepo.GetOTPForLogIn(objModel);
            if (modal.results.LoginStatus == "Success")
            {
                if (modal.results.IsSameDevice == 0)
                {
                    if (!String.IsNullOrEmpty(modal.results.MobileNo))
                    {
                        HttpWebResponse response = null;
                        WebRequest request = null;
                        string MsgText = modal.results.return_OTP + " is the OTP for your Pearl Insurance Broker verification. Please DO NOT share your OTP with anyone";

                        string url = "http://msg.msgclub.net/rest/services/sendSMS/sendGroupSms?AUTH_KEY=a07df1e68b3dd146667cfb6a859c213&message=" + Convert.ToString(MsgText) + "&senderId=PRLBKR&routeId=1&mobileNos=" + Convert.ToString(modal.results.MobileNo) + "&smsContentType=english";
                        request = WebRequest.Create(url);
                        response = (HttpWebResponse)request.GetResponse();
                        Stream stream = response.GetResponseStream();
                        Encoding ec = System.Text.Encoding.GetEncoding("utf-8");
                        StreamReader reader = new
                        System.IO.StreamReader(stream, ec);
                        reader.Close();
                        stream.Close();
                    }
                }
            }
            return Json(modal);
        }

        [HttpPost]
        public IActionResult Login(Login_Model objModel)
        {
            string error = "";
            try
            {
                if (objModel.IsSameMac == 1)
                {
                    ModelState.Remove("OTP");
                }
                if (ModelState.IsValid)
                {
                    objModel.IPAddress = GetIP();
                    objModel.MacAddress = GetMacAddress();

                    var data = objAuthRepo.CheckLogin(objModel);
                    error = data.Error;

                    if (data.results.LoginStatus == "Success")
                    {
                        CookieOptions option = new CookieOptions();
                        //HttpContext.Session.SetString("Token", data.results.token);
                        //HttpContext.Session.SetString("ActiveTheme", data.results.theme);

                        option.Expires = DateTime.Now.AddMinutes(30);
                        option.IsEssential = true;
                       // option.SameSite = SameSiteMode.None;

                        Response.Cookies.Append("Token", data.results.Token, option);
                        Response.Cookies.Append("ActiveTheme", data.results.Theme, option);
                        Response.Cookies.Append("UserId", data.results.UserId.ToString(), option);
                        Response.Cookies.Append("USCId", data.results.UserSubCompId.ToString(), option);
                        Response.Cookies.Append("UDId", data.results.UserDomainId.ToString(), option);
                        Response.Cookies.Append("UPCId", data.results.UserParentCompId.ToString(), option);
                        Response.Cookies.Append("UType", data.results.UserType.ToString(), option);
                        Response.Cookies.Append("UserDivision", data.results.UserDivision.ToString(), option);
                        Response.Cookies.Append("EmpName", data.results.EmployeeName.ToString(), option);

                        Response.Cookies.Append("UEmail", data.results.UserEmail.ToString(), option);
                        Response.Cookies.Append("UName", data.results.UserName.ToString(), option);
                        Response.Cookies.Append("URole", data.results.RoleId.ToString(), option);
                        Response.Cookies.Append("URoleName", data.results.UserRole.ToString(), option);
                        Response.Cookies.Append("OfficeId", data.results.OfficeId.ToString(), option);
                        Response.Cookies.Append("UEmpId", data.results.EmpId == null ? "" : data.results.EmpId.ToString(), option);
                        Response.Cookies.Append("UImage", data.results.fileName.ToString(), option);

                        if (Convert.ToInt64(data.results.UserId) > 0)
                        {
                            if (objModel.HDReturnurl == null)
                            {
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                Response.Redirect(objModel.HDReturnurl);
                            }
                        }

                    }
                    else
                    {
                        @ViewBag.Message = data.results.LoginStatus;  //"Enter Valid Credentials!!!";
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Enter Mandatory Field!!!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, "", "WebAPP", "AuthController/Login");
                @ViewBag.Message = error; //ex.Message;
                @ViewBag.HideClass = "alert alert-danger";
            }
            return View();
        }

        #endregion

        public IActionResult InValidUserRole()
        {
            return View();
        }

        #region AdminProfile (GET)

        [HttpGet]
        [SessionAuthorizeFilter]
        public IActionResult AdminProfile()
        {
            Users modal = new Users();
            modal = objAuthRepo.GetAdminProfileForView(HttpContext.Request.Cookies["Token"].ToString());
            modal.Address = modal.Address + ", " + modal.Town + ", " + modal.CityName + ", " + modal.StateName;
            return View(modal);
        }

        #endregion

        #region UserProfile (GET)

        [HttpGet]
        [SessionAuthorizeFilter]
        public IActionResult UserProfile()
        {
            Users modal = new Users();
            modal = objAuthRepo.GetAdminProfileForView(HttpContext.Request.Cookies["Token"].ToString());
            return View(modal);
        }

        #endregion

        #region UserProfile (POST)

        [HttpPost]
        [SessionAuthorizeFilter]
        public IActionResult UserProfile(UserProfile_Model objModel, List<IFormFile> filename1, string profileupdate, string delegateupdate, string EnableBtn, string DisableBtn)
        {
            try
            {
                if (!string.IsNullOrEmpty(profileupdate))
                {
                    long size = filename1.Sum(f => f.Length);

                    // full path to file in temp location
                    var filePath = Path.GetTempFileName();

                    foreach (var formFile in filename1)
                    {
                        long iFileSize = formFile.Length;

                        if (iFileSize > 200000)  // 200KB
                        {
                            // File exceeds the file maximum size
                            @ViewBag.Message = "Exceeded maximum size of the image!";
                            @ViewBag.HideClass = "alert alert-danger";
                            return View(objModel);
                        }

                        using (var memoryStream = new MemoryStream())
                        {
                            formFile.CopyToAsync(memoryStream);
                            var stream = memoryStream.ToArray();
                            string thePictureDataAsString = Convert.ToBase64String(stream);
                            objModel.EmpPhoto = thePictureDataAsString;
                        }
                    }


                    var result = objEmpRepo.UpdateUserProfile(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result.ReturnStatus == "SUCCESS")
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else if (!string.IsNullOrEmpty(delegateupdate))
                {

                    var result = objEmpRepo.UpdateUserDelegate(objModel.User_Delegation_Table_Model, HttpContext.Request.Cookies["Token"].ToString());
                    if (result.ReturnStatus == "SUCCESS")
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else if (!string.IsNullOrEmpty(EnableBtn))
                {
                    objModel.User_Delegation_Table_Model.IsEnabled = true;
                    var result = objEmpRepo.EnableDisableUserDelegate(objModel.User_Delegation_Table_Model, HttpContext.Request.Cookies["Token"].ToString());
                    if (result.ReturnStatus == "SUCCESS")
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else if (!string.IsNullOrEmpty(DisableBtn))
                {
                    objModel.User_Delegation_Table_Model.IsEnabled = false;
                    var result = objEmpRepo.EnableDisableUserDelegate(objModel.User_Delegation_Table_Model, HttpContext.Request.Cookies["Token"].ToString());
                    if (result.ReturnStatus == "SUCCESS")
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result.ReturnMessage;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "AuthController/UserProfile");
            }

            UserProfile_Model objModelN = new UserProfile_Model();
            try
            {
                long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
                var EmpModel = objEmpRepo.GetEmployeeById(SLNO, HttpContext.Request.Cookies["Token"].ToString()).results;
                var delegatedusermodel = objEmpRepo.GetUserDelegationById(SLNO, HttpContext.Request.Cookies["Token"].ToString());
                objModelN.Designation = EmpModel.Designation;
                objModelN.Dob = EmpModel.Dob;
                objModelN.Doj = EmpModel.Doj;
                objModelN.DomainName = EmpModel.DomainName;
                objModelN.Domain_ID = EmpModel.Domain_ID;
                objModelN.EmployeeName = EmpModel.EmployeeName;
                objModelN.Email = EmpModel.Email;
                objModelN.EmpId = EmpModel.EmpId;
                objModelN.EmpPhoto = EmpModel.EmpPhoto;
                objModelN.Location = EmpModel.Location;
                objModelN.SLNO = EmpModel.SLNO;
                objModelN.SubCompanyName = EmpModel.SubCompanyName;
                objModelN.Sub_Company = EmpModel.Sub_Company;
                objModelN.UpdatedBy = EmpModel.UpdatedBy;
                objModelN.UpdatedOn = EmpModel.UpdatedOn;
                objModelN.User_type = EmpModel.User_type;

                objModelN.User_Delegation_Table_Model = delegatedusermodel;
                if (delegatedusermodel.parentCoy != 0)
                {
                }
                else
                {
                    objModelN.User_Delegation_Table_Model.parentCoy = Convert.ToInt64(HttpContext.Request.Cookies["UPCId"].ToString());
                }
                if (delegatedusermodel.subCoy != 0)
                {
                }
                else
                {
                    objModelN.User_Delegation_Table_Model.subCoy = Convert.ToInt64(HttpContext.Request.Cookies["USCId"].ToString());
                }
                if (delegatedusermodel.FD != 0)
                {
                }
                else
                {
                    objModelN.User_Delegation_Table_Model.FD = Convert.ToInt64(HttpContext.Request.Cookies["UDId"].ToString());
                }
                if (delegatedusermodel.delegatedBy != 0)
                {
                }
                else
                {
                    objModelN.User_Delegation_Table_Model.delegatedBy = SLNO;
                }
                //var userdelegate = objDropRepo.GetInternalUserListFromSubCompAndDomain(HttpContext.Request.Cookies["Token"].ToString(), objModel.User_Delegation_Table_Model.subCoy, objModel.User_Delegation_Table_Model.FD).results;
                //List<SelectListItem> objDelList = new List<SelectListItem>();
                //foreach (var data in userdelegate)
                //{
                //    if (data.Value != SLNO.ToString())
                //    {
                //        objDelList.Add(new SelectListItem
                //        {
                //            Text = data.Text,
                //            Value = data.Value
                //        });
                //    }
                //}
                //ViewBag.DelegateToList = objDelList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "Web APP", "AuthController/UserProfile");
            }
            return View(objModelN);
        }

        #endregion UserProfile

        #region UserChangePassword (GET)

        [HttpGet]
        [SessionAuthorizeFilter]
        public IActionResult UserChangePassword()
        {
            long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
            ResetPassword_Model objModel = new ResetPassword_Model();
            objModel.SLNO = SLNO;
            return View(objModel);
        }

        #endregion

        #region UserChangePassword (POST)

        [HttpPost]
        [SessionAuthorizeFilter]
        public IActionResult UserChangePassword(ResetPassword_Model objModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (objModel.New_Password == objModel.Confirm_Password)
                    {
                        var result = objEmpRepo.ChangeUserPassword(objModel, HttpContext.Request.Cookies["Token"].ToString());
                        if (result == "SUCCESS")
                        {
                            @ViewBag.Message = "Password Changed Successfully...!";
                            @ViewBag.HideClass = "alert alert-success";
                            ModelState.Clear();


                            long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
                            ResetPassword_Model objModelN = new ResetPassword_Model();
                            objModelN.SLNO = SLNO;
                            return View(objModelN);
                        }
                        else
                        {
                            @ViewBag.Message = result;
                            @ViewBag.HideClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        @ViewBag.Message = "New Password and Confirm Password should be same!";
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Enter Mandatory Fields!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "AuthController/UserChangePassword");
            }
            return View(objModel);
        }

        #endregion

        #region AdminChangePassword (GET)

        [HttpGet]
        [SessionAuthorizeFilter]
        public IActionResult AdminChangePassword()
        {
            long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
            ResetPassword_Model objModel = new ResetPassword_Model();
            objModel.SLNO = SLNO;
            return View(objModel);
        }


        //[HttpPost]
        //[SessionAuthorizeFilter("SaveChangePassword")]
        //public ActionResult SaveChangePassword(ResetPassword_Model objModel)
        //{
        //    Group model = new Group();
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (objModel.New_Password == objModel.Confirm_Password)
        //            {
        //                var result = objEmpRepo.ChangeUserPassword(objModel, HttpContext.Request.Cookies["Token"].ToString());
        //                if (result.ReturnMessage == "1")
        //                {
        //                    @ViewBag.Message = "Password Change Successfully!!!";
        //                    @ViewBag.HideClass = "alert alert-success";
        //                    ModelState.Clear();


        //                    long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
        //                    ResetPassword_Model objModelN = new ResetPassword_Model();
        //                    objModelN.SLNO = SLNO;
        //                    return View(objModelN);
        //                }
        //                else
        //                {
        //                    @ViewBag.Message = result.ReturnMessage;
        //                    @ViewBag.HideClass = "alert alert-danger";
        //                }
        //            }
        //            else
        //            {
        //                @ViewBag.Message = "New Password and Confirm Password should be same!";
        //                @ViewBag.HideClass = "alert alert-danger";
        //            }
        //        }
        //        else
        //        {
        //            @ViewBag.Message = "Enter Mandatory Fields!";
        //            @ViewBag.HideClass = "alert alert-danger";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "AuthController/AdminChangePassword");
        //    }
        //    return View(objModel);
        //}

        #endregion

        #region AdminChangePassword (POST)

        [HttpPost]
        [SessionAuthorizeFilter("AdminChangePassword")]
        public IActionResult AdminChangePassword(ResetPassword_Model objModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (objModel.New_Password == objModel.Confirm_Password)
                    {
                        var result = objEmpRepo.ChangeUserPassword(objModel, HttpContext.Request.Cookies["Token"].ToString());
                        if (result == "1")
                        {
                            @ViewBag.Message = "Password Change Successfully!!!";
                            @ViewBag.HideClass = "alert alert-success";
                            ModelState.Clear();


                            long SLNO = Convert.ToInt64(HttpContext.Request.Cookies["UserId"].ToString());
                            ResetPassword_Model objModelN = new ResetPassword_Model();
                            objModelN.SLNO = SLNO;
                            return View(objModelN);
                        }
                        else
                        {
                            @ViewBag.Message = result;
                            @ViewBag.HideClass = "alert alert-danger";
                        }
                    }
                    else
                    {
                        @ViewBag.Message = "New Password and Confirm Password should be same!";
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Enter Mandatory Fields!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "AuthController/AdminChangePassword");
            }
            return View(objModel);
        }

        #endregion

        #region Menu Data Dynamic

        private string getcompanypaneldataformenu(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";
            string html = "";
            if (Forms.Contains("Sub_company_List") || Forms.Contains("Create_Sub_company") || Forms.Contains("EmailDomainManager")
                    || Forms.Contains("Department_List") || Forms.Contains("Department_master")
                    || Forms.Contains("Domain_Mapping") || Forms.Contains("Employee_search")
                    )
            {
                html += "<li class='xn-openable @(ViewBag.Panel == 'Company Panel' ? 'active' : '')'>";
                html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Company Panel</span></a>";
                html += "<ul>";
                if (mo.Contains("Company"))
                {
                    if (Forms.Contains("Sub_company_List") || Forms.Contains("Create_Sub_company"))
                    {
                        html += "<li class='xn-openable SubCompany'>";
                        html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Sub Company</span></a><ul>";
                        if (Forms.Contains("Sub_company_List"))
                            html += "<li class='NewSubCompany'><a onclick=\"href = '" + baseUrl + "SubCompany/NewSubCompany'\" ><span class='fa fa-circle-o'></span> Add New Sub Company</a></li>";
                        if (Forms.Contains("Create_Sub_company"))
                            html += "<li class='ListSubCompany'><a onclick=\"href = '" + baseUrl + "SubCompany/ListSubCompany'\" ><span class='fa fa-list-ul'></span> Sub Company List</a></li>";
                        html += "</ul> </li>";
                    }
                    if (Forms.Contains("EmailDomainManager"))
                        html += " <li class='@(ViewBag.Active == 'MailDomain' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MailDomain/NewMailDomain'\" ><span class='fa fa-circle-o'></span> Manage Email Domain</a></li>";
                    if (Forms.Contains("Department_List") || Forms.Contains("Department_master") || Forms.Contains("Domain_Mapping"))
                    {
                        html += "<li class='xn-openable @(ViewBag.Open == 'Domain' ? 'active' : '')'>";
                        html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Functional Domain</span></a>";
                        html += " <ul>";
                        if (Forms.Contains("Department_List"))
                            html += "<li class='@(ViewBag.Active == 'ListDomain' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Domain/ListDomain'\" ><span class='fa fa-list-ul'></span> Domain List</a></li>";
                        if (Forms.Contains("Department_master"))
                            html += "<li class='@(ViewBag.Active == 'NewDomain' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Domain/NewDomain'\" ><span class='fa fa-circle-o'></span> Add New Domain</a></li>";
                        if (Forms.Contains("Domain_Mapping"))
                            html += "<li class='@(ViewBag.Active == 'DomainMapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Domain/DomainMapping'\" ><span class='fa fa-list-ul'></span> Domain Mapping</a></li>";
                        html += "</ul></li>";
                    }
                    if (Forms.Contains("Domain_wisereport") || Forms.Contains("Employee_search"))
                    {
                        html += "<li class='xn-openable @(ViewBag.Open == 'OrganizationReports' ? 'active' : '')'>";
                        html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Reports</span></a><ul>";
                        //if (Forms.Contains("Domain_wisereport"))
                        //    html += "<li><a onclick=\"href = '" + Reports + "Domain-wisereport'\"> View </a></li>";
                        if (Forms.Contains("Employee_search"))
                            html += "<li class='@(ViewBag.Active == 'GetOrganizationChat' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "OrganizationChat/GetOrganizationChat'\" ><span class='fa fa-circle-o'></span>Organization Structure</a></li>";

                        //html += "<li><a onclick=\"href = '" + baseUrl + "DMS/CrystalReports/Company_List_rpt'\"> Company List </a></li>";
                        //html += "<li><a onclick=\"href = '" + baseUrl + "DMS/CrystalReports/DomainListReport'\"> Domain List </a></li>";

                        html += "</ul></li>";
                    }

                }
                html += "</ul></li> ";

            }
            return html;

        }


        //private string getuserpaneldataformenu(List<string> Forms, string mo)
        //{
        //    //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

        //    string html = "";
        //    /******************************User Panel*******************************/
        //    if (
        //       Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation")
        //    || Forms.Contains("skill_list-view") || Forms.Contains("skill_master")
        //    || Forms.Contains("skill_map-list") || Forms.Contains("skill_map")
        //    || Forms.Contains("Group_List") || Forms.Contains("Add_Groups")
        //    || Forms.Contains("group_map_list") || Forms.Contains("group_map")
        //    || Forms.Contains("EmpStructureChart")
        //    || Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor")
        //    || Forms.Contains("service_list") || Forms.Contains("service_master")
        //    || Forms.Contains("service_map_list") || Forms.Contains("service_map")
        //    )
        //    {
        //        html += "<li class='xn-openable @(ViewBag.Panel == 'Manage User' ? 'active' : '')'>";
        //        html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage User</span></a>";
        //        html += "<ul>";
        //        //if (mo.Contains("user"))
        //        //{
        //        if (Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation"))
        //        {
        //            html += "<li class='xn-openable @(ViewBag.Open == 'Manage Employee' ? 'active' : '')'>";
        //            html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Employee</span></a>";
        //            html += "<ul>";
        //            if (Forms.Contains("Add_user"))
        //                html += "<li class='@(ViewBag.Active == 'NewEmployee' ? 'active' : '')'><a  onclick=\"href = '" + baseUrl + "Employee/NewEmployee'\" ><span class='fa fa-circle-o'></span> Add New Employee</a></li>";
        //            if (Forms.Contains("Employee_List"))
        //                html += "<li class='@(ViewBag.Active == 'ListEmployee' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ListEmployee'\" ><span class='fa fa-list-ul'></span> Employee List</a></li>";
        //            if (Forms.Contains("ApproverLimitation"))
        //                html += "<li class='@(ViewBag.Active == 'ApproverLimitation' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ApproverLimitation'\" ><span class='fa fa-circle-o'></span> Approver Limitation</a></li>";
        //            html += "</ul> </li>";
        //        }
        //        //}
        //        if (Forms.Contains("skill_list_view") || Forms.Contains("skill_master") || Forms.Contains("skill_map_list") || Forms.Contains("skill_map"))
        //        {
        //            html += "<li class='xn-openable @(ViewBag.Open == 'Skill Master' ? 'active' : ')'>";
        //            html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Skill Master</span></a>";
        //            html += "<ul>";
        //            if (Forms.Contains("skill_master"))
        //                html += "<li class='@(ViewBag.Active == 'NewSkillMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastSkill/NewSkillMaster'\" ><span class='fa fa-circle-o'></span> Add New Skill Master</a></li>";
        //            if (Forms.Contains("skill_list_view"))
        //                html += "<li class='@(ViewBag.Active == 'ListSkillMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastSkill/ListSkillMaster'\" ><span class='fa fa-list-ul'></span> Skill Master List</a></li>";

        //            if (Forms.Contains("skill_map"))
        //                html += "<li class='@(ViewBag.Active == 'NewSkillMap ? active' : '')'><a onclick=\"href = '" + baseUrl + "SkillMap/NewSkillMap'\" ><span class='fa fa-circle-o'></span> New Skill Map</a></li>";
        //            if (Forms.Contains("skill_map_list"))
        //                html += "<li class='@(ViewBag.Active == 'ListSkillMapBypagination' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "SkillMap/ListSkillMapBypagination'\" ><span class='fa fa-list-ul'></span> Skill Map List</a></li>";
        //            html += "</ul></li>";
        //        }
        //        if (Forms.Contains("Group_List") || Forms.Contains("Add_Groups"))
        //        {
        //            html += "<li class='xn-openable @(ViewBag.Open == 'Group' ? 'active' : '')'>";
        //            html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Groups</span></a>";
        //            html += "<ul>";
        //            html += "<ul class='nav child_menu'>";
        //            if (Forms.Contains("Add_Groups"))
        //                html += "<li class='@(ViewBag.Active == 'NewGroup' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/NewGroup'\" ><span class='fa fa-circle-o'></span> Add New Group</a></li>";
        //            if (Forms.Contains("Group_List"))
        //                html += "<li class='@(ViewBag.Active == 'ListGroup' ? 'active' :'')'><a onclick=\"href = '" + baseUrl + "Group/ListGroup'\" ><span class='fa fa-list-ul'></span> Group List</a></li>";
        //            html += "<li class='@(ViewBag.Active == 'GroupMapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/GroupMapping'\" ><span class='fa fa-circle-o'></span> Group Mapping</a></li>";
        //            html += "<li class='@(ViewBag.Active == 'ListGroupMap' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/ListGroupMap'\" ><span class='fa fa-circle-o'></span> Group Mapping List</a></li>";

        //            html += "</ul> </li>";
        //        }
        //        //if (Forms.Contains("group_map_list") || Forms.Contains("group_map"))
        //        //{
        //        //    html += "<li class='gropumap'><a><i class='fa fa-desktop'></i>Group Map<span class='fa fa-chevron-down'></span></a>";
        //        //    html += "<ul class='nav child_menu'>";
        //        //    if (Forms.Contains("group_map_list"))
        //        //        html += "<li><a onclick=\"href='" + baseUrl + "group_map_list'\">View</a></li>";
        //        //    if (Forms.Contains("group_map"))
        //        //        html += "<li><a onclick=\"href='" + baseUrl + "group_map'\">Add Group</a></li>";
        //        //    html += "</ul></li>";
        //        //}
        //        if (Forms.Contains("EmpStructureChart"))
        //            html += "<li class='@(ViewBag.Active == 'ReportingStructure' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ReportingStructure'\" ><span class='fa fa-circle-o'></span> Reporting Structure</a></li>";

        //        //if (mo.Contains("vendor"))
        //        //{
        //        if (Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor"))
        //        {
        //            html += "<li class='xn-openable @(ViewBag.Open == 'Manage External User' ? 'active' : '')'>";
        //            html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage External User</span></a>";
        //            html += "<ul>";
        //            if (Forms.Contains("Add_vendor"))
        //                html += "<li class='@(ViewBag.Active == 'NewVendor' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Vendor/NewVendor'\" ><span class='fa fa-circle-o'></span> Add External User</a></li>";
        //            if (Forms.Contains("Manage_vendor"))
        //                html += "<li class='@(ViewBag.Active == 'ListVendor' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Vendor/ListVendor'\" ><span class='fa fa-list-ul'></span> External User List</a></li>";

        //            html += "</ul> </li>";
        //        }
        //        if (Forms.Contains("service_list") || Forms.Contains("service_master") || Forms.Contains("service_map_list") || Forms.Contains("service_map"))
        //        {
        //            html += "<li class='xn-openable @(ViewBag.Open == 'Service/Product Master' ? 'active' : '')'>";
        //            html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Service/Product Master</span></a>";
        //            html += "<ul>";
        //            if (Forms.Contains("service_master"))
        //                html += "<li class='@(ViewBag.Active == 'NewServiceProductMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastServPro/NewServiceProductMaster'\" ><span class=fa fa-circle-o></span> Add Service </a></li>";
        //            if (Forms.Contains("service_list"))
        //                html += "<li class='@(ViewBag.Active == 'ListServiceProductMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastServPro/ListServiceProductMaster'\"><span class='fa fa-list-ul'></span> Service/Product List</a></li>";
        //            if (Forms.Contains("service_map"))
        //                html += "<li class='@(ViewBag.Active == 'NewProSerSkillMap' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "ProSerSkillMap/NewProSerSkillMap'\" ><span class=fa fa-circle-o></span> Add Service/Product Map</a></li>";
        //            if (Forms.Contains("service_map_list"))
        //                html += "<li class='@(ViewBag.Active == 'ListProSerSkillMapBypagination' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "ProSerSkillMap/ListProSerSkillMapBypagination'\" ><span class=fa fa-list-ul></span> Service/Product Map List</a></li>";
        //            html += "</ul></li>";
        //        }

        //        //}
        //        html += "</ul></li>";
        //    }
        //    /*****************************End User Panel*****************************************/
        //    return html;

        //}


        private string geteippaneldataformenu(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************EIP Panel*******************************/
            if (Forms.Contains("EmailManager") || Forms.Contains("calendar") || Forms.Contains("add-calendar")
                    || Forms.Contains("Manage_Event") || Forms.Contains("Announcement_List") || Forms.Contains("Manage_Discussion")
                    )
            {
                html += "<li class='xn-openable @(ViewBag.Panel == 'EIP Panel' ? 'active' : '')'>";
                html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>EIP Panel</span></a>";
                html += "<ul>";
                //if (Forms.Contains("EmailManager"))
                //    html += " <li><a onclick=\"href = '" + baseUrl + "DMS/EmailManager/EmailManager'\"> Manage Email Template</a></li>";
                if (Forms.Contains("calendar") || Forms.Contains("add_calendar"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Calender' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'> Factory Calender</span></a>";
                    html += "<ul>";
                    html += "<li class='@(ViewBag.Active == 'NewCompanyHoliday' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "CompanyHoliday/NewCompanyHoliday'\" ><span class='fa fa-circle-o'></span> New Calender </a></li>";
                    html += "<li class='@(ViewBag.Active == 'ListCompanyHoliday' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "CompanyHoliday/ListCompanyHoliday'\" ><span class='fa fa-list-ul'></span> List Calender</a></li>";
                    html += "</ul></li>";
                }
                if (Forms.Contains("Manage_Event"))
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Event Master' ? 'active' : '')''>";
                html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Events</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'NewEventMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "EventMaster/NewEventMaster'\" ><span class='fa fa-circle-o'></span> Add New Event Master</a></li>";
                html += "<li class='@(ViewBag.Active == 'ListEventMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "EventMaster/ListEventMaster'\" ><span class='fa fa-list-ul'></span> Event List</a></li>";
                html += "</ul>";
                html += "</li>";
                if (Forms.Contains("Announcement_List"))
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Announcement' ? 'active' : '')'>";
                html += "<a href = '#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Announcement</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'CreateNewAnnouncement' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Announcement/CreateNewAnnouncement'\" ><span class='fa fa-circle-o'></span> Add New Announcement</a></li>";
                html += "<li class='@(ViewBag.Active == 'ListAnnouncement' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Announcement/ListAnnouncement'\" ><span class='fa fa-list-ul'></span> Announcement List</a></li>";
                html += "</ul>";
                html += "</li>";
                if (Forms.Contains("Manage_Discussion"))
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Discussion' ? 'active' : '')'>";
                html += "<a href = '#' ><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Discussion</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'NewDiscussMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Discuss/NewDiscussMaster'\" ><span class='fa fa-circle-o'></span> Add New Discussion</a></li>";
                html += "<li class='@(ViewBag.Active == 'ListDiscussMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Discuss/ListDiscussMaster'\" ><span class='fa fa-list-ul'></span> Discussion List</a></li>";
                html += "</ul>";
                html += "</li>";



                //html += "</ul></li>";
            }

            html += "</ul></li>";

            /*****************************End EIP Panel*****************************************/
            return html;

        }



        private string getuserpaneldataformenumain(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (
               Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation")
            || Forms.Contains("skill_list-view") || Forms.Contains("skill_master")
            || Forms.Contains("skill_map-list") || Forms.Contains("skill_map")
            || Forms.Contains("Group_List") || Forms.Contains("Add_Groups")
            || Forms.Contains("group_map_list") || Forms.Contains("group_map")
            || Forms.Contains("EmpStructureChart")
            || Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor")
            || Forms.Contains("service_list") || Forms.Contains("service_master")
            || Forms.Contains("service_map_list") || Forms.Contains("service_map")
            )
            {

                html = "UserPanel";

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getuserpaneldataformenu1(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (
               Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation")
            || Forms.Contains("skill_list-view") || Forms.Contains("skill_master")
            || Forms.Contains("skill_map-list") || Forms.Contains("skill_map")
            || Forms.Contains("Group_List") || Forms.Contains("Add_Groups")
            || Forms.Contains("group_map_list") || Forms.Contains("group_map")
            || Forms.Contains("EmpStructureChart")
            || Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor")
            || Forms.Contains("service_list") || Forms.Contains("service_master")
            || Forms.Contains("service_map_list") || Forms.Contains("service_map")
            )
            {

                if (Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Employee' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Employee</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Add_user"))
                        html += "<li class='@(ViewBag.Active == 'NewEmployee' ? 'active' : '')'><a  onclick=\"href = '" + baseUrl + "Employee/NewEmployee'\" ><span class='fa fa-circle-o'></span> Add New Employee</a></li>";
                    if (Forms.Contains("Employee_List"))
                        html += "<li class='@(ViewBag.Active == 'ListEmployee' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ListEmployee'\" ><span class='fa fa-list-ul'></span> Employee List</a></li>";
                    if (Forms.Contains("ApproverLimitation"))
                        html += "<li class='@(ViewBag.Active == 'ApproverLimitation' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ApproverLimitation'\" ><span class='fa fa-circle-o'></span> Approver Limitation</a></li>";
                    html += "</ul> </li>";
                }
                //}
                if (Forms.Contains("skill_list_view") || Forms.Contains("skill_master") || Forms.Contains("skill_map_list") || Forms.Contains("skill_map"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Skill Master' ? 'active' : ')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Skill Master</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("skill_master"))
                        html += "<li class='@(ViewBag.Active == 'NewSkillMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastSkill/NewSkillMaster'\" ><span class='fa fa-circle-o'></span> Add New Skill Master</a></li>";
                    if (Forms.Contains("skill_list_view"))
                        html += "<li class='@(ViewBag.Active == 'ListSkillMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastSkill/ListSkillMaster'\" ><span class='fa fa-list-ul'></span> Skill Master List</a></li>";

                    if (Forms.Contains("skill_map"))
                        html += "<li class='@(ViewBag.Active == 'NewSkillMap ? active' : '')'><a onclick=\"href = '" + baseUrl + "SkillMap/NewSkillMap'\" ><span class='fa fa-circle-o'></span> New Skill Map</a></li>";
                    if (Forms.Contains("skill_map_list"))
                        html += "<li class='@(ViewBag.Active == 'ListSkillMapBypagination' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "SkillMap/ListSkillMapBypagination'\" ><span class='fa fa-list-ul'></span> Skill Map List</a></li>";
                    html += "</ul></li>";
                }

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getuserpaneldataformenu2(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (
               Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation")
            || Forms.Contains("skill_list-view") || Forms.Contains("skill_master")
            || Forms.Contains("skill_map-list") || Forms.Contains("skill_map")
            || Forms.Contains("Group_List") || Forms.Contains("Add_Groups")
            || Forms.Contains("group_map_list") || Forms.Contains("group_map")
            || Forms.Contains("EmpStructureChart")
            || Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor")
            || Forms.Contains("service_list") || Forms.Contains("service_master")
            || Forms.Contains("service_map_list") || Forms.Contains("service_map")
            )
            {

                if (Forms.Contains("Group_List") || Forms.Contains("Add_Groups"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Group' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Groups</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Add_Groups"))
                        html += "<li class='@(ViewBag.Active == 'NewGroup' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/NewGroup'\" ><span class='fa fa-circle-o'></span> Add New Group</a></li>";
                    if (Forms.Contains("Group_List"))
                        html += "<li class='@(ViewBag.Active == 'ListGroup' ? 'active' :'')'><a onclick=\"href = '" + baseUrl + "Group/ListGroup'\" ><span class='fa fa-list-ul'></span> Group List</a></li>";
                    html += "<li class='@(ViewBag.Active == 'GroupMapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/GroupMapping'\" ><span class='fa fa-circle-o'></span> Group Mapping</a></li>";
                    html += "<li class='@(ViewBag.Active == 'ListGroupMap' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Group/ListGroupMap'\" ><span class='fa fa-circle-o'></span> Group Mapping List</a></li>";

                    html += "</ul> </li>";
                }
                //if (Forms.Contains("group_map_list") || Forms.Contains("group_map"))
                //{
                //    html += "<li class='gropumap'><a><i class='fa fa-desktop'></i>Group Map<span class='fa fa-chevron-down'></span></a>";
                //    html += "<ul class='nav child_menu'>";
                //    if (Forms.Contains("group_map_list"))
                //        html += "<li><a onclick=\"href='" + baseUrl + "group_map_list'\">View</a></li>";
                //    if (Forms.Contains("group_map"))
                //        html += "<li><a onclick=\"href='" + baseUrl + "group_map'\">Add Group</a></li>";
                //    html += "</ul></li>";
                //}
                if (Forms.Contains("EmpStructureChart"))
                    html += "<li class='@(ViewBag.Active == 'ReportingStructure' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Employee/ReportingStructure'\" ><span class='fa fa-circle-o'></span> Reporting Structure</a></li>";



            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getuserpaneldataformenu3(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (
               Forms.Contains("Employee_List") || Forms.Contains("Add_user") || Forms.Contains("ApproverLimitation")
            || Forms.Contains("skill_list-view") || Forms.Contains("skill_master")
            || Forms.Contains("skill_map-list") || Forms.Contains("skill_map")
            || Forms.Contains("Group_List") || Forms.Contains("Add_Groups")
            || Forms.Contains("group_map_list") || Forms.Contains("group_map")
            || Forms.Contains("EmpStructureChart")
            || Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor")
            || Forms.Contains("service_list") || Forms.Contains("service_master")
            || Forms.Contains("service_map_list") || Forms.Contains("service_map")
            )
            {


                //if (mo.Contains("vendor"))
                //{
                if (Forms.Contains("Manage_vendor") || Forms.Contains("Add_vendor"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage External User' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage External User</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Add_vendor"))
                        html += "<li class='@(ViewBag.Active == 'NewVendor' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Vendor/NewVendor'\" ><span class='fa fa-circle-o'></span> Add External User</a></li>";
                    if (Forms.Contains("Manage_vendor"))
                        html += "<li class='@(ViewBag.Active == 'ListVendor' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "Vendor/ListVendor'\" ><span class='fa fa-list-ul'></span> External User List</a></li>";

                    html += "</ul> </li>";
                }
                if (Forms.Contains("service_list") || Forms.Contains("service_master") || Forms.Contains("service_map_list") || Forms.Contains("service_map"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Service/Product Master' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Service/Product Master</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("service_master"))
                        html += "<li class='@(ViewBag.Active == 'NewServiceProductMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastServPro/NewServiceProductMaster'\" ><span class=fa fa-circle-o></span> Add Service </a></li>";
                    if (Forms.Contains("service_list"))
                        html += "<li class='@(ViewBag.Active == 'ListServiceProductMaster' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "MastServPro/ListServiceProductMaster'\"><span class='fa fa-list-ul'></span> Service/Product List</a></li>";
                    if (Forms.Contains("service_map"))
                        html += "<li class='@(ViewBag.Active == 'NewProSerSkillMap' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "ProSerSkillMap/NewProSerSkillMap'\" ><span class=fa fa-circle-o></span> Add Service/Product Map</a></li>";
                    if (Forms.Contains("service_map_list"))
                        html += "<li class='@(ViewBag.Active == 'ListProSerSkillMapBypagination' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "ProSerSkillMap/ListProSerSkillMapBypagination'\" ><span class=fa fa-list-ul></span> Service/Product Map List</a></li>";
                    html += "</ul></li>";
                }

                //}

            }
            /*****************************End User Panel*****************************************/
            return html;

        }




        private string getdmspaneldataformenumain(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                html = "DMSPanel";

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getdmspaneldataformenu1(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                if (Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                            || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                            || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Document Class' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Manage Document Class</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Add_Document_Class"))
                        html += "<li class='@(ViewBag.Active == 'NewDocumentClass' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentClass/NewDocumentClass'\" ><span class='fa fa-circle-o'></span> New Document Class</a></li>";
                    if (Forms.Contains("DocumentClass_List"))
                        html += "<li class='@(ViewBag.Active == 'ListDocumentClass' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentClass/ListDocumentClass'\" ><span class='fa fa-circle-o'></span> Document Class List</a></li>";
                    //if (Forms.Contains("Manage_RecordFields_Mapping"))
                    //    html += "<li><a onclick=\"href = '" + DMS + "Manage_RecordFields_Mapping'\"> Map record field</a></li>";
                    //if (Forms.Contains("Manage_DocumentType_Mapping"))
                    //    html += "<li><a onclick=\"href = '" + DMS + "Manage_DocumentType_Mapping'\"> Map Document Type</a></li>";
                    if (Forms.Contains("Manage_UserMapping_DocClass"))
                        html += "<li class='xn-openable @(ViewBag.OpenSub == 'Map User' ? 'active' : '')'>";
                    html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Map User</span></a>";
                    html += "<ul>";
                    html += "<li class='@(ViewBag.Active == 'NewUserMapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassUserMapping/InsertMapping'\" ><span class='fa fa-circle-o'></span> New User Mapping</a></li>";
                    html += "<li class='@(ViewBag.Active == 'ListUserMapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassUserMapping/ListMapping'\" ><span class='fa fa-list-ul'></span> User maping list</a></li>";
                    html += "</ul>";
                    html += "</li>";
                    if (Forms.Contains("DocumentTypeAccessControl_User"))
                        html += "<li class='@(ViewBag.Active == 'AddPermission' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentPermission/AddPermission'\" ><span class='fa fa-circle-o'></span> Manage Document Permission</a></li>";
                    html += "</ul></li>";
                }

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getdmspaneldataformenu2(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                if (Forms.Contains("Manage_DocumentType"))
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Document Type' ? 'active' : '')'>";
                html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Manage Document Type</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'NewDocumentType' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentType/NewDocumentType'\"  ><span class='fa fa-circle-o'></span> New Document Type</a></li>";
                html += "<li class='@(ViewBag.Active == 'ListDocumentType' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentType/ListDocumentType'\"  ><span class='fa fa-list-ul'></span> Document Type List</a></li>";

                html += "<li class='xn-penable @(ViewBag.OpenSub == 'Map Document Type' ? 'active' : '')'>";
                html += "<a href = '#' ><span class='fa fa-desktop'></span> <span class='xn-text'>Map Document Type</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'New Document Type Mapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassDocTypeMapping/InsertMapping'\" ><span class='fa fa-circle-o'></span> New Document Type Mapping</a></li>";
                html += "<li class='@(ViewBag.Active == 'Document Type Mapping List' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassDocTypeMapping/ListMapping'\" ><span class='fa fa-list-ul'></span> Document Type Mapping List</a></li>";
                html += "</ul>";
                html += "</li>";
                html += "</ul>";
                html += "</li>";
                //if (Forms.Contains("Manage_RecordFields_MappingDT") || usertype == "Admin")
                //    html += "<li><a onclick=\"href = '" + DMS + "Manage_RecordFields_MappingDT'\">  Record Fields Type Mapping </a></li>";
                //if (Forms.Contains("Manage_List") || usertype == "Admin")
                //    html += "<li><a onclick=\"href = '" + DMS + "Manage_List'\"> Manage Record Fields</a></li>";
                //if (Forms.Contains("Manage_Checkout") || usertype == "Admin")
                //    html += " <li><a onclick=\"href = '" + DMS + "Manage_Checkout'\"> Manage CheckOut </a></li>";

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getdmspaneldataformenu3(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                if (Forms.Contains("Manage_RecordFields_Mapping"))
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Record Field' ? 'active' : '')'>";
                html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Manage Record Field</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'NewRecordField' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "RecordField/NewRecordField'\"  ><span class='fa fa-circle-o'></span> New Record Field</a></li>";
                html += "<li class='@(ViewBag.Active == 'ListRecordField' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "RecordField/ListRecordField'\"  ><span class='fa fa-list-ul'></span> Record Field List</a></li>";

                html += "<li class='xn-openable @(ViewBag.OpenSub == 'Document Class Record Field Mapping' ? 'active' : '')'>";
                html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Record Field CL Mapping</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'New Record Field CL Mapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassRecordFieldMapping/InsertMapping'\" ><span class='fa fa-circle-o'></span> New Record Field CL Mapping</a></li>";
                html += "<li class='@(ViewBag.Active == 'Record Field CL Mapping List' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocClassRecordFieldMapping/ListMapping'\" ><span class='fa fa-list-ul'></span> Record Field CL Mapping List</a></li>";
                html += "</ul>";
                html += "</li>";
                html += "<li class='xn-openable @(ViewBag.OpenSub == 'Document Type Record Field Mapping' ? 'active' : '')'>";
                html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Record Field TL Mapping</span></a>";
                html += "<ul>";
                html += "<li class='@(ViewBag.Active == 'New Record Field TL Mapping' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocTypeRecordFieldMapping/InsertMapping'\" ><span class='fa fa-circle-o'></span> New Record Field TL Mapping</a></li>";
                html += "<li class='@(ViewBag.Active == 'Record Field TL Mapping List' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocTypeRecordFieldMapping/ListMapping'\" ><span class='fa fa-list-ul'></span> Record Field TL Mapping List</a></li>";
                html += "</ul>";
                html += "</li>";


                html += "</ul>";
                html += "</li>";
                //if (Forms.Contains("Manage_RecordFields_MappingDT") || usertype == "Admin")
                //    html += "<li><a onclick=\"href = '" + DMS + "Manage_RecordFields_MappingDT'\">  Record Fields Type Mapping </a></li>";
                //if (Forms.Contains("Manage_List") || usertype == "Admin")
                //    html += "<li><a onclick=\"href = '" + DMS + "Manage_List'\"> Manage Record Fields</a></li>";
                //if (Forms.Contains("Manage_Checkout") || usertype == "Admin")
                //    html += " <li><a onclick=\"href = '" + DMS + "Manage_Checkout'\"> Manage CheckOut </a></li>";

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getdmspaneldataformenu4(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                if (Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Document Sensitivity' ? 'active' : '')'>";
                    html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class='xn-text'>Manage Document Sensitivity</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Add_DocumentSensitivity"))
                        html += "<li class='@(ViewBag.Active == 'NewDocumentSensitivity' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentSensitivity/NewDocumentSensitivity'\" ><span class='fa fa-circle-o'></span> New Document Sensitivity</a></li>";
                    if (Forms.Contains("Manage_DocumentSensitivity"))
                        html += "<li class='@(ViewBag.Active == 'ListDocumentSensitivity' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentSensitivity/ListDocumentSensitivity'\" ><span class='fa fa-list-ul'></span> Document Sensitivity List</a></li>";

                    html += " </ul></li>";
                }
                if (Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permissionpage"))
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'Manage Document Set' ? 'active' : '')'>";
                    html += "<a href='#'><span class='fa fa-desktop'></span> <span class='xn-text'>Document Set</span></a>";
                    html += "<ul>";
                    if (Forms.Contains("Document_Set_Add"))
                        html += "<li class='@(ViewBag.Active == 'NewDocumentSet' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentSet/NewDocumentSet'\" ><span class='fa fa-circle-o'></span> New Document Set</a></li>";
                    if (Forms.Contains("Document_Set_List"))
                        html += "<li class='@(ViewBag.Active == 'ListDocumentSet' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentSet/ListDocumentSet'\" ><span class='fa fa-list-ul'></span> Document Set List</a></li>";
                    if (Forms.Contains("Document_Set_Permissionpage"))
                        html += "<li class='@(ViewBag.Active == 'DocumentSetPermission' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DocumentSet/DocumentSetPermission'\" ><span class='fa fa-list-ul'></span> Document Set Permission</a></li>";
                    html += " </ul></li>";
                }

            }
            /*****************************End User Panel*****************************************/
            return html;

        }

        private string getdmspaneldataformenu5(List<string> Forms, string mo)
        {
            //string baseUrl = HttpContext.cu.Request.Url.GetLeftPart(UriPartial.Authority) + @"/";

            string html = "";
            /******************************User Panel*******************************/
            if (Forms.Contains("Manage_Indexes")
                    || Forms.Contains("DocumentClass_List") || Forms.Contains("Add_Document_Class")
                      || Forms.Contains("Manage_RecordFields_Mapping") || Forms.Contains("Manage_DocumentType_Mapping")
                    || Forms.Contains("Manage_UserMapping_DocClass") || Forms.Contains("DocumentTypeAccessControl_User")
                    || Forms.Contains("Manage_RecordFields_MappingDT")
                    || Forms.Contains("Document_Set_List") || Forms.Contains("Document_Set_Add") || Forms.Contains("Document_Set_Permission")
                    || Forms.Contains("Manage_DocumentSensitivity") || Forms.Contains("Add_DocumentSensitivity")
                    || Forms.Contains("Manage_DocumentType") || Forms.Contains("Manage_List") || Forms.Contains("Manage_Checkout")
                    )
            {

                if (Forms.Contains("Manage_Checkout"))
                    html += "<li class='@(ViewBag.Active == 'ManageCheckout' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "ManageCheckout/DocumentPermission'\"><span class='fa fa-circle-o'></span> Manage Checkout</a></li>";
                if (Forms.Contains("Manage_DocumentClass_Archieve")
                    || Forms.Contains("documentrevisionreport")
                    || Forms.Contains("doc_uploadreport")
                    || Forms.Contains("documentclass_report")
                    )
                {
                    html += "<li class='xn-openable @(ViewBag.Open == 'DMS Reports' ? 'active' : '')'>";
                    html += "<a href = '#' >< span class='fa fa-desktop'></span> <span class=xn-text'>DMS Reports</span></a>";
                    html += "<ul>";
                    //if (Forms.Contains("Manage_DocumentClass_Archieve"))
                    //    html += "<li><a onclick=\"href = '" + DMS + "Manage_DocumentClass_Archieve'\"> Document Class Archieve </a></li>";
                    if (Forms.Contains("documentrevisionreport"))
                        html += "<li class='@(ViewBag.Active == 'DocumentRevisionReport' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DMS_Reports/DocumentRevisionReport'\" ><span class='fa fa-circle-o'></span> Document Revision Report</a></li>";
                    if (Forms.Contains("doc_uploadreport"))
                        html += "<li class='@(ViewBag.Active == 'DocumentUploadReport' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DMS_Reports/DocumentUploadReport'\" ><span class='fa fa-circle-o'></span> Document Upload Report</a></li>";
                    if (Forms.Contains("documentclass_report"))
                        html += "<li class='@(ViewBag.Active == 'DocumentClassAccessReport' ? 'active' : '')'><a onclick=\"href = '" + baseUrl + "DMS_Reports/DocumentClassAccessReport'\" ><span class='fa fa-circle-o'></span> Document Class Access</a></li>";
                    //html += "<li><a onclick=\"href = '" + baseUrl + "DMS/CrystalReports/DocumentClassListReport'\">Document Class </a></li>";

                    html += " </ul></li>";
                }

            }
            /*****************************End User Panel*****************************************/
            return html;

        }


        #endregion
    }
}