﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_APP.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        DropDown_Repository objDrop = new DropDown_Repository();
        Report_Repository objReport = new Report_Repository();
        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetTotalCountPolicy")]
        public JsonResult GetTotalCountPolicy()
        {
            PolicyCount_API data = new PolicyCount_API();
            data = objDrop.GetTotalCountPolicy(HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetTotalRenewalPolicy")]
        public JsonResult GetTotalRenewalPolicy()
        {
            PolicyCount_API data = new PolicyCount_API();
            data = objDrop.GetTotalRenewalPolicy(HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }
        
        [HttpPost]
        [SessionAuthorizeFilter("GetDailyBusinessReportForChart")]
        public JsonResult GetDailyBusinessReportForChart()
        {
            string InsCompChart = string.Empty;
            string CatChart = string.Empty;
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            string sDate = startDate.ToString("yyyy-MM-dd");
            string eDate = endDate.ToString("yyyy-MM-dd");
            DailyBusiness modal = new DailyBusiness();
            modal = objReport.ViewAdminDailyBUsiness(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), sDate, eDate);

            if ( modal.ViewPolicy != null)
            {
                InsCompChart += " var chart = new CanvasJS.Chart('chartContainer', { theme: 'light1', title: {  text: 'Insurance Company Wise Daily Business' },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Rupess', legendText: '{ indexLabel}', dataPoints: [ ";
                for (int i = 0; i < modal.ViewPolicy.Count; i++)
                {
                    InsCompChart += " { y: " + modal.ViewPolicy[i].PremiumPayable.ToString() + ", indexLabel: '" + modal.ViewPolicy[i].InsCo.ToString() + "' },";
                }
                InsCompChart += " ] } ] }); chart.render(); ";
            }
            modal.CompWiseChart = InsCompChart;

            if (modal.ViewPolicy1 != null)
            {
                CatChart += " var chart = new CanvasJS.Chart('chartContainer1', { theme: 'light1', title: {  text: 'Category Wise Daily Business' },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Rupess', legendText: '{ indexLabel}', dataPoints: [ ";
                for (int i = 0; i < modal.ViewPolicy1.Count; i++)
                {
                    CatChart += " { y: " + modal.ViewPolicy1[i].PremiumPayable.ToString() + ", indexLabel: '" + modal.ViewPolicy1[i].Category.ToString() + "' },";
                }
                CatChart += " ] } ] }); chart.render(); ";
            }
            modal.CategoryWiseChart = CatChart;
            return Json(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetMonthlySalesForGraph")]
        public JsonResult GetMonthlySalesForGraph()
        {
            string graphStr = string.Empty;
            List<DashboardGraph> datalist = new List<DashboardGraph>();
            datalist = objReport.GetMonthlySalesForGraph(HttpContext.Request.Cookies["Token"].ToString());
            if (datalist.Count > 0)
            {
                string cYear = DateTime.Now.Year.ToString();
                graphStr += "  var options = { animationEnabled: true, title: { text: 'Monthly Sales - " + cYear + "' }, axisX: { valueFormatString: 'MMM' }, axisY: { title: 'Sales(in Rupees)', prefix: 'Rs - ', includeZero: false }, data: [{ yValueFormatString: '$#,###', xValueFormatString: 'MMMM', type: 'spline', dataPoints: [ ";
                for (int i = 0; i < datalist.Count; i++)
                {
                    graphStr += " { x: new Date(" + cYear + ", " + datalist[i].Id + "), y: " + datalist[i].SaleAmount.ToString() + " },";
                    //graphStr += " { x: new Date(2017, 1), y: 27980 },";
                    //graphStr += " { x: new Date(2017, 2), y: 33800 },";
                    //graphStr += " { x: new Date(2017, 3), y: 49400 },";
                    //graphStr += " { x: new Date(2017, 4), y: 40260 },";
                    //graphStr += " { x: new Date(2017, 5), y: 33900 },";
                    //graphStr += " { x: new Date(2017, 6), y: 48000 },";
                    //graphStr += " { x: new Date(2017, 7), y: 31500 },";
                    //graphStr += " { x: new Date(2017, 8), y: 32300 },";
                    //graphStr += " { x: new Date(2017, 9), y: 42000 },";
                    //graphStr += " { x: new Date(2017, 10), y: 52160 },";
                    //graphStr += " { x: new Date(2017, 11), y: 49400 }";
                }
                graphStr += " ] }] }; $('#graph').CanvasJSChart(options); ";
            }
            return Json(graphStr);
        }
    }
}