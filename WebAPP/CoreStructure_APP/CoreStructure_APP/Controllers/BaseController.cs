﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CoreStructure_APP.Controllers
{
    public class BaseController : Controller
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            //Response.Cache.SetNoStore();
            var location = new Uri($"{Request.Scheme}://{Request.Host}{Request.Path}{Request.QueryString}");
            var url = location.AbsoluteUri;

            try
            {
                if (HttpContext.Request.Cookies["Token"].ToString() == null)
                {
                    filterContext.Result = new RedirectResult("~/Auth/Login?Returnurl=" + url + "");
                }
                else
                {
                    // filterContext.Result = new RedirectResult("~/Logins/Logout");
                }
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Auth/Login?Returnurl=" + url + "");
            }
        }
    }
}