﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;


namespace CoreStructure_APP.Controllers
{
    public class ReportController : BaseController
    {
        DropDown_Repository objDrop = new DropDown_Repository();
        Report_Repository objReport = new Report_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        public ReportController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }
        #region PolicyReport
        public IActionResult PolicyReport(Policy model)
        {
            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            List<Policy> objdatalist = new List<Policy>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objReport.ViewPolicyReport(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, model.TypeId, model.FromDate, model.ToDate, model.ContactPersonId, model.CategoryId, model.InsuranceCompanyId, model.IssuingOfficeId, model.ProductId, model.GroupId, model.PolicyStatusId);
            ViewBag.PolicyList = objdatalist;
            return View();
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetIssuingOfficeList")]
        public JsonResult GetIssuingOfcListbyInsCoId(int InsuranceCompanyId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetIssuingOfcListbyInsCoId(InsuranceCompanyId, HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetCategorylistListbyInsCoId")]
        public JsonResult GetCategorylistListbyInsCoId(int InsuranceCompanyId, int catid)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetCategorylistListbyInsCoId(InsuranceCompanyId, catid, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString());
            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetIssuingofficeByInsco")]
        public JsonResult GetIssuingofficeByInsco(int InsuranceCompanyId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetIssuingofficeByInsco(InsuranceCompanyId, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString());
            return Json(data);
        }
        public IActionResult Excel(PolicyReport model)
        {
            List<Policy> objdatalist = new List<Policy>();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy Holder Name";
                worksheet.Cell(currentRow, 3).Value = "Contact Person Name";
                worksheet.Cell(currentRow, 4).Value = "Policy";
                worksheet.Cell(currentRow, 5).Value = "Insurance Company";
                worksheet.Cell(currentRow, 6).Value = "Policy No";
                worksheet.Cell(currentRow, 7).Value = "Expiry Date";
                worksheet.Cell(currentRow, 8).Value = "Premium";
                worksheet.Cell(currentRow, 9).Value = "GST";
                worksheet.Cell(currentRow, 10).Value = "Status";
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdatalist = objReport.ViewPolicyReportExcel(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, model.HdTypeId, model.hdfromdate, model.hdtodate, model.hdContactPersonId, model.hdCategoryId, model.hdInsuranceCompanyId, model.hdIssuingOfficeId, model.hdProductId, model.hdGroupId, model.hdPolicyStatusId);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyHolderName;
                    worksheet.Cell(currentRow, 3).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 4).Value = user.ProductName;
                    worksheet.Cell(currentRow, 5).Value = user.InsuranceCompanyName;
                    worksheet.Cell(currentRow, 6).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 7).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 8).Value = user.Premium;
                    worksheet.Cell(currentRow, 9).Value = user.GST;
                    worksheet.Cell(currentRow, 10).Value = user.PolicyStatusName;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "PolicyReport.xlsx");
                }
            }
        }
        //public ActionResult GetPolicyReportExportexcel(int TypeId, string FromDate, string ToDate, int ContactPersonId, int CategoryId, int InsuranceCompanyId,int IssuingOfficeId,int ProductId,int GroupId,int PolicyStatusId)
        //{
        //    var result = "";
        //    result = string.Empty;

        //    List<Policy> objdatalist = new List<Policy>();
        //    DataSet dset = new DataSet();

        //    dset= objReport.GetPolicyReportExportexcel(TypeId, FromDate, ToDate, ContactPersonId, CategoryId, InsuranceCompanyId, IssuingOfficeId, ProductId, GroupId, PolicyStatusId);

        //    if (dset.Tables[0].Rows.Count > 0)
        //    {
        //        model.objdatalist = (from item in dset.Tables[0].AsEnumerable()
        //                                  select new Policy
        //                                  {

        //                                  }).ToList();
        //    }


        //    GridView gv = new GridView();
        //    gv.DataSource = model.Employeedatalist;
        //    gv.DataBind();
        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment; filename=" + Datatext + ".xls");
        //    Response.ContentType = "application/ms-excel";
        //    Response.Charset = "";
        //    StringWriter sw = new StringWriter();


        //    HtmlTextWriter htw = new HtmlTextWriter(sw);
        //    gv.RenderControl(htw);
        //    Response.Output.Write(sw.ToString());

        //    Response.Flush();
        //    Response.End();

        //    return View();
        //}
        #endregion
        #region AdminPolicyReport
        public IActionResult AdminPolicyReport(Policy model)
        {
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            List<Policy> objdatalist = new List<Policy>();
            //int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objReport.AdminViewPolicyReport(HttpContext.Request.Cookies["Token"].ToString(), model.OfficeId, model.TypeId, model.FromDate, model.ToDate, model.ContactPersonId, model.CategoryId, model.InsuranceCompanyId, model.IssuingOfficeId, model.ProductId, model.GroupId, model.PolicyStatusId);
            ViewBag.PolicyList = objdatalist;
            return View();
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetIssuingOfficeList")]
        public JsonResult GetIssuingOfcListbyInsCoIdAdmin(int InsuranceCompanyId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetIssuingOfcListbyInsCoId(InsuranceCompanyId, HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            return Json(data);
        }

        public IActionResult ExcelAdminPolicyReport(PolicyReport model)
        {
            List<Policy> objdatalist = new List<Policy>();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy Holder Name";
                worksheet.Cell(currentRow, 3).Value = "Contact Person Name";
                worksheet.Cell(currentRow, 4).Value = "Policy";
                worksheet.Cell(currentRow, 5).Value = "Insurance Company";
                worksheet.Cell(currentRow, 6).Value = "Policy No";
                worksheet.Cell(currentRow, 7).Value = "Expiry Date";
                worksheet.Cell(currentRow, 8).Value = "Premium";
                worksheet.Cell(currentRow, 9).Value = "GST";
                worksheet.Cell(currentRow, 10).Value = "Status";
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdatalist = objReport.ViewPolicyReportExcel(HttpContext.Request.Cookies["Token"].ToString(), model.hdOfficeid, model.HdTypeId, model.hdfromdate, model.hdtodate, model.hdContactPersonId, model.hdCategoryId, model.hdInsuranceCompanyId, model.hdIssuingOfficeId, model.hdProductId, model.hdGroupId, model.hdPolicyStatusId);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyHolderName;
                    worksheet.Cell(currentRow, 3).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 4).Value = user.ProductName;
                    worksheet.Cell(currentRow, 5).Value = user.InsuranceCompanyName;
                    worksheet.Cell(currentRow, 6).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 7).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 8).Value = user.Premium;
                    worksheet.Cell(currentRow, 9).Value = user.GST;
                    worksheet.Cell(currentRow, 10).Value = user.PolicyStatusName;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "PolicyReport.xlsx");
                }
            }
        }
        #endregion
        #region AdminDailyBusiness
        public IActionResult AdminDailyBusiness(DailyBusiness model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            int Office = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            if (Office > 0)
            {
                list = list.Where(x => Convert.ToInt32(x.Value) == Office).ToList();
                ViewBag.OfficeList = list;
                model.OfficeId = Office;
            }
            else
            {
                ViewBag.OfficeList = list;
            }
            if (String.IsNullOrEmpty(model.FromDate) || String.IsNullOrEmpty(model.ToDate))
            {
                model.FromDate = DateTime.Now.ToString("yyyy-MM-dd");
                model.ToDate = DateTime.Now.ToString("yyyy-MM-dd");
            }
            DailyBusiness objdatalist = new DailyBusiness();
            objdatalist = objReport.ViewAdminDailyBUsiness(HttpContext.Request.Cookies["Token"].ToString(), model.OfficeId, model.FromDate, model.ToDate);
            ViewBag.DailyBusinessData = objdatalist.ViewPolicy;
            ViewBag.DailyBusinessData1 = objdatalist.ViewPolicy1;
            return View();
        }
        #endregion
        #region AdminDailyBusinessClientWise
        public IActionResult AdminDailyBusinessClientWise(DailyBusiness model)
        {
            int Office = 0;
            if(model.OfficeId > 0)
            {
                Office = model.OfficeId;
            }
            else
            {
                Office = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            }
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.ContactPersonListWithMobile = objDrop.GetContactPersonListWithMobile(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            DailyBusiness objdatalist = new DailyBusiness();
            objdatalist = objReport.ViewAdminDailyBusinessClientWise(HttpContext.Request.Cookies["Token"].ToString(), model.FromDate, model.ToDate, model.ClientId, model.OrderBy, Office);
            ViewBag.DailyBusinessDataClientWise = objdatalist.ViewPolicy;
            return View();
        }
        #endregion
        #region AdminDailyBusinessDateWise
        public IActionResult AdminDailyBusinessDateWise(DailyBusiness model)
        {
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            return View();
        }
        public JsonResult AdminDailyBusinessDayWiseByfilter(int OfficeId, string FromDate, string ToDate)
        {
            DailyBusiness objdatalist = new DailyBusiness();
            objdatalist = objReport.ViewAdminDailyBusinessDayWise(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, FromDate, ToDate);
            return Json(objdatalist);
        }
        #endregion
        #region UpcomingPolicy
        public IActionResult UpcomingPolicy(UpcomingPolicy model)
        {
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            List<UpcomingPolicy> objdatalist = new List<UpcomingPolicy>();
            var CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
            var Date = DateTime.Now.AddDays(model.DaysId).ToString("yyyy-MM-dd");
            //DateTime dt1 = Convert.ToDateTime(CurrentDate);
            //DateTime dt2 = Convert.ToDateTime(Date);
            objdatalist = objReport.ViewUpcomingPolicy(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, CurrentDate, Date);
            ViewBag.UpcomingPolicyList = objdatalist;
            return View(model);
        }

        public IActionResult ExcelUpcomingPolicy(UpcomingPolicy model)
        {
            List<UpcomingPolicy> objdatalist = new List<UpcomingPolicy>();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy No";
                worksheet.Cell(currentRow, 3).Value = "Policy Date";
                worksheet.Cell(currentRow, 4).Value = "PH Name";
                worksheet.Cell(currentRow, 5).Value = "CP Name";
                worksheet.Cell(currentRow, 6).Value = "Ins Co";
                worksheet.Cell(currentRow, 7).Value = "Issuing Office";
                worksheet.Cell(currentRow, 8).Value = "Premium";
                worksheet.Cell(currentRow, 9).Value = "Category";
                worksheet.Cell(currentRow, 10).Value = "Product";
                worksheet.Cell(currentRow, 11).Value = "Subject Insured";
                worksheet.Cell(currentRow, 12).Value = "Cost Center";
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                var CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
                var Date = DateTime.Now.AddDays(model.hdDaysId).ToString("yyyy-MM-dd");
                //DateTime dt1 = Convert.ToDateTime(CurrentDate);
                //DateTime dt2 = Convert.ToDateTime(Date);
                objdatalist = objReport.ViewUpcomingPolicy(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, CurrentDate, Date);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 3).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 4).Value = user.Policyholdername;
                    worksheet.Cell(currentRow, 5).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 6).Value = user.InsCompany;
                    worksheet.Cell(currentRow, 7).Value = user.IssuingOfc;
                    worksheet.Cell(currentRow, 8).Value = user.PremiumPayable;
                    worksheet.Cell(currentRow, 9).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 10).Value = user.ProductName;
                    worksheet.Cell(currentRow, 11).Value = user.SubjectInsured;
                    worksheet.Cell(currentRow, 12).Value = user.CostCenterName;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Upcoming Policy.xlsx");
                }
            }
        }
        #endregion
        #region AdminUpcomingPolicy
        public IActionResult AdminUpcomingPolicy(UpcomingPolicy model)
        {
            //ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            //int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            List<UpcomingPolicy> objdatalist = new List<UpcomingPolicy>();
            var CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
            var Date = DateTime.Now.AddDays(model.DaysId).ToString("yyyy-MM-dd");
            //DateTime dt1 = Convert.ToDateTime(CurrentDate);
            //DateTime dt2 = Convert.ToDateTime(Date);
            objdatalist = objReport.ViewUpcomingPolicy(HttpContext.Request.Cookies["Token"].ToString(), model.OfficeId, CurrentDate, Date);
            ViewBag.UpcomingPolicyList = objdatalist;
            return View(model);
        }

        public IActionResult ExcelAdminUpcomingPolicy(UpcomingPolicy model)
        {
            List<UpcomingPolicy> objdatalist = new List<UpcomingPolicy>();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy No";
                worksheet.Cell(currentRow, 3).Value = "Policy Date";
                worksheet.Cell(currentRow, 4).Value = "PH Name";
                worksheet.Cell(currentRow, 5).Value = "CP Name";
                worksheet.Cell(currentRow, 6).Value = "Ins Co";
                worksheet.Cell(currentRow, 7).Value = "Issuing Office";
                worksheet.Cell(currentRow, 8).Value = "Premium";
                worksheet.Cell(currentRow, 9).Value = "Category";
                worksheet.Cell(currentRow, 10).Value = "Product";
                worksheet.Cell(currentRow, 11).Value = "Subject Insured";
                worksheet.Cell(currentRow, 12).Value = "Cost Center";
                //int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                var CurrentDate = DateTime.Now.ToString("yyyy-MM-dd");
                var Date = DateTime.Now.AddDays(model.hdDaysId).ToString("yyyy-MM-dd");
                //DateTime dt1 = Convert.ToDateTime(CurrentDate);
                //DateTime dt2 = Convert.ToDateTime(Date);
                objdatalist = objReport.ViewUpcomingPolicy(HttpContext.Request.Cookies["Token"].ToString(), model.hdOfficeId, CurrentDate, Date);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 3).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 4).Value = user.Policyholdername;
                    worksheet.Cell(currentRow, 5).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 6).Value = user.InsCompany;
                    worksheet.Cell(currentRow, 7).Value = user.IssuingOfc;
                    worksheet.Cell(currentRow, 8).Value = user.PremiumPayable;
                    worksheet.Cell(currentRow, 9).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 10).Value = user.ProductName;
                    worksheet.Cell(currentRow, 11).Value = user.SubjectInsured;
                    worksheet.Cell(currentRow, 12).Value = user.CostCenterName;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Upcoming Policy.xlsx");
                }
            }
        }
        #endregion
        #region ClaimReport
        public IActionResult ClaimReport(ClaimReport model)
        {

            ViewBag.PolicyNoList = objDrop.GetAllPolicyNoList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetPolicyholderList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            List<ClaimReport> objdatalist = new List<ClaimReport>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            var PolicyNo = model.PolicyNo;
            if (PolicyNo == "---Select Type---")
            {
                model.PolicyNo = string.Empty;
            }
            objdatalist = objReport.ViewClaimReport(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, model.PolicyNo, model.ContactPersonId, model.PolicyholderId);
            ViewBag.ClaimStatusList = objdatalist;
            return View(model);
        }

        public IActionResult ExcelClaimReport(ClaimReport model)
        {
           
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Date";
                worksheet.Cell(currentRow, 3).Value = "Policy Holder Name";
                worksheet.Cell(currentRow, 4).Value = "Patient Name";
                worksheet.Cell(currentRow, 5).Value = "Contact Person";
                worksheet.Cell(currentRow, 6).Value = "Policy No";
                worksheet.Cell(currentRow, 7).Value = "Policy Date";
                worksheet.Cell(currentRow, 8).Value = "Expiry Date";
                worksheet.Cell(currentRow, 9).Value = "Claim Intimation";
                worksheet.Cell(currentRow, 10).Value = "Surveyor";
                worksheet.Cell(currentRow, 11).Value = "Category(Cashless/Reim)";
                worksheet.Cell(currentRow, 12).Value = "Ins Co";
                worksheet.Cell(currentRow, 13).Value = "TP Name";
                worksheet.Cell(currentRow, 14).Value = "Lodge Amount";
                worksheet.Cell(currentRow, 15).Value = "Amount Settled";
                worksheet.Cell(currentRow, 16).Value = "Status";
                List<ClaimReport> objdatalist = new List<ClaimReport>();
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                var PolicyNo = model.hdPolicyNo;
                if (PolicyNo == "---Select Type---")
                {
                    model.hdPolicyNo = string.Empty;
                }
                objdatalist = objReport.ViewClaimReport(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, model.hdPolicyNo, model.hdContactPersonId, model.hdPolicyholderId);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.UnderwriteDate;
                    worksheet.Cell(currentRow, 3).Value = user.PolicyholderName;
                    worksheet.Cell(currentRow, 4).Value = user.PatientName;
                    worksheet.Cell(currentRow, 5).Value = user.ContactPerson;
                    worksheet.Cell(currentRow, 6).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 7).Value = user.PolicyDate;
                    worksheet.Cell(currentRow, 8).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 9).Value = user.ClaimDate;
                    worksheet.Cell(currentRow, 10).Value = user.TPName;
                    worksheet.Cell(currentRow, 11).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 12).Value = user.InsCo;
                    worksheet.Cell(currentRow, 13).Value = user.TPName;
                    worksheet.Cell(currentRow, 14).Value = user.ClaimAmount;
                    worksheet.Cell(currentRow, 15).Value = user.SettleAmount;
                    worksheet.Cell(currentRow, 16).Value = user.ClaimStatus;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Claim Report.xlsx");
                }
            }
        }
        #endregion
        #region AdminClaimReport
        public IActionResult AdminClaimReport(ClaimReport model)
        {
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.PolicyNoList = objDrop.GetAllPolicyNoList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetPolicyholderList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            List<ClaimReport> objdatalist = new List<ClaimReport>();
            //int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            var PolicyNo = model.PolicyNo;
            if (PolicyNo == "---Select Type---")
            {
                model.PolicyNo = string.Empty;
            }
            objdatalist = objReport.ViewClaimReport(HttpContext.Request.Cookies["Token"].ToString(), model.OfficeId, model.PolicyNo, model.ContactPersonId, model.PolicyholderId);
            ViewBag.ClaimStatusList = objdatalist;
            return View(model);
        }

        public IActionResult ExcelAdminClaimReport(ClaimReport model)
        {

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Date";
                worksheet.Cell(currentRow, 3).Value = "Policy Holder Name";
                worksheet.Cell(currentRow, 4).Value = "Patient Name";
                worksheet.Cell(currentRow, 5).Value = "Contact Person";
                worksheet.Cell(currentRow, 6).Value = "Policy No";
                worksheet.Cell(currentRow, 7).Value = "Policy Date";
                worksheet.Cell(currentRow, 8).Value = "Expiry Date";
                worksheet.Cell(currentRow, 9).Value = "Claim Intimation";
                worksheet.Cell(currentRow, 10).Value = "Surveyor";
                worksheet.Cell(currentRow, 11).Value = "Category(Cashless/Reim)";
                worksheet.Cell(currentRow, 12).Value = "Ins Co";
                worksheet.Cell(currentRow, 13).Value = "TP Name";
                worksheet.Cell(currentRow, 14).Value = "Lodge Amount";
                worksheet.Cell(currentRow, 15).Value = "Amount Settled";
                worksheet.Cell(currentRow, 16).Value = "Status";
                List<ClaimReport> objdatalist = new List<ClaimReport>();
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                var PolicyNo = model.hdPolicyNo;
                if (PolicyNo == "---Select Type---")
                {
                    model.hdPolicyNo = string.Empty;
                }
                objdatalist = objReport.ViewClaimReport(HttpContext.Request.Cookies["Token"].ToString(), model.hdOfficeId, model.hdPolicyNo, model.hdContactPersonId, model.hdPolicyholderId);
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.UnderwriteDate;
                    worksheet.Cell(currentRow, 3).Value = user.PolicyholderName;
                    worksheet.Cell(currentRow, 4).Value = user.PatientName;
                    worksheet.Cell(currentRow, 5).Value = user.ContactPerson;
                    worksheet.Cell(currentRow, 6).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 7).Value = user.PolicyDate;
                    worksheet.Cell(currentRow, 8).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 9).Value = user.ClaimDate;
                    worksheet.Cell(currentRow, 10).Value = user.TPName;
                    worksheet.Cell(currentRow, 11).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 12).Value = user.InsCo;
                    worksheet.Cell(currentRow, 13).Value = user.TPName;
                    worksheet.Cell(currentRow, 14).Value = user.ClaimAmount;
                    worksheet.Cell(currentRow, 15).Value = user.SettleAmount;
                    worksheet.Cell(currentRow, 16).Value = user.ClaimStatus;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Claim Report.xlsx");
                }
            }
        }
        #endregion
        #region Renewal Report

        public IActionResult RenewalReport(RenewalReport_Filter model, string Type)
        {
            long Office = 0;
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            if (!String.IsNullOrEmpty(Type))
            {
                model.RenewalType = Convert.ToString(Type);
            }
            List<Policy> objdatalist = new List<Policy>();
            if (model.Officeid > 0)
            {
                Office = model.Officeid;
            }
            else
            {
                Office = Convert.ToInt64(HttpContext.Request.Cookies["OfficeId"].ToString());
            }
            objdatalist = objReport.ViewAdminRenewalReport(HttpContext.Request.Cookies["Token"].ToString(), Type, Office, model.FromDate, model.ToDate);
            if(model.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == model.ContactPersonId).ToList();
            }
            if (!String.IsNullOrEmpty(model.PolicyNo))
            {
                objdatalist = objdatalist.Where(x => x.PolicyNo == model.PolicyNo).ToList();
            }
            ViewBag.PolicyList = objdatalist;
            return View(model);
        }

        public IActionResult ExcelRenewalReport(RenewalReport_Filter model,string Type)
        {
            long Office = 0;
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy No";
                worksheet.Cell(currentRow, 3).Value = "Policy Date";
                worksheet.Cell(currentRow, 4).Value = "Policy Holder Name";
                worksheet.Cell(currentRow, 5).Value = "Contact Person Name";
                worksheet.Cell(currentRow, 6).Value = "Insurance Company";
                worksheet.Cell(currentRow, 7).Value = "Issuing Office";
          
                worksheet.Cell(currentRow, 8).Value = "Premium";
                worksheet.Cell(currentRow, 9).Value = "Category";
                worksheet.Cell(currentRow, 10).Value = "Product";
                worksheet.Cell(currentRow, 11).Value = "Subject Insured";
               
                worksheet.Cell(currentRow, 12).Value = "Cost Center";

                if (!String.IsNullOrEmpty(Type))
                {
                    model.RenewalType = Convert.ToString(Type);
                }
                List<Policy> objdatalist = new List<Policy>();
                if (model.hdOfficeid > 0)
                {
                    Office = model.hdOfficeid;
                }
                else
                {
                    Office = Convert.ToInt64(HttpContext.Request.Cookies["OfficeId"].ToString());
                }
                objdatalist = objReport.ViewAdminRenewalReport(HttpContext.Request.Cookies["Token"].ToString(), Type, Office, model.hdFromDate, model.hdToDate);
                if (model.ContactPersonId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.ContactPersonId == model.hdContactPersonId).ToList();
                }
                if (!String.IsNullOrEmpty(model.PolicyNo))
                {
                    objdatalist = objdatalist.Where(x => x.PolicyNo == model.hdPolicyNo).ToList();
                }
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 3).Value = user.PolicyDate;
                    worksheet.Cell(currentRow, 4).Value = user.PolicyHolderName;
                    worksheet.Cell(currentRow, 5).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 6).Value = user.InsuranceCompanyName;
                    worksheet.Cell(currentRow, 7).Value = user.IssuingOfficeName;
                    worksheet.Cell(currentRow, 8).Value = user.Premium;
                    worksheet.Cell(currentRow, 9).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 10).Value = user.ProductName;
                    worksheet.Cell(currentRow, 11).Value = user.SubjectInsured;
                    worksheet.Cell(currentRow, 12).Value = user.CostCenter;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "Renewal Report.xlsx");
                }
            }
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetContactPersonByOfficeId")]
        public JsonResult GetContactPersonByOfficeId(int OfficeId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToString(OfficeId));
            return Json(data);
        }
        #endregion
        #region MIS Report

        public IActionResult AdminMISReport()
        {
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            return View();
        }

        [HttpPost]
        [SessionAuthorizeFilter("AdminGetMISReport")]
        public JsonResult AdminGetMISReport(int Office, string fDate, string tDate)
        {
            int OfficeId = 0;
            if(Office > 0)
            {
                OfficeId = Convert.ToInt32(Office);
            }
            else
            {
                OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            }
            string InsCompChart = "";
            string CatChart = "";
            //var startDate = DateTime.ParseExact(fDate, "MMM dd yyyy", CultureInfo.InvariantCulture);
            //var endDate = DateTime.ParseExact(tDate, "MMM dd yyyy", CultureInfo.InvariantCulture);
            //string sDate = startDate.ToString("yyyy-MM-dd");
            //string eDate = endDate.ToString("yyyy-MM-dd");
            DailyBusiness modal = new DailyBusiness();
            modal = objReport.ViewAdminDailyBUsiness(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, fDate, tDate);
            //modal = objReport.ViewAdminDailyBUsiness(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), sDate, eDate);

            if (modal.ViewPolicy != null)
            {
                InsCompChart += " var chart = new CanvasJS.Chart('chartContainer', { theme: 'light1', title: {  text: 'Insurance Company Wise Daily Business', },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Rupess', legendText: '{ indexLabel}', dataPoints: [ ";
                for (int i = 0; i < modal.ViewPolicy.Count; i++)
                {
                    InsCompChart += " { y: " + modal.ViewPolicy[i].PremiumPayable.ToString() + ", indexLabel: '" + modal.ViewPolicy[i].InsCo.ToString() + "' },";
                }
                InsCompChart += " ] } ] }); chart.render(); ";
            }
            modal.CompWiseChart = InsCompChart;

            if (modal.ViewPolicy1 != null)
            {
                CatChart += " var chart = new CanvasJS.Chart('chartContainer1', { theme: 'light1', title: {  text: 'Category Wise Daily Business' },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Rupess', legendText: '{ indexLabel}', dataPoints: [ ";
                for (int i = 0; i < modal.ViewPolicy1.Count; i++)
                {
                    CatChart += " { y: " + modal.ViewPolicy1[i].PremiumPayable.ToString() + ", indexLabel: '" + modal.ViewPolicy1[i].Category.ToString() + "' },";
                }
                CatChart += " ] } ] }); chart.render(); ";
            }
            modal.CategoryWiseChart = CatChart;
            return Json(modal);
        }

        #endregion
    }
}