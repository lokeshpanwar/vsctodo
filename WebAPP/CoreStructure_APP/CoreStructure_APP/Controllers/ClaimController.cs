﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_APP.Controllers
{
    public class ClaimController : BaseController
    {
        DropDown_Repository objDrop = new DropDown_Repository();
        Claim_Repository objClaim = new Claim_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        public ClaimController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        #region lodge Claim
        public IActionResult LodgeClaim(Claim_Filter filter)
        {
            Claim_Filter modal = new Claim_Filter();
            List<Policy> objdatalist = new List<Policy>();
            objdatalist = objClaim.GetPolicyListForClaim(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            if (filter.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filter.ContactPersonId).ToList();
            }
            if (!String.IsNullOrEmpty(filter.PolicyNo))
            {
                objdatalist = objdatalist.Where(x => x.PolicyNo.Contains(filter.PolicyNo.Trim())).ToList();
            }
            if (!String.IsNullOrEmpty(filter.PolicyHolder))
            {
                objdatalist = objdatalist.Where(x => x.PolicyHolderName.Contains(filter.PolicyHolder.Trim())).ToList();
            }
            ViewBag.PolicyList = objdatalist;
            return View(modal);
        }

        public IActionResult PersonalAccidentLodgeClaim(long PId)
        {
            LodgeClaim_Model modal = new LodgeClaim_Model();
            List<TP_Surveyor> tpSurveyor = new List<TP_Surveyor>();
            TP_Surveyor TP_Model = new TP_Surveyor();
            TP_Model = objClaim.GetTPSurveyorForLodgeClaim(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), PId).FirstOrDefault();
            modal.PolicyId = PId;
            if (TP_Model != null)
            {
                modal.TPSurveyor = Convert.ToString(TP_Model.TP_Name);
                modal.TPSurveyorContactNo = Convert.ToString(TP_Model.TP_ContactNo);
            }
            return View(modal);
        }

        public IActionResult MedicalLodgeClaim(long PId)
        {
            LodgeClaim_Model modal = new LodgeClaim_Model();
            modal.PolicyId = PId;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SavePALodgeClaimDetails")]
        public IActionResult SavePALodgeClaimDetails(LodgeClaim_Model objModel)
        {
            LodgeClaim_Model model = new LodgeClaim_Model();
            try
            {
                ModelState.Remove("PatientName");
                if (ModelState.IsValid)
                {
                    string fileName = "";
                    string fileExt = "";
                    var filePath = Path.GetTempFileName();
                    foreach (var formFile in Request.Form.Files)
                    {
                        if (formFile.Length > 0)
                        {
                            byte[] array1 = new byte[formFile.Length];
                            string fName = formFile.FileName;
                            fileName = formFile.FileName;
                            fileExt = System.IO.Path.GetExtension(formFile.FileName);
                            objModel.ClaimFileName = fileName;
                            objModel.fileExt = fileExt;
                            objModel.fileData = array1;
                        }
                    }

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objClaim.SavePALodgeClaimDetails(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Claim Lodge Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Claim/SavePALodgeClaimDetails");
            }
            return RedirectToAction("LodgeClaim");
        }


        [HttpPost]
        [SessionAuthorizeFilter("SaveMedicalClaimDetails")]
        public IActionResult SaveMedicalClaimDetails(LodgeClaim_Model objModel)
        {
            LodgeClaim_Model model = new LodgeClaim_Model();
            try
            {
                if (ModelState.IsValid)
                {
                    string fileName = "";
                    string fileExt = "";
                    var filePath = Path.GetTempFileName();
                    foreach (var formFile in Request.Form.Files)
                    {
                        if (formFile.Length > 0)
                        {
                            byte[] array1 = new byte[formFile.Length];
                            string fName = formFile.FileName;
                            fileName = formFile.FileName;
                            fileExt = System.IO.Path.GetExtension(formFile.FileName);
                            objModel.ClaimFileName = fileName;
                            objModel.fileExt = fileExt;
                            objModel.fileData = array1;
                        }
                    }

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objClaim.SaveMedicalClaimDetails(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Claim Lodge Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Claim/SaveMedicalClaimDetails");
            }
            return RedirectToAction("LodgeClaim");
        }
        #endregion

        #region View Claim

        public IActionResult ViewLodgeClaim(Claim_Filter filter)
        {
            Claim_Filter modal = new Claim_Filter();
            List<ViewLodgeClaim_Model> objdatalist = new List<ViewLodgeClaim_Model>();
            objdatalist = objClaim.GetClaimedPolicyList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ClaimStepList = objDrop.GetClaimStepList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            if (!String.IsNullOrEmpty(filter.FromDate) && !String.IsNullOrEmpty(filter.ToDate))
            {
                DateTime frmdt = DateTime.Parse(filter.FromDate);
                DateTime todt = DateTime.Parse(filter.ToDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.ClaimDate) >= frmdt && DateTime.Parse(x.ClaimDate) <= todt).ToList();
            }
            if (filter.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filter.ContactPersonId).ToList();
            }
            if (filter.CategoryId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CategoryId == filter.CategoryId).ToList();
            }
            ViewBag.ClaimedPolicyList = objdatalist;
            return View(modal);
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteClaim")]
        public ActionResult DeleteClaim(long ClaimId)
        {
            string result = objClaim.DeleteClaim(ClaimId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Claim Deleted Successfully")
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-success";
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return RedirectToAction("ViewLodgeClaim");
        }

        public IActionResult EditPALodgeClaim(long ClaimId)
        {
            LodgeClaim_Model modal = new LodgeClaim_Model();
            LodgeClaim_Model modal1 = new LodgeClaim_Model();
            modal = objClaim.GetLodgeClaimDataForEdit(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ClaimId).FirstOrDefault();
            modal1.ClaimId = modal.ClaimId;
            modal1.PolicyId = modal.PolicyId;
            modal1.ClaimOfficer = modal.ClaimOfficer;
            modal1.ClaimAmount = modal.ClaimAmount;
            modal1.ClaimDate = Convert.ToDateTime(modal.ClaimDate).ToString("yyyy-MM-dd");
            modal1.ClaimNo = modal.ClaimNo;
            modal1.TPName = modal.TPName;
            modal1.Mobile = modal.Mobile;
            modal1.ClaimFileName = modal.ClaimFileName;
            modal1.TPSurveyor = modal.TPSurveyor;
            modal1.TPSurveyorContactNo = modal.TPSurveyorContactNo;
            return View(modal1);
        }
        [HttpPost]
        [SessionAuthorizeFilter("UpdatePALodgeClaimDetails")]
        public IActionResult UpdatePALodgeClaimDetails(LodgeClaim_Model objModel)
        {
            LodgeClaim_Model model = new LodgeClaim_Model();
            try
            {
                ModelState.Remove("PatientName");
                if (ModelState.IsValid)
                {
                    string fileName = "";
                    string fileExt = "";
                    var filePath = Path.GetTempFileName();
                    foreach (var formFile in Request.Form.Files)
                    {
                        if (formFile.Length > 0)
                        {
                            byte[] array1 = new byte[formFile.Length];
                            string fName = formFile.FileName;
                            fileName = formFile.FileName;
                            fileExt = System.IO.Path.GetExtension(formFile.FileName);
                            objModel.ClaimFileName = fileName;
                            objModel.fileExt = fileExt;
                            objModel.fileData = array1;
                        }
                    }

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objClaim.SavePALodgeClaimDetails(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Claim Lodge Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Claim/SavePALodgeClaimDetails");
            }
            return RedirectToAction("ViewLodgeClaim");
        }
        public IActionResult EditMedicalLodgeClaim(long ClaimId)
        {
            LodgeClaim_Model modal = new LodgeClaim_Model();
            LodgeClaim_Model modal1 = new LodgeClaim_Model();
            modal = objClaim.GetLodgeClaimDataForEdit(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ClaimId).FirstOrDefault();
            modal1.ClaimId = modal.ClaimId;
            modal1.PolicyId = modal.PolicyId;
            modal1.ClaimOfficer = modal.ClaimOfficer;
            modal1.ClaimAmount = modal.ClaimAmount;
            modal1.ClaimDate = Convert.ToDateTime(modal.ClaimDate).ToString("yyyy-MM-dd");
            modal1.ClaimNo = modal.ClaimNo;
            modal1.TPName = modal.TPName;
            modal1.PatientName = modal.PatientName;
            modal1.Mobile = modal.Mobile;
            modal1.ClaimFileName = modal.ClaimFileName;
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateMedicalClaimDetails")]
        public IActionResult UpdateMedicalClaimDetails(LodgeClaim_Model objModel)
        {
            LodgeClaim_Model model = new LodgeClaim_Model();
            try
            {
                if (ModelState.IsValid)
                {
                    string fileName = "";
                    string fileExt = "";
                    var filePath = Path.GetTempFileName();
                    foreach (var formFile in Request.Form.Files)
                    {
                        if (formFile.Length > 0)
                        {
                            byte[] array1 = new byte[formFile.Length];
                            string fName = formFile.FileName;
                            fileName = formFile.FileName;
                            fileExt = System.IO.Path.GetExtension(formFile.FileName);
                            objModel.ClaimFileName = fileName;
                            objModel.fileExt = fileExt;
                            objModel.fileData = array1;
                        }
                    }

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objClaim.SaveMedicalClaimDetails(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Claim Lodge Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Claim/UpdateMedicalClaimDetails");
            }
            return RedirectToAction("ViewLodgeClaim");
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveClaimStepsDetails")]
        public JsonResult SaveClaimStepsDetails(long ClaimId, int StepId, string stDate, long empId, string SettleAmount, string settleDate, string appRemark, string rejectRemark, string pendingRemark, int clStatusId)
        {
            string data = "";
            data = objClaim.SaveClaimStepsDetails(HttpContext.Request.Cookies["Token"].ToString(), ClaimId, StepId, stDate, empId, SettleAmount, settleDate, appRemark, rejectRemark, pendingRemark, clStatusId);
            return Json(data);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetClaimStepsDetailList")]
        public JsonResult GetClaimStepsDetailList(long ClaimId)
        {
            List<ClaimStepsDetails_Model> datalist = new List<ClaimStepsDetails_Model>();
            datalist = objClaim.GetClaimStepsDetailList(HttpContext.Request.Cookies["Token"].ToString(), ClaimId);
            return Json(datalist);
        }
        #endregion
    }
}