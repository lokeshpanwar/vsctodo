﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreStructure_APP.Controllers
{
    public class TimeSheetController : BaseController
    {
        private IHttpContextAccessor _accessor;
        private readonly IHostingEnvironment _hostingEnvironment;
        public TimeSheetController(IHttpContextAccessor accessor, IHostingEnvironment hostingEnvironment)
        {
            _accessor = accessor;
            _hostingEnvironment = hostingEnvironment;
        }

        DropDown_Repository objDrop = new DropDown_Repository();
        Timesheet_Repository objTimeRepo = new Timesheet_Repository();

        public string GetIP()
        {
            var ip = _accessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();
            return ip;
        }
        #region Fill Timesheet
        public IActionResult FillTimeSheet()
        {
            Filltimesheet_Model model = new Filltimesheet_Model();
            ViewBag.ClientList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("Getfilledtimesheetdata")]
        public JsonResult Getfilledtimesheetdata()
        {
            FilledTimesheetData modal = new FilledTimesheetData();
            string result = "";

            modal = objTimeRepo.Getfilledtimesheetdata(HttpContext.Request.Cookies["Token"].ToString());

            result += " var calendarEl = document.getElementById('calendar1'); ";
            result += " var calendar = new FullCalendar.Calendar(calendarEl, { plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],  header: { left: 'prev,next,today', center: 'title', right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth' }, ";
            result += " defaultDate: '2020-06-06', navLinks: true, businessHours: true, editable: true, events: [ ";

            if (modal.CalFillList.Count > 0)
            {
                for (int i = 0; i < modal.CalFillList.Count; i++)
                {
                    if (Convert.ToInt32(modal.CalFillList[i].IsTimesheetfill) == 1)
                    {
                        result += " {  title: 'Already Filled', start: '" + modal.CalFillList[i].Dates.ToString() + "', constraint: 'businessHours', color: '#257e4a' }, ";
                    }
                    else if (Convert.ToInt32(modal.CalFillList[i].IsTimesheetfill) == 2 && Convert.ToInt32(modal.CalFillList[i].isholiday) >= 0)
                    {
                        result += " { title: '" + modal.CalFillList[i].Remarks.ToString() + "', start: '" + modal.CalFillList[i].Dates.ToString() + "', constraint: 'businessHours', color: '#9b59b6' }, ";
                    }
                    else
                    {
                        result += " { title: 'Fill TimeSheet',  start: '" + modal.CalFillList[i].Dates.ToString() + "', constraint: 'businessHours', color: '#F8CB00',  url: 'javascript:filltimesheet(" + modal.CalFillList[i].Newdates.ToString() + ");' }, ";
                    }
                }
            }

            result += " ] }); calendar.render(); ";


            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetfilledtimesheetdataByDate")]
        public JsonResult GetfilledtimesheetdataByDate(string Date)
        {
            CalFilledTimeSheet modal = new CalFilledTimeSheet();
            modal = objTimeRepo.GetfilledtimesheetdataByDate(HttpContext.Request.Cookies["Token"].ToString(), Date);
            return Json(modal.CasualTimeList);
        }

        #region Test Function Only
        public IActionResult TestCalendar()
        {
            return View();
        }

        public IActionResult testtimesheet()
        {


            return View();
        }
        #endregion

        [HttpPost]
        [SessionAuthorizeFilter("GetSubActivityList")]
        public JsonResult GetSubActivityList(int ActivityId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetSubActivityListByActivityId(ActivityId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }

        protected static String Dateformat(string e)
        {
            string inputFormat = "dd-MM-yyyy";
            string outputFormat = "yyyy-MM-dd";
            string output = string.Empty;
            if (e == null || e == "")
            {
                output = "";
            }
            else
            {
                var dateTime = DateTime.ParseExact(e.ToString(), inputFormat, CultureInfo.InvariantCulture);
                output = dateTime.ToString(outputFormat);
            }
            return output;
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveTimesheet")]
        public JsonResult SaveTimesheet(string Date, string fromtime, string Totime, string Totaltime, string clientId, string ActivityId, string SubactivityId, string Balancetobefill, string Remarks, string NoOfHours)
        {
            Filltimesheet_Model model = new Filltimesheet_Model();
            string result = "";
            try
            {
                model.Date = Dateformat(Date);
                model.ClientId = Convert.ToInt64(clientId);
                model.ActivityId = Convert.ToInt32(ActivityId);
                model.SubActivityId = Convert.ToInt32(SubactivityId);
                model.Remarks = Remarks;
                model.NoOfHours = NoOfHours;
                model.FromTime = fromtime;
                model.ToTime = Totime;
                model.TotalTime = Totaltime;
                model.Balancetobefill = Balancetobefill;
                model.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                result = objTimeRepo.SaveTimesheet(model, HttpContext.Request.Cookies["Token"].ToString());
                if (result == "1")
                {
                    @ViewBag.Message = result;
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                else
                {
                    @ViewBag.Message = result;
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "TimeSheet/SaveTimesheet");
            }

            return Json(result);
        }

        [HttpPost]
        public PartialViewResult GetTimesheetFillGridcasual(string Date)
        {
            TimesheetGrid model = new TimesheetGrid();
            List<TimesheetGrid> objdatalist = new List<TimesheetGrid>();
            try
            {
                objdatalist = objTimeRepo.GetFillTimesheetGridData(HttpContext.Request.Cookies["Token"].ToString(), Dateformat(Date));
                ViewBag.DataList = objdatalist;
                return PartialView("_fillsheetgridcasual", model);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return PartialView("_fillsheetgridcasual", model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("DeleteSheetData")]
        public JsonResult DeleteSheetData(long SheetId)
        {
            string result = "";
            try
            {
                result = objTimeRepo.DeleteSheetData(SheetId, HttpContext.Request.Cookies["Token"].ToString());
                if (result == "1")
                {
                    return Json("Sucess");
                }
                else
                {
                    return Json("Error");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SubmitTimesheetCasual")]
        public JsonResult SubmitTimesheetCasual(string Date)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                result = objTimeRepo.SubmitTimesheetCasual(HttpContext.Request.Cookies["Token"].ToString(), Dateformat(Date));
                //return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        #endregion

        #region View Timesheet User

        public IActionResult ViewTimeSheetUser()
        {
            return View();
        }

        [HttpPost]
        public PartialViewResult GetTimesheetSelfData(string fromDate, string toDate)
        {
            SelfTimeSheet objdata = new SelfTimeSheet();
            //List<TimesheetGrid> objdatalist = new List<TimesheetGrid>();
            try
            {
                objdata = objTimeRepo.GetTimesheetSelfData(HttpContext.Request.Cookies["Token"].ToString(), fromDate, toDate);
                //ViewBag.DataList = objdatalist;
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return PartialView("_ViewSelfTimesheetData", objdata);
        }
        #endregion

        #region View Timesheet Admin

        public IActionResult AdminViewTimeSheet()
        {
            AdminViewTimesheet model = new AdminViewTimesheet();
            ViewBag.ClientList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => Convert.ToInt32(x.Value) == 3 || Convert.ToInt32(x.Value) == 4).ToList();
            return View(model);
        }

        [HttpPost]
        public PartialViewResult GetAdminTimesheetView(string FromDate, string ToDate, long ClientId, long EmployeeId, int ActivityId, int SubActivityId, int RoleId)
        {
            AdminTimeSheetOfUsers objdata = new AdminTimeSheetOfUsers();
            try
            {
                string Token = HttpContext.Request.Cookies["Token"].ToString();
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdata = objTimeRepo.GetAdminTimesheetView(Token, OfficeId, FromDate, ToDate, ClientId, EmployeeId, ActivityId, SubActivityId, RoleId);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return PartialView("_ViewAdminTimesheetData", objdata);
        }
        #endregion

        #region Delete TimeSheet
        public IActionResult AdminDeleteTimeSheet()
        {
            AdminViewTimesheet model = new AdminViewTimesheet();
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(model);
        }

        [HttpPost]
        public PartialViewResult GetTimesheetDataTobeDelete(long EmployeeId, string Date)
        {
            SelfTimeSheet objdata = new SelfTimeSheet();
            try
            {
                objdata = objTimeRepo.GetTimesheetDataTobeDelete(HttpContext.Request.Cookies["Token"].ToString(), EmployeeId, Date);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return PartialView("_AdminDeleteTimesheet", objdata);
        }

        [HttpPost]
        public JsonResult GetTimesheetDataDelete(long EmployeeId, string Date)
        {
            string result = "0";
            try
            {
                result = objTimeRepo.GetTimesheetDataDelete(HttpContext.Request.Cookies["Token"].ToString(), EmployeeId, Date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        #endregion

        #region SuperAdmin View TimeSheet

        public IActionResult SuperAdminViewTimeSheet()
        {
            AdminViewTimesheet model = new AdminViewTimesheet();
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            //ViewBag.ClientList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            //ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            //ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => Convert.ToInt32(x.Value) == 3 || Convert.ToInt32(x.Value) == 4).ToList();
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetClientListByOffice")]
        public JsonResult GetClientListByOffice(string OfficeId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            return Json(data);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetEmployeeListByOffice")]
        public JsonResult GetEmployeeListByOffice(string OfficeId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            return Json(data);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetActivityListByOffice")]
        public JsonResult GetActivityListByOffice(string OfficeId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            return Json(data);
        }

        [HttpPost]
        public PartialViewResult GetSuperAdminTimesheetView(string FromDate, string ToDate, long ClientId, long EmployeeId, int ActivityId, int SubActivityId, int RoleId, int OfficeId)
        {
            AdminTimeSheetOfUsers objdata = new AdminTimeSheetOfUsers();
            try
            {
                string Token = HttpContext.Request.Cookies["Token"].ToString();
                //int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdata = objTimeRepo.GetAdminTimesheetView(Token, OfficeId, FromDate, ToDate, ClientId, EmployeeId, ActivityId, SubActivityId, RoleId);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return PartialView("_ViewAdminTimesheetData", objdata);
        }
        #endregion

        #region Apply Leave
        public IActionResult LeaveRequest()
        {
            Leave_Model model = new Leave_Model();
            List<LeaveData> objdatalist = new List<LeaveData>();
            objdatalist = objTimeRepo.GetAllLeaveApplyDetails(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.LeaveDataList = objdatalist;
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("LeaveValidation")]
        public JsonResult LeaveValidation(string Date)
        {
            string result = "0";
            try
            {
                result = objTimeRepo.ApplyLeaveValidation(HttpContext.Request.Cookies["Token"].ToString(), Date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetHoldayName")]
        public JsonResult GetHoldayName(string Date)
        {
            string result = "0";
            try
            {
                result = objTimeRepo.GetHoldayName(HttpContext.Request.Cookies["Token"].ToString(), Date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        public JsonResult GetLeaveCount(string FromDate, string ToDate)
        {
            int DayCount = 0;
            string FinaleResult = "";
            try
            {
                var datef = Regex.Replace(FromDate.ToString(), @"\b(?<day>\d{1,2})-(?<month>\d{1,2})-(?<year>\d{2,4})\b", "${year}-${month}-${day}");
                DateTime datefrom = Convert.ToDateTime(datef);
                var dateT = Regex.Replace(ToDate.ToString(), @"\b(?<day>\d{1,2})-(?<month>\d{1,2})-(?<year>\d{2,4})\b", "${year}-${month}-${day}");
                DateTime dateTo = Convert.ToDateTime(dateT);
                if (FromDate != "" && ToDate != "")
                {

                    TimeSpan day;
                    //var txtstart1 = date(FromDate.ToString());
                    //var txtstart2 = date(ToDate.ToString());
                    var txtstart1 = FromDate.ToString();
                    var txtstart2 = ToDate.ToString();
                    day = (DateTime.Parse(txtstart2).Date) - (DateTime.Parse(txtstart1).Date);
                    if (day != null)
                    {
                        DayCount = Convert.ToInt32(day.TotalDays + 1);
                        if (DayCount <= 0)
                        {
                            FinaleResult = "0";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(DayCount);
        }

        public String date(object e)
        {
            string inputFormat = "dd-MM-yyyy";
            string outputFormat = "yyyy-MM-dd";
            var dateTime = DateTime.ParseExact(e.ToString(), inputFormat, CultureInfo.InvariantCulture);
            string output = dateTime.ToString(outputFormat);
            return output;

        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveLeaveRequest")]
        public JsonResult SaveLeaveRequest(string FromDate, string ToDate, string Remarks, string DayCount, long UpdateID)
        {
            Leave_Model model = new Leave_Model();
            string result = "";
            try
            {
                model.UpdateID = UpdateID;
                model.FromDate = FromDate;
                model.ToDate = ToDate;
                model.Remarks = Remarks;
                model.NoOfDays = DayCount;
                model.IPAddress = GetIP();
                model.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                result = objTimeRepo.SaveLeaveRequest(model, HttpContext.Request.Cookies["Token"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("EditLeaveDetails")]
        public JsonResult EditLeaveDetails(long ID)
        {
            List<LeaveData> objdatalist = new List<LeaveData>();
            LeaveData modal = new LeaveData();
            //string result = "";
            try
            {
                objdatalist = objTimeRepo.GetLeaveDetailsForEdit(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ID);
                modal.LId = objdatalist[0].LId;
                modal.FromDate = Convert.ToDateTime(objdatalist[0].FromDate).ToString("yyyy-MM-dd");
                modal.ToDate = Convert.ToDateTime(objdatalist[0].ToDate).ToString("yyyy-MM-dd");
                modal.Remarks = objdatalist[0].Remarks;
                modal.DayCount = objdatalist[0].DayCount;
                modal.UserId = objdatalist[0].UserId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("DeleteLeaveRequestByID")]
        public JsonResult DeleteLeaveRequestByID(long ID)
        {
            string result = "";
            try
            {
                result = objTimeRepo.DeleteLeaveRequestByID(HttpContext.Request.Cookies["Token"].ToString(), ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        #endregion

        #region Mark Leave
        public IActionResult MarkLeave()
        {
            Leave_Model model = new Leave_Model();
            List<LeaveData> objdatalist = new List<LeaveData>();
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            objdatalist = objTimeRepo.GetAllMarkLeaveDetails(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.LeaveDataList = objdatalist;
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveMarkLeave")]
        public JsonResult SaveMarkLeave(string FromDate, string ToDate, string Remarks, string DayCount, long UserId)
        {
            Leave_Model model = new Leave_Model();
            string result = "";
            try
            {
                model.UserId = UserId;
                model.FromDate = FromDate;
                model.ToDate = ToDate;
                model.Remarks = Remarks;
                model.NoOfDays = DayCount;
                model.IPAddress = GetIP();
                model.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                result = objTimeRepo.SaveMarkLeave(model, HttpContext.Request.Cookies["Token"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        #endregion

        #region Verify Leave

        public IActionResult LeaveVerify()
        {
            Leave_Model model = new Leave_Model();
            List<LeaveData> objdatalist = new List<LeaveData>();
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            objdatalist = objTimeRepo.GetAllLeaveToVerifyDetails(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.LeaveDataList = objdatalist;
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("ApproveLeaveRequest")]
        public JsonResult ApproveLeaveRequest(long LId)
        {
            string result = "";
            try
            {
                result = objTimeRepo.ApproveLeaveRequest(HttpContext.Request.Cookies["Token"].ToString(), LId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("RejectLeaveRequest")]
        public JsonResult RejectLeaveRequest(long LId, string Remarks)
        {
            string result = "";
            try
            {
                result = objTimeRepo.RejectLeaveRequest(HttpContext.Request.Cookies["Token"].ToString(), LId, Remarks);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }

        #endregion

        #region HR Report Sheet

        public IActionResult HRSheet()
        {
            Leave_Model model = new Leave_Model();
            ViewBag.ClientList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetHrSheet")]
        public JsonResult GetHrSheet(int ClientId, string FromDate, string ToDate)
        {
            string result = "";
            try
            {
                // FromDate = Convert.ToString(DateTime.Now);
                result = objTimeRepo.GetHrSheet(HttpContext.Request.Cookies["Token"].ToString(), ClientId, FromDate, ToDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(result);
        }
        #endregion

        #region HR Report Sheet
        public IActionResult HRSheetofficewiseChart()
        {
            Leave_Model model = new Leave_Model();
            return View(model);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetHRsheetGraphEmployeewise")]
        public JsonResult GetHRsheetGraphEmployeewise(int ClientId, string FromDate, string ToDate, int OfficeId)
        {
            MISReport_Model objdata = new MISReport_Model();
            graphdata datagraph = new graphdata();
            string result = "";
            int Ofc = 0;
            try
            {
                if (Convert.ToInt32(HttpContext.Request.Cookies["URole"].ToString()) == 1)
                {
                    Ofc = OfficeId;
                }
                else
                {
                    Ofc = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                }
                objdata = objTimeRepo.GetHRsheetGraphEmployeewise(HttpContext.Request.Cookies["Token"].ToString(), Ofc, ClientId, FromDate, ToDate);
                if (objdata.ReportGridData != null)
                {
                    result += " var chart = new CanvasJS.Chart('chartContainer', { theme: 'light1', title: {  text: 'Employee Wise MIS Report', },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Hours', legendText: '{ indexLabel}', dataPoints: [ ";
                    for (int i = 0; i < objdata.ReportGridData.Count; i++)
                    {
                        string newst = Convert.ToString(objdata.ReportGridData[i].Hours.ToString()).Replace(":", ".");
                        result += " { y: " + newst.ToString() + ", indexLabel: '" + objdata.ReportGridData[i].Name.ToString() + "' },";
                    }
                    result += " ] } ] }); chart.render(); ";
                }

                List<SelectListItem> datalist = new List<SelectListItem>();

                if (objdata.ReportGridData != null)
                {
                    datalist = (from item in objdata.ReportGridData.AsEnumerable()
                                select new SelectListItem
                                {
                                    Value = Convert.ToString(item.Hours),
                                    Text = Convert.ToString(item.Name),
                                }).ToList();
                }


                //result += " var e = AmCharts.makeChart('chart_7', {";
                //result += "  type: 'pie',";
                //result += " theme: 'light',";
                //result += " fontFamily: 'Open Sans',";
                //result += " color: '#888',";
                //result += " dataProvider: [";
                //if (objdata.ReportGridData != null)
                //{
                //    for (int i = 0; i < objdata.ReportGridData.Count; i++)
                //    {
                //        string time = objdata.ReportGridData[i].Hours.ToString();
                //        time = time.Replace(":", ".");
                //        string clientname = objdata.ReportGridData[i].Name.ToString();
                //        clientname = clientname.Replace("'", "");
                //        result += " {country: '" + clientname + " (" + objdata.ReportGridData[i].Hours.ToString() + ')' + "',value: " + time + "},";
                //    }
                //}
                //else
                //{
                //    result += " {country: 'No Data',value: 0.0},";
                //}



                //result += " ],";
                //result += "valueField: 'value',";
                //result += "titleField: 'country',";
                //result += "outlineAlpha: .4,";
                //result += " depth3D: 15,";
                //result += " balloonText: '[[title]]<br><span style=font-size:14px><b>[[value]]</b> ([[percents]]%)</span>',";
                //result += " angle: 30,";
                //result += " exportConfig: {";
                //result += "   menuItems: [{";
                //result += "    icon: 'export.png',";
                //result += "   format: 'png'";
                //result += " }]";
                //result += " }";
                //result += "  });";
                //result += "jQuery('.chart_7_chart_input').off().on('input change', function () {";
                //result += "  var t = jQuery(this).data('property'),";
                //result += "    a = e,";
                //result += "  l = Number(this.value);";
                //result += " e.startDuration = 0, 'innerRadius' == t && (l += '%'), a[t] = l, e.validateNow()";
                //result += " }), $('#chart_7').closest('.portlet').find('.fullscreen').click(function () {";
                //result += "   e.invalidateSize()";
                //result += " })";

                datagraph.graphjs = result;

                if (objdata.Totaltime != null && objdata.Totaltime != "")
                {
                    datagraph.totaltime = objdata.Totaltime.ToString();
                }
                datagraph.Graphdetails = datalist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(datagraph);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetHRsheetGraphClientwise")]
        public JsonResult GetHRsheetGraphClientwise(int ClientId, string FromDate, string ToDate, int OfficeId)
        {
            MISReport_Model objdata = new MISReport_Model();
            graphdata datagraph = new graphdata();
            string result = "";
            int Ofc = 0;
            try
            {
                if (Convert.ToInt32(HttpContext.Request.Cookies["URole"].ToString()) == 1)
                {
                    Ofc = OfficeId;
                }
                else
                {
                    Ofc = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                }
                objdata = objTimeRepo.GetHRsheetGraphClientwise(HttpContext.Request.Cookies["Token"].ToString(), Ofc, ClientId, FromDate, ToDate);
                if (objdata.ReportGridData != null)
                {
                    result += " var chart = new CanvasJS.Chart('chartContainer1', { theme: 'light1', title: {  text: 'Client Wise MIS Report', },  data: [ { type: 'pie', showInLegend: true, toolTipContent: '{ y} - #percent %', yValueFormatString: '#0.## Hours', legendText: '{ indexLabel}', dataPoints: [ ";
                    for (int i = 0; i < objdata.ReportGridData.Count; i++)
                    {
                        string newst = Convert.ToString(objdata.ReportGridData[i].Hours.ToString()).Replace(":", ".");
                        result += " { y: " + newst.ToString() + ", indexLabel: '" + objdata.ReportGridData[i].Name.ToString() + "' },";
                    }
                    result += " ] } ] }); chart.render(); ";
                }

                List<SelectListItem> datalist = new List<SelectListItem>();

                if (objdata.ReportGridData != null)
                {
                    datalist = (from item in objdata.ReportGridData.AsEnumerable()
                                select new SelectListItem
                                {
                                    Value = Convert.ToString(item.Hours),
                                    Text = Convert.ToString(item.Name),
                                }).ToList();
                }

                datagraph.graphjs = result;

                if (objdata.Totaltime != null && objdata.Totaltime != "")
                {
                    datagraph.totaltime = objdata.Totaltime.ToString();
                }
                datagraph.Graphdetails = datalist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(datagraph);
        }
        #endregion
    }
}