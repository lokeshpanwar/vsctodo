﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_APP.Controllers
{
    public class MastersController : BaseController
    {

        DropDown_Repository objDrop = new DropDown_Repository();
        Master_Repository objMaster = new Master_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;

        public MastersController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }


        #region Common Fn
        public IActionResult Index()
        {
            return View();
        }

        private string GetSortOrder(string SortOrder)
        {
            if (SortOrder == null)
            {
                SortOrder = "";
            }
            if (SortOrder != "")
            {
                if (TempData["SortOrder"] != null)
                {
                    string oSortOrder = TempData["SortOrder"].ToString();
                    if (oSortOrder != null)
                    {
                        SortOrder = oSortOrder == "asc" ? "desc" : "asc";
                    }
                }
                TempData["SortOrder"] = SortOrder;
            }
            else
            {
                if (TempData["SortOrder"] != null)
                {
                    SortOrder = TempData["SortOrder"].ToString();
                    TempData["SortOrder"] = SortOrder;
                }
                else
                {
                    SortOrder = "asc";
                    TempData["SortOrder"] = SortOrder;
                }
            }

            return SortOrder;
        }
        private string GetSortCol(string SortCol)
        {
            if (SortCol == null)
            {
                SortCol = "";
            }
            if (SortCol != "")
            {
                ViewData["SortCol"] = SortCol;
            }
            else
            {
                if (ViewData["SortCol"] != null)
                {
                    SortCol = TempData["SortCol"].ToString();
                    ViewData["SortCol"] = SortCol;
                }
            }

            return SortCol;
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetCityList")]
        public JsonResult GetCityList(int StateId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetCityListByStateId(StateId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }


        [HttpPost]
        [SessionAuthorizeFilter("BindUsersByRoles")]
        public JsonResult BindUsersByRoles(int RoleId)
        {
            DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            data = objDrop.GetUserListByRoleId(RoleId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }
        #endregion

        #region Users
        public IActionResult AddUser()
        {
            var webRoot = _hostingEnvironment.WebRootPath;
            DropDownReturn_Model_API ddmodel = new DropDownReturn_Model_API();
            Users modal = new Users();
            if (Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()) == 0)
            {
                ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value == "2");
                ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            }
            else
            {
                int officeid = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value != "2");
                ViewBag.OfficeList = objDrop.GetOfficeListActive(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => Convert.ToInt32(x.Value) == officeid);
                modal.OfficeId = officeid;
            }
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewUser")]
        public async Task<IActionResult> NewUser(Users objModel, List<IFormFile> filename1)
        {
            Users model = new Users();

            var webRoot = _hostingEnvironment.WebRootPath;
            var PathWithFolderName = System.IO.Path.Combine(webRoot, "Content/Images/UserImage/Img");
            

            try
            {
                if (ModelState.IsValid)
                {

                    foreach (var formFile in filename1)
                    {
                        long iFileSize = formFile.Length;


                        if (iFileSize > 2000000)  // 200KB
                        {
                            // File exceeds the file maximum size
                            @ViewBag.Message = "Exceeded maximum size of the image!";
                            @ViewBag.HideClass = "alert alert-danger";
                            return View(objModel);
                        }

                        if (!Directory.Exists(PathWithFolderName))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(PathWithFolderName);
                        }
                                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(formFile.FileName);
                                var filePath = Path.Combine(PathWithFolderName, fileName);
                                using (var fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    await formFile.CopyToAsync(fileStream);
                                }

                                objModel.fileName = fileName;
                    }

                    string result = objMaster.AddNewUser(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "User Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    if (Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()) == 0)
                    {
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value == "2");
                        ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
                    }
                    else
                    {
                        int officeid = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value != "2");
                        ViewBag.OfficeList = objDrop.GetOfficeListActive(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => Convert.ToInt32(x.Value) == officeid);
                        model.OfficeId = officeid;
                    }
                    @ViewBag.Message = "Please Check All Fields....!";
                    @ViewBag.HideClass = "alert alert-danger";
                    return View("AddUser", model);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewUser");
            }
            return View("AddUser", model);
        }

        public IActionResult ManageUsers(Common_Filter_Model fltrModel)
        {
            List<Users> objdatalist = new List<Users>();
            ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.OfficeList = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            objdatalist = objMaster.GetUserList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            if (fltrModel.OfficeId > 0)
            {
                objdatalist = objdatalist.Where(x => x.OfficeId == fltrModel.OfficeId).ToList();
            }
            if (fltrModel.RoleId > 0)
            {
                objdatalist = objdatalist.Where(x => x.RoleId == fltrModel.RoleId).ToList();
            }
            if (fltrModel.UserId > 0)
            {
                objdatalist = objdatalist.Where(x => x.UserId == fltrModel.UserId).ToList();
            }
            ViewBag.UserList = objdatalist;
            return View(fltrModel);
        }

        public IActionResult EditUser(long UserId)
        {
            List<Users> modal = new List<Users>();
            Users modal1 = new Users();
            //modal = objMaster.GetUserList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).Where(x => x.UserId == UserId).FirstOrDefault();
            modal = objMaster.GetUserDataForEdit(HttpContext.Request.Cookies["Token"].ToString(), UserId);
            ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            if (modal != null)
            {
                modal1.UserId = UserId;
                modal1.RoleId = modal[0].RoleId;
                modal1.UserName = modal[0].UserName;
                modal1.Password = modal[0].Password;
                modal1.Name = modal[0].Name;
                modal1.FatherName = modal[0].FatherName;
                modal1.MotherName = modal[0].MotherName;
                modal1.DOB = Convert.ToDateTime(modal[0].DOB).ToString("yyyy-MM-dd");
                modal1.DOJ = Convert.ToDateTime(modal[0].DOJ).ToString("yyyy-MM-dd");
                modal1.EmailId = modal[0].EmailId;
                modal1.MobileNo = modal[0].MobileNo;
                modal1.Address = modal[0].Address;
                modal1.StateId = modal[0].StateId;
                modal1.Town = modal[0].Town;
                modal1.EmergencyContactNo = modal[0].EmergencyContactNo;
                modal1.NameRelation = modal[0].NameRelation;
                ViewBag.CityList = objDrop.GetCityListByStateId(modal[0].StateId, HttpContext.Request.Cookies["Token"].ToString()).results;
                modal1.CityId = modal[0].CityId;

                modal1.fileName = modal[0].fileName;
                //modal1.UserImage = modal.fileName;
                ViewBag.ImagePath = modal[0].photo;
                modal1.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());

            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateUser")]
        public async Task<IActionResult> UpdateUser(Users objModel, List<IFormFile> filename1)
        {
            Common_Filter_Model fltrMode = new Common_Filter_Model();
            var webRoot = _hostingEnvironment.WebRootPath;
            var PathWithFolderName = System.IO.Path.Combine(webRoot, "Content/Images/UserImage/Img");
            try
            {
                if (Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()) > 0)
                {
                    ModelState.Remove("OfficeId");
                }
                ModelState.Remove("Password");
                if (ModelState.IsValid)
                {
                    foreach (var formFile in filename1)
                    {
                        long iFileSize = formFile.Length;


                        if (iFileSize > 2000000)  // 200KB
                        {
                            // File exceeds the file maximum size
                            @ViewBag.Message = "Exceeded maximum size of the image!";
                            @ViewBag.HideClass = "alert alert-danger";
                            return View(objModel);
                        }

                        if (!Directory.Exists(PathWithFolderName))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(PathWithFolderName);
                        }
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(formFile.FileName);
                        var filePath = Path.Combine(PathWithFolderName, fileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(fileStream);
                        }

                        objModel.fileName = fileName;
                    }

                    string result = objMaster.UpdateUser(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "User Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Users> objdatalist = new List<Users>();
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        objdatalist = objMaster.GetUserList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.UserList = objdatalist;
                    }
                    else
                    {
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateUser");
            }
            return View("ManageUsers", fltrMode);
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteUser")]
        public IActionResult DeleteUser(long UserId)
        {
            string result = objMaster.DeleteUser(UserId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "User Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Users> objdatalist = new List<Users>();
                ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                objdatalist = objMaster.GetUserList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.UserList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageUsers");
        }

        [HttpPost]
        [SessionAuthorizeFilter("CheckUserExistOrNot")]
        public JsonResult CheckUserExistOrNot(string UserName)
        {
            string result = "0";
            //DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            result = objMaster.CheckUserExistOrNot(UserName, HttpContext.Request.Cookies["Token"].ToString());
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetLatestPOSCode")]
        public JsonResult GetLatestPOSCode(int RoleId)
        {
            string result = "";
            //DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            result = objMaster.GetLatestPOSCode(RoleId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("CheckContactPersonExistOrNot")]
        public JsonResult CheckContactPersonExistOrNot(string Name, string MobileNo)
        {
            string result = "0";
            result = objMaster.CheckContactPersonExistOrNot(Name, MobileNo, HttpContext.Request.Cookies["Token"].ToString());
            return Json(result);
        }

        #endregion

        #region Group Master
        public IActionResult AddGroup()
        {
            Group modal = new Group();
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewGroup")]
        public ActionResult NewGroup(Group objModel)
        {
            Group model = new Group();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewGroup(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Group Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                @ViewBag.Message = ex.ToString();
                @ViewBag.HideClass = "alert alert-danger";
            }
            return View("AddGroup", model);
        }

        public IActionResult ManageGroups()
        {
            List<Group> objdatalist = new List<Group>();
            Group modal = new Group();
            objdatalist = objMaster.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.GroupList = objdatalist;
            return View(modal);
        }

        public IActionResult EditGroup(int GroupId)
        {
            Group modal = new Group();
            Group modal1 = new Group();
            modal = objMaster.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).Where(x => x.GroupId == GroupId).FirstOrDefault();

            if (modal != null)
            {
                modal1.GroupId = GroupId;
                modal1.GroupName = modal.GroupName;
            }

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateGroup")]
        public ActionResult UpdateGroup(Group objModel)
        {
            Common_Filter_Model fltrMode = new Common_Filter_Model();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateGroup(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Group Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Group> objdatalist = new List<Group>();
                        objdatalist = objMaster.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.GroupList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateGroup");
            }
            return View("ManageGroups");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteGroup")]
        public ActionResult DeleteGroup(int GroupId)
        {
            string result = objMaster.DeleteGroup(GroupId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Group Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Group> objdatalist = new List<Group>();
                objdatalist = objMaster.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.GroupList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageGroups");
        }

        #endregion

        #region AddCategory
        public IActionResult AddCategory()
        {
            Category model = new Category();
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("NewCategory")]
        public ActionResult NewCategory(Category objModel)
        {
            Category model = new Category();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewCategory(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Category Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewCategory");
            }
            return View("AddCategory");
        }
        public IActionResult ManageCategories()
        {
            List<Category> objdatalist = new List<Category>();
            Category modal = new Category();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.CategoryList = objdatalist;
            return View(modal);
        }

        public IActionResult EditCategory(int CategoryId)
        {
            Category modal = new Category();
            Category modal1 = new Category();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.CategoryId == CategoryId).FirstOrDefault();
            //ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            if (modal != null)
            {
                modal1.CategoryId = CategoryId;
                modal1.CategoryName = modal.CategoryName;
            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateCategory")]
        public ActionResult UpdateCategory(Category objModel)
        {
            Category model = new Category();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateCategory(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Category Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Category> objdatalist = new List<Category>();
                        objdatalist = objMaster.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.CategoryList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateCategory");
            }
            return View("ManageCategories");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteCategory")]
        public ActionResult DeleteCategory(int CategoryId)
        {
            string result = objMaster.DeleteCategory(CategoryId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Category Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Category> objdatalist = new List<Category>();
                objdatalist = objMaster.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.CategoryList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageCategories");
        }
        #endregion

        #region Product Master

        public IActionResult AddProduct()
        {
            Product modal = new Product();
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewProduct")]
        public ActionResult NewProduct(Product objModel)
        {
            Product model = new Product();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewProduct(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Product Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewProduct");
            }
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View("AddProduct");
        }

        public IActionResult ManageProducts()
        {
            List<Product> objdatalist = new List<Product>();
            Product modal = new Product();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.ProductList = objdatalist;
            return View(modal);
        }

        public IActionResult EditProduct(int ProductId)
        {
            Product modal = new Product();
            Product modal1 = new Product();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.ProductId == ProductId).FirstOrDefault();
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (modal != null)
            {
                modal1.ProductId = ProductId;
                modal1.ProductName = modal.ProductName;
                modal1.CategoryId = modal.CategoryId;
                modal1.CategoryName = modal.CategoryName;
                modal1.Rate = modal.Rate;
                modal1.InsuranceCompanyId = modal.InsuranceCompanyId;
                modal1.ODRate = modal.ODRate;
            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateProduct")]
        public ActionResult UpdateProduct(Product objModel)
        {
            Product model = new Product();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateProduct(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Product Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Product> objdatalist = new List<Product>();
                        objdatalist = objMaster.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.ProductList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateProduct");
            }
            return View("ManageProducts");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteProduct")]
        public ActionResult DeleteProduct(int ProductId)
        {
            string result = objMaster.DeleteProduct(ProductId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Product Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Product> objdatalist = new List<Product>();
                objdatalist = objMaster.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.ProductList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageProducts");
        }
        #endregion

        #region Issuing Office Master
        public IActionResult AddIssuingOffice()
        {
            IssuingOffice modal = new IssuingOffice();
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetInsuranceCompanyShortName")]
        public JsonResult GetInsuranceCompanyShortName(int CompanyId)
        {
            string result = "";
            //DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            result = objMaster.GetInsuranceCompanyShortName(CompanyId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(result);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewIssuingOffice")]
        public ActionResult NewIssuingOffice(IssuingOffice objModel)
        {
            IssuingOffice model = new IssuingOffice();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewIssuingOffice(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Issuing Office Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Fill Required Fields....!";
                    @ViewBag.HideClass = "alert alert-danger";
                    ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    return View("AddIssuingOffice");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewIssuingOffice");
            }
            return View("AddIssuingOffice");
        }

        public IActionResult ManageIssuingOffice()
        {
            List<IssuingOffice> objdatalist = new List<IssuingOffice>();
            IssuingOffice modal = new IssuingOffice();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.IssuingOfficeList = objdatalist;
            return View(modal);
        }

        public IActionResult EditIssuingOffice(int IssuingOfficeId)
        {
            IssuingOffice modal = new IssuingOffice();
            IssuingOffice modal1 = new IssuingOffice();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.IssuingOfficeId == IssuingOfficeId).FirstOrDefault();
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            if (modal != null)
            {
                modal1.IssuingOfficeId = IssuingOfficeId;
                modal1.IssuingOfficeName = modal.IssuingOfficeName;
                modal1.InsuranceCompanyId = modal.InsuranceCompanyId;
                modal1.InsuranceCompanyName = modal.InsuranceCompanyName;
                modal1.ShortName = modal.ShortName;
                modal1.ManagerName = modal.ManagerName;
                modal1.Address = modal.Address;
                modal1.City = modal.City;
                modal1.MobileNo = modal.MobileNo;
                modal1.EmailID = modal.EmailID;
                modal1.Code = modal.Code;
                modal1.OfficeId = modal.OfficeId;
                modal1.SalesManagerName = modal.SalesManagerName;
                modal1.SMContactNo = modal.SMContactNo;
            }

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateIssuingOffice")]
        public ActionResult UpdateIssuingOffice(IssuingOffice objModel)
        {
            IssuingOffice model = new IssuingOffice();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateIssuingOffice(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Issuing Office Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<IssuingOffice> objdatalist = new List<IssuingOffice>();
                        int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                        objdatalist = objMaster.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
                        ViewBag.IssuingOfficeList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Fill Required Fields....!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateIssuingOffice");
            }
            return View("ManageIssuingOffice");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteIssuingOffice")]
        public ActionResult DeleteIssuingOffice(int IssuingOfficeId)
        {
            string result = objMaster.DeleteIssuingOffice(IssuingOfficeId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Issuing Office Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<IssuingOffice> objdatalist = new List<IssuingOffice>();
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdatalist = objMaster.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
                ViewBag.IssuingOfficeList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageIssuingOffice");
        }
        #endregion

        #region ContactPersonMaster
        public IActionResult AddContactPerson()
        {
            ContactPerson modal = new ContactPerson();
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewContactPerson")]
        public ActionResult NewContactPerson(ContactPerson objModel)
        {
            //Users model = new Users();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewContactPerson(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Contact Person Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                    }
                    else
                    {
                        ViewBag.RoleList = objDrop.GetRoleList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                    @ViewBag.Message = "Please Check All Fields....!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewContactPerson");
            }
            return View("AddContactPerson");
        }

        public IActionResult ManageContactPersons()
        {
            List<ContactPerson> objdatalist = new List<ContactPerson>();
            ContactPerson modal = new ContactPerson();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.ContactPersonList = objdatalist;
            return View(modal);
        }

        public IActionResult EditContactPerson(int ContactPersonId)
        {
            ContactPerson modal = new ContactPerson();
            ContactPerson modal1 = new ContactPerson();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.ContactPersonId == ContactPersonId).FirstOrDefault();
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            if (modal != null)
            {
                modal1.ContactPersonId = modal.ContactPersonId;
                modal1.Name = modal.Name;
                modal1.FatherName = modal.FatherName;
                modal1.MobileNo = modal.MobileNo;
                modal1.EmailId = modal.EmailId;
                modal1.Address = modal.Address;
                if (modal.DOB != "")
                {
                    modal1.DOB = Convert.ToDateTime(modal.DOB).ToString("yyyy-MM-dd");
                }
                //else
                //{
                //    modal1.DOB = "1900-01-01";
                //}
                modal1.StateId = modal.StateId;
                ViewBag.CityList = objDrop.GetCityListByStateId(modal.StateId, HttpContext.Request.Cookies["Token"].ToString()).results;
                modal1.CityId = modal.CityId;
                modal1.OfficeId = modal.OfficeId;
            }

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateContactPerson")]
        public ActionResult UpdateContactPerson(ContactPerson objModel)
        {
            ContactPerson model = new ContactPerson();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.UpdateContactPerson(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Contact Person Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<ContactPerson> objdatalist = new List<ContactPerson>();
                        objdatalist = objMaster.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.ContactPersonList = objdatalist;
                    }
                    else
                    {
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateContactPerson");
            }
            return View("ManageContactPersons");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteContactPerson")]
        public ActionResult DeleteContactPerson(int ContactPersonId)
        {
            string result = objMaster.DeleteContactPerson(ContactPersonId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Contact Person Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                List<ContactPerson> objdatalist = new List<ContactPerson>();
                objdatalist = objMaster.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.ContactPersonList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageContactPersons");
        }

        #endregion

        #region TP/Surveyour
        public IActionResult AddTP_Surveyor()
        {
            TP_Surveyor modal = new TP_Surveyor();
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyListForTPSurveyor(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewTPSurveyor")]
        public ActionResult NewTPSurveyor(TP_Surveyor objModel)
        {
            //Users model = new Users();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewTPSurveyor(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "TP Surveyor Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyListForTPSurveyor(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    }
                    else
                    {
                        ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyListForTPSurveyor(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyListForTPSurveyor(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    @ViewBag.Message = "Please Check All Fields....!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewTPSurveyor");
            }
            return View("AddTP_Surveyor");
        }

        public IActionResult ManageTPSurveyor()
        {
            List<TP_Surveyor> objdatalist = new List<TP_Surveyor>();
            TP_Surveyor modal = new TP_Surveyor();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetTPSurveyorList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.TPSurveyorList = objdatalist;
            return View(modal);
        }

        public IActionResult EditTP_Surveyor(int TPId)
        {
            TP_Surveyor modal = new TP_Surveyor();
            TP_Surveyor modal1 = new TP_Surveyor();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetTPSurveyorList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.TPS_Id == TPId).FirstOrDefault();
            if (modal != null)
            {
                modal1.TPS_Id = TPId;
                modal1.Ins_Comp = modal.Ins_Comp;
                modal1.InsuranceCompanyName = modal.InsuranceCompanyName;
                modal1.TP_Name = modal.TP_Name;
                modal1.TP_ContactNo = modal.TP_ContactNo;
                modal1.TP_Address = modal.TP_Address;
                modal1.OfficeId = modal.OfficeId;
            }

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateTP_Surveyor")]
        public ActionResult UpdateTP_Surveyor(TP_Surveyor objModel)
        {
            TP_Surveyor model = new TP_Surveyor();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateTP_Surveyor(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "TP Surveyor Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<TP_Surveyor> objdatalist = new List<TP_Surveyor>();
                        objdatalist = objMaster.GetTPSurveyorList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.TPSurveyorList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateTP_Surveyor");
            }
            return View("ManageTPSurveyor");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteTP_Surveyor")]
        public ActionResult DeleteTP_Surveyor(int TPId)
        {
            string result = objMaster.DeleteTP_Surveyor(TPId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "TP Surveyor Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                List<TP_Surveyor> objdatalist = new List<TP_Surveyor>();
                objdatalist = objMaster.GetTPSurveyorList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.TPSurveyorList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageTPSurveyor");
        }
        #endregion


        #region Activity
        public IActionResult AddActivity()
        {
            Activity model = new Activity();
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("NewActivity")]
        public ActionResult NewActivity(Activity objModel)
        {
            Activity model = new Activity();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewActivity(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Activity Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewActivity");
            }
            return View("AddActivity");
        }
        public IActionResult ManageActivity()
        {
            List<Activity> objdatalist = new List<Activity>();
            Activity modal = new Activity();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetActivityList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.ActivityList = objdatalist;
            return View(modal);
        }

        public IActionResult EditActivity(int ActivityId)
        {
            Activity modal = new Activity();
            Activity modal1 = new Activity();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetActivityList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.ActivityId == ActivityId).FirstOrDefault();
            if (modal != null)
            {
                modal1.ActivityId = ActivityId;
                modal1.ActivityName = modal.ActivityName;
            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateActivity")]
        public ActionResult UpdateActivity(Activity objModel)
        {
            Activity model = new Activity();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateActivity(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Activity Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Activity> objdatalist = new List<Activity>();
                        objdatalist = objMaster.GetActivityList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.ActivityList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateActivity");
            }
            return View("ManageActivity");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteActivity")]
        public ActionResult DeleteActivity(int ActivityId)
        {
            string result = objMaster.DeleteActivity(ActivityId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Activity Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Activity> objdatalist = new List<Activity>();
                objdatalist = objMaster.GetActivityList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.ActivityList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageActivity");
        }
        #endregion

        #region Sub_Activity
        public IActionResult AddSubActivity()
        {
            SubActivity modal = new SubActivity();
            ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewSubActivity")]
        public ActionResult NewSubActivity(SubActivity objModel)
        {
            SubActivity model = new SubActivity();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewSubActivity(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "SubActivity Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewSubActivity");
            }
            ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            return View("AddSubActivity");
        }

        public IActionResult ManageSubActivity()
        {
            List<SubActivity> objdatalist = new List<SubActivity>();
            SubActivity modal = new SubActivity();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetSubActivityList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.SubActivityList = objdatalist;
            return View(modal);
        }

        public IActionResult EditSubActivity(int SubActivityId)
        {
            SubActivity modal = new SubActivity();
            SubActivity modal1 = new SubActivity();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetSubActivityList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.SubActivityId == SubActivityId).FirstOrDefault();
            ViewBag.ActivityList = objDrop.GetActivityListForDropDown(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (modal != null)
            {
                modal1.SubActivityId = SubActivityId;
                modal1.SubActivityName = modal.SubActivityName;
                modal1.ActivityId = modal.ActivityId;
                modal1.ActivityName = modal.ActivityName;
            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateSubActivity")]
        public ActionResult UpdateSubActivity(SubActivity objModel)
        {
            SubActivity model = new SubActivity();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateSubActivity(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "SubActivity Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<SubActivity> objdatalist = new List<SubActivity>();
                        objdatalist = objMaster.GetSubActivityList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.SubActivityList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateSubActivity");
            }
            return View("ManageSubActivity");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteSubActivity")]
        public ActionResult DeleteSubActivity(int SubActivityId)
        {
            string result = objMaster.DeleteSubActivity(SubActivityId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "SubActivity Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<SubActivity> objdatalist = new List<SubActivity>();
                objdatalist = objMaster.GetSubActivityList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.SubActivityList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageSubActivity");
        }
        #endregion



        #region Holiday
        public IActionResult Holiday()
        {
            Holiday model = new Holiday();
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("NewHoliday")]
        public ActionResult NewHoliday(Holiday objModel)
        {
            Holiday model = new Holiday();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objMaster.AddNewHoliday(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Holiday Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/NewHoliday");
            }
            return View("Holiday");
        }

        public IActionResult ManageHoliday()
        {
            List<Holiday> objdatalist = new List<Holiday>();
            Holiday modal = new Holiday();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objMaster.GetHolidayList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            ViewBag.HolidayList = objdatalist;
            return View(modal);
        }

        public IActionResult EditHoliday(int HolidayId)
        {
            Holiday modal = new Holiday();
            Holiday modal1 = new Holiday();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objMaster.GetHolidayList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.HolidayId == HolidayId).FirstOrDefault();
            if (modal != null)
            {
                modal1.HolidayId = HolidayId;
                modal1.HolidayName = modal.HolidayName;
                modal1.Date = Convert.ToDateTime(modal.Date).ToString("yyyy-MM-dd"); ;
            }
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateHoliday")]
        public ActionResult UpdateHoliday(Holiday objModel)
        {
            Holiday model = new Holiday();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objMaster.UpdateHoliday(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Holiday Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Holiday> objdatalist = new List<Holiday>();
                        objdatalist = objMaster.GetHolidayList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.HolidayList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateHoliday");
            }
            return View("ManageHoliday");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteHoliday")]
        public ActionResult DeleteHoliday(int HolidayId)
        {
            string result = objMaster.DeleteHoliday(HolidayId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Holiday Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Holiday> objdatalist = new List<Holiday>();
                objdatalist = objMaster.GetHolidayList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.HolidayList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageHoliday");
        }
        #endregion Holiday
    }
}