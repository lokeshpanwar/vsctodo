﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_APP.Controllers
{
    public class ServerLogController : BaseController
    {
        ServerLog_Repository objRepo = new ServerLog_Repository();



        #region ListServerLog (GET)

        [HttpGet]
        [SessionAuthorizeFilter]
        public ActionResult ListServerLog()
        {

           
            var objModel = objRepo.GetServerLogList(HttpContext.Request.Cookies["Token"].ToString());
            return View(objModel);
        }

        #endregion
    }
}