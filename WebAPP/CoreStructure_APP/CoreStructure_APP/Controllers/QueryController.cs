﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreStructure_APP.Controllers
{
    public class QueryController : BaseController
    {
        DropDown_Repository objDrop = new DropDown_Repository();
        Query_Repository objQuery = new Query_Repository();
        Master_Repository objMaster = new Master_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        public QueryController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        #region Common Fn
        public IActionResult Index()
        {
            return View();
        }
        private string GetSortOrder(string SortOrder)
        {
            if (SortOrder == null)
            {
                SortOrder = "";
            }
            if (SortOrder != "")
            {
                if (TempData["SortOrder"] != null)
                {
                    string oSortOrder = TempData["SortOrder"].ToString();
                    if (oSortOrder != null)
                    {
                        SortOrder = oSortOrder == "asc" ? "desc" : "asc";
                    }
                }
                TempData["SortOrder"] = SortOrder;
            }
            else
            {
                if (TempData["SortOrder"] != null)
                {
                    SortOrder = TempData["SortOrder"].ToString();
                    TempData["SortOrder"] = SortOrder;
                }
                else
                {
                    SortOrder = "asc";
                    TempData["SortOrder"] = SortOrder;
                }
            }

            return SortOrder;
        }
        private string GetSortCol(string SortCol)
        {
            if (SortCol == null)
            {
                SortCol = "";
            }
            if (SortCol != "")
            {
                ViewData["SortCol"] = SortCol;
            }
            else
            {
                if (ViewData["SortCol"] != null)
                {
                    SortCol = TempData["SortCol"].ToString();
                    ViewData["SortCol"] = SortCol;
                }
            }

            return SortCol;
        }
        #endregion

        #region Query
        public IActionResult AddQuery()
        {
            Query modal = new Query();
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            //ViewBag.CityList = "";
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("NewQuery")]
        public ActionResult NewQuery(Query objModel)
        {
            Query model = new Query();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objQuery.AddNewQuery(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Query Added Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                        ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
                    ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                    @ViewBag.Message = "Please Fill All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/NewQuery");
            }
            return View("AddQuery");
        }

        public IActionResult ManageQuery(Query objMod)
        {
            string sDate = objMod.Date;
            string cName = objMod.CName;
            int sId = objMod.QStatus;
            List<Query> objdatalist = new List<Query>();
            Query modal = new Query();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
            if (sId > 0)
            {
                objdatalist = objdatalist.Where(x => x.QStatus == sId).ToList();
            }
            if (!String.IsNullOrEmpty(cName))
            {
                objdatalist = objdatalist.Where(x => x.ClientName.Trim().Contains(cName.Trim())).ToList();
            }
            if (!String.IsNullOrEmpty(sDate))
            {
                DateTime dt = DateTime.Parse(sDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.QueryDate) == dt).ToList();
            }
            ViewBag.QueryList = objdatalist;
            return View(modal);
        }

        public IActionResult EditQuery(int QId)
        {
            Query modal = new Query();
            Query modal1 = new Query();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId).Where(x => x.QId == QId).FirstOrDefault();
            ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (modal != null)
            {
                modal1.QId = QId;
                modal1.ClientName = modal.ClientName;
                modal1.ContactPersonId = modal.ContactPersonId;
                modal1.ContactPersonName = modal.ContactPersonName;
                modal1.QueryDate = Convert.ToDateTime(modal.QueryDate).ToString("yyyy-MM-dd");
                modal1.FatherName = modal.FatherName;
                modal1.MobileNo = modal.MobileNo;
                modal1.EmailId = modal.EmailId;
                modal1.Address = modal.Address;
                modal1.StateId = modal.StateId;
                ViewBag.CityList = objDrop.GetCityListByStateId(modal.StateId, HttpContext.Request.Cookies["Token"].ToString()).results;
                modal1.CityId = modal.CityId;
                modal1.CategoryId = modal.CategoryId;
                modal1.CategoryName = modal.CategoryName;
                modal1.PolicyExpiryDate = modal.PolicyExpiryDate;
                modal1.QuatationAmount = modal.QuatationAmount;
                modal1.Remarks = modal.Remarks;
                modal1.ReferencePerson = modal.ReferencePerson;
                if (!String.IsNullOrEmpty(modal.PolicyExpiryDate))
                {
                    modal1.PolicyExpiryDate = Convert.ToDateTime(modal.PolicyExpiryDate).ToString("yyyy-MM-dd");
                }
                modal1.OfficeId = modal.OfficeId;
            }

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateQuery")]
        public ActionResult UpdateQuery(Query objModel)
        {
            Query model = new Query();
            try
            {
                if (ModelState.IsValid)
                {
                    string result = objQuery.UpdateQuery(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Query Details Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Query> objdatalist = new List<Query>();
                        int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                        objdatalist = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
                        ViewBag.QueryList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Fill All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                    return View("EditQuery");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/UpdateQuery");
            }
            return View("ManageQuery");
        }

        [HttpGet]
        [SessionAuthorizeFilter("DeleteQuery")]
        public ActionResult DeleteQuery(int QId)
        {
            string result = objQuery.DeleteQuery(QId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Query Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Query> objdatalist = new List<Query>();
                objdatalist = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.QueryList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ManageQuery");
        }

        [HttpPost]
        [SessionAuthorizeFilter("CloseQuery")]
        public JsonResult CloseQuery(long QId, string Remark)
        {
            string data = "";
            data = objQuery.CloseQueryUpdate(QId, Remark, HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }

        public IActionResult ConvertQuery(long QId)
        {
            string result = objQuery.ConvertQuery(QId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Query Converted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                ModelState.Clear();
                List<Query> objdatalist = new List<Query>();
                objdatalist = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                ViewBag.QueryList = objdatalist;
            }
            else
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-danger";
            }
            return View("ManageQuery");
        }
        public IActionResult ClosedQueries(Policy_Filter filter)
        {
            List<Query> objdatalist = new List<Query>();
            Policy_Filter modal = new Policy_Filter();
            objdatalist = objQuery.GetClosedQueryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (!String.IsNullOrEmpty(filter.FromDate))
            {
                DateTime dt = DateTime.Parse(filter.FromDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.QueryDate) == dt).ToList();
            }
            if (filter.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filter.ContactPersonId).ToList();
            }
            if (filter.CategoryId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CategoryId == filter.CategoryId).ToList();
            }

            ViewBag.QueryList = objdatalist;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("PendingQueryRemarkUpdate")]
        public JsonResult PendingQueryRemarkUpdate(long QId, string Remark)
        {
            string data = "";
            data = objQuery.PendingQueryRemarkUpdate(QId, Remark, HttpContext.Request.Cookies["Token"].ToString());
            return Json(data);
        }

        public IActionResult AllQuery(Query objMod)
        {
            string sDate = objMod.ToDate;
            string cName = objMod.CName;
            int sId = objMod.QStatus;
            List<Query> objdatalist = new List<Query>();
            Query modal = new Query();
            ViewBag.EmployeeList = objDrop.GetEmployeeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objQuery.GetAllQueryListByUser(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);

            if (!String.IsNullOrEmpty(objMod.FromDate) && !String.IsNullOrEmpty(objMod.ToDate))
            {
                DateTime fDate = DateTime.Parse(objMod.FromDate);
                DateTime tDate = DateTime.Parse(objMod.ToDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.QueryDate) >= fDate && DateTime.Parse(x.QueryDate) <= tDate).ToList();
            }
            if (sId > 0)
            {
                objdatalist = objdatalist.Where(x => x.QStatus == sId).ToList();
            }
            if (!String.IsNullOrEmpty(cName))
            {
                objdatalist = objdatalist.Where(x => x.ClientName.Trim().Contains(cName.Trim())).ToList();
            }
            if (objMod.EmpId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CreatedBy == objMod.EmpId).ToList();
            }
            ViewBag.QueryList = objdatalist;
            return View(modal);
        }
        #endregion

        #region UnderWrite
        public IActionResult UnderWriteQuery(long QId)
        {
            UnderWrite modal = new UnderWrite();
            modal.QId = QId;
            modal = objQuery.GetQueryDetailsForUnderWrite(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString(), QId).FirstOrDefault();
            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveUnderWritePolicy")]
        public IActionResult SaveUnderWritePolicy(UnderWrite objModel)
        {
            UnderWrite model = new UnderWrite();
            try
            {
                if (ModelState.IsValid)
                {
                    //string filePath = "";
                    //foreach (var formFile in Request.Form.Files)
                    //{
                    //    if (formFile.Length > 0)
                    //    {
                    //        var webRoot = _hostingEnvironment.WebRootPath;
                    //        var PathWithFolderName = Path.Combine(webRoot, "querydata");

                    //        string uniqueFileName = null;
                    //        if (objModel.UploadFile != null)
                    //        {
                    //            uniqueFileName = Guid.NewGuid().ToString() + "_" + objModel.UploadFile.FileName;
                    //            filePath = Path.Combine(PathWithFolderName, uniqueFileName);
                    //            objModel.UploadFile.CopyTo(new FileStream(filePath, FileMode.Create));
                    //            objModel.fileName = uniqueFileName;
                    //            //objModel.fileExt = Path.GetExtension(objModel.UploadFile.FileName);
                    //        }
                    //    }
                    //}

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objQuery.SaveUnderWritePolicy(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Query UnderWrite Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Query> objdatalist = new List<Query>();
                        objdatalist = objQuery.GetQueryList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.QueryList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                    return View("UnderWriteQuery");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/SaveUnderWritePolicy");
            }
            return View("ManageQuery");
        }

        [HttpPost]
        [SessionAuthorizeFilter("GetCommissionRateOfProduct")]
        public JsonResult GetCommissionRateOfProduct(int ProductId)
        {
            string data = "";
            Product modal = new Product();
            modal = objMaster.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).Where(x => x.ProductId == ProductId).FirstOrDefault();
            data = Convert.ToString(modal.totalrate);
            return Json(modal);
        }
        #endregion

        #region Policy

        public IActionResult ManagePolicy(Policy_Filter filtrModel)
        {
            Policy_Filter modal = new Policy_Filter();
            List<Policy> objdatalist = new List<Policy>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);

            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;


            if (!String.IsNullOrEmpty(filtrModel.ExpiryDate))
            {
                DateTime dt = DateTime.Parse(filtrModel.ExpiryDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.ExpiryDate) == dt).ToList();
            }
            if (filtrModel.PStatus > 0)
            {
                objdatalist = objdatalist.Where(x => x.PolicyStatus == filtrModel.PStatus).ToList();
            }
            if (filtrModel.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filtrModel.ContactPersonId).ToList();
            }
            if (filtrModel.CategoryId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CategoryId == filtrModel.CategoryId).ToList();
            }
            if (filtrModel.GroupId > 0)
            {
                objdatalist = objdatalist.Where(x => x.GroupId == filtrModel.GroupId).ToList();
            }


            ViewBag.PolicyList = objdatalist;
            return View(modal);
        }

        public IActionResult GeneratePolicy(long PId)
        {
            Policy modal = new Policy();
            Policy modal1 = new Policy();
            modal = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).Where(x => x.PId == PId).FirstOrDefault();

            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            // ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            //ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.IssuingOfficeList = objDrop.GetIssuingofficeByInsco(modal.InsuranceCompanyId, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetCategorylistListbyInsCoId(modal.InsuranceCompanyId, modal.CategoryId, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            modal1.PId = modal.PId;
            modal1.QId = modal.QId;
            modal1.TypeId = modal.TypeId;
            modal1.TypeName = modal.TypeName;
            modal1.GroupId = modal.GroupId;
            modal1.GroupName = modal.GroupName;
            modal1.ContactPersonId = modal.ContactPersonId;
            modal1.ContactPersonName = modal.ContactPersonName;
            modal1.PolicyHolderId = modal.PolicyHolderId;
            modal1.PolicyHolderName = modal.PolicyHolderName;
            modal1.UnderWriteDate = Convert.ToDateTime(modal.UnderWriteDate).ToString("yyyy-MM-dd");
            modal1.CategoryId = modal.CategoryId;
            modal1.CategoryName = modal.CategoryName;
            modal1.InsuranceCompanyId = modal.InsuranceCompanyId;
            modal1.InsuranceCompanyName = modal.InsuranceCompanyName;
            modal1.IssuingOfficeId = modal.IssuingOfficeId;
            modal1.IssuingOfficeName = modal.IssuingOfficeName;
            modal1.ProductId = modal.ProductId;
            modal1.ProductName = modal.ProductName;
            modal1.SubjectInsured = modal.SubjectInsured;
            modal1.CostCenter = modal.CostCenter;
            modal1.CommissionRate = modal.CommissionRate;
            modal1.Premium = modal.Premium;
            modal1.CGST = modal.CGST;
            modal1.SGST = modal.SGST;
            modal1.PremiumPayable = modal.PremiumPayable;
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveGeneratePolicy")]
        public async Task<IActionResult> SaveGeneratePolicy(Policy objModel, List<IFormFile> filename1)
        {
            Policy model = new Policy();
            var webRoot = _hostingEnvironment.WebRootPath;
            var PathWithFolderName = System.IO.Path.Combine(webRoot, "Content/Uploadfiles");

            try
            {
                ModelState.Remove("VehicleNo");
                ModelState.Remove("OnDamage");
                ModelState.Remove("PersonalAccident");
                ModelState.Remove("ThirdParty");
                if (ModelState.IsValid)
                {
                    foreach (var formFile in filename1)
                    {
                        long iFileSize = formFile.Length;
                        if (iFileSize > 20000000)  // 200KB
                        {
                            // File exceeds the file maximum size
                            @ViewBag.Message = "Exceeded maximum size of the image!";
                            @ViewBag.HideClass = "alert alert-danger";
                            return View(objModel);
                        }
                        if (!Directory.Exists(PathWithFolderName))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(PathWithFolderName);
                        }
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(formFile.FileName);
                        var filePath = Path.Combine(PathWithFolderName, fileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(fileStream);
                        }

                        objModel.fileName = fileName;
                    }
                    
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objQuery.SaveGeneratePolicy(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Policy Generated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Policy> objdatalist = new List<Policy>();
                        int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                        objdatalist = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);

                        ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.PolicyList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                    return View("GeneratePolicy");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/SaveGeneratePolicy");
            }
            return View("ManagePolicy");
        }

        #endregion

        #region Policy Renewal

        public IActionResult RenewalPolicies(Policy_Filter filtrModel)
        {
            Policy_Filter modal = new Policy_Filter();
            List<Policy> objdatalist = new List<Policy>();
            try
            {
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                objdatalist = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);
                //objdatalist = objdatalist.Where(x => x.PolicyStatus == 2).ToList();
                objdatalist = objdatalist.Where(x => x.PolicyStatus == 2 && x.IsRenewal == 0).ToList();
                ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

                if (!String.IsNullOrEmpty(filtrModel.FromDate) && !String.IsNullOrEmpty(filtrModel.ToDate))
                {
                    DateTime fDate = DateTime.Parse(filtrModel.FromDate);
                    DateTime tDate = DateTime.Parse(filtrModel.ToDate);
                    objdatalist = objdatalist.Where(x => DateTime.Parse(x.ExpiryDate) >= fDate && DateTime.Parse(x.ExpiryDate) <= tDate).ToList();
                }
                else
                {
                    DateTime defaultFDate = DateTime.Now;
                    DateTime defaultTDate = defaultFDate.AddDays(30);
                    objdatalist = objdatalist.Where(x => DateTime.Parse(x.ExpiryDate) >= defaultFDate && DateTime.Parse(x.ExpiryDate) <= defaultTDate).ToList();
                }
                if (filtrModel.InsuranceCompanyId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.InsuranceCompanyId == filtrModel.InsuranceCompanyId).ToList();
                }
                if (filtrModel.GroupId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.GroupId == filtrModel.GroupId).ToList();
                }
                if (filtrModel.ContactPersonId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.ContactPersonId == filtrModel.ContactPersonId).ToList();
                }
                if (filtrModel.CategoryId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.CategoryId == filtrModel.CategoryId).ToList();
                }
                if (filtrModel.PolicyHolderId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.PolicyHolderId == filtrModel.PolicyHolderId).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.PolicyList = objdatalist.OrderBy(x => DateTime.Parse(x.ExpiryDate)).ToList();
            return View(modal);
        }

        public IActionResult RenewPolicy(long PId)
        {
            Policy modal = new Policy();
            Policy modal1 = new Policy();
            modal = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).Where(x => x.PId == PId).FirstOrDefault();

            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;


            modal1.PId = modal.PId;
            modal1.QId = modal.QId;
            modal1.TypeId = modal.TypeId;
            modal1.TypeName = modal.TypeName;
            modal1.GroupId = modal.GroupId;
            modal1.GroupName = modal.GroupName;
            modal1.ContactPersonId = modal.ContactPersonId;
            modal1.ContactPersonName = modal.ContactPersonName;
            modal1.PolicyHolderId = modal.PolicyHolderId;
            modal1.PolicyHolderName = modal.PolicyHolderName;
            //modal1.UnderWriteDate = modal.UnderWriteDate;
            modal1.CategoryId = modal.CategoryId;
            modal1.CategoryName = modal.CategoryName;
            //modal1.InsuranceCompanyId = modal.InsuranceCompanyId;
            //modal1.InsuranceCompanyName = modal.InsuranceCompanyName;
            //modal1.IssuingOfficeId = modal.IssuingOfficeId;
            //modal1.IssuingOfficeName = modal.IssuingOfficeName;
            modal1.ProductId = modal.ProductId;
            modal1.ProductName = modal.ProductName;
            modal1.SubjectInsured = modal.SubjectInsured;
            modal1.CostCenter = modal.CostCenter;
            modal1.CommissionRate = modal.CommissionRate;
            //modal1.Premium = modal.Premium;
            //modal1.CGST = modal.CGST;
            //modal1.SGST = modal.SGST;
            //modal1.PremiumPayable = modal.PremiumPayable;
            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveRenewPolicy")]
        public IActionResult SaveRenewPolicy(Policy objModel)
        {
            //Policy model = new Policy();
            try
            {
                ModelState.Remove("VehicleNo");
                ModelState.Remove("OnDamage");
                ModelState.Remove("PersonalAccident");
                ModelState.Remove("ThirdParty");
                ModelState.Remove("IDV");
                ModelState.Remove("PolicyDate");
                ModelState.Remove("ExpiryDate");
                ModelState.Remove("PolicyNo");
                ModelState.Remove("PreviousPolicyNo");
                if (ModelState.IsValid)
                {
                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objQuery.SaveRenewPolicy(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Policy Renew Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Policy> objdatalist = new List<Policy>();
                        objdatalist = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()));
                        ViewBag.PolicyList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/SaveRenewPolicy");
            }
            return View("ManagePolicy");
        }
        #endregion

        #region Deleted Policy
        public IActionResult DeletedPolicies(Policy_Filter filtrModel)
        {
            Policy_Filter modal = new Policy_Filter();
            List<Policy> objdatalist = new List<Policy>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objQuery.GetDeletedPolicyList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);

            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (!String.IsNullOrEmpty(filtrModel.ExpiryDate))
            {
                DateTime dt = DateTime.Parse(filtrModel.ExpiryDate);
                objdatalist = objdatalist.Where(x => DateTime.Parse(x.ExpiryDate) == dt).ToList();
            }
            if (filtrModel.PStatus > 0)
            {
                objdatalist = objdatalist.Where(x => x.PolicyStatus == filtrModel.PStatus).ToList();
            }
            if (filtrModel.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filtrModel.ContactPersonId).ToList();
            }
            if (filtrModel.CategoryId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CategoryId == filtrModel.CategoryId).ToList();
            }
            if (filtrModel.GroupId > 0)
            {
                objdatalist = objdatalist.Where(x => x.GroupId == filtrModel.GroupId).ToList();
            }
            ViewBag.PolicyList = objdatalist;
            return View(modal);
        }
        #endregion

        #region MasterPolicyData

        public IActionResult MasterPolicyData(Policy_Filter filtrModel)
        {
            Policy_Filter modal = new Policy_Filter();
            List<Policy> objdatalist = new List<Policy>();
           // int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            DateTime fDate;
            DateTime tDate;
            if (!String.IsNullOrEmpty(filtrModel.FromDate) && !String.IsNullOrEmpty(filtrModel.ToDate))
            {
                fDate = DateTime.Parse(filtrModel.FromDate);
                tDate = DateTime.Parse(filtrModel.ToDate);
                objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), filtrModel.Officeid, fDate.Date.ToString("MM/dd/yyyy"), tDate.Date.ToString("MM/dd/yyyy"));
            }
            else
            {
                if (filtrModel.InsuranceCompanyId > 0 || filtrModel.ContactPersonId > 0 || filtrModel.CategoryId > 0 || filtrModel.GroupId > 0 || filtrModel.PolicyHolderId > 0)
                {
                    objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), filtrModel.Officeid, null, null);
                }
                else
                {
                    tDate = DateTime.Now;
                    fDate = tDate.AddDays(-30);
                    objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), filtrModel.Officeid, fDate.Date.ToString("MM/dd/yyyy"), tDate.Date.ToString("MM/dd/yyyy"));
                }
            }
            // objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, fDate.Date.ToString("MM/dd/yyyy"), tDate.Date.ToString("MM/dd/yyyy"));
     
            List<SelectListItem> list = new List<SelectListItem>();
            list = objDrop.GetOfficeList(HttpContext.Request.Cookies["Token"].ToString()).results;
            int Office = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            if (Office > 0)
            {
                list = list.Where(x => Convert.ToInt32(x.Value) == Office).ToList();
                ViewBag.OfficeList = list;
                filtrModel.Officeid = Office;
            }
            else
            {
                ViewBag.OfficeList = list;
            }

            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            if (filtrModel.InsuranceCompanyId > 0)
            {
                objdatalist = objdatalist.Where(x => x.InsuranceCompanyId == filtrModel.InsuranceCompanyId).ToList();
            }
            if (filtrModel.ContactPersonId > 0)
            {
                objdatalist = objdatalist.Where(x => x.ContactPersonId == filtrModel.ContactPersonId).ToList();
            }
            if (filtrModel.CategoryId > 0)
            {
                objdatalist = objdatalist.Where(x => x.CategoryId == filtrModel.CategoryId).ToList();
            }
            if (filtrModel.GroupId > 0)
            {
                objdatalist = objdatalist.Where(x => x.GroupId == filtrModel.GroupId).ToList();
            }
            if (filtrModel.PolicyHolderId > 0)
            {
                objdatalist = objdatalist.Where(x => x.PolicyHolderId == filtrModel.PolicyHolderId).ToList();
            }
            ViewBag.PolicyList = objdatalist;
            return View(modal);
        }
        public IActionResult Excel(Policy_Filter filtrModel)
        {
            Policy_Filter modal = new Policy_Filter();
            List<Policy> objdatalist = new List<Policy>();
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                var SNo = 0;
                worksheet.Cell(currentRow, 1).Value = "Sr No.";
                worksheet.Cell(currentRow, 2).Value = "Policy Holder";
                worksheet.Cell(currentRow, 3).Value = "Contact Person";
                worksheet.Cell(currentRow, 4).Value = "Contact No";
                worksheet.Cell(currentRow, 5).Value = "Group";
                worksheet.Cell(currentRow, 6).Value = "Subject Insured";
                worksheet.Cell(currentRow, 7).Value = "Category";
                worksheet.Cell(currentRow, 8).Value = "Vehicle No";
                worksheet.Cell(currentRow, 9).Value = "Request Type";
                worksheet.Cell(currentRow, 10).Value = "Insurance Co.";
                worksheet.Cell(currentRow, 11).Value = "Policy No.";
                worksheet.Cell(currentRow, 12).Value = "Policy Date";
                worksheet.Cell(currentRow, 13).Value = "Expiry Date";
                worksheet.Cell(currentRow, 14).Value = "Premium";
                worksheet.Cell(currentRow, 15).Value = "GST";
                worksheet.Cell(currentRow, 16).Value = "Total";
                worksheet.Cell(currentRow, 17).Value = "OD Premium";
                worksheet.Cell(currentRow, 18).Value = "Brokerage";
                worksheet.Cell(currentRow, 19).Value = "Brokerage Amount";
                worksheet.Cell(currentRow, 20).Value = "TP Premium";
                worksheet.Cell(currentRow, 21).Value = "Brokerage";
                worksheet.Cell(currentRow, 22).Value = "Brokerage Amount";
                worksheet.Cell(currentRow, 23).Value = "Total Brokerage Amount";
                int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                DateTime hdfDate;
                DateTime hdtDate;
                if (!String.IsNullOrEmpty(filtrModel.hdfromdate) && !String.IsNullOrEmpty(filtrModel.hdtodate))
                {
                    hdfDate = DateTime.Parse(filtrModel.hdfromdate);
                    hdtDate = DateTime.Parse(filtrModel.hdtodate);
                    objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, hdfDate.Date.ToString("MM/dd/yyyy"), hdtDate.Date.ToString("MM/dd/yyyy"));
                }
                else
                {
                    if (filtrModel.InsuranceCompanyId > 0 || filtrModel.ContactPersonId > 0 || filtrModel.CategoryId > 0 || filtrModel.GroupId > 0 || filtrModel.PolicyHolderId > 0)
                    {
                        objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, null, null);
                    }
                    else
                    {
                        hdtDate = DateTime.Now;
                        hdfDate = hdtDate.AddDays(-30);
                        objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, hdfDate.Date.ToString("MM/dd/yyyy"), hdtDate.Date.ToString("MM/dd/yyyy"));
                    }
                }
                // objdatalist = objQuery.GetMasterPolicyDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, fDate.Date.ToString("MM/dd/yyyy"), tDate.Date.ToString("MM/dd/yyyy"));

                ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

                if (filtrModel.hdInsuranceCompanyId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.InsuranceCompanyId == filtrModel.hdInsuranceCompanyId).ToList();
                }
                if (filtrModel.hdContactPersonId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.ContactPersonId == filtrModel.hdContactPersonId).ToList();
                }
                if (filtrModel.hdCategoryId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.CategoryId == filtrModel.hdCategoryId).ToList();
                }
                if (filtrModel.hdGroupId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.GroupId == filtrModel.hdGroupId).ToList();
                }
                if (filtrModel.hdPolicyHolderId > 0)
                {
                    objdatalist = objdatalist.Where(x => x.PolicyHolderId == filtrModel.hdPolicyHolderId).ToList();
                }
                foreach (var user in objdatalist)
                {
                    currentRow++;
                    { SNo = SNo + 1; }
                    worksheet.Cell(currentRow, 1).Value = SNo;
                    worksheet.Cell(currentRow, 2).Value = user.PolicyHolderName;
                    worksheet.Cell(currentRow, 3).Value = user.ContactPersonName;
                    worksheet.Cell(currentRow, 4).Value = user.ContactNo;
                    worksheet.Cell(currentRow, 5).Value = user.GroupName;
                    worksheet.Cell(currentRow, 6).Value = user.SubjectInsured;
                    worksheet.Cell(currentRow, 7).Value = user.CategoryName;
                    worksheet.Cell(currentRow, 8).Value = user.VehicleNo;
                    if (user.IsRenewal == 0)
                    {
                        worksheet.Cell(currentRow, 9).Value = "Internal";
                    }
                    else
                    {
                        worksheet.Cell(currentRow, 9).Value = "Renewal";
                    }
                    worksheet.Cell(currentRow, 10).Value = user.InsuranceCompanyName;
                    worksheet.Cell(currentRow, 11).Value = user.PolicyNo;
                    worksheet.Cell(currentRow, 12).Value = user.PolicyDate;
                    worksheet.Cell(currentRow, 13).Value = user.ExpiryDate;
                    worksheet.Cell(currentRow, 14).Value = user.Premium;
                    worksheet.Cell(currentRow, 15).Value = user.GSTTotal;
                    worksheet.Cell(currentRow, 16).Value = user.PremiumPayable;
                    worksheet.Cell(currentRow, 17).Value = user.OnDamage;
                    //string values2 = Math.Round(Convert.ToDecimal(user.ODRate), 2).ToString(); //user.ODRate.ToString("#.##");



                    worksheet.Cell(currentRow, 18).Value = convertdecimal(user.ODRate.ToString());                 
                    worksheet.Cell(currentRow, 19).Value = user.BrokerageAmount;
                    worksheet.Cell(currentRow, 20).Value = user.ThirdParty;
                    worksheet.Cell(currentRow, 21).Value = user.ProductRate;

                    worksheet.Cell(currentRow, 22).Value = user.BrokerAmount;
                    if ((user.OnDamage != null || user.OnDamage != "") && (user.ThirdParty != null || user.ThirdParty != ""))
                    {
                        //worksheet.Cell(currentRow, 23).Value = Convert.ToDecimal(user.BrokerageAmount) + Convert.ToDecimal(user.BrokerAmount);
                        worksheet.Cell(currentRow, 23).Value = user.totalBorkerageamt;
                    }
                    else
                    {
                    }
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                    content,
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "MasterData.xlsx");
                }
            }
        }

        public string convertdecimal(string values)
        {
            string[] str = values.Split('.');
            string value1, value2;
            value1 = str[0].ToString();
            value2 = str[1].ToString();

            if (value2.Length == 0)
            {
                value2 = "00";
            }
            else if (value2.Length == 1)
            {
                value2 = value2 + "0";
            }
            return value1 + "." + value2;

        }

        public IActionResult EditPolicy(long PId)
        {
            Policy modal = new Policy();
            Policy modal1 = new Policy();
            modal = objQuery.GetPolicyDetailsForEdit(HttpContext.Request.Cookies["Token"].ToString(), PId).FirstOrDefault();

            ViewBag.TypeList = objDrop.GetPolicyTypeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.PolicyHolderList = objDrop.GetClientList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.InsuranceCompanyList = objDrop.GetInsuranceCompanyList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            // ViewBag.IssuingOfficeList = objDrop.GetIssuingOfficeList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            //ViewBag.ProductList = objDrop.GetProductList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            ViewBag.IssuingOfficeList = objDrop.GetIssuingofficeByInsco(modal.InsuranceCompanyId, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
            ViewBag.ProductList = objDrop.GetCategorylistListbyInsCoId(modal.InsuranceCompanyId, modal.CategoryId, HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;

            modal1.PId = modal.PId;
            modal1.QId = modal.QId;
            modal1.TypeId = modal.TypeId;
            modal1.TypeName = modal.TypeName;
            modal1.GroupId = modal.GroupId;
            modal1.GroupName = modal.GroupName;
            modal1.ContactPersonId = modal.ContactPersonId;
            modal1.ContactPersonName = modal.ContactPersonName;
            modal1.PolicyHolderId = modal.PolicyHolderId;
            modal1.PolicyHolderName = modal.PolicyHolderName;
            modal1.UnderWriteDate = Convert.ToDateTime(modal.UnderWriteDate).ToString("yyyy-MM-dd");
            modal1.CategoryId = modal.CategoryId;
            modal1.CategoryName = modal.CategoryName;

            modal1.VehicleNo = modal.VehicleNo;
            modal1.OnDamage = modal.OnDamage;
            modal1.PersonalAccident = modal.PersonalAccident;
            modal1.ThirdParty = modal.ThirdParty;

            modal1.InsuranceCompanyId = modal.InsuranceCompanyId;
            modal1.InsuranceCompanyName = modal.InsuranceCompanyName;
            modal1.IssuingOfficeId = modal.IssuingOfficeId;
            modal1.IssuingOfficeName = modal.IssuingOfficeName;
            modal1.ProductId = modal.ProductId;
            modal1.ProductName = modal.ProductName;
            modal1.SubjectInsured = modal.SubjectInsured;
            modal1.CostCenter = modal.CostCenter;
            modal1.CommissionRate = modal.CommissionRate;
            modal1.Premium = modal.Premium;
            modal1.CGST = modal.CGST;
            modal1.SGST = modal.SGST;
            modal1.PremiumPayable = modal.PremiumPayable;

            modal1.IDV = modal.IDV;
            modal1.PolicyDate = Convert.ToDateTime(modal.PolicyDate).ToString("yyyy-MM-dd");
            modal1.ExpiryDate = Convert.ToDateTime(modal.ExpiryDate).ToString("yyyy-MM-dd");
            modal1.PolicyNo = modal.PolicyNo;
            modal1.PreviousPolicyNo = modal.PreviousPolicyNo;
            modal1.UploadFileName = modal.UploadFileName;

            return View(modal1);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveUpdatePolicy")]
        public async Task<IActionResult> SaveUpdatePolicy(Policy objModel, List<IFormFile> filename1)
        {
            Policy model = new Policy();
            var webRoot = _hostingEnvironment.WebRootPath;
            var PathWithFolderName = System.IO.Path.Combine(webRoot, "Content/Uploadfiles");
            try
            {
                ModelState.Remove("VehicleNo");
                ModelState.Remove("OnDamage");
                ModelState.Remove("PersonalAccident");
                ModelState.Remove("ThirdParty");
                if (ModelState.IsValid)
                {
                    foreach (var formFile in filename1)
                    {
                        long iFileSize = formFile.Length;
                        if (iFileSize > 20000000)  // 200KB
                        {
                            // File exceeds the file maximum size
                            @ViewBag.Message = "Exceeded maximum size of the image!";
                            @ViewBag.HideClass = "alert alert-danger";
                            return View(objModel);
                        }
                        if (!Directory.Exists(PathWithFolderName))
                        {

                        }
                        else
                        {
                            Directory.CreateDirectory(PathWithFolderName);
                        }
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(formFile.FileName);
                        var filePath = Path.Combine(PathWithFolderName, fileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(fileStream);
                        }

                        objModel.fileName = fileName;
                    }

                    objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                    string result = objQuery.SaveUpdatePolicy(objModel, HttpContext.Request.Cookies["Token"].ToString());
                    if (result == "Policy Updated Successfully..!")
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-success";
                        ModelState.Clear();
                        List<Policy> objdatalist = new List<Policy>();
                        int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
                        objdatalist = objQuery.GetPolicyList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId);

                        ViewBag.GroupList = objDrop.GetGroupList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.ContactPersonList = objDrop.GetContactPersonList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.CategoryList = objDrop.GetCategoryList(HttpContext.Request.Cookies["Token"].ToString(), HttpContext.Request.Cookies["OfficeId"].ToString()).results;
                        ViewBag.PolicyList = objdatalist;
                    }
                    else
                    {
                        @ViewBag.Message = result;
                        @ViewBag.HideClass = "alert alert-danger";
                    }
                }
                else
                {
                    @ViewBag.Message = "Please Check All Required Fields...!";
                    @ViewBag.HideClass = "alert alert-danger";
                    return View("GeneratePolicy");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Query/SaveUpdatePolicy");
            }
            return View("ManagePolicy");
        }

        #endregion
    }
}