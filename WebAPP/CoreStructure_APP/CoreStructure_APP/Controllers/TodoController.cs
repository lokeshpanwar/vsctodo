﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreStructure_APP.Controllers
{
    public class TodoController : BaseController
    {
        ToDo_Repository objDrop = new ToDo_Repository();
        VSCCO_Repository objvscco = new VSCCO_Repository();
        VSCCC_Repository objvsccc = new VSCCC_Repository();
        PIB_Repository objpib = new PIB_Repository();
        PTS_Repository objpts = new PTS_Repository();
        Attorneys_Repository objAttorneys = new Attorneys_Repository();

        public IActionResult Index()
        {
            Task_Table model = new Task_Table();
            Task_Model objmode = new Task_Model();

            try
            {

                model = objDrop.GetTodoList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString()));
                ViewBag.TodoList = model.results.Where(x => x.StatusId == 1 || x.StatusId == 4 || x.StatusId == 0).ToList();
                ViewBag.PendingList = model.results.Where(x => x.StatusId == 2).ToList();
                ViewBag.CompleteList = model.results.Where(x => x.StatusId == 3 || x.StatusId == 5).ToList();
                objmode.Date = DateTime.Now.ToString("dd-MM-yyyy");
                ViewBag.statuslist = objDrop.GetStatuslist(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value == "2" || x.Value == "3");
                ViewBag.VerticalList = objDrop.VerticalListUserwise(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
                objmode.VerticalsList = objDrop.VerticalListUserwise(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;

                objmode.HDVertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());
            }
            catch (Exception ex)
            {


            }

            return View(objmode);
        }


        public IActionResult AddTask()
        {
            Task_Model model = new Task_Model();
            model.Date = DateTime.Now.ToString("dd-MM-yyyy");
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalListUserwise(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            ViewBag.CategoryList = objAttorneys.Category(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            //ViewBag.ClientList = objvscco.VSCCOClientlist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            //ViewBag.ActivityList = objvscco.VSCCOActivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            model.HDVertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("SaveTask")]
        public ActionResult SaveTask(Task_Model objModel)
        {
            Task_Model model = new Task_Model();
            Task_Model returnmodel = new Task_Model();
            try
            {
                //if (ModelState.IsValid)
                //{
                objModel.AssignToId = Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString());
                string result = objDrop.SaveTask(objModel, HttpContext.Request.Cookies["Token"].ToString());
                if (result == "ToDo Added Successfully..!")
                {
                    @ViewBag.Message = result;
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                if (result == "ToDo Updated Successfully..!")
                {
                    @ViewBag.Message = result;
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                else
                {
                    @ViewBag.Message = result;
                    @ViewBag.HideClass = "alert alert-danger";
                }
                //}
                //else
                //{
                //    @ViewBag.Message = "Please Check All Fields...!";
                //    @ViewBag.HideClass = "alert alert-danger";
                //}


                model.Date = DateTime.Now.ToString("dd-MM-yyyy");
                ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
                ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;



            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("ViewTask", returnmodel);
        }
        public IActionResult ViewTask()
        {
            List<Task_Model> objdatalist = new List<Task_Model>();
            Task_Model model = new Task_Model();
            int UserId = Convert.ToInt32(HttpContext.Request.Cookies["UserId"].ToString());
            objdatalist = objDrop.GetViewTaskList(HttpContext.Request.Cookies["Token"].ToString(), UserId).Where(x => x.Selfassign == 1).ToList();
            ViewBag.TaskList = objdatalist;

            return View("ViewTask", model);
        }
        public IActionResult EditTask(int TaskId)
        {
            Task_Model modal = new Task_Model();
            Task_Model modal1 = new Task_Model();
            List<Task_Model> objdatalist = new List<Task_Model>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objDrop.GetEditTaskList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), TaskId);
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalListUserwise(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            ViewBag.EmployeeList = objDrop.Employeelist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            var Verticals = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());
            if (Verticals == 1)
            {
                ViewBag.ClientList = objvscco.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
                ViewBag.ActivityList = objvscco.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
                if (objdatalist[0].Activityid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubActivityList = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), objdatalist[0].Activityid).results;
                }
            }
            else if (Verticals == 2)
            {
                ViewBag.ClientList = objvsccc.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
                ViewBag.ActivityList = objvsccc.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
                if (objdatalist[0].Activityid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubActivityList = objvsccc.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), objdatalist[0].Activityid).results;
                }
            }
            else if (Verticals == 3)
            {
                ViewBag.ClientList = objpib.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                ViewBag.ActivityList = objpib.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                if (objdatalist[0].Activityid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubActivityList = objpib.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, objdatalist[0].Activityid).results;
                }
            }
            else if (Verticals == 4)
            {
                ViewBag.ClientList = objpts.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                ViewBag.ActivityList = objpts.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                if (objdatalist[0].Activityid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubActivityList = objpts.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, objdatalist[0].Activityid).results;
                }
                ViewBag.Projectlist = objpts.Projectlist(HttpContext.Request.Cookies["Token"].ToString(), objdatalist[0].Clientid).results;
                ViewBag.Modulelist = objpts.Projectmodules(HttpContext.Request.Cookies["Token"].ToString(), objdatalist[0].Projectid).results;
            }
            else if (Verticals == 5)
            {
                ViewBag.ClientList = objAttorneys.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                ViewBag.ActivityList = objAttorneys.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
                if (objdatalist[0].Activityid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubActivityList = objAttorneys.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, objdatalist[0].Activityid).results;
                }
                ViewBag.Projectlist = objAttorneys.Projectname(HttpContext.Request.Cookies["Token"].ToString(), objdatalist[0].Clientid).results;
                ViewBag.CategoryList = objAttorneys.Category(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
                if (objdatalist[0].Categoryid != 0 || objdatalist[0] != null)
                {
                    ViewBag.SubcategoryList = objAttorneys.SubCategory(HttpContext.Request.Cookies["Token"].ToString(), 1, objdatalist[0].Categoryid).results;
                }
            }
            if (modal != null)
            {
                modal1.Id = objdatalist[0].Id;
                modal1.Date = objdatalist[0].Date;
                modal1.Title = objdatalist[0].Title;
                modal1.Description = objdatalist[0].Description;
                modal1.PriorityId = objdatalist[0].PriorityId;
                modal1.AssignToId = objdatalist[0].AssignToId;
                modal1.StartDate = objdatalist[0].StartDate;
                modal1.EndDate = objdatalist[0].EndDate;
                modal1.FrequencyId = objdatalist[0].FrequencyId;
                modal1.Verticals = objdatalist[0].Verticals;
                modal1.Clientid = objdatalist[0].Clientid;
                modal1.Activityid = objdatalist[0].Activityid;
                modal1.Subactivityid = objdatalist[0].Subactivityid;
                modal1.Projectid = objdatalist[0].Projectid;
                modal1.Moduleid = objdatalist[0].Moduleid;
                modal1.Categoryid = objdatalist[0].Categoryid;
                modal1.Subcategoryid = objdatalist[0].Subcategoryid;
            }
            modal1.DataList = objdatalist;
            modal1.gridcount = objdatalist.Count();

            return View(modal1);
        }
        //[HttpPost]
        //[SessionAuthorizeFilter("UpdateTask")]
        //public ActionResult UpdateTask(Task_Model objModel)
        //{
        //    Task_Model model = new Task_Model();
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            //objModel.OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
        //            string result = objDrop.UpdateTask(objModel, HttpContext.Request.Cookies["Token"].ToString());
        //            if (result == "Task Details Updated Successfully..!")
        //            {
        //                @ViewBag.Message = result;
        //                @ViewBag.HideClass = "alert alert-success";
        //                ModelState.Clear();
        //                List<Task_Model> objdatalist = new List<Task_Model>();
        //                int UserId = Convert.ToInt32(HttpContext.Request.Cookies["UserId"].ToString());
        //                objdatalist = objDrop.GetViewTaskList(HttpContext.Request.Cookies["Token"].ToString(), UserId);
        //                ViewBag.TaskList = objdatalist;
        //            }
        //            else
        //            {
        //                //ViewBag.StateList = objDrop.GetStateList(HttpContext.Request.Cookies["Token"].ToString()).results;
        //                @ViewBag.Message = result;
        //                @ViewBag.HideClass = "alert alert-danger";
        //            }
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.LogError(ex, HttpContext.Request.Cookies["Token"].ToString(), "WebAPP", "Masters/UpdateContactPerson");
        //    }
        //    return View("ViewTask");
        //}
        [HttpGet]
        [SessionAuthorizeFilter("DeleteTask")]
        public ActionResult DeleteTask(int @TaskId)
        {
            string result = objDrop.DeleteTask(@TaskId, HttpContext.Request.Cookies["Token"].ToString());
            if (result == "Task Deleted Successfully..!")
            {
                @ViewBag.Message = result;
                @ViewBag.HideClass = "alert alert-success";
                List<Task_Model> objdatalist = new List<Task_Model>();
                int UserId = Convert.ToInt32(HttpContext.Request.Cookies["UserId"].ToString());
                objdatalist = objDrop.GetViewTaskList(HttpContext.Request.Cookies["Token"].ToString(), UserId);
                ViewBag.TaskList = objdatalist;
            }
            else
            {
                TempData["EmployeeData"] = result;
                TempData["EmployeeClass"] = "alert alert-danger";
            }
            return View("ViewTask");
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetClientlist")]
        public JsonResult GetClientlist(int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            if (Verticals == 1)
            {

                data = objvscco.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;

            }
            else if (Verticals == 2)
            {
                data = objvsccc.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            }
            else if (Verticals == 3)
            {
                data = objpib.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 4)
            {
                data = objpts.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.Clientlist(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }

            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetActivitylist")]
        public JsonResult GetActivitylist(int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            if (Verticals == 1)
            {
                data = objvscco.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            }
            else if (Verticals == 2)
            {
                data = objvsccc.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            }
            else if (Verticals == 3)
            {
                data = objpib.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 4)
            {
                data = objpts.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.Category(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }

            return Json(data);
        }



        [HttpPost]
        [SessionAuthorizeFilter("GetSubActivityList")]
        public JsonResult GetSubActivityList(int ActivityId, int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();
            //data = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), 8, ActivityId).results;

            if (Verticals == 1)
            {
                data = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ActivityId).results;
            }
            else if (Verticals == 2)
            {
                data = objvsccc.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ActivityId).results;
            }
            else if (Verticals == 3)
            {
                data = objpib.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }
            else if (Verticals == 4)
            {
                data = objpts.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.SubCategory(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }

            return Json(data);
        }


        [HttpPost]
        [SessionAuthorizeFilter("GetActivitylistAttorny")]
        public JsonResult GetActivitylistAttorny(int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            if (Verticals == 1)
            {
                data = objvscco.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            }
            else if (Verticals == 2)
            {
                data = objvsccc.Activity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString())).results;
            }
            else if (Verticals == 3)
            {
                data = objpib.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 4)
            {
                data = objpts.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.Activity(HttpContext.Request.Cookies["Token"].ToString(), 1).results;
            }

            return Json(data);
        }


        [HttpPost]
        [SessionAuthorizeFilter("GetSubActivityListAttorny")]
        public JsonResult GetSubActivityListAttorny(int ActivityId, int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();
            //data = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), 8, ActivityId).results;

            if (Verticals == 1)
            {
                data = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ActivityId).results;
            }
            else if (Verticals == 2)
            {
                data = objvsccc.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString()), ActivityId).results;
            }
            else if (Verticals == 3)
            {
                data = objpib.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }
            else if (Verticals == 4)
            {
                data = objpts.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.SubActivity(HttpContext.Request.Cookies["Token"].ToString(), 1, ActivityId).results;
            }

            return Json(data);
        }




        [HttpPost]
        [SessionAuthorizeFilter("GetSubCategoryList")]
        public JsonResult GetSubCategoryList(int Categoryid, int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();
            //data = objvscco.SubACtivity(HttpContext.Request.Cookies["Token"].ToString(), 8, ActivityId).results;

            if (Verticals == 5)
            {
                data = objAttorneys.SubCategory(HttpContext.Request.Cookies["Token"].ToString(), 1, Categoryid).results;
            }
            return Json(data);
        }

        public ActionResult TaskDetails(Task_Model model, int Id)
        {
            //Task_Model model = new Task_Model();
            Task_Table objmodal = new Task_Table();
            if (Id == 0)
            {
                Id = model.Todoid;
            }
            objmodal = objDrop.GetTodoDetailList(HttpContext.Request.Cookies["Token"].ToString(), Id);
            ViewBag.TodoDetailList = objmodal.results;
            //model.Todoid = Id;
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetAlltaskdetails")]
        public JsonResult GetAlltaskdetails(int todoid)
        {
            Task_Table modal = new Task_Table();

            modal = objDrop.GetAlltaskdetails(HttpContext.Request.Cookies["Token"].ToString(), todoid);

            return Json(modal.results);
        }
        [HttpPost]
        [SessionAuthorizeFilter("Savetodostatus")]
        public JsonResult Savetodostatus(int todoid, string date, string Time, int StatusId, string Description, int ActivityId, int SubActivity)
        {
            string resulttimesheet = "";
            List<Task_Model> result = new List<Task_Model>();
            string token = Convert.ToString(HttpContext.Request.Cookies["Token"].ToString());
            int empid = Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString());
            int vertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());

            result = objDrop.Updatetodo(todoid, date, Time, StatusId, Description, empid, token).results;

            if (vertical == 1)
            {

                resulttimesheet = objvscco.filltimesheet(HttpContext.Request.Cookies["Token"].ToString(), empid, date, Time, Description, result[0].Clientid, result[0].Activityid, result[0].Subactivityid);

            }
            else if (vertical == 2)
            {
                resulttimesheet = objvsccc.filltimesheet(HttpContext.Request.Cookies["Token"].ToString(), empid, date, Time, Description, result[0].Clientid, result[0].Activityid, result[0].Subactivityid);
            }
            else if (vertical == 3)
            {
                resulttimesheet = objpib.filltimesheet(HttpContext.Request.Cookies["Token"].ToString(), empid, date, Time, Description, result[0].Clientid, result[0].Activityid, result[0].Subactivityid);
            }
            else if (vertical == 4)
            {
                resulttimesheet = objpts.filltimesheet(HttpContext.Request.Cookies["Token"].ToString(), empid, date, Time, Description, result[0].Clientid, result[0].Activityid, result[0].Subactivityid, result[0].Projectid, result[0].Moduleid);
            }
            else if (vertical == 5)
            {
                resulttimesheet = objAttorneys.filltimesheet(HttpContext.Request.Cookies["Token"].ToString(), empid, date, Time, Description, result[0].Clientid, ActivityId, SubActivity, result[0].Projectid, todoid, StatusId);
            }

            return Json(result);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetProject")]
        public JsonResult GetProject(int Clientid, int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            if (Verticals == 4)
            {
                data = objpts.Projectlist(HttpContext.Request.Cookies["Token"].ToString(), Clientid).results;
            }
            else if (Verticals == 5)
            {
                data = objAttorneys.Projectname(HttpContext.Request.Cookies["Token"].ToString(), Clientid).results;
            }

            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetProjectModules")]
        public JsonResult GetProjectModules(int Projectid, int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();

            if (Verticals == 4)
            {
                data = objpts.Projectmodules(HttpContext.Request.Cookies["Token"].ToString(), Projectid).results;
            }


            return Json(data);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetEmployeelist")]
        public JsonResult GetEmployeelist(int Verticals)
        {
            List<SelectListItem> data = new List<SelectListItem>();
            data = objDrop.Employeelist(HttpContext.Request.Cookies["Token"].ToString(), Verticals).results;
            return Json(data);
        }
        public IActionResult AssignTask()
        {
            Task_Model model = new Task_Model();
            model.Date = DateTime.Now.ToString("dd-MM-yyyy");
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            model.HDVertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());
            return View(model);
        }
        [HttpPost]
        [SessionAuthorizeFilter("GetTotalTasks")]
        public JsonResult GetTotalTasks(int Verticals, int EmpId)
        {
            string result = "";
            //DropDownReturn_Model_API data = new DropDownReturn_Model_API();
            result = objDrop.GetTotalTasks(Verticals, EmpId, HttpContext.Request.Cookies["Token"].ToString());
            return Json(result);
        }
        [HttpPost]
        [SessionAuthorizeFilter("SaveAssignTask")]
        public ActionResult SaveAssignTask(Task_Model objModel)
        {
            Task_Model model = new Task_Model();
            Task_Model returnmodel = new Task_Model();
            VSCUserData objmodel = new VSCUserData();
            VSCCO_Repository objVSCCOrepo = new VSCCO_Repository();
            VSCCC_Repository objVSCCCrepo = new VSCCC_Repository();
            PIB_Repository objPIBrepo = new PIB_Repository();
            PTS_Repository objPTSrepo = new PTS_Repository();
            Attorneys_Repository objAttrepo = new Attorneys_Repository();
            string DetailResult = string.Empty;
            int loop = objModel.RowCount;
            try
            {

                string result = objDrop.SaveAssignTask(objModel, HttpContext.Request.Cookies["Token"].ToString());

                if (Convert.ToInt32(result) > 0)
                {
                    for (int i = 0; i < loop; i++)
                    {
                        model.Todoid = Convert.ToInt32(result);
                        model.AssignTo = HttpContext.Request.Form["hdEmployee" + i + ""];
                        model.StartDate = HttpContext.Request.Form["StartDate" + i + ""];
                        model.EndDate = HttpContext.Request.Form["EndDate" + i];
                        model.Verticals = Convert.ToInt32(HttpContext.Request.Form["hdVertical" + i]);
                        model.Status = "1";
                        model.PriorityId = Convert.ToInt32(HttpContext.Request.Form["hdPriority" + i]);
                        model.FrequencyId = Convert.ToInt32(HttpContext.Request.Form["hdFrequency" + i]);
                        
                        DetailResult = objDrop.SaveAssignTaskDetails(model, HttpContext.Request.Cookies["Token"].ToString());

                        List<Task_Model> objList = new List<Task_Model>();

                        objList = objDrop.GetUserDetail(HttpContext.Request.Cookies["Token"].ToString(), model.AssignTo);
                        SentEmail(objList[0].EmailId, HttpContext.Request.Cookies["Name"].ToString(), objModel.Title,objModel.Description,objList[0].Name);
                    }
                }

                if (Convert.ToInt32(result) > 0)
                {
                    @ViewBag.Message = "ToDo Added Successfully..!";
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                else
                {
                    @ViewBag.Message = "Oops Somthing went wrong!";
                    @ViewBag.HideClass = "alert alert-danger";
                }

            }
            catch (Exception ex)
            {

            }

            Task_Model newmodel = new Task_Model();
            newmodel.Date = DateTime.Now.ToString("dd-MM-yyyy");
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            return RedirectToAction("AssignTask", newmodel);
        }

        public void SentEmail(string EmailId, string Name,string Title, string Description,string AssigneeName)
        {
            string body = string.Empty;
            body = body + "<!DOCTYPE html> <html lang='en'><head>  <meta charset='utf-8'> <meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>";
            body = body + "<title>Table Mail</title><style type='text/css'> .tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;} .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;} ";
            body = body + ".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#FFFF;background-color:#373a3c;}.tg .tg-vn4c{background-color:#f0f0f0}</style>";
            body = body + "<style type='text/css'> .table-area { margin: 0;  width: 100%; }.table-area table {margin: 0 auto; width: 611px !important;}";
            body = body + ".table-area .col-sm-6 {border-right: 1px solid #ddd;}.table-area .col-sm-6:last-child {border-right:0; }</style></head><body><div class='table-area'> Hello "+ AssigneeName + ", <br><br/> You received new task from " + Name + " <br/>";
            body = body + "<br><div class='row'> <div class='col-sm-12'>Task Title: "+ Title + "<br></div></div>";
            body = body + "<br><div class='row'> <div class='col-sm-12'>Task Description: " + Description + "<br></div></div>";

            body = body + "</tbody></table></div> <br><br> ";


            body = body + "</div>";
            body = body + "<b style='color:Green'>Note: This is ToDo system generated mail for your information and necessary action. Please do not reply to this mail.</b></body></html>";

            MailMessage Mail = new MailMessage();
            Mail.From = new MailAddress("todo@vscconsulting.in");
            Mail.To.Add(EmailId);
            //Mail.Bcc.Add("ankitsinghal27@gmail.com");
            Mail.Subject = "New Task is received";
            Mail.Body = body;
            Mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.UseDefaultCredentials = true;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            Mail.Priority = System.Net.Mail.MailPriority.High;
            smtp.Timeout = 20000;
            smtp.Credentials = new System.Net.NetworkCredential("todo@vscconsulting.in", "hrvNCwEdqsv!");
            smtp.Send(Mail);
        }
        [HttpPost]
        [SessionAuthorizeFilter("AcceptTask")]
        public JsonResult AcceptTask(int todoid, int Clientid, int Activityid, int Subactivityid, int projectid, int moduleid)
        {
            string resulttimesheet = "";
            List<Task_Model> result = new List<Task_Model>();
            string token = Convert.ToString(HttpContext.Request.Cookies["Token"].ToString());
            int empid = Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString());
            int vertical = Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString());
            
            result = objDrop.AcceptTask(todoid, Clientid, Activityid, Subactivityid, projectid, moduleid, vertical, token).results;


            return Json(result);
        }

        public IActionResult ViewAssignTask()
        {
            List<Task_Model> objdatalist = new List<Task_Model>();
            Task_Model model = new Task_Model();
            int UserId = Convert.ToInt32(HttpContext.Request.Cookies["UserId"].ToString());
            objdatalist = objDrop.GetViewTaskList(HttpContext.Request.Cookies["Token"].ToString(), UserId).Where(x => x.Selfassign == 0).ToList();
            ViewBag.TaskList = objdatalist;

            return View(model);
        }

        public IActionResult EditAssignTask(int TaskId)
        {
            Task_Model modal = new Task_Model();
            Task_Model modal1 = new Task_Model();
            List<Task_Model> objdatalist = new List<Task_Model>();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            objdatalist = objDrop.GetEditAssignTaskList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), TaskId);
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            ViewBag.EmployeeList = objDrop.Employeelist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;

            if (modal != null)
            {
                modal1.Id = objdatalist[0].Id;
                modal1.Date = objdatalist[0].Date;
                modal1.Title = objdatalist[0].Title;
                modal1.Description = objdatalist[0].Description;
                modal1.PriorityId = objdatalist[0].PriorityId;
                //modal1.AssignToId = objdatalist[0].AssignToId;
                //modal1.StartDate = objdatalist[0].StartDate;
                //modal1.EndDate = objdatalist[0].EndDate;
                modal1.FrequencyId = objdatalist[0].FrequencyId;
                //modal1.Verticals = objdatalist[0].Verticals;

            }
            modal1.DataList = objdatalist;
            modal1.gridcount = objdatalist.Count();

            return View(modal1);
        }


        [HttpPost]
        [SessionAuthorizeFilter("RejectTask")]
        public JsonResult RejectTask(int todoid, string RejectRemark)
        {
            string resulttimesheet = "";
            List<Task_Model> result = new List<Task_Model>();
            string token = Convert.ToString(HttpContext.Request.Cookies["Token"].ToString());

            result = objDrop.RejectTask(todoid, RejectRemark, token).results;

            return Json(result);
        }


        public IActionResult ViewTotalTaskDetails(int VerticalId, int EmpId)
        {
            List<Task_Model> objdatalist = new List<Task_Model>();
            Task_Model model = new Task_Model();
            int UserId = Convert.ToInt32(HttpContext.Request.Cookies["UserId"].ToString());
            objdatalist = objDrop.ViewTotalTaskDetails(HttpContext.Request.Cookies["Token"].ToString(), VerticalId, EmpId);
            ViewBag.TaskList = objdatalist;
            return View(model);
        }
        public IActionResult ViewAllTask()
        {
            Task_Table model = new Task_Table();
            Task_Model objmode = new Task_Model();

            try
            {

                model = objDrop.GetTodoList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString()));
                ViewBag.TodoList = model.results.Where(x => x.StatusId == 1 || x.StatusId == 4 || x.StatusId == 0).ToList();
                ViewBag.PendingList = model.results.Where(x => x.StatusId == 2).ToList();
                ViewBag.CompleteList = model.results.Where(x => x.StatusId == 3 || x.StatusId == 5).ToList();
                objmode.Date = DateTime.Now.ToString("dd-MM-yyyy");
                ViewBag.statuslist = objDrop.GetStatuslist(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value == "2" || x.Value == "3");
                ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
                ViewBag.EmployeeList = objDrop.Employeelist(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            }
            catch (Exception ex)
            {


            }

            return View(objmode);
        }
        [HttpPost]
        public IActionResult GetEmployeelistFilterWise(Task_Model model)
        {
            Task_Table objmodel = new Task_Table();
            //Task_Model objmodel = new Task_Model();

            try
            {

                objmodel = objDrop.GetTodoListFilterWise(HttpContext.Request.Cookies["Token"].ToString(), model.EmployeeId, model.Vertical);
                ViewBag.TodoList = objmodel.results.Where(x => x.StatusId == 1 || x.StatusId == 4 || x.StatusId == 0).ToList();
                ViewBag.PendingList = objmodel.results.Where(x => x.StatusId == 2).ToList();
                ViewBag.CompleteList = objmodel.results.Where(x => x.StatusId == 3 || x.StatusId == 5).ToList();
                model.Date = DateTime.Now.ToString("dd-MM-yyyy");
                ViewBag.statuslist = objDrop.GetStatuslist(HttpContext.Request.Cookies["Token"].ToString()).results.Where(x => x.Value == "2" || x.Value == "3");
                ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
                ViewBag.EmployeeList = objDrop.Employeelist(HttpContext.Request.Cookies["Token"].ToString(), model.Vertical).results;

            }
            catch (Exception ex)
            {


            }

            return View("ViewAllTask", model);
        }

        public IActionResult ReassignTask(int Id)
        {
            Task_Model modal = new Task_Model();
            Task_Model modal1 = new Task_Model();
            int OfficeId = Convert.ToInt32(HttpContext.Request.Cookies["OfficeId"].ToString());
            modal = objDrop.GetReassignDataList(HttpContext.Request.Cookies["Token"].ToString(), OfficeId, Id).FirstOrDefault();
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            return View(modal);
        }

        [HttpPost]
        [SessionAuthorizeFilter("SaveReAssignTask")]
        public ActionResult SaveReAssignTask(Task_Model objModel)
        {
            Task_Model model = new Task_Model();
            Task_Model returnmodel = new Task_Model();
            VSCUserData objmodel = new VSCUserData();
            VSCCO_Repository objVSCCOrepo = new VSCCO_Repository();
            VSCCC_Repository objVSCCCrepo = new VSCCC_Repository();
            PIB_Repository objPIBrepo = new PIB_Repository();
            PTS_Repository objPTSrepo = new PTS_Repository();
            Attorneys_Repository objAttrepo = new Attorneys_Repository();
            string DetailResult = string.Empty;
            int loop = objModel.RowCount;
            try
            {

                int result = objModel.Id;

                if (Convert.ToInt32(result) > 0)
                {
                    for (int i = 0; i < loop; i++)
                    {
                        model.Todoid = Convert.ToInt32(result);
                        model.AssignTo = HttpContext.Request.Form["hdEmployee" + i + ""];
                        model.StartDate = HttpContext.Request.Form["StartDate" + i + ""];
                        model.EndDate = HttpContext.Request.Form["EndDate" + i];
                        model.Verticals = Convert.ToInt32(HttpContext.Request.Form["hdVertical" + i]);

                        //if (model.Verticals == 1)
                        //{
                        //    objmodel = objVSCCOrepo.UserData(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(model.AssignTo)).results;
                        //}
                        //if (model.Verticals == 2)
                        //{
                        //    objmodel = objVSCCCrepo.UserData(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(model.AssignTo)).results;
                        //}
                        //if (model.Verticals == 3)
                        //{
                        //    objmodel = objPIBrepo.UserData(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(model.AssignTo)).results;
                        //}
                        //if (model.Verticals == 4)
                        //{
                        //    objmodel = objPTSrepo.UserData(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(model.AssignTo)).results;
                        //}
                        //if (model.Verticals == 5)
                        //{
                        //    objmodel = objAttrepo.UserData(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(model.AssignTo)).results;
                        //}
                        model.Status = "1";
                        model.PriorityId = Convert.ToInt32(HttpContext.Request.Form["hdPriority" + i]);
                        model.FrequencyId = Convert.ToInt32(HttpContext.Request.Form["hdFrequency" + i]);
                        DetailResult = objDrop.SaveReAssignTaskDetails(model, HttpContext.Request.Cookies["Token"].ToString());

                        List<Task_Model> objList = new List<Task_Model>();

                        objList = objDrop.GetUserDetail(HttpContext.Request.Cookies["Token"].ToString(), model.AssignTo);
                        SentEmail(objList[0].EmailId, HttpContext.Request.Cookies["Name"].ToString(), objModel.Title, objModel.Description, objList[0].Name);
                    }
                }

                if (Convert.ToInt32(result) > 0)
                {
                    @ViewBag.Message = "ToDo Added Successfully..!";
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                else
                {
                    @ViewBag.Message = "Oops Somthing went wrong!";
                    @ViewBag.HideClass = "alert alert-danger";
                }

            }
            catch (Exception ex)
            {

            }

            Task_Model newmodel = new Task_Model();
            newmodel.Date = DateTime.Now.ToString("dd-MM-yyyy");
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            return RedirectToAction("AssignTask", newmodel);
        }

        [HttpPost]
        [SessionAuthorizeFilter("UpdateAssignTask")]
        public ActionResult UpdateAssignTask(Task_Model objModel)
        {
            Task_Model model = new Task_Model();
            Task_Model returnmodel = new Task_Model();
            string DetailResult = string.Empty;
            int loop = objModel.RowCount;
            try
            {

                int result = objModel.Id;

                if (Convert.ToInt32(result) > 0)
                {
                    for (int i = 0; i < loop; i++)
                    {
                        model.Todoid = Convert.ToInt32(result);
                        model.AssignTo = HttpContext.Request.Form["hdEmployee" + i + ""];
                        model.StartDate = HttpContext.Request.Form["StartDate" + i + ""];
                        model.EndDate = HttpContext.Request.Form["EndDate" + i];
                        model.Verticals = Convert.ToInt32(HttpContext.Request.Form["hdVertical" + i]);
                        model.Status = "1";
                        model.PriorityId = Convert.ToInt32(HttpContext.Request.Form["hdPriority" + i]);
                        model.FrequencyId = Convert.ToInt32(HttpContext.Request.Form["hdFrequency" + i]);
                        DetailResult = objDrop.UpdateAssignTaskDetails(model, HttpContext.Request.Cookies["Token"].ToString());
                    }
                }

                if (Convert.ToInt32(result) > 0)
                {
                    @ViewBag.Message = "ToDo Added Successfully..!";
                    @ViewBag.HideClass = "alert alert-success";
                    ModelState.Clear();
                }
                else
                {
                    @ViewBag.Message = "Oops Somthing went wrong!";
                    @ViewBag.HideClass = "alert alert-danger";
                }

            }
            catch (Exception ex)
            {

            }

            Task_Model newmodel = new Task_Model();
            newmodel.Date = DateTime.Now.ToString("dd-MM-yyyy");
            ViewBag.PriorityList = objDrop.GetPriorityList(HttpContext.Request.Cookies["Token"].ToString()).results;
            ViewBag.VerticalList = objDrop.VerticalList(HttpContext.Request.Cookies["Token"].ToString(), Convert.ToInt32(HttpContext.Request.Cookies["EmpId"].ToString()), Convert.ToInt32(HttpContext.Request.Cookies["Vertical"].ToString())).results;
            return RedirectToAction("ViewAssignTask", newmodel);
        }
    }
}

