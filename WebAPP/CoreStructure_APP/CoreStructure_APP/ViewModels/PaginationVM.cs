using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.ViewModels
{
    public class PaginationVM
    {
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int RowsPerPage { get; set; }
        public string Search { get; set; }
        public string SortCol { get; set; }
        public string SortOrder { get; set; }
        public string PageAction { get; set; }

        public string Message { get; set; }

        public string Token { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;


    }
}
