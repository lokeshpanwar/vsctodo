﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class ServerLog_Model
    {
        public List<Log_Table_Model> Log_Table_Model_List { get; set; }
    }

    public class ServerLog_Model_Result
    {
        public ServerLog_Model results { get; set; }
    }
}
