﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Employee_Process_value_Model
    {
        public long SLNO { get; set; }

        public long UserID { get; set; }

        public float MaxValue { get; set; }

        public float MinValue { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime ModifiedOn { get; set; }

        public long ModifiedBy { get; set; }

        public bool isApprover { get; set; }



        public string Employee_Name { get; set; }
        public string Employee_ID { get; set; }
        public string Employee_Company_Name { get; set; }
        public string Employee_Domain { get; set; }
        public string Manager_Name { get; set; }
        public string Manager_ID { get; set; }
        public string Manager_Domain { get; set; }

    }


    public class Employee_Process_value_list_Model
    {
        public List<Employee_Process_value_Model> Employee_Process_value_list { get; set; }


        [Required(ErrorMessage ="Sub Company can't be blank!")]
        [Display(Name="SUB COMPANY")]
        public long Sub_Company { get; set; }


        [Display(Name = "DOMAIN")]
        [Required(ErrorMessage = "Domain can't be blank!")]
        public long Domain_ID { get; set; }


        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

    }


    public class Employee_Process_value_results
    {
        public List<Employee_Process_value_Model> results { get; set; }
    }

    public class Employee_Process_value_list_results
    {
        public Employee_Process_value_list_Model results { get; set; }
    }


}
