﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Exception_Error_Model
    {
        public int trace_id { get; set; }
        public string stack_trace { get; set; }
        public string exception_message { get; set; }
        public string inner_exception { get; set; }
        public string exception_from { get; set; }
        public string error_page { get; set; }
        public DateTime created_on { get; set; }
        public string created_by { get; set; }
    }
}
