﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Theme_table_Model
    {
        [Display(Name = "THEME NAME")]
        [Required(ErrorMessage = "Theme Name can't be blank!")]
        [MaxLength(100)]
        public string Theme_Name { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }
    }
}
