﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class PolicyReport
    {
        [Display(Name = "Type")]
        public int TypeId { get; set; }

        public int HdTypeId { get; set; }
        public string TypeName { get; set; }

        [Display(Name = "Office")]
        public int Officeid { get; set; }
        public string Office { get; set; }


        [Display(Name = "Group")]
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        [Display(Name = "Contact Person Name")]
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        [Display(Name = "Insurance Company")]
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        [Display(Name = "Issuing Office")]
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        [Display(Name = "Policy Status")]
        public int PolicyStatusId { get; set; }
        public string PolicyStatus { get; set; }
        [Display(Name = "From Date")]
        [Required(ErrorMessage = "From Date is Required")]
        public string FromDate { get; set; }
        [Display(Name = "To Date")]
        [Required(ErrorMessage = "To Date is Required")]
        public string ToDate { get; set; }

        public string hdfromdate { get; set; }
        public string hdtodate { get; set; }
        public int hdContactPersonId { get; set; }
        public int hdCategoryId { get; set; }
        public int hdInsuranceCompanyId { get; set; }
        public int hdIssuingOfficeId { get; set; }

        public int hdProductId { get; set; }
        public int hdGroupId { get; set; }
        public int hdPolicyStatusId { get; set; }
        public int hdOfficeid { get; set; }
        //public int TotalRows { get; set; }
        //public int CurrentPage { get; set; }
        //public int TotalPages { get; set; }
        //public int RowsPerPage { get; set; }
        //public string Search { get; set; }
        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;
        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }

        public List<PolicyReport> ReportGrid { get; set; }
    }

    public class DailyBusiness
    {
        [Display(Name = "Office")]
        public int OfficeId { get; set; }
        public string Office { get; set; }
        [Display(Name = "From Date")]
        public string FromDate { get; set; }
        [Display(Name = "To Date")]
        public string ToDate { get; set; }
        public string InsCo { get; set; }
        public string Category { get; set; }
        public string PremiumPayable { get; set; }

        public string Percentage { get; set; }
        [Display(Name = "Client Name")]
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        [Display(Name = "Order By")]
        public string OrderBy { get; set; }
        public List<DailyBusiness> ViewPolicy { get; set; }
        public List<DailyBusiness> ViewPolicy1 { get; set; }
        //public DayWiseData ListData { get; set; }
        public String QueryData { get; set; }
        public String TotalAmt { get; set; }

        public string CompWiseChart { get; set; }
        public string CategoryWiseChart { get; set; }
    }
    //public class DayWiseData
    //{
    //    public String QueryData { get; set; }
    //    public String TotalAmt { get; set; }
    //}
    public class DailyBusiness_Table
    {
        public List<DailyBusiness> results { get; set; }

    }
    public class DailyBusiness_API
    {
        public DailyBusiness results { get; set; }
    }

    public class UpcomingPolicy
    {
        [Display(Name = "Days")]
        public int DaysId { get; set; }
        public string Days { get; set; }

        public int hdDaysId { get; set; }
        public int hdOfficeId { get; set; }

        [Display(Name = "Office")]
        public int OfficeId { get; set; }
        public string Office { get; set; }
        public string PolicyNo { get; set; }
        public string ExpiryDate { get; set; }
        public string Policyholdername { get; set; }
        public string ContactPersonName { get; set; }
        public string InsCompany { get; set; }
        public string IssuingOfc { get; set; }
        public decimal PremiumPayable { get; set; }
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CostCenterName { get; set; }
    }
    public class UpcomingPolicy_API
    {
        public List<UpcomingPolicy> results { get; set; }


    }
    public class PolicyCount_API
    {
        public List<SelectListItem> results { get; set; }
    }


    public class ClaimReport
    {
        [Display(Name = "Office")]
        public int OfficeId { get; set; }
        public string Office { get; set; }
        public List<SelectListItem> OfficeList { get; set; }

        [Display(Name = "Policy No")]
        public int PolicyNoId { get; set; }
        [Display(Name = "Policy No")]
        public string PolicyNo { get; set; }

        public List<SelectListItem> PolicyNoList { get; set; }

        public string hdPolicyNo { get; set; }
        public int hdContactPersonId { get; set; }
        public int hdPolicyholderId { get; set; }

        public int hdOfficeId { get; set; }
        [Display(Name = "CP Name")]
        public int ContactPersonId { get; set; }
        public string ContactPerson { get; set; }
        public List<SelectListItem> ContactPersonList { get; set; }
        [Display(Name = "PH Name")]
        public int PolicyholderId { get; set; }
        public string PolicyholderName { get; set; }
        public List<SelectListItem> PolicyholderList { get; set; }

        public string UnderwriteDate { get; set; }
        public string PolicyDate { get; set; }
        public string ClaimDate { get; set; }
        public string TPName { get; set; }
        public string CategoryName { get; set; }
        public string MobileNo { get; set; }
        public string ProductName { get; set; }
        public string InsCo { get; set; }
        public string ClaimAmount { get; set; }
        public string SettleAmount { get; set; }
        public string PremiumPayable { get; set; }
        public string CommissionRate { get; set; }
        public string IssuingOffice { get; set; }
        public string ExpiryDate { get; set; }
        public int TypeId { get; set; }
        public string PolicyStatus { get; set; }
        public string ClaimStatus { get; set; }
        public string SubjectInsured { get; set; }
        public string PatientName { get; set; }
        public int Pid { get; set; }
    }

    public class Claim_Table
    {
        public List<ClaimReport> results { get; set; }
    }

    public class Dashboard_Model
    {
        public List<DailyBusiness> ViewPolicy { get; set; }
        public List<DailyBusiness> ViewPolicy1 { get; set; }
    }

    public class DashboardGraph
    {
        public int Id { get; set; }
        public string MonthYear { get; set; }
        public string SaleAmount { get; set; }
    }
    public class DashboardGraph_Table
    {
        public List<DashboardGraph> results { get; set; }
    }

    public class RenewalReport_Filter
    {
        [Display(Name = "Office")]
        public int Officeid { get; set; }
        public string Office { get; set; }

        [Display(Name = "Contact Person Name")]
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        public string ToDate { get; set; }

        [Display(Name = "Policy No.")]
        public string PolicyNo { get; set; }
        public string RenewalType { get; set; }
        public string hdFromDate { get; set; }
        public string hdToDate { get; set; }
        public int hdContactPersonId { get; set; }
        public string hdPolicyNo { get; set; }
        public string hdRenewalType { get; set; }
        public int hdOfficeid { get; set; }
    }
}
