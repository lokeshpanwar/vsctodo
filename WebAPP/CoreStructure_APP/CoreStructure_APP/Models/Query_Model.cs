﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Query_Model
    {
    }
    public class Query
    {
        public long QId { get; set; }

        public int ClientId{ get; set; }

        [Display(Name = "Client Name")]
        [Required(ErrorMessage = "Client Name can't be blank")]
        public string ClientName { get; set; }

        [Display(Name = "Query Date")]
        [Required(ErrorMessage = "Query Date can't be blank")]
        public string QueryDate { get; set; }

        [Display(Name = "Contact Person")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }

        [Display(Name = "Father's Name")]
        //[Required(ErrorMessage = "Father's Name can't be blank")]
        public string FatherName { get; set; }

        [Display(Name = "Email")]
        //[Required(ErrorMessage = "Email Id can't be blank")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailId { get; set; }

        [Display(Name = "Contact No")]
        [Required(ErrorMessage = "Contact No can't be blank")]

        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
            
        public string MobileNo { get; set; }

        [Display(Name = "Address")]
        //[Required(ErrorMessage = "Address can't be blank")]
        public string Address { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select a State")]
        public int StateId { get; set; }
        public string StateName { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select a City")]
        public int CityId { get; set; }
        public string CityName { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Select a Category")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        [Display(Name = "Expiry Date")]
        [Required(ErrorMessage = "Expiry Date can't be blank")]
        public string PolicyExpiryDate { get; set; }

        [Display(Name = "Quotation Amount")]
        public decimal QuatationAmount { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        [Display(Name = "Status")]
        public int QStatus { get; set; }
        public string QStatusName { get; set; }

        [Display(Name = "User")]
        public long EmpId { get; set; }

        [Display(Name = "Reference Person")]
        public string ReferencePerson { get; set; }
        public string PendingRemark { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
        [Display(Name = "Date")]
        public string Date { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }
        [Display(Name = "To Date")]
        public string ToDate { get; set; }
        [Display(Name = "Client Name")]
        public string CName { get; set; }
    }

    public class Query_Table
    {
        public List<Query> results { get; set; }
    }

    public class UnderWrite
    {

        public long QId { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select a Type")]
        public int TypeId { get; set; }
        public string TypeName { get; set; }

        [Display(Name = "Group")]
        [Required(ErrorMessage = "Please Select a Group")]
        [BindRequired]
        public int GroupId { get; set; }
        public string GroupName { get; set; }

        [Display(Name = "Contact Person")]
        [Required(ErrorMessage = "Please Select a Contact Person")]
        [BindRequired]
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }

        [Display(Name = "Policy Holder")]
        [Required(ErrorMessage = "Please Select a Policy Holder")]
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }

        [Display(Name = "UnderWrite Date")]
        [Required(ErrorMessage = "Please Select a Date")]
        public string UnderWriteDate { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Select a Category")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        [Display(Name = "Insurance Company")]
        [Required(ErrorMessage = "Please Select a Insurance Company")]
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }

        [Display(Name = "Issuing Office")]
        [Required(ErrorMessage = "Please Select a Issuing Office")]
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }

        [Display(Name = "Product")]
        [Required(ErrorMessage = "Please Select a Product")]
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        [Display(Name = "Subject Insured")]
        [Required(ErrorMessage = "Subject Insured is Required")]
        public string SubjectInsured { get; set; }

        [Display(Name = "Cost Center")]
        public string CostCenter { get; set; }

        [Display(Name = "Commission Rate")]
        [Required(ErrorMessage = "Commission Rate is Required")]
        public string CommissionRate { get; set; }

        [Display(Name = "Premium")]
        [Required(ErrorMessage = "Premium is Required")]
        public string Premium { get; set; }

        [Display(Name = "CGST")]
        [Required(ErrorMessage = "CGST is Required")]
        public string CGST { get; set; }

        [Display(Name = "SGST")]
        [Required(ErrorMessage = "SGST is Required")]
        public string SGST { get; set; }

        [Display(Name = "Premium Payable")]
        [Required(ErrorMessage = "Premium Payable is Required")]
        public string PremiumPayable { get; set; }

        [Display(Name = "Discount Provided")]
        public string DiscountProvided { get; set; }

        [Display(Name = "Upload")]
        public IFormFile UploadFile { get; set; }
        public string fileName { get; set; }
        public string fileExt { get; set; }

        public int OfficeId { get; set; }
        [Display(Name = "OD Commission Rate")]
        public string ODCommissionRate { get; set; }

        [Display(Name = "TP Commission Rate")]
        public string TPCommissionRate { get; set; }
    }

    public class UnderWrite_Table
    {
        public List<UnderWrite> results { get; set; }
    }

    public class Policy
    {
        public long PId { get; set; }
        public long QId { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select a Type")]
        public int TypeId { get; set; }
        public string TypeName { get; set; }

        [Display(Name = "Group")]
        [Required(ErrorMessage = "Please Select a Group")]
        public int GroupId { get; set; }
        public string GroupName { get; set; }

        [Display(Name = "Contact Person")]
        [Required(ErrorMessage = "Please Select a Contact Person")]
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }

        [Display(Name = "Policy Holder")]
        [Required(ErrorMessage = "Please Select a Policy Holder")]
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }

        [Display(Name = "UnderWrite Date")]
        [Required(ErrorMessage = "Please Select a Date")]
        public string UnderWriteDate { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Select a Category")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        [Display(Name = "Insurance Company")]
        [Required(ErrorMessage = "Please Select a Insurance Company")]
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }

        [Display(Name = "Issuing Office")]
        [Required(ErrorMessage = "Please Select a Issuing Office")]
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }

        [Display(Name = "Product")]
        [Required(ErrorMessage = "Please Select a Product")]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        [Display(Name = "Subject Insured")]
        [Required(ErrorMessage = "Subject Insured is Required")]
        public string SubjectInsured { get; set; }

        [Display(Name = "Cost Center")]
        [Required(ErrorMessage = "Cost Center is Required")]
        public string CostCenter { get; set; }

        [Display(Name = "Commission Rate")]
        [Required(ErrorMessage = "Commission Rate is Required")]
        public string CommissionRate { get; set; }

        [Display(Name = "Premium")]
        [Required(ErrorMessage = "Premium is Required")]
        public string Premium { get; set; }

        [Display(Name = "CGST")]
        [Required(ErrorMessage = "CGST is Required")]
        public string CGST { get; set; }

        [Display(Name = "SGST")]
        [Required(ErrorMessage = "SGST is Required")]
        public string SGST { get; set; }

        [Display(Name = "Premium Payable")]
        [Required(ErrorMessage = "Premium Payable is Required")]
        public string PremiumPayable { get; set; }
        public int PolicyStatus { get; set; }
        public string PolicyStatusName { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }


        [Display(Name = "Vehicle No.")]
        [Required(ErrorMessage = "Vehicle No. is Required")]
        public string VehicleNo { get; set; }

        [Display(Name = "OD")]
        [Required(ErrorMessage = "OD is Required")]
        public string OnDamage { get; set; }

        [Display(Name = "PA")]
        [Required(ErrorMessage = "PA is Required")]
        public string PersonalAccident { get; set; }

        [Display(Name = "TP")]
        [Required(ErrorMessage = "TP is Required")]
        public string ThirdParty { get; set; }



        [Display(Name = "Inception Date")]
        
        public string inceptiondate { get; set; }

        [Display(Name = "IDV Sum Insured")]
        [Required(ErrorMessage = "IDV Sum Insured is Required")]
        public string IDV { get; set; }

        [Display(Name = "Policy Date")]
        [Required(ErrorMessage = "Policy Date is Required")]
        //[Required(ErrorMessage = "{0} is required.")]
        public string PolicyDate { get; set; }

        [Display(Name = "Expiry Date")]
        [Required(ErrorMessage = "Expiry Date is Required")]
        public string ExpiryDate { get; set; }

        [Display(Name = "Policy No.")]
        [Required(ErrorMessage = "Policy No. is Required")]
        public string PolicyNo { get; set; }

        [Display(Name = "Previous Policy No.")]
        [Required(ErrorMessage = "Previous Policy No. is Required")]
        public string PreviousPolicyNo { get; set; }

        [Display(Name = "Upload")]
        public IFormFile UploadFile { get; set; }

        public string UploadFileName { get; set; }

        public string fileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }
        public int IsRenewal { get; set; }
        public int IsClaimed { get; set; }

        public string ContactNo { get; set; }
        public string ProductRate { get; set; }

        public string ODRate { get; set; }

        //public int TotalRows { get; set; }
        //public int CurrentPage { get; set; }
        //public int TotalPages { get; set; }
        //public int RowsPerPage { get; set; }
        //public string Search { get; set; }
        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;
        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }
        
        public decimal GST { get; set; }
        public string MobileNo { get; set; }       
        public int PolicyStatusId { get; set; }
        public string UW_FileName { get; set; }
        public string DiscountProvided { get; set; }
        [Display(Name = "OD Commission Rate")]
        public string ODCommissionRate { get; set; }

        [Display(Name = "TP Commission Rate")]
        public string TPCommissionRate { get; set; }
        public string BrokerageAmount { get; set; }
        public string BrokerAmount { get; set; }
        public string GSTTotal { get; set; }
        public string totalBorkerageamt { get; set; }
    }
    public class Policy_Table
    {
        public List<Policy> results { get; set; }
    }
    public class Policy_Filter
    {

        [Display(Name = "Office")]
        public int Officeid { get; set; }
        public string Office { get; set; }


        [Display(Name = "Policy Status")]
        public int PStatus { get; set; }

        [Display(Name = "Contact Person")]
        public int ContactPersonId { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Group")]
        public int GroupId { get; set; }

        [Display(Name = "Query Type")]
        public int QueryTypeId { get; set; }

        [Display(Name = "Expiry Date")]
        public string ExpiryDate { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        public string ToDate { get; set; }

        [Display(Name = "InsCo.")]
        public int InsuranceCompanyId { get; set; }

        [Display(Name = "Policy Holder")]
        public int PolicyHolderId { get; set; }

        public int hdPolicyHolderId { get; set; }
        public string hdfromdate { get; set; }
        public string hdtodate { get; set; }
        public int hdContactPersonId { get; set; }
        public int hdCategoryId { get; set; }
        public int hdInsuranceCompanyId { get; set; }
        public int hdIssuingOfficeId { get; set; }

        public int hdProductId { get; set; }
        public int hdGroupId { get; set; }
        public int hdPolicyStatusId { get; set; }
    }
}
