﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Task_Model
    {
        public int Id { get; set; }

        public int Todoid { get; set; }

        [Display(Name = "Date")]
        [Required(ErrorMessage = "Date can't be blank")]
        public string Date { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title can't be blank")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Display(Name = "Frequency")]
        [Required(ErrorMessage = "Frequency can't be blank")]
        public int FrequencyId { get; set; }
        public string Frequency { get; set; }
        [Display(Name = "Rejection Remark")]
        public string RejectRemark { get; set; }
        public int CreateBy { get; set; }
        [Display(Name = "Priority")]
        [Required(ErrorMessage = "Priority can't be blank")]
        public int PriorityId { get; set; }
        [Display(Name = "Assign To")]
        [Required(ErrorMessage = "Assign To can't be blank")]
        public int AssignToId { get; set; }
        [Display(Name = "Start Date")]
        [Required(ErrorMessage = "Start Date can't be blank")]
        public string StartDate { get; set; }
        [Display(Name = "End Date")]
        [Required(ErrorMessage = "End Date can't be blank")]
        public string EndDate { get; set; }

        [Display(Name = "Time")]
        public string Time { get; set; }



        public string Priority { get; set; }

        public string PriorityText { get; set; }
        public string VerticalText { get; set; }
        public string EmployeeText { get; set; }
        public int Selfassign { get; set; }
        public string AssignTo { get; set; }
        public string Status { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "Please Select Status")]
        public int StatusId { get; set; }
        [Display(Name = "Vertical")]
        [Required(ErrorMessage = "Vertical can't be blank")]
        public int Verticals { get; set; }
        [Display(Name = "Vertical")]
        [Required(ErrorMessage = "Vertical can't be blank")]
        public int Vertical { get; set; }

        [Display(Name = "Client Name")]
        [Required(ErrorMessage = "Client Name can't be blank")]
        public int Clientid { get; set; }


        [Display(Name = "Activity")]
        [Required(ErrorMessage = "Activity can't be blank")]
        public int Activityid { get; set; }


        [Display(Name = "Activity")]
        [Required(ErrorMessage = "Activity can't be blank")]
        public int AttorneyActivityid { get; set; }

        [Display(Name = "SubActivity")]
        [Required(ErrorMessage = "Subactivity can't be blank")]
        public int Subactivityid { get; set; }

        [Display(Name = "SubActivity")]
        [Required(ErrorMessage = "Subactivity can't be blank")]
        public int AttorneySubactivityid { get; set; }

        [Display(Name = "Project")]
        public int Projectid { get; set; }

        [Display(Name = "Module")]
        public int Moduleid { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category can't be blank")]
        public int Categoryid { get; set; }

        [Display(Name = "SubCategory")]
        [Required(ErrorMessage = "SubCategory can't be blank")]
        public int Subcategoryid { get; set; }
        public string CreateOn { get; set; }
        public string AssignBy { get; set; }
        public int TotalTask { get; set; }
        [Display(Name = "Employee Name")]
        public int EmpId { get; set; }

        [Display(Name = "Employee Name")]
        public int EmployeeId { get; set; }
        //public string totaltask { get; set; }
        public int TotalRowAdded { get; set; }
        public int RowCount { get; set; }

        public int Isaccepted { get; set; }

        public int Hdaccepted { get; set; }

        //public string TotalTask { get; set; }

        public int IsActiveaction { get; set; }

        public int HDVertical { get; set; }


        [Display(Name = "Vertical")]
        public int Verticals1 { get; set; }

        public List<SelectListItem> VerticalsList { get; set; }

        [Display(Name = "Client Name")]
        public int Clientid1 { get; set; }

        [Display(Name = "Project")]
        public int Projectid1 { get; set; }

        [Display(Name = "Module")]
        public int Moduleid1 { get; set; }


        [Display(Name = "Activity")]
        public int Activityid1 { get; set; }



        [Display(Name = "SubActivity")]
        public int Subactivityid1 { get; set; }

        public string userstring { get; set; }
        public int IsReassign { get; set; }

        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        //public int Statusid { get; set; }
        public List<Task_Model> TaskDetailList { get; set; }
        public List<Task_Model> DataList { get; set; }

        public int gridcount { get; set; }
    }
    public class Task_Model_API
    {
        public List<SelectListItem> results { get; set; }
    }
    public class Task_Table
    {
        public List<Task_Model> results { get; set; }


    }
}
