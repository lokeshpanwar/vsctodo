﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Claim_Model
    {
    }

    public class Claim_Filter
    {
        [Display(Name = "Contact Person")]
        public int ContactPersonId { get; set; }

        [Display(Name = "Policy No")]
        public string PolicyNo { get; set; }

        [Display(Name = "Policy Holder")]
        public string PolicyHolder { get; set; }

        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        public string ToDate { get; set; }
    }

    public class LodgeClaim_Model
    {
        public long ClaimId { get; set; }
        public long PolicyId { get; set; }

        [Display(Name = "Claim Officer")]
        [Required(ErrorMessage = "Claim Officer can't be blank")]
        public string ClaimOfficer { get; set; }

        [Display(Name = "Claim Amount")]
        [Required(ErrorMessage = "Claim Amount can't be blank")]
        public string ClaimAmount { get; set; }

        [Display(Name = "Claim Date")]
        [Required(ErrorMessage = "Claim Date can't be blank")]
        public string ClaimDate { get; set; }

        [Display(Name = "Claim Number")]
        [Required(ErrorMessage = "Claim Number can't be blank")]
        public string ClaimNo { get; set; }

        [Display(Name = "Spot Surveyor Name")]
        [Required(ErrorMessage = "Spot Surveyor Name can't be blank")]
        public string TPName { get; set; }

        [Display(Name = "Patient Name")]
        [Required(ErrorMessage = "Patient Name can't be blank")]
        public string PatientName { get; set; }

        [Display(Name = "Contact No")]
        [Required(ErrorMessage = "Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string Mobile { get; set; }

        [Display(Name = "Upload File")]
        public string ClaimFileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }
        public int IsActive { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }
        [Display(Name = "TP Name/Surveyor")]
        [Required(ErrorMessage = "TP Name/Surveyor can't be blank")]
        public string TPSurveyor { get; set; }
        [Display(Name = "Contact No")]
        [Required(ErrorMessage = "Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string TPSurveyorContactNo { get; set; }

    }
    public class LodgeClaim_Model_Table
    {
        public List<LodgeClaim_Model> results { get; set; }
    }

    public class ViewLodgeClaim_Model
    {
        public long ClaimId { get; set; }
        public long PolicyId { get; set; }
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CommissionRate { get; set; }
        public string ExpiryDate { get; set; }
        public string PolicyNo { get; set; }
        public string ClaimFileName { get; set; }
        public int ClaimStatus { get; set; }
        public int ClaimType { get; set; }
        public string ClaimDate { get; set; }
        public int ClaimStepId { get; set; }
        public string ClaimStepName { get; set; }
        public string TPSurveyor { get; set; }
        public string TPSurveyorContactNo { get; set; }
    }
    public class ViewLodgeClaim_Model_Table
    {
        public List<ViewLodgeClaim_Model> results { get; set; }
    }

    public class ClaimStepsDetails_Model
    {
        public long Id { get; set; }
        public long ClaimId { get; set; }
        public int StepId { get; set; }
        public string StepName { get; set; }
        public string StepDate { get; set; }
        public string UserName { get; set; }
        public string Remark { get; set; }
    }

    public class ClaimStepsDetails_Model_Table
    {
        public List<ClaimStepsDetails_Model> results { get; set; }
    }

}
