﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Master_Model
    {
    }
    public class States
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }
    public class Cities
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
    public class Users
    {
        public long UserId { get; set; }


        [Display(Name = "Role")]
        //[Required(ErrorMessage = "Please Select a Role")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        [Display(Name = "Office")]
        //[Required(ErrorMessage = "Please Select a Role")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }

        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User Name can't be blank")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Password Code can't be blank")]
        public string Password { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name can't be blank")]
        public string Name { get; set; }

        [Display(Name = "Father's Name")]
        [Required(ErrorMessage = "Father's Name can't be blank")]
        public string FatherName { get; set; }

        [Display(Name = "Mother's Name")]
        [Required(ErrorMessage = "Mother's Name can't be blank")]
        public string MotherName { get; set; }

        [Display(Name = "DOB")]
        [Required(ErrorMessage = "DOB Name can't be blank")]
        public string DOB { get; set; }

        [Display(Name = "DOJ")]
        [Required(ErrorMessage = "DOJ can't be blank")]
        public string DOJ { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email can't be blank")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailId { get; set; }

        [Display(Name = "Contact")]
        [Required(ErrorMessage = "Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string MobileNo { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address can't be blank")]
        public string Address { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select a State")]
        [BindRequired]
        public int StateId { get; set; }
        public string StateName { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select a City")]
        [BindRequired]
        public int CityId { get; set; }
        public string CityName { get; set; }

        [Display(Name = "Town")]
        //[Required(ErrorMessage = "Town can't be blank")]
        public string Town { get; set; }

        [Display(Name = "Emergency Contact")]
        [Required(ErrorMessage = "Emergency Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Emergency Contact format is not valid.")]
        public string EmergencyContactNo { get; set; }

        [Display(Name = "Name With Relation")]
        [Required(ErrorMessage = "Name With Relation can't be blank")]
        public string NameRelation { get; set; }

        [Display(Name = "POS Code")]
        public string POSCode { get; set; }
        public long CreatedBy { get; set; }

        [Display(Name = "User Image")]
        public IFormFile UserImage { get; set; }

        public string fileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }


        public String fileDataInString { get; set; }

        //public int TotalRows { get; set; }

        //public int CurrentPage { get; set; }

        //public int TotalPages { get; set; }

        //public int RowsPerPage { get; set; }


        //public string Search { get; set; }

        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;

        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }

        public string photo { get; set; }

    }
    public class Users_Table
    {
        public List<Users> results { get; set; }
    }

    public class Users_Profile
    {
        public Users results { get; set; }
    }

    public class Common_Filter_Model
    {
        [Display(Name = "Role")]
        public int RoleId { get; set; }

        [Display(Name = "Name")]
        public int UserId { get; set; }

        [Display(Name = "Office")]
        public int OfficeId { get; set; }

    }

    #region Group
    public class Group
    {
        public int GroupId { get; set; }
        [Display(Name = "Group Name")]
        [Required(ErrorMessage = "Name can't be blank")]
        public string GroupName { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }


        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }
    public class Group_Table
    {
        public List<Group> results { get; set; }
    }
    #endregion

    #region Product
    public class Product
    {
        public int ProductId { get; set; }

        [Display(Name = "Product Name")]
        [Required(ErrorMessage = "Name can't be blank")]
        public string ProductName { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category can't be blank")]
        public int CategoryId { get; set; }

        [Display(Name = "Category")]
        public string CategoryName { get; set; }


        [Display(Name = "Insurance Company")]
        [Required(ErrorMessage = "Category can't be blank")]
        public int InsuranceCompanyId { get; set; }

        [Display(Name = "Insurance Company")]
        public string InsuranceCompany { get; set; }



        [Display(Name = "Rate")]
        [Required(ErrorMessage = "OD Rate can't be blank")]
        public string ODRate { get; set; }

        [Display(Name = "Other Rate")]
        [Required(ErrorMessage = "OD Rate can't be blank")]
        public string Rate { get; set; }

        public int OfficeId { get; set; }

        public long CreatedBy { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
        public decimal totalrate { get; set; }
    }
    public class Product_Table
    {
        public List<Product> results { get; set; }
    }
    #endregion

    #region IssuingOffice
    public class IssuingOffice
    {
        //[Required(ErrorMessage = "{0} is required.")]
        //[BindRequired]
        public int IssuingOfficeId { get; set; }

        [Display(Name = "Issuing Office")]
        [Required(ErrorMessage = "Issuing Office can't be blank")]


        public string IssuingOfficeName { get; set; }

        [Display(Name = "Insurance Company")]
        [Required(ErrorMessage = "Insurance Company can't be blank")]
        public int InsuranceCompanyId { get; set; }

        [Display(Name = "Insurance Company")]
        public string InsuranceCompanyName { get; set; }

        [Display(Name = "Short Name")]
        [Required(ErrorMessage = "Short Name can't be blank")]
        public string ShortName { get; set; }

        [Display(Name = "BM Name")]
        [Required(ErrorMessage = "Manager Name can't be blank")]
        public string ManagerName { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address can't be blank")]
        public string Address { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City can't be blank")]
        public string City { get; set; }

        [Display(Name = "BM Contact No")]
        [Required(ErrorMessage = "Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]

        public string MobileNo { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email ID can't be blank")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailID { get; set; }

        [Display(Name = "Broker Code")]
        [Required(ErrorMessage = "Code can't be blank")]
        public string Code { get; set; }

        [Display(Name = "SM Name")]
        [Required(ErrorMessage = "SM Name can't be blank")]
        public string SalesManagerName { get; set; }

        [Display(Name = "SM Contact No")]
        [Required(ErrorMessage = "SM Contact No can't be blank")]
        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string SMContactNo { get; set; }

        public int OfficeId { get; set; }

        public long CreatedBy { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }
    public class IssuingOffice_Table
    {
        public List<IssuingOffice> results { get; set; }
    }
    #endregion

    #region Category
    public class Category
    {
        public int CategoryId { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category Name can't be blank")]
        public string CategoryName { get; set; }

        public int OfficeId { get; set; }
        public long CreateBy { get; set; }
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }
        public int RowsPerPage { get; set; }
        public string Search { get; set; }
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }

    public class Category_Table
    {
        public List<Category> results { get; set; }
    }
    #endregion

    #region ContactPerson
    public class ContactPerson
    {
        public int ContactPersonId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Contact Person Name can't be blank")]
        public string Name { get; set; }

        [Display(Name = "Father's Name")]
        [Required(ErrorMessage = "Father's Name can't be blank")]
        public string FatherName { get; set; }

        [Display(Name = "Contact No.")]
        [Required(ErrorMessage = "Contact No can't be blank")]

        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string MobileNo { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email can't be blank")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string EmailId { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address can't be blank")]
        public string Address { get; set; }

        [Display(Name = "DOB")]
        [Required(ErrorMessage = "DOB can't be blank")]
        public string DOB { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int StateId { get; set; }
        public string StateName { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City can't be blank")]
        public int CityId { get; set; }
        public string CityName { get; set; }

        public string NameRelation { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        //public int TotalRows { get; set; }

        //public int CurrentPage { get; set; }

        //public int TotalPages { get; set; }

        //public int RowsPerPage { get; set; }


        //public string Search { get; set; }

        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;

        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }
    }
    public class ContactPerson_Table
    {
        public List<ContactPerson> results { get; set; }
    }
    #endregion

    #region TP/Surveyor
    public class TP_Surveyor
    {
        public int TPS_Id { get; set; }

        [Display(Name = "Insurance Co.")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int Ins_Comp { get; set; }
        public string InsuranceCompanyName { get; set; }

        [Display(Name = "TP Name")]
        [Required(ErrorMessage = "TP Name can't be blank")]
        public string TP_Name { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address can't be blank")]
        public string TP_Address { get; set; }

        [Display(Name = "Contact No.")]
        [Required(ErrorMessage = "Contact No can't be blank")]

        [RegularExpression(@"^\(?([6-9]{1})\)?[-. ]?([0-9]{5})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered Contact format is not valid.")]
        public string TP_ContactNo { get; set; }
        public int OfficeId { get; set; }
    }

    public class TP_Surveyor_Table
    {
        public List<TP_Surveyor> results { get; set; }
    }
    #endregion

    #region Activity
    public class Activity
    {
        public int ActivityId { get; set; }

        [Display(Name = "Activity")]
        [Required(ErrorMessage = "Activity Name can't be blank")]
        public string ActivityName { get; set; }

        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }

    public class Activity_Table
    {
        public List<Activity> results { get; set; }
    }

    #endregion

    #region SubActivity
    public class SubActivity
    {
        public int SubActivityId { get; set; }

        [Display(Name = "Activity")]
        [Required(ErrorMessage = "{0} is required.")]
        [BindRequired]
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }

        [Display(Name = "Sub-Activity")]
        [Required(ErrorMessage = "Sub-Activity Name can't be blank")]
        public string SubActivityName { get; set; }

        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }
    public class SubActivity_Table
    {
        public List<SubActivity> results { get; set; }
    }
    #endregion

    #region Holiday
    public class Holiday
    {
        public int HolidayId { get; set;}
        [Display(Name = "Holiday Name")]
        [Required(ErrorMessage = "Holiday Name can't be blank")]
        public string HolidayName { get; set; }
        public string Date { get; set; }
        public int OfficeId { get; set; }
        public long CreateBy { get; set; }
    }
    public class Holiday_Table
    {
        public List<Holiday> results { get; set; }
    }
    #endregion
}
