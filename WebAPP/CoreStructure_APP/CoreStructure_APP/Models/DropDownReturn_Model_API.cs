﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class DropDownReturn_Model_API
    {
        public List<SelectListItem> results { get; set; }
    }
}
    