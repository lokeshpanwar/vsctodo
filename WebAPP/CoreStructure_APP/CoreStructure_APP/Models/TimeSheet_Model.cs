﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class TimeSheet_Model
    {
    }

    public class Filltimesheet_Model
    {
        public int UpdateID { get; set; }
        public string HdBalancetobefill { get; set; }
        public string Date { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int SubActivityId { get; set; }
        public string SubActivityName { get; set; }
        public string Remarks { get; set; }
        public string NOHH { get; set; }
        public string NOMM { get; set; }
        public string NoOfHours { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string TotalTime { get; set; }
        public string Balancetobefill { get; set; }
        public int OfficeId { get; set; }
    }

    public class FilledTimesheetData
    {
        public List<FilledTimesheetCal> CalFillList { get; set; }
        public List<CurrentMonth> CrMonthList { get; set; }
    }
    public class FilledTimesheetCal
    {
        public string Dates { get; set; }
        public string Days { get; set; }
        public string Months { get; set; }
        public string Years { get; set; }
        public string IsTimesheetfill { get; set; }
        public string Remarks { get; set; }

        public int isholiday { get; set; }

        public string Newdates { get; set; }

    }
    public class CurrentMonth
    {
        public string CrMonth { get; set; }
    }
    public class FilledTimesheetData_API
    {
        public FilledTimesheetData results { get; set; }
    }

    public class TimesheetGrid
    {
        public string SheetId { get; set; }
        public string Date { get; set; }
        public string name { get; set; }
        public string clientname { get; set; }
        public string Activity { get; set; }
        public string SubActivity { get; set; }
        public string TimeTaken { get; set; }
        public string Remarks { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string Costhr { get; set; }
        public string TotalCost { get; set; }
    }

    public class TimesheetGrid_Table
    {
        public List<TimesheetGrid> results { get; set; }
    }

    public class SelfTimeSheet
    {
        public List<TimesheetGrid> TimsheetGridData { get; set; }
        public string Totaltime { get; set; }
    }
    public class SelfTimeSheet_Table
    {
        public SelfTimeSheet results { get; set; }
    }

    public class AdminViewTimesheet
    {
        [Display(Name = "Office")]
        public int OfficeId { get; set; }
        [Display(Name = "From Date")]
        public string FromDate { get; set; }
        [Display(Name = "To Date")]
        public string ToDate { get; set; }

        [Display(Name = "Client Name")]
        public long ClientId { get; set; }
        public string ClientName { get; set; }

        [Display(Name = "Employee Name")]
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }

        [Display(Name = "Activity")]
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }

        [Display(Name = "SubActivity")]
        public int SubActivityId { get; set; }
        public string SubActivityName { get; set; }

        [Display(Name = "Designation")]
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        [Display(Name = "Date")]
        public string Date { get; set; }
        public string TotalTime { get; set; }
        public string Remarks { get; set; }
        public string CostHr { get; set; }
        public string TotalCost { get; set; }
    }

    public class CalFilledTimeSheet
    {
        public List<Casualtimesheet> CasualTimeList { get; set; }
    }
    public class CalFilledTimeSheet_Table
    {
        public CalFilledTimeSheet results { get; set; }
    }
    public class Casualtimesheet
    {

        public string DateofCal { get; set; }

        public string Day { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }

        public string FDay { get; set; }

        public string FMonth { get; set; }

        public string FYear { get; set; }

        public string TDay { get; set; }

        public string TMonth { get; set; }

        public string TYear { get; set; }

        public string istimesheetfill { get; set; }


        public string Other { get; set; }

        public string Remarks { get; set; }

        public string Newdates { get; set; }

    }

    public class AdminTimeSheetOfUsers
    {
        public List<AdminViewTimesheet> TimeSheetGrid { get; set; }
        public string Totaltime { get; set; }
        public string TotalCost { get; set; }
    }
    public class AdminTimeSheetOfUsers_Table
    {
        public AdminTimeSheetOfUsers results { get; set; }
    }

    public class Leave_Model
    {
        public long UpdateID { get; set; }

        [Display(Name = "From Date")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        public string ToDate { get; set; }

        [Display(Name = "Remark")]
        public string Remarks { get; set; }

        [Display(Name = "No Of Days")]
        public string NoOfDays { get; set; }        
        public string IPAddress { get; set; }
        [Display(Name = "Office")]
        public int OfficeId { get; set; }

        [Display(Name = "No Of Days")]
        public long UserId { get; set; }
        public string UserName { get; set; }

        [Display(Name = "Client")]
        public long ClientId { get; set; }
    }

    public class LeaveData
    {
        public long LId { get; set; }
        public string Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string DayCount { get; set; }
        public string Remarks { get; set; }
        public string LeaveRequestStatus { get; set; }
        public int LeaveStatus { get; set; }
        public string TotalLeaves { get; set; }
        public long UserId { get; set; }
    }
    public class LeaveData_Table
    {
        public List<LeaveData> results { get; set; }
    }

    public class MISReport_Model
    {
        public List<MISReportGrid> ReportGridData { get; set; }
        public string Totaltime { get; set; }
    }
    public class MISReportGrid
    {
        public string Name { get; set; }
        public string Hours { get; set; }
    }

    public class MISReport_Model_Table
    {
        public MISReport_Model results { get; set; }
    }


    public class graphdata
    {
        public string graphjs { get; set; }
        public string totaltime { get; set; }
        public List<SelectListItem> Graphdetails { get; set; }
    }
}
