﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class APIReturn_Model
    {
        public string results { get; set; }
    }


    public class APIReturnlist_Model
    {
        public List<string> results { get; set; }
    }
    public class APIReturnWithStatus_Model
    {
        public ReturnModel results { get; set; }
    }

    public class ReturnModel
    {
        public string ReturnStatus { get; set; }
        public string ReturnMessage { get; set; }

        public long Id { get; set; }
    }
}
