﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Models
{
    public class Login_Model
    {
        [Required(ErrorMessage = "User ID can't be blank")]
        public string UserName { get; set; }


        public string Password { get; set; }


        public string OTP { get; set; }


        //[Required(ErrorMessage = "IPAddress can't be blank")]
        public string IPAddress { get; set; }

        public string MacAddress { get; set; }
        public int IsSameMac { get; set; }

        public string HDReturnurl { get; set; }


        public int UserId { get; set; }

        public string Name { get; set; }

        public int Vertical { get; set; }

        public int Role { get; set; }



    }
}
