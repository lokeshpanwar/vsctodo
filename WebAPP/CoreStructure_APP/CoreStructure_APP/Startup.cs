﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Repository;
//using Doconut.Viewer.Middleware;
//using GleamTech.AspNet.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Rotativa.AspNetCore;

namespace CoreStructure_APP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Doconut Configuration
            services.AddHttpContextAccessor();                     

            services.AddMemoryCache();
            // doconut end

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //services.AddGleamTech();

            services.Configure<CookieTempDataProviderOptions>(options =>
            {
                options.Cookie.IsEssential = true;
            });


            services.Configure<DBModel>(Configuration.GetSection("ConnectionStrings"));


            services.AddSession(options =>
            {
                options.Cookie.Name = ".PIB.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(10);
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddHttpContextAccessor();

            //services.AddTransient<MenuMasterService, MenuMasterService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            //    app.UseExceptionHandler("/ErrorPage/AppError");
            }
            else
            {
              //  app.UseExceptionHandler("/ErrorPage/AppError");
            }
            //app.UseGleamTech();

            app.UseStaticFiles();




          

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), @"Content")),
                RequestPath = "/Content"
            });

            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //Path.Combine(Directory.GetCurrentDirectory(), @"UserImages")),
            //    RequestPath = "wwwroot/UserImages"
            //});


            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //Path.Combine(Directory.GetCurrentDirectory(), @"querydata")),
            //    RequestPath = "wwwroot/querydata"
            //});


            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //Path.Combine(Directory.GetCurrentDirectory(), @"policydata")),
            //    RequestPath = "wwwroot/policydata"
            //});



            app.UseCookiePolicy();
            app.UseSession();


            //app.Run(async (context) => {
            //    await context.Response.WriteAsync(System.Diagnostics.Process.GetCurrentProcess().ProcessName);

            //});


            // doconut 
          //  app.MapWhen(
          //context => context.Request.Path.ToString().EndsWith("DocImage.axd"),
          //appBranch =>
          //{
          //    appBranch.UseDoconut();
          //});


            app.UseMvc(routes =>
            {
                //       routes.MapRoute(
                //name: "areaRoute",
                //template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
         name: "areaRoute",
         template: "{area:exists}/{controller}/{action}/{id?}");

                //    routes.MapRoute(
                //        name: "default",
                //        template: "{controller=Auth}/{action=Login}/{id?}");
                //});

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Auth}/{action=Login}");
            });

           // RotativaConfiguration.Setup(env);

            ConnectionString = Configuration["ConnectionStrings:DbConn"];
			BaseURL = Configuration["ConnectionStrings:BaseURL"];
            Export1 = Configuration["Export1"];
            Export2 = Configuration["Export2"];
            EncryptionKey = Configuration["EncryptionKey"];
        }



        public static string ConnectionString { get; private set; }
        public static string BaseURL { get; private set; }
        public static string Export1 { get; private set; }
        public static string Export2 { get; private set; }
        public static string EncryptionKey { get; private set; }
    }
}
