﻿using CoreStructure_APP.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace CoreStructure_APP.Utility
{
    public class SessionAuthorizeFilter : ActionFilterAttribute
    {
        Global_Repository objGRepo = new Global_Repository();
        long RoleId = 0;
        string FormName = "";

        private readonly IHttpContextAccessor _httpContextAccessor;

        public SessionAuthorizeFilter(string Form = "")
        {
            FormName = Form;
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var controllerName = filterContext.RouteData.Values["controller"];
                var actionName = filterContext.RouteData.Values["action"];
                var message = String.Format("{0} controller:{1} action:{2}", "onactionexecuting", controllerName, actionName);

                var cookieValueFromContext = filterContext.HttpContext.Request.Cookies["Token"];


                if (cookieValueFromContext == null)
                {
                    cookieValueFromContext = "";
                }
                if (cookieValueFromContext != "")
                {
                    RoleId = Convert.ToInt64(filterContext.HttpContext.Request.Cookies["URole"]);
                    if (filterContext.HttpContext.Request.Cookies["UType"] == "ADMIN")
                    {
                        FormName = "";
                    }
                    if (FormName != "")
                    {                         
                        //if (objGRepo.CheckIfModulePermissionExistsForRole(RoleId, FormName, filterContext.HttpContext.Request.Cookies["Token"]) == "")
                        //{

                        //}
                        //else
                        //{
                        //    string redirectTo = "~/Auth/InValidUserRole";
                        //    ActionResult result = new RedirectResult(redirectTo);
                        //    filterContext.Result = result;
                        //}
                    }
                }
                else
                {
                    string redirectTo = "~/Auth/Login";
                    ActionResult result = new RedirectResult(redirectTo);
                    filterContext.Result = result;
                }
            }
            catch (Exception ex)
            {
                filterContext.Result = new ForbidResult();
            }

            base.OnActionExecuting(filterContext);
        }
    }


}
