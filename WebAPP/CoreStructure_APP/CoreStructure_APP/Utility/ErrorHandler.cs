﻿using CoreStructure_APP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Utility
{
    public class ErrorHandler
    {

        public static void LogError(Exception ex, string Token, string ExceptionFrom, string ErrorPage)
        {
            try
            {
                Exception_Error_Model objModel = new Exception_Error_Model();
                objModel.created_by = "";
                objModel.error_page = ErrorPage;
                objModel.exception_from = ExceptionFrom;
                objModel.exception_message = ex.Message;
                objModel.inner_exception = ex.Source;
                objModel.stack_trace = ex.StackTrace;
                
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ErrorLog/InsertErrorLog", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        //result = deserialized.results;
                    }
                    else
                    {
                        //result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
            }
            finally
            {
            }
        }

    }
}
