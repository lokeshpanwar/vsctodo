﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Utility
{
    public class Standard
    {
        public static class StandardDateTime
        {
            public static DateTime GetDateTime()
            {
                DateTime utcTime = DateTime.UtcNow;
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);

                return localTime;
            }

        }
        public static class StandardList
        {
            public static int GetListDefaultRowsPerPage()
            {
                int rows = 10;
                return rows;
            }
        }
        public static class StandardDBSelectionNameForPO
        {
            public static string SL = "SL";
            public static string SL_FullName = "SRI LANKA";
            public static string BD = "BD";
            public static string BD_FullName = "BANGLADESH";
        }
        public static class StandardGridTypeForPO
        {
            public static string Grid1 = "DIV1";
            public static string Grid2 = "DIV2";
            public static string Grid3 = "DIV3";
        }
        public static class StandardUserType
        {
            public static string ADMIN = "Admin";
            public static string SuperAdmin = "SuperAdmin";
            public static string Employee = "Employee";
            public static string DC = "DC";
            public static string POS = "POS";
            public static string USER = "USER";
            public static string subsuperadmin = "Subsuperadmin";
        }
    }
}
