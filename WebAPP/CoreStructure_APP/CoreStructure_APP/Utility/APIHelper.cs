﻿using CoreStructure_APP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_APP.Utility
{
    public class APIHelper
    {

        public static string Connection()
        {
            string connString = Startup.ConnectionString;

            return connString;
        }
        
        public static string BaseURL()
        {
            string connString = Startup.BaseURL;

            return connString;
        }
    }
}
