﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Repository
{
    public class DropDown_Repository
    {
        public DropDownReturn_Model_API GetThemeList(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetThemeList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetThemeList");
            }

            return result;
        }
        public List<SelectListItem> RowsPerPage()
        {
            List<SelectListItem> objRowsList = new List<SelectListItem>();

            //objRowsList.Add(new SelectListItem
            //{
            //    Text = "5",
            //    Value = "5"
            //});
            objRowsList.Add(new SelectListItem
            {
                Text = "10",
                Value = "10"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "20",
                Value = "20"
            });
            objRowsList.Add(new SelectListItem
            {
                Text = "50",
                Value = "50"
            });
            return objRowsList;
        }
        public DropDownReturn_Model_API GetDomainList(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetDomainList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetDomainList");
            }

            return result;
        }
        public DropDownReturn_Model_API GetStateList(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetStateList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }
        public DropDownReturn_Model_API GetOfficeList(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetOfficeListForReport").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetOfficeList");
            }

            return result;
        }
        public DropDownReturn_Model_API GetOfficeListActive(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetOfficeListActive").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetOfficeListActive");
            }

            return result;
        }
        public DropDownReturn_Model_API GetRoleList(string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetRoleList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetRoleList");
            }

            return result;
        }
        public DropDownReturn_Model_API GetCityListByStateId(int stateId, string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetCityListByStateId?StateId=" + stateId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetCityListByStateId");
            }

            return result;
        }
        public DropDownReturn_Model_API GetCategoryList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetCategoryList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetCategoryList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetInsuranceCompanyList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetInsuranceCompanyList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetInsuranceCompanyList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetContactPersonList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetContactPersonList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetContactPersonList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetPolicyTypeList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetPolicyTypeList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetPolicyTypeList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetGroupList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetGroupList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetGroupList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetIssuingOfficeList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetIssuingOfficeList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetIssuingOfficeList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetProductList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetProductList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetProductList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetClientList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetClientList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetClientList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetIssuingOfcListbyInsCoId(int insuranceCompanyId, string Token, int officeid)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetIssuingOfcListbyInsCoId?insuranceCompanyId=" + insuranceCompanyId + "&officeid="+ officeid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetIssuingOfcListbyInsCoId");
            }

            return result;
        }
        public DropDownReturn_Model_API GetCategorylistListbyInsCoId(int insuranceCompanyId,int catid, string Token, string officeid)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetCategorylistListbyInsCoId?insuranceCompanyId=" + insuranceCompanyId + "&officeid=" + officeid + "&Catid="+ catid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetIssuingOfcListbyInsCoId");
            }

            return result;
        }
        public DropDownReturn_Model_API GetIssuingofficeByInsco(int insuranceCompanyId, string Token, string officeid)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetIssuingofficeByInsco?insuranceCompanyId=" + insuranceCompanyId + "&officeid=" + officeid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetIssuingofficeByInsco");
            }

            return result;
        }
        public DropDownReturn_Model_API GetContactPersonListWithMobile(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetContactPersonListWithMobile?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetContactPersonListWithMobile");
            }
            return result;
        }
        public DropDownReturn_Model_API GetUserListByRoleId(int RoleId, string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetUserListByRoleId?RoleId=" + RoleId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetUserListByRoleId");
            }
            return result;
        }
        public PolicyCount_API GetTotalCountPolicy(string Token)
        {
            PolicyCount_API result = new PolicyCount_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetTotalPolicyList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<PolicyCount_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetTotalCountPolicy");
            }

            return result;
        }
        public PolicyCount_API GetTotalRenewalPolicy(string Token)
        {
            PolicyCount_API result = new PolicyCount_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetTotalRenewalPolicy").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<PolicyCount_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetTotalCountPolicy");
            }

            return result;
        }
        public DropDownReturn_Model_API GetAllPolicyNoList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetAllPolicyNoList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetAllPolicyNoList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetPolicyholderList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetPolicyholderList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetContactPersonList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetClaimStepList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetClaimStepList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetClaimStepList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetEmployeeList(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetEmployeeList?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetEmployeeList");
            }
            return result;
        }
        public DropDownReturn_Model_API GetInsuranceCompanyListForTPSurveyor(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetInsuranceCompanyListForTPSurveyor?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetInsuranceCompanyListForTPSurveyor");
            }
            return result;
        }
        public DropDownReturn_Model_API GetActivityListForDropDown(string Token, string OfficeId)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetActivityListForDropDown?OfficeId=" + Convert.ToInt32(OfficeId)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetActivityListForDropDown");
            }
            return result;
        }
        public DropDownReturn_Model_API GetSubActivityListByActivityId(int ActivityId, string Token)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/DropDown/GetSubActivityListByActivityId?ActivityId=" + ActivityId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetSubActivityListByActivityId");
            }
            return result;
        }

    }
}
