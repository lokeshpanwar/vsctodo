﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Repository
{
    public class Attorneys_Repository
    {
        public VSCCO_Model_table Clientlist(string Token, int officeid)
        {
            VSCCO_Model_table result = new VSCCO_Model_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Clientlist?officeid=" + officeid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCCO_Model_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/clientlis");
            }

            return result;
        }


        public VSCActivity_table Activity(string Token, int OfficeId)
        {
            VSCActivity_table result = new VSCActivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Activity?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCActivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/Activitylist");
            }

            return result;
        }


        public VSCSubactivity_table SubActivity(string Token, int OfficeId, int Activityid)
        {
            VSCSubactivity_table result = new VSCSubactivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/SubActivity?OfficeId=" + OfficeId + "&Activityid=" + Activityid).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCSubactivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "VSCCO_Repository/SubActivity");
            }

            return result;
        }



        public VSCActivity_table Userlist(string Token, int OfficeId)
        {
            VSCActivity_table result = new VSCActivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Userlist?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCActivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/Activitylist");
            }

            return result;
        }

        public string filltimesheet(string Token, int empid, string date, string time, string description, int clientid, int activityid, int subactivityid, int projectid,int todoid, int StatusId)
        {
            string result = "Error on fill timesheet from Task !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Filltimesheet?empid=" + empid + "&date=" + date + "&time=" + time + "&description=" + description + "&clientid=" + clientid + "&activityid=" + activityid + "&subactivityid=" + subactivityid + "&projectid="+ projectid+ "&todoid="+ todoid + "&StatusId="+ StatusId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "PTS_Repository/filltimesheet");
            }
            return result;
        }


        public VSCActivity_table Projectname(string Token, int Clientid)
        {
            VSCActivity_table result = new VSCActivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Projectname?Clientid=" + Clientid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCActivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/Projectname");
            }

            return result;
        }

        public VSCUserData_Table UserData(string Token, int Userid)
        {
            VSCUserData_Table result = new VSCUserData_Table();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/AttorneysUserData?Userid=" + Userid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCUserData_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

        public VSCActivity_table Category(string Token, int OfficeId)
        {
            VSCActivity_table result = new VSCActivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/Category?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCActivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/Activitylist");
            }

            return result;
        }

        public VSCSubactivity_table SubCategory(string Token, int OfficeId, int Categoryid)
        {
            VSCSubactivity_table result = new VSCSubactivity_table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Attorneys/SubCategory?OfficeId=" + OfficeId + "&Categoryid=" + Categoryid).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<VSCSubactivity_table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "VSCCO_Repository/SubCategory");
            }

            return result;
        }
    }
}
