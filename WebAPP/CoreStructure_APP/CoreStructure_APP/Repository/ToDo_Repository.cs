﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Repository
{
    public class ToDo_Repository
    {
        public Task_Model_API GetPriorityList(string Token)
        {
            Task_Model_API result = new Task_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetPriorityList").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }


        public Task_Model_API GetStatuslist(string Token)
        {
            Task_Model_API result = new Task_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetStatuslist").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }



        public Task_Model_API VerticalList(string Token,int EmpId,int Vertical)
        {
            Task_Model_API result = new Task_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/VerticalList?Userid=" + EmpId + "&VerticalId=" + Vertical).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

       

        public Task_Model_API VerticalListUserwise(string Token, int EmpId, int Vertical)
        {
            Task_Model_API result = new Task_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/VerticalListUserwise?Userid=" + EmpId + "&VerticalId=" + Vertical).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }



        public string SaveTask(Task_Model objModel, string Token)
        {
            string result = "Error on Inserting Group!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/SaveTask", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "ToDo/SaveTask");
            }
            return result;
        }

        public List<Task_Model> GetViewTaskList(string Token,int UserId)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetViewTaskList?UserId=" + UserId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

     

        public string UpdateTask(Task_Model objModel, string Token)
        {
            string result = "Error on Updating Contact Person!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/SaveTask", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Masters/UpdateContactPerson");
            }
            return result;
        }

        public string DeleteTask(int TaskId, string Token)
        {
            string result = "Error on Deleting Task !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/DeleteTask?TaskId=" + TaskId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Master_Repository/DeleteContactPerson");
            }
            return result;
        }
        public Task_Table GetTodoList(string Token, int EmpId,int Vertical)
        {
            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetTodoList?Userid=" + EmpId + "&VerticalId=" + Vertical).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

        public Task_Table GetTodoDetailList(string Token, int Id)
        {
            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetTodoDetailList?Id=" + Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }


        public Task_Table GetAlltaskdetails(string Token, int Id)
        {
            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetAlltaskdetails?Id=" + Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }



        public Task_Table Updatetodo(int todoid, string date, string time, int statusId, string description,int empid, string token)
        {
           
            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", token);
                    var response = client.GetAsync("api/ToDo/Updatetodo?todoid=" + todoid + "&date="+date + "&time="+ time+ "&statusId="+ statusId+ "&description="+ description + "&empid="+ empid).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, token, "API", "Todo_Repository/Updatetodo");
            }

            return result;
        }


        public Task_Table AcceptTask(int todoid, int Clientid, int Activityid, int Subactivityid, int projectid, int moduleid,int vertical, string token)
        {

            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", token);
                    var response = client.GetAsync("api/ToDo/AcceptTask?todoid=" + todoid + "&Clientid=" + Clientid + "&Activityid=" + Activityid + "&Subactivityid=" + Subactivityid + "&projectid=" + projectid + "&moduleid=" + moduleid + "&vertical="+ vertical).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, token, "API", "Todo_Repository/Updatetodo");
            }

            return result;
        }

        public Task_Table RejectTask(int todoid, string RejectRemark, string token)
        {

            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", token);
                    var response = client.GetAsync("api/ToDo/RejectTask?todoid=" + todoid + "&RejectRemark=" + RejectRemark).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, token, "API", "Todo_Repository/Updatetodo");
            }

            return result;
        }
        public Task_Model_API Employeelist(string Token, int Verticals)
        {
            Task_Model_API result = new Task_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetEmployeelist?Verticalid=" + Verticals).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

        public string SaveAssignTask(Task_Model objModel, string Token)
        {
            string result = "Error on Inserting Group!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/SaveAssignTask", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "ToDo/SaveTask");
            }
            return result;
        }

        public string SaveAssignTaskDetails(Task_Model model,string Token)
        {
            string result = "Error on Inserting Group!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/SaveAssignTaskDetails", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "ToDo/SaveTask");
            }
            return result;
        }

        public string GetTotalTasks(int Verticals, int EmpId,string Token)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetTotalTasks?Verticalid=" + Verticals +"&EmpId="+EmpId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

        public List<Task_Model> ViewTotalTaskDetails(string Token, int VerticalId, int EmpId)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/ViewTotalTaskDetails?Verticalid=" + VerticalId + "&EmpId=" + EmpId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

        public Task_Table GetTodoListFilterWise(string Token, int EmpId, int Vertical)
        {
            Task_Table result = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetTodoListFilterWise?Userid=" + EmpId + "&VerticalId=" + Vertical).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "DropDown_Repository/GetStateList");
            }

            return result;
        }

        public List<Task_Model> GetReassignDataList(string Token, int OfficeId,int Id)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetReassignDataList?Id=" + Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

        public string SaveReAssignTaskDetails(Task_Model model, string Token)
        {
            string result = "Error on Inserting Group!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/SaveReAssignTaskDetails", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "ToDo/SaveTask");
            }
            return result;
        }

       
        public List<Task_Model> GetEditTaskList(string Token, int UserId,int TaskId)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetEditTaskList?UserId=" + UserId + "&TaskId=" + TaskId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

       

        public List<Task_Model> GetEditAssignTaskList(string Token, int UserId, int TaskId)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetEditAssignTaskList?UserId=" + UserId + "&TaskId=" + TaskId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

        public string UpdateAssignTaskDetails(Task_Model model, string Token)
        {
            string result = "Error on Inserting Group!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/ToDo/UpdateAssignTaskDetails", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "ToDo/SaveTask");
            }
            return result;
        }


        public List<Task_Model> GetUserDetail(string Token, string AssignTo)
        {
            List<Task_Model> result = new List<Task_Model>();
            Task_Table result1 = new Task_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/ToDo/GetUserDetail?UserId=" + AssignTo).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Task_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Master_Repository/GetContactPersonList");
            }
            return result;
        }

    }
}
