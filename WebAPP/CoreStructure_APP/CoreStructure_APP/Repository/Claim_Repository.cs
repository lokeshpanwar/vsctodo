﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;

namespace CoreStructure_APP.Repository
{
    public class Claim_Repository
    {
        public List<Policy> GetPolicyListForClaim(string Token, int OfficeId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/GetPolicyListForClaim?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Claim_Repository/GetPolicyListForClaim");
            }
            return result;
        }

        public string SavePALodgeClaimDetails(LodgeClaim_Model objModel, string Token)
        {
            string result = "Error on Claim Lodge!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Claim/SavePALodgeClaimDetails", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Claim_Repository/SavePALodgeClaimDetails");
            }
            return result;
        }
        public string SaveMedicalClaimDetails(LodgeClaim_Model objModel, string Token)
        {
            string result = "Error on Claim Lodge!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Claim/SaveMedicalClaimDetails", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Claim_Repository/SaveMedicalClaimDetails");
            }
            return result;
        }

        public List<ViewLodgeClaim_Model> GetClaimedPolicyList(string Token, int OfficeId)
        {
            List<ViewLodgeClaim_Model> result = new List<ViewLodgeClaim_Model>();
            ViewLodgeClaim_Model_Table result1 = new ViewLodgeClaim_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/GetClaimedPolicyList?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<ViewLodgeClaim_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Claim_Repository/GetClaimedPolicyList");
            }
            return result;
        }

        public string DeleteClaim(long ClaimId, string Token)
        {
            string result = "Error on Deleting Claim !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/Claim/DeleteClaim?ClaimId=" + ClaimId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/DeleteClaim");
            }
            return result;
        }

        public List<LodgeClaim_Model> GetLodgeClaimDataForEdit(string Token, int OfficeId, long ClaimId)
        {
            List<LodgeClaim_Model> result = new List<LodgeClaim_Model>();
            LodgeClaim_Model_Table result1 = new LodgeClaim_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/GetLodgeClaimDataForEdit?OfficeId=" + OfficeId + "&ClaimId=" + ClaimId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<LodgeClaim_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Claim_Repository/GetLodgeClaimDataForEdit");
            }
            return result;
        }

        public string SaveClaimStepsDetails(string Token, long ClaimId, int StepId, string StepDate, long EmpId, string SettleAmount, string SettleDate, string AppRemark, string RejectRemark, string PendingRemark, int ClStatusId)
        {
            string result = "Error !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/SaveClaimStepsDetails?ClaimId=" + ClaimId + "&StepId=" + StepId + "&StepDate=" + StepDate + "&EmpId=" + EmpId + "&SettleAmount=" + SettleAmount + "&SettleDate=" + SettleDate + "&AppRemark=" + AppRemark + "&RejectRemark=" + RejectRemark + "&PendingRemark=" + PendingRemark + "&ClStatusId=" + ClStatusId).Result;
                   
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Claim_Repository/SaveClaimStepsDetails");
            }
            return result;
        }

        public List<ClaimStepsDetails_Model> GetClaimStepsDetailList(string Token, long ClaimId)
        {
            List<ClaimStepsDetails_Model> result = new List<ClaimStepsDetails_Model>();
            ClaimStepsDetails_Model_Table result1 = new ClaimStepsDetails_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/GetClaimStepsDetailList?ClaimId=" + ClaimId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<ClaimStepsDetails_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Claim_Repository/GetClaimStepsDetailList");
            }
            return result;
        }

        public List<TP_Surveyor> GetTPSurveyorForLodgeClaim(string Token, int OfficeId, long PId)
        {
            List<TP_Surveyor> result = new List<TP_Surveyor>();
            TP_Surveyor_Table result1 = new TP_Surveyor_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Claim/GetTPSurveyorForLodgeClaim?OfficeId=" + OfficeId + "&PId=" + PId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<TP_Surveyor_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Claim_Repository/GetTPSurveyorForLodgeClaim");
            }
            return result;
        }
    }
}
