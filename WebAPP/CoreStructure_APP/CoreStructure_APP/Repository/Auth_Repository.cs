﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Repository
{
    public class Auth_Repository
    {

        public LoginReturn_Model_API CheckLogin(Login_Model model)
        {
            LoginReturn_Model_API objModel = new LoginReturn_Model_API();
            LoginReturn_Model objlogModel = new LoginReturn_Model();
            try
            {
                if(model.IPAddress==null)
                {
                    model.IPAddress = "";
                }

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    var response = client.PostAsJsonAsync("api/Auth/Login", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //result = "Success";
                        var obj = response.Content.ReadAsStringAsync();
                        var deserialized = JsonConvert.DeserializeObject<LoginReturn_Model_API>(obj.Result);
                        objModel.results = deserialized.results;
                    }
                    else
                    {
                        objModel.Error = response.ReasonPhrase.ToString() + response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                objlogModel.LoginStatus = "Invalid Credentials";
                objModel.results = objlogModel;
                ErrorHandler.LogError(ex, "", "API", "AuthRepository/CheckLogin");
            }
            return objModel;
        }

        public OTP_Return_Value_API GetOTPForLogIn(Login_Model model)
        {
            OTP_Return_Value_API objModel = new OTP_Return_Value_API();
            OTP_Return_Value objlogModel = new OTP_Return_Value();
            try
            {
                if (model.IPAddress == null)
                {
                    model.IPAddress = "";
                }
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    var response = client.PostAsJsonAsync("api/Auth/GetOTPForLogIn", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //result = "Success";
                        var obj = response.Content.ReadAsStringAsync();
                        var deserialized = JsonConvert.DeserializeObject<OTP_Return_Value_API>(obj.Result);
                        objModel.results = deserialized.results;
                    }
                    else
                    {
                        objModel.Error = response.ReasonPhrase.ToString() + response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                objlogModel.LoginStatus = "Invalid Credentials";
                objModel.results = objlogModel;
                ErrorHandler.LogError(ex, "", "API", "AuthRepository/GetOTPForLogIn");
            }
            return objModel;
        }
        public RoleFormList_Result GetFormPermissionListFromUserID(long UserID)
        {
            RoleFormList_Result objModel = new RoleFormList_Result();
            LoginReturn_Model objlogModel = new LoginReturn_Model();
            try
            {
               
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    var response = client.GetAsync("api/Auth/GetFormPermissionListFromUserID?UserID="+ UserID).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //result = "Success";
                        var obj = response.Content.ReadAsStringAsync();
                        var deserialized = JsonConvert.DeserializeObject<RoleFormList_Result>(obj.Result);
                        objModel.results = deserialized.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                objlogModel.LoginStatus = "Invalid Credentials";
                ErrorHandler.LogError(ex, "", "API", "AuthRepository/GetFormPermissionListFromUserID");
            }
            return objModel;
        }

        public Users GetAdminProfileForView(string Token)
        {
            Users objModel = new Users();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Masters/GetAdminProfileForView").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Users_Profile>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Auth/GetAdminProfileForView");
            }
            return objModel;
        }
    }
}


