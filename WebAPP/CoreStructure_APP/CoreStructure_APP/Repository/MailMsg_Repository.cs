﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;

namespace CoreStructure_APP.Repository
{
    public class MailMsg_Repository
    {
        public MailMsg_Model GetEmailDataForAdmin()
        {
            MailMsg_Model result = new MailMsg_Model();
            MailMsg_Model_Table result1 = new MailMsg_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    var response = client.GetAsync("api/MailMsg/GetEmailDataForAdmin").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<MailMsg_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, "", "API", "MailMsg_Repository/GetEmailDataForAdmin");
            }
            return result;
        }

        public Msg_Model GetDailyMsgShootData()
        {
            Msg_Model result = new Msg_Model();
            Msg_Model_Table result1 = new Msg_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    var response = client.GetAsync("api/MailMsg/GetDailyMsgShootData").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Msg_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, "", "API", "MailMsg_Repository/GetDailyMsgShootData");
            }
            return result;
        }

        public string SaveSentMsg(Remind_Msg_Model model)
        {
            string result = "Error on Renew Policy!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    //client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/MailMsg/SaveSentMsg", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/SaveRenewPolicy");
            }
            return result;
        }
    }
}
