﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreStructure_APP.Repository
{
    public class SubCompany_Repository
    {
        public ReturnModel InsertSubCompany(Sub_Company_table_Model objModel, string Token)
        {
            string result = "Error on Inserting Sub Company!";
            ReturnModel objReturn = new ReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/SubCompany/NewSubCompany", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objReturn = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/InsertSubCompany");
            }

            return objReturn;
        }
        public List<Sub_Company_table_Model> GetSubCompanyList(string Token)
        {
            List<Sub_Company_table_Model> objModel = new List<Sub_Company_table_Model>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/SubCompany/ListSubCompany").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Sub_Company_table_Model_List>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/GetSubCompanyList");
            }

            return objModel;

        }
        public Sub_Company_table_Model GetOneSubCompany(long SLNO, string Token)
        {
            Sub_Company_table_Model objModel = new Sub_Company_table_Model();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/SubCompany/OneSubCompany?Id=" + SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Sub_Company_table_Model_One>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/GetOneSubCompany");
            }

            return objModel;

        }
        public ReturnModel UpdateSubCompany(Sub_Company_table_Model objModel, string Token)
        {
            string result = "Error on Updating SubCompany!";
            ReturnModel objReturn = new ReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PutAsJsonAsync("api/SubCompany/EditSubCompany", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objReturn = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/UpdateSubCompany");
            }

            return objReturn;
        }
        public string DisableSubCompany(long SLNO, string Token)
        {
            string result = "Error on Deleting Sub Company!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/SubCompany/DisableSubCompany?Id=" + SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/DeleteSubCompany");
            }

            return result;
        }
        public string EnableSubCompany(long SLNO, string Token)
        {
            string result = "Error on Enabling Sub Company!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/SubCompany/EnableSubCompany?Id=" + SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/EnableSubCompany");
            }

            return result;
        }
        public Sub_Company_table_page_Model_Result GetSubCompanyListByPage(string Token, int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            Sub_Company_table_page_Model_Result objModel = new Sub_Company_table_page_Model_Result();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/SubCompany/ListSubCompanyByPagination?max=" + max + "&page=" + page + "&search=" + search + "&sort_col=" + sort_col + "&sort_dir=" + sort_dir;
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Sub_Company_table_page_Model_Result>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/GetSubCompanyListByPage");
            }

            return objModel;

        }
        public ReturnModel InsertSubCompanyForBulk(List<Sub_Company_table_Model> objList, string Token)
        {
            string result = "Error on Inserting Sub Company!";
            ReturnModel objReturn = new ReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                Sub_Company_table_page_Model objModel = new Sub_Company_table_page_Model();
                objModel.Sub_Company_table_Model_List = objList;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/SubCompany/NewSubCompanyBulkUpload", objModel).Result;
                    //var response = client.PostAsJsonAsync("api/SubCompany/NewSubCompanyBulkUpload", data).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objReturn = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/InsertSubCompanyForBulk");
            }

            return objReturn;
        }
        public ReturnModel DeleteSubCompany(long SLNO, string Token)
        {
            string result = "Error on Deleting SubCompany!";
            ReturnModel objReturn = new ReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/SubCompany/DeleteSubCompany?ID="+ SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objReturn = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/DeleteSubCompany");
            }

            return objReturn;
        }
        public Sub_Company_table_Model GetOneParentSubCompany(string Token)
        {
            Sub_Company_table_Model objModel = new Sub_Company_table_Model();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/SubCompany/OneParentSubCompany").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Sub_Company_table_Model_One>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "SubCompany_Repository/GetOneParentSubCompany");
            }

            return objModel;

        }
    }
}
