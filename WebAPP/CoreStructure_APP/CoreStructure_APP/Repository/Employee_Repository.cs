﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace CoreStructure_APP.Repository
{
    public class Employee_Repository
    {
        public string InsertEmployee(Employee_table_Model objModel, string Token)
        {
            string result = "Error on Inserting Employee!";
            try
            {
                objModel = NullToBlank(objModel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Employee/NewEmployee", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/InsertEmployee");
            }

            return result;
        }
        public string DisableEmployee(long SLNO, string Token)
        {
            string result = "Error on Deleting Employee!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/Employee/DisableEmployee?Id=" + SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/DeleteEmployee");
            }

            return result;
        }
        public string EnableEmployee(long SLNO, string Token)
        {
            string result = "Error on Enabling Employee!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.DeleteAsync("api/Employee/EnableEmployee?Id=" + SLNO).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/EnableEmployee");
            }

            return result;
        }
        public Employee_table_page_Model_Result GetEmployeeListByPage(string Token, int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            Employee_table_page_Model_Result objModel = new Employee_table_page_Model_Result();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/Employee/ListEmployeeByPagination?max=" + max + "&page=" + page + "&search=" + search + "&sort_col=" + sort_col + "&sort_dir=" + sort_dir;
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employee_table_page_Model_Result>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/GetEmployeeListByPage");
            }

            return objModel;

        }
        public Employee_table_Model_One GetEmployeeById(long SLNO, string Token)
        {
            Employee_table_Model_One objModel = new Employee_table_Model_One();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/Employee/OneEmployee?Id=" + SLNO;
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employee_table_Model_One>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/GetEmployeeById");
            }

            return objModel;

        }
        public string UpdateEmployee(Employee_table_Model objModel, string Token)
        {
            string result = "Error on Updating Employee!";
            try
            {
                objModel = NullToBlank(objModel);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PutAsJsonAsync("api/Employee/EditEmployee", objModel).Result;                    
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/UpdateEmployee");
            }

            return result;
        }
        private Employee_table_Model NullToBlank(Employee_table_Model objModel)
        {
            if(objModel.Department==null)
            {
                objModel.Department = "";
            }
            if(objModel.Location==null)
            {
                objModel.Location = "";
            }
            if(objModel.CountryCode == null)
            {
                objModel.CountryCode = "";
            }
            if(objModel.Email == null)
            {
                objModel.Email = "";
            }
            if(objModel.Reporting_Manager_Id == null)
            {
                objModel.Reporting_Manager_Id = "";
            }
            objModel.CreatedDate = Standard.StandardDateTime.GetDateTime();
            objModel.UpdatedOn = Standard.StandardDateTime.GetDateTime();
            return objModel;
        }
        public List<Employee_Process_value_Model> GetEmployeeListForApproverLimitation(string Token, long SubCompId, long DomainId)
        {
            List<Employee_Process_value_Model> objModel = new List<Employee_Process_value_Model>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Employee/ListUserForApproverLimitation?SubCompId=" + SubCompId + "&Domain_ID="+DomainId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employee_Process_value_results>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "WebAPP", "Employee_Repository/GetEmployeeListForApproverLimitation");
            }

            return objModel;

        }
        public string InsertApproverLimitation(Employee_Process_value_list_Model objModel, string Token)
        {
            string result = "Error on Inserting Approver Limitation!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Employee/ListUserForApproverLimitation", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "WebAPP", "Employee_Repository/InsertApproverLimitation");
            }

            return result;
        }
        public List<Employee_table_Model> GetEmployeeList(string Token)
        {
            List<Employee_table_Model> objModel = new List<Employee_table_Model>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Employee/ListAllEmployee").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employee_table_Model_List>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "WebAPP", "Employee_Repository/GetEmployeeList");
            }

            return objModel;

        }
        public ReturnModel UpdateUserProfile(UserProfile_Model objModel, string Token)
        {
            ReturnModel objRetModel = new ReturnModel();
            string result = "Error on Updating User Profile!";
            objRetModel.ReturnStatus = "ERROR";
            objRetModel.ReturnMessage = result;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PutAsJsonAsync("api/Employee/EditUserProfile", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objRetModel = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/UpdateUserProfile");
            }

            return objRetModel;
        }
        public ReturnModel UpdateUserDelegate(User_Delegation_Table_Model objModel, string Token)
        {
            ReturnModel objRetModel = new ReturnModel();
            string result = "Error on Updating User Profile!";
            objRetModel.ReturnStatus = "ERROR";
            objRetModel.ReturnMessage = result;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PutAsJsonAsync("api/Employee/EditUserDelegate", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objRetModel = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/UpdateUserDelegate");
            }

            return objRetModel;
        }
        public ReturnModel EnableDisableUserDelegate(User_Delegation_Table_Model objModel, string Token)
        {
            ReturnModel objRetModel = new ReturnModel();
            string result = "Error on Updating User Delegate!";
            objRetModel.ReturnStatus = "ERROR";
            objRetModel.ReturnMessage = result;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PutAsJsonAsync("api/Employee/EnableDisableUserDelegate", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objRetModel = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/EnableDisableUserDelegate");
            }

            return objRetModel;
        }
        public string ChangeUserPassword(ResetPassword_Model objModel, string Token)
        {
            ReturnModel objRetModel = new ReturnModel();
            string result = "Error on Updating User Passowrd!";
            objRetModel.ReturnStatus = "ERROR";
            objRetModel.ReturnMessage = result;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Employee/ChangeUserPassword", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(APIHelper.Connection());
                //    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                //    var response = client.PostAsJsonAsync("api/Employee/ChangeUserPassword", objModel).Result;
                //    if (response.IsSuccessStatusCode)
                //    {
                //        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                //        objRetModel = deserialized.results;
                //    }
                //    else
                //    {
                //        result = response.StatusCode.ToString();
                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/ChangeUserPassword");
            }

            return result;
        }
        public User_Delegation_Table_Model GetUserDelegationById(long SLNO, string Token)
        {
            User_Delegation_Table_Model objModel = new User_Delegation_Table_Model();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/Employee/OneUserDelegation?Id=" + SLNO;
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<User_Delegation_Table_Model_One>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/GetUserDelegationById");
            }

            return objModel;

        }
        public List<Employees_Reporting> GetEmployeeReportingList(string Token, string empname)
        {
            List<Employees_Reporting> objModel = new List<Employees_Reporting>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Employee/ListAllEmployeeReporting?empnm="+empname).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employees_Reporting_List>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "WebAPP", "Employee_Repository/GetEmployeeReportingList");
            }

            return objModel;

        }
        public Employee_table_Model_One GetEmployeeByEmail(string Email, string Token)
        {
            Employee_table_Model_One objModel = new Employee_table_Model_One();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/Employee/OneEmployeeFromEmail?Email=" + Email;
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Employee_table_Model_One>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Employee_Repository/GetEmployeeByEmail");
            }

            return objModel;

        }
        public ReturnModel InsertEmployeeForBulk(List<Employee_table_Model> objList, string Token)
        {
            string result = "Error on Inserting Employee!";
            ReturnModel objReturn = new ReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                Employee_table_page_Model objModel = new Employee_table_page_Model();
                objModel.Employee_table_Model_List = objList;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Employee/NewEmployeeBulkUpload", objModel).Result;
                    //var response = client.PostAsJsonAsync("api/SubCompany/NewSubCompanyBulkUpload", data).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturnWithStatus_Model>(response.Content.ReadAsStringAsync().Result);
                        objReturn = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Employee_Repository/InsertEmployeeForBulk");
            }

            return objReturn;
        }
    }
}
