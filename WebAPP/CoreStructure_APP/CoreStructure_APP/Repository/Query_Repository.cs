﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;

namespace CoreStructure_APP.Repository
{
    public class Query_Repository
    {
        #region Query
        public string AddNewQuery(Query objModel, string Token)
        {
            string result = "Error on Inserting Query!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/AddNewQuery", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/AddNewQuery");
            }
            return result;
        }

        public List<Query> GetQueryList(string Token, int OfficeId)
        {
            List<Query> result = new List<Query>();
            Query_Table result1 = new Query_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetQueryList?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Query_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetQueryList");
            }
            return result;
        }

        public string UpdateQuery(Query objModel, string Token)
        {
            string result = "Error on Updating Query!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/AddNewQuery", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/AddNewQuery");
            }
            return result;
        }

        public string DeleteQuery(int QId, string Token)
        {
            string result = "Error on Deleting Query !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/DeleteQuery?QId=" + QId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/DeleteQuery");
            }
            return result;
        }

        public string CloseQueryUpdate(long QId, string Remark, string Token)
        {
            string result = "Error on Updating Status Query !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/CloseQueryUpdate?QId=" + QId + "&Remark=" + Remark).Result;
                    //client.BaseAddress = new Uri(APIHelper.Connection());
                    //client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    //var response = client.DeleteAsync("api/Query/CloseQueryUpdate?QId=" + QId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/CloseQueryUpdate");
            }
            return result;
        }

        public string ConvertQuery(long QId, string Token)
        {
            string result = "Error on Convert Query !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/ConvertQueryUpdate?QId=" + QId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/ConvertQueryUpdate");
            }
            return result;
        }
        public List<Query> GetClosedQueryList(string Token, int OfficeId)
        {
            List<Query> result = new List<Query>();
            Query_Table result1 = new Query_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetClosedQueryList?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Query_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetClosedQueryList");
            }
            return result;
        }
        public string PendingQueryRemarkUpdate(long QId, string Remark, string Token)
        {
            string result = "Error on Updating Status Query !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/PendingQueryRemarkUpdate?QId=" + QId + "&Remark=" + Remark).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/PendingQueryRemarkUpdate");
            }
            return result;
        }

        public List<Query> GetAllQueryListByUser(string Token, int OfficeId)
        {
            List<Query> result = new List<Query>();
            Query_Table result1 = new Query_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetAllQueryListByUser?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Query_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetAllQueryListByUser");
            }
            return result;
        }
        #endregion


        #region UnderWrite Query
        public List<UnderWrite> GetQueryDetailsForUnderWrite(string Token, string OfficeId, long QId)
        {
            List<UnderWrite> result = new List<UnderWrite>();
            UnderWrite_Table result1 = new UnderWrite_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetQueryDetailsForUnderWrite?OfficeId=" + OfficeId + "&QId=" + QId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<UnderWrite_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetQueryDetailsForUnderWrite");
            }
            return result;
        }
        public string SaveUnderWritePolicy(UnderWrite objModel, string Token)
        {
            string result = "Error on UnderWrite Query!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/SaveUnderWritePolicy", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/SaveUnderWritePolicy");
            }
            return result;
        }
        #endregion
        #region Policy

        public List<Policy> GetPolicyList(string Token, int OfficeId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetPolicyList?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetPolicyList");
            }
            return result;
        }
        public string SaveGeneratePolicy(Policy objModel, string Token)
        {
            string result = "Error on Generating Policy!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/SaveGeneratePolicy", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/SaveGeneratePolicy");
            }
            return result;
        }


        public string SaveRenewPolicy(Policy objModel, string Token)
        {
            string result = "Error on Renew Policy!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/SaveRenewPolicy", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/SaveRenewPolicy");
            }
            return result;
        }

        public List<Policy> GetDeletedPolicyList(string Token, int OfficeId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetDeletedPolicyList?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetDeletedPolicyList");
            }
            return result;
        }

        public List<Policy> GetMasterPolicyDataList(string Token, int OfficeId, string fDate, string tDate)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetMasterPolicyDataList?OfficeId=" + OfficeId + "&fDate=" + fDate + "&tDate=" + tDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetMasterPolicyDataList");
            }
            return result;
        }

        public List<Policy> GetPolicyDetailsForEdit(string Token, long PId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Query/GetPolicyDetailsForEdit?PId=" + PId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Query_Repository/GetPolicyDetailsForEdit");
            }
            return result;
        }

        public string SaveUpdatePolicy(Policy objModel, string Token)
        {
            string result = "Error on Updating Policy!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/Query/SaveUpdatePolicy", objModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "Query_Repository/SaveUpdatePolicy");
            }
            return result;
        }

        #endregion
    }
}
