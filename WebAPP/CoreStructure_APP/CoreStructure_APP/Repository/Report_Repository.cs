﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;

namespace CoreStructure_APP.Repository
{
    public class Report_Repository
    {
        public List<Policy> ViewPolicyReport(string Token, int officeId, int typeId, string FromDate, string ToDate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewPolicyReport?OfficeId=" + officeId + "&typeId=" + typeId + "&fromdate=" + FromDate + "&ToDate=" + ToDate + "&contactPersonId=" + contactPersonId + "&categoryId=" + categoryId + "&insuranceCompanyId=" + insuranceCompanyId + "&issuingOfficeId=" + issuingOfficeId + "&productId=" + productId + "&groupId=" + groupId + "&policyStatusId=" + policyStatusId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewPolicyReport");
            }
            return result;
        }


        public List<Policy> ViewPolicyReportExcel(string Token, int officeId, int hdTypeId, string hdfromdate, string hdtodate, int hdContactPersonId, int hdCategoryId, int hdInsuranceCompanyId, int hdIssuingOfficeId, int hdProductId, int hdGroupId, int hdPolicyStatusId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewPolicyReport?OfficeId=" + officeId + "&typeId=" + hdTypeId + "&fromdate=" + hdfromdate + "&ToDate=" + hdtodate + "&contactPersonId=" + hdContactPersonId + "&categoryId=" + hdCategoryId + "&insuranceCompanyId=" + hdInsuranceCompanyId + "&issuingOfficeId=" + hdIssuingOfficeId + "&productId=" + hdProductId + "&groupId=" + hdGroupId + "&policyStatusId=" + hdPolicyStatusId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewPolicyReport");
            }
            return result;
        }
        public DailyBusiness ViewAdminDailyBUsiness(string Token, int officeId, string fromDate, string toDate)
        {
            DailyBusiness result = new DailyBusiness();
            DailyBusiness_API result1 = new DailyBusiness_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/AdminDailyBusiness?OfficeId=" + officeId + "&fromdate=" + fromDate + "&ToDate=" + toDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DailyBusiness_API>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewAdminDailyBUsiness");
            }
            return result;
        }

        public DailyBusiness ViewAdminDailyBusinessClientWise(string Token, string fromDate, string toDate, int ClientId, string OrderBy, int OfficeId)
        {
            DailyBusiness result = new DailyBusiness();
            DailyBusiness_API result1 = new DailyBusiness_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewAdminDailyBusinessClientWise?fromdate=" + fromDate + "&todate=" + toDate + "&ClientId=" + ClientId + "&OrderBy=" + OrderBy + "&OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DailyBusiness_API>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }

            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewAdminDailyBusinessClientWise");
            }
            return result;
        }

        internal DataSet GetPolicyReportExportexcel(int typeId, string fromDate, string toDate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId)
        {
            throw new NotImplementedException();
        }

        public DailyBusiness ViewAdminDailyBusinessDayWise(string Token, int officeId, string fromDate, string toDate)
        {
            DailyBusiness result = new DailyBusiness();
            DailyBusiness_API result1 = new DailyBusiness_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewAdminDailyBusinessDayWise?OfficeId=" + officeId + "&fromdate=" + fromDate + "&ToDate=" + toDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DailyBusiness_API>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewAdminDailyBusinessDayWise");
            }
            return result;
        }

   

        public List<UpcomingPolicy> ViewUpcomingPolicy(string Token, int OfficeId, string CurrentDate, string Date)
        {
            List<UpcomingPolicy> result = new List<UpcomingPolicy>();
            UpcomingPolicy_API result1 = new UpcomingPolicy_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewUpcomingPolicy?OfficeId=" + OfficeId + "&fromdate=" + CurrentDate + "&ToDate=" + Date).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<UpcomingPolicy_API>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewUpcomingPolicy");
            }
            return result;
        }


        public List<Policy> AdminViewPolicyReport(string Token, int officeId, int typeId, string fromDate, string toDate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewPolicyReport?OfficeId=" + officeId + "&typeId=" + typeId + "&fromdate=" + fromDate + "&ToDate=" + toDate + "&contactPersonId=" + contactPersonId + "&categoryId=" + categoryId + "&insuranceCompanyId=" + insuranceCompanyId + "&issuingOfficeId=" + issuingOfficeId + "&productId=" + productId + "&groupId=" + groupId + "&policyStatusId=" + policyStatusId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/AdminViewPolicyReport");
            }
            return result;
        }

        public List<ClaimReport> ViewClaimReport(string Token, int officeId, string policyNo, int contactPersonId, int policyholderId)
        {
            List<ClaimReport> result = new List<ClaimReport>();
            Claim_Table result1 = new Claim_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewClaimReport?OfficeId=" + officeId + "&policyNo=" + policyNo + "&contactPersonId=" + contactPersonId + "&policyholderId=" + policyholderId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Claim_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewClaimReport");
            }
            return result;
        }

        public List<DashboardGraph> GetMonthlySalesForGraph(string Token)
        {
            List<DashboardGraph> result = new List<DashboardGraph>();
            DashboardGraph_Table result1 = new DashboardGraph_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/GetMonthlySalesForGraph").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DashboardGraph_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Report_Repository/GetMonthlySalesForGraph");
            }
            return result;
        }

        public List<Policy> ViewAdminRenewalReport(string Token, string Type, long officeId, string fromDate, string toDate)
        {
            List<Policy> result = new List<Policy>();
            Policy_Table result1 = new Policy_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/Report/ViewRenewalReport?OfficeId=" + officeId + "&Type=" + Type + "&Fromdate=" + fromDate + "&ToDate=" + toDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<Policy_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/ViewRenewalReport");
            }
            return result;
        }
    }
}
