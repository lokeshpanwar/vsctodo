﻿using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace CoreStructure_APP.Repository
{
    public class ServerLog_Repository
    {

        public ServerLog_Model GetServerLogList(string Token)
        {
            ServerLog_Model objModel = new ServerLog_Model();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    string api = "api/ServerLog/ListServerLog";
                    var response = client.GetAsync(api).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<ServerLog_Model_Result>(response.Content.ReadAsStringAsync().Result);
                        objModel = deserialized.results;
                    }
                    else
                    {
                        objModel = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "WebAPP", "ServerLog_Repository/GetServerLogList");
            }

            return objModel;

        }

    }
}
