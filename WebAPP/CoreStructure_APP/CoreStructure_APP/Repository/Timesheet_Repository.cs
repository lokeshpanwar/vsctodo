﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreStructure_APP.Models;
using CoreStructure_APP.Utility;
using Newtonsoft.Json;

namespace CoreStructure_APP.Repository
{
    public class Timesheet_Repository
    {
        #region TimeSheet
        public FilledTimesheetData Getfilledtimesheetdata(string Token)
        {
            FilledTimesheetData result = new FilledTimesheetData();
            FilledTimesheetData_API result1 = new FilledTimesheetData_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/Getfilledtimesheetdata").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<FilledTimesheetData_API>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Policy_Repository/Getfilledtimesheetdata");
            }
            return result;
        }

        public CalFilledTimeSheet GetfilledtimesheetdataByDate(string Token, string Date)
        {
            CalFilledTimeSheet result = new CalFilledTimeSheet();
            CalFilledTimeSheet_Table result1 = new CalFilledTimeSheet_Table();
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetfilledtimesheetdataByDate?Date=" + Date).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<CalFilledTimeSheet_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetfilledtimesheetdataByDate");
            }
            return result;
        }

        public string SaveTimesheet(Filltimesheet_Model model, string Token)
        {
            string result = "Error on Saving Timesheet!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/TimeSheet/SaveTimesheet", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet/SaveTimesheet");
            }
            return result;
        }

        public List<TimesheetGrid> GetFillTimesheetGridData(string Token, string Date)
        {
            List<TimesheetGrid> result = new List<TimesheetGrid>();
            TimesheetGrid_Table result1 = new TimesheetGrid_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetFillTimesheetGridData?Date=" + Date).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<TimesheetGrid_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetFillTimesheetGridData");
            }
            return result;
        }

        public string DeleteSheetData(long SheetId, string Token)
        {
            string result = "Error on Deleting Sheet Data !";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/DeleteSheetData?SheetId=" + SheetId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/DeleteSheetData");
            }

            return result;
        }

        public DropDownReturn_Model_API SubmitTimesheetCasual(string Token, string Date)
        {
            DropDownReturn_Model_API result = new DropDownReturn_Model_API();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/SubmitTimesheetCasual?Date=" + Date).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<DropDownReturn_Model_API>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/SubmitTimesheetCasual");
            }
            return result;
        }

        public SelfTimeSheet GetTimesheetSelfData(string Token, string fromDate, string toDate)
        {
            SelfTimeSheet result = new SelfTimeSheet();
            SelfTimeSheet_Table result1 = new SelfTimeSheet_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetTimesheetSelfData?Fromdate=" + fromDate + "&ToDate=" + toDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<SelfTimeSheet_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Timesheet_Repository/GetTimesheetSelfData");
            }
            return result;
        }

        public AdminTimeSheetOfUsers GetAdminTimesheetView(string Token, int OfficeId, string FromDate, string ToDate, long ClientId, long EmployeeId, int ActivityId, int SubActivityId, int RoleId)
        {
            AdminTimeSheetOfUsers result = new AdminTimeSheetOfUsers();
            AdminTimeSheetOfUsers_Table result1 = new AdminTimeSheetOfUsers_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetAdminTimesheetView?OfficeId=" + OfficeId + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&ClientId=" + ClientId + "&EmployeeId=" + EmployeeId + "&ActivityId=" + ActivityId + "&SubActivityId=" + SubActivityId + "&RoleId=" + RoleId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<AdminTimeSheetOfUsers_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Timesheet_Repository/GetAdminTimesheetView");
            }
            return result;
        }

        public SelfTimeSheet GetTimesheetDataTobeDelete(string Token, long EmployeeId, string Date)
        {
            SelfTimeSheet result = new SelfTimeSheet();
            SelfTimeSheet_Table result1 = new SelfTimeSheet_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetTimesheetDataTobeDelete?EmployeeId=" + EmployeeId + "&Date=" + Date).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<SelfTimeSheet_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Timesheet_Repository/GetTimesheetDataTobeDelete");
            }
            return result;
        }

        public string GetTimesheetDataDelete(string Token, long EmployeeId, string Date)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetTimesheetDataDelete?EmployeeId=" + EmployeeId + "&Date=" + Date).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/GetTimesheetDataDelete");
            }

            return result;
        }

        public string ApplyLeaveValidation(string Token, string Date)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/ApplyLeaveValidation?Date=" + Date).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/ApplyLeaveValidation");
            }

            return result;
        }

        public string GetHoldayName(string Token, string Date)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetHoldayName?Date=" + Date).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/GetHoldayName");
            }

            return result;
        }

        #endregion

        #region Leave Request
        public List<LeaveData> GetAllLeaveApplyDetails(string Token, int OfficeId)
        {
            List<LeaveData> result = new List<LeaveData>();
            LeaveData_Table result1 = new LeaveData_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetAllLeaveApplyDetails?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<LeaveData_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetAllLeaveApplyDetails");
            }
            return result;
        }

        public string SaveLeaveRequest(Leave_Model model, string Token)
        {
            string result = "Error on Saving Leave!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/TimeSheet/SaveLeaveRequest", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet/SaveLeaveRequest");
            }
            return result;
        }

        public List<LeaveData> GetLeaveDetailsForEdit(string Token, int OfficeId, long LId)
        {
            List<LeaveData> result = new List<LeaveData>();
            LeaveData_Table result1 = new LeaveData_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetLeaveDetailsForEdit?OfficeId=" + OfficeId + "&LId=" + LId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<LeaveData_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetLeaveDetailsForEdit");
            }
            return result;
        }

        public string DeleteLeaveRequestByID(string Token, long LId)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/DeleteLeaveRequestByID?LId=" + LId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/DeleteLeaveRequestByID");
            }

            return result;
        }

        #endregion

        #region Mark Leave
        public List<LeaveData> GetAllMarkLeaveDetails(string Token, int OfficeId)
        {
            List<LeaveData> result = new List<LeaveData>();
            LeaveData_Table result1 = new LeaveData_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetAllMarkLeaveDetails?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<LeaveData_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetAllMarkLeaveDetails");
            }
            return result;
        }

        public string SaveMarkLeave(Leave_Model model, string Token)
        {
            string result = "Error on Saving Leave!";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.PostAsJsonAsync("api/TimeSheet/SaveMarkLeave", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet/SaveMarkLeave");
            }
            return result;
        }
        #endregion

        #region Verify Leave
        public List<LeaveData> GetAllLeaveToVerifyDetails(string Token, int OfficeId)
        {
            List<LeaveData> result = new List<LeaveData>();
            LeaveData_Table result1 = new LeaveData_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetAllLeaveToVerifyDetails?OfficeId=" + OfficeId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<LeaveData_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "TimeSheet_Repository/GetAllLeaveToVerifyDetails");
            }
            return result;
        }

        public string ApproveLeaveRequest(string Token, long LId)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/ApproveLeaveRequest?LId=" + LId).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/ApproveLeaveRequest");
            }

            return result;
        }

        public string RejectLeaveRequest(string Token, long LId, string Remarks)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/RejectLeaveRequest?LId=" + LId + "&Remarks=" + Remarks).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/RejectLeaveRequest");
            }

            return result;
        }
        #endregion
        #region HR Report Sheet
        public string GetHrSheet(string Token, int ClientId, string FromDate, string ToDate)
        {
            string result = "0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetHrSheet?ClientId=" + ClientId + "&FromDate=" + FromDate + "&ToDate=" + ToDate).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<APIReturn_Model>(response.Content.ReadAsStringAsync().Result);
                        result = deserialized.results;
                    }
                    else
                    {
                        result = response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "Web App", "TimeSheet_Repository/GetHrSheet");
            }

            return result;
        }
        #endregion
        #region MIS Report
        public MISReport_Model GetHRsheetGraphEmployeewise(string Token, int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            MISReport_Model result = new MISReport_Model();
            MISReport_Model_Table result1 = new MISReport_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetHRsheetGraphEmployeewise?OfficeId=" + OfficeId + "&ClientId=" + ClientId + "&FromDate=" + FromDate + "&ToDate=" + ToDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<MISReport_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Timesheet_Repository/GetHRsheetGraphEmployeewise");
            }
            return result;
        }

        public MISReport_Model GetHRsheetGraphClientwise(string Token, int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            MISReport_Model result = new MISReport_Model();
            MISReport_Model_Table result1 = new MISReport_Model_Table();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIHelper.Connection());
                    client.DefaultRequestHeaders.Add("Authorization-Token", Token);
                    var response = client.GetAsync("api/TimeSheet/GetHRsheetGraphClientwise?OfficeId=" + OfficeId + "&ClientId=" + ClientId + "&FromDate=" + FromDate + "&ToDate=" + ToDate).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var deserialized = JsonConvert.DeserializeObject<MISReport_Model_Table>(response.Content.ReadAsStringAsync().Result);
                        result1 = deserialized;
                        result = result1.results;
                    }
                    else
                    {
                        //result = null;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Token, "API", "Timesheet_Repository/GetHRsheetGraphClientwise");
            }
            return result;
        }
        #endregion
    }
}
