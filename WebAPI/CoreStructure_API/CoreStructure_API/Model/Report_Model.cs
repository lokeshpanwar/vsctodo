﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Report_Model
    {
    }

    public class PolicyReport
    {
        public long PId { get; set; }
        public long QId { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }
        public string UnderWriteDate { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public string SubjectInsured { get; set; }


        public string CostCenter { get; set; }


        public string CommissionRate { get; set; }


        public string Premium { get; set; }


        public string CGST { get; set; }


        public string SGST { get; set; }


        public string PremiumPayable { get; set; }
        public int PolicyStatus { get; set; }
        public string PolicyStatusName { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }



        public string VehicleNo { get; set; }


        public string OnDamage { get; set; }


        public string PersonalAccident { get; set; }


        public string ThirdParty { get; set; }


        public string IDV { get; set; }


        public string PolicyDate { get; set; }


        public string ExpiryDate { get; set; }


        public string PolicyNo { get; set; }


        public string PreviousPolicyNo { get; set; }


        public string UploadFile { get; set; }


        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int RowsPerPage { get; set; }
        public string Search { get; set; }
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;
        public string SortCol { get; set; }
        public string SortOrder { get; set; }

        public decimal GST { get; set; }
        public string MobileNo { get; set; }
        public string TotalPremium { get; set; }

        public string TotalRenewal { get; set; }
        public string Processed { get; set; }
        public string Remaining { get; set; }



















    }
    public class DailyBusiness
    {

        public int OfficeId { get; set; }
        public string Office { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string InsCo { get; set; }
        public string Category { get; set; }
        public string PremiumPayable { get; set; }

        public string Percentage { get; set; }
        public string ClientName { get; set; }
        public List<DailyBusiness> ViewPolicy { get; set; }
        public List<DailyBusiness> ViewPolicy1 { get; set; }

        //public DayWiseData ListData { get; set; }

        public String QueryData { get; set; }
        public String TotalAmt { get; set; }
    }
    public class UpcomingPolicy
    {

        public int DaysId { get; set; }
        public string Days { get; set; }
        public string PolicyNo { get; set; }
        public string ExpiryDate { get; set; }
        public string Policyholdername { get; set; }
        public string ContactPersonName { get; set; }
        public string InsCompany { get; set; }
        public string IssuingOfc { get; set; }
        public decimal PremiumPayable { get; set; }
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CostCenterName { get; set; }
        public string VehicleNo { get; set; }
        public string MobileNo { get; set; }
        public List<UpcomingPolicy> ViewUpcomingpolicy { get; set; }
    }
    public class UpcomingPolicy_API
    {
        public UpcomingPolicy results { get; set; }


    }


    public class ClaimReport
    {
        [Display(Name = "Policy No")]
        public int PolicyNoId { get; set; }
        public string PolicyNo { get; set; }
        public List<SelectListItem> PolicyNoList { get; set; }
        [Display(Name = "CP Name")]
        public int ContactPersonId { get; set; }
        public string ContactPerson { get; set; }
        public List<SelectListItem> ContactPersonList { get; set; }
        [Display(Name = "PH Name")]
        public int PolicyholderId { get; set; }
        public string PolicyholderName { get; set; }
        public List<SelectListItem> PolicyholderList { get; set; }

        public string UnderwriteDate { get; set; }
        public string PolicyDate { get; set; }
        public string ClaimDate { get; set; }
        public string TPName { get; set; }
        public string CategoryName { get; set; }
        public string MobileNo { get; set; }
        public string ProductName { get; set; }
        public string InsCo { get; set; }
        public string ClaimAmount { get; set; }
        public string SettleAmount { get; set; }
        public string PremiumPayable { get; set; }
        public string CommissionRate { get; set; }
        public string IssuingOffice { get; set; }
        public string ExpiryDate { get; set; }
        public int TypeId { get; set; }
        public string PolicyStatus { get; set; }
        public string ClaimStatus { get; set; }
        public string SubjectInsured { get; set; }
        public string PatientName { get; set; }
        public int Pid { get; set; }
    }

    public class Claim_Table
    {
        public List<ClaimReport> results { get; set; }
    }

    public class DashboardGraph
    {
        public int Id { get; set; }
        public string MonthYear { get; set; }
        public string SaleAmount { get; set; }
    }
}
