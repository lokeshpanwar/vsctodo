﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Domain_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage = "Domain Name can't be blank!")]
        [MaxLength(100)]
        public string Domain_Name { get; set; }


        [Required(ErrorMessage = "Domain Description can't be blank!")]
        [MaxLength(250)]
        public string Domain_Description { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long DeletedBy { get; set; }


        public bool IsDeleted { get; set; }



        public DateTime DeletedOn { get; set; }


        [Required(ErrorMessage = "Center Code can't be blank!")]
        [MaxLength(200)]
        public string CenterCode { get; set; }

    }



    public class Domain_table_page_Model
    {
        public List<Domain_table_Model> Domain_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }


    public class Domain_Mapping_Display
    {
        public long Parent_CompId { get; set; }

        public long Maped_CompId { get; set; }

        public long Maped_By { get; set; }

        public List<Domain_table_Model_for_mapping> Domain_table_Model_List { get; set; }
    }


    public class Domain_table_Model_for_mapping
    {
        [Key]
        public long SLNO { get; set; }

        public string Domain_Name { get; set; }

        public bool IsSelected { get; set; } // Used in Domain Mapping

    }



}
