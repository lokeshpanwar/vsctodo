﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class BasicColor
    {
        public string EventColor { get; set; }
    }


    public class JsonEvent
    {
        public string id { get; set; }
        public string text { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public string backColor { get; set; }
    }
}
