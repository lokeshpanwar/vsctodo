﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class FunctionalDomainWiseReport_Model
    {
        [Display(Name = "SUB COMPANY")]
        [Required(ErrorMessage = "Sub Company can't be blank!")]
        public long SubCompId { get; set; }

        [Display(Name = "DOMAIN")]
        [Required(ErrorMessage = "Domain can't be blank!")]
        public long DomainId { get; set; }


        public long DocumentClassCount { get; set; }

        public long DocumentCount { get; set; }

        public long UserCount { get; set; }

        public long ACLTemplatesCount { get; set; }

        public List<DocumentClass_Model> DocumentClass_Model_List { get; set; }

        public List<Document_Model> Document_Model_List { get; set; }

        public List<User_Model> User_Model_List { get; set; }

    }


    public class DocumentClass_Model
    {
        public string DocumentClassName { get; set; }
        public long Documents { get; set; }
        public long ClassDomain { get; set; }
    }

    public class Document_Model
    {
        public string DocumentName { get; set; }
        public long DocumentSize { get; set; }
        public string DocumentVersion { get; set; }
    }

    public class User_Model
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ReportingManagerId { get; set; }
    }

}
