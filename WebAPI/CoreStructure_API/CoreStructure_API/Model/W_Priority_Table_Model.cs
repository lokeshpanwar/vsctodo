﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class W_Priority_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long SCoy { get; set; }


        public long FDom { get; set; }

        [Display(Name ="PRIORITY NAME")]
        [Required(ErrorMessage ="Priority Name cannot be blank!")]
        [MaxLength(100)]
        public string PriName { get; set; }


        [Display(Name = "DESCRIPTION")]
        [Required(ErrorMessage = "Priority Description cannot be blank!")]
        [MaxLength(500)]
        public string PriDesc { get; set; }



        [Display(Name = "PERCENTAGE")]
        [Required(ErrorMessage = "Priority Percentage cannot be blank!")]
        [MaxLength(50)]
        public string PriPer { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public bool IsDeleted { get; set;}


        public DateTime DeletedOn { get; set; }


        public long DeletedBy { get; set; }


    }


    public class W_Priority_Table_Page_Model
    {
        public List<W_Priority_Table_Model> W_Priority_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
