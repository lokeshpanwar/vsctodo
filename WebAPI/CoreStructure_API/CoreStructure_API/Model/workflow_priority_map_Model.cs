﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class workflow_priority_map_Model
    {
        [Key]
        public long SLNO { get; set; }


        [MaxLength(50)]
        public string BPMID { get; set; }


        public string StgID { get; set; }


        [MaxLength(50)]
        public string PriID { get; set; }


        public string Day { get; set; }


        public DateTime CreatedOn { get; set; }


        public string CreatedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


        public string DeletedBy { get; set; }

    }


    public class workflow_priority_map_Model_One
    {
        public List<workflow_priority_map_Model> workflow_priority_map_Model_List { get; set; }
    }
}
