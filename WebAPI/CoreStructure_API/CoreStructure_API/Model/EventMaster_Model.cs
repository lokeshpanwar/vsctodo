﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class EventMaster_Model
    {
        [Key]
        public long SLNO { get; set; }
        [Display(Name = "SUB COMPANY ")]
        public long Sub_CompID { get; set; }

        
        [Required(ErrorMessage = "EventTitle can't be blank!")]
        [MaxLength(100)]
        public string EventTitle { get; set;}

        [Required(ErrorMessage = "StartDate can't be blank!")]

        public DateTime StartDate { get; set; }

        [Required (ErrorMessage = "EndDate can't be blank!")]
        public DateTime EndDate { get; set; }

        public string EventDetail { get; set; }

        [Required(ErrorMessage = "EventDomain can't be blnak!")]
        public long EventDomain { get; set; }

        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }
        public DateTime DeletedOn { get; set; }

        public string CompanyName { get; set; }
        public string DomainName { get; set; }

    }


    public class EventMaster_page_Model
    {
        public List<EventMaster_Model> EventMaster_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
