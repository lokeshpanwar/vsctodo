﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DBReturnModel
    {
        public string ReturnStatus { get; set; }
        public string ReturnMessage { get; set; }

        public long Id { get; set; }
    }
}
