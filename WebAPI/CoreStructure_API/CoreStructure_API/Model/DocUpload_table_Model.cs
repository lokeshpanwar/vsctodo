﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocUpload_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long Doc_ID { get; set; }


        public long Sub_CompID { get; set; }


        public long DomainID { get; set; }

        [Display(Name = "DOCUMENT CLASS")]
        public long Doc_Class_ID { get; set; }

        [Display(Name = "DOCUMENT TYPE")]
        public long DocType_ID { get; set; }

        //[DocumentID]  AS ([dbo].[DocumentID]([SLNO])),

        [Display(Name = "REVISION ID")]
        public int Doc_Version { get; set; }


        [MaxLength(250)]
        [Display(Name = "SELECT DOCUMENT")]
        public string FileName { get; set; }


        //[MaxLength(200)]
        public string FilePath { get; set; }


        [MaxLength(50)]
        [Display(Name = "REVISION ID")]
        public string VersionID { get; set; }


        [Display(Name = "SELECT DOCUMENT")]
        [MaxLength(50)]
        public string FileSize { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public DateTime ModifiedOn { get; set; }


        public DateTime UploadedOn { get; set; }


        public long UploadedBy { get; set; }


        public bool CheckoutStatus { get; set; }


        public long CheckoutBy { get; set; }

        public DateTime CheckoutOn { get; set; }


        public bool CheckoutRight { get; set; }


        public bool Checkout_Available { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


        public long DeletedBy { get; set; }


        public bool IsCopied { get; set; }


        public long CopiedBy { get; set; }


        public DateTime CopiedOn { get; set; }

        [MaxLength(50)]
        public string CopiedTo { get; set; }



        public bool IsMoved { get; set; }


        public DateTime MovedOn { get; set; }


        public long MovedBy { get; set; }


        [MaxLength(50)]
        public string MovedTo { get; set; }


        public string Comments { get; set; }

        [Display(Name = "DOCUMENT SENSITIVITY")]
        public long sensitivity { get; set; }

        [Display(Name = "DOCUMENT CLASS")]
        public string DocumentClassName { get; set; } // Document Class Name Only for View Purpose

        [Display(Name = "DOCUMENT CLASS")]
        public string DocumentClassDescription { get; set; } // Document Class Description Only for View Purpose

        public string DocumentTypeName { get; set; } // Document Type Name Only for View Purpose

        public string DCUserName { get; set; } // View Only


        public List<List_Item_Table_Model> List_Item_Table_Model { get; set; }

        public List<User_Document_Mapping_Table_Model> User_Document_Mapping_Table_Model { get; set; } // User List Having Access To Document View Purpose
        
        public List<OldDocumentData> OldDocumentData_List { get; set; }


        public List<DMSProcess_Approvers_Model> DMSProcess_Approvers_Model { get; set; }

        public List<DMSProcess_Checkers_Model> DMSProcess_Checkers_Model { get; set; }

        public DocumentComent_Table_Model DocumentComent_Table_Model { get; set; }

        public DMSProcess_Model DMSProcess_Model { get; set; }

        public List<DocumentComent_Table_Model> DocumentComent_Table_Model_List { get; set; }
    }

    public class DocumentCheckout_Model
    {
        public long DocId { get; set; }
    }


    public class OldDocumentData
    {
        public long SLNO { get; set; }
        public string FileName { get; set; }
        public string VersionID { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
    }
}
