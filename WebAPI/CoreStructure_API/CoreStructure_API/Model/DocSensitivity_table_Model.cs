﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocSensitivity_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [MaxLength(50)]
        public string Sub_CompID { get; set; }


        [Required(ErrorMessage ="Name can't be blank!")]
        [MaxLength(50)]
        public string sensiName { get; set; }


        [Required(ErrorMessage = "Code can't be blank!")]
        [MaxLength(50)]
        public string sensiCode { get; set; }


        [Required(ErrorMessage = "Description can't be blank!")]
        [MaxLength(1000)]
        public string sensiDescrip { get; set; }


        [Required(ErrorMessage = "Sensitivity Level can't be blank!")]
        [MaxLength(50)]
        public string sensiType { get; set; }


        [Required(ErrorMessage = "Sensitivity Percent can't be blank!")]
        public int sensiPercent { get; set; }


        public DateTime CreatedOn { get; set; }

        [MaxLength(50)]
        public string CreatedBy { get; set; }



        public DateTime ModifiedOn { get; set; }


        [MaxLength(50)]
        public string ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


        [MaxLength(50)]
        public string DeletedBy { get; set; }

    }




    public class DocSensitivity_table_page_Model
    {
        public List<DocSensitivity_table_Model> DocSensitivity_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
