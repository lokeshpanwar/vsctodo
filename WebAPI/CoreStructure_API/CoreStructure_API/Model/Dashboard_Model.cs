﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class Dashboard_Model
    {
    }


    public class DashBPMFormsTrigger_Model
    {
        public int SLNO { get; set; }
        public long BPMID { get; set; }
        public string stageOrderID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long Days { get; set; }
        public string RASCI { get; set; }

        public string Company_Name { get; set; }
        public string Domain { get; set; }

    }

    public class DashBPMOpenTab_Model
    {


        public long BPMID { get; set; }

        public string workFlowName { get; set; }

        public long TotalDays { get; set; }

        public string priority { get; set; }

        public string description { get; set; }
        public List<DashBPMWipOrClosedTab_Model> DashBPMWipOrClosedTab_Model_List { get; set; }


    }

    public class DashBPMOpenTab_page_Model
    {

        public List<DashBPMOpenTab_Model> DashBPMOpenTab_Model_List { get; set; }



    }
    public class DashBPMWipOrClosedTab_Model
    {


        public long BPMID { get; set; }

        public string ThrdRef { get; set; }

        public long threadID { get; set; }

        public string stageName { get; set; }

        public string stageDescription { get; set; }

        public long stageDays { get; set; }

        public string RASCI { get; set; }


        public long stageOrderId { get; set; }

        public string BPM_Name { get; set; }
        public string Domain_Name { get; set; }
        public string SubCompanyName { get; set; }
        public string AdditionalSearchableColumn { get; set; }
    }


    public class DashBPMExceptionTab_Model
    {

        public string WorkflowName { get; set; }
        public long BPMID { get; set; }

        public long thrdID { get; set; }

        public string stageName { get; set; }

        public string stageDescription { get; set; }

        public long stageDays { get; set; }

        public long raisedBy { get; set; }

        public string status { get; set; }
        public string AdditionalSearchableColumn { get; set; }

        public long stageOrderId { get; set; }
    }

    public class workflow_thread_progress_table
    {
        //[Key]
        public long SLNO { get; set; }

        // [Required(ErrorMessage = "Work Flow Id can't be blank!")]
        public long workFlowID { get; set; }

        public long threadID { get; set; }

        //[Required(ErrorMessage = "Stage Order Id can't be blank!")]
        public string stageOrderID { get; set; }

        // [Required(ErrorMessage = "Stage Name can't be blank!")]
        // [MaxLength(500)]
        public string stageName { get; set; }


        //[Required(ErrorMessage = "Stage Description can't be blank!")]
        public string stageDescription { get; set; }

        // [Required(ErrorMessage = "Stage Days can't be blank!")]
        // [MaxLength(500)]
        public string stageDays { get; set; }


        //  [MaxLength(500)]
        public string people { get; set; }


        public string peopleInfo { get; set; }


        //  [MaxLength(500)]
        public string RASCI { get; set; }



        public string attachment { get; set; }


        public string url { get; set; }


        public string form { get; set; }


        public string checklist { get; set; }

        // [MaxLength(500)]
        public string approverWorkFlow { get; set; }


        public string processYes { get; set; }


        public string processNo { get; set; }

        //[MaxLength(500)]
        // public string createdBy { get; set; }


        public DateTime createdOn { get; set; }

        public bool threadStatus { get; set; }

        public bool complitionStatus { get; set; }

        public bool isOpen { get; set; }

        public bool isWip { get; set; }

        public bool isClosed { get; set; }

        public bool isOnHold { get; set; }

        public DateTime start_date { get; set; }

        public DateTime end_date { get; set; }

        public DateTime recv_date { get; set; }

        public DateTime submission_date { get; set; }

        public string Comment { get; set; }

        
        public string ThrdRef { get; set; }

        public long DynGroupId { get; set; }

        public string formTemplate { get; set; }



        public long PeopleID { get; set; }


        //  [MaxLength(50)]
        public string Viewright { get; set; }



        public string communicateTo { get; set; }


        public bool IsDelegated { get; set; }


        public string DelegatedTo { get; set; }

        //  [MaxLength(200)]
        public string DynRef { get; set; }


        public string sapForm { get; set; }

        //  public long DynGroupId { get; set; }

        // public string formTemplate { get; set; }

         public DateTime progress_submission_date { get; set; }
        public string PriID { get; set; }

        public string CPSimulationRefNumber { get; set; }

        public long BPMTriggerId { get; set; }

        public workflow_table_Model workflow_table_Model { get; set; }
    }


    public class workflow_progress_Model
    {
        [Key]
        public long SLNO { get; set; }

        // [Required(ErrorMessage = "Work Flow Id can't be blank!")]
        public long thrdID { get; set; }

        //[Required(ErrorMessage = "Stage Order Id can't be blank!")]
        public long wfID { get; set; }

        // [Required(ErrorMessage = "Stage Name can't be blank!")]
        // [MaxLength(500)]
        public long stgID { get; set; }


        //[Required(ErrorMessage = "Stage Description can't be blank!")]
        public string peopleType { get; set; }

        // [Required(ErrorMessage = "Stage Days can't be blank!")]
        // [MaxLength(500)]
        public string personID { get; set; }


        //  [MaxLength(500)]
        public string formID { get; set; }


        public string formData { get; set; }


        //  [MaxLength(500)]
        public string chkID { get; set; }



        public string chkData { get; set; }


        public string processStatus { get; set; }


        public string comments { get; set; }


        public bool complitionStatus { get; set; }

        // [MaxLength(500)]
        public bool threadStatus { get; set; }


        public DateTime recv_date { get; set; }


        public DateTime start_date { get; set; }

        //[MaxLength(500)]
        public DateTime end_date { get; set; }


        public DateTime submission_date { get; set; }

        //  [MaxLength(500)]
        public bool submissionStatus { get; set; }


        public string submittedBy { get; set; }

        public string communicateTo { get; set; }

        //[MaxLength(50)]
        public string defaultFile { get; set; }



        public string raisedTo { get; set; }
        public string raisedMsg { get; set; }
        public string raisedBy { get; set; }

        public List<workflow_process_docs_Model> workflow_process_docs_Model_List { get; set; }

    }

    public class Dashboard_Widget
    {
        public long DocumentView { get; set; }
        public long Document { get; set; }
        public long Event { get; set; }
        public long Announcement { get; set; }
    }

}
