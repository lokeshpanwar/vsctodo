﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class workflow_stage_table_Model
    {
        [Key]
        public long SLNO { get; set; }

        [Required(ErrorMessage ="Work Flow Id can't be blank!")]
        public long workFlowID { get; set; }

        //[Required(ErrorMessage = "Stage Order Id can't be blank!")]
        public string stageOrderID { get; set; }

        [Required(ErrorMessage = "Stage Name can't be blank!")]
        [MaxLength(500)]
        public string stageName { get; set; }


        [Required(ErrorMessage = "Stage Description can't be blank!")]
        public string stageDescription { get; set; }

        [Required(ErrorMessage = "Stage Days can't be blank!")]
        [MaxLength(500)]
        public string stageDays { get; set; }


        [MaxLength(500)]
        public string people { get; set; }


        public string peopleInfo { get; set; }


        [MaxLength(500)]
        public string RASCI { get; set; }



        public string attachment { get; set; }


        public string url { get; set; }


        public string form { get; set; }


        public string checklist { get; set; }

        [MaxLength(500)]
        public string approverWorkFlow { get; set; }


        public string processYes { get; set; }


        public string processNo { get; set; }

        [MaxLength(500)]
        public string createdBy { get; set; }


        public DateTime createdOn { get; set; }
        
        [MaxLength(500)]
        public string editedBy { get; set; }


        public DateTime editedOn { get; set; }

        public bool isDeleted { get; set; }

        [MaxLength(50)]
        public string deletedBy { get; set; }


        public DateTime deletedOn { get; set; }


        public bool activeStatus { get; set; }


        public bool IsAllpeople { get; set; }


        public long PeopleID { get; set; }


        [MaxLength(50)]
        public string Viewright { get; set; }



        public string communicateTo { get; set; }


        public bool IsDelegated { get; set; }


        public string DelegatedTo { get; set; }

        [MaxLength(200)]
        public string DynRef { get; set; }


        public string sapForm { get; set; }

        public long DynGroupId { get; set; }

        public string formTemplate { get; set; }


    }


    public class workflow_stage_table_Model_View
    {
        [Key]
        public long SLNO { get; set; }

        public long workFlowID { get; set; }

        public string stageOrderID { get; set; }


        public string stageName { get; set; }

        public string stageDescription { get; set; }

        public string stageDays { get; set; }


        public string people { get; set; }

        public string peopleInfo { get; set; }


        public string RASCI { get; set; }


        public string attachment { get; set; }


        public string url { get; set; }



        public string form { get; set; }



        public string checklist { get; set; }



        public string approverWorkFlow { get; set; }

        public bool approverWorkFlow_CheckBox { get; set; } // Only display model 



        public string processYes { get; set; }



        public string processNo { get; set; }



        public string createdBy { get; set; }


        public DateTime createdOn { get; set; }



        public string editedBy { get; set; }


        public DateTime editedOn { get; set; }


        public bool isDeleted { get; set; }



        public string deletedBy { get; set; }


        public DateTime deletedOn { get; set; }


        public bool activeStatus { get; set; }


        public bool IsAllpeople { get; set; }


        public long PeopleID { get; set; }



        public string Viewright { get; set; }


        public string communicateTo { get; set; }



        public bool IsDelegated { get; set; }



        public string DelegatedTo { get; set; }


        public string DynRef { get; set; }



        public string sapForm { get; set; }


        public long DynGroupId { get; set; }



        public string formTemplate { get; set; }


        public List<workflow_stage_for_communication> workflow_stage_for_graph_List { get; set; } // Only For Workflow Graph

    }



    public class workflow_stage_for_communication
    {
        public string stage_name { get; set; }
        public long stage_id { get; set; }
        public bool IsSelected { get; set; }
    }


    public class workflowstage_user_right
    {
        public long SLNO { get; set; }
        public string Viewright { get; set; }
    }

    public class workflowstage_user_right_List
    {
        public List<workflowstage_user_right> workflowstage_user_right_list { get; set; }
    }

}
