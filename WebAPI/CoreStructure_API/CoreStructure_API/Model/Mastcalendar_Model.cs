﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace CoreStructure_API.Model
{
    public class Mastcalendar_Model
    {

        [Key]
        public long Cl_ID { get; set; }

        public long Empid { get; set; }


        [Required(ErrorMessage = "leave date can't be blank!")]
        public DateTime leave_date { get; set; }

        //[Display (Name ="")]
        //[Required(ErrorMessage = "Calender Descritpion can't be blank!")]

        public string Cl_description { get; set; }

        public bool isactive { get; set; }
        public DateTime creationdate { get; set; }


        public string creationuser { get; set; }


        public DateTime modifactiondate { get; set; }


        public string modifactionuser { get; set; }


        public bool isdeleted { get; set; }

        public string backgroundcolor { get; set; }
    }


    public class Mastcalendar_page_Model
    {
        public List<Mastcalendar_Model> Mastcalendar_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
