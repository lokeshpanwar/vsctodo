﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocumentType_DocClass_Mapping_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage ="Document Class ID can't be blank!")]
        public long Document_ClassID { get; set; }


        public long DocumentType_ID { get; set; }


        public long Sub_CompID { get; set; }

        
        public long CreatedBy { get; set; }
        

        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


        public DateTime DeletedOn { get; set; }


        public List<DocType> DocType_List { get; set; }


        public string DCClassName { get; set; }
        public string DCDocumentTypeName { get; set; }
        public string DCDomainName { get; set; }

    }

    public class DocType
    {
        [Required(ErrorMessage = "Document Type ID can't be blank!")]
        public long DocType_Id { get; set; }
        public string DocType_Name { get; set; }
        public bool IsSelected { get; set; }
    }


    public class DocumentType_DocClass_Mapping_Table_page_Model
    {
        public List<DocumentType_DocClass_Mapping_Table_Model> DocumentType_DocClass_Mapping_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
