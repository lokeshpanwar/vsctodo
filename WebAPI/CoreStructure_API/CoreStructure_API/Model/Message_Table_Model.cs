﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Message_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long AdminID { get; set; }


        public long From_user { get; set; }


        public long To_user { get; set; }


        [MaxLength(50)]
        public string ToEmail { get; set; }

        [MaxLength(100)]
        public string FileLink { get; set; }


        public long Doc_ID { get; set; }


        public DateTime Date { get; set; }


        public bool Status { get; set; }


        [MaxLength(50)]
        public string Section { get; set; }



        public string Message { get; set; }


        public int Expirein { get; set; }
        

        public bool isReaded { get; set; }


        public List<EmpData> EmployeeList { get; set; }
        public List<VenData> VendorList { get; set; }
        public List<GroupData> GroupList { get; set; }
        public List<DomainData> DomainList { get; set; }

    }


    public class EmpData
    {
        public long SLNO { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsSelected { get; set; }
    }
    public class VenData
    {
        public long SLNO { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsSelected { get; set; }
    }
    public class GroupData
    {
        public long SLNO { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
    public class DomainData
    {
        public long SLNO { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

}
