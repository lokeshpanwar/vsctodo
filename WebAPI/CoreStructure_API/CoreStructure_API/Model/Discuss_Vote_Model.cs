﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class Discuss_Vote_Model
    {

        [Key]

        public long ID { get; set; }

        public long Discuss_ID { get; set; }

        public long UserID { get; set; }
        public bool Up { get; set; }

        public bool Down { get; set; }


        public DateTime Creadedon { get; set; }

        
    }

    public class Discuss_Vote_page_Model
    {
        public List<Discuss_Vote_Model> Discuss_Vote_Model_List { get; set; }



        public long Discuss_ID { get; set; }

        public long UserID { get; set; }

      //  public List<Discuss_Model> Discuss_Model_List { get; set; }
    }

}
