﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Log_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [MaxLength(100)]
        public string Parent_CompID { get; set; }

        [MaxLength(100)]
        public string Sub_CompID { get; set; }



        public string UserID { get; set; }


        [MaxLength(50)]
        public string OperationType { get; set; }


        [MaxLength(150)]
        public string Operation_Performed { get; set; }


        public DateTime OperationON { get; set; }


        public string Remarks { get; set; }

        [MaxLength(50)]
        public string IP_Address { get; set; }


        public long user_id { get; set; }

        public string EmployeeName { get; set; }

        public string SubCompanyName { get; set; }

        public string Domain_Name { get; set; }


    }
}
