﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class User_DocClass_Mapping_Table_Model
    {
        [Key]
        public long SLNO { get; set; }

        
        public long Sub_CompID { get; set; }


        public long Domain_ID { get; set; }


        [Required(ErrorMessage ="Document Class can't be blank!")]
        public long Document_ClassID { get; set; }


        [Required(ErrorMessage = "User Id can't be blank!")]
        public long User_ID { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


        public DateTime DeletedOn { get; set; }


        public bool view_right { get; set; }


        public bool print_right { get; set; }

        public bool email_right { get; set; }

        public bool checkin_right { get; set; }

        public bool checkout_right { get; set; }

        public bool download_right { get; set; }

        public bool upload_right { get; set; }

        public bool selectall_right { get; set; }


        public List<UserDet> Emp_UserDet_List { get; set; }
        public List<UserDet> Ven_UserDet_List { get; set; }


        public string UserName { get; set; }
        public string UserType { get; set; }
        public string DCClassName { get; set; }
        public string DCDomainName { get; set; }

    }

    public class UserDet
    {
        [Required(ErrorMessage = "User ID can't be blank!")]
        public long User_ID { get; set; }
        public string User_Name { get; set; }
        public bool IsSelected { get; set; }
    }


    public class User_DocClass_Mapping_Table_page_Model
    {
        public List<User_DocClass_Mapping_Table_Model> User_DocClass_Mapping_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }



    public class UserDocMapping
    {
        public long User_ID { get; set; }

        public string UserName { get; set; }

        public List<User_DocClass_Mapping_Table_Model> User_DocClass_Mapping_Table_Model_List { get; set; }

        public List<User_DocType_Mapping_Table_Model> User_DocType_Mapping_Table_Model_List { get; set; }

        public List<User_Document_Mapping_Table_Model> User_Document_Mapping_Table_Model_List { get; set; }

    }

    public class UserDocMapping_Model
    {
        public List<UserDocMapping> UserDocMapping_List { get; set; }

        public List<UserDocMapping> ExternalUserDocMapping_List { get; set; }
        public long Sub_CompID { get; set; }
        public long Domain_ID { get; set; }
        public long CreatedBy { get; set; }
    }

}
