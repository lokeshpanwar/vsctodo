﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class BusinessProcess_Model
    {
        public long ref_Id { get; set; }
        public long BPMID { get; set; }
        public long StageId { get; set; }

        [Display(Name = "Performa Invoice Number")]
        public string Performa_Invoice_number { get; set; }

        [Display(Name = "Buyer")]
        public string Buyer { get; set; }

        [Display(Name = "Manufacturing Factory")]
        public string Manufacturing_Factory { get; set; }

        [Display(Name = "Manufacturing Address")]
        public string Manufacturing_Address { get; set; }

        [Display(Name = "Transhipment")]
        public int Transhipment { get; set; }

        [Display(Name = "Shipment Terms")]
        public string Shipment_Terms { get; set; }

        [Display(Name = "LC Terms")]
        public string LC_Terms { get; set; }

        [Display(Name = "Partial Shipment")]
        public int Partial_Shipment { get; set; }

        [Display(Name = "Third Party Document")]
        public int Third_Party_Documnet { get; set; }

        [Display(Name = "Mode Of Transport")]
        public string Mode_Of_Transport { get; set; }

        [Display(Name = "Advising Bank Details")]
        public string Advising_Bank_Details { get; set; }

        [Display(Name = "Trims Free Of Cost")]
        public int Trims_Free_Of_Cost { get; set; }

        [Display(Name = "Trims Included In Factory Cost")]
        public int Trims_Included_In_Factory_Cost { get; set; }

        [Display(Name = "Packing Mode")]
        public string Packing_Mode { get; set; }

        [Display(Name = "Inspection Certificate Signed By")]
        public string Inspection_Certificate_Signed_By { get; set; }

        [Display(Name = "GSP")]
        public string GSP { get; set; }

        [Display(Name = "Export License")]
        public int Export_License { get; set; }

        [Display(Name = "Presentation Period")]
        public string Presentation_Period { get; set; }

        [Display(Name = "Notify Party")]
        public string Notify_Party { get; set; }

        [Display(Name = "Expiry Date")]
        public string Expiry_Date { get; set; }

        [Display(Name = "Shipment From Country")]
        public string Shipment_From_Country { get; set; }

        [Display(Name = "Shipment From Port")]
        public string Shipment_From_Port { get; set; }

        [Display(Name = "Shipment To Country")]
        public string Shipment_To_Country { get; set; }

        [Display(Name = "Shipment To Port")]
        public string Shipment_To_Port { get; set; }

        [Display(Name = "Variation")]
        public string Variation { get; set; }
        public string Total_Qty { get; set; }
        public string Value_InUSD { get; set; }
        public string Value_InWords { get; set; }
        public int CurrentUserId { get; set; }

        public List<PODetails_Model> PoList { get; set; }
    }

    public class PODetails_Model
    {
        public long SrNo { get; set; }
        public long SAP_PO { get; set; }
        public string SAP_PO_Line_Item { get; set; }
        public string Material_SAP_Style_Number { get; set; }
        public string Style_Description { get; set; }
        public string Buyer_PO_Line { get; set; }
        public string Colour { get; set; }
        public string UOM_SAP { get; set; }
        public string Qty_In_Pcs { get; set; }
        public string Price_Per_Pc { get; set; }
        public decimal Value_In_USD { get; set; }
        public string Shipment_Date { get; set; }
        public string Remarks { get; set; }
    }

    public class PODetails_Model_Table
    {
        public List<PODetails_Model> results { get; set; }
    }

}
