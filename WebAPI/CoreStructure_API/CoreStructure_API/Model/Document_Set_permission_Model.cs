﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Document_Set_permission_Model
    {
        [Key]
        public long SLNO { get; set; }

        [Display(Name ="User Name")]
        public long User_ID { get; set; }

        [Display(Name = "SELECT DOCUMENT SET")]
        public long Set_ID { get; set; }


        public bool view_right { get; set; }


        public bool print_right { get; set; }


        public bool email_right { get; set; }


        public bool checkin_right { get; set; }


        public bool checkout_right { get; set; }


        public bool download_right { get; set; }


        public bool upload_right { get; set; }


        public bool selectall_right { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public DateTime ModifiedOn { get; set; }

        [Display(Name = "SELECT COMPANY")]
        public long SubCompId { get; set; }

        [Display(Name = "SELECT DOMAIN")]
        public long DomainId { get; set; }

        public List<DocSetPermission> DocSetPermission_List { get; set; }

    }


    public class DocSetPermission
    {
        public long User_ID { get; set; }

        public long Set_ID { get; set; }


        public bool view_right { get; set; }


        public bool print_right { get; set; }


        public bool email_right { get; set; }


        public bool checkin_right { get; set; }


        public bool checkout_right { get; set; }


        public bool download_right { get; set; }


        public bool upload_right { get; set; }


        public bool selectall_right { get; set; }


        public string UserName { get; set; }

    }


    public class DocSetUserDashboard
    {
        public long SLNO { get; set; }
        
        public string DocumentSetName { get; set; }

        public string EmpId { get; set; }

        public long DomainID { get; set; }

        public long Sub_CompID { get; set; }
    }

}
