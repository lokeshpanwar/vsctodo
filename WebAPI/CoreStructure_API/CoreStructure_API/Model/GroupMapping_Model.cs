﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace CoreStructure_API.Model
{
    public class GroupMapping_Model
    {
        public long SLNO { get; set; }
        public long GroupID { get; set; }

        public string GroupName { get; set; }

        public string Domain_Name { get; set; }

        public string SubCompanyName { get; set; }

        public string UserName { get; set; }

        public string Vendor { get; set; }

    }

    public class GroupMapping_page_Model
    {
        public List<GroupMapping_Model> GroupMapping_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
