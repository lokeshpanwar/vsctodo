﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace CoreStructure_API.Model
{
    public class CompanyHoliday_Model
    {

        [Key]
        public long Entryid { get; set; }

        public string CompanyId { get; set; }


        [Required(ErrorMessage = "Start date can't be blank!")]
        public DateTime YearStartDate { get; set; }

       // [Display(Name = "")]
       [Required(ErrorMessage = "End date can't be blank!")]

        public DateTime YearEndDate { get; set; }

      
        public string HolidaysDate { get; set; }


        public int Workingdays { get; set; }

       
        public bool isdeleted { get; set; }
    }

    public class CompanyHoliday_page_Model
    {
        public List<CompanyHoliday_Model> CompanyHoliday_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
