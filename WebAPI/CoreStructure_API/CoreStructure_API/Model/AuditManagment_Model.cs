﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class AuditManagment_Model
    {

        public class FrequencyModel
        {

            public string FrequencyName { get; set; }


            public string FrequencyCode { get; set; }


            public string FrequencyDetails { get; set; }


            public string FrequencyDays { get; set; }

            public int Frequencyid { get; set; }


            public long CreatedBy { get; set; }

            public int IsDelete { get; set; }


            public int TotalRows { get; set; }


        }


        public class FrequencyModel_table
        {
            public List<FrequencyModel> result { get; set; }

        }


        public class CompanyDetails
        {
            public string ComapnyName { get; set; }

            public string ComapnyCode { get; set; }

            public string ComapnyDetails { get; set; }

            public int comapnyid { get; set; }

            public long CreatebyC { get; set; }

            public int IsClientDelete { get; set; }

            public int IsClientActive { get; set; }

            public  int TotalRows { get; set; }

        }

        public class DatesRange
        {

            public int Dateid { get; set; }

            public string Dates { get; set; }


        }

        public class DatesRange_table
        {
            public List<DatesRange> results { get; set; }

        }
        public class TeamModel
        {
            [Display(Name = "NAME")]
            [Required(ErrorMessage = "Team Name can't be blank")]
            public string TeamName { get; set; }

            [Display(Name = "Code")]
            [Required(ErrorMessage = "Team Code can't be blank")]
            public string TeamCode { get; set; }

            [Display(Name = "Details")]
            [Required(ErrorMessage = "Team Details can't be blank")]
            public string TeamDetails { get; set; }

            public int Teamid { get; set; }

            public int TeamCreatedby { get; set; }

            public int IsTeamDelete { get; set; }

            public int IsTeamActive { get; set; }

        }

        public class Team_table
        {
            public List<TeamModel> results { get; set; }
        }

        public class LocationsModel
        {
            public string LocationName { get; set; }

            public string LocationCode { get; set; }

            public string LocationDetails { get; set; }

            public int Locationid { get; set; }

            public int LocationCreatedby { get; set; }

            public int IsLocationDelete { get; set; }

            public int IsLocationActive { get; set; }

            public string AuditClientId { get; set; }

            public string AuditClientName { get; set; }

            public long createbyclient { get; set; }

            public int TotalRows { get; set; }

        }

        public class LocationsModel_table
        {
            public List<LocationsModel> results { get; set; }
        }


        public class AuditGroupMapping_Model
        {

            public int GroupId { get; set; }

            public string GroupName { get; set; }

            public string APGroupName { get; set; }

            public int APGroupId { get; set; }

            public string ACGroupName { get; set; }

            public int ACGroupId { get; set; }

            public string ACGroupIds { get; set; }

            public string AuditCompany { get; set; }

            public string AuditScope { get; set; }

            public long AuditCreatedby { get; set; }

            public string AuditscopName { get; set; }

            public string AuditscopCompanyName { get; set; }

            public int GroupActive { get; set; }

            public string AuditscopNamelist { get; set; }
            public string AuditscopCompanyNamelist { get; set; }

            public int DocumentCompany { get; set; }

            public int FunctionalDomain { get; set; }

            public string DocumentClass { get; set; }

        }
        public class AuditGroupMapping_Model_table
        {
            public List<AuditGroupMapping_Model> results { get; set; }

            public int TotalRows { get; set; }
        }


        public class AuditGroupMapping_Model_table_Results
        {
            public AuditGroupMapping_Model_table results { get; set; }

            public int TotalRows { get; set; }
        }


        public class AuditPlanner
        {

            public List<AuditGroupMapping_Model> AuditPlannerList { get; set; }

            public int AuditPlannerId { get; set; }

            [Display(Name = "Frequency")]
            [Required(ErrorMessage = "Frequency can't be blank")]
            public string AuditPlannerFrequencyName { get; set; }

            public int AuditPlannerFrequencyId { get; set; }

            [Display(Name = "Start Date")]
            [Required(ErrorMessage = "Start Date can't be blank")]
            public string AuditPlannerStartDate { get; set; }

            [Display(Name = "END Date")]
            [Required(ErrorMessage = "END Date can't be blank")]
            public string AuditPlannerEndDate { get; set; }


            [Display(Name = "User Name")]
            [Required(ErrorMessage = "User can't be blank")]
            public string AuditPlannerUsre { get; set; }


            public int AuditPlannerUsreId { get; set; }



        }


        public class AuditData_Model
        {

            [Display(Name = "Audit Code")]
            [Required(ErrorMessage = "Audit Code can't be blank")]
            public string AuditCode { get; set; }

            [Display(Name = "Audit Description")]
            [Required(ErrorMessage = "Audit Description can't be blank")]
            public string AuditDescription { get; set; }

            [Display(Name = "Frequency Name")]
            //[Required(ErrorMessage = "Frequency can't be blank")]
            public string Frequencyname { get; set; }

            [Display(Name = "Audit Duration")]
            [Required(ErrorMessage = "Audit Duration can't be blank")]
            public int AuditDuration { get; set; }

            public string Frequencys { get; set; }

            public int IsDelete { get; set; }

            public int IsActive { get; set; }

            public int MainId { get; set; }

            public string Createdate { get; set; }

            public string Modifydate { get; set; }

            public string ScheduleId { get; set; }

            public string ScheduleDatefrom { get; set; }

            public string ScheduleDateto { get; set; }
            public List<Audit_Manage> Audit_Manage_Table { get; set; }
        }

        public class AuditData_Model_Table
        {
            public List<AuditData_Model> results { get; set; }
            public int TotalRows { get; set; }

        }

        public class Audit_Model
        {
            //[Required(ErrorMessage = "Header Name can't be blank")]
            public string HeaderName { get; set; }

            [Display(Name = "Audit Code")]
            [Required(ErrorMessage = "Audit Code can't be blank")]
            public string AuditCode { get; set; }



            [Display(Name = "Audit Description")]
            [Required(ErrorMessage = "Audit Description can't be blank")]
            public string AuditDescription { get; set; }

            [Display(Name = "Audit Duration")]
            [Required(ErrorMessage = "Audit Duration can't be blank")]
            public int AuditDuration { get; set; }

            [Display(Name = "Frequency Name")]
            //[Required(ErrorMessage = "Frequency can't be blank")]
            public string Frequencyname { get; set; }

            public string Frequencys { get; set; }

            public List<SelectListItem> FrequencyList { get; set; }


            //public List<FrequencynameList> FrequencyList { get; set; }

            public int FrequencyId { get; set; }

            public string ControlArea { get; set; }

            public string ControlNo { get; set; }

            public string ControlObject { get; set; }

            public string ControlDescription { get; set; }

            public string Artefacts { get; set; }

            public int AuditId { get; set; }
            public string Remarks { get; set; }

            public int RowCount { get; set; }
            public int TotalRowAdded { get; set; }

            public string TotalRowAddedList { get; set; }

            public int TotalRowDel { get; set; }

            public string TotalRowDelList { get; set; }
            public List<Audit_Manage> Audit_Manage_Table { get; set; }


            public long AuditCreatedBy { get; set; }

            public int IsDelete { get; set; }

            public int AIsActive { get; set; }

            public int MainId { get; set; }

            public string AuditCreatedate { get; set; }

            public string AuditAFIDStatus { get; set; }

            public string ArefactRef { get; set; }

            public string st2DateofUpload { get; set; }

            public string st1comment { get; set; }

            public string st1Date { get; set; }

            public string purposeCommentsAuditee { get; set; }

            public int NYStatus { get; set; }

            public string st2Date { get; set; }

            public int St1UserId { get; set; }

            public string St1UserName { get; set; }

            public string St2Currentuser { get; set; }

            public int StageUpdate { get; set; }

            public string ScheduleId { get; set; }


            public string AuditIdLists { get; set; }

            public int PurposeReqUserId { get; set; }

            public int AFIDStatus { get; set; }


            public string ActualDateofCloser { get; set; }

            public string St4CurrentDate { get; set; }

            public string st4currentusername { get; set; }


            public int CurrentUserId { get; set; }

            public string st3Date { get; set; }

            public string ActualDtofClosure { get; set; }

            public string St4Date { get; set; }

            public string st1CurrentUserName { get; set; }


            public string st2CurrentUserName { get; set; }

            public string Observation { get; set; }

            public string Priority { get; set; }

            public string Recommendation { get; set; }

            public int st2Status { get; set; }

            public int st3UserId { get; set; }

            public string st3CurrentUserName { get; set; }

            public string Response { get; set; }

            public string EstDateofClosure { get; set; }

            public string RefDocClose { get; set; }

            public int St3Status { get; set; }

            public int st4st4status { get; set; }

            public string st4comment { get; set; }

            public string St4CurrenUsername { get; set; }

            public int loopcount { get; set; }

            public string SchDatefrom { get; set; }

            public string SchDateto { get; set; }

            public int St5Status { get; set; }

            public string Description { get; set; }

            public string Name { get; set; }

            public int HDRowStage { get; set; }

            public string Documentclass { get; set; }

            
            public int MessageCount { get; set; }

            public int auditscheduleid { get; set; }

            public int GroupMappingId { get; set; }

            public int AuditControlId { get; set; }
        }

        
        public class Audit_Conversation
        {
            public int MsgAuditId { get; set; }

            [Required(ErrorMessage = "Message can't be blank")]
            public string Message { get; set; }

            public int SenderId { get; set; }
            public int RecieverId { get; set; }

            public string SenderName { get; set; }
            public string RecieverName { get; set; }

            public int IsView { get; set; }
            public string CreateDate { get; set; }
        }
        
        public class Audit_Trans
        {

            public int Trans_AuditId { get; set; }


            [Display(Name = "Audit Id")]
            public string Trans_Auditcode { get; set; }

            [Display(Name = "Audit Name")]
            public string Trans_AuditDesc { get; set; }

            [Display(Name = "Schedule Ref")]
            public string Frequencyname { get; set; }

            public string Frequencys { get; set; }

            public int Frequencyid { get; set; }

            [Display(Name = "Schedule Id")]
            public string ScheduleId { get; set; }

            [Display(Name = "From Date")]
            [Required(ErrorMessage = "Date From can't be blank")]
            public string SchDatefrom { get; set; }


            [Display(Name = "From To")]
            [Required(ErrorMessage = "Date To can't be blank")]
            public string SchDateto { get; set; }


            [Display(Name = "Auditor")]
            public string Auditor { get; set; }

            [Display(Name = "Client")]
            public string Client { get; set; }


            [Display(Name = "Location")]
            public string Location { get; set; }

            public int CurrentUserId { get; set; }

            public string st3Date { get; set; }

            public string ActualDtofClosure { get; set; }

            public string St4Date { get; set; }

            public string st1CurrentUserName { get; set; }

            public string ArefactRef { get; set; }

            public string st1comment { get; set; }

            public string st2CurrentUserName { get; set; }

            public string Observation { get; set; }

            public string Priority { get; set; }

            public string Recommendation { get; set; }

            public int st2Status { get; set; }

            public string st3CurrentUserName { get; set; }

            public string Response { get; set; }

            public int EstDateofClosure { get; set; }

            public string RefDocClose { get; set; }

            public int St3Status { get; set; }

            public int st4st4status { get; set; }

            public string st4comment { get; set; }


            public int loopcount { get; set; }

            public string AuditIdLists { get; set; }

            public int AuditCreatedBy { get; set; }

            public int HDRowStage { get; set; }

        }
        public class Audit_Manage
        {

            public string Audit_ControlArea { get; set; }

            public string Audit_ControlNo { get; set; }

            public string Audit_ControlObject { get; set; }

            public string Audit__ControlDescription { get; set; }

            public string Audit__Artefacts { get; set; }

            public string Audit_Remarks { get; set; }
            public int AuditMasterId { get; set; }

            public long AudCreatedBy { get; set; }

            public string Audit_Description { get; set; }

            public string Audit_Name { get; set; }

            public int Id { get; set; }

        }

        public class AuditUserMapping_Model
        {
            public long Id { get; set; }
            public long GroupMappingId { get; set; }
            public long ControlId { get; set; }
            public long EmployeeId { get; set; }
            public long GroupId { get; set; }
            public string Type { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public long CreatedBy { get; set; }
            public long ModifiedBy { get; set; }
            public string ControlNo { get; set; }
            public string ControlArea { get; set; }
        }

        public class AuditScheduling_Model
        {
            public long Id { get; set; }
            public long GroupMappingId { get; set; }
            public long FrequencyId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Status { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public long CreatedBy { get; set; }
            public long ModifiedBy { get; set; }
            public string Name { get; set; }
            public string Days { get; set; }
            public bool IsDelete { get; set; }
            public bool IsActive { get; set; }

            public string Location { get; set; }
            public string Scope { get; set; }
            public string Client { get; set; }
            public string Group { get; set; }
        }

        public class AuditScheduling_Model_Result
        {
            public List<AuditScheduling_Model> results { get; set; }
        }

        public class AuditScheduling_Model_Table
        {
            public List<AuditScheduling_Model> results { get; set; }
            public int TotalRows { get; set; }
        }

        public class AuditRegister_Model
        {
            public long AuditID { get; set; }
            public string AuditCode { get; set; }
            
            public string Description { get; set; }
            public long AuditScheduleId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        public class AuditRegister_Model_Table
        {
            public List<AuditRegister_Model> results { get; set; }
            public int TotalRows { get; set; }

        }

        public class AuditDashboardClient_Model
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string LocationName { get; set; }
            public long LocationId { get; set; }
        }

        public class AuditDashboardNotification_Model
        {
            public long Id { get; set; }
            public long AuditTransactionId { get; set; }
            public string Message { get; set; }
           
        }

        public class AuditAdminDashboard_Model
        {
            public int AuditCount { get; set; }
            public int EnableAuditCount { get; set; }
            public int DisableAuditCount { get; set; }

            public List<LocationsModel> locations { get; set; }
            public List<AuditDashboardClient_Model> clients { get; set; }
            public List<AuditDashboardNotification_Model> notifications { get; set; }
            public List<PieChartReportViewModel> PieChartReportViewModel_List { get; set; }
        }
    }
}
