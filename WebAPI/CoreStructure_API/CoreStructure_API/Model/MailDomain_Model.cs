﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class MailDomain_Model
    {
        [Key]
        public int ID { get; set; }


        [Required(ErrorMessage ="Domain can't be blank!")]
        [MaxLength(100)]
        public string Domain { get; set; }
        

        public DateTime CreatedOn { get; set; }


        public bool IsActive { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public long CreatedBy { get; set; }


    }

    

    public class MailDomain_page_Model
    {
        public List<MailDomain_Model> MailDomain_Model_List { get; set; }

        public int TotalRows { get; set; }
    }



}
