﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Employee_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name = "EMPLOYEE ID")]
        //[Required(ErrorMessage = "Employee Id can't be blank!")]
        //[MaxLength(100)]
        public string EmpId { get; set; }


        [Display(Name = "USER TYPE")]
        //[Required(ErrorMessage = "User Type can't be blank!")]
        //[MaxLength(100)]
        public string User_type { get; set; }


        [Display(Name = "EMPLOYEE NAME")]
        //[Required(ErrorMessage = "Employee Name can't be blank!")]
        //[MaxLength(100)]
        public string EmployeeName { get; set; }


        [Display(Name = "DESIGNATION")]
        //[Required(ErrorMessage = "Designation can't be blank!")]
        //[MaxLength(100)]
        public string Designation { get; set; }


        [Display(Name = "SUB COMPANY")]
        //[Required(ErrorMessage = "Sub Company can't be blank!")]
        public long Sub_Company { get; set; }



        [Display(Name = "DEPARTMENT")]
        //[Required(ErrorMessage = "Department can't be blank!")]
        //[MaxLength(100)]
        public string Department { get; set; }


        [Display(Name = "DOMAIN")]
        //[Required(ErrorMessage = "Domain can't be blank!")]
        public long Domain_ID { get; set; }


        [Display(Name = "LOCATION")]
       // [Required(ErrorMessage = "Location can't be blank!")]
        //[MaxLength(100)]
        public string Location { get; set; }



        [Display(Name = "REPORTING MANAGER")]
        //[Required(ErrorMessage = "Reporting Manager can't be blank!")]
        //[MaxLength(100)]
        public string Reporting_Manager_Id { get; set; }


        [Display(Name = "CONTACT NO")]
        //[Required(ErrorMessage = "Contact No can't be blank!")]
        public long Contact_No { get; set; }



        //public string Emp_Photo { get; set; }

        [Display(Name = "EMAIL")]
        [Required(ErrorMessage = "Email can't be blank!")]
        [MaxLength(100)]
        public string Email { get; set; }


        [Display(Name = "COUNTRY")]
        //[Required(ErrorMessage = "Country can't be blank!")]
        //[MaxLength(5)]
        public string CountryCode { get; set; }


        [Display(Name = "DATE OF JOIN")]
        //[Required(ErrorMessage = "Date Of Join can't be blank!")]
        public DateTime Doj { get; set; }


        [Display(Name = "DATE OF BIRTH")]
        //[Required(ErrorMessage = "Date Of Birth can't be blank!")]
        public DateTime Dob { get; set; }


        [Display(Name = "USER ID")]
        //[Required(ErrorMessage = "User Id can't be blank!")]
        //[MaxLength(100)]
        public string Userid { get; set; }


        [Display(Name = "PASSWORD")]
        //[Required(ErrorMessage = "Password can't be blank!")]
        //[MaxLength(100)]
        public string Password { get; set; }



        public bool IsDeleted { get; set; }


        [Display(Name = "IS MANAGER")]
        public bool isManager { get; set; }

        [Display(Name = "IS USER FP")]
        public bool isUserFp { get; set; }



        public DateTime CreatedDate { get; set; }


        public long CreatedBy { get; set; }


        public DateTime UpdatedOn { get; set; }


        public long UpdatedBy { get; set; }


        [Display(Name = "EMPLOYEE PHOTO")]
        public string EmpPhoto { get; set; }


        [Display(Name = "GENDER")]
        //[Required(ErrorMessage = "Gender can't be blank!")]
        //[MaxLength(30)]
        public string Gender { get; set; }



        public string SubCompanyName { get; set; } // Only for display
        public string DomainName { get; set; } // Only for display

    }


    public class Employee_table_page_Model
    {
        public List<Employee_table_Model> Employee_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

    public class test_employee
    {
        
        public string EmpPhoto { get; set; }

        public long SLNO { get; set; }

        public long CreatedBy { get; set; }


    }


    public class UserProfile_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name = "DESIGNATION")]
        //[Required(ErrorMessage = "Designation can't be blank!")]
        //[MaxLength(100)]
        public string Designation { get; set; }



        [Display(Name = "LOCATION")]
        // [Required(ErrorMessage = "Location can't be blank!")]
        //[MaxLength(100)]
        public string Location { get; set; }

        
        [Display(Name = "DATE OF BIRTH")]
        //[Required(ErrorMessage = "Date Of Birth can't be blank!")]
        public DateTime Dob { get; set; }


        public DateTime UpdatedOn { get; set; }


        public long UpdatedBy { get; set; }


        [Display(Name = "EMPLOYEE PHOTO")]
        public string EmpPhoto { get; set; }



        public string SubCompanyName { get; set; } // Only for display
        public string DomainName { get; set; } // Only for display


        public User_Delegation_Table_Model User_Delegation_Table_Model { get; set; }

    }




    public class Employees_Reporting
    {
        public string Employee_ID { get; set; }
        public string Employee_Name { get; set; }
        public string Title { get; set; }
        public string ReportTo { get; set; }
    }


}
