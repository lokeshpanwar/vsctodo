﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Form_Item_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage ="Form Id can't be blank!")]
        public long Form_ID { get; set; }


        [MaxLength(50)]
        [Required(ErrorMessage = "Form Item Type can't be blank!")]
        public string Form_ItemType { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "Form Item Value can't be blank!")]
        public string Form_ItemValue { get; set; }



        public bool isMulti { get; set; }


        public bool isMandatory { get; set; }


        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }
        

        public DateTime DeletedOn { get; set; }


        public long DeletedBy { get; set; }


        public bool IsHorizAllign { get; set; }



        public string FormName { get; set; }

    }


    public class Form_Item_table_page_Model
    {
        public List<Form_Item_Table_Model> Form_Item_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
