﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DMS_Report_DocRevision_Model
    {
        public long UserId { get; set; }
        public long DocClassId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public List<DocRevision> DocRevision_List { get; set; }
    }

    public class DocRevision
    {
        public long SLNO { get; set; }
        public long DocumentId { get; set; }
        public string RevisionId { get; set; }
        public string Action { get; set; }
        public string UserId { get; set; }
        public DateTime DateTime1 { get; set; }
        public string DocumentClassName { get; set; }
        public long DocumentClassId { get; set; }
        public string FileName { get; set; }
        public string Comments { get; set; }
    }
}
