﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class TimeSheet_Model
    {
    }
    public class FilledTimesheetData
    {
        public List<FilledTimesheetCal> CalFillList { get; set; }
        public List<CurrentMonth> CrMonthList { get; set; }
    }
    public class FilledTimesheetCal
    {
        public string Dates { get; set; }
        public string Days { get; set; }
        public string Months { get; set; }
        public string Years { get; set; }
        public string IsTimesheetfill { get; set; }
        public int isholiday { get; set; }
        public string Remarks { get; set; }

        public string Newdates { get; set; }

    }
    public class CurrentMonth
    {
        public string CrMonth { get; set; }
    }
    public class Filltimesheet_Model
    {
        public int UpdateID { get; set; }
        public string HdBalancetobefill { get; set; }
        public string Date { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int SubActivityId { get; set; }
        public string SubActivityName { get; set; }
        public string Remarks { get; set; }
        public string NOHH { get; set; }
        public string NOMM { get; set; }
        public string NoOfHours { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string TotalTime { get; set; }
        public string Balancetobefill { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }

    public class TimesheetGrid
    {
        public string SheetId { get; set; }
        public string Date { get; set; }
        public string name { get; set; }
        public string clientname { get; set; }
        public string Activity { get; set; }
        public string SubActivity { get; set; }
        public string TimeTaken { get; set; }
        public string Remarks { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string Costhr { get; set; }
        public string TotalCost { get; set; }
    }

    public class SelfTimeSheet
    {
        public List<TimesheetGrid> TimsheetGridData { get; set; }
        public string Totaltime { get; set; }
    }

    public class CalFilledTimeSheet
    {
        public List<Casualtimesheet> CasualTimeList { get; set; }
    }

    public class Casualtimesheet
    {

        public string DateofCal { get; set; }

        public string Day { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }

        public string FDay { get; set; }

        public string FMonth { get; set; }

        public string FYear { get; set; }

        public string TDay { get; set; }

        public string TMonth { get; set; }

        public string TYear { get; set; }

        public string istimesheetfill { get; set; }


        public string Other { get; set; }

        public string Remarks { get; set; }

    }

    public class AdminViewTimesheet
    {
        public int OfficeId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int SubActivityId { get; set; }
        public string SubActivityName { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public string Date { get; set; }
        public string TotalTime { get; set; }
        public string Remarks { get; set; }
        public string CostHr { get; set; }
        public string TotalCost { get; set; }
    }
    public class AdminTimeSheetOfUsers
    {
        public List<AdminViewTimesheet> TimeSheetGrid { get; set; }
        public string Totaltime { get; set; }
        public string TotalCost { get; set; }
    }

    public class LeaveData
    {
        public long LId { get; set; }
        public string Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string DayCount { get; set; }
        public string Remarks { get; set; }
        public string LeaveRequestStatus { get; set; }
        public int LeaveStatus { get; set; }
        public string TotalLeaves { get; set; }
        public long UserId { get; set; }
    }

    public class Leave_Model
    {
        public long UpdateID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Remarks { get; set; }
        public string NoOfDays { get; set; }
        public string IPAddress { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
    }


    public class MISReport_Model
    {
        public List<MISReportGrid> ReportGridData { get; set; }
        public string Totaltime { get; set; }
    }
    public class MISReportGrid
    {
        public string Name { get; set; }
        public string Hours { get; set; }
    }
}
