﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class LodgeClaim_Model
    {
        public long ClaimId { get; set; }
        public long PolicyId { get; set; }
        public string ClaimOfficer { get; set; }
        public string ClaimAmount { get; set; }
        public string ClaimDate { get; set; }
        public string ClaimNo { get; set; }
        public string TPName { get; set; }
        public string PatientName { get; set; }
        public string Mobile { get; set; }
        public string ClaimFileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }
        public int IsActive { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }
        public string TPSurveyor { get; set; }
        public string TPSurveyorContactNo { get; set; }

    }

    public class ViewLodgeClaim_Model
    {
        public long ClaimId { get; set; }
        public long PolicyId { get; set; }
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CommissionRate { get; set; }
        public string ExpiryDate { get; set; }
        public string PolicyNo { get; set; }
        public string ClaimFileName { get; set; }
        public int ClaimStatus { get; set; }
        public int ClaimType { get; set; }
        public string ClaimDate { get; set; }
        public int ClaimStepId { get; set; }
        public string ClaimStepName { get; set; }
        public string TPSurveyor { get; set; }
        public string TPSurveyorContactNo { get; set; }
    }

    public class ClaimStepsDetails_Model
    {
        public long Id { get; set; }
        public long ClaimId { get; set; }
        public int StepId { get; set; }
        public string StepName { get; set; }
        public string StepDate { get; set; }
        public string UserName { get; set; }
        public string Remark { get; set; }
    }
}
