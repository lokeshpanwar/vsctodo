﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Theme_table_Model
    {
        [Required(ErrorMessage = "Theme Name can't be blank!")]
        [MaxLength(100)]
        public string Theme_Name { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }
    }
}
