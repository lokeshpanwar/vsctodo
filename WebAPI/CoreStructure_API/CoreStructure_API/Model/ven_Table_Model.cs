﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class ven_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name ="USER ID")]
        [Required(ErrorMessage ="User Id can't be blank!")]
        public long User_id { get; set; }


        [Display(Name = "COMPANY NAME")]
        [Required(ErrorMessage = "company Name can't be blank!")]
        [MaxLength(50)]
        public string CompanyName { get; set; }



        [Display(Name = "URL")]
        [Required(ErrorMessage = "URL can't be blank!")]
        [MaxLength(100)]
        public string Url { get; set; }


        [Display(Name = "ADDRESS")]
        [Required(ErrorMessage = "Address can't be blank!")]
        [MaxLength(1000)]
        public string Address { get; set; }



        [Display(Name = "LEGAL STATUS")]
        [Required(ErrorMessage = "Legal Status can't be blank!")]
        [MaxLength(50)]
        public string LegalStatus { get; set; }



        [Display(Name = "REMARKS")]
        [Required(ErrorMessage = "Remarks can't be blank!")]
        [MaxLength(1000)]
        public string Remarks { get; set; }


        [Display(Name = "COUNTRY")]
        [Required(ErrorMessage = "Country can't be blank!")]
        [MaxLength(50)]
        public string BCountry { get; set; }


        [Display(Name = "SWIFT CODE")]
        [Required(ErrorMessage = "Swift Code can't be blank!")]
        [MaxLength(50)]
        public string SwiftCode { get; set; }


        [Display(Name = "EXTERNAL USER BANK ACCOUNT NO.")]
        [Required(ErrorMessage = "External User Bank Account No. can't be blank!")]
        [MaxLength(50)]
        public string ExtUserBankAcNo { get; set; }


        [Display(Name = "ACCOUNT HOLDER NAME")]
        [Required(ErrorMessage = "Account Holder Name can't be blank!")]
        [MaxLength(100)]
        public string AcHolderName { get; set; }


        [Display(Name = "IBAN NO.")]
        [Required(ErrorMessage = "IBAN No. can't be blank!")]
        [MaxLength(50)]
        public string IBANNO { get; set; }


        [Display(Name = "BANK NAME")]
        [Required(ErrorMessage = "Bank Name can't be blank!")]
        [MaxLength(100)]
        public string BankName { get; set; }


        [Display(Name = "IFSC CODE")]
        [Required(ErrorMessage = "IFSC Code can't be blank!")]
        [MaxLength(50)]
        public string IFSC_Code { get; set; }


        [Display(Name = "BANK BRANCH")]
        [Required(ErrorMessage = "Bank Branch can't be blank!")]
        [MaxLength(50)]
        public string BANK_BRANCH { get; set; }


        [Display(Name = "HOUSE NO BANK")]
        [Required(ErrorMessage = "House No Bamk can't be blank!")]
        [MaxLength(50)]
        public string HOUSE_No_BANK { get; set; }


        [Display(Name = "STREET NAME")]
        [Required(ErrorMessage = "Street Name can't be blank!")]
        [MaxLength(50)]
        public string STREET_NAME { get; set; }


        [Display(Name = "CITY NAME")]
        [Required(ErrorMessage = "City Name can't be blank!")]
        [MaxLength(50)]
        public string CITY_NAME { get; set; }


        [Display(Name = "CITY POSTAL CODE")]
        [Required(ErrorMessage = "City Postal Code can't be blank!")]
        [MaxLength(50)]
        public string CITY_POSTAL_CODE { get; set; }


        [Display(Name = "REGION STATE")]
        [Required(ErrorMessage = "Region State can't be blank!")]
        [MaxLength(50)]
        public string REGION_STATE { get; set; }


        [Display(Name = "IS DASHBOARD")]
        [Required(ErrorMessage = "Is Dashboard can't be blank!")]
        public bool IsDashboard { get; set; }


        [Display(Name = "BANK TEL NO.")]
        [Required(ErrorMessage = "Bank Tel No. can't be blank!")]
        [MaxLength(50)]
        public string BankTelNo { get; set; }


        [Display(Name = "BANK MOBILE NO.")]
        [Required(ErrorMessage = "Bank Mobile No. can't be blank!")]
        [MaxLength(50)]
        public string BANKMOBILENo { get; set; }


        [Display(Name = "BANK FAX NO")]
        [Required(ErrorMessage = "Bank Fax No. can't be blank!")]
        [MaxLength(50)]
        public string BANKFAXNo { get; set; }


        [Display(Name = "BANK EMAIL")]
        [Required(ErrorMessage = "Bank Email can't be blank!")]
        [MaxLength(50)]
        public string BANKEMAIL { get; set; }



        

    }



    public class ven_Mapper_page_Model
    {
        public List<Vendor_Mapper_Model> Vendor_Mapper_Model_List { get; set; }

        public int TotalRows { get; set; }
    }



    public class Vendor_Mapper_Model
    {
        public ven_Table_Model ven_Table_Model { get; set; }

        public Employee_table_Model Employee_table_Model { get; set; }
    }

}
