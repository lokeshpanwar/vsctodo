﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class ProSerSkillMap_Model
    {
        [Key]
        public long EntryID { get; set; }
        [Required(ErrorMessage = "ProSerId can't be blank!")]
        public long ProSerId { get; set; }


        [Required(ErrorMessage = "VendorId can't be blank!")]

        public long VendorId { get; set; }

        public float Rating { get; set; }

        public DateTime creationdate { get; set; }


        public string creationuser { get; set; }


        public DateTime modifactiondate { get; set; }


        public string modifactionuser { get; set; }


        public bool isdeleted { get; set; }

        public string EmployeeName { get; set; }
        public string ServiceProductName { get; set; }
    }


    public class ProSerSkillMap_page_Model
    {
        public List<ProSerSkillMap_Model> ProSerSkillMap_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
