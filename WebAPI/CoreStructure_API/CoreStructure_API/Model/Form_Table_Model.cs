﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Form_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name = "COMPANY")]
        [Required(ErrorMessage = "Company can't be blank!")]
        public long Sub_CompID { get; set; }


        [Display(Name = "DOMAIN")]
        [Required(ErrorMessage = "Domain can't be blank!")]
        public long Domain_ID { get; set; }


        [MaxLength(100)]
        [Display(Name = "FORM NAME")]
        [Required(ErrorMessage = "Form Name can't be blank!")]
        public string FormName { get; set; }


        [MaxLength(250)]
        [Display(Name = "FORM DESCRIPTION")]
        [Required(ErrorMessage = "Form Description can't be blank!")]
        public string FormDescription { get; set; }


        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


        public long DeletedBy { get; set; }


        public bool IsMapped { get; set; }


        public string SubCompanyName { get; set; }
        public string Domain_Name { get; set; }
    }



    public class Form_table_page_Model
    {
        public List<Form_Table_Model> Form_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
