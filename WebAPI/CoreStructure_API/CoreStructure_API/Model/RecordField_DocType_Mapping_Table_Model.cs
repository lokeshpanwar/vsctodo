﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class RecordField_DocType_Mapping_Table_Model
    { 
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage ="Document Class can't be blank!")]
        public long Document_ClassID { get; set; }


        [Required(ErrorMessage = "Document Type can't be blank!")]
        public long DocumentType_ID { get; set; }


        [Required(ErrorMessage = "Record Field can't be blank!")]
        public long RecordField_ID { get; set; }



        public string CreatedBy { get; set; }
        
        public DateTime CreatedOn { get; set; }
               
        public string ModifiedBy { get; set; }
        
        public DateTime ModifiedOn { get; set; }
        
        public bool IsDeleted { get; set; }
               
        public string DeletedBy { get; set; }
        
        public DateTime DeletedOn { get; set; }


        public string DCClassName { get; set; }
        public string DCDocumentTypeName { get; set; }
        public string DCRecordFieldName { get; set; }

    }



    public class RecordField_DocType_Mapping_Table_page_Model
    {
        public List<RecordField_DocType_Mapping_Table_Model> RecordField_DocType_Mapping_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }


}
