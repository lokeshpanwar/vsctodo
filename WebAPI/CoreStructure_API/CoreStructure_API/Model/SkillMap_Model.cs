﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class SkillMap_Model
    {

        [Key]
        public long entryid { get; set; }
        [Required(ErrorMessage = "Skillid can't be blank!")]
        public long Skillid { get; set; }


        [Required(ErrorMessage = "Empid can't be blank!")]
        [MaxLength(100)]
        public string Empid { get; set; }

        public float Rating { get; set; }

        public DateTime creationdate { get; set; }


        public string creationuser { get; set; }


        public DateTime modifactiondate { get; set; }


        public string modifactionuser { get; set; }


        public bool isdeleted { get; set; }

        public string EmployeeName { get; set; }
        public string SkillName { get; set; }
    }

    public class SkillMap_page_Model
    {
        public List<SkillMap_Model> SkillMap_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
