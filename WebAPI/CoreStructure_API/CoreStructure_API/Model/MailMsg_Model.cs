﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class MailMsg_Model
    {
        public List<CategoryWiseReport> CatReportList_Jaipur { get; set; }
        public List<CompanyWiseReport> CompReportList_Jaipur { get; set; }
        public List<CountReports> ReportCount_Jaipur { get; set; }
        public List<CategoryWiseReport> CatReportList_Delhi { get; set; }
        public List<CompanyWiseReport> CompReportList_Delhi { get; set; }
        public List<CountReports> ReportCount_Delhi { get; set; }
    }
    public class CategoryWiseReport
    {
        public string PremiumPayable { get; set; }
        public string Category { get; set; }
    }
    public class CompanyWiseReport
    {
        public string InsCompany { get; set; }
        public string PremiumPayable { get; set; }
    }
    public class CountReports
    {
        public string PolicyUW { get; set; }
        public string PolicyGenenrate { get; set; }
        public string PolicyScan { get; set; }
        public string PolicyDeliver { get; set; }
    }

    public class Msg_Model
    {
        public List<Remind_Msg_Model> RemindMsgList { get; set; }
    }
    public class Remind_Msg_Model
    {
        public long PolicyId { get; set; }
        public string MobileNo { get; set; }
        public string MsgText { get; set; }
        public int SmsDays { get; set; }
        public string Error { get; set; }
    }
}
