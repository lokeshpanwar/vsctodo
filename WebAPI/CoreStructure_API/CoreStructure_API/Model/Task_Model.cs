﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Task_Model
    {
        public int Id { get; set; }
        public int Todoid { get; set; }
        public string Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int CreateBy { get; set; }
        public int PriorityId { get; set; }
        public string Priority { get; set; }

        public string PriorityText { get; set; }
        public string VerticalText { get; set; }
        public string EmployeeText { get; set; }
        public int Selfassign { get; set; }
        public int AssignToId { get; set; }
        public string AssignTo { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public int TotalTask { get; set; }
        public int ClientId { get; set; }
        public int ActivityId { get; set; }
        public int SubActivityId { get; set; }

        public string CreateOn { get; set; }
        public string AssignBy { get; set; }
        public int Verticals { get; set; }
        public int FrequencyId { get; set; }

        public int CategoryId { get; set; }
        public int SubcategoryId { get; set; }
        public string Frequency { get; set; }
        public int Isaccepted { get; set; }

        public int IsActiveaction { get; set; }
        public int Projectid { get; set; }
        public int Moduleid { get; set; }
        public int IsReassign { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string Name { get; set; }
        //public int Todoid { get; set; }
    }
}
