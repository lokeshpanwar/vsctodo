﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class WorkFlowUserReport_Model
    {
        public long SLNO { get; set; }

        public long workFlowID { get; set; }

        public string workFlowName { get; set; }

        public long TotalDays { get; set; }

        public string workFlowLevel { get; set; }


        public string bucketing { get; set; }

        public string priority { get; set; }

        public string description { get; set; }

        public string RASCI { get; set; }


        public long DocCompanyID { get; set; }

        public long DocDomainID { get; set; }

        public long DocClassID { get; set; }

        public bool activeStatus { get; set; }

        public DateTime createdOn { get; set; }

        public long createdBy { get; set; }

        public DateTime editedOn { get; set; }

        public long editedBy { get; set; }

        public bool isDeleted { get; set; }

        public DateTime deletedOn { get; set; }

        public long deletedBy { get; set; }

        public List<WorkFlowUserReportStage_Model> WorkFlowUserReportStage_Model_List { get; set; }
    }

    public class WorkFlowUserReportStage_Model
    {

        public long SLNO { get; set; }

        public long workFlowID { get; set; }

        public long threadID { get; set; }

        public string stageOrderID { get; set; }

        public string stageName { get; set; }

        public string stageDescription  { get; set; }


        public string stageDays { get; set; }

        public string people { get; set; }

        public string peopleInfo { get; set; }

        public string RASCI { get; set; }

        public string attachment { get; set; }

        public string url { get; set; }

        public string form { get; set; }

        public string checklist { get; set; }

        public string approverWorkFlow { get; set; }

        public string processYes { get; set; }

        public string processNo { get; set; }

        public string createdBy { get; set; }

        public DateTime createdOn { get; set; }

        public string editedBy { get; set; }

        public DateTime editedOn { get; set; }

        public bool isDeleted { get; set; }

        public string deletedBy { get; set; }

        public DateTime deletedOn { get; set; }

        public bool activeStatus { get; set; }

        public bool IsAllpeople { get; set; }

        public long PeopleID { get; set; }

        public string Viewright { get; set; }

        public string communicateTo { get; set; }

        public bool IsDelegated { get; set; }

        public string DelegatedTo { get; set; }

        public string DynRef { get; set; }

        public string sapForm { get; set; }

        public long DynGroupId { get; set; }

        public string formTemplate { get; set; }

        public bool isOpen { get; set; }
        public bool isClosed { get; set; }

        public bool isWip{ get; set; }

    }


    public class WorkFlowUserException_Model
    {
        public string workFlowID { get; set; }
        public string thrdID { get; set; }
        public string stageOrder { get; set; }
        public string raisedTo { get; set; }
        public string raisedMsg { get; set; }
        public string raisedBy { get; set; }
        public string comTo { get; set; }
        public string msg { get; set; }

        public string peopleType { get; set; }
    }
}
