﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class Document_Revision_Log_Table_Model
    {

        [Key]
        public long SLNO { get; set; }
        
        //public long Parent_CompID { get; set; }
        
        //public long Sub_CompID { get; set; }
        
        //public long UserID { get; set; }

        public DateTime Datetime { get; set; }

        public string RevisionID { get; set; }
        public string Action { get; set; }


       // public long DocumentClassID { get; set; }
        
        public string DocumentClassName { get; set; }

        public string Filename { get; set; }

        public string Comments { get; set; }
    }
}
