﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class AnnouncementTable_Model
    {
        [Key]
        public long SLNO { get; set; }
        public long Sub_CompID { get; set; }

        [Required]
        public string AnnouncementTitle { get; set;}

        
        public string AnnouncementDetail { get; set; }

        public DateTime ExpiryDate { get; set; }

        public long AnnouncemnetDomain { get; set; }

        public DateTime CreatedOn { get; set; }

        public long CreatedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public long ModifiedBy { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime DeletedOn { get; set; }

        public long DeletedBy { get; set; }

        public string CompanyName { get; set; }
        public string DomainName { get; set; }
    }

    public class AnnouncementTable_page_Model
    {
        public List<AnnouncementTable_Model> AnnouncementTable_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
