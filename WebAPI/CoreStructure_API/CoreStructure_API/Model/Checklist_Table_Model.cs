﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Checklist_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long SubComp_ID { get; set; }


        public long Domain_ID { get; set; }


        [Required(ErrorMessage ="Check List Name can't be blank!")]
        [MaxLength(100)]
        public string Checklist_Name { get; set; }


        public string Checklist_Items { get; set; }


        public long Checklist_Itemcount { get; set; }


        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime DeletedOn { get; set; }


        public bool IsMaped { get; set; }


        public string CLCompanyName { get; set; }
        public string CLDomainName { get; set; }

    }


    


    public class Checklist_Table_page_Model
    {
        public List<Checklist_Table_Model> Checklist_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }


}
