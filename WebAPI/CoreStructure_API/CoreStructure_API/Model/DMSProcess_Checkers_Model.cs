﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DMSProcess_Checkers_Model
    {
        [Key]
        public long SLNO { get; set; }

        
        public long DMSProceesID { get; set; }
        

        public long User_ID { get; set; }

        [MaxLength(200)]
        public string Action { get; set; }


        [MaxLength(1000)]
        public string Comments { get; set; }


        public DateTime CreatedOn { get; set; }


        public DateTime ModifiedOn { get; set; }

    }
}
