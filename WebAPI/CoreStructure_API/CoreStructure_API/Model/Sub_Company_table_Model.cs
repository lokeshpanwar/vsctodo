﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Sub_Company_table_Model
    {

        [Key]
        public long SLNO { get; set; }

        [Display(Name = "PARENT COMPANY NAME")]
        [Required(ErrorMessage = "Parent Company can't be blank!")]
        public long Parent_compID { get; set; }



        [Display(Name = "PARENT COMPANY NAME")]
        [MaxLength(100)]
        public string Parent_compName { get; set; }


        [Display(Name = "SUB COMPANY ID")]
        [Required(ErrorMessage = "Sub Company Id can't be blank!")]
        [MaxLength(200)]
        public string SubCompanyID { get; set; }


        [Display(Name = "SUB COMPANY NAME")]
        [Required(ErrorMessage = "Sub Company Name can't be blank!")]
        [MaxLength(200)]
        public string SubCompanyName { get; set; }


        [Display(Name = "ADDRESS")]
        [Required(ErrorMessage = "Address can't be blank!")]
        [MaxLength(250)]
        public string Address { get; set; }



        [Display(Name = "CONTACT NO")]
        [Required(ErrorMessage = "Contact No can't be blank!")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid contact number")]
        public long Contact_No { get; set; }


        [Display(Name = "CONTACT PERSON")]
        [Required(ErrorMessage = "Contact Person can't be blank!")]
        [MaxLength(100)]
        public string ContactPerson { get; set; }


        [Display(Name = "EMAIL")]
        [Required(ErrorMessage = "Email can't be blank!")]
        [MaxLength(100)]
        public string Email { get; set; }



        public long CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }


        public long UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }

        public DateTime DeletedOn { get; set; }


        public bool IsParentCompany { get; set; }

        public string CountryCode { get; set; }

    }


    public class Sub_Company_table_page_Model
    {
        public List<Sub_Company_table_Model> Sub_Company_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
