﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class PieChartReportViewModel
    {
        public string DataName { get; set; }
        public int Count { get; set; }
    }
}
