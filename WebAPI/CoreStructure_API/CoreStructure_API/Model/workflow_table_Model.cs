﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class workflow_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage = "Company Id can't be blank!")]
        public long subCmpID { get; set; }


        [Required(ErrorMessage = "Domain Id can't be blank!")]
        public long Fnc_Domain { get; set; }


        [Required(ErrorMessage ="Process Id can't be blank!")]
        public long workFlowID { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "Process Name can't be blank!")]
        public string workFlowName { get; set; }


        [Required(ErrorMessage = "Process Total Days can't be blank!")]
        public long TotalDays { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "Work Flow Level can't be blank!")]
        public string workFlowLevel { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "Bucketing can't be blank!")]
        public string bucketing { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "Priority can't be blank!")]
        public string priority { get; set; }


        [Required(ErrorMessage = "Process Description can't be blank!")]
        public string description { get; set; }


        [MaxLength(500)]
        [Required(ErrorMessage = "RASCI can't be blank!")]
        public string RASCI { get; set; }


        [Required(ErrorMessage = "Document Company ID can't be blank!")]
        public long DocCompanyID { get; set; }


        [Required(ErrorMessage = "Documnet Domain ID can't be blank!")]
        public long DocDomainID { get; set; }


        [Required(ErrorMessage = "Document Class can't be blank!")]
        public long DocClassID { get; set; }



        public bool activeStatus { get; set; }



        public DateTime createdOn { get; set; }


        public long createdBy { get; set; }


        public DateTime editedOn { get; set; }


        public long editedBy { get; set; }


        public bool isDeleted { get; set; }


        public DateTime deletedOn { get; set; }


        public long deletedBy { get; set; }



        public string BPCompanyName { get; set; }
        public string BPDomainName { get; set; }
        public string DocCompanyName { get; set; }
        public string DocDomainName { get; set; }
        public string DocClassName { get; set; }



        [MaxLength(500)]
        public string docClass { get; set; }


        [MaxLength(500)]
        public string owner { get; set; }
        

    }



    public class workflow_table_page_Model
    {
        public List<workflow_table_Model> workflow_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
