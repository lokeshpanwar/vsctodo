﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DMSProcess_Model
    {
        [Key]
        public long DMSProceesID { get; set; }


        public long Maker { get; set; }


        public long Doc_ID { get; set; }


        public float ProcessValue { get; set; }


        public DateTime CreatedOn { get; set; }


        public DateTime ModifiedOn { get; set; }


        public DateTime ModifiedBy { get; set; }


        public bool Status { get; set; }


        public bool IsActive { get; set; }


        [MaxLength(500)]
        public string PageUrl { get; set; }

    }
}
