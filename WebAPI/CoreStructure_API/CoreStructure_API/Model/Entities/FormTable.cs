﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class FormTable
    {
        public FormTable()
        {
            FormItemTable = new HashSet<FormItemTable>();
        }

        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public bool? IsMapped { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DomainTable Domain { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<FormItemTable> FormItemTable { get; set; }
    }
}
