﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowProgressTable
    {
        public long Slno { get; set; }
        public long? ThrdId { get; set; }
        public long? WfId { get; set; }
        public long? StgId { get; set; }
        public string PeopleType { get; set; }
        public string PersonId { get; set; }
        public string FormId { get; set; }
        public string FormData { get; set; }
        public string ChkId { get; set; }
        public string ChkData { get; set; }
        public string ProcessStatus { get; set; }
        public string Comments { get; set; }
        public bool? ComplitionStatus { get; set; }
        public bool? ThreadStatus { get; set; }
        public DateTime? RecvDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public bool? SubmissionStatus { get; set; }
        public string SubmittedBy { get; set; }
        public string CommunicateTo { get; set; }
        public string DefaultFile { get; set; }
        public bool? IsKillRights { get; set; }
        public bool? IsComments { get; set; }

        public WorkflowTable Wf { get; set; }
    }
}
