﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentSetMaster
    {
        public DocumentSetMaster()
        {
            DocumentSetDocClass = new HashSet<DocumentSetDocClass>();
            DocumentSetDocTypes = new HashSet<DocumentSetDocTypes>();
            DocumentSetFiles = new HashSet<DocumentSetFiles>();
            DocumentSetPermission = new HashSet<DocumentSetPermission>();
        }

        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public string DocumentSetName { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsExpire { get; set; }
        public DateTime? IsExpireDate { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public DomainTable Domain { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<DocumentSetDocClass> DocumentSetDocClass { get; set; }
        public ICollection<DocumentSetDocTypes> DocumentSetDocTypes { get; set; }
        public ICollection<DocumentSetFiles> DocumentSetFiles { get; set; }
        public ICollection<DocumentSetPermission> DocumentSetPermission { get; set; }
    }
}
