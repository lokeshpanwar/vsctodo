﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastServPro
    {
        public long EntryId { get; set; }
        public long ProSerid { get; set; }
        public string SerProductname { get; set; }
        public string SerDescription { get; set; }
        public int? MastProSerTypeId { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
