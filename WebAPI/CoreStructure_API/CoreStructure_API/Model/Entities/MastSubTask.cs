﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastSubTask
    {
        public long SubTaskId { get; set; }
        public long? TaskCreateId { get; set; }
        public string SubTaskName { get; set; }
        public string SubTaskCreatedBy { get; set; }
        public string SubTaskDescription { get; set; }
        public string AssignedTo { get; set; }
        public string AssignedId { get; set; }
        public string RasciValue { get; set; }
        public string WorkflowValue { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public string AttachmentType { get; set; }
        public string AttachmentPath { get; set; }
        public string ReportingEmpId { get; set; }
        public int? TaskStatusId { get; set; }
        public long? SkillId { get; set; }
        public int? TaskStatusEmpId { get; set; }
        public int? ApprovalStatusId { get; set; }
        public int? TaskOrderId { get; set; }
        public int? MastCheckListId { get; set; }
        public bool? ChecklistDoneStatus { get; set; }
        public bool? NotifyViewStatus { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
        public DateTime? TaskCompDate { get; set; }
        public long? Workingdays { get; set; }
    }
}
