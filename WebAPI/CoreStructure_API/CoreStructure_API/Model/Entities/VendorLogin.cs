﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class VendorLogin
    {
        public long Entryid { get; set; }
        public long? Mastvendorid { get; set; }
        public int? Masttaskid { get; set; }
        public long? Subtaskid { get; set; }
        public string Userid { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public string Creationuser { get; set; }
    }
}
