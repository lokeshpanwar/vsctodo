﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentSetFiles
    {
        public long Id { get; set; }
        public long? SetId { get; set; }
        public long? DocId { get; set; }

        public DocumentSetMaster Set { get; set; }
    }
}
