﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class InvitationAnswer
    {
        public long EntryId { get; set; }
        public long? TaskCreateId { get; set; }
        public int? TaskAnswer { get; set; }
        public string AsnwerBy { get; set; }
        public DateTime? Creationdate { get; set; }
        public string Creationuser { get; set; }
        public string OwnerType { get; set; }
    }
}
