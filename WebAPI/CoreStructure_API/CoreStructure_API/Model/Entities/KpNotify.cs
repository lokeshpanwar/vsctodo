﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class KpNotify
    {
        public long NotifactionId { get; set; }
        public long? TaskCreateId { get; set; }
        public DateTime? Notifydate { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
