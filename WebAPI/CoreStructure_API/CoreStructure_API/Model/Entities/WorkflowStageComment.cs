﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowStageComment
    {
        public long CommentId { get; set; }
        public string Action { get; set; }
        public long? StageOrderId { get; set; }
        public long? WorkFlowId { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
