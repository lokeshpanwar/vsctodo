﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class SurvayAnswer
    {
        public long EntryId { get; set; }
        public long? TaskCreateId { get; set; }
        public long? SurveyId { get; set; }
        public string TaskAnswer { get; set; }
        public string AsnwerBy { get; set; }
        public string CommentBy { get; set; }
        public string Attachmentpath { get; set; }
        public DateTime? Creationdate { get; set; }
        public string Creationuser { get; set; }
        public string Ownertype { get; set; }
    }
}
