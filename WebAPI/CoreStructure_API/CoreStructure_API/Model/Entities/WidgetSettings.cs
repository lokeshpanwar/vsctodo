﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WidgetSettings
    {
        public long Slno { get; set; }
        public long? SubCmpId { get; set; }
        public long? DomainId { get; set; }
        public long? UserId { get; set; }
        public string PieDocClass { get; set; }
        public DateTime? CreatedOn { get; set; }

        public DomainTable Domain { get; set; }
        public SubCompanyTable SubCmp { get; set; }
        public EmployeeTable User { get; set; }
    }
}
