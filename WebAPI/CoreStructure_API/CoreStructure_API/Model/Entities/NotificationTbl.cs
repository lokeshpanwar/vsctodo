﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class NotificationTbl
    {
        public long NotificationId { get; set; }
        public string Message { get; set; }
        public DateTime? CreeatedOn { get; set; }
        public bool? IsRead { get; set; }
        public long? FromUserid { get; set; }
        public long? ToUserId { get; set; }
        public string Url { get; set; }
    }
}
