﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowSapTable
    {
        public long Slno { get; set; }
        public long? SubcmpId { get; set; }
        public long? DomainId { get; set; }
        public long? SapFormId { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public long? PeopleId { get; set; }
    }
}
