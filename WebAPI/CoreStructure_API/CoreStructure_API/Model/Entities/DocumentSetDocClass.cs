﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentSetDocClass
    {
        public long Id { get; set; }
        public long? SetId { get; set; }
        public long? DocumentClassId { get; set; }

        public DocumentClassTable DocumentClass { get; set; }
        public DocumentSetMaster Set { get; set; }
    }
}
