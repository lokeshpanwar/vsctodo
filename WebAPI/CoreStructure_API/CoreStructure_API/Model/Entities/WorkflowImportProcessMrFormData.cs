﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowImportProcessMrFormData
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string SapPo { get; set; }
        public string LineItm { get; set; }
        public string SapItmCode { get; set; }
        public string SapItmDesc { get; set; }
        public string Color { get; set; }
        public string PoLnQty { get; set; }
        public string DelQty { get; set; }
        public string Balnc { get; set; }
        public string ShpQty { get; set; }
        public string UnitPrice { get; set; }
        public string Value { get; set; }
        public string BatchNo { get; set; }
        public string RollNo { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
