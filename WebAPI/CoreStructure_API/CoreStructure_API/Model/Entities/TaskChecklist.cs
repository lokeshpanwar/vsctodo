﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class TaskChecklist
    {
        public long EntryId { get; set; }
        public long? SubTaskId { get; set; }
        public long? TdlcheckId { get; set; }
        public bool? Iscompleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public string Creationuser { get; set; }
    }
}
