﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ComplianceReg
    {
        public long EntryId { get; set; }
        public long? TaskCreatedId { get; set; }
        public string CompClass { get; set; }
        public string Legislation { get; set; }
        public string Obligation { get; set; }
        public string TimeofObligation { get; set; }
        public DateTime? PeriodStartdate { get; set; }
        public DateTime? PeriodEnddate { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
