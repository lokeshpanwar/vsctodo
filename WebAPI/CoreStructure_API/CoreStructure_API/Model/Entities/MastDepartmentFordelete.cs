﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastDepartmentFordelete
    {
        public long EntryId { get; set; }
        public long MastDepartmentId { get; set; }
        public string MastDepartmentName { get; set; }
        public string DepartmentDescription { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
