﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastGroupTask
    {
        public long EntryId { get; set; }
        public int? MastTaskId { get; set; }
        public string Taskheading { get; set; }
        public string TaskCreateId { get; set; }
        public string TaskOwnerId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public string TaskDescription { get; set; }
        public bool? Isactive { get; set; }
        public string TaskOwnerType { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
