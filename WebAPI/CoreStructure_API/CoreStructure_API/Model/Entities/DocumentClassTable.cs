﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentClassTable
    {
        public DocumentClassTable()
        {
            DocClassMappingTable = new HashSet<DocClassMappingTable>();
            DocRecordedItems = new HashSet<DocRecordedItems>();
            DocUploadMaster = new HashSet<DocUploadMaster>();
            DocUploadTable = new HashSet<DocUploadTable>();
            DocumentRevisionLogTable = new HashSet<DocumentRevisionLogTable>();
            DocumentSetDocClass = new HashSet<DocumentSetDocClass>();
            DocumentSetDocTypes = new HashSet<DocumentSetDocTypes>();
            DocumentTypeDocClassMappingTable = new HashSet<DocumentTypeDocClassMappingTable>();
            RecordFieldDocClassMappingTable = new HashSet<RecordFieldDocClassMappingTable>();
            UserDocClassMappingTable = new HashSet<UserDocClassMappingTable>();
            UserDocTypeMappingTable = new HashSet<UserDocTypeMappingTable>();
            UserDocumentMappingTable = new HashSet<UserDocumentMappingTable>();
            WorkflowProcessDocs = new HashSet<WorkflowProcessDocs>();
        }

        public long Slno { get; set; }
        public long? SubCompanyId { get; set; }
        public long? ClassDomain { get; set; }
        public string DocumentClassName { get; set; }
        public string DocumentClassDesc { get; set; }
        public string AllowedExtension { get; set; }
        public bool? AccessControlStatus { get; set; }
        public bool? VersionControlStatus { get; set; }
        public string DocumentAccessBy { get; set; }
        public bool? ArchieveStatus { get; set; }
        public string DocumentTypeId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool? PrintRight { get; set; }
        public bool? EmailRight { get; set; }
        public bool? CheckinRight { get; set; }
        public bool? CheckoutRight { get; set; }
        public bool? DownloadRight { get; set; }
        public bool? UploadRight { get; set; }
        public bool? SelectallRight { get; set; }

        public DomainTable ClassDomainNavigation { get; set; }
        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public SubCompanyTable SubCompany { get; set; }
        public ICollection<DocClassMappingTable> DocClassMappingTable { get; set; }
        public ICollection<DocRecordedItems> DocRecordedItems { get; set; }
        public ICollection<DocUploadMaster> DocUploadMaster { get; set; }
        public ICollection<DocUploadTable> DocUploadTable { get; set; }
        public ICollection<DocumentRevisionLogTable> DocumentRevisionLogTable { get; set; }
        public ICollection<DocumentSetDocClass> DocumentSetDocClass { get; set; }
        public ICollection<DocumentSetDocTypes> DocumentSetDocTypes { get; set; }
        public ICollection<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTable { get; set; }
        public ICollection<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTable { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTable { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTable { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTable { get; set; }
        public ICollection<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
    }
}
