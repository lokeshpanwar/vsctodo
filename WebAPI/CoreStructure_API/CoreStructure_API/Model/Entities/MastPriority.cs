﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastPriority
    {
        public long PriorityId { get; set; }
        public string PriorityName { get; set; }
        public string PriorityDescription { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
