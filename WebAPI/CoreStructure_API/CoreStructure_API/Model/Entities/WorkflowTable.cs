﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowTable
    {
        public WorkflowTable()
        {
            WorkflowDocTables = new HashSet<WorkflowDocTables>();
            WorkflowProcessDocs = new HashSet<WorkflowProcessDocs>();
            WorkflowProgressTable = new HashSet<WorkflowProgressTable>();
        }

        public long Slno { get; set; }
        public long? SubCmpId { get; set; }
        public long? FncDomain { get; set; }
        public long? WorkFlowId { get; set; }
        public string WorkFlowName { get; set; }
        public long? TotalDays { get; set; }
        public string WorkFlowLevel { get; set; }
        public string Bucketing { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
        public string Rasci { get; set; }
        public string Owner { get; set; }
        public string DocClass { get; set; }
        public bool? ActiveStatus { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public long? EdditedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public bool? IsHide { get; set; }
        public long? DocClassId { get; set; }
        public long? EditedBy { get; set; }
        public long? DocCompanyId { get; set; }
        public long? DocDomainId { get; set; }
        public bool? IsReferenceEditable { get; set; }

        public ICollection<WorkflowDocTables> WorkflowDocTables { get; set; }
        public ICollection<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
        public ICollection<WorkflowProgressTable> WorkflowProgressTable { get; set; }
    }
}
