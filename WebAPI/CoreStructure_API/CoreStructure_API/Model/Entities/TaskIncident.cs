﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class TaskIncident
    {
        public long EntryId { get; set; }
        public long? TaskCreateId { get; set; }
        public string IncidentType { get; set; }
        public long? MastGroupId { get; set; }
        public int? MastComModeId { get; set; }
        public string Message { get; set; }
        public int? IntervalDuration { get; set; }
        public string IntervalType { get; set; }
    }
}
