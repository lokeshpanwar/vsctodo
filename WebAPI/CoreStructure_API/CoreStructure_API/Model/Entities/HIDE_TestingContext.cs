﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CoreStructure_API.Model.Entities
{
    public partial class HIDE_TestingContext : DbContext
    {
        public HIDE_TestingContext()
        {
        }

        public HIDE_TestingContext(DbContextOptions<HIDE_TestingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AclPermissionMappingTable> AclPermissionMappingTable { get; set; }
        public virtual DbSet<AclTemplateTable> AclTemplateTable { get; set; }
        public virtual DbSet<AnnouncementTable> AnnouncementTable { get; set; }
        public virtual DbSet<AuditMaster> AuditMaster { get; set; }
        public virtual DbSet<AuditMasterDetails> AuditMasterDetails { get; set; }
        public virtual DbSet<AuditTable> AuditTable { get; set; }
        public virtual DbSet<ChecklistTable> ChecklistTable { get; set; }
        public virtual DbSet<CheckoutTable> CheckoutTable { get; set; }
        public virtual DbSet<Clientmaster> Clientmaster { get; set; }
        public virtual DbSet<CompanyHoliday> CompanyHoliday { get; set; }
        public virtual DbSet<ComplianceReg> ComplianceReg { get; set; }
        public virtual DbSet<CourierTracking> CourierTracking { get; set; }
        public virtual DbSet<CpDlData> CpDlData { get; set; }
        public virtual DbSet<CpDlMapping> CpDlMapping { get; set; }
        public virtual DbSet<CpDlMappingRtemplate> CpDlMappingRtemplate { get; set; }
        public virtual DbSet<CpErpMapData> CpErpMapData { get; set; }
        public virtual DbSet<CpMesData> CpMesData { get; set; }
        public virtual DbSet<CpMileData> CpMileData { get; set; }
        public virtual DbSet<CpSimulation> CpSimulation { get; set; }
        public virtual DbSet<CpSoGroup> CpSoGroup { get; set; }
        public virtual DbSet<CraiTblExceptionError> CraiTblExceptionError { get; set; }
        public virtual DbSet<DesignationMaster> DesignationMaster { get; set; }
        public virtual DbSet<DiscussionMessageTable> DiscussionMessageTable { get; set; }
        public virtual DbSet<DiscussTable> DiscussTable { get; set; }
        public virtual DbSet<DiscussVote> DiscussVote { get; set; }
        public virtual DbSet<Dmsprocess> Dmsprocess { get; set; }
        public virtual DbSet<DmsprocessApprovers> DmsprocessApprovers { get; set; }
        public virtual DbSet<DmsprocessCheckers> DmsprocessCheckers { get; set; }
        public virtual DbSet<DocClassMappingTable> DocClassMappingTable { get; set; }
        public virtual DbSet<DocRecordedItems> DocRecordedItems { get; set; }
        public virtual DbSet<DocRecordedItemsUpdate> DocRecordedItemsUpdate { get; set; }
        public virtual DbSet<DocSensitivityTable> DocSensitivityTable { get; set; }
        public virtual DbSet<DocumentClassTable> DocumentClassTable { get; set; }
        public virtual DbSet<DocumentComentTable> DocumentComentTable { get; set; }
        public virtual DbSet<DocumentRevisionLogTable> DocumentRevisionLogTable { get; set; }
        public virtual DbSet<DocumentSetDocClass> DocumentSetDocClass { get; set; }
        public virtual DbSet<DocumentSetDocTypes> DocumentSetDocTypes { get; set; }
        public virtual DbSet<DocumentSetFiles> DocumentSetFiles { get; set; }
        public virtual DbSet<DocumentSetMaster> DocumentSetMaster { get; set; }
        public virtual DbSet<DocumentSetPermission> DocumentSetPermission { get; set; }
        public virtual DbSet<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTable { get; set; }
        public virtual DbSet<DocumentTypeTable> DocumentTypeTable { get; set; }
        public virtual DbSet<DocUploadMaster> DocUploadMaster { get; set; }
        public virtual DbSet<DocUploadTable> DocUploadTable { get; set; }
        public virtual DbSet<DomainMappingTable> DomainMappingTable { get; set; }
        public virtual DbSet<DomainTable> DomainTable { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<EmpHoliday> EmpHoliday { get; set; }
        public virtual DbSet<EmployeeProcessValue> EmployeeProcessValue { get; set; }
        public virtual DbSet<EmployeeTable> EmployeeTable { get; set; }
        public virtual DbSet<EventTable> EventTable { get; set; }
        public virtual DbSet<ExceptionError> ExceptionError { get; set; }
        public virtual DbSet<ForgotPwdTable> ForgotPwdTable { get; set; }
        public virtual DbSet<FormItemTable> FormItemTable { get; set; }
        public virtual DbSet<FormTable> FormTable { get; set; }
        public virtual DbSet<FrequencyMaster> FrequencyMaster { get; set; }
        public virtual DbSet<GroupCompany> GroupCompany { get; set; }
        public virtual DbSet<GroupDomain> GroupDomain { get; set; }
        public virtual DbSet<GroupMap> GroupMap { get; set; }
        public virtual DbSet<GroupMapping> GroupMapping { get; set; }
        public virtual DbSet<GroupTable> GroupTable { get; set; }
        public virtual DbSet<GroupUser> GroupUser { get; set; }
        public virtual DbSet<IndexTable> IndexTable { get; set; }
        public virtual DbSet<InvitationAnswer> InvitationAnswer { get; set; }
        public virtual DbSet<InvitationDetail> InvitationDetail { get; set; }
        public virtual DbSet<KpModules> KpModules { get; set; }
        public virtual DbSet<KpNotify> KpNotify { get; set; }
        public virtual DbSet<Kptable> Kptable { get; set; }
        public virtual DbSet<ListItemTable> ListItemTable { get; set; }
        public virtual DbSet<ListTable> ListTable { get; set; }
        public virtual DbSet<LocationMaster> LocationMaster { get; set; }
        public virtual DbSet<LogHistoryTable> LogHistoryTable { get; set; }
        public virtual DbSet<LoginTable> LoginTable { get; set; }
        public virtual DbSet<LogTable> LogTable { get; set; }
        public virtual DbSet<MailDomain> MailDomain { get; set; }
        public virtual DbSet<MastBuckting> MastBuckting { get; set; }
        public virtual DbSet<Mastcalendar> Mastcalendar { get; set; }
        public virtual DbSet<MastCheckList> MastCheckList { get; set; }
        public virtual DbSet<MastCommunicationMode> MastCommunicationMode { get; set; }
        public virtual DbSet<MastDepartmentFordelete> MastDepartmentFordelete { get; set; }
        public virtual DbSet<MastEmployeeFordelete> MastEmployeeFordelete { get; set; }
        public virtual DbSet<MastEmployeeTemp> MastEmployeeTemp { get; set; }
        public virtual DbSet<MastGroupFordelete> MastGroupFordelete { get; set; }
        public virtual DbSet<MastGroupTask> MastGroupTask { get; set; }
        public virtual DbSet<MastLocation> MastLocation { get; set; }
        public virtual DbSet<MastPeriodicity> MastPeriodicity { get; set; }
        public virtual DbSet<MastPriority> MastPriority { get; set; }
        public virtual DbSet<MastProSerType> MastProSerType { get; set; }
        public virtual DbSet<MastSchTask> MastSchTask { get; set; }
        public virtual DbSet<MastSector> MastSector { get; set; }
        public virtual DbSet<MastServPro> MastServPro { get; set; }
        public virtual DbSet<MastServProTemp> MastServProTemp { get; set; }
        public virtual DbSet<MastSkill> MastSkill { get; set; }
        public virtual DbSet<MastSkillTemp> MastSkillTemp { get; set; }
        public virtual DbSet<MastSubTask> MastSubTask { get; set; }
        public virtual DbSet<MastSurveyOption> MastSurveyOption { get; set; }
        public virtual DbSet<MastTask> MastTask { get; set; }
        public virtual DbSet<MastTaskEmpStatus> MastTaskEmpStatus { get; set; }
        public virtual DbSet<MastTaskStatus> MastTaskStatus { get; set; }
        public virtual DbSet<MastToDolist> MastToDolist { get; set; }
        public virtual DbSet<MastVendor> MastVendor { get; set; }
        public virtual DbSet<MastVendorTemp> MastVendorTemp { get; set; }
        public virtual DbSet<MastWorkFlow> MastWorkFlow { get; set; }
        public virtual DbSet<MastWorkFlowLevel> MastWorkFlowLevel { get; set; }
        public virtual DbSet<MessageTable> MessageTable { get; set; }
        public virtual DbSet<NotificationTbl> NotificationTbl { get; set; }
        public virtual DbSet<ParentCompanyTable> ParentCompanyTable { get; set; }
        public virtual DbSet<ProSerSkillMap> ProSerSkillMap { get; set; }
        public virtual DbSet<ProSerSkillMapTemp> ProSerSkillMapTemp { get; set; }
        public virtual DbSet<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTable { get; set; }
        public virtual DbSet<RecordFieldDocTypeMappingTable> RecordFieldDocTypeMappingTable { get; set; }
        public virtual DbSet<RoleMappingTable> RoleMappingTable { get; set; }
        public virtual DbSet<RoleModulesPermission> RoleModulesPermission { get; set; }
        public virtual DbSet<RoleTable> RoleTable { get; set; }
        public virtual DbSet<RoleVanderMappingTable> RoleVanderMappingTable { get; set; }
        public virtual DbSet<RoleVendorTable> RoleVendorTable { get; set; }
        public virtual DbSet<SkillMap> SkillMap { get; set; }
        public virtual DbSet<SkillMapTemp> SkillMapTemp { get; set; }
        public virtual DbSet<Stage1Stage> Stage1Stage { get; set; }
        public virtual DbSet<Stage2Stage> Stage2Stage { get; set; }
        public virtual DbSet<Stage3Stage> Stage3Stage { get; set; }
        public virtual DbSet<SubCompanyTable> SubCompanyTable { get; set; }
        public virtual DbSet<SubTaskFiles> SubTaskFiles { get; set; }
        public virtual DbSet<SurvayAnswer> SurvayAnswer { get; set; }
        public virtual DbSet<SurveyDetail> SurveyDetail { get; set; }
        public virtual DbSet<TaskChecklist> TaskChecklist { get; set; }
        public virtual DbSet<TaskComment> TaskComment { get; set; }
        public virtual DbSet<TaskIncident> TaskIncident { get; set; }
        public virtual DbSet<TblModules> TblModules { get; set; }
        public virtual DbSet<TblModulesForm> TblModulesForm { get; set; }
        public virtual DbSet<UserDelegationTable> UserDelegationTable { get; set; }
        public virtual DbSet<UserDocClassMappingTable> UserDocClassMappingTable { get; set; }
        public virtual DbSet<UserDocTypeMappingTable> UserDocTypeMappingTable { get; set; }
        public virtual DbSet<UserDocumentMappingTable> UserDocumentMappingTable { get; set; }
        public virtual DbSet<UserNotification> UserNotification { get; set; }
        public virtual DbSet<VendorLogin> VendorLogin { get; set; }
        public virtual DbSet<VenTable> VenTable { get; set; }
        public virtual DbSet<WidgetSettings> WidgetSettings { get; set; }
        public virtual DbSet<WorkflowCpProcessTable> WorkflowCpProcessTable { get; set; }
        public virtual DbSet<WorkflowDocTables> WorkflowDocTables { get; set; }
        public virtual DbSet<WorkflowExportProcessE1Data> WorkflowExportProcessE1Data { get; set; }
        public virtual DbSet<WorkflowExportProcessE2Data> WorkflowExportProcessE2Data { get; set; }
        public virtual DbSet<WorkflowExportProcessE3Data> WorkflowExportProcessE3Data { get; set; }
        public virtual DbSet<WorkflowImportProcessI1Data> WorkflowImportProcessI1Data { get; set; }
        public virtual DbSet<WorkflowImportProcessI2Data> WorkflowImportProcessI2Data { get; set; }
        public virtual DbSet<WorkflowImportProcessI3Data> WorkflowImportProcessI3Data { get; set; }
        public virtual DbSet<WorkflowImportProcessMrFormData> WorkflowImportProcessMrFormData { get; set; }
        public virtual DbSet<WorkflowImportSapTableData> WorkflowImportSapTableData { get; set; }
        public virtual DbSet<WorkflowPriorityMap> WorkflowPriorityMap { get; set; }
        public virtual DbSet<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
        public virtual DbSet<WorkflowProcessException> WorkflowProcessException { get; set; }
        public virtual DbSet<WorkflowProgressTable> WorkflowProgressTable { get; set; }
        public virtual DbSet<WorkflowSapTable> WorkflowSapTable { get; set; }
        public virtual DbSet<WorkflowStageComment> WorkflowStageComment { get; set; }
        public virtual DbSet<WorkflowStageTable> WorkflowStageTable { get; set; }
        public virtual DbSet<WorkflowTable> WorkflowTable { get; set; }
        public virtual DbSet<WorkflowThreadProgressTable> WorkflowThreadProgressTable { get; set; }
        public virtual DbSet<WPriorityTable> WPriorityTable { get; set; }

        // Unable to generate entity type for table 'dbo.workflow_export_carton'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.workflow_Import_process_form_data'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Theme_table'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TeamMaster'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CountryCodeMst'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SurveyTypeOne'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Data Source=tcp:charan,1434; Database=HID-E_Testing;User Id=sa; Password=gtech123;");
                optionsBuilder.UseSqlServer(Startup.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AclPermissionMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("ACL_PermissionMapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AclId).HasColumnName("ACL_ID");

                entity.Property(e => e.AssignedOn).HasColumnType("datetime");

                entity.Property(e => e.AssignedSection)
                    .HasColumnName("Assigned_Section")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AssignedTo)
                    .HasColumnName("Assigned_To")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateAdd).HasColumnName("Create_Add");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.Property(e => e.ReadView).HasColumnName("Read_View");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.WriteEdit).HasColumnName("Write_Edit");

                entity.HasOne(d => d.Acl)
                    .WithMany(p => p.AclPermissionMappingTable)
                    .HasForeignKey(d => d.AclId)
                    .HasConstraintName("FK_ACL_PermissionMapping_Table_ACL_Template_Table");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.AclPermissionMappingTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_ACL_PermissionMapping_Table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.AclPermissionMappingTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_ACL_PermissionMapping_Table_Sub_Company_table");
            });

            modelBuilder.Entity<AclTemplateTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("ACL_Template_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AclFunctionalDomainId)
                    .HasColumnName("ACL_FunctionalDomainID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AclTemplateDesc)
                    .HasColumnName("ACL_TemplateDesc")
                    .IsUnicode(false);

                entity.Property(e => e.AclTemplateName)
                    .HasColumnName("ACL_TemplateName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.AclTemplateTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_ACL_Template_Table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.AclTemplateTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_ACL_Template_Table_Sub_Company_table");
            });

            modelBuilder.Entity<AnnouncementTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Announcement_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AnnouncementDetail)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AnnouncementTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.AnnouncementTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Announcement_Table_Sub_Company_table");
            });

            modelBuilder.Entity<AuditMaster>(entity =>
            {
                entity.Property(e => e.AuditCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AuditDiscription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Createon).HasColumnType("datetime");

                entity.Property(e => e.Frequency)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Modifyon).HasColumnType("datetime");
            });

            modelBuilder.Entity<AuditMasterDetails>(entity =>
            {
                entity.Property(e => e.ActualDateofCloser).HasColumnType("datetime");

                entity.Property(e => e.Afidstatus).HasColumnName("AFIDStatus");

                entity.Property(e => e.ArtefactRef)
                    .HasColumnName("Artefact_Ref")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Artefacts)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ControlArea)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ControlDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ControlNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Createon)
                    .HasColumnName("createon")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOfUpload).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Edc)
                    .HasColumnName("EDC")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifyon)
                    .HasColumnName("modifyon")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Ncstatus).HasColumnName("NCStatus");

                entity.Property(e => e.Objective)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Observation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PurposeCommentsAuditee)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PurposeDate).HasColumnType("datetime");

                entity.Property(e => e.Recommendation)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RefDocClose)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Response)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.St3Date).HasColumnType("datetime");

                entity.Property(e => e.St3Status).HasColumnName("st3Status");

                entity.Property(e => e.St4Date).HasColumnType("datetime");

                entity.Property(e => e.St5Comment)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.St5Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<AuditTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Audit_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AuditId).HasColumnName("AuditID");

                entity.Property(e => e.AuditIssues).IsUnicode(false);

                entity.Property(e => e.AuditorTracking).IsUnicode(false);

                entity.Property(e => e.Category).IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ImplementationDate).HasColumnType("date");

                entity.Property(e => e.ManagementResponse).IsUnicode(false);

                entity.Property(e => e.Recommandation).IsUnicode(false);

                entity.Property(e => e.RiskPriority)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ChecklistTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Checklist_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ChecklistId)
                    .HasColumnName("Checklist_ID")
                    .HasComputedColumnSql("([dbo].[Checklist_ID]([SLNO]))");

                entity.Property(e => e.ChecklistItemcount)
                    .HasColumnName("Checklist_Itemcount")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChecklistItems)
                    .HasColumnName("Checklist_Items")
                    .IsUnicode(false);

                entity.Property(e => e.ChecklistName)
                    .HasColumnName("Checklist_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("SubComp_ID");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.ChecklistTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_Checklist_Table_Domain_table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.ChecklistTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Checklist_Table_Sub_Company_table");
            });

            modelBuilder.Entity<CheckoutTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Checkout_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckOutdateTime).HasColumnType("datetime");

                entity.Property(e => e.DocumentClass)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VersionId)
                    .HasColumnName("VersionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.CheckoutTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_Checkout_Table_Domain_table");
            });

            modelBuilder.Entity<Clientmaster>(entity =>
            {
                entity.ToTable("clientmaster");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Createon).HasColumnType("datetime");

                entity.Property(e => e.Details)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Modifyon).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CompanyHoliday>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.Property(e => e.CompanyId).HasMaxLength(100);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.YearEndDate).HasColumnType("date");

                entity.Property(e => e.YearStartDate).HasColumnType("date");
            });

            modelBuilder.Entity<ComplianceReg>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Legislation).HasColumnName("legislation");

                entity.Property(e => e.PeriodEnddate).HasColumnType("date");

                entity.Property(e => e.PeriodStartdate).HasColumnType("date");
            });

            modelBuilder.Entity<CourierTracking>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.ThreadId).HasColumnName("threadID");

                entity.Property(e => e.TrackingNo).HasMaxLength(100);

                entity.Property(e => e.Wfid).HasColumnName("wfid");

                entity.Property(e => e.Xmldata)
                    .HasColumnName("XMLData")
                    .HasColumnType("xml");
            });

            modelBuilder.Entity<CpDlData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_DL_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dcode)
                    .HasColumnName("DCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ddesc)
                    .HasColumnName("DDesc")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Dname)
                    .HasColumnName("DName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCoyId).HasColumnName("SubCoyID");
            });

            modelBuilder.Entity<CpDlMapping>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_DL_mapping");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Dlid).HasColumnName("DLID");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Msid).HasColumnName("MSID");

                entity.Property(e => e.Tbac).HasColumnName("TBAC");

                entity.Property(e => e.Tbd)
                    .HasColumnName("TBD")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CpDlMappingRtemplate>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_DL_mapping_Rtemplate");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bom)
                    .HasColumnName("BOM")
                    .HasMaxLength(15);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CpSimulationNo).HasColumnName("CP_Simulation_No");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Dlid).HasColumnName("DLID");

                entity.Property(e => e.Etnal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Intnal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Msid).HasColumnName("MSID");

                entity.Property(e => e.Ndate)
                    .HasColumnName("ndate")
                    .HasColumnType("date");

                entity.Property(e => e.Nod)
                    .HasColumnName("nod")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prity)
                    .HasColumnName("prity")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdate)
                    .HasColumnName("sdate")
                    .HasColumnType("date");

                entity.Property(e => e.Tbac).HasColumnName("TBAC");

                entity.Property(e => e.Tbd)
                    .HasColumnName("TBD")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CpErpMapData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_ERP_MAP_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Dec)
                    .HasColumnName("DEC")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.Erp)
                    .HasColumnName("ERP")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mes)
                    .HasColumnName("MES")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCoyId).HasColumnName("SubCoyID");
            });

            modelBuilder.Entity<CpMesData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_MES_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.Mcode)
                    .HasColumnName("MCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mdesc)
                    .HasColumnName("MDesc")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Mname)
                    .HasColumnName("MName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCoyId).HasColumnName("SubCoyID");
            });

            modelBuilder.Entity<CpMileData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_MILE_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.Mcode)
                    .HasColumnName("MCode")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Mdesc)
                    .HasColumnName("MDesc")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Mname)
                    .HasColumnName("MName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCoyId).HasColumnName("SubCoyID");
            });

            modelBuilder.Entity<CpSimulation>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_Simulation");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerName).HasMaxLength(255);

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Dlid).HasColumnName("DLId");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Mesid).HasColumnName("MESId");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RefNumber).HasMaxLength(50);

                entity.Property(e => e.Sappo)
                    .HasColumnName("SAPPO")
                    .HasMaxLength(50);

                entity.Property(e => e.SimulationType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SoTemplateName).HasMaxLength(255);

                entity.Property(e => e.StartDate).HasColumnType("date");
            });

            modelBuilder.Entity<CpSoGroup>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("CP_SO_Group");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ActivatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.GroupName).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Solist).HasColumnName("SOList");
            });

            modelBuilder.Entity<CraiTblExceptionError>(entity =>
            {
                entity.HasKey(e => e.TraceId);

                entity.ToTable("CRAI_Tbl_ExceptionError");

                entity.Property(e => e.TraceId).HasColumnName("trace_id");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("created_on")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExceptionMessage)
                    .HasColumnName("exception_message")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.InnerException)
                    .HasColumnName("inner_exception")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace)
                    .HasColumnName("stack_trace")
                    .HasMaxLength(5000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DesignationMaster>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Createdon)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Designation).HasMaxLength(100);
            });

            modelBuilder.Entity<DiscussionMessageTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Discussion_Message_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DiscussionId).HasColumnName("Discussion_ID");

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Userid)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Discussion)
                    .WithMany(p => p.DiscussionMessageTable)
                    .HasForeignKey(d => d.DiscussionId)
                    .HasConstraintName("FK_Discussion_Message_Table_Discuss_Table");
            });

            modelBuilder.Entity<DiscussTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Discuss_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DiscussionDetail).IsUnicode(false);

                entity.Property(e => e.DiscussionTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Interval).HasColumnName("interval");

                entity.Property(e => e.IsClosed).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentId).HasColumnName("ParentID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DiscussTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Discuss_Table_Sub_Company_table");
            });

            modelBuilder.Entity<DiscussVote>(entity =>
            {
                entity.ToTable("Discuss_Vote");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Creadedon)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DiscussId).HasColumnName("Discuss_ID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Discuss)
                    .WithMany(p => p.DiscussVote)
                    .HasForeignKey(d => d.DiscussId)
                    .HasConstraintName("FK_Discuss_Vote_Discuss_Table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DiscussVote)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Discuss_Vote_Employee_table");
            });

            modelBuilder.Entity<Dmsprocess>(entity =>
            {
                entity.HasKey(e => e.DmsproceesId);

                entity.ToTable("DMSProcess");

                entity.Property(e => e.DmsproceesId).HasColumnName("DMSProceesID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DocId).HasColumnName("Doc_ID");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModifiedBy).HasColumnType("date");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PageUrl).HasMaxLength(500);

                entity.Property(e => e.Status).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.MakerNavigation)
                    .WithMany(p => p.Dmsprocess)
                    .HasForeignKey(d => d.Maker)
                    .HasConstraintName("FK_DMSProcess_Employee_table");
            });

            modelBuilder.Entity<DmsprocessApprovers>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DMSProcess_Approvers");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Action).HasMaxLength(200);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DmsproceesId).HasColumnName("DMSProceesID");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.Dmsprocees)
                    .WithMany(p => p.DmsprocessApprovers)
                    .HasForeignKey(d => d.DmsproceesId)
                    .HasConstraintName("FK_DMSProcess_Approvers_DMSProcess");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DmsprocessApprovers)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_DMSProcess_Approvers_Employee_table");
            });

            modelBuilder.Entity<DmsprocessCheckers>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DMSProcess_Checkers");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Action).HasMaxLength(200);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DmsproceesId).HasColumnName("DMSProceesID");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.Dmsprocees)
                    .WithMany(p => p.DmsprocessCheckers)
                    .HasForeignKey(d => d.DmsproceesId)
                    .HasConstraintName("FK_DMSProcess_Checkers_DMSProcess");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DmsprocessCheckers)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_DMSProcess_Checkers_Employee_table");
            });

            modelBuilder.Entity<DocClassMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Doc_Class_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_Class_ID");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.RecordFieldId).HasColumnName("RecordField_ID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocClassMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Doc_Class_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DocClassMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Doc_Class_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocClassMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_Doc_Class_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.RecordField)
                    .WithMany(p => p.DocClassMappingTable)
                    .HasForeignKey(d => d.RecordFieldId)
                    .HasConstraintName("FK_Doc_Class_Mapping_Table_List_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocClassMappingTableUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Doc_Class_Mapping_Table_Employee_table");
            });

            modelBuilder.Entity<DocRecordedItems>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Doc_Recorded_Items");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_Class_ID");

                entity.Property(e => e.DocumentId).HasColumnName("Document_ID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OldValues).IsUnicode(false);

                entity.Property(e => e.RecordFieldId).HasColumnName("RecordField_ID");

                entity.Property(e => e.RecordFieldItems)
                    .HasColumnName("RecordField_Items")
                    .IsUnicode(false);

                entity.Property(e => e.SensiData)
                    .HasColumnName("sensiData")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TypeRfdata)
                    .HasColumnName("typeRFdata")
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocRecordedItemsCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Doc_Recorded_Items_Employee_table1");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocRecordedItems)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_Doc_Recorded_Items_DocumentClass_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DocRecordedItemsModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Doc_Recorded_Items_Employee_table2");

                entity.HasOne(d => d.RecordField)
                    .WithMany(p => p.DocRecordedItems)
                    .HasForeignKey(d => d.RecordFieldId)
                    .HasConstraintName("FK_Doc_Recorded_Items_List_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocRecordedItemsUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Doc_Recorded_Items_Employee_table");
            });

            modelBuilder.Entity<DocRecordedItemsUpdate>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Doc_Recorded_Items_Update");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.DocRcId).HasColumnName("Doc_RC_ID");

                entity.Property(e => e.UpdateOn).HasColumnType("date");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.DocRc)
                    .WithMany(p => p.DocRecordedItemsUpdate)
                    .HasForeignKey(d => d.DocRcId)
                    .HasConstraintName("FK_Doc_Recorded_Items_Update_Doc_Recorded_Items");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocRecordedItemsUpdate)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Doc_Recorded_Items_Update_Employee_table");
            });

            modelBuilder.Entity<DocSensitivityTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocSensitivity_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SensiCode)
                    .HasColumnName("sensiCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SensiDescrip)
                    .HasColumnName("sensiDescrip")
                    .IsUnicode(false);

                entity.Property(e => e.SensiName)
                    .HasColumnName("sensiName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SensiPercent).HasColumnName("sensiPercent");

                entity.Property(e => e.SensiType)
                    .HasColumnName("sensiType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId)
                    .HasColumnName("Sub_CompID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DocumentClassTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocumentClass_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AllowedExtension)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ArchieveStatus).HasColumnName("Archieve_Status");

                entity.Property(e => e.CheckinRight).HasColumnName("checkin_right");

                entity.Property(e => e.CheckoutRight).HasColumnName("checkout_right");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentAccessBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentClassDesc).IsUnicode(false);

                entity.Property(e => e.DocumentClassName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentTypeId)
                    .HasColumnName("DocumentType_ID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DownloadRight).HasColumnName("download_right");

                entity.Property(e => e.EmailRight).HasColumnName("email_right");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PrintRight).HasColumnName("print_right");

                entity.Property(e => e.SelectallRight).HasColumnName("selectall_right");

                entity.Property(e => e.SubCompanyId).HasColumnName("SubCompanyID");

                entity.Property(e => e.UploadRight).HasColumnName("upload_right");

                entity.HasOne(d => d.ClassDomainNavigation)
                    .WithMany(p => p.DocumentClassTable)
                    .HasForeignKey(d => d.ClassDomain)
                    .HasConstraintName("FK_DocumentClass_table_Domain_table");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocumentClassTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_DocumentClass_table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DocumentClassTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_DocumentClass_table_Employee_table2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DocumentClassTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_DocumentClass_table_Employee_table1");

                entity.HasOne(d => d.SubCompany)
                    .WithMany(p => p.DocumentClassTable)
                    .HasForeignKey(d => d.SubCompanyId)
                    .HasConstraintName("FK_DocumentClass_table_Sub_Company_table");
            });

            modelBuilder.Entity<DocumentComentTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocumentComent_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.Status)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.VersionId)
                    .HasColumnName("VersionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentComentTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_DocumentComent_Table_Employee_table");
            });

            modelBuilder.Entity<DocumentRevisionLogTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Document_Revision_Log_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Action)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.Datetime).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("DocumentClassID");

                entity.Property(e => e.DocumentClassName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.Filename).IsUnicode(false);

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.Property(e => e.RevisionId)
                    .HasColumnName("RevisionID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocumentRevisionLogTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_Document_Revision_Log_Table_DocumentClass_table");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.DocumentRevisionLogTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_Document_Revision_Log_Table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DocumentRevisionLogTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Document_Revision_Log_Table_Sub_Company_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentRevisionLogTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Document_Revision_Log_Table_Employee_table");
            });

            modelBuilder.Entity<DocumentSetDocClass>(entity =>
            {
                entity.ToTable("Document_Set_Doc_Class");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.SetId).HasColumnName("Set_ID");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocumentSetDocClass)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_DocumentSet_Doc_Class_DocumentClass_table");

                entity.HasOne(d => d.Set)
                    .WithMany(p => p.DocumentSetDocClass)
                    .HasForeignKey(d => d.SetId)
                    .HasConstraintName("FK_DocumentSet_Doc_Class_Document_Set_master");
            });

            modelBuilder.Entity<DocumentSetDocTypes>(entity =>
            {
                entity.ToTable("Document_Set_Doc_Types");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentType_ID");

                entity.Property(e => e.SetId).HasColumnName("Set_ID");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocumentSetDocTypes)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_DocumentSet_Types_DocumentClass_table");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.DocumentSetDocTypes)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_DocumentSet_Types_DocumentType_Table");

                entity.HasOne(d => d.Set)
                    .WithMany(p => p.DocumentSetDocTypes)
                    .HasForeignKey(d => d.SetId)
                    .HasConstraintName("FK_DocumentSet_Types_DocumentSet_Types");
            });

            modelBuilder.Entity<DocumentSetFiles>(entity =>
            {
                entity.ToTable("Document_Set_Files");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DocId).HasColumnName("Doc_ID");

                entity.Property(e => e.SetId).HasColumnName("Set_ID");

                entity.HasOne(d => d.Set)
                    .WithMany(p => p.DocumentSetFiles)
                    .HasForeignKey(d => d.SetId)
                    .HasConstraintName("FK_DocumentSet_Files_Document_Set_master");
            });

            modelBuilder.Entity<DocumentSetMaster>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Document_Set_master");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.DocumentSetName).HasMaxLength(500);

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.IsExpireDate).HasColumnType("date");

                entity.Property(e => e.ModifiedOn).HasColumnType("date");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocumentSetMasterCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Document_Set_master_Employee_table");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.DocumentSetMaster)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_Document_Set_master_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DocumentSetMasterModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Document_Set_master_Employee_table1");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DocumentSetMaster)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Document_Set_master_Sub_Company_table");
            });

            modelBuilder.Entity<DocumentSetPermission>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Document_Set_permission");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckinRight).HasColumnName("checkin_right");

                entity.Property(e => e.CheckoutRight).HasColumnName("checkout_right");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DownloadRight).HasColumnName("download_right");

                entity.Property(e => e.EmailRight).HasColumnName("email_right");

                entity.Property(e => e.ModifiedOn).HasColumnType("date");

                entity.Property(e => e.PrintRight).HasColumnName("print_right");

                entity.Property(e => e.SelectallRight).HasColumnName("selectall_right");

                entity.Property(e => e.SetId).HasColumnName("Set_ID");

                entity.Property(e => e.UploadRight).HasColumnName("upload_right");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.ViewRight).HasColumnName("view_right");

                entity.HasOne(d => d.Set)
                    .WithMany(p => p.DocumentSetPermission)
                    .HasForeignKey(d => d.SetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Document_Set_permission_Document_Set_master");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentSetPermission)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Document_Set_permission_Employee_table");
            });

            modelBuilder.Entity<DocumentTypeDocClassMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocumentType_DocClass_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentType_ID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocumentTypeDocClassMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_DocumentType_DocClass_Mapping_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DocumentTypeDocClassMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_DocumentType_DocClass_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.DocumentTypeDocClassMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_DocumentType_DocClass_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.DocumentTypeDocClassMappingTable)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_DocumentType_DocClass_Mapping_Table_DocumentType_Table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DocumentTypeDocClassMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_DocumentType_DocClass_Mapping_Table_Employee_table1");
            });

            modelBuilder.Entity<DocumentTypeTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocumentType_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumenTypeDesc)
                    .HasColumnName("DocumenType_desc")
                    .IsUnicode(false);

                entity.Property(e => e.DocumentTypeName)
                    .HasColumnName("DocumentType_Name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocumentTypeTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_DocumentType_Table_Employee_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DocumentTypeTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_DocumentType_Table_Employee_table1");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.DocumentTypeTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_DocumentType_Table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DocumentTypeTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_DocumentType_Table_Sub_Company_table");
            });

            modelBuilder.Entity<DocUploadMaster>(entity =>
            {
                entity.HasKey(e => e.DocumentId);

                entity.ToTable("DocUpload_Master");

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.DocClassId).HasColumnName("Doc_Class_ID");

                entity.Property(e => e.DocTypeId).HasColumnName("DocType_ID");

                entity.Property(e => e.Doctype)
                    .HasColumnName("doctype")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.FileName).HasMaxLength(250);

                entity.Property(e => e.Sensitivity).HasColumnName("sensitivity");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.DocClass)
                    .WithMany(p => p.DocUploadMaster)
                    .HasForeignKey(d => d.DocClassId)
                    .HasConstraintName("FK_DocUpload_Master_DocumentClass_table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DocUploadMaster)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_DocUpload_Master_Sub_Company_table");
            });

            modelBuilder.Entity<DocUploadTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("DocUpload_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckoutAvailable).HasColumnName("Checkout_Available");

                entity.Property(e => e.CheckoutOn).HasColumnType("datetime");

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.CopiedOn).HasColumnType("datetime");

                entity.Property(e => e.CopiedTo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocClassId).HasColumnName("Doc_Class_ID");

                entity.Property(e => e.DocId).HasColumnName("Doc_ID");

                entity.Property(e => e.DocTypeId).HasColumnName("DocType_ID");

                entity.Property(e => e.DocVersion).HasColumnName("Doc_Version");

                entity.Property(e => e.DocumentId)
                    .HasColumnName("DocumentID")
                    .HasComputedColumnSql("([dbo].[DocumentID]([SLNO]))");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MovedOn).HasColumnType("datetime");

                entity.Property(e => e.MovedTo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sensitivity).HasColumnName("sensitivity");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UploadedOn).HasColumnType("datetime");

                entity.Property(e => e.VersionId)
                    .HasColumnName("VersionID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CheckoutByNavigation)
                    .WithMany(p => p.DocUploadTableCheckoutByNavigation)
                    .HasForeignKey(d => d.CheckoutBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table1");

                entity.HasOne(d => d.CopiedByNavigation)
                    .WithMany(p => p.DocUploadTableCopiedByNavigation)
                    .HasForeignKey(d => d.CopiedBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table3");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.DocUploadTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DocUploadTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table4");

                entity.HasOne(d => d.DocClass)
                    .WithMany(p => p.DocUploadTable)
                    .HasForeignKey(d => d.DocClassId)
                    .HasConstraintName("FK_DocUpload_table_DocumentClass_table");

                entity.HasOne(d => d.Doc)
                    .WithMany(p => p.DocUploadTable)
                    .HasForeignKey(d => d.DocId)
                    .HasConstraintName("FK_DocUpload_table_DocUpload_Master");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.DocUploadTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_DocUpload_table_Domain_table");

                entity.HasOne(d => d.MovedByNavigation)
                    .WithMany(p => p.DocUploadTableMovedByNavigation)
                    .HasForeignKey(d => d.MovedBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table5");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.DocUploadTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_DocUpload_table_Sub_Company_table");

                entity.HasOne(d => d.UploadedByNavigation)
                    .WithMany(p => p.DocUploadTableUploadedByNavigation)
                    .HasForeignKey(d => d.UploadedBy)
                    .HasConstraintName("FK_DocUpload_table_Employee_table2");
            });

            modelBuilder.Entity<DomainMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Domain_Mapping_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.DeletedOn).HasColumnType("date");

                entity.Property(e => e.MapedBy).HasColumnName("Maped_By");

                entity.Property(e => e.MapedCompId).HasColumnName("Maped_CompID");

                entity.Property(e => e.MapedCompName)
                    .HasColumnName("Maped_CompName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MapedDomain)
                    .HasColumnName("Maped_Domain")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MapedDomainId).HasColumnName("Maped_DomainID");

                entity.Property(e => e.MapedOn)
                    .HasColumnName("Maped_On")
                    .HasColumnType("date");

                entity.Property(e => e.ModifiedBy).HasColumnName("Modified_By");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnName("Modified_On")
                    .HasColumnType("date");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DomainMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Domain_Mapping_table_Employee_table2");

                entity.HasOne(d => d.MapedByNavigation)
                    .WithMany(p => p.DomainMappingTableMapedByNavigation)
                    .HasForeignKey(d => d.MapedBy)
                    .HasConstraintName("FK_Domain_Mapping_table_Employee_table");

                entity.HasOne(d => d.MapedComp)
                    .WithMany(p => p.DomainMappingTable)
                    .HasForeignKey(d => d.MapedCompId)
                    .HasConstraintName("FK_Domain_Mapping_table_Sub_Company_table");

                entity.HasOne(d => d.MapedDomainNavigation)
                    .WithMany(p => p.DomainMappingTable)
                    .HasForeignKey(d => d.MapedDomainId)
                    .HasConstraintName("FK_Domain_Mapping_table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DomainMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Domain_Mapping_table_Employee_table1");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.DomainMappingTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_Domain_Mapping_table_Parent_Company_Table");
            });

            modelBuilder.Entity<DomainTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Domain_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CenterCode).HasMaxLength(200);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainDescription)
                    .HasColumnName("Domain_Description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.DomainName)
                    .HasColumnName("Domain_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.CretedByNavigation)
                    .WithMany(p => p.DomainTableCretedByNavigation)
                    .HasForeignKey(d => d.CretedBy)
                    .HasConstraintName("FK_Domain_table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.DomainTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Domain_table_Employee_table2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.DomainTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Domain_table_Employee_table1");
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.HasKey(e => e.EmailId);

                entity.Property(e => e.EmailId).HasColumnName("EmailID");

                entity.Property(e => e.PushMsg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Smsmsg)
                    .HasColumnName("SMSMsg")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.Subject).HasMaxLength(50);

                entity.Property(e => e.TempName)
                    .HasColumnName("Temp_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<EmpHoliday>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.Property(e => e.EmpId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Leavedate).HasColumnType("date");
            });

            modelBuilder.Entity<EmployeeProcessValue>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Employee_Process_value");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsApprover).HasColumnName("isApprover");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.EmployeeProcessValue)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Employee_Process_value_Employee_table");
            });

            modelBuilder.Entity<EmployeeTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Employee_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ContactNo).HasColumnName("Contact_No");

                entity.Property(e => e.CountryCode).HasMaxLength(5);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Creationuser)
                    .HasColumnName("creationuser")
                    .HasMaxLength(50);

                entity.Property(e => e.Department)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Designation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Dob).HasColumnType("date");

                entity.Property(e => e.Doj).HasColumnType("date");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmpPhoto).HasColumnName("Emp_Photo");

                entity.Property(e => e.EmpPhoto1)
                    .HasColumnName("EmpPhoto")
                    .HasMaxLength(500);

                entity.Property(e => e.EmployeeName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Gender).HasMaxLength(30);

                entity.Property(e => e.IsManager).HasColumnName("isManager");

                entity.Property(e => e.IsUserFp).HasColumnName("isUserFp");

                entity.Property(e => e.Location)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser)
                    .HasColumnName("modifactionuser")
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReportingManagerId)
                    .HasColumnName("Reporting_Manager_Id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompany).HasColumnName("Sub_Company");

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.UserType)
                    .HasColumnName("User_type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Userid)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.SubCompanyNavigation)
                    .WithMany(p => p.EmployeeTable)
                    .HasForeignKey(d => d.SubCompany)
                    .HasConstraintName("FK_Employee_table_Sub_Company_table");
            });

            modelBuilder.Entity<EventTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Event_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EventDetail).IsUnicode(false);

                entity.Property(e => e.EventTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.EventTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Event_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.EventTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Event_Table_Employee_table2");

                entity.HasOne(d => d.EventDomainNavigation)
                    .WithMany(p => p.EventTable)
                    .HasForeignKey(d => d.EventDomain)
                    .HasConstraintName("FK_Event_Table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.EventTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Event_Table_Employee_table1");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.EventTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Event_Table_Sub_Company_table");
            });

            modelBuilder.Entity<ExceptionError>(entity =>
            {
                entity.HasKey(e => e.TraceId);

                entity.ToTable("Exception_Error");

                entity.Property(e => e.TraceId).HasColumnName("trace_id");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("created_on")
                    .HasColumnType("datetime");

                entity.Property(e => e.ErrorPage)
                    .HasColumnName("error_page")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ExceptionFrom)
                    .HasColumnName("exception_from")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExceptionMessage)
                    .HasColumnName("exception_message")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.InnerException)
                    .HasColumnName("inner_exception")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace)
                    .HasColumnName("stack_trace")
                    .HasMaxLength(5000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ForgotPwdTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("ForgotPwd_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ClientIp)
                    .HasColumnName("ClientIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EmpId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiryPeriod).HasColumnType("datetime");

                entity.Property(e => e.ResetPasswordLink)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UsedOn).HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FormItemTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Form_Item_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.FormId).HasColumnName("Form_ID");

                entity.Property(e => e.FormItemType)
                    .HasColumnName("Form_ItemType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FormItemValue)
                    .HasColumnName("Form_ItemValue")
                    .IsUnicode(false);

                entity.Property(e => e.IsMandatory).HasColumnName("isMandatory");

                entity.Property(e => e.IsMulti).HasColumnName("isMulti");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.FormItemTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Form_Item_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.FormItemTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Form_Item_Table_Employee_table2");

                entity.HasOne(d => d.Form)
                    .WithMany(p => p.FormItemTable)
                    .HasForeignKey(d => d.FormId)
                    .HasConstraintName("FK_Form_Item_Table_Form_Table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.FormItemTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Form_Item_Table_Employee_table1");
            });

            modelBuilder.Entity<FormTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Form_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.FormDescription).IsUnicode(false);

                entity.Property(e => e.FormName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.FormTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Form_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.FormTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Form_Table_Employee_table1");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.FormTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_Form_Table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.FormTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Form_Table_Employee_table2");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.FormTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Form_Table_Sub_Company_table");
            });

            modelBuilder.Entity<FrequencyMaster>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Cretatedon).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Modifiedon).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GroupCompany>(entity =>
            {
                entity.ToTable("Group_Company");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");
            });

            modelBuilder.Entity<GroupDomain>(entity =>
            {
                entity.ToTable("Group_Domain");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");
            });

            modelBuilder.Entity<GroupMap>(entity =>
            {
                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MapType).HasMaxLength(20);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<GroupMapping>(entity =>
            {
                entity.Property(e => e.AcgroupName).HasColumnName("ACGroupName");

                entity.Property(e => e.ApgroupName).HasColumnName("APGroupName");

                entity.Property(e => e.AuditscopCompanyName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AuditscopName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Createon).HasColumnType("datetime");

                entity.Property(e => e.Modifyon).HasColumnType("datetime");
            });

            modelBuilder.Entity<GroupTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Group_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.GroupDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.GroupEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GroupName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_compID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_compID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.GroupTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Group_table_Group_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.GroupTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Group_table_Employee_table1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.GroupTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Group_table_Employee_table");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.GroupTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_Group_table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.GroupTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Group_table_Sub_Company_table");
            });

            modelBuilder.Entity<GroupUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");
            });

            modelBuilder.Entity<IndexTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Index_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.IndexDescription)
                    .HasColumnName("Index_description")
                    .IsUnicode(false);

                entity.Property(e => e.IndexLength)
                    .HasColumnName("Index_Length")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IndexName)
                    .HasColumnName("Index_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IndexType)
                    .HasColumnName("Index_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId)
                    .HasColumnName("Parent_CompID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId)
                    .HasColumnName("Sub_COmpID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvitationAnswer>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.AsnwerBy).HasMaxLength(200);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.OwnerType).HasMaxLength(50);
            });

            modelBuilder.Entity<InvitationDetail>(entity =>
            {
                entity.HasKey(e => e.InvitationId);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<KpModules>(entity =>
            {
                entity.HasKey(e => e.Mid);

                entity.ToTable("kpModules");

                entity.Property(e => e.Mid).HasColumnName("mid");

                entity.Property(e => e.ModulId).HasColumnName("ModulID");

                entity.Property(e => e.ModuleName).HasMaxLength(500);
            });

            modelBuilder.Entity<KpNotify>(entity =>
            {
                entity.HasKey(e => e.NotifactionId);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Notifydate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Kptable>(entity =>
            {
                entity.HasKey(e => e.KingPinId);

                entity.ToTable("KPTable");

                entity.Property(e => e.KingPinId)
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");
            });

            modelBuilder.Entity<ListItemTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("List_Item_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.IsMandatory).HasColumnName("isMandatory");

                entity.Property(e => e.IsMulti).HasColumnName("isMulti");

                entity.Property(e => e.ListId).HasColumnName("List_ID");

                entity.Property(e => e.ListItemType)
                    .HasColumnName("List_ItemType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ListItemValue)
                    .HasColumnName("List_ItemValue")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ListItemTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_List_Item_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.ListItemTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_List_Item_Table_Employee_table2");

                entity.HasOne(d => d.List)
                    .WithMany(p => p.ListItemTable)
                    .HasForeignKey(d => d.ListId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_List_Item_Table_List_Item_Table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ListItemTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_List_Item_Table_Employee_table1");
            });

            modelBuilder.Entity<ListTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("List_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.ListDescription).IsUnicode(false);

                entity.Property(e => e.ListName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ListTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_List_table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.ListTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_List_table_Employee_table2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ListTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_List_table_Employee_table1");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.ListTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_List_table_Sub_Company_table");
            });

            modelBuilder.Entity<LocationMaster>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Location).HasMaxLength(100);
            });

            modelBuilder.Entity<LogHistoryTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("LogHistory_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Action)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.Datetime).HasColumnType("datetime");

                entity.Property(e => e.Ipaddress)
                    .HasColumnName("IPAddress")
                    .IsUnicode(false);

                entity.Property(e => e.Parameter)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_CompID");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.Type)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.LogHistoryTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_LogHistory_Table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.LogHistoryTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_LogHistory_Table_Sub_Company_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.LogHistoryTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_LogHistory_Table_Employee_table");
            });

            modelBuilder.Entity<LoginTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Login_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AdminId).HasColumnName("Admin_id");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.LoginDate)
                    .HasColumnName("Login_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModificationDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.ProfileImg).HasColumnName("Profile_img");

                entity.Property(e => e.Session).HasMaxLength(150);

                entity.Property(e => e.UserType)
                    .HasColumnName("User_type")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.LoginTableAdmin)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_Login_table_Employee_table");

                entity.HasOne(d => d.CreationuserNavigation)
                    .WithMany(p => p.LoginTableCreationuserNavigation)
                    .HasForeignKey(d => d.Creationuser)
                    .HasConstraintName("FK_Login_table_Employee_table2");

                entity.HasOne(d => d.CurrentDomainNavigation)
                    .WithMany(p => p.LoginTableCurrentDomainNavigation)
                    .HasForeignKey(d => d.CurrentDomain)
                    .HasConstraintName("FK_Login_table_Domain_table2");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.LoginTableDomain)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_Login_table_Domain_table");

                entity.HasOne(d => d.ModificationuserNavigation)
                    .WithMany(p => p.LoginTableModificationuserNavigation)
                    .HasForeignKey(d => d.Modificationuser)
                    .HasConstraintName("FK_Login_table_Employee_table3");

                entity.HasOne(d => d.PreviousDomainNavigation)
                    .WithMany(p => p.LoginTablePreviousDomainNavigation)
                    .HasForeignKey(d => d.PreviousDomain)
                    .HasConstraintName("FK_Login_table_Domain_table1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.LoginTableUser)
                    .HasForeignKey(d => d.Userid)
                    .HasConstraintName("FK_Login_table_Employee_table1");
            });

            modelBuilder.Entity<LogTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Log_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.IpAddress)
                    .HasColumnName("IP_Address")
                    .HasMaxLength(50);

                entity.Property(e => e.OperationOn)
                    .HasColumnName("OperationON")
                    .HasColumnType("datetime");

                entity.Property(e => e.OperationPerformed)
                    .HasColumnName("Operation_Performed")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.OperationType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCompId)
                    .HasColumnName("Parent_CompID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ramarks).IsUnicode(false);

                entity.Property(e => e.SubCompId)
                    .HasColumnName("Sub_CompID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(100);

                entity.Property(e => e.UserId1).HasColumnName("user_id");
            });

            modelBuilder.Entity<MailDomain>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Domain).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<MastBuckting>(entity =>
            {
                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastBucktingValue).HasMaxLength(500);

                entity.Property(e => e.PriorityId).HasMaxLength(500);
            });

            modelBuilder.Entity<Mastcalendar>(entity =>
            {
                entity.HasKey(e => e.ClId);

                entity.Property(e => e.ClId).HasColumnName("Cl_ID");

                entity.Property(e => e.ClDescription).HasColumnName("Cl_description");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser)
                    .HasColumnName("creationuser")
                    .HasMaxLength(50);

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.LeaveDate)
                    .HasColumnName("leave_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser)
                    .HasColumnName("modifactionuser")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MastCheckList>(entity =>
            {
                entity.HasKey(e => e.Tdlid);

                entity.Property(e => e.Tdlid).HasColumnName("TDLid");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.Tdldescription).HasColumnName("TDLdescription");
            });

            modelBuilder.Entity<MastCommunicationMode>(entity =>
            {
                entity.HasKey(e => e.MastComModeId);

                entity.Property(e => e.MastComModeType).HasMaxLength(200);
            });

            modelBuilder.Entity<MastDepartmentFordelete>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.ToTable("MastDepartment___Fordelete");

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastDepartmentName).HasMaxLength(200);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<MastEmployeeFordelete>(entity =>
            {
                entity.HasKey(e => e.EmpEntryid);

                entity.ToTable("MastEmployee__Fordelete");

                entity.Property(e => e.ContactPhone).HasMaxLength(50);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Designation).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.Doj)
                    .HasColumnName("DOJ")
                    .HasColumnType("date");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.EmpPhoto).HasMaxLength(500);

                entity.Property(e => e.Empid).HasMaxLength(20);

                entity.Property(e => e.Empname).HasMaxLength(100);

                entity.Property(e => e.IsManager).HasColumnName("isManager");

                entity.Property(e => e.IsUserFp).HasColumnName("isUserFp");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.LastLogindate).HasColumnType("datetime");

                entity.Property(e => e.Location).HasMaxLength(200);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.NotifyViewdate).HasColumnType("datetime");

                entity.Property(e => e.Password).HasMaxLength(200);

                entity.Property(e => e.ReportingManagerEmpid).HasMaxLength(20);

                entity.Property(e => e.Userid).HasMaxLength(200);
            });

            modelBuilder.Entity<MastEmployeeTemp>(entity =>
            {
                entity.HasKey(e => e.EmpEntryid);

                entity.ToTable("MastEmployee_Temp");

                entity.Property(e => e.ContactPhone).HasMaxLength(50);

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Department).HasMaxLength(200);

                entity.Property(e => e.Designation).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasMaxLength(20);

                entity.Property(e => e.Doj)
                    .HasColumnName("DOJ")
                    .HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.Empid).HasMaxLength(20);

                entity.Property(e => e.Empname).HasMaxLength(100);

                entity.Property(e => e.IsManager).HasColumnName("isManager");

                entity.Property(e => e.Location).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(200);
            });

            modelBuilder.Entity<MastGroupFordelete>(entity =>
            {
                entity.HasKey(e => e.MastGroupId);

                entity.ToTable("MastGroup__Fordelete");

                entity.Property(e => e.MastGroupId).ValueGeneratedNever();

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastGroupName).HasMaxLength(500);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<MastGroupTask>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.DeadlineDate).HasColumnType("date");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TaskOwnerId).HasMaxLength(20);

                entity.Property(e => e.TaskOwnerType).HasMaxLength(50);
            });

            modelBuilder.Entity<MastLocation>(entity =>
            {
                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastLocationName).HasMaxLength(100);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<MastPeriodicity>(entity =>
            {
                entity.HasKey(e => e.PeriodicityId);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");
            });

            modelBuilder.Entity<MastPriority>(entity =>
            {
                entity.HasKey(e => e.PriorityId);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.PriorityName).HasMaxLength(200);
            });

            modelBuilder.Entity<MastProSerType>(entity =>
            {
                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastProSerTypeName).HasMaxLength(500);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<MastSchTask>(entity =>
            {
                entity.HasKey(e => e.SchId);

                entity.ToTable("MAST_SCH_TASK");

                entity.Property(e => e.SchId).HasColumnName("SCH_ID");

                entity.Property(e => e.SCheduleDate).HasColumnName("sCHEDULE_DATE");

                entity.Property(e => e.Taskid).HasColumnName("TASKID");
            });

            modelBuilder.Entity<MastSector>(entity =>
            {
                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastSectorName).HasMaxLength(500);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<MastServPro>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastProSerTypeId).HasColumnName("MastProSerTypeID");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.SerDescription).HasColumnName("serDescription");

                entity.Property(e => e.SerProductname).HasMaxLength(500);
            });

            modelBuilder.Entity<MastServProTemp>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.ToTable("MastServPro_Temp");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.MastProSerTypeName).HasMaxLength(20);

                entity.Property(e => e.SerDescription).HasColumnName("serDescription");

                entity.Property(e => e.SerProductname).HasMaxLength(500);
            });

            modelBuilder.Entity<MastSkill>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.Property(e => e.Entryid).HasColumnName("entryid");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.SkillId).HasColumnName("SkillID");

                entity.Property(e => e.SkillName).HasMaxLength(200);
            });

            modelBuilder.Entity<MastSkillTemp>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.ToTable("MastSkill_Temp");

                entity.Property(e => e.Entryid).HasColumnName("entryid");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.SkillName).HasMaxLength(200);
            });

            modelBuilder.Entity<MastSubTask>(entity =>
            {
                entity.HasKey(e => e.SubTaskId);

                entity.Property(e => e.AssignedId).HasMaxLength(50);

                entity.Property(e => e.AssignedTo).HasMaxLength(10);

                entity.Property(e => e.AttachmentType).HasMaxLength(20);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.DeadlineDate).HasColumnType("date");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.RasciValue).HasMaxLength(10);

                entity.Property(e => e.ReportingEmpId).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.SubTaskCreatedBy).HasMaxLength(200);

                entity.Property(e => e.TaskCompDate).HasColumnType("datetime");

                entity.Property(e => e.WorkflowValue).HasMaxLength(10);
            });

            modelBuilder.Entity<MastSurveyOption>(entity =>
            {
                entity.HasKey(e => e.OptionId);

                entity.Property(e => e.OptionId).ValueGeneratedNever();

                entity.Property(e => e.OptionValue)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MastTask>(entity =>
            {
                entity.HasKey(e => e.TaskCreateId);

                entity.Property(e => e.Bucketing).HasColumnName("bucketing");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.DeadlineDate).HasColumnType("date");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TaskOwnerId).HasMaxLength(20);

                entity.Property(e => e.TaskOwnerType).HasMaxLength(50);
            });

            modelBuilder.Entity<MastTaskEmpStatus>(entity =>
            {
                entity.HasKey(e => e.TaskStatusId);

                entity.Property(e => e.TaskStatusName).HasMaxLength(50);
            });

            modelBuilder.Entity<MastTaskStatus>(entity =>
            {
                entity.HasKey(e => e.TaskStatusId);

                entity.Property(e => e.TaskStatusName).HasMaxLength(50);
            });

            modelBuilder.Entity<MastToDolist>(entity =>
            {
                entity.HasKey(e => e.TdlcheckId);

                entity.ToTable("MastToDOList");

                entity.Property(e => e.TdlcheckId).HasColumnName("TDLcheckID");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.Tdldescription).HasColumnName("TDLdescription");

                entity.Property(e => e.Tdlid).HasColumnName("TDLid");

                entity.Property(e => e.Tdlorder).HasColumnName("TDLOrder");
            });

            modelBuilder.Entity<MastVendor>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.Activestatus).HasColumnName("activestatus");

                entity.Property(e => e.CompanyPhone)
                    .HasColumnName("companyPhone")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyaddress).HasColumnName("companyaddress");

                entity.Property(e => e.Companycontctperson)
                    .HasColumnName("companycontctperson")
                    .HasMaxLength(100);

                entity.Property(e => e.Companydepartment).HasColumnName("companydepartment");

                entity.Property(e => e.Companydesignation)
                    .HasColumnName("companydesignation")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyemail)
                    .HasColumnName("companyemail")
                    .HasMaxLength(100);

                entity.Property(e => e.Companylocation)
                    .HasColumnName("companylocation")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyname).HasMaxLength(100);

                entity.Property(e => e.Companyreportingmanager)
                    .HasColumnName("companyreportingmanager")
                    .HasMaxLength(20);

                entity.Property(e => e.Companyurl)
                    .HasColumnName("companyurl")
                    .HasMaxLength(100);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.DateofIncorporation).HasColumnType("date");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.KingpinId).HasMaxLength(200);

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.Password).HasMaxLength(100);

                entity.Property(e => e.Remarks).HasColumnName("remarks");

                entity.Property(e => e.UserId).HasMaxLength(100);

                entity.Property(e => e.Vendorphoto)
                    .HasColumnName("vendorphoto")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<MastVendorTemp>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.ToTable("MastVendor_Temp");

                entity.Property(e => e.CompanyPhone)
                    .HasColumnName("companyPhone")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyaddress).HasColumnName("companyaddress");

                entity.Property(e => e.Companycontctperson)
                    .HasColumnName("companycontctperson")
                    .HasMaxLength(100);

                entity.Property(e => e.Companydepartment)
                    .HasColumnName("companydepartment")
                    .HasMaxLength(100);

                entity.Property(e => e.Companydesignation)
                    .HasColumnName("companydesignation")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyemail)
                    .HasColumnName("companyemail")
                    .HasMaxLength(100);

                entity.Property(e => e.Companylocation)
                    .HasColumnName("companylocation")
                    .HasMaxLength(100);

                entity.Property(e => e.Companyname).HasMaxLength(100);

                entity.Property(e => e.Companyreportingmanager)
                    .HasColumnName("companyreportingmanager")
                    .HasMaxLength(20);

                entity.Property(e => e.Companyurl)
                    .HasColumnName("companyurl")
                    .HasMaxLength(100);

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.DateofIncorporation).HasColumnType("date");

                entity.Property(e => e.KingpinId).HasMaxLength(200);
            });

            modelBuilder.Entity<MastWorkFlow>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.WorkFlowId).HasColumnName("WorkFlowID");

                entity.Property(e => e.WorkFlowValue).HasMaxLength(200);
            });

            modelBuilder.Entity<MastWorkFlowLevel>(entity =>
            {
                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.MastWorkFlowLevelValue).HasMaxLength(200);
            });

            modelBuilder.Entity<MessageTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Message_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AdminId).HasColumnName("AdminID");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DocId).HasColumnName("Doc_ID");

                entity.Property(e => e.FileLink)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FromUser).HasColumnName("From_user");

                entity.Property(e => e.IsReaded)
                    .HasColumnName("isReaded")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.Section)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToEmail).HasMaxLength(50);

                entity.Property(e => e.ToUser).HasColumnName("To_user");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.MessageTable)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_Message_Table_Employee_table");
            });

            modelBuilder.Entity<NotificationTbl>(entity =>
            {
                entity.HasKey(e => e.NotificationId);

                entity.ToTable("Notification_tbl");

                entity.Property(e => e.NotificationId).HasColumnName("NotificationID");

                entity.Property(e => e.CreeatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsRead)
                    .HasColumnName("isRead")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.ToUserId).HasColumnName("ToUserID");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<ParentCompanyTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Parent_Company_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CompanyName)
                    .HasColumnName("Company_Name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Kpid)
                    .HasColumnName("KPID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ParentCompanyTable)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Parent_Company_Table_Employee_table");
            });

            modelBuilder.Entity<ProSerSkillMap>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<ProSerSkillMapTemp>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.ToTable("ProSerSkillMap_Temp");

                entity.Property(e => e.EntryId).HasColumnName("EntryID");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.ProSerName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.VendorName)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RecordFieldDocClassMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("RecordField_DocClass_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RecordFieldId).HasColumnName("RecordField_ID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.RecordFieldDocClassMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_RecordField_DocClass_Mapping_Table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.RecordFieldDocClassMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_RecordField_DocClass_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.RecordFieldDocClassMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_RecordField_DocClass_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.RecordFieldDocClassMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_RecordField_DocClass_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.RecordField)
                    .WithMany(p => p.RecordFieldDocClassMappingTable)
                    .HasForeignKey(d => d.RecordFieldId)
                    .HasConstraintName("FK_RecordField_DocClass_Mapping_Table_List_table");
            });

            modelBuilder.Entity<RecordFieldDocTypeMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("RecordField_DocType_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DocumentTypeId).HasColumnName("DocumentType_ID");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.RecordFieldId).HasColumnName("RecordField_ID");
            });

            modelBuilder.Entity<RoleMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Role_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.RoleId).HasColumnName("Role_ID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.RoleMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Role_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleMappingTable)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Role_Mapping_Table_Role_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.RoleMappingTableUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_Role_Mapping_Table_Employee_table");
            });

            modelBuilder.Entity<RoleModulesPermission>(entity =>
            {
                entity.ToTable("Role_Modules_Permission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FormName).HasMaxLength(250);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");
            });

            modelBuilder.Entity<RoleTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Role_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_compID");

                entity.Property(e => e.RoleDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RoleEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId).HasColumnName("Sub_compID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.RoleTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Role_table_Employee_table");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.RoleTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_Role_table_Employee_table2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.RoleTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Role_table_Employee_table1");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.RoleTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_Role_table_Parent_Company_Table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.RoleTable)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_Role_table_Sub_Company_table");
            });

            modelBuilder.Entity<RoleVanderMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Role_Vander_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("date");

                entity.Property(e => e.RoleId).HasColumnName("Role_ID");

                entity.Property(e => e.UserId).HasColumnName("User_ID");
            });

            modelBuilder.Entity<RoleVendorTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("RoleVendor_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_compID");

                entity.Property(e => e.RoleDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RoleEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId).HasColumnName("Sub_compID");
            });

            modelBuilder.Entity<SkillMap>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.Property(e => e.Entryid).HasColumnName("entryid");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Empid).HasMaxLength(20);

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");
            });

            modelBuilder.Entity<SkillMapTemp>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.ToTable("SkillMap_Temp");

                entity.Property(e => e.Entryid).HasColumnName("entryid");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.EmpName).HasMaxLength(200);

                entity.Property(e => e.SkillName).HasMaxLength(500);
            });

            modelBuilder.Entity<Stage1Stage>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stage2Stage>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stage3Stage>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SubCompanyTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("Sub_Company_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Address)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNo).HasColumnName("Contact_No");

                entity.Property(e => e.ContactPerson)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCompId).HasColumnName("Parent_compID");

                entity.Property(e => e.ParentCompName)
                    .HasColumnName("Parent_compName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompanyId)
                    .HasColumnName("SubCompanyID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompanyName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.ParentComp)
                    .WithMany(p => p.SubCompanyTable)
                    .HasForeignKey(d => d.ParentCompId)
                    .HasConstraintName("FK_Sub_Company_table_Parent_Company_Table");
            });

            modelBuilder.Entity<SubTaskFiles>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Taskfile).HasColumnName("taskfile");

                entity.Property(e => e.Uploaddate)
                    .HasColumnName("uploaddate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<SurvayAnswer>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.AsnwerBy).HasMaxLength(200);

                entity.Property(e => e.Attachmentpath).HasColumnName("attachmentpath");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Ownertype).HasMaxLength(200);
            });

            modelBuilder.Entity<SurveyDetail>(entity =>
            {
                entity.HasKey(e => e.SurveyId);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Modifactiondate)
                    .HasColumnName("modifactiondate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Modifactionuser).HasColumnName("modifactionuser");

                entity.Property(e => e.RowId).HasColumnName("RowID");
            });

            modelBuilder.Entity<TaskChecklist>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("date");

                entity.Property(e => e.Creationuser)
                    .HasColumnName("creationuser")
                    .HasMaxLength(500);

                entity.Property(e => e.Iscompleted).HasColumnName("iscompleted");

                entity.Property(e => e.TdlcheckId).HasColumnName("TDlcheckId");
            });

            modelBuilder.Entity<TaskComment>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.CommentBy).HasMaxLength(100);

                entity.Property(e => e.CommentFor).HasMaxLength(100);

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser)
                    .HasColumnName("creationuser")
                    .HasMaxLength(200);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasMaxLength(10);

                entity.Property(e => e.UserType).HasMaxLength(50);
            });

            modelBuilder.Entity<TaskIncident>(entity =>
            {
                entity.HasKey(e => e.EntryId);

                entity.Property(e => e.IncidentType).HasMaxLength(50);

                entity.Property(e => e.IntervalType).HasMaxLength(50);
            });

            modelBuilder.Entity<TblModules>(entity =>
            {
                entity.HasKey(e => e.ModuleId);

                entity.ToTable("tblModules");

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ModuleName).HasMaxLength(100);

                entity.Property(e => e.ModulePage).HasMaxLength(100);

                entity.Property(e => e.PageTag).HasMaxLength(50);
            });

            modelBuilder.Entity<TblModulesForm>(entity =>
            {
                entity.ToTable("tblModulesForm");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DisplayName).HasMaxLength(250);

                entity.Property(e => e.FormName).HasMaxLength(300);

                entity.Property(e => e.GroupName).HasMaxLength(50);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ModuleName).HasMaxLength(250);
            });

            modelBuilder.Entity<UserDelegationTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("User_Delegation_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .IsUnicode(false);

                entity.Property(e => e.DelFrom)
                    .HasColumnName("del_from")
                    .HasMaxLength(10);

                entity.Property(e => e.DelTo)
                    .HasColumnName("del_to")
                    .HasMaxLength(10);

                entity.Property(e => e.DelegatedBy).HasColumnName("delegatedBy");

                entity.Property(e => e.DelegatedTo).HasColumnName("delegatedTo");

                entity.Property(e => e.Fd).HasColumnName("FD");

                entity.Property(e => e.ParentCoy).HasColumnName("parentCoy");

                entity.Property(e => e.SubCoy).HasColumnName("subCoy");
            });

            modelBuilder.Entity<UserDocClassMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("User_DocClass_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckinRight)
                    .HasColumnName("checkin_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CheckoutRight)
                    .HasColumnName("checkout_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.DownloadRight)
                    .HasColumnName("download_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EmailRight)
                    .HasColumnName("email_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PrintRight)
                    .HasColumnName("print_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SelectallRight)
                    .HasColumnName("selectall_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UploadRight)
                    .HasColumnName("upload_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.ViewRight)
                    .HasColumnName("view_right")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.UserDocClassMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.UserDocClassMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_Employee_table3");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.UserDocClassMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.UserDocClassMappingTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.UserDocClassMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDocClassMappingTableUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_User_DocClass_Mapping_Table_Employee_table");
            });

            modelBuilder.Entity<UserDocTypeMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("User_DocType_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckinRight)
                    .HasColumnName("checkin_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CheckoutRight)
                    .HasColumnName("checkout_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DocumentType).HasColumnName("Document_Type");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.DownloadRight)
                    .HasColumnName("download_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EmailRight)
                    .HasColumnName("email_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PrintRight)
                    .HasColumnName("print_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SelectallRight)
                    .HasColumnName("selectall_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UploadRight)
                    .HasColumnName("upload_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.ViewRight)
                    .HasColumnName("view_right")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.UserDocTypeMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.UserDocTypeMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_Employee_table3");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.UserDocTypeMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.UserDocTypeMappingTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.UserDocTypeMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDocTypeMappingTableUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_User_DocType_Mapping_Table_Employee_table");
            });

            modelBuilder.Entity<UserDocumentMappingTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("User_Document_Mapping_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CheckinRight)
                    .HasColumnName("checkin_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CheckoutRight)
                    .HasColumnName("checkout_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.DocumentClassId).HasColumnName("Document_ClassID");

                entity.Property(e => e.DocumentId).HasColumnName("Document_ID");

                entity.Property(e => e.DocumentType).HasColumnName("Document_Type");

                entity.Property(e => e.DomainId).HasColumnName("Domain_ID");

                entity.Property(e => e.DownloadRight)
                    .HasColumnName("download_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.EmailRight)
                    .HasColumnName("email_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PrintRight)
                    .HasColumnName("print_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SelectallRight)
                    .HasColumnName("selectall_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.UploadRight)
                    .HasColumnName("upload_right")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UserId).HasColumnName("User_ID");

                entity.Property(e => e.ViewRight)
                    .HasColumnName("view_right")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.UserDocumentMappingTableCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_User_Document_Mapping_Table_Employee_table1");

                entity.HasOne(d => d.DeletedByNavigation)
                    .WithMany(p => p.UserDocumentMappingTableDeletedByNavigation)
                    .HasForeignKey(d => d.DeletedBy)
                    .HasConstraintName("FK_User_Document_Mapping_Table_Employee_table3");

                entity.HasOne(d => d.DocumentClass)
                    .WithMany(p => p.UserDocumentMappingTable)
                    .HasForeignKey(d => d.DocumentClassId)
                    .HasConstraintName("FK_User_Document_Mapping_Table_DocumentClass_table");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.UserDocumentMappingTable)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_User_Document_Mapping_Table_Domain_table");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.UserDocumentMappingTableModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_User_Document_Mapping_Table_Employee_table2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserDocumentMappingTableUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_User_Document_Mapping_Table_Employee_table");
            });

            modelBuilder.Entity<UserNotification>(entity =>
            {
                entity.HasKey(e => e.NotiId);

                entity.Property(e => e.NotiId)
                    .HasColumnName("Noti_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsReaded).HasDefaultValueSql("((0))");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.PageUrl).HasColumnName("PageURL");

                entity.Property(e => e.ReceiverId).HasColumnName("ReceiverID");

                entity.Property(e => e.SenerId).HasColumnName("SenerID");

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");

                entity.HasOne(d => d.Receiver)
                    .WithMany(p => p.UserNotificationReceiver)
                    .HasForeignKey(d => d.ReceiverId)
                    .HasConstraintName("FK_UserNotification_Employee_table1");

                entity.HasOne(d => d.Sener)
                    .WithMany(p => p.UserNotificationSener)
                    .HasForeignKey(d => d.SenerId)
                    .HasConstraintName("FK_UserNotification_Employee_table");
            });

            modelBuilder.Entity<VendorLogin>(entity =>
            {
                entity.HasKey(e => e.Entryid);

                entity.Property(e => e.Entryid).HasColumnName("entryid");

                entity.Property(e => e.Creationdate)
                    .HasColumnName("creationdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Creationuser).HasColumnName("creationuser");

                entity.Property(e => e.Isdeleted).HasColumnName("isdeleted");

                entity.Property(e => e.Masttaskid).HasColumnName("masttaskid");

                entity.Property(e => e.Mastvendorid).HasColumnName("mastvendorid");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(50);

                entity.Property(e => e.Subtaskid).HasColumnName("subtaskid");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(500);

                entity.Property(e => e.Userid)
                    .HasColumnName("userid")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<VenTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("ven_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AcHolderName).HasMaxLength(100);

                entity.Property(e => e.Address).IsUnicode(false);

                entity.Property(e => e.BankBranch)
                    .HasColumnName("BANK_BRANCH")
                    .HasMaxLength(50);

                entity.Property(e => e.BankName).HasMaxLength(100);

                entity.Property(e => e.BankTelNo).HasMaxLength(50);

                entity.Property(e => e.Bankemail)
                    .HasColumnName("BANKEMAIL")
                    .HasMaxLength(50);

                entity.Property(e => e.Bankfaxno)
                    .HasColumnName("BANKFAXNo")
                    .HasMaxLength(50);

                entity.Property(e => e.Bankmobileno)
                    .HasColumnName("BANKMOBILENo")
                    .HasMaxLength(50);

                entity.Property(e => e.Bcountry)
                    .HasColumnName("BCountry")
                    .HasMaxLength(50);

                entity.Property(e => e.CityName)
                    .HasColumnName("CITY_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.CityPostalCode)
                    .HasColumnName("CITY_POSTAL_CODE")
                    .HasMaxLength(50);

                entity.Property(e => e.CompanyName).HasMaxLength(50);

                entity.Property(e => e.ExtUserBankAcNo).HasMaxLength(50);

                entity.Property(e => e.HouseNoBank)
                    .HasColumnName("HOUSE_No_BANK")
                    .HasMaxLength(50);

                entity.Property(e => e.Ibanno)
                    .HasColumnName("IBANNO")
                    .HasMaxLength(50);

                entity.Property(e => e.IfscCode)
                    .HasColumnName("IFSC_Code")
                    .HasMaxLength(50);

                entity.Property(e => e.IsDashboard).HasDefaultValueSql("((1))");

                entity.Property(e => e.LegalStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegionState)
                    .HasColumnName("REGION_STATE")
                    .HasMaxLength(50);

                entity.Property(e => e.Remarks).IsUnicode(false);

                entity.Property(e => e.StreetName)
                    .HasColumnName("STREET_NAME")
                    .HasMaxLength(50);

                entity.Property(e => e.SwiftCode).HasMaxLength(50);

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("User_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.VenTable)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_ven_Table_Employee_table1");
            });

            modelBuilder.Entity<WidgetSettings>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("widget_settings");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("createdOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.PieDocClass)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCmpId).HasColumnName("SubCmpID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.WidgetSettings)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_widget_settings_Domain_table");

                entity.HasOne(d => d.SubCmp)
                    .WithMany(p => p.WidgetSettings)
                    .HasForeignKey(d => d.SubCmpId)
                    .HasConstraintName("FK_widget_settings_Sub_Company_table");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.WidgetSettings)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_widget_settings_Employee_table");
            });

            modelBuilder.Entity<WorkflowCpProcessTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_cp_process_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bom)
                    .HasColumnName("bom")
                    .HasMaxLength(15);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CpGroupingNumber)
                    .HasColumnName("Cp_GroupingNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CpsimulationNumber)
                    .HasColumnName("CPSimulationNumber")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Msid).HasColumnName("MSID");

                entity.Property(e => e.Nod).HasColumnName("nod");

                entity.Property(e => e.Prity).HasColumnName("prity");

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.StatusOn).HasColumnType("datetime");

                entity.Property(e => e.Tbac).HasColumnName("TBAC");

                entity.Property(e => e.Tbd).HasColumnName("TBD");
            });

            modelBuilder.Entity<WorkflowDocTables>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Doc_tables");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.FileName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath).IsUnicode(false);

                entity.Property(e => e.StgNme)
                    .HasColumnName("stgNme")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ThId).HasColumnName("th_ID");

                entity.Property(e => e.WfId).HasColumnName("wf_ID");

                entity.HasOne(d => d.Wf)
                    .WithMany(p => p.WorkflowDocTables)
                    .HasForeignKey(d => d.WfId)
                    .HasConstraintName("FK_workflow_Doc_tables_workflow_table");
            });

            modelBuilder.Entity<WorkflowExportProcessE1Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Export_process_E1_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CombinedBpmid).HasColumnName("CombinedBPMID");

                entity.Property(e => e.CrtDimension).HasColumnName("crtDimension");

                entity.Property(e => e.DealayReason).HasMaxLength(200);

                entity.Property(e => e.DischargePort).HasMaxLength(200);

                entity.Property(e => e.DlvDate).HasColumnType("date");

                entity.Property(e => e.ExCntryEtd)
                    .HasColumnName("ExCntryETD")
                    .HasMaxLength(200);

                entity.Property(e => e.GohTtlPacks)
                    .HasColumnName("gohTtlPacks")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GohVcpdetails)
                    .HasColumnName("gohVCPdetails")
                    .IsUnicode(false);

                entity.Property(e => e.Gweight).HasMaxLength(200);

                entity.Property(e => e.HndovrDate).HasMaxLength(200);

                entity.Property(e => e.ItmDesc).IsUnicode(false);

                entity.Property(e => e.Li)
                    .HasColumnName("LI")
                    .HasMaxLength(200);

                entity.Property(e => e.LineItm).HasMaxLength(200);

                entity.Property(e => e.MerchEmail).HasMaxLength(200);

                entity.Property(e => e.NorMerchName).HasMaxLength(200);

                entity.Property(e => e.PkgType)
                    .HasColumnName("pkgType")
                    .HasMaxLength(200);

                entity.Property(e => e.Poqty)
                    .HasColumnName("POqty")
                    .HasMaxLength(200);

                entity.Property(e => e.Remarks).HasColumnName("remarks");

                entity.Property(e => e.SPort)
                    .HasColumnName("sPort")
                    .HasMaxLength(200);

                entity.Property(e => e.Sappo)
                    .HasColumnName("SAPPO")
                    .HasMaxLength(50);

                entity.Property(e => e.ShpMode)
                    .HasColumnName("shpMode")
                    .HasMaxLength(200);

                entity.Property(e => e.ShpQty)
                    .HasColumnName("shpQty")
                    .HasMaxLength(200);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.StylNo)
                    .HasColumnName("stylNo")
                    .HasMaxLength(200);

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.TtlCbm)
                    .HasColumnName("TtlCBM")
                    .HasMaxLength(200);

                entity.Property(e => e.TtlCrt).HasMaxLength(200);

                entity.Property(e => e.TtlVolWeight).HasMaxLength(200);
            });

            modelBuilder.Entity<WorkflowExportProcessE2Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Export_process_E2_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CertiOrigin)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Cogog)
                    .HasColumnName("COGOG")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Color)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CrgHandDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CusPo)
                    .HasColumnName("CusPO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Eta)
                    .HasColumnName("ETA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Etd)
                    .HasColumnName("ETD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FacInvoice)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FacName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Fcrblhawb)
                    .HasColumnName("FCRBLHAWB")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Goh)
                    .HasColumnName("GOH")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gweight)
                    .HasColumnName("GWeight")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hscode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ibd)
                    .HasColumnName("IBD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvCurrency)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvRef)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InvValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.ItmDisc).IsUnicode(false);

                entity.Property(e => e.NetWeight)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoCrtn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pod)
                    .HasColumnName("POD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PreCarriage)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SapLi)
                    .HasColumnName("SapLI")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SapPo)
                    .HasColumnName("SapPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShipMode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.StylNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.TtleMeasurement)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UltDestination)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VesselName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowExportProcessE3Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Export_process_E3_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.AlNotifyPrty)
                    .HasColumnName("alNotifyPrty")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.Cbm)
                    .HasColumnName("CBM")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CertiOrigin)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Consignee)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CrdNoteDeductions)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Eta)
                    .HasColumnName("ETA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Etd)
                    .HasColumnName("ETD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fcrblhawb)
                    .HasColumnName("FCRBLHAWB")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GrsInvValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrsWgt)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NetInvValPay)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NetWgt)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NoCntr)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NotifyPrty)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OtherDeduc)
                    .HasColumnName("otherDeduc")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Rewards)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RexDeclare)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RmDeduction)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.VesselName)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowImportProcessI1Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Import_process_I1_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.ConsigneeNmAdd).IsUnicode(false);

                entity.Property(e => e.DelvDate)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DocCrdNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Eta)
                    .HasColumnName("ETA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Etd)
                    .HasColumnName("ETD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrpCoyName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gweight)
                    .HasColumnName("GWeight")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LcopenBy)
                    .HasColumnName("LCOpenBy")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MerchEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MerchName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NoPkgs)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PayTerm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SapForm)
                    .HasColumnName("sapForm")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShipMode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShiprNmAdd).IsUnicode(false);

                entity.Property(e => e.Srno)
                    .HasColumnName("SRNO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.VesselName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Vol)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowImportProcessI2Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Import_process_I2_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ApplicantNm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Blawb)
                    .HasColumnName("BLAWB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.CntnNo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConsigneeNm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Coo)
                    .HasColumnName("COO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DebNote)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DelvDate)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DespatchNo)
                    .HasColumnName("despatchNo")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FormE)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gweight)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HandlSample)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvRef)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lcn)
                    .HasColumnName("LCN")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Lco)
                    .HasColumnName("LCO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MerchName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NetPay)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nfe).HasColumnName("NFE");

                entity.Property(e => e.NoOfPkgs)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Notify)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Othr)
                    .HasColumnName("othr")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayTrm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Pocurncy)
                    .HasColumnName("POcurncy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pod)
                    .HasColumnName("POD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Pol)
                    .HasColumnName("POL")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ProformaInvNo)
                    .HasColumnName("proformaInvNo")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShipMarks)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ShiprNm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ShiprTrm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.SupCrdNote)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.TtlGv)
                    .HasColumnName("TtlGV")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TtlNetWght)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VesselNm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Volm)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VsEta)
                    .HasColumnName("VsETA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VsEtd)
                    .HasColumnName("VsETD")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowImportProcessI3Data>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Import_process_I3_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.GdsInHouseDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RmInHouse)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.SuplrInNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SuplrNm)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");
            });

            modelBuilder.Entity<WorkflowImportProcessMrFormData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Import_process_MR_form_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Balnc)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BatchNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.Color)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DelQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LineItm)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoLnQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RollNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SapItmCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SapItmDesc).IsUnicode(false);

                entity.Property(e => e.SapPo)
                    .HasColumnName("SapPO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShpQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.UnitPrice)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowImportSapTableData>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_Import_sap_table_data");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Balance)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BatchNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.DelQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("isDeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Litm)
                    .HasColumnName("LItm")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecvdQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RollNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SapItmCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SapItmDesc).IsUnicode(false);

                entity.Property(e => e.SapPo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShipQty)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.Tin)
                    .HasColumnName("TIN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TolU)
                    .HasColumnName("tolU")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitPrice)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowPriorityMap>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_priority_map");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid)
                    .HasColumnName("BPMID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Day)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.PriId)
                    .HasColumnName("PriID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StgId)
                    .HasColumnName("StgID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowProcessDocs>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_process_docs");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Cmt)
                    .HasColumnName("cmt")
                    .IsUnicode(false);

                entity.Property(e => e.CombinedBpmid).HasColumnName("CombinedBPMID");

                entity.Property(e => e.DocClassId).HasColumnName("Doc_Class_ID");

                entity.Property(e => e.DomainId).HasColumnName("DomainID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath).IsUnicode(false);

                entity.Property(e => e.FileSize)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("isDeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StageName)
                    .HasColumnName("Stage_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StageOrder).HasColumnName("Stage_order");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubCompId).HasColumnName("Sub_CompID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");

                entity.Property(e => e.UploadedOn).HasColumnType("datetime");

                entity.Property(e => e.VerId)
                    .HasColumnName("verID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Wfid).HasColumnName("WFID");

                entity.HasOne(d => d.DocClass)
                    .WithMany(p => p.WorkflowProcessDocs)
                    .HasForeignKey(d => d.DocClassId)
                    .HasConstraintName("FK_workflow_process_docs_DocumentClass_table");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.WorkflowProcessDocs)
                    .HasForeignKey(d => d.DomainId)
                    .HasConstraintName("FK_workflow_process_docs_Domain_table");

                entity.HasOne(d => d.SubComp)
                    .WithMany(p => p.WorkflowProcessDocs)
                    .HasForeignKey(d => d.SubCompId)
                    .HasConstraintName("FK_workflow_process_docs_Sub_Company_table");

                entity.HasOne(d => d.UploadedByNavigation)
                    .WithMany(p => p.WorkflowProcessDocs)
                    .HasForeignKey(d => d.UploadedBy)
                    .HasConstraintName("FK_workflow_process_docs_Employee_table");

                entity.HasOne(d => d.Wf)
                    .WithMany(p => p.WorkflowProcessDocs)
                    .HasForeignKey(d => d.Wfid)
                    .HasConstraintName("FK_workflow_process_docs_workflow_table");
            });

            modelBuilder.Entity<WorkflowProcessException>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_process_exception");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.RaisedBy).HasColumnName("raisedBy");

                entity.Property(e => e.RaisedMsg)
                    .HasColumnName("raisedMsg")
                    .IsUnicode(false);

                entity.Property(e => e.RaisedOn)
                    .HasColumnName("raisedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.RaisedTo).HasColumnName("raisedTo");

                entity.Property(e => e.StageOrder).HasColumnName("stageOrder");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdId).HasColumnName("thrdID");

                entity.Property(e => e.WorkFlowId).HasColumnName("workFlowID");
            });

            modelBuilder.Entity<WorkflowProgressTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_progress_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ChkData)
                    .HasColumnName("chkData")
                    .IsUnicode(false);

                entity.Property(e => e.ChkId)
                    .HasColumnName("chkID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .IsUnicode(false);

                entity.Property(e => e.CommunicateTo)
                    .HasColumnName("communicateTo")
                    .IsUnicode(false);

                entity.Property(e => e.ComplitionStatus).HasColumnName("complitionStatus");

                entity.Property(e => e.DefaultFile)
                    .HasColumnName("defaultFile")
                    .IsUnicode(false);

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.FormData)
                    .HasColumnName("formData")
                    .IsUnicode(false);

                entity.Property(e => e.FormId)
                    .HasColumnName("formID")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PeopleType)
                    .HasColumnName("peopleType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonId)
                    .HasColumnName("personID")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ProcessStatus)
                    .HasColumnName("processStatus")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecvDate)
                    .HasColumnName("recv_date")
                    .HasColumnType("date");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.StgId).HasColumnName("stgID");

                entity.Property(e => e.SubmissionDate)
                    .HasColumnName("submission_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.SubmissionStatus).HasColumnName("submissionStatus");

                entity.Property(e => e.SubmittedBy)
                    .HasColumnName("submittedBy")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ThrdId).HasColumnName("thrdID");

                entity.Property(e => e.ThreadStatus).HasColumnName("threadStatus");

                entity.Property(e => e.WfId).HasColumnName("wfID");

                entity.HasOne(d => d.Wf)
                    .WithMany(p => p.WorkflowProgressTable)
                    .HasForeignKey(d => d.WfId)
                    .HasConstraintName("FK_workflow_progress_table_workflow_table");
            });

            modelBuilder.Entity<WorkflowSapTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_sap_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.Bpmid).HasColumnName("BPMID");

                entity.Property(e => e.DomainId).HasColumnName("domainID");

                entity.Property(e => e.PeopleId).HasColumnName("PeopleID");

                entity.Property(e => e.SapFormId).HasColumnName("sapFormID");

                entity.Property(e => e.StgId).HasColumnName("StgID");

                entity.Property(e => e.SubcmpId).HasColumnName("subcmpID");

                entity.Property(e => e.ThrdId).HasColumnName("ThrdID");
            });

            modelBuilder.Entity<WorkflowStageComment>(entity =>
            {
                entity.HasKey(e => e.CommentId);

                entity.ToTable("workflow_stage_Comment");

                entity.Property(e => e.CommentId)
                    .HasColumnName("CommentID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Action).HasMaxLength(500);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.StageOrderId).HasColumnName("StageOrderID");

                entity.Property(e => e.WorkFlowId).HasColumnName("WorkFlowID");
            });

            modelBuilder.Entity<WorkflowStageTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_stage_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ActiveStatus).HasColumnName("activeStatus");

                entity.Property(e => e.ApproverWorkFlow)
                    .HasColumnName("approverWorkFlow")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Attachment)
                    .HasColumnName("attachment")
                    .IsUnicode(false);

                entity.Property(e => e.Checklist)
                    .HasColumnName("checklist")
                    .IsUnicode(false);

                entity.Property(e => e.CommunicateTo)
                    .HasColumnName("communicateTo")
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("createdOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.DelegatedTo).IsUnicode(false);

                entity.Property(e => e.DeletedBy)
                    .HasColumnName("deletedBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeletedOn)
                    .HasColumnName("deletedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.DynGroupId).HasDefaultValueSql("((0))");

                entity.Property(e => e.DynRef)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EditedBy)
                    .HasColumnName("editedBy")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EditedOn)
                    .HasColumnName("editedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.Form)
                    .HasColumnName("form")
                    .IsUnicode(false);

                entity.Property(e => e.FormTemplate)
                    .HasColumnName("formTemplate")
                    .IsUnicode(false);

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.People)
                    .HasColumnName("people")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PeopleId).HasColumnName("PeopleID");

                entity.Property(e => e.PeopleInfo)
                    .HasColumnName("peopleInfo")
                    .IsUnicode(false);

                entity.Property(e => e.ProcessNo)
                    .HasColumnName("processNo")
                    .IsUnicode(false);

                entity.Property(e => e.ProcessYes)
                    .HasColumnName("processYes")
                    .IsUnicode(false);

                entity.Property(e => e.Rasci)
                    .HasColumnName("RASCI")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SapForm)
                    .HasColumnName("sapForm")
                    .IsUnicode(false);

                entity.Property(e => e.StageDays)
                    .HasColumnName("stageDays")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StageDescription)
                    .HasColumnName("stageDescription")
                    .IsUnicode(false);

                entity.Property(e => e.StageName)
                    .HasColumnName("stageName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StageOrderId)
                    .HasColumnName("stageOrderID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .IsUnicode(false);

                entity.Property(e => e.Viewright)
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.WorkFlowId).HasColumnName("workFlowID");
            });

            modelBuilder.Entity<WorkflowTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ActiveStatus).HasColumnName("activeStatus");

                entity.Property(e => e.Bucketing)
                    .HasColumnName("bucketing")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("createdOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeletedBy).HasColumnName("deletedBy");

                entity.Property(e => e.DeletedOn)
                    .HasColumnName("deletedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.DocClass)
                    .HasColumnName("docClass")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DocClassId).HasColumnName("DocClassID");

                entity.Property(e => e.DocCompanyId).HasColumnName("DocCompanyID");

                entity.Property(e => e.DocDomainId).HasColumnName("DocDomainID");

                entity.Property(e => e.EdditedBy).HasColumnName("edditedBy");

                entity.Property(e => e.EditedBy).HasColumnName("editedBy");

                entity.Property(e => e.EditedOn)
                    .HasColumnName("editedOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.FncDomain).HasColumnName("Fnc_Domain");

                entity.Property(e => e.IsDeleted).HasColumnName("isDeleted");

                entity.Property(e => e.IsReferenceEditable).HasColumnName("isReferenceEditable");

                entity.Property(e => e.Owner)
                    .HasColumnName("owner")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Rasci)
                    .HasColumnName("RASCI")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SubCmpId).HasColumnName("subCmpID");

                entity.Property(e => e.WorkFlowId).HasColumnName("workFlowID");

                entity.Property(e => e.WorkFlowLevel)
                    .HasColumnName("workFlowLevel")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.WorkFlowName)
                    .HasColumnName("workFlowName")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WorkflowThreadProgressTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("workflow_thread_progress_table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.ApproverWorkFlow)
                    .HasColumnName("approverWorkFlow")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Attachment)
                    .HasColumnName("attachment")
                    .IsUnicode(false);

                entity.Property(e => e.BpmtriggerId).HasColumnName("BPMTriggerId");

                entity.Property(e => e.Checklist)
                    .HasColumnName("checklist")
                    .IsUnicode(false);

                entity.Property(e => e.CombinedBpmid).HasColumnName("CombinedBPMID");

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.CommunicateTo)
                    .HasColumnName("communicateTo")
                    .IsUnicode(false);

                entity.Property(e => e.ComplitionStatus).HasColumnName("complitionStatus");

                entity.Property(e => e.CpsimulationRefNumber)
                    .HasColumnName("CPSimulationRefNumber")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .HasColumnName("createdOn")
                    .HasColumnType("datetime");

                entity.Property(e => e.DelegatedTo).IsUnicode(false);

                entity.Property(e => e.DynGroupId).HasDefaultValueSql("((0))");

                entity.Property(e => e.DynRef)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.Form)
                    .HasColumnName("form")
                    .IsUnicode(false);

                entity.Property(e => e.FormTemplate)
                    .HasColumnName("formTemplate")
                    .IsUnicode(false);

                entity.Property(e => e.IsClosed).HasColumnName("isClosed");

                entity.Property(e => e.IsOnHold).HasColumnName("isOnHold");

                entity.Property(e => e.IsOpen).HasColumnName("isOpen");

                entity.Property(e => e.IsWip).HasColumnName("isWip");

                entity.Property(e => e.People)
                    .HasColumnName("people")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PeopleId).HasColumnName("peopleID");

                entity.Property(e => e.PeopleInfo)
                    .HasColumnName("peopleInfo")
                    .IsUnicode(false);

                entity.Property(e => e.ProcessNo)
                    .HasColumnName("processNo")
                    .IsUnicode(false);

                entity.Property(e => e.ProcessYes)
                    .HasColumnName("processYes")
                    .IsUnicode(false);

                entity.Property(e => e.Rasci)
                    .HasColumnName("RASCI")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RecvDate)
                    .HasColumnName("recv_date")
                    .HasColumnType("date");

                entity.Property(e => e.SapForm)
                    .HasColumnName("sapForm")
                    .IsUnicode(false);

                entity.Property(e => e.StageDays).HasColumnName("stageDays");

                entity.Property(e => e.StageDescription)
                    .HasColumnName("stageDescription")
                    .IsUnicode(false);

                entity.Property(e => e.StageName)
                    .HasColumnName("stageName")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.StageOrderId).HasColumnName("stageOrderID");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.SubmissionDate)
                    .HasColumnName("submission_date")
                    .HasColumnType("date");

                entity.Property(e => e.ThrdRef)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ThreadId).HasColumnName("threadID");

                entity.Property(e => e.ThreadStatus).HasColumnName("threadStatus");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .IsUnicode(false);

                entity.Property(e => e.Viewright).HasMaxLength(150);

                entity.Property(e => e.WorkFlowId).HasColumnName("workFlowID");
            });

            modelBuilder.Entity<WPriorityTable>(entity =>
            {
                entity.HasKey(e => e.Slno);

                entity.ToTable("W_Priority_Table");

                entity.Property(e => e.Slno).HasColumnName("SLNO");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DeletedOn).HasColumnType("datetime");

                entity.Property(e => e.Fdom).HasColumnName("FDom");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PriDesc)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PriName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PriPer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Scoy).HasColumnName("SCoy");
            });
        }
    }
}
