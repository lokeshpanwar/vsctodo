﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class EmployeeProcessValue
    {
        public long Slno { get; set; }
        public long? UserId { get; set; }
        public double? MaxValue { get; set; }
        public double? MinValue { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsApprover { get; set; }

        public EmployeeTable User { get; set; }
    }
}
