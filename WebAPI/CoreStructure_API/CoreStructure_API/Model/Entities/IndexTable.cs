﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class IndexTable
    {
        public long Slno { get; set; }
        public string ParentCompId { get; set; }
        public string SubCompId { get; set; }
        public string IndexName { get; set; }
        public string IndexDescription { get; set; }
        public string IndexType { get; set; }
        public string IndexLength { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
