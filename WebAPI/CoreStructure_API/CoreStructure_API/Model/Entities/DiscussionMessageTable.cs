﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DiscussionMessageTable
    {
        public long Slno { get; set; }
        public long? DiscussionId { get; set; }
        public string Userid { get; set; }
        public string Message { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }

        public DiscussTable Discussion { get; set; }
    }
}
