﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpSimulation
    {
        public long Slno { get; set; }
        public string SoTemplateName { get; set; }
        public string Sappo { get; set; }
        public string RefNumber { get; set; }
        public string CustomerName { get; set; }
        public string ProductDetails { get; set; }
        public long? Mesid { get; set; }
        public long? Dlid { get; set; }
        public string SimulationType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IncludeStartDate { get; set; }
        public bool? AdditionOneDay { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
