﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DmsprocessApprovers
    {
        public long Slno { get; set; }
        public long? DmsproceesId { get; set; }
        public long? UserId { get; set; }
        public string Action { get; set; }
        public string Comments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public Dmsprocess Dmsprocees { get; set; }
        public EmployeeTable User { get; set; }
    }
}
