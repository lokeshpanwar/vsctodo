﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class VenTable
    {
        public long Slno { get; set; }
        public long? UserId { get; set; }
        public string CompanyName { get; set; }
        public string Url { get; set; }
        public string Address { get; set; }
        public string LegalStatus { get; set; }
        public string Remarks { get; set; }
        public string Bcountry { get; set; }
        public string SwiftCode { get; set; }
        public string ExtUserBankAcNo { get; set; }
        public string AcHolderName { get; set; }
        public string Ibanno { get; set; }
        public string BankName { get; set; }
        public string IfscCode { get; set; }
        public string BankBranch { get; set; }
        public string HouseNoBank { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string CityPostalCode { get; set; }
        public string RegionState { get; set; }
        public bool? IsDashboard { get; set; }
        public string BankTelNo { get; set; }
        public string Bankmobileno { get; set; }
        public string Bankfaxno { get; set; }
        public string Bankemail { get; set; }

        public EmployeeTable User { get; set; }
    }
}
