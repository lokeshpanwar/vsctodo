﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastCommunicationMode
    {
        public int MastComModeId { get; set; }
        public string MastComModeType { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
