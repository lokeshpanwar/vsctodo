﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentTypeDocClassMappingTable
    {
        public long Slno { get; set; }
        public long? DocumentClassId { get; set; }
        public long? DocumentTypeId { get; set; }
        public long? SubCompId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DocumentClassTable DocumentClass { get; set; }
        public DocumentTypeTable DocumentType { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
    }
}
