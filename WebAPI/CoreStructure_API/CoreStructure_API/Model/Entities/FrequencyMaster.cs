﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class FrequencyMaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? Days { get; set; }
        public int? Isdelete { get; set; }
        public DateTime? Cretatedon { get; set; }
        public DateTime? Modifiedon { get; set; }
        public int? IsActive { get; set; }
        public int? Updatedby { get; set; }
    }
}
