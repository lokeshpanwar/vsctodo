﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ParentCompanyTable
    {
        public ParentCompanyTable()
        {
            AclPermissionMappingTable = new HashSet<AclPermissionMappingTable>();
            AclTemplateTable = new HashSet<AclTemplateTable>();
            DocumentRevisionLogTable = new HashSet<DocumentRevisionLogTable>();
            DocumentTypeTable = new HashSet<DocumentTypeTable>();
            DomainMappingTable = new HashSet<DomainMappingTable>();
            GroupTable = new HashSet<GroupTable>();
            LogHistoryTable = new HashSet<LogHistoryTable>();
            RoleTable = new HashSet<RoleTable>();
            SubCompanyTable = new HashSet<SubCompanyTable>();
        }

        public long Slno { get; set; }
        public string CompanyName { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string Kpid { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? UpdatedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public ICollection<AclPermissionMappingTable> AclPermissionMappingTable { get; set; }
        public ICollection<AclTemplateTable> AclTemplateTable { get; set; }
        public ICollection<DocumentRevisionLogTable> DocumentRevisionLogTable { get; set; }
        public ICollection<DocumentTypeTable> DocumentTypeTable { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTable { get; set; }
        public ICollection<GroupTable> GroupTable { get; set; }
        public ICollection<LogHistoryTable> LogHistoryTable { get; set; }
        public ICollection<RoleTable> RoleTable { get; set; }
        public ICollection<SubCompanyTable> SubCompanyTable { get; set; }
    }
}
