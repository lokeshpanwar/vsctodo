﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AuditMasterDetails
    {
        public int Id { get; set; }
        public int? Auditid { get; set; }
        public string ControlArea { get; set; }
        public string Objective { get; set; }
        public string ControlNo { get; set; }
        public string ControlDescription { get; set; }
        public string Artefacts { get; set; }
        public string Remarks { get; set; }
        public int? IsDelete { get; set; }
        public int? IsActive { get; set; }
        public DateTime? Createon { get; set; }
        public DateTime? Modifyon { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public DateTime? PurposeDate { get; set; }
        public int? PurposeReqUserId { get; set; }
        public int? Afidstatus { get; set; }
        public string ArtefactRef { get; set; }
        public DateTime? DateOfUpload { get; set; }
        public string PurposeCommentsAuditee { get; set; }
        public int? Ncstatus { get; set; }
        public DateTime? St3Date { get; set; }
        public int? St3Userid { get; set; }
        public string Observation { get; set; }
        public int? Priority { get; set; }
        public string Recommendation { get; set; }
        public int? St3Status { get; set; }
        public DateTime? St4Date { get; set; }
        public int? St4Userid { get; set; }
        public string Response { get; set; }
        public DateTime? Edc { get; set; }
        public string RefDocClose { get; set; }
        public DateTime? ActualDateofCloser { get; set; }
        public int? St4Status { get; set; }
        public DateTime? St5Date { get; set; }
        public int? St5Userid { get; set; }
        public int? St5Status { get; set; }
        public string St5Comment { get; set; }
    }
}
