﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowCpProcessTable
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? Tbd { get; set; }
        public int? Tbac { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Prity { get; set; }
        public long? Intnal { get; set; }
        public long? Etnal { get; set; }
        public int? Nod { get; set; }
        public string Bom { get; set; }
        public string CpsimulationNumber { get; set; }
        public string CpGroupingNumber { get; set; }
        public int? Status { get; set; }
        public int? Version { get; set; }
        public long? Msid { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? StatusOn { get; set; }
    }
}
