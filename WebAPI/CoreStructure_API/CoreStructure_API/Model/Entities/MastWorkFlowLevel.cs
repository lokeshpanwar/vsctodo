﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastWorkFlowLevel
    {
        public int MastWorkFlowLevelId { get; set; }
        public string MastWorkFlowLevelValue { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
