﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class TblModules
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string ModulePage { get; set; }
        public string PageTag { get; set; }
    }
}
