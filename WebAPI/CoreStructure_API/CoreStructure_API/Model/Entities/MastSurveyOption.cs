﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastSurveyOption
    {
        public int OptionId { get; set; }
        public string OptionValue { get; set; }
    }
}
