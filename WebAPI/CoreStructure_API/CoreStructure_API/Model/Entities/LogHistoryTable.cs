﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class LogHistoryTable
    {
        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public string Action { get; set; }
        public long? UserId { get; set; }
        public DateTime? Datetime { get; set; }
        public string Ipaddress { get; set; }
        public string Type { get; set; }
        public string Parameter { get; set; }
        public string Comments { get; set; }

        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public EmployeeTable User { get; set; }
    }
}
