﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class EmployeeTable
    {
        public EmployeeTable()
        {
            DiscussVote = new HashSet<DiscussVote>();
            Dmsprocess = new HashSet<Dmsprocess>();
            DmsprocessApprovers = new HashSet<DmsprocessApprovers>();
            DmsprocessCheckers = new HashSet<DmsprocessCheckers>();
            DocClassMappingTableCreatedByNavigation = new HashSet<DocClassMappingTable>();
            DocClassMappingTableDeletedByNavigation = new HashSet<DocClassMappingTable>();
            DocClassMappingTableUser = new HashSet<DocClassMappingTable>();
            DocRecordedItemsCreatedByNavigation = new HashSet<DocRecordedItems>();
            DocRecordedItemsModifiedByNavigation = new HashSet<DocRecordedItems>();
            DocRecordedItemsUpdate = new HashSet<DocRecordedItemsUpdate>();
            DocRecordedItemsUser = new HashSet<DocRecordedItems>();
            DocUploadTableCheckoutByNavigation = new HashSet<DocUploadTable>();
            DocUploadTableCopiedByNavigation = new HashSet<DocUploadTable>();
            DocUploadTableCreatedByNavigation = new HashSet<DocUploadTable>();
            DocUploadTableDeletedByNavigation = new HashSet<DocUploadTable>();
            DocUploadTableMovedByNavigation = new HashSet<DocUploadTable>();
            DocUploadTableUploadedByNavigation = new HashSet<DocUploadTable>();
            DocumentClassTableCreatedByNavigation = new HashSet<DocumentClassTable>();
            DocumentClassTableDeletedByNavigation = new HashSet<DocumentClassTable>();
            DocumentClassTableModifiedByNavigation = new HashSet<DocumentClassTable>();
            DocumentComentTable = new HashSet<DocumentComentTable>();
            DocumentRevisionLogTable = new HashSet<DocumentRevisionLogTable>();
            DocumentSetMasterCreatedByNavigation = new HashSet<DocumentSetMaster>();
            DocumentSetMasterModifiedByNavigation = new HashSet<DocumentSetMaster>();
            DocumentSetPermission = new HashSet<DocumentSetPermission>();
            DocumentTypeDocClassMappingTableCreatedByNavigation = new HashSet<DocumentTypeDocClassMappingTable>();
            DocumentTypeDocClassMappingTableDeletedByNavigation = new HashSet<DocumentTypeDocClassMappingTable>();
            DocumentTypeDocClassMappingTableModifiedByNavigation = new HashSet<DocumentTypeDocClassMappingTable>();
            DocumentTypeTableCreatedByNavigation = new HashSet<DocumentTypeTable>();
            DocumentTypeTableModifiedByNavigation = new HashSet<DocumentTypeTable>();
            DomainMappingTableDeletedByNavigation = new HashSet<DomainMappingTable>();
            DomainMappingTableMapedByNavigation = new HashSet<DomainMappingTable>();
            DomainMappingTableModifiedByNavigation = new HashSet<DomainMappingTable>();
            DomainTableCretedByNavigation = new HashSet<DomainTable>();
            DomainTableDeletedByNavigation = new HashSet<DomainTable>();
            DomainTableModifiedByNavigation = new HashSet<DomainTable>();
            EmployeeProcessValue = new HashSet<EmployeeProcessValue>();
            EventTableCreatedByNavigation = new HashSet<EventTable>();
            EventTableDeletedByNavigation = new HashSet<EventTable>();
            EventTableModifiedByNavigation = new HashSet<EventTable>();
            FormItemTableCreatedByNavigation = new HashSet<FormItemTable>();
            FormItemTableDeletedByNavigation = new HashSet<FormItemTable>();
            FormItemTableModifiedByNavigation = new HashSet<FormItemTable>();
            FormTableCreatedByNavigation = new HashSet<FormTable>();
            FormTableDeletedByNavigation = new HashSet<FormTable>();
            FormTableModifiedByNavigation = new HashSet<FormTable>();
            GroupTableCreatedByNavigation = new HashSet<GroupTable>();
            GroupTableDeletedByNavigation = new HashSet<GroupTable>();
            GroupTableModifiedByNavigation = new HashSet<GroupTable>();
            ListItemTableCreatedByNavigation = new HashSet<ListItemTable>();
            ListItemTableDeletedByNavigation = new HashSet<ListItemTable>();
            ListItemTableModifiedByNavigation = new HashSet<ListItemTable>();
            ListTableCreatedByNavigation = new HashSet<ListTable>();
            ListTableDeletedByNavigation = new HashSet<ListTable>();
            ListTableModifiedByNavigation = new HashSet<ListTable>();
            LogHistoryTable = new HashSet<LogHistoryTable>();
            LoginTableAdmin = new HashSet<LoginTable>();
            LoginTableCreationuserNavigation = new HashSet<LoginTable>();
            LoginTableModificationuserNavigation = new HashSet<LoginTable>();
            LoginTableUser = new HashSet<LoginTable>();
            MessageTable = new HashSet<MessageTable>();
            ParentCompanyTable = new HashSet<ParentCompanyTable>();
            RecordFieldDocClassMappingTableCreatedByNavigation = new HashSet<RecordFieldDocClassMappingTable>();
            RecordFieldDocClassMappingTableDeletedByNavigation = new HashSet<RecordFieldDocClassMappingTable>();
            RecordFieldDocClassMappingTableModifiedByNavigation = new HashSet<RecordFieldDocClassMappingTable>();
            RoleMappingTableCreatedByNavigation = new HashSet<RoleMappingTable>();
            RoleMappingTableUser = new HashSet<RoleMappingTable>();
            RoleTableCreatedByNavigation = new HashSet<RoleTable>();
            RoleTableDeletedByNavigation = new HashSet<RoleTable>();
            RoleTableModifiedByNavigation = new HashSet<RoleTable>();
            UserDocClassMappingTableCreatedByNavigation = new HashSet<UserDocClassMappingTable>();
            UserDocClassMappingTableDeletedByNavigation = new HashSet<UserDocClassMappingTable>();
            UserDocClassMappingTableModifiedByNavigation = new HashSet<UserDocClassMappingTable>();
            UserDocClassMappingTableUser = new HashSet<UserDocClassMappingTable>();
            UserDocTypeMappingTableCreatedByNavigation = new HashSet<UserDocTypeMappingTable>();
            UserDocTypeMappingTableDeletedByNavigation = new HashSet<UserDocTypeMappingTable>();
            UserDocTypeMappingTableModifiedByNavigation = new HashSet<UserDocTypeMappingTable>();
            UserDocTypeMappingTableUser = new HashSet<UserDocTypeMappingTable>();
            UserDocumentMappingTableCreatedByNavigation = new HashSet<UserDocumentMappingTable>();
            UserDocumentMappingTableDeletedByNavigation = new HashSet<UserDocumentMappingTable>();
            UserDocumentMappingTableModifiedByNavigation = new HashSet<UserDocumentMappingTable>();
            UserDocumentMappingTableUser = new HashSet<UserDocumentMappingTable>();
            UserNotificationReceiver = new HashSet<UserNotification>();
            UserNotificationSener = new HashSet<UserNotification>();
            VenTable = new HashSet<VenTable>();
            WidgetSettings = new HashSet<WidgetSettings>();
            WorkflowProcessDocs = new HashSet<WorkflowProcessDocs>();
        }

        public long Slno { get; set; }
        public string EmpId { get; set; }
        public string UserType { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public long? SubCompany { get; set; }
        public string Department { get; set; }
        public long? DomainId { get; set; }
        public string Location { get; set; }
        public string ReportingManagerId { get; set; }
        public long? ContactNo { get; set; }
        public byte[] EmpPhoto { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public DateTime? Doj { get; set; }
        public DateTime? Dob { get; set; }
        public string Userid { get; set; }
        public string Password { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsManager { get; set; }
        public bool? IsUserFp { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Creationuser { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Modifactionuser { get; set; }
        public string EmpPhoto1 { get; set; }
        public string Gender { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }

        public SubCompanyTable SubCompanyNavigation { get; set; }
        public ICollection<DiscussVote> DiscussVote { get; set; }
        public ICollection<Dmsprocess> Dmsprocess { get; set; }
        public ICollection<DmsprocessApprovers> DmsprocessApprovers { get; set; }
        public ICollection<DmsprocessCheckers> DmsprocessCheckers { get; set; }
        public ICollection<DocClassMappingTable> DocClassMappingTableCreatedByNavigation { get; set; }
        public ICollection<DocClassMappingTable> DocClassMappingTableDeletedByNavigation { get; set; }
        public ICollection<DocClassMappingTable> DocClassMappingTableUser { get; set; }
        public ICollection<DocRecordedItems> DocRecordedItemsCreatedByNavigation { get; set; }
        public ICollection<DocRecordedItems> DocRecordedItemsModifiedByNavigation { get; set; }
        public ICollection<DocRecordedItemsUpdate> DocRecordedItemsUpdate { get; set; }
        public ICollection<DocRecordedItems> DocRecordedItemsUser { get; set; }
        public ICollection<DocUploadTable> DocUploadTableCheckoutByNavigation { get; set; }
        public ICollection<DocUploadTable> DocUploadTableCopiedByNavigation { get; set; }
        public ICollection<DocUploadTable> DocUploadTableCreatedByNavigation { get; set; }
        public ICollection<DocUploadTable> DocUploadTableDeletedByNavigation { get; set; }
        public ICollection<DocUploadTable> DocUploadTableMovedByNavigation { get; set; }
        public ICollection<DocUploadTable> DocUploadTableUploadedByNavigation { get; set; }
        public ICollection<DocumentClassTable> DocumentClassTableCreatedByNavigation { get; set; }
        public ICollection<DocumentClassTable> DocumentClassTableDeletedByNavigation { get; set; }
        public ICollection<DocumentClassTable> DocumentClassTableModifiedByNavigation { get; set; }
        public ICollection<DocumentComentTable> DocumentComentTable { get; set; }
        public ICollection<DocumentRevisionLogTable> DocumentRevisionLogTable { get; set; }
        public ICollection<DocumentSetMaster> DocumentSetMasterCreatedByNavigation { get; set; }
        public ICollection<DocumentSetMaster> DocumentSetMasterModifiedByNavigation { get; set; }
        public ICollection<DocumentSetPermission> DocumentSetPermission { get; set; }
        public ICollection<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTableCreatedByNavigation { get; set; }
        public ICollection<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTableDeletedByNavigation { get; set; }
        public ICollection<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTableModifiedByNavigation { get; set; }
        public ICollection<DocumentTypeTable> DocumentTypeTableCreatedByNavigation { get; set; }
        public ICollection<DocumentTypeTable> DocumentTypeTableModifiedByNavigation { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTableDeletedByNavigation { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTableMapedByNavigation { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTableModifiedByNavigation { get; set; }
        public ICollection<DomainTable> DomainTableCretedByNavigation { get; set; }
        public ICollection<DomainTable> DomainTableDeletedByNavigation { get; set; }
        public ICollection<DomainTable> DomainTableModifiedByNavigation { get; set; }
        public ICollection<EmployeeProcessValue> EmployeeProcessValue { get; set; }
        public ICollection<EventTable> EventTableCreatedByNavigation { get; set; }
        public ICollection<EventTable> EventTableDeletedByNavigation { get; set; }
        public ICollection<EventTable> EventTableModifiedByNavigation { get; set; }
        public ICollection<FormItemTable> FormItemTableCreatedByNavigation { get; set; }
        public ICollection<FormItemTable> FormItemTableDeletedByNavigation { get; set; }
        public ICollection<FormItemTable> FormItemTableModifiedByNavigation { get; set; }
        public ICollection<FormTable> FormTableCreatedByNavigation { get; set; }
        public ICollection<FormTable> FormTableDeletedByNavigation { get; set; }
        public ICollection<FormTable> FormTableModifiedByNavigation { get; set; }
        public ICollection<GroupTable> GroupTableCreatedByNavigation { get; set; }
        public ICollection<GroupTable> GroupTableDeletedByNavigation { get; set; }
        public ICollection<GroupTable> GroupTableModifiedByNavigation { get; set; }
        public ICollection<ListItemTable> ListItemTableCreatedByNavigation { get; set; }
        public ICollection<ListItemTable> ListItemTableDeletedByNavigation { get; set; }
        public ICollection<ListItemTable> ListItemTableModifiedByNavigation { get; set; }
        public ICollection<ListTable> ListTableCreatedByNavigation { get; set; }
        public ICollection<ListTable> ListTableDeletedByNavigation { get; set; }
        public ICollection<ListTable> ListTableModifiedByNavigation { get; set; }
        public ICollection<LogHistoryTable> LogHistoryTable { get; set; }
        public ICollection<LoginTable> LoginTableAdmin { get; set; }
        public ICollection<LoginTable> LoginTableCreationuserNavigation { get; set; }
        public ICollection<LoginTable> LoginTableModificationuserNavigation { get; set; }
        public ICollection<LoginTable> LoginTableUser { get; set; }
        public ICollection<MessageTable> MessageTable { get; set; }
        public ICollection<ParentCompanyTable> ParentCompanyTable { get; set; }
        public ICollection<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTableCreatedByNavigation { get; set; }
        public ICollection<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTableDeletedByNavigation { get; set; }
        public ICollection<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTableModifiedByNavigation { get; set; }
        public ICollection<RoleMappingTable> RoleMappingTableCreatedByNavigation { get; set; }
        public ICollection<RoleMappingTable> RoleMappingTableUser { get; set; }
        public ICollection<RoleTable> RoleTableCreatedByNavigation { get; set; }
        public ICollection<RoleTable> RoleTableDeletedByNavigation { get; set; }
        public ICollection<RoleTable> RoleTableModifiedByNavigation { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTableCreatedByNavigation { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTableDeletedByNavigation { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTableModifiedByNavigation { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTableUser { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTableCreatedByNavigation { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTableDeletedByNavigation { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTableModifiedByNavigation { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTableUser { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTableCreatedByNavigation { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTableDeletedByNavigation { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTableModifiedByNavigation { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTableUser { get; set; }
        public ICollection<UserNotification> UserNotificationReceiver { get; set; }
        public ICollection<UserNotification> UserNotificationSener { get; set; }
        public ICollection<VenTable> VenTable { get; set; }
        public ICollection<WidgetSettings> WidgetSettings { get; set; }
        public ICollection<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
    }
}
