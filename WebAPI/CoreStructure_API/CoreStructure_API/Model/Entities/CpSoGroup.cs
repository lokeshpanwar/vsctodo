﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpSoGroup
    {
        public long Slno { get; set; }
        public string GroupName { get; set; }
        public string Solist { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? Activated { get; set; }
        public DateTime? ActivatedOn { get; set; }
        public long? ActivatedBy { get; set; }
    }
}
