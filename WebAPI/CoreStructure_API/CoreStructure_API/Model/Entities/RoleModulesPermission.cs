﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class RoleModulesPermission
    {
        public long Id { get; set; }
        public int? ModuleId { get; set; }
        public long? RoleId { get; set; }
        public string FormName { get; set; }
        public bool? IsRight { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsWrite { get; set; }
        public bool? IsView { get; set; }
        public bool? IsDelete { get; set; }
    }
}
