﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowExportProcessE2Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string FacName { get; set; }
        public string InvRef { get; set; }
        public string FacInvoice { get; set; }
        public string CusPo { get; set; }
        public string SapPo { get; set; }
        public string SapLi { get; set; }
        public string Etd { get; set; }
        public string Eta { get; set; }
        public string InvCurrency { get; set; }
        public string Fcrblhawb { get; set; }
        public string VesselName { get; set; }
        public string Pod { get; set; }
        public string PreCarriage { get; set; }
        public string UltDestination { get; set; }
        public string Cogog { get; set; }
        public string ShipMode { get; set; }
        public string CertiOrigin { get; set; }
        public string InvDate { get; set; }
        public string ShipQty { get; set; }
        public string InvValue { get; set; }
        public string NoCrtn { get; set; }
        public string Goh { get; set; }
        public string StylNo { get; set; }
        public string TtleMeasurement { get; set; }
        public string CrgHandDate { get; set; }
        public string ItmDisc { get; set; }
        public string Gweight { get; set; }
        public string NetWeight { get; set; }
        public string Ibd { get; set; }
        public string Hscode { get; set; }
        public string Color { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
