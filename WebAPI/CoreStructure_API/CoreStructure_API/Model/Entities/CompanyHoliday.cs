﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CompanyHoliday
    {
        public long Entryid { get; set; }
        public string CompanyId { get; set; }
        public DateTime? YearStartDate { get; set; }
        public DateTime? YearEndDate { get; set; }
        public string HolidaysDate { get; set; }
        public int? Workingdays { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
