﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocUploadMaster
    {
        public DocUploadMaster()
        {
            DocUploadTable = new HashSet<DocUploadTable>();
        }

        public long DocumentId { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public long? DocClassId { get; set; }
        public long? DocTypeId { get; set; }
        public string FileName { get; set; }
        public string Doctype { get; set; }
        public long? Sensitivity { get; set; }

        public DocumentClassTable DocClass { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<DocUploadTable> DocUploadTable { get; set; }
    }
}
