﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class InvitationDetail
    {
        public long InvitationId { get; set; }
        public long? TaskCreateId { get; set; }
        public int? MastGroupId { get; set; }
        public int? TaskstatusId { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
