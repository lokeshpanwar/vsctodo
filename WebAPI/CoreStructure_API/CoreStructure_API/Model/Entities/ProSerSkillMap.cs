﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ProSerSkillMap
    {
        public long EntryId { get; set; }
        public long? ProSerId { get; set; }
        public long? VendorId { get; set; }
        public double? Rating { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
