﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowStageTable
    {
        public int Slno { get; set; }
        public long? WorkFlowId { get; set; }
        public string StageOrderId { get; set; }
        public string StageName { get; set; }
        public string StageDescription { get; set; }
        public string StageDays { get; set; }
        public string People { get; set; }
        public string PeopleInfo { get; set; }
        public string Rasci { get; set; }
        public string Attachment { get; set; }
        public string Url { get; set; }
        public string Form { get; set; }
        public string Checklist { get; set; }
        public string ApproverWorkFlow { get; set; }
        public string ProcessYes { get; set; }
        public string ProcessNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool? ActiveStatus { get; set; }
        public bool? IsAllpeople { get; set; }
        public long? PeopleId { get; set; }
        public string Viewright { get; set; }
        public string CommunicateTo { get; set; }
        public bool? IsDelegated { get; set; }
        public string DelegatedTo { get; set; }
        public string DynRef { get; set; }
        public string SapForm { get; set; }
        public long? DynGroupId { get; set; }
        public string FormTemplate { get; set; }
        public bool? IsKillRights { get; set; }
        public bool? IsComments { get; set; }
        public string ExceptionForm { get; set; }
        public string ExceptionFormTemplate { get; set; }
    }
}
