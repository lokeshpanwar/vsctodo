﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowThreadProgressTable
    {
        public long Slno { get; set; }
        public long? ThreadId { get; set; }
        public long? WorkFlowId { get; set; }
        public long? StageOrderId { get; set; }
        public string StageName { get; set; }
        public string StageDescription { get; set; }
        public long? StageDays { get; set; }
        public string People { get; set; }
        public string PeopleInfo { get; set; }
        public string Rasci { get; set; }
        public string Attachment { get; set; }
        public string Url { get; set; }
        public string Form { get; set; }
        public string Checklist { get; set; }
        public string ApproverWorkFlow { get; set; }
        public string ProcessYes { get; set; }
        public string ProcessNo { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? ThreadStatus { get; set; }
        public bool? ComplitionStatus { get; set; }
        public bool? IsOpen { get; set; }
        public bool? IsWip { get; set; }
        public bool? IsClosed { get; set; }
        public bool? IsOnHold { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? RecvDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public string Comment { get; set; }
        public string CommunicateTo { get; set; }
        public bool? IsDelegated { get; set; }
        public string DelegatedTo { get; set; }
        public long? PeopleId { get; set; }
        public string Viewright { get; set; }
        public string DynRef { get; set; }
        public string SapForm { get; set; }
        public string ThrdRef { get; set; }
        public long? DynGroupId { get; set; }
        public string FormTemplate { get; set; }
        public string CpsimulationRefNumber { get; set; }
        public long? CombinedBpmid { get; set; }
        public long? Combinedthrdid { get; set; }
        public long? BpmtriggerId { get; set; }
    }
}
