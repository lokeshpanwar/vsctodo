﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class SkillMap
    {
        public long Entryid { get; set; }
        public long? Skillid { get; set; }
        public string Empid { get; set; }
        public double? Rating { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
