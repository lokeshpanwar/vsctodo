﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastVendor
    {
        public long EntryId { get; set; }
        public long? Vendorid { get; set; }
        public string Companyname { get; set; }
        public string CompanyPhone { get; set; }
        public string Companyurl { get; set; }
        public string Companyemail { get; set; }
        public string Companyaddress { get; set; }
        public string Companycontctperson { get; set; }
        public string Companydesignation { get; set; }
        public long? Companydepartment { get; set; }
        public string Companylocation { get; set; }
        public string Companyreportingmanager { get; set; }
        public string Vendorphoto { get; set; }
        public DateTime? DateofIncorporation { get; set; }
        public string LegalStatusofcompany { get; set; }
        public bool? Activestatus { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string KingpinId { get; set; }
        public string Remarks { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
