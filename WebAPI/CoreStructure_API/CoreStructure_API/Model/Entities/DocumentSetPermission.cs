﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentSetPermission
    {
        public long Slno { get; set; }
        public long UserId { get; set; }
        public long SetId { get; set; }
        public bool? ViewRight { get; set; }
        public bool? PrintRight { get; set; }
        public bool? EmailRight { get; set; }
        public bool? CheckinRight { get; set; }
        public bool? CheckoutRight { get; set; }
        public bool? DownloadRight { get; set; }
        public bool? UploadRight { get; set; }
        public bool? SelectallRight { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public DocumentSetMaster Set { get; set; }
        public EmployeeTable User { get; set; }
    }
}
