﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class UserDocClassMappingTable
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public long? DocumentClassId { get; set; }
        public long? UserId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool? ViewRight { get; set; }
        public bool? PrintRight { get; set; }
        public bool? EmailRight { get; set; }
        public bool? CheckinRight { get; set; }
        public bool? CheckoutRight { get; set; }
        public bool? DownloadRight { get; set; }
        public bool? UploadRight { get; set; }
        public bool? SelectallRight { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DocumentClassTable DocumentClass { get; set; }
        public DomainTable Domain { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public EmployeeTable User { get; set; }
    }
}
