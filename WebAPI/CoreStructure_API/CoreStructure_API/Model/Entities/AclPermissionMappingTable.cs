﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AclPermissionMappingTable
    {
        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public long? AclId { get; set; }
        public string AssignedSection { get; set; }
        public string AssignedTo { get; set; }
        public bool? CreateAdd { get; set; }
        public bool? ReadView { get; set; }
        public bool? WriteEdit { get; set; }
        public bool? Delet { get; set; }
        public bool? SelectedAll { get; set; }
        public DateTime? AssignedOn { get; set; }
        public long? AssignedBy { get; set; }

        public AclTemplateTable Acl { get; set; }
        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
    }
}
