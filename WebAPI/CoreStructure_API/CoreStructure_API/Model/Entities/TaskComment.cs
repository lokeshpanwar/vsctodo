﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class TaskComment
    {
        public long EntryId { get; set; }
        public long? TaskCreatedId { get; set; }
        public string CommentBy { get; set; }
        public string CommentFor { get; set; }
        public string CommentDes { get; set; }
        public string UserType { get; set; }
        public string Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public string Creationuser { get; set; }
    }
}
