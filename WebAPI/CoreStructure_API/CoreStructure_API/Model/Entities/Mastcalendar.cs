﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class Mastcalendar
    {
        public long ClId { get; set; }
        public long Empid { get; set; }
        public DateTime? LeaveDate { get; set; }
        public string ClDescription { get; set; }
        public bool? Isactive { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
