﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentSetDocTypes
    {
        public long Id { get; set; }
        public long? SetId { get; set; }
        public long? DocumentClassId { get; set; }
        public long? DocumentTypeId { get; set; }

        public DocumentClassTable DocumentClass { get; set; }
        public DocumentTypeTable DocumentType { get; set; }
        public DocumentSetMaster Set { get; set; }
    }
}
