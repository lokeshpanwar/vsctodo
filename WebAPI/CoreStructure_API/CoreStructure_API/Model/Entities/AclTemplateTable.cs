﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AclTemplateTable
    {
        public AclTemplateTable()
        {
            AclPermissionMappingTable = new HashSet<AclPermissionMappingTable>();
        }

        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public string AclTemplateName { get; set; }
        public string AclTemplateDesc { get; set; }
        public string AclFunctionalDomainId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<AclPermissionMappingTable> AclPermissionMappingTable { get; set; }
    }
}
