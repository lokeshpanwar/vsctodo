﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpErpMapData
    {
        public long Slno { get; set; }
        public long? SubCoyId { get; set; }
        public long? DomainId { get; set; }
        public string Mes { get; set; }
        public string Erp { get; set; }
        public string Dec { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
