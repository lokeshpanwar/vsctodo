﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class Stage1Stage
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
