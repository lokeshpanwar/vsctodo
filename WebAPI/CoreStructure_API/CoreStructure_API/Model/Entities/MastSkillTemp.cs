﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastSkillTemp
    {
        public long Entryid { get; set; }
        public string SkillName { get; set; }
        public string SkillDescritpion { get; set; }
        public string Creationuser { get; set; }
    }
}
