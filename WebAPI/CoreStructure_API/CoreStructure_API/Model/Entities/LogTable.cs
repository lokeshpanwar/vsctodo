﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class LogTable
    {
        public long Slno { get; set; }
        public string ParentCompId { get; set; }
        public string SubCompId { get; set; }
        public string UserId { get; set; }
        public string OperationType { get; set; }
        public string OperationPerformed { get; set; }
        public DateTime? OperationOn { get; set; }
        public string Ramarks { get; set; }
        public string IpAddress { get; set; }
        public long? UserId1 { get; set; }
    }
}
