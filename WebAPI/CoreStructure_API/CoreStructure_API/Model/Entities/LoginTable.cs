﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class LoginTable
    {
        public long Slno { get; set; }
        public long? AdminId { get; set; }
        public long? DomainId { get; set; }
        public long? Userid { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public byte[] ProfileImg { get; set; }
        public DateTime? LoginDate { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public long? Creationuser { get; set; }
        public long? Modificationuser { get; set; }
        public long? CurrentDomain { get; set; }
        public long? PreviousDomain { get; set; }
        public bool? IsFirstTimeLogin { get; set; }
        public string Session { get; set; }

        public EmployeeTable Admin { get; set; }
        public EmployeeTable CreationuserNavigation { get; set; }
        public DomainTable CurrentDomainNavigation { get; set; }
        public DomainTable Domain { get; set; }
        public EmployeeTable ModificationuserNavigation { get; set; }
        public DomainTable PreviousDomainNavigation { get; set; }
        public EmployeeTable User { get; set; }
    }
}
