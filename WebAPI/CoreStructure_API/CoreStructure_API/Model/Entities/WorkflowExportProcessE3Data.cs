﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowExportProcessE3Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string GrsInvValue { get; set; }
        public string CrdNoteDeductions { get; set; }
        public string RmDeduction { get; set; }
        public string OtherDeduc { get; set; }
        public string Rewards { get; set; }
        public string NetInvValPay { get; set; }
        public string RexDeclare { get; set; }
        public string CertiOrigin { get; set; }
        public string Fcrblhawb { get; set; }
        public string NotifyPrty { get; set; }
        public string AlNotifyPrty { get; set; }
        public string Etd { get; set; }
        public string Eta { get; set; }
        public string Consignee { get; set; }
        public string Cbm { get; set; }
        public string VesselName { get; set; }
        public string NetWgt { get; set; }
        public string GrsWgt { get; set; }
        public string NoCntr { get; set; }
    }
}
