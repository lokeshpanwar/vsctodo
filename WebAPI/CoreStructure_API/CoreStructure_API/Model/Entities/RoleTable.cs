﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class RoleTable
    {
        public RoleTable()
        {
            RoleMappingTable = new HashSet<RoleMappingTable>();
        }

        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public string RoleEmail { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public long? DeletedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<RoleMappingTable> RoleMappingTable { get; set; }
    }
}
