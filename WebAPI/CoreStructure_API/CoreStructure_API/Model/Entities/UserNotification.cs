﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class UserNotification
    {
        public long NotiId { get; set; }
        public long? SenerId { get; set; }
        public long? ReceiverId { get; set; }
        public long? PageUrl { get; set; }
        public bool? IsReaded { get; set; }
        public long? TypeId { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedOn { get; set; }

        public EmployeeTable Receiver { get; set; }
        public EmployeeTable Sener { get; set; }
    }
}
