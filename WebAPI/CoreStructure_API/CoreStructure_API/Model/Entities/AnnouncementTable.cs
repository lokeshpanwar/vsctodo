﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AnnouncementTable
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public string AnnouncementTitle { get; set; }
        public string AnnouncementDetail { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public long? AnnouncemnetDomain { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public SubCompanyTable SubComp { get; set; }
    }
}
