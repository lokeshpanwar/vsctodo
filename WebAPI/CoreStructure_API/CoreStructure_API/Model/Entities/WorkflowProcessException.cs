﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowProcessException
    {
        public long Slno { get; set; }
        public long? WorkFlowId { get; set; }
        public long? ThrdId { get; set; }
        public long? StageOrder { get; set; }
        public long? RaisedTo { get; set; }
        public string RaisedMsg { get; set; }
        public long? RaisedBy { get; set; }
        public DateTime? RaisedOn { get; set; }
        public string Status { get; set; }
        public bool? IsClosed { get; set; }
    }
}
