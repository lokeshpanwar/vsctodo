﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ListTable
    {
        public ListTable()
        {
            DocClassMappingTable = new HashSet<DocClassMappingTable>();
            DocRecordedItems = new HashSet<DocRecordedItems>();
            ListItemTable = new HashSet<ListItemTable>();
            RecordFieldDocClassMappingTable = new HashSet<RecordFieldDocClassMappingTable>();
        }

        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public string ListName { get; set; }
        public string ListDescription { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<DocClassMappingTable> DocClassMappingTable { get; set; }
        public ICollection<DocRecordedItems> DocRecordedItems { get; set; }
        public ICollection<ListItemTable> ListItemTable { get; set; }
        public ICollection<RecordFieldDocClassMappingTable> RecordFieldDocClassMappingTable { get; set; }
    }
}
