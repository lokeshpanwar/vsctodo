﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class KpModules
    {
        public int Mid { get; set; }
        public int? ModulId { get; set; }
        public string ModuleName { get; set; }
    }
}
