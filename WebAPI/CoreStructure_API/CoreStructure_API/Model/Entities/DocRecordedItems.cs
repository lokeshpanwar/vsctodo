﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocRecordedItems
    {
        public DocRecordedItems()
        {
            DocRecordedItemsUpdate = new HashSet<DocRecordedItemsUpdate>();
        }

        public long Slno { get; set; }
        public long? DocumentClassId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }
        public long? RecordFieldId { get; set; }
        public string RecordFieldItems { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string SensiData { get; set; }
        public string TypeRfdata { get; set; }
        public string OldValues { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public DocumentClassTable DocumentClass { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public ListTable RecordField { get; set; }
        public EmployeeTable User { get; set; }
        public ICollection<DocRecordedItemsUpdate> DocRecordedItemsUpdate { get; set; }
    }
}
