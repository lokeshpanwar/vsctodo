﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowImportProcessI1Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string Srno { get; set; }
        public string ShiprNmAdd { get; set; }
        public string GrpCoyName { get; set; }
        public string ConsigneeNmAdd { get; set; }
        public string PayTerm { get; set; }
        public string LcopenBy { get; set; }
        public string DocCrdNo { get; set; }
        public string ShipMode { get; set; }
        public string VesselName { get; set; }
        public string Etd { get; set; }
        public string Eta { get; set; }
        public string MerchName { get; set; }
        public string MerchEmail { get; set; }
        public string NoPkgs { get; set; }
        public string Vol { get; set; }
        public string Gweight { get; set; }
        public string DelvDate { get; set; }
        public string SapForm { get; set; }
    }
}
