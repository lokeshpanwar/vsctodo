﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastEmployeeFordelete
    {
        public long EmpEntryid { get; set; }
        public string Empid { get; set; }
        public string Empname { get; set; }
        public string Designation { get; set; }
        public long? DepartmentId { get; set; }
        public string Location { get; set; }
        public string ReportingManagerEmpid { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }
        public string EmpPhoto { get; set; }
        public DateTime? Doj { get; set; }
        public DateTime? Dob { get; set; }
        public string Userid { get; set; }
        public string Password { get; set; }
        public bool? IsUserFp { get; set; }
        public bool? IsManager { get; set; }
        public DateTime? LastLogindate { get; set; }
        public DateTime? NotifyViewdate { get; set; }
        public string MastGroupName { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
