﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DesignationMaster
    {
        public int Id { get; set; }
        public string Designation { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? Createdon { get; set; }
    }
}
