﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastVendorTemp
    {
        public long EntryId { get; set; }
        public string Companyname { get; set; }
        public string CompanyPhone { get; set; }
        public string Companyurl { get; set; }
        public string Companyemail { get; set; }
        public string Companyaddress { get; set; }
        public string Companycontctperson { get; set; }
        public string Companydesignation { get; set; }
        public string Companydepartment { get; set; }
        public string Companylocation { get; set; }
        public string Companyreportingmanager { get; set; }
        public DateTime? DateofIncorporation { get; set; }
        public string LegalStatusofcompany { get; set; }
        public string KingpinId { get; set; }
        public string Creationuser { get; set; }
    }
}
