﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class Kptable
    {
        public string KingPinId { get; set; }
        public bool? Isactive { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
