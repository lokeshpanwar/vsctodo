﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastSchTask
    {
        public long SchId { get; set; }
        public long? Taskid { get; set; }
        public string SCheduleDate { get; set; }
    }
}
