﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastEmployeeTemp
    {
        public int EmpEntryid { get; set; }
        public string Empid { get; set; }
        public string Empname { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public long? ReportingManagerEmpid { get; set; }
        public string ContactPhone { get; set; }
        public string Email { get; set; }
        public string Doj { get; set; }
        public string Dob { get; set; }
        public string Password { get; set; }
        public bool? IsManager { get; set; }
        public string Creationuser { get; set; }
    }
}
