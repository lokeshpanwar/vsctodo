﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastPeriodicity
    {
        public int PeriodicityId { get; set; }
        public string PeriodicityType { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
