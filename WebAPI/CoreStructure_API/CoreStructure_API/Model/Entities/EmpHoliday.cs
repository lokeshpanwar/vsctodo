﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class EmpHoliday
    {
        public long Entryid { get; set; }
        public string EmpId { get; set; }
        public DateTime? Leavedate { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
