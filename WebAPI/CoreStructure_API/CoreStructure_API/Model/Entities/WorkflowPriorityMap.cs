﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowPriorityMap
    {
        public long Slno { get; set; }
        public string Bpmid { get; set; }
        public string StgId { get; set; }
        public string PriId { get; set; }
        public string Day { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
