﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class SubCompanyTable
    {
        public SubCompanyTable()
        {
            AclPermissionMappingTable = new HashSet<AclPermissionMappingTable>();
            AclTemplateTable = new HashSet<AclTemplateTable>();
            AnnouncementTable = new HashSet<AnnouncementTable>();
            ChecklistTable = new HashSet<ChecklistTable>();
            DiscussTable = new HashSet<DiscussTable>();
            DocUploadMaster = new HashSet<DocUploadMaster>();
            DocUploadTable = new HashSet<DocUploadTable>();
            DocumentClassTable = new HashSet<DocumentClassTable>();
            DocumentRevisionLogTable = new HashSet<DocumentRevisionLogTable>();
            DocumentSetMaster = new HashSet<DocumentSetMaster>();
            DocumentTypeTable = new HashSet<DocumentTypeTable>();
            DomainMappingTable = new HashSet<DomainMappingTable>();
            EmployeeTable = new HashSet<EmployeeTable>();
            EventTable = new HashSet<EventTable>();
            FormTable = new HashSet<FormTable>();
            GroupTable = new HashSet<GroupTable>();
            ListTable = new HashSet<ListTable>();
            LogHistoryTable = new HashSet<LogHistoryTable>();
            RoleTable = new HashSet<RoleTable>();
            WidgetSettings = new HashSet<WidgetSettings>();
            WorkflowProcessDocs = new HashSet<WorkflowProcessDocs>();
        }

        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public string ParentCompName { get; set; }
        public string SubCompanyId { get; set; }
        public string SubCompanyName { get; set; }
        public string Address { get; set; }
        public long? ContactNo { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool? IsParentCompany { get; set; }
        public string CountryCode { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? UpdatedBy { get; set; }
        public long? CreatedBy { get; set; }
        public long? DeletedBy { get; set; }

        public ParentCompanyTable ParentComp { get; set; }
        public ICollection<AclPermissionMappingTable> AclPermissionMappingTable { get; set; }
        public ICollection<AclTemplateTable> AclTemplateTable { get; set; }
        public ICollection<AnnouncementTable> AnnouncementTable { get; set; }
        public ICollection<ChecklistTable> ChecklistTable { get; set; }
        public ICollection<DiscussTable> DiscussTable { get; set; }
        public ICollection<DocUploadMaster> DocUploadMaster { get; set; }
        public ICollection<DocUploadTable> DocUploadTable { get; set; }
        public ICollection<DocumentClassTable> DocumentClassTable { get; set; }
        public ICollection<DocumentRevisionLogTable> DocumentRevisionLogTable { get; set; }
        public ICollection<DocumentSetMaster> DocumentSetMaster { get; set; }
        public ICollection<DocumentTypeTable> DocumentTypeTable { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTable { get; set; }
        public ICollection<EmployeeTable> EmployeeTable { get; set; }
        public ICollection<EventTable> EventTable { get; set; }
        public ICollection<FormTable> FormTable { get; set; }
        public ICollection<GroupTable> GroupTable { get; set; }
        public ICollection<ListTable> ListTable { get; set; }
        public ICollection<LogHistoryTable> LogHistoryTable { get; set; }
        public ICollection<RoleTable> RoleTable { get; set; }
        public ICollection<WidgetSettings> WidgetSettings { get; set; }
        public ICollection<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
    }
}
