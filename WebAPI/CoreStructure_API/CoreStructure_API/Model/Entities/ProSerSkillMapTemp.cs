﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ProSerSkillMapTemp
    {
        public long EntryId { get; set; }
        public string ProSerName { get; set; }
        public string VendorName { get; set; }
        public double? Rating { get; set; }
        public string Creationuser { get; set; }
    }
}
