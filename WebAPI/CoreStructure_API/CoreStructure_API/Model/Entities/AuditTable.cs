﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AuditTable
    {
        public long Slno { get; set; }
        public long? AuditId { get; set; }
        public string Category { get; set; }
        public string RiskPriority { get; set; }
        public string AuditIssues { get; set; }
        public string Recommandation { get; set; }
        public string ManagementResponse { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public string AuditorTracking { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
