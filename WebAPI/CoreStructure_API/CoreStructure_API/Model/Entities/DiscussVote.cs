﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DiscussVote
    {
        public long Id { get; set; }
        public long? DiscussId { get; set; }
        public long? UserId { get; set; }
        public bool? Up { get; set; }
        public bool? Down { get; set; }
        public DateTime? Creadedon { get; set; }

        public DiscussTable Discuss { get; set; }
        public EmployeeTable User { get; set; }
    }
}
