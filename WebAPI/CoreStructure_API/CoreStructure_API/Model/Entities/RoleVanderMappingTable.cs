﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class RoleVanderMappingTable
    {
        public long Slno { get; set; }
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
    }
}
