﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentRevisionLogTable
    {
        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public long? DocumentId { get; set; }
        public string RevisionId { get; set; }
        public string Action { get; set; }
        public long? UserId { get; set; }
        public DateTime? Datetime { get; set; }
        public long? DocumentClassId { get; set; }
        public string DocumentClassName { get; set; }
        public string Filename { get; set; }
        public string Comments { get; set; }

        public DocumentClassTable DocumentClass { get; set; }
        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public EmployeeTable User { get; set; }
    }
}
