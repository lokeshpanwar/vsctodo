﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class RoleMappingTable
    {
        public long Slno { get; set; }
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public RoleTable Role { get; set; }
        public EmployeeTable User { get; set; }
    }
}
