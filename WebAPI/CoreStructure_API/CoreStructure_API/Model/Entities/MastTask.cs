﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastTask
    {
        public long TaskCreateId { get; set; }
        public int? MastTaskId { get; set; }
        public string Taskheading { get; set; }
        public int? ProrityId { get; set; }
        public int? Workflowid { get; set; }
        public int? ActionId { get; set; }
        public string TaskOwnerId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public string AttachmentPath { get; set; }
        public string TaskDescription { get; set; }
        public int? TaskstatusId { get; set; }
        public string TaskOwnerType { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
        public int? Bucketing { get; set; }
        public long? Workingdays { get; set; }
    }
}
