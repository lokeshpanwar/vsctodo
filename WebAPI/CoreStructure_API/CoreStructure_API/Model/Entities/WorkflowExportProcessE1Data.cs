﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowExportProcessE1Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string Sappo { get; set; }
        public string LineItm { get; set; }
        public string Li { get; set; }
        public string StylNo { get; set; }
        public string Poqty { get; set; }
        public DateTime? DlvDate { get; set; }
        public string ShpQty { get; set; }
        public string HndovrDate { get; set; }
        public string ExCntryEtd { get; set; }
        public string DealayReason { get; set; }
        public string ShpMode { get; set; }
        public string Gweight { get; set; }
        public string SPort { get; set; }
        public string DischargePort { get; set; }
        public string PkgType { get; set; }
        public string CrtDimension { get; set; }
        public string TtlCrt { get; set; }
        public string TtlCbm { get; set; }
        public string TtlVolWeight { get; set; }
        public string GohVcpdetails { get; set; }
        public string GohTtlPacks { get; set; }
        public string Remarks { get; set; }
        public string NorMerchName { get; set; }
        public string MerchEmail { get; set; }
        public string ItmDesc { get; set; }
        public long? CombinedBpmid { get; set; }
        public long? Combinedthrdid { get; set; }
    }
}
