﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CourierTracking
    {
        public long Id { get; set; }
        public long? Wfid { get; set; }
        public long? ThreadId { get; set; }
        public string TrackingNo { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string Status { get; set; }
        public string Xmldata { get; set; }
    }
}
