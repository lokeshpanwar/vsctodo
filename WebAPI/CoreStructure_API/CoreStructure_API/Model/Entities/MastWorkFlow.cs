﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastWorkFlow
    {
        public int EntryId { get; set; }
        public int? WorkFlowId { get; set; }
        public string WorkFlowValue { get; set; }
        public int? MastWorkFlowLevelId { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
