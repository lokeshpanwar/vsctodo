﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class Dmsprocess
    {
        public Dmsprocess()
        {
            DmsprocessApprovers = new HashSet<DmsprocessApprovers>();
            DmsprocessCheckers = new HashSet<DmsprocessCheckers>();
        }

        public long DmsproceesId { get; set; }
        public long? Maker { get; set; }
        public long? DocId { get; set; }
        public double? ProcessValue { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? ModifiedBy { get; set; }
        public bool? Status { get; set; }
        public bool? IsActive { get; set; }
        public string PageUrl { get; set; }

        public EmployeeTable MakerNavigation { get; set; }
        public ICollection<DmsprocessApprovers> DmsprocessApprovers { get; set; }
        public ICollection<DmsprocessCheckers> DmsprocessCheckers { get; set; }
    }
}
