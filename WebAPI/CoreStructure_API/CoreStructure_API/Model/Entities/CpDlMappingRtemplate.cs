﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpDlMappingRtemplate
    {
        public long Slno { get; set; }
        public long? CpSimulationNo { get; set; }
        public long? Dlid { get; set; }
        public long? Msid { get; set; }
        public long? Bpmid { get; set; }
        public string Tbd { get; set; }
        public long? Tbac { get; set; }
        public string Intnal { get; set; }
        public string Etnal { get; set; }
        public string Prity { get; set; }
        public string Nod { get; set; }
        public DateTime? Sdate { get; set; }
        public DateTime? Ndate { get; set; }
        public string Bom { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
