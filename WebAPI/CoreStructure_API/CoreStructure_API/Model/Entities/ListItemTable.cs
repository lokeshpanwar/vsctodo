﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ListItemTable
    {
        public long Slno { get; set; }
        public long ListId { get; set; }
        public string ListItemType { get; set; }
        public string ListItemValue { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public bool? IsMulti { get; set; }
        public int? IsHorizAllign { get; set; }
        public bool? IsMandatory { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public ListTable List { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
    }
}
