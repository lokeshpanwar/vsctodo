﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowDocTables
    {
        public long Slno { get; set; }
        public long? ThId { get; set; }
        public long? WfId { get; set; }
        public string StgNme { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public WorkflowTable Wf { get; set; }
    }
}
