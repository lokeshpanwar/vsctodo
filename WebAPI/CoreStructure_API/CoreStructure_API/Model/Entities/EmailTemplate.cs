﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class EmailTemplate
    {
        public int EmailId { get; set; }
        public long? SubCompId { get; set; }
        public string TempName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public long? UpdatedBy { get; set; }
        public string Smsmsg { get; set; }
        public string PushMsg { get; set; }
    }
}
