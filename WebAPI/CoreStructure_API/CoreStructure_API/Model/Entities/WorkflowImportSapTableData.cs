﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowImportSapTableData
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string SapPo { get; set; }
        public string Litm { get; set; }
        public string SapItmCode { get; set; }
        public string SapItmDesc { get; set; }
        public string PoQty { get; set; }
        public string DelQty { get; set; }
        public string Balance { get; set; }
        public string ShipQty { get; set; }
        public string UnitPrice { get; set; }
        public string Value { get; set; }
        public string BatchNo { get; set; }
        public string RollNo { get; set; }
        public string RecvdQty { get; set; }
        public string TolU { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsDeleted { get; set; }
        public string Tin { get; set; }
    }
}
