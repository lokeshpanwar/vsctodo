﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocRecordedItemsUpdate
    {
        public long Slno { get; set; }
        public long? DocRcId { get; set; }
        public long? UserId { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string OldValue { get; set; }

        public DocRecordedItems DocRc { get; set; }
        public EmployeeTable User { get; set; }
    }
}
