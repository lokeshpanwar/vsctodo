﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentComentTable
    {
        public long Slno { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }
        public string VersionId { get; set; }
        public string Comments { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }

        public EmployeeTable User { get; set; }
    }
}
