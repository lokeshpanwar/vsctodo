﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowImportProcessI2Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string InvRef { get; set; }
        public string ShiprNm { get; set; }
        public string ApplicantNm { get; set; }
        public string ConsigneeNm { get; set; }
        public string InvNo { get; set; }
        public string InvDate { get; set; }
        public string VesselNm { get; set; }
        public string VsEtd { get; set; }
        public string VsEta { get; set; }
        public string NoOfPkgs { get; set; }
        public string Volm { get; set; }
        public string Gweight { get; set; }
        public string ShiprTrm { get; set; }
        public string PayTrm { get; set; }
        public string Blawb { get; set; }
        public string MerchName { get; set; }
        public string Pocurncy { get; set; }
        public string TtlGv { get; set; }
        public string DebNote { get; set; }
        public string SupCrdNote { get; set; }
        public string HandlSample { get; set; }
        public string Othr { get; set; }
        public string NetPay { get; set; }
        public string Coo { get; set; }
        public string CntnNo { get; set; }
        public string Pol { get; set; }
        public string Pod { get; set; }
        public string FormE { get; set; }
        public string TtlNetWght { get; set; }
        public string Lco { get; set; }
        public string Lcn { get; set; }
        public string Notify { get; set; }
        public string ShipMarks { get; set; }
        public bool? Nfe { get; set; }
        public string ProformaInvNo { get; set; }
        public string DespatchNo { get; set; }
        public string DelvDate { get; set; }
    }
}
