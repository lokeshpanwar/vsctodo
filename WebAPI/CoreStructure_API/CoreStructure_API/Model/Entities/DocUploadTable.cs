﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocUploadTable
    {
        public long Slno { get; set; }
        public long? DocId { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public long? DocClassId { get; set; }
        public long? DocTypeId { get; set; }
        public long? DocumentId { get; set; }
        public int? DocVersion { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string VersionId { get; set; }
        public string FileSize { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? UploadedOn { get; set; }
        public long? UploadedBy { get; set; }
        public bool? CheckoutStatus { get; set; }
        public long? CheckoutBy { get; set; }
        public DateTime? CheckoutOn { get; set; }
        public bool? CheckoutRight { get; set; }
        public bool? CheckoutAvailable { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public bool? IsCopied { get; set; }
        public long? CopiedBy { get; set; }
        public DateTime? CopiedOn { get; set; }
        public string CopiedTo { get; set; }
        public bool? IsMoved { get; set; }
        public DateTime? MovedOn { get; set; }
        public long? MovedBy { get; set; }
        public string MovedTo { get; set; }
        public string Comments { get; set; }
        public long? Sensitivity { get; set; }

        public EmployeeTable CheckoutByNavigation { get; set; }
        public EmployeeTable CopiedByNavigation { get; set; }
        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DocUploadMaster Doc { get; set; }
        public DocumentClassTable DocClass { get; set; }
        public DomainTable Domain { get; set; }
        public EmployeeTable MovedByNavigation { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public EmployeeTable UploadedByNavigation { get; set; }
    }
}
