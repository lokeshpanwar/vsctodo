﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MessageTable
    {
        public long Slno { get; set; }
        public long? AdminId { get; set; }
        public long? FromUser { get; set; }
        public long? ToUser { get; set; }
        public string ToEmail { get; set; }
        public string FileLink { get; set; }
        public long? DocId { get; set; }
        public DateTime? Date { get; set; }
        public bool? Status { get; set; }
        public string Section { get; set; }
        public string Message { get; set; }
        public int? Exparein { get; set; }
        public bool? IsReaded { get; set; }

        public EmployeeTable Admin { get; set; }
    }
}
