﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowImportProcessI3Data
    {
        public long Slno { get; set; }
        public long? Bpmid { get; set; }
        public long? ThrdId { get; set; }
        public long? StgId { get; set; }
        public string RmInHouse { get; set; }
        public string SuplrInNo { get; set; }
        public string SuplrNm { get; set; }
        public string GdsInHouseDate { get; set; }
    }
}
