﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WorkflowProcessDocs
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public long? DocClassId { get; set; }
        public long? Wfid { get; set; }
        public long? ThrdId { get; set; }
        public string StageName { get; set; }
        public long? StageOrder { get; set; }
        public string Status { get; set; }
        public long? UploadedBy { get; set; }
        public DateTime? UploadedOn { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string Cmt { get; set; }
        public string VerId { get; set; }
        public long? CombinedBpmid { get; set; }
        public long? Combinedthrdid { get; set; }
        public bool? IsDeleted { get; set; }

        public DocumentClassTable DocClass { get; set; }
        public DomainTable Domain { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public EmployeeTable UploadedByNavigation { get; set; }
        public WorkflowTable Wf { get; set; }
    }
}
