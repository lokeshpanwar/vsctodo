﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class WPriorityTable
    {
        public long Slno { get; set; }
        public long? Scoy { get; set; }
        public long? Fdom { get; set; }
        public string PriName { get; set; }
        public string PriDesc { get; set; }
        public string PriPer { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
    }
}
