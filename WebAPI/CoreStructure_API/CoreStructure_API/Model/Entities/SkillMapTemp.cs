﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class SkillMapTemp
    {
        public long Entryid { get; set; }
        public string SkillName { get; set; }
        public string EmpName { get; set; }
        public double? Rating { get; set; }
        public string Creationuser { get; set; }
    }
}
