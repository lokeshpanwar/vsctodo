﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class GroupUser
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public long? GroupId { get; set; }
    }
}
