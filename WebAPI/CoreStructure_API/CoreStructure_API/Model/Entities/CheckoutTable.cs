﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CheckoutTable
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public string VersionId { get; set; }
        public string DocumentClass { get; set; }
        public long? UserId { get; set; }
        public DateTime? CheckOutdateTime { get; set; }

        public DomainTable Domain { get; set; }
    }
}
