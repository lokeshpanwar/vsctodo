﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpMileData
    {
        public long Slno { get; set; }
        public long? SubCoyId { get; set; }
        public long? DomainId { get; set; }
        public string Mcode { get; set; }
        public string Mname { get; set; }
        public string Mdesc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
    //public class CpMileData_Page
    //{
    //    public List<CpMileData> CpMileData_Page_List { get; set; }

    //    public int TotalRows { get; set; }
    //}
}
