﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class GroupDomain
    {
        public long Id { get; set; }
        public long? GroupId { get; set; }
        public long? CompanyId { get; set; }
        public long? DomainId { get; set; }
    }
}
