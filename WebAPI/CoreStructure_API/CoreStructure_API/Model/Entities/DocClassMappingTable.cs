﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocClassMappingTable
    {
        public long Slno { get; set; }
        public long? DocumentClassId { get; set; }
        public long? UserId { get; set; }
        public long? RecordFieldId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DocumentClassTable DocumentClass { get; set; }
        public ListTable RecordField { get; set; }
        public EmployeeTable User { get; set; }
    }
}
