﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastSector
    {
        public int MastSectorId { get; set; }
        public string MastSectorName { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
