﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class EventTable
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public string EventTitle { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EventDetail { get; set; }
        public long? EventDomain { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public DomainTable EventDomainNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public SubCompanyTable SubComp { get; set; }
    }
}
