﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ChecklistTable
    {
        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public long? DomainId { get; set; }
        public long? ChecklistId { get; set; }
        public string ChecklistName { get; set; }
        public string ChecklistItems { get; set; }
        public string ChecklistItemcount { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool? IsMaped { get; set; }

        public DomainTable Domain { get; set; }
        public SubCompanyTable SubComp { get; set; }
    }
}
