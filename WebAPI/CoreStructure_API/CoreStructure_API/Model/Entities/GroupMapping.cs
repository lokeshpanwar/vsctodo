﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class GroupMapping
    {
        public int Id { get; set; }
        public int? ApgroupName { get; set; }
        public int? AcgroupName { get; set; }
        public string AuditscopName { get; set; }
        public string AuditscopCompanyName { get; set; }
        public int? IsDelete { get; set; }
        public int? IsActive { get; set; }
        public DateTime? Createon { get; set; }
        public DateTime? Modifyon { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
