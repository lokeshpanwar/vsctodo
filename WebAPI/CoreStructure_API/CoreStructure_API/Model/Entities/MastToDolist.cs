﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastToDolist
    {
        public long TdlcheckId { get; set; }
        public long Tdlid { get; set; }
        public string Tdldescription { get; set; }
        public long Tdlorder { get; set; }
        public bool? Isactive { get; set; }
        public bool? Isdeleted { get; set; }
        public DateTime? Creationdate { get; set; }
        public DateTime? Modifactiondate { get; set; }
        public string Creationuser { get; set; }
        public string Modifactionuser { get; set; }
    }
}
