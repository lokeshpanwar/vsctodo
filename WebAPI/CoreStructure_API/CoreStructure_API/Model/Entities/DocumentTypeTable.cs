﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocumentTypeTable
    {
        public DocumentTypeTable()
        {
            DocumentSetDocTypes = new HashSet<DocumentSetDocTypes>();
            DocumentTypeDocClassMappingTable = new HashSet<DocumentTypeDocClassMappingTable>();
        }

        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? SubCompId { get; set; }
        public string DocumentTypeName { get; set; }
        public string DocumenTypeDesc { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }

        public EmployeeTable CreatedByNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public ParentCompanyTable ParentComp { get; set; }
        public SubCompanyTable SubComp { get; set; }
        public ICollection<DocumentSetDocTypes> DocumentSetDocTypes { get; set; }
        public ICollection<DocumentTypeDocClassMappingTable> DocumentTypeDocClassMappingTable { get; set; }
    }
}
