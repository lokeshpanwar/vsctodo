﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class TblModulesForm
    {
        public int Id { get; set; }
        public int? ModuleId { get; set; }
        public string FormName { get; set; }
        public string ModuleName { get; set; }
        public string DisplayName { get; set; }
        public string GroupName { get; set; }
    }
}
