﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastBuckting
    {
        public int MastBucktingId { get; set; }
        public string MastBucktingValue { get; set; }
        public string PriorityId { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
