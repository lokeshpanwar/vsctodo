﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class UserDelegationTable
    {
        public long Slno { get; set; }
        public long? ParentCoy { get; set; }
        public long? SubCoy { get; set; }
        public long? Fd { get; set; }
        public long? DelegatedBy { get; set; }
        public string DelFrom { get; set; }
        public string DelTo { get; set; }
        public long? DelegatedTo { get; set; }
        public string Comment { get; set; }
        public bool? IsEnabled { get; set; }
    }
}
