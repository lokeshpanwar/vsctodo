﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastTaskStatus
    {
        public int TaskStatusId { get; set; }
        public string TaskStatusName { get; set; }
        public bool? Isdeleted { get; set; }
    }
}
