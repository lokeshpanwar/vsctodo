﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class GroupCompany
    {
        public long Id { get; set; }
        public long? CompanyId { get; set; }
        public long? GroupId { get; set; }
    }
}
