﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DiscussTable
    {
        public DiscussTable()
        {
            DiscussVote = new HashSet<DiscussVote>();
            DiscussionMessageTable = new HashSet<DiscussionMessageTable>();
        }

        public long Slno { get; set; }
        public long? SubCompId { get; set; }
        public string DiscussionTitle { get; set; }
        public string DiscussionDetail { get; set; }
        public long? DiscussionDomain { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }
        public string DisWithUsers { get; set; }
        public long? ParentId { get; set; }
        public int? Interval { get; set; }
        public bool? IsClosed { get; set; }
        public int? Up { get; set; }
        public int? Down { get; set; }

        public SubCompanyTable SubComp { get; set; }
        public ICollection<DiscussVote> DiscussVote { get; set; }
        public ICollection<DiscussionMessageTable> DiscussionMessageTable { get; set; }
    }
}
