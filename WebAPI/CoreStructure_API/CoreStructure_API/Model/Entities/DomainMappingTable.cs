﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DomainMappingTable
    {
        public long Slno { get; set; }
        public long? ParentCompId { get; set; }
        public long? MapedCompId { get; set; }
        public string MapedCompName { get; set; }
        public string MapedDomain { get; set; }
        public long? MapedDomainId { get; set; }
        public DateTime? MapedOn { get; set; }
        public long? MapedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public long? DeletedBy { get; set; }

        public EmployeeTable DeletedByNavigation { get; set; }
        public EmployeeTable MapedByNavigation { get; set; }
        public SubCompanyTable MapedComp { get; set; }
        public DomainTable MapedDomainNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public ParentCompanyTable ParentComp { get; set; }
    }
}
