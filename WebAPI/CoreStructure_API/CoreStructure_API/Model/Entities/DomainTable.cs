﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DomainTable
    {
        public DomainTable()
        {
            ChecklistTable = new HashSet<ChecklistTable>();
            CheckoutTable = new HashSet<CheckoutTable>();
            DocUploadTable = new HashSet<DocUploadTable>();
            DocumentClassTable = new HashSet<DocumentClassTable>();
            DocumentSetMaster = new HashSet<DocumentSetMaster>();
            DomainMappingTable = new HashSet<DomainMappingTable>();
            EventTable = new HashSet<EventTable>();
            FormTable = new HashSet<FormTable>();
            LoginTableCurrentDomainNavigation = new HashSet<LoginTable>();
            LoginTableDomain = new HashSet<LoginTable>();
            LoginTablePreviousDomainNavigation = new HashSet<LoginTable>();
            UserDocClassMappingTable = new HashSet<UserDocClassMappingTable>();
            UserDocTypeMappingTable = new HashSet<UserDocTypeMappingTable>();
            UserDocumentMappingTable = new HashSet<UserDocumentMappingTable>();
            WidgetSettings = new HashSet<WidgetSettings>();
            WorkflowProcessDocs = new HashSet<WorkflowProcessDocs>();
        }

        public long Slno { get; set; }
        public string DomainName { get; set; }
        public string DomainDescription { get; set; }
        public long? CretedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string CenterCode { get; set; }
        public long? CreatedBy { get; set; }

        public EmployeeTable CretedByNavigation { get; set; }
        public EmployeeTable DeletedByNavigation { get; set; }
        public EmployeeTable ModifiedByNavigation { get; set; }
        public ICollection<ChecklistTable> ChecklistTable { get; set; }
        public ICollection<CheckoutTable> CheckoutTable { get; set; }
        public ICollection<DocUploadTable> DocUploadTable { get; set; }
        public ICollection<DocumentClassTable> DocumentClassTable { get; set; }
        public ICollection<DocumentSetMaster> DocumentSetMaster { get; set; }
        public ICollection<DomainMappingTable> DomainMappingTable { get; set; }
        public ICollection<EventTable> EventTable { get; set; }
        public ICollection<FormTable> FormTable { get; set; }
        public ICollection<LoginTable> LoginTableCurrentDomainNavigation { get; set; }
        public ICollection<LoginTable> LoginTableDomain { get; set; }
        public ICollection<LoginTable> LoginTablePreviousDomainNavigation { get; set; }
        public ICollection<UserDocClassMappingTable> UserDocClassMappingTable { get; set; }
        public ICollection<UserDocTypeMappingTable> UserDocTypeMappingTable { get; set; }
        public ICollection<UserDocumentMappingTable> UserDocumentMappingTable { get; set; }
        public ICollection<WidgetSettings> WidgetSettings { get; set; }
        public ICollection<WorkflowProcessDocs> WorkflowProcessDocs { get; set; }
    }
}
