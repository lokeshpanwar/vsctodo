﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class Clientmaster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Details { get; set; }
        public int? IsDelete { get; set; }
        public int? IsActive { get; set; }
        public DateTime? Createon { get; set; }
        public DateTime? Modifyon { get; set; }
        public int? Updatedby { get; set; }
    }
}
