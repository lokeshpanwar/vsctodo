﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class AuditMaster
    {
        public int Id { get; set; }
        public string AuditCode { get; set; }
        public string AuditDiscription { get; set; }
        public string Frequency { get; set; }
        public DateTime? Createon { get; set; }
        public DateTime? Modifyon { get; set; }
        public int? IsDelete { get; set; }
        public int? IsActive { get; set; }
    }
}
