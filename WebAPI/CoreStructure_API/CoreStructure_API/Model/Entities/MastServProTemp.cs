﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class MastServProTemp
    {
        public long EntryId { get; set; }
        public string SerProductname { get; set; }
        public string SerDescription { get; set; }
        public string MastProSerTypeName { get; set; }
        public string Creationuser { get; set; }
    }
}
