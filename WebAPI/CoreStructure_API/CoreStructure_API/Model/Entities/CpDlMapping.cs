﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class CpDlMapping
    {
        public long Slno { get; set; }
        public long? Dlid { get; set; }
        public long? Msid { get; set; }
        public long? Bpmid { get; set; }
        public string Tbd { get; set; }
        public long? Tbac { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
