﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class SubTaskFiles
    {
        public long EntryId { get; set; }
        public long? SubTaskId { get; set; }
        public string Taskfile { get; set; }
        public DateTime? Uploaddate { get; set; }
        public string Creationuser { get; set; }
    }
}
