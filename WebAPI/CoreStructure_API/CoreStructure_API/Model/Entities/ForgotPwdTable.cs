﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class ForgotPwdTable
    {
        public long Slno { get; set; }
        public string EmpId { get; set; }
        public string UserId { get; set; }
        public string ResetPasswordLink { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ExpiryPeriod { get; set; }
        public bool? UsedLink { get; set; }
        public DateTime? UsedOn { get; set; }
        public string ClientIp { get; set; }
        public Guid? Uniqueid { get; set; }
    }
}
