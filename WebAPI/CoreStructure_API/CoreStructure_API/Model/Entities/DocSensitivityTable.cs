﻿using System;
using System.Collections.Generic;

namespace CoreStructure_API.Model.Entities
{
    public partial class DocSensitivityTable
    {
        public long Slno { get; set; }
        public string SubCompId { get; set; }
        public string SensiName { get; set; }
        public string SensiCode { get; set; }
        public string SensiDescrip { get; set; }
        public string SensiType { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
        public int? SensiPercent { get; set; }
    }
}
