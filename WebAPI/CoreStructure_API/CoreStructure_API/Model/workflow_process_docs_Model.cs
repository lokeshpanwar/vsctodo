﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class workflow_process_docs_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long Sub_CompID { get; set; }


        public long DomainID { get; set; }


        public long Doc_Class_ID { get; set; }


        public long WFID { get; set; }


        public long ThrdID { get; set; }


        [MaxLength(50)]
        public string Stage_name { get; set; }


        public long Stage_order { get; set; }


        [MaxLength(50)]
        public string status { get; set; }


        public long UploadedBy { get; set; }


        public DateTime UploadedOn { get; set; }


        [MaxLength(200)]
        public string FileName { get; set; }


        public string FilePath { get; set; }


        [MaxLength(50)]
        public string FileSize { get; set; }


        public string cmt { get; set; }


        public string verID { get; set; }


    }


    public class workflow_thread_tracker_Model
    {
        public long SLNO { get; set; }
        public string workFlowName { get; set; }
        public string killed { get; set; }
        public long TotalProcess { get; set; }
        public long wip_lag { get; set; }
        public long wip_lead { get; set; }
        public long open_lag { get; set; }
        public long open_lead { get; set; }
        public long hold { get; set; }
        public long close_lag { get; set; }
        public long close_lead { get; set; }
    }
}
