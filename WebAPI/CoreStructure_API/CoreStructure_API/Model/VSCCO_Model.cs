﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class VSCCO_Model
    {
        public int clientid { get; set; }

        public string clientname { get; set; }

        public int filetype { get; set; }
    }


    public class VSCActivity
    {
        public int activityid { get; set; }

        public string Activitynmae { get; set; }


    }

    public class VSCSubactivity
    {
        public int activityid { get; set; }

        public int subactivityid { get; set; }

        public string subactivityname { get; set; }

    }

    public class VSCUserData
    {
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }


    }

    public class VSCUserData_Table
    {
        public VSCUserData results { get; set; }
    }


}
