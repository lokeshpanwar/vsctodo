﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
namespace CoreStructure_API.Model
{
    public class AdminDashboard_Model
    {
       
        public int TotalSubcompany { get; set; }

       
        public int TotalDomain { get; set; }

      
        public int TotalRole { get; set; }

     
        public int TotalUser { get; set; }

      
        public int TotalDocument { get; set; }

        public List<DocumentClassAdminDash_Model> DocumentClassAdminDash_Model_List { get; set; }
        public List<CheckList_Model> CheckList_Model_List { get; set; }

        public List<Event_Model> Event_Model_List { get; set; }

        public List<Emp_CompanyWise_Model> Emp_CompanyWise_Model_List { get; set; }

        public List<Emp_DepartmentWise_Model> Emp_DepartmentWise_Model_List { get; set; }

        public List<RecentlyViewDoc_Model> RecentlyViewDoc_Model_List { get; set; }
    }

    public class DocumentClassAdminDash_Model
    {
        public string DocumentClassName { get; set; }
        public string DocumentClassDesc { get; set; }
        public int SLNO { get; set; }
    }

    public class CheckList_Model
    {
        public string Checklist_Name { get; set; }
        
    }

    public class Event_Model
    {
        public string EventTitle { get; set; }

    }
    public class Emp_CompanyWise_Model
    {
        public string SubCompanyName { get; set; }

        public string TotalEmp { get; set; }

    }

    public class Emp_DepartmentWise_Model
    {
        public string Domain_Name { get; set; }

        public string TotalEmp { get; set; }

    }

    public class RecentlyViewDoc_Model
    {
        public string Operation_Performed { get; set; }

       

    }
}
