﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Group_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name ="PARENT COMPANY")]
        public long Parent_compID { get; set; }


        [Display(Name = "SUB COMPANY")]
        public long Sub_compID { get; set; }


        [MaxLength(100)]
        [Display(Name = "GROUP NAME")]
        [Required(ErrorMessage ="Group Name can't be blank!")]
        public string GroupName { get; set; }


        [MaxLength(250)]
        [Display(Name = "GROUP DESCRIPTION")]
        public string GroupDescription { get; set; }


        [MaxLength(100)]
        [Display(Name = "GROUP EMAIL")]
        [Required(ErrorMessage = "Group Email can't be blank!")]
        public string GroupEmail { get; set; }


        public DateTime CreatedDate { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedDate { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


    }



    public class Group_table_page_Model
    {
        public List<Group_table_Model> Group_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }


    public class Group_Company_Model
    {
        public long ID { get; set; }

        public long GroupID { get; set; }

        public List<Group_Company_Model_Mapping> Group_Company_Model_Mapping_List { get; set; }
        public List<Group_Domain_Model_Mapping> Group_Domain_Model_Mapping_List { get; set; }
        public List<GroupUser_Model_Mapping> GroupUser_Model_Mapping_List { get; set; }
        public List<GroupUser_Model_Mapping> GroupVendor_Model_Mapping_List { get; set; }

        public long CompanyID { get; set; }

        public long CompanyIDForUser { get; set; }
        public long FunctionalDomainID { get; set; }

        public long CompanyIDForVendor { get; set; }

    }
    public class Group_Company_Model_Mapping
    {
        public long ID { get; set; }

        public long CompanyID { get; set; }

        public string CompanyName { get; set; }

        public bool IsSelected { get; set; } // Used in Domain Mapping
    }
    public class Group_Domain_Model_Mapping
    {
        public long ID { get; set; }
        public long GroupID { get; set; }
        //public long CompanyID { get; set; }
        public long DomainID { get; set; }
        public string DomainName { get; set; }
        public bool IsSelected { get; set; } // Used in Domain Mapping
    }
    public class GroupUser_Model_Mapping
    {
        public long ID { get; set; }

        public long User_ID { get; set; }

        public string UserName { get; set; }

        public long GroupID { get; set; }

        public bool IsSelected { get; set; }
    }

}
