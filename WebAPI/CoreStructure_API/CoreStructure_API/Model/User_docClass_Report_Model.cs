﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class User_docClass_Report_Model
    {
        public long ClassId { get; set; }

        public string ClassName { get; set; }

        public string CreatedBy { get; set; }

        public string Status { get; set; }

        public string Version { get; set; }

        public string Type { get; set; }

        public List<UserDocType> UserDocTypeList { get; set; }

        public List<UserDoc> UserDocList { get; set; }

    }


    public class UserDocType
    {
        public long SLNO { get; set; }
        public string DocumentTypeName { get; set; }
        public int NoOfDocuments { get; set; }
        public int NoOfUsers { get; set; }
        public string Status { get; set; }
    }

    public class UserDoc
    {
        public long SLNO { get; set; }
        public string UserName { get; set; }
        public int NoOfDocumentsUploaded { get; set; }
        public string Status { get; set; }
    }

}
