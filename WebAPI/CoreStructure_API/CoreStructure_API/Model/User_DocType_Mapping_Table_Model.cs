﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class User_DocType_Mapping_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long Sub_CompID { get; set; }

        public long Domain_ID { get; set; }


        public long Document_ClassID { get; set; }


        public long Document_Type { get; set; }


        public long User_ID { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }

        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public long DeletedBy { get; set; }

        public DateTime DeletedOn { get; set; }

        public bool view_right { get; set; }

        public bool print_right { get; set; }

        public bool email_right { get; set; }

        public bool checkin_right { get; set; }

        public bool checkout_right { get; set; }

        public bool download_right { get; set; }

        public bool upload_right { get; set; }

        public bool selectall_right { get; set; }


        public string DCTypeName { get; set; }
        public string DCClassName { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }

    }
}
