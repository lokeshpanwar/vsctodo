﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Parent_Company_Table_Model
    {
        [Key]
        public long SLNO { get; set; }

        [Display(Name = "COMPANY NAME")]
        [Required(ErrorMessage = "Company Name can't be blank!")]
        [MaxLength(200)]
        public string Company_Name { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [Required(ErrorMessage = "KPID can't be blank!")]
        [MaxLength(50)]
        [Display(Name = "KPID")]
        public string KPID { get; set; }


        public long UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

    }


    public class Parent_Company_Table_page_Model
    {
        public List<Parent_Company_Table_Model> Parent_Company_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
