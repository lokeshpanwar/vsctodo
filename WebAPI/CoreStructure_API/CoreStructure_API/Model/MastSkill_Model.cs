﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class MastSkill_Model
    {
        [Key]
        public long entryid { get; set; }
        
        public long SkillID { get; set; }


        [Required(ErrorMessage = "SkillName can't be blank!")]
        [MaxLength(100)]
        public string SkillName { get; set; }

        [Required(ErrorMessage = "SkillDescritpion can't be blank!")]

        public string SkillDescritpion { get; set; }
   

        public DateTime creationdate { get; set; }


        public string creationuser { get; set; }


        public DateTime modifactiondate { get; set; }


        public string modifactionuser { get; set; }


        public bool isdeleted { get; set; }


       
    }

    public class MastSkill_page_Model
    {
        public List<MastSkill_Model> MastSkill_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
