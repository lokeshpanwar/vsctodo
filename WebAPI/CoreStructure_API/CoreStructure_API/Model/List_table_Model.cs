﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class List_table_Model
    {
        [Key]

        public long SLNO { get; set; }


        public long Sub_CompID { get; set; }


        public long DomainID { get; set; }


        [Required(ErrorMessage ="Record Field Name can't be blank!")]
        [MaxLength(100)]
        public string ListName { get; set; }

        [Required(ErrorMessage = "Record Field Description can't be blank!")]
        [MaxLength(1000)]
        public string ListDescription { get; set; }



        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }



        public long DeletedBy { get; set; }


    }


    public class List_table_page_Model
    {
        public List<List_table_Model> List_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
