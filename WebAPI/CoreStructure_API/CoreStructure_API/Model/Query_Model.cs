﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Query_Model
    {
    }
    public class Query
    {
        public long QId { get; set; }
        public string ClientName { get; set; }
        public string QueryDate { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public string FatherName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string PolicyExpiryDate { get; set; }
        public decimal QuatationAmount { get; set; }
        public string Remarks { get; set; }
        public string ReferencePerson { get; set; }
        public string PendingRemark { get; set; }
        public int QStatus { get; set; }
        public string QStatusName { get; set; }

        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }

    public class UnderWrite
    {
        public long QId { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }
        public string UnderWriteDate { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CostCenter { get; set; }
        public string CommissionRate { get; set; }
        public string Premium { get; set; }
        public string CGST { get; set; }
        public string SGST { get; set; }
        public string PremiumPayable { get; set; }
        public string DiscountProvided { get; set; }
        public long CreatedBy { get; set; }
        public string fileName { get; set; }
        public string fileExt { get; set; }
        public int OfficeId { get; set; }
    }

    public class Policy
    {
        public long PId { get; set; }
        public long QId { get; set; }
        public long ClientId { get; set; }
        public string ClientName { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int ContactPersonId { get; set; }
        public string ContactPersonName { get; set; }
        public int PolicyHolderId { get; set; }
        public string PolicyHolderName { get; set; }
        public string UnderWriteDate { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SubjectInsured { get; set; }
        public string CostCenter { get; set; }
        public string CommissionRate { get; set; }
        public string Premium { get; set; }
        public string CGST { get; set; }
        public string SGST { get; set; }
        public string PremiumPayable { get; set; }
        public int PolicyStatus { get; set; }
        public string PolicyStatusName { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }
        public string VehicleNo { get; set; }
        public string OnDamage { get; set; }
        public string PersonalAccident { get; set; }
        public string ThirdParty { get; set; }
        public string IDV { get; set; }
        public string PolicyDate { get; set; }
        public string ExpiryDate { get; set; }
        public string PolicyNo { get; set; }
        public string MobileNo { get; set; }
        public string PreviousPolicyNo { get; set; }
        //public IFormFile UploadFile { get; set; }
        public string UploadFileName { get; set; }
        public int IsRenewal { get; set; }
        public int IsClaimed { get; set; }
        public string fileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }
        public string ContactNo { get; set; }
        public string ProductRate { get; set; }
        public string ODRate { get; set; }
        public string BrokerageAmount { get; set; }
        public string BrokerAmount { get; set; }
        public string GSTTotal { get; set; }
        public string totalBorkerageamt { get; set; }
        //public int TotalRows { get; set; }
        //public int CurrentPage { get; set; }
        //public int TotalPages { get; set; }
        //public int RowsPerPage { get; set; }
        //public string Search { get; set; }
        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;
        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }
        public string DiscountProvided { get; set; }
        public string inceptiondate { get; set; }
        public string UW_FileName { get; set; }

    }
}
