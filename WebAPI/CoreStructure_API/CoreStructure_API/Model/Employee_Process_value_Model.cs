﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Employee_Process_value_Model
    {
        public long SLNO { get; set; }

        public long UserID { get; set; }

        public float MaxValue { get; set; }

        public float MinValue { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime ModifiedOn { get; set; }

        public long ModifiedBy { get; set; }

        public bool isApprover { get; set; }



        public string Employee_Name { get; set; }
        public string Employee_ID { get; set; }
        public string Employee_Company_Name { get; set; }
        public string Employee_Domain { get; set;  }
        public string Manager_Name { get; set; }
        public string Manager_ID { get; set; }
        public string Manager_Domain { get; set; }

    }


    public class Employee_Process_value_list_Model
    {
        public List<Employee_Process_value_Model> Employee_Process_value_list { get; set; }


        public long Sub_Company { get; set; }


        public long Domain_ID { get; set; }

        public long CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

    }
}
