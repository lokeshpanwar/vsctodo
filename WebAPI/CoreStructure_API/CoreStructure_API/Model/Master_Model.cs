﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    #region Users
    public class Master_Model
    {
    }
    public class States
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
    }
    public class Cities
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
    public class Users
    {
        public long UserId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string DOB { get; set; }
        public string DOJ { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string Town { get; set; }
        public string EmergencyContactNo { get; set; }
        public string NameRelation { get; set; }
        public string POSCode { get; set; }
        public long CreatedBy { get; set; }
        public string fileName { get; set; }
        public string fileExt { get; set; }
        public byte[] fileData { get; set; }
        public String fileDataInString { get; set; }
        public string photo { get; set; }
    }

    #endregion

    #region Groups
    public class Group
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }


        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }
    public class Group_Table
    {
        public List<Group> results { get; set; }
    }
    #endregion

    #region Product

    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompany { get; set; }

        public string ODRate { get; set; }
        public string Rate { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int RowsPerPage { get; set; }
        public string Search { get; set; }
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;
        public string SortCol { get; set; }
        public string SortOrder { get; set; }

        public decimal totalrate { get; set; }

    }
    #endregion

    #region IssuingOffice
    public class IssuingOffice
    {
        public int IssuingOfficeId { get; set; }
        public string IssuingOfficeName { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string ShortName { get; set; }
        public string ManagerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string MobileNo { get; set; }
        public string EmailID { get; set; }
        public string Code { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
        public string SalesManagerName { get; set; }
        public string SMContactNo { get; set; }
        //public int TotalRows { get; set; }
        //public int CurrentPage { get; set; }
        //public int TotalPages { get; set; }
        //public int RowsPerPage { get; set; }
        //public string Search { get; set; }
        //public bool ShowPrevious => CurrentPage > 1;
        //public bool ShowNext => CurrentPage < TotalPages;
        //public bool ShowFirst => CurrentPage != 1;
        //public bool ShowLast => CurrentPage != TotalPages;
        //public string SortCol { get; set; }
        //public string SortOrder { get; set; }
    }
    #endregion


    #region Category
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
        public int TotalRows { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int RowsPerPage { get; set; }
        public string Search { get; set; }
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;
        public string SortCol { get; set; }
        public string SortOrder { get; set; }

    }
    #endregion

    #region ContactPerson
    public class ContactPerson
    {
        public int ContactPersonId { get; set; }
        public string Name { get; set; }

        public string FatherName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }

        public string Address { get; set; }
        public string DOB { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CityId { get; set; }
        public string CityName { get; set; }
        public string NameRelation { get; set; }
        public long CreatedBy { get; set; }
        public int OfficeId { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }


        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }
    #endregion

    #region TP/Surveyor
    public class TP_Surveyor
    {
        public int TPS_Id { get; set; }
        public int Ins_Comp { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string TP_Name { get; set; }
        public string TP_Address { get; set; }
        public string TP_ContactNo { get; set; }
        public int OfficeId { get; set; }
        public int CreatedBy { get; set; }
    }
    #endregion

    #region Activity
    public class Activity
    {
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }
    #endregion
    #region SubActivity
    public class SubActivity
    {
        public int SubActivityId { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public string SubActivityName { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }
    #endregion

    #region Holiday
    public class Holiday
    {
        public int HolidayId { get; set; }
        public string HolidayName { get; set; }
        public string Date { get; set; }
        public int OfficeId { get; set; }
        public long CreatedBy { get; set; }
    }
 
    #endregion

}
