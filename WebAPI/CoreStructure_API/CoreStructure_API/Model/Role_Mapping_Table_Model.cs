﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Role_Mapping_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name ="SELECT ROLE")]
        [Required(ErrorMessage ="Role can't be blank!")]
        public long Role_ID { get; set; }


        [Display(Name = "SELECT USER")]
        [Required(ErrorMessage = "User can't be blank!")]
        public long User_ID { get; set; }



        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public string RoleName { get; set; }

        public string EmployeeName { get; set; }

    }




    public class Role_Mapping_Table_page_Model
    {
        public List<Role_Mapping_Table_Model> Role_Mapping_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
