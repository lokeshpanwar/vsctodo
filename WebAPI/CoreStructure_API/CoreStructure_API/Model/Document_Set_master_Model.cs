﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Document_Set_master_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name = "SELECT COMPANY")]
        [Required(ErrorMessage = "Company can't be blank!")]
        public long Sub_CompID { get; set; }


        [Display(Name = "SELECT DOMAIN")]
        [Required(ErrorMessage = "Domain can't be blank!")]
        public long DomainID { get; set; }


        [Display(Name ="DOCUMENT SET NAME")]
        [Required(ErrorMessage ="Document Set Name can't be blank!")]
        [MaxLength(500)]
        public string DocumentSetName { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsExpire { get; set; }


        public Nullable<DateTime> IsExpireDate { get; set; }


        public List<Document_Set_Doc_Class_Model> Document_Set_Doc_Class_Model_List { get; set; }

        public List<Document_Set_Doc_Types_Model> Document_Set_Doc_Types_Model_List { get; set; }

        public List<Document_Set_Files> Document_Set_Files_Class_List { get; set; }

        public string SubCompName { get; set; } // For Display Purpose
        public string DomainName { get; set; } // For Display Purpose

    }



    public class Document_Set_master_page_Model
    {
        public List<Document_Set_master_Model> Document_Set_master_Model_List { get; set; }

        public int TotalRows { get; set; }
    }


    public class Document_Set_Doc_Class_Model
    {
        [Key]
        public long ID { get; set; }

        
        public long Set_ID { get; set; }


        public long Document_ClassID { get; set; }

        public bool IsSelected { get; set; }


        //public List<Document_Set_Doc_Types_Model> Document_Set_Doc_Types_Model_List { get; set; }


        //public List<Document_Set_Files> Document_Set_Files_Class_List { get; set; }

        public string DocumentClassName { get; set; }

    }

    public class Document_Set_Doc_Types_Model
    {
        [Key]
        public long ID { get; set; }


        public long Set_ID { get; set; }


        public long Document_ClassID { get; set; }


        public long DocumentType_ID { get; set; }


        public bool IsSelected { get; set; }


        //public List<Document_Set_Files> Document_Set_Files_Type_List { get; set; }


        public string DocumentTypeName { get; set; }

    }

    public class Document_Set_Files
    {
        public long ID { get; set; }
        public long Set_ID { get; set; }
        public long Doc_ID { get; set; }

        public bool IsSelected { get; set; }
        public string DocumentName { get; set; }
    }


}
