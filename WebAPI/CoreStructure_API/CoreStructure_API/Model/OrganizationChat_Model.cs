﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class OrganizationChat_Model
    {
        public string Id { get; set; }

        public string Name { get; set; }
        public string ReportTo { get; set; }
        public string Title { get; set; }
       
    }
}
