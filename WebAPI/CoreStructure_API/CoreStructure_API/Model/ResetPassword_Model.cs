﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class ResetPassword_Model
    {
        public long SLNO { get; set; }
        //[MaxLength(20)]
        //[Display(Name = "CURRENT PASSWORD")]
        //[Required(ErrorMessage = "Current Password can't be blank!")]
        public string Current_Password { get; set; }



        //[MaxLength(20)]
        //[Display(Name = "NEW PASSWORD")]
        //[Required(ErrorMessage = "New Password can't be blank!")]

        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        //[RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string New_Password { get; set; }



        //[MaxLength(20)]
        //[Display(Name = "CONFRM PASSWORD")]
        //[Required(ErrorMessage = "Confirm Password can't be blank!")]
        public string Confirm_Password { get; set; }



        public long UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }


    }
}
