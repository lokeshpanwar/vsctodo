﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class Discuss_Model
    {

        [Key]
        public long SLNO { get; set; }


        public long Sub_CompID { get; set; }



        public string DiscussionTitle { get; set; }


       // [Required(ErrorMessage = "Discussion Detail can't be blank!")]
        //[MaxLength(100)]
        public string DiscussionDetail { get; set; }


        [Required(ErrorMessage = "Discussion Domain can't be blank!")]
        public long DiscussionDomain { get; set; }

        



        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


        public DateTime DeletedOn { get; set; }


        public string DisWithUsers { get; set; }

        public long ParentID { get; set; }

        public int interval { get; set; }

        public bool IsClosed { get; set; }

        public int Up { get; set; }

        public int Down { get; set; }

        public string EmployeeName { get; set; }
         public string Email { get; set; }
    }

    public class Discuss_page_Model
    {
        public List<Discuss_Model> Discuss_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

   
}
