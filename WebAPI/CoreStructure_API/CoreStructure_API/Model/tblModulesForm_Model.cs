﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class tblModulesForm_Model
    {
        [Key]
        public int ID { get; set; }


        public int ModuleID { get; set; }


        [MaxLength(300)]
        public string FormName { get; set; }


        [MaxLength(250)]
        public string ModuleName { get; set; }


        [MaxLength(250)]
        public string DisplayName { get; set; }


        [MaxLength(50)]
        public string GroupName { get; set; }


    }
}
