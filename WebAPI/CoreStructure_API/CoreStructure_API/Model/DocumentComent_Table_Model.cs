﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocumentComent_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long UserID { get; set; }


        public long DocumentID { get; set; }


        [MaxLength(50)]
        public string VersionID { get; set; }


        public string Comments { get; set; }

        [MaxLength(100)]
        public string Status { get; set; }


        public DateTime Date { get; set; }


    }
}
