﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class RecordField_DocClass_Mapping_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Required(ErrorMessage = "Document Class ID can't be blank!")]
        public long Document_ClassID { get; set; }


        [Required(ErrorMessage = "Record Field ID can't be blank!")]
        public long RecordField_ID { get; set; }


        public long CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }


        public long ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


        public DateTime DeletedOn { get; set; }



        public string DCClassName { get; set; }
        public string DCRecordFieldName { get; set; }
        public string DCDomainName { get; set; }

    }
    

    public class RecordField_DocClass_Mapping_Table_page_Model
    {
        public List<RecordField_DocClass_Mapping_Table_Model> RecordField_DocClass_Mapping_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
