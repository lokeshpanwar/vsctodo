﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Role_table_Model
    {
        [Key]
        public long SLNO { get; set; }



        public long Parent_compID { get; set; }


        [Required(ErrorMessage = "Select Company can't be blank!")]
        public long Sub_compID { get; set; }


        [Display(Name = "ROLE NAME")]
        [Required(ErrorMessage = "Role Name can't be blank!")]
        [MaxLength(100)]
        public string RoleName { get; set; }


        [Display(Name = "ROLE DESCRIPTION")]
        [Required(ErrorMessage = "Role Description can't be blank!")]
        [MaxLength(250)]
        public string RoleDescription { get; set; }


        [Display(Name = "ROLE EMAIL")]
        [MaxLength(100)]
        public string RoleEmail { get; set; }


        public DateTime CreatedDate { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public bool DeletedBy { get; set; }

    }


    public class Role_table_page_Model
    {
        public List<Role_table_Model> Role_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
