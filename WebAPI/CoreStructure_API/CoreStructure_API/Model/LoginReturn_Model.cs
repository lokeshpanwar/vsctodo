﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class LoginReturn_Model
    {
        public string Token { get; set; }

        public string Theme { get; set; }

        public string LoginStatus { get; set; }

        public long UserId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public string EmployeeName { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public int OfficeId { get; set; }

        public string fileName { get; set; }


        public long UserDomainId { get; set; }
        public long UserSubCompId { get; set; }
        public long UserParentCompId { get; set; }
        public string UserType { get; set; }
        public string UserImage { get; set; }
        public long UserRoleId { get; set; }
        public string UserRole { get; set; }
        public List<Role_Modules_Permission_Model> Role_Modules_Permission_Model_List { get; set; }
        public List<RoleFormList> GroupNameList { get; set; }
        public string UserDivision { get; set; }
        public string EmpId { get; set; }
        public string POZone { get; set; }

        public string Name { get; set; }
        
        public int Vertical { get; set; }

        public int Role { get; set; }
    }

    public class OTP_Return_Value
    {
        public long UserId { get; set; }
        public string return_OTP { get; set; }
        public string MobileNo { get; set; }
        public string LoginStatus { get; set; }
        public string LoginStatusMessage { get; set; }
        public int IsSameDevice { get; set; }
    }
    public class RoleFormList
    {
        public string GroupName { get; set; }

        //public List<Role_Modules_Permission_Model> Role_Modules_Permission_Model_List { get; set; }
    }
}
