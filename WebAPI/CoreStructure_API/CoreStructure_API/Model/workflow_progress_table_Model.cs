﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class workflow_progress_table_Model
    {
        public long SLNO { get; set; }
        
        public long thrdID { get; set; }

        public long wfID { get; set; }
        
        public long stgID { get; set; }

        public string peopleType { get; set; }

        [MaxLength(20)]
        public string personID { get; set; }
        
        [MaxLength(500)]
        public string formID { get; set; }
        
        public string formData { get; set; }
        
        [MaxLength(500)]
        public string chkID { get; set; }
        
        public string chkData { get; set; }
        
        [MaxLength(50)]
        public string processStatus { get; set; }


        public string comments { get; set; }

        public bool complitionStatus { get; set; }
        
        public bool threadStatus { get; set; }

        public DateTime recv_date { get; set; }

        public DateTime start_date { get; set; }

        public DateTime end_date { get; set; }

        public DateTime submission_date { get; set; }

        public bool submissionStatus { get; set; }


        [MaxLength(200)]
        public string submittedBy { get; set; }

        public string communicateTo { get; set; }

        public string defaultFile { get; set; }
    }


}
