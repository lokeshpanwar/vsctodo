﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class LC_REQUEST_TABLE_Model
    {
        public long SLNO { get; set; }
        public long LC_REF_ID { get; set; }

        public string EBELN { get; set; }
        
        public string EBELP { get; set; }
        public string ETENR { get; set; }
        public string C_NAME1 { get; set; }
        public string VBELN { get; set; }
        public string POSNR { get; set; }
        public string BSTKD { get; set; }
        public string BEZEI { get; set; }
        public string LIFNR { get; set; }
        public string V_NAME1 { get; set; }
        public string MATNR { get; set; }
        public string TXZ01 { get; set; }
        public DateTime EINDT { get; set; }
        public string J_3ASIZE { get; set; }
        public string MENGE { get; set; }
        
        public string MEINS { get; set; }
        public string J3AX { get; set; }
        public string VNETPR { get; set; }
        public string T_AMOUNT { get; set; }
        public string WAERS { get; set; }
        public string SHP_MODE { get; set; }
        public string PORT_OF_ORG { get; set; }
        public string PORT_OF_DEST { get; set; }
        public string VTEXT { get; set; }
        public string INCO { get; set; }
        public string T_ZTERM { get; set; }
        public string UEBTO { get; set; }
        public string UNTTO { get; set; }
        public string EKORG { get; set; }
        public string PUR_ORG { get; set; }
        public string BUTXT { get; set; }
        public string KO_PRCTR { get; set; }
        public DateTime PO_R_DATE { get; set; }
        public string PO_R_TIME { get; set; }
        public string ELIKZ { get; set; }
        public string EMAIL { get; set; }
        public string V_ADD { get; set; }
        public string MTART { get; set; }
        public string LAND1 { get; set; }
        public string LANDX { get; set; }
        public string SAP_PO_VER { get; set; }
        public string HID_PO_VER { get; set; }
        public string PO_STATUS { get; set; }
        public string EMATN { get; set; }
        public string NETPR { get; set; }
        public string PEINH { get; set; }
        public string NETWR { get; set; }
        public string KUNNR { get; set; }
        public string NAM1 { get; set; }
        public string ZTERM { get; set; }
        public string TEXT1 { get; set; }
        public string INCO2 { get; set; }
        public string VBELP { get; set; }
        public string SPART { get; set; }
        public string TSPAT { get; set; }
        public string MVGR5 { get; set; }
        public string TVM5T { get; set; }
        public string EKGRP { get; set; }
        public string IEVER { get; set; }
        public string KZABE { get; set; }
        public string STABE { get; set; }
        public string KZGBE { get; set; }
        public string STGBE { get; set; }
        public string LADEL { get; set; }
        public string PRONU { get; set; }
        public string STAWN { get; set; }
        public string MSEHL { get; set; }
        public string J_3APQTY { get; set; }
        public string J_3ACASE { get; set; }
        public string J_3ACASET { get; set; }
        public string J_3APQUN { get; set; }
        public string DIV1 { get; set; }
        public string DIV2 { get; set; }
        public string DIV3 { get; set; }
        public string ITEM_CAT { get; set; }
        public string PO_TYPE { get; set; }
        public string Notify_Party { get; set; }
        public string Notify_party1 { get; set; }
        public string ZLSCH { get; set; }
        public string ZTAG1 { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public bool IsSelected { get; set; }
    }

    public class LC_Process
    {
        public long LC_REF_ID { get; set; }
        public string EBELN { get; set; }
        public string WAERS { get; set; }
        public string C_NAME1 { get; set; }
        public string T_AMOUNT { get; set; }
    }
    public class LC_Request_List_Model
    {
        public List<LC_REQUEST_TABLE_Model> LC_Request_Model { get; set; }

        public long CreatedBy { get; set; }
    }

    public class LC_RequestLC_Pagination
    {
        public List<LC_REQUEST_TABLE_Model> LC_REQUEST_TABLE_Model_List { get; set; }
        public List<LC_Process> LC_Process_List { get; set; }

        public int TotalRows { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages { get; set; }

        public int RowsPerPage { get; set; }

        public string Search { get; set; }

        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;
        public bool ShowFirst => CurrentPage != 1;
        public bool ShowLast => CurrentPage != TotalPages;

        public string SortCol { get; set; }
        public string SortOrder { get; set; }
    }
}
