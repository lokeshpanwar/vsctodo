﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocumentType_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long Parent_CompID { get; set; }


        public long Sub_CompID { get; set; }


        [Required(ErrorMessage = "Document Type Name can't be blank!")]
        [MaxLength(200)]
        public string DocumentType_Name { get; set; }


        [Required(ErrorMessage = "Document Type Description can't be blank!")]
        [MaxLength(200)]
        public string DocumenType_desc { get; set; }
        

        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


    }


    public class DocumentType_Table_page_Model
    {
        public List<DocumentType_Table_Model> DocumentType_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
