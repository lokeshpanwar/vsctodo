﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class User_Delegation_Table_Model
    {
        [Key]
        public long SLNO { get; set; }


        [Display(Name = "PARENT COMPANY")]
        public long parentCoy { get; set; }


        [Display(Name = "SUB COMPANY")]
        public long subCoy { get; set; }


        [Display(Name = "FUNCTIONAL DOMAIN")]
        public long FD { get; set; }


        [Display(Name = "DELEGATED BY")]
        public long delegatedBy { get; set; }


        [Display(Name = "FROM")]
        public string del_from { get; set; }


        [Display(Name = "TO")]
        public string del_to { get; set; }


        [Display(Name = "DELEGATED TO")]
        public long delegatedTo { get; set; }


        [Display(Name = "COMMENT")]
        public string comment { get; set; }


        public bool IsEnabled { get; set; }


    }
}
