﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace CoreStructure_API.Model
{
    public class Discussion_Message_Model
    {
        [Key]

        public long SLNO { get; set; }

        public long Discussion_ID { get; set; }

        public string  Userid { get; set; }
        public string Message { get; set; }

        public DateTime CreatedOn { get; set; }

       
        public long CreatedBy { get; set; }

        public string EmployeeName { get; set; }

        public string Email { get; set; }

       
    }

    public class Discussion_Message_page_Model
    {
        public List<Discussion_Message_Model> Discussion_Message_Model_List { get; set; }

        
        public string Userid { get; set; }

        public long SLNO { get; set; }
        public int upvote { get; set; }
        public int downvote { get; set; }

        public string newtopic { get; set; }

        public List<Discuss_Model> Discuss_Model_List { get; set; }
    }
}
