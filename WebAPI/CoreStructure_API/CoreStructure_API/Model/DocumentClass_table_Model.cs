﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class DocumentClass_table_Model
    {
        [Key]
        public long SLNO { get; set; }


        public long SubCompanyID { get; set; }



        public long ClassDomain { get; set; }


        [Required(ErrorMessage = "Document Class Name can't be blank!")]
        [MaxLength(100)]
        public string DocumentClassName { get; set; }


        [Required(ErrorMessage = "Document Class Name can't be blank!")]
        public string DocumentClassDesc { get; set; }


        [Required(ErrorMessage = "Allowed Extension can't be blank!")]
        [MaxLength(100)]
        public string AllowedExtension { get; set; }


        public bool AccessControlStatus { get; set; }


        public bool VersionControlStatus { get; set; }


        [Required(ErrorMessage = "Document Access By can't be blank!")]
        [MaxLength(20)]
        public string DocumentAccessBy { get; set; }


        public bool Archieve_Status { get; set; }

        //[Required(ErrorMessage = "Document Type Id can't be blank!")]
        [MaxLength(100)]
        public string DocumentType_ID { get; set; }


        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public long DeletedBy { get; set; }


        public DateTime DeletedOn { get; set; }


        public bool print_right { get; set; }

        public bool email_right { get; set; }

        public bool checkin_right { get; set; }

        public bool checkout_right { get; set; }

        public bool download_right { get; set; }

        public bool upload_right { get; set; }

        public bool selectall_right { get; set; }



        public string DCCompanyName { get; set; }
        public string DCDomainName { get; set; }


        public bool view_right { get; set; }

    }



    public class DocumentClass_table_page_Model
    {
        public List<DocumentClass_table_Model> DocumentClass_table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }

}
