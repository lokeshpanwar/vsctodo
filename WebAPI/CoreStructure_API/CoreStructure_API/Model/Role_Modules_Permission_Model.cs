﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class Role_Modules_Permission_Model
    {
        [Key]
        public long ID { get; set; }


        
        public int ModuleID { get; set; }

        [Display(Name ="ROLE")]
        [Required(ErrorMessage ="Role can't be blank!")]
        public long RoleID { get; set; }


        [MaxLength(250)]
        [Display(Name = "FORM NAME")]
        public string FormName { get; set; }


        public bool IsRight { get; set; }


        public bool IsEdit { get; set; }


        public bool IsRead { get; set; }


        public bool IsWrite { get; set; }


        public bool IsView { get; set; }


        public bool IsDelete { get; set; }


        // To Display Purpose
        public string ModuleName { get; set; }
        public string DisplayName { get; set; }
        public string GroupName { get; set; }


        public bool IsSelected { get; set; }

    }


    public class Role_Modules_Permission_Display_Model
    {
        public long SubComp_ID { get; set; }
        public long Role_ID { get; set; }
        public List<Role_Modules_Permission_Model> Role_Modules_Permission_Model_List { get; set; }

        public long CreatedBy { get; set; }

    }
}
