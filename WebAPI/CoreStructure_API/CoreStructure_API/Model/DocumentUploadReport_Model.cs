﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CoreStructure_API.Model
{
    public class DocumentUploadReport_Model
    {

        public long UserId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public List<Log_Table_Model> Log_Table_Model_List { get; set; }


        public long SubCompanyId { get; set; }

        public long DomainId { get; set; }

        public long ClassDomain { get; set; }

        public string DocumentClassName { get; set; }
    }
}
