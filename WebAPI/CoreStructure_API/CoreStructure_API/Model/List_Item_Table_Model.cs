﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class List_Item_Table_Model
    {
        [Key]
        public long SLNO { get; set; }

        [Required(ErrorMessage = "List ID can't be blank!")]
        public long List_ID { get; set; }


        [Required(ErrorMessage = "Item Type can't be blank!")]
        [MaxLength(50)]
        public string List_ItemType { get; set; }


        [Required(ErrorMessage = "Item Value can't be blank!")]
        [MaxLength(100)]
        public string List_ItemValue { get; set; }


        public DateTime CreatedOn { get; set; }


        public long CreatedBy { get; set; }


        public DateTime ModifiedOn { get; set; }


        public long ModifiedBy { get; set; }


        public bool IsDeleted { get; set; }


        public DateTime DeletedOn { get; set; }


        public long DeletedBy { get; set; }


        public string ListName { get; set; }


        public string ListItemUserEnterValue { get; set; }


        public bool isMulti { get; set; }

        public bool isMandatory { get; set; }

        public bool IsHorizAllign { get; set; }

    }



    public class List_Item_Table_page_Model
    {
        public List<List_Item_Table_Model> List_Item_Table_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}