﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CoreStructure_API.Model
{
    public class MastServPro_Model
    {

        [Key]
        public long EntryId { get; set; }
       
        public long ProSerid { get; set; }


        [Required(ErrorMessage = "Service ProductName can't be blank!")]
        [MaxLength(100)]
        public string SerProductname { get; set; }

       

        public string serDescription { get; set; }

       
        public int MastProSerTypeID { get; set; }

        public DateTime creationdate { get; set; }


        public string creationuser { get; set; }


        public DateTime modifactiondate { get; set; }


        public string modifactionuser { get; set; }


        public bool isdeleted { get; set; }

        public string ServiceProductType { get; set; }
    }


    public class MastServPro_page_Model
    {
        public List<MastServPro_Model> MastServPro_Model_List { get; set; }

        public int TotalRows { get; set; }
    }
}
