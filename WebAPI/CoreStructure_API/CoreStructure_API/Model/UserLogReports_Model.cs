﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class UserLogReports_Model
    {

        public string EmployeeName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public List<Log_Table_Model> Log_Table_Model_List { get; set; }


        public string SubCompanyName { get; set; }

        public string Domain_Name { get; set; }
    }
}
