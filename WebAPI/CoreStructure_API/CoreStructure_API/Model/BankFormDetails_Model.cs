﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Model
{
    public class BankFormDetails_Model
    {
        public long bDetail_Id { get; set; }
        public long ref_Id { get; set; }
        public long BPMID { get; set; }
        public long StageId { get; set; }
        public long ThreadId { get; set; }
        public string BankName { get; set; }
        public string DateOfApplication { get; set; }
        public string ApplicantName { get; set; }
        public string ApplicantAddress { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAddress { get; set; }
        public string DCCurrency { get; set; }
        public string DCAmount { get; set; }
        public string DCAmountInWords { get; set; }
        public string DCAmount_Currency { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string PARTIAL_SHIPMENT { get; set; }
        public string AllowanceInQty { get; set; }
        public string TRANSHIPMENT { get; set; }
        public string DCTenor { get; set; }
        public string DCTenorDays { get; set; }
        public string AdvisingSwiftCode { get; set; }
        public string AdvisingBankName { get; set; }
        public string AdvisingBankAddress { get; set; }
        public int Presentation_Period { get; set; }
        public string SHIP_FROM_COUNTRY { get; set; }
        public string SHIP_FROM_PORT { get; set; }
        public string SHIP_TO_COUNTRY { get; set; }
        public string SHIP_TO_PORT { get; set; }
        public string LastShipmentDate { get; set; }
        public string AccountNumber { get; set; }
        public string ApplicantContactPerson { get; set; }
        public string ApplicantTel { get; set; }
        public string ImportAccountNo { get; set; }
        public string BeneficiaryContactPerson { get; set; }
        public string BenificiaryTel { get; set; }
        public string BenificiaryFax { get; set; }
        public string DCDispatchBy { get; set; }
        public string DCNumber { get; set; }
        public string Expiry_Place { get; set; }
        public string AllowanceInDCAmount { get; set; }
        public string Confirmation { get; set; }
        public string DCAvailableWith { get; set; }
        public string DraftRequired { get; set; }
        public string DCAvailableBy { get; set; }
        public string Transferable { get; set; }
        public string DescriptionOfGoods { get; set; }
        public string InsuranceCoveredBy { get; set; }
        public string Incoterms { get; set; }
        public string IncotermsOther { get; set; }
        public string SignedCommercialOriginals { get; set; }
        public string SignedCommercialCopies { get; set; }
        public string PackingListOriginals { get; set; }
        public string PackingListCopies { get; set; }
        public string ShipmentBySea { get; set; }
        public string ShipmentByAir { get; set; }
        public string CargoReceipt { get; set; }
        public string InsurancePolicy { get; set; }
        public string BeneficiaryCertificate { get; set; }
        public string BeneficiaryCertificateDays { get; set; }
        public string BeneficiaryCertificateType { get; set; }
        public string AdditionalDocuments { get; set; }
        public string TTReimbursement { get; set; }
        public string DCOpeningCommission { get; set; }
        public string IssuingBankOtherCharges { get; set; }
        public string CorrespondentBankCharges { get; set; }
        public string DCConfirmationCharges { get; set; }
        public string TransitInterestCharges { get; set; }
        public string DelayedReimbursement { get; set; }
        public string HKDBillCommission { get; set; }
        public string AccountNoCharges { get; set; }
        public string CurrencyAccount { get; set; }
        public string MasterCreditNumber { get; set; }
        public string MasterCreditNumberIssuedBy { get; set; }
        public string MasterDCBy { get; set; }
        public string CollateralCash { get; set; }
        public string CollateralAmount { get; set; }
        public string CollateralAccountNumber { get; set; }
        public string CollateralCurrency { get; set; }
        public string AdditionalInformation { get; set; }
        public string Inspection_SignedBY { get; set; }
    }

    public class BankAccountNumber
    {
        public string Bank { get; set; }
        public string Currency { get; set; }
        public string AccountNumber { get; set; }
    }
}
