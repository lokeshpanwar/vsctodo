﻿using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Global
{
    public class GlobalFunction
    {
        public static int getLoggedInUser(string AuthorizationToken)
        {
            int strUserId = 0;
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    strUserId = Convert.ToInt32(arrToken[0]);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUser");
            }
            return strUserId;
        }


        public static int getLoggedInUserRole(string AuthorizationToken)
        {
            int strRoleId = 0;
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    strRoleId = Convert.ToInt32(arrToken[1]);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUser");
            }
            return strRoleId;
        }


        public static string getImageURL()
        {
            //string url = Startup.ImageURL + "/Content/CustomImages/Employee";
            string url = Startup.ImageURL;
            return url;
        }


        public static string GetRandomNumber(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!@#$";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public static string CheckIfValidDomainFromEmail(string Email, long AdminId)
        {
            string result = "Invalid!";
            try
            {
                string domain = Email.Split('@').Last(); 
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select * From MailDomain where Domain=@Domain and IsActive=@IsDeleted ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Domain", domain);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    result = "Valid";
                }
                connection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, AdminId, "API", "GlobalFunction/CheckIfValidDomainFromEmail");
            }

            return result;
        }


        public static int getLoggedInUserSubCompanyId(string AuthorizationToken)
        {
            int strUserId = 0;
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    strUserId = Convert.ToInt32(arrToken[4]);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUserSubCompanyId");
            }
            return strUserId;
        }


        public static int getLoggedInUserDomainId(string AuthorizationToken)
        {
            int strUserId = 0;
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    strUserId = Convert.ToInt32(arrToken[5]);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUserDomainId");
            }
            return strUserId;
        }
        

        public static int getLoggedInUserParentCompanyId(string AuthorizationToken)
        {
            int strParentCompId = 0;
            try
            {
                long subcomId = 0;
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    subcomId = Convert.ToInt64(arrToken[4]);
                }



                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Parent_compID From Sub_Company_table where SLNO=@SLNO";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", subcomId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    strParentCompId = Convert.ToInt32(sdr["Parent_compID"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUserParentCompanyId");
            }
            return strParentCompId;
        }


        public static string getLoggedInUserIPAddress(string AuthorizationToken)
        {
            string IPAdd = "";
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    IPAdd = arrToken[0];
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUserIPAddress");
            }
            return IPAdd;
        }

        public static string getLoggedInUserRoleName(string AuthorizationToken)
        {
            string strRole = "";
            try
            {
                String Token = CryptorEngine.Decrypt(AuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    strRole = arrToken[3];
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "GlobalFunction/getLoggedInUserRoleName");
            }
            return strRole;
        }
    }
}
