﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Global
{
    public static class LogType
    {
        public static string Upload = "UPLOAD";
        public static string View = "VIEW";
        public static string Insert = "INSERT";
        public static string Update = "UPDATE";
        public static string Delete = "DELETE";
        public static string CheckIn = "CHECKED IN";
        public static string CheckOut = "CHECKED OUT";
        public static string CheckOutCancel = "CHECKED OUT CANCEL";
        public static string Enable = "ENABLE";
        public static string Disable = "DISABLE";
    }
}
