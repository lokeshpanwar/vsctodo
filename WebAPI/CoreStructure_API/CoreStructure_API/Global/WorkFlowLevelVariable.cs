﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Global
{
    public static class WorkFlowLevelVariable
    {
        public static string Level2 = "Level 2 (Submitter & Approver)";
        public static string Level3 = "Level 3 (Maker, Checker & Approver)";
        public static string Level4 = "Level 4 (Maker, Checker, Approver & Releaser)";
    }



    public static class WorkFlowVariable
    {
        public static string Submitter = "Submitter";
        public static string Approver = "Approver";
        public static string Maker = "Maker";
        public static string Checker = "Checker";
        public static string Releaser = "Releaser";
    }


    public static class RASCIVariable
    {
        public static string Responsible = "Responsible";
        public static string Accountable = "Accountable";
        public static string Support = "Support";
        public static string Consultant = "Consultant";
        public static string Informed = "Informed";
    }


    public static class WorkFlowProcessVariable
    {
        public static string Next = "Next";
        public static string End = "End";
        public static string OldStages = "Old Stages";
    }
}
