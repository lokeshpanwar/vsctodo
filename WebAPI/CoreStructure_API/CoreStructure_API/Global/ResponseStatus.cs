﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Global
{
    public struct ResponseStatus
    {
        public const string SUCCESS = "Success";
        public const string FAILED = "Failed";

        public const string AUTH_FAILED = "Authentication Failed";
        public const string AUTH_TOKEN_MISSING = "Authentication Token Missing";
        public const string AUTH_TOKEN_EXPIRED = "Authentication Token Expired";
        public const string AUTH_UNAUTHORIZED = "Authorization Failed";

        public const string SYSTEM_ERROR = "System Error";

        public const string EMAIL_DOMAIN_INVALID = "Invalid!";
        public const string EMAIL_DOMAIN_VALID = "Valid";
        public const string EMAIL_DOMAIN_INVALID_MESSAGE = "Invalid Email Domain!";
    }
}
