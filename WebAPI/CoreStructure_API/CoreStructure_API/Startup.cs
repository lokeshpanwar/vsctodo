﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
//using Doconut.Viewer.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace CoreStructure_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Doconut Configuration
            services.AddHttpContextAccessor();

            services.AddMemoryCache();
            // doconut end

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });

            services.Configure<DBModel>(Configuration.GetSection("ConnectionStrings"));

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            ConnectionString = Configuration["ConnectionStrings:DbConn"];
            ConnectionStringSL = Configuration["ConnectionStrings:DbConnSL"];
            ConnectionStringBD = Configuration["ConnectionStrings:DbConnBD"];
            ConnectionStringVSCCO = Configuration["ConnectionStrings:DbConnVSCCO"];
            ConnectionStringVSCCC = Configuration["ConnectionStrings:DbConnVSCCC"];
            ConnectionPIB = Configuration["ConnectionStrings:DbConnPIB"];
            ConnectionPTS = Configuration["ConnectionStrings:DbConnPTS"];
            ConnectionAttorneys = Configuration["ConnectionStrings:DbConnAttorneys"];


            Export1 = Configuration["Export1"];
            Export2 = Configuration["Export2"];

            EmailAddress = Configuration["EmailAddress"];
            Password = Configuration["Password"];
            Host = Configuration["Host"];
            Port = Configuration["Port"];
            EnableSsl = Configuration["EnableSsl"];
            CompanyName = Configuration["CompanyName"];
            weblogo = Configuration["weblogo"];
            ImageURL = Configuration["URL:ImageUrl"];
            FormTriggerTemplate = Configuration["FormTriggerTemplate"];
            ExceptionTemplate = Configuration["ExceptionTemplate"];
            DynamicFormTemplate = Configuration["DynamicFormTemplate"];
            CurrentHostEnvironment = env;

            //  // doconut 
            //  app.MapWhen(
            //context => context.Request.Path.ToString().EndsWith("DocImage.axd"),
            //appBranch => {
            //    appBranch.UseDoconut();
            //});

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoreStructure_API");
            });


            app.UseMvc();
        }




        public static string ConnectionString
        {
            get;
            private set;
        }


        public static string ConnectionStringVSCCO
        {
            get;
            private set;
        }

        public static string ConnectionStringVSCCC
        {
            get;
            private set;
        }
        public static string ConnectionPIB
        {
            get;
            private set;
        }

        public static string ConnectionPTS
        {
            get;
            private set;
        }

        public static string ConnectionAttorneys
        {
            get;
            private set;
        }


        
        public static string ConnectionStringSL
        {
            get;
            private set;
        }

        public static string ConnectionStringBD
        {
            get;
            private set;
        }
        public static string ImageURL
        {
            get;
            private set;
        }


        public static string Export1
        {
            get;
            private set;
        }


        public static string Export2
        {
            get;
            private set;
        }
        public static string EmailAddress
        {
            get;
            private set;
        }

        public static string Password
        {
            get;
            private set;
        }

        public static string Host
        {
            get;
            private set;
        }

        public static string Port
        {
            get;
            private set;
        }

        public static string EnableSsl
        {
            get;
            private set;
        }

        public static string CompanyName
        {
            get;
            private set;
        }

        public static string ExceptionTemplate
        {
            get;
            private set;
        }
        public static string DynamicFormTemplate
        {
            get;
            private set;
        }

        public static string FormTriggerTemplate
        {
            get;
            private set;
        }

        public static string weblogo
        {
            get;
            private set;
        }

        public static IHostingEnvironment CurrentHostEnvironment
        {
            get;
            private set;
        }
    }
}
