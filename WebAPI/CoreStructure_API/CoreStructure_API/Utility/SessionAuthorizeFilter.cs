﻿using CoreStructure_API.Global;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Utility
{
    
    public class SessionAuthorizeFilter : ActionFilterAttribute
    {
        //public override void OnResultExecuting(ResultExecutingContext filterContext)
        //{
        //    //You may fetch data from database here 
        //    //filterContext.Controller = "Hello Foo";
        //    base.OnResultExecuting(filterContext);
        //}

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var controllerName = filterContext.RouteData.Values["controller"];
                var actionName = filterContext.RouteData.Values["action"];
                var message = String.Format("{0} controller:{1} action:{2}", "onactionexecuting", controllerName, actionName);

                var strTokenMessage = filterContext.HttpContext.Request.Headers["Authorization-Token"].First();

                var chkToken = CheckToken(strTokenMessage);

                if (chkToken != "")
                {

                }
                else
                {
                    filterContext.Result = new ForbidResult();
                }
            }
            catch(Exception ex)
            {
                filterContext.Result = new ForbidResult();
            }

            ////Debug.WriteLine(message, "Action Filter Log");
            //filterContext.Result = new ForbidResult();
            base.OnActionExecuting(filterContext);
        }



        private string CheckToken(string strAuthorizationToken)
        {
            string strTokenMessage = ResponseStatus.AUTH_UNAUTHORIZED;
            try
            {
                String Token = CryptorEngine.Decrypt(strAuthorizationToken, true);
                string[] arrToken = Token.Split(',');

                if (arrToken.Length > 1)
                {
                    string strUserId = arrToken[0];
                    string strRole = arrToken[1];

                    string expdate= arrToken[7];

                    //if(Convert.ToDateTime(expdate).AddHours(2)>Standard.StandardDateTime.GetDateTime())
                    //{
                        strTokenMessage = ResponseStatus.AUTH_TOKEN_EXPIRED;
                        return strTokenMessage;
                    //}
                    //else
                    //{
                    //    strTokenMessage = String.Empty;
                    //    return strTokenMessage;
                    //}




                    //if (false)//(userTokenCookie == null)
                    //{
                    //    return strTokenMessage;
                    //}
                    //else
                    //{
                    //bool roleFound = false;
                    //AllowedRoles = GlobalFunction.getRoleListAccessibleToAPI(APIId);
                    //string[] rolesList = AllowedRoles.Split(',');

                    //foreach (string str in rolesList)
                    //{
                    //    if (strRole == str.Trim())
                    //    {
                    //        roleFound = true;
                    //        break;
                    //    }
                    //}


                    
                    //}
                }
                else
                {
                    return strTokenMessage;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "SessionAuthorizeFilter/CheckToken");
            }
            return strTokenMessage;
        }



    }

}
