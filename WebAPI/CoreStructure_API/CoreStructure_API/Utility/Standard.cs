﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Utility
{
    public class Standard
    {
        public static class StandardDateTime
        {
            public static DateTime GetDateTime()
            {
                DateTime utcTime = DateTime.UtcNow;
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);

                return localTime;
            }

        }

        public static class StandardUserType
        {

            public static string SUPERADMIN = "SuperAdmin";
            public static string ADMIN = "ADMIN";
            public static string EMPLOYEE = "EMPLOYEE";
            public static string USER = "USER";
            public static string CUSTOMER = "CUSTOMER";
            public static string VENDOR = "VENDOR";
        }


        public static class StandardDBSelectionNameForPO
        {
            public static string SL = "SL";
            public static string SL_FullName = "SRI LANKA";
            public static string BD = "BD";
            public static string BD_FullName = "BANGLADESH";
        }


        public static class StandardGridTypeForPO
        {
            public static string Grid1 = "DIV1";
            public static string Grid2 = "DIV2";
            public static string Grid3 = "DIV3";
        }
    }
}
