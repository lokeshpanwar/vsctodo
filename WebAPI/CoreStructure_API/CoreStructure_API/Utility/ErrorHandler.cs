﻿using CoreStructure_API.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Utility
{
    public class ErrorHandler
    {
        public static void LogError(Exception ex, long AdminId, string ExceptionFrom, string ErrorPage)
        {
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "insert into Exception_Error(stack_trace,exception_message,inner_exception,exception_from,"
                    + " error_page,created_on,created_by) values "
                    + " (@stack_trace,@exception_message,@inner_exception,@exception_from,"
                    + " @error_page,@created_on,@created_by)";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@stack_trace", ex.StackTrace);
                    cmd.Parameters.AddWithValue("@exception_message", ex.Message);
                    cmd.Parameters.AddWithValue("@inner_exception", ex.Source);
                    cmd.Parameters.AddWithValue("@exception_from", ExceptionFrom);
                    cmd.Parameters.AddWithValue("@error_page", ErrorPage);
                    cmd.Parameters.AddWithValue("@created_by", AdminId);
                    cmd.Parameters.AddWithValue("@created_on", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                }
                catch (Exception ex1)
                {
                    transaction.Rollback();
                    connection.Close();
                }
            }
            catch (Exception exp)
            {
            }
            finally
            {
            }
        }



        public string InsertLogError(Exception_Error_Model objModel)
        {
            string result = "Errro on inserting exception log!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "insert into Exception_Error(stack_trace,exception_message,inner_exception,exception_from,"
                    + " error_page,created_on,created_by) values "
                    + " (@stack_trace,@exception_message,@inner_exception,@exception_from,"
                    + " @error_page,@created_on,@created_by)";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@stack_trace", objModel.stack_trace);
                    cmd.Parameters.AddWithValue("@exception_message", objModel.exception_message);
                    cmd.Parameters.AddWithValue("@inner_exception", objModel.inner_exception);
                    cmd.Parameters.AddWithValue("@exception_from", objModel.exception_from);
                    cmd.Parameters.AddWithValue("@error_page", objModel.error_page);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.created_by);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Error log Inserted Successfully!";
                }
                catch (Exception ex1)
                {
                    transaction.Rollback();
                    connection.Close();
                }
            }
            catch (Exception exp)
            {
            }
            finally
            {
            }
            return result;
        }

    }
}
