﻿using CoreStructure_API.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Utility
{
    public class SqlHelper
    {
        

        public static SqlConnection Connection()
        {
            string connString = Startup.ConnectionString;

            SqlConnection con = new SqlConnection(connString);
            return con;
        }

        public static SqlConnection ConnectionVSCCO()
        {
            string connString = Startup.ConnectionStringVSCCO;

            SqlConnection con = new SqlConnection(connString);
            return con;

        }

        public static SqlConnection ConnectionStringVSCCC()
        {
            string connString = Startup.ConnectionStringVSCCC;
            SqlConnection con = new SqlConnection(connString);
            return con;
        }

        public static SqlConnection ConnectionPIB()
        {
            string connString = Startup.ConnectionPIB;
            SqlConnection con = new SqlConnection(connString);
            return con;
        }

        public static SqlConnection ConnectionPTS()
        {
            string connString = Startup.ConnectionPTS;
            SqlConnection con = new SqlConnection(connString);
            return con;
        }

        public static SqlConnection ConnectionAttorneys()
        {
            string connString = Startup.ConnectionAttorneys;
            SqlConnection con = new SqlConnection(connString);
            return con;
        }



    }
}
