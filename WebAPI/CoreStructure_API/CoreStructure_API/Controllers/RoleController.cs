﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        Role_Repository objRoleRepo = new Role_Repository();


        #region NewRole (POST)

        [HttpPost]
        [Route("NewRole")]
        [SessionAuthorizeFilter]
        public ActionResult NewRole(Role_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.Parent_compID = GlobalFunction.getLoggedInUserParentCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objRoleRepo.InsertRole(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion

        

        #region ListRoleByPagination (GET)

        [HttpGet]
        [Route("ListRoleByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListRoleByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objRoleRepo.GetRoleListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion
                     


        #region OneRole (GET)

        [HttpGet]
        [Route("OneRole")]
        [SessionAuthorizeFilter]
        public ActionResult OneRole(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetOneRole(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion



        #region EditRole (PUT)

        [HttpPut]
        [Route("EditRole")]
        [SessionAuthorizeFilter]
        public ActionResult EditRole(Role_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objRoleRepo.UpdateRole(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region DisableRole

        [HttpDelete]
        [Route("DisableRole")]
        [SessionAuthorizeFilter]
        public ActionResult DisableRole(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objRoleRepo.DisableRole(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion

        #region EnableRole

        [HttpDelete]
        [Route("EnableRole")]
        [SessionAuthorizeFilter]
        public ActionResult EnableRole(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objRoleRepo.EnableRole(Id, Modified_ById);
            return Ok(new { results = result });

        }


        #endregion


    }
}