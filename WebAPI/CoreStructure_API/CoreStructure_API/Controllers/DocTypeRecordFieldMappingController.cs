﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocTypeRecordFieldMapping")]
    [ApiController]
    public class DocTypeRecordFieldMappingController : ControllerBase
    {

        DocTypeRecordFieldMapping_Repository objDocTypeRecordFieldRepo = new DocTypeRecordFieldMapping_Repository();


        #region NewDocTypeRecordFieldMapping (POST)

        [HttpPost]
        [Route("NewDocTypeRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocTypeRecordFieldMapping(RecordField_DocType_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objReturn = objDocTypeRecordFieldRepo.InsertDocTypeRecordFieldMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


        #region ListDocTypeRecordFieldMappingByPagination (GET)

        [HttpGet]
        [Route("ListDocTypeRecordFieldMappingByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocTypeRecordFieldMappingByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocTypeRecordFieldRepo.GetDocTypeRecordFieldMappingListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion







        #region DisableDocTypeRecordFieldMapping

        [HttpDelete]
        [Route("DisableDocTypeRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocTypeRecordFieldMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocTypeRecordFieldRepo.DisableDocTypeRecordFieldMapping(Id, Modified_ById);
            return Ok(new { results = result });
        }

        #endregion



        #region EnableDocTypeRecordFieldMapping

        [HttpDelete]
        [Route("EnableDocTypeRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocTypeRecordFieldMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocTypeRecordFieldRepo.EnableDocTypeRecordFieldMapping(Id, Modified_ById);
            return Ok(new { results = result });
        }

        #endregion



        #region DeleteDocTypeRecordFieldMapping

        [HttpDelete]
        [Route("DeleteDocTypeRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteDocTypeRecordFieldMapping(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocTypeRecordFieldRepo.DeleteDocTypeRecordFieldMapping(Id, Deleted_ById);
            return Ok(new { results = objreturn });
        }

        #endregion


    }
}