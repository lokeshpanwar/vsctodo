﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocClassUserMapping")]
    [ApiController]
    public class DocClassUserMappingController : ControllerBase
    {

        DocClassUserMapping_Repository objDocClassUserMappingRepo = new DocClassUserMapping_Repository();


        #region NewDocClassUserMappingMapping (POST)

        [HttpPost]
        [Route("NewDocClassUserMapping")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocClassUserMapping(User_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocClassUserMappingRepo.InsertDocClassUserMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion




        #region GetDocClassUserListNotMapped (GET)

        [HttpGet]
        [Route("GetDocClassUserListNotMapped")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocClassUserListForMapping(long subcompid, long domainid, long classid)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassUserMappingRepo.GetEmployeeListNotMapped(subcompid, domainid, classid, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion
        
        #region GetDocClassVendorListNotMapped (GET)

        [HttpGet]
        [Route("GetDocClassVendorListNotMapped")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocClassVendorListForMapping(long subcompid, long domainid, long classid)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassUserMappingRepo.GetVendorListNotMapped(subcompid, domainid, classid, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

        



        #region GetDocClassUserListToUnMap (GET)

        [HttpGet]
        [Route("GetDocClassUserListToUnMap")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocClassUserListToUnMap(long subcompid, long domainid, long classid)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassUserMappingRepo.GetEmployeeListToUnMap(subcompid, domainid, classid, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

        #region GetDocClassVendorListToUnMap (GET)

        [HttpGet]
        [Route("GetDocClassVendorListToUnMap")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocClassVendorListToUnMap(long subcompid, long domainid, long classid)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassUserMappingRepo.GetVendorListToUnMap(subcompid, domainid, classid, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion





        #region ListDocClassUserMappingByPagination (GET)

        [HttpGet]
        [Route("ListDocClassUserMappingByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocClassUserMappingByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassUserMappingRepo.GetDocClassUserMappingListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion







        #region UnMapDocClassUserMappingMapping (POST)

        [HttpPost]
        [Route("UnMapDocClassUserMapping")]
        [SessionAuthorizeFilter]
        public ActionResult UnMapDocClassUserMapping(User_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocClassUserMappingRepo.UnMapDocClassUserMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


    }
}