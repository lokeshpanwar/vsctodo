﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Domain")]
    [ApiController]
    public class DomainController : ControllerBase
    {
        Domain_Repository objDomainRepo = new Domain_Repository();


        [HttpPost]
        [Route("NewDomain")]
        [SessionAuthorizeFilter]
        public ActionResult NewDomain(Domain_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objDomainRepo.InsertDomain(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }




        [HttpGet]
        [Route("ListDomain")]
        [SessionAuthorizeFilter]
        public ActionResult ListDomain()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDomainRepo.GetDomainList(CreatedBy);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneDomain")]
        [SessionAuthorizeFilter]
        public ActionResult OneDomain(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDomainRepo.GetOneDomain(Id,ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditDomain")]
        [SessionAuthorizeFilter]
        public ActionResult EditDomain(Domain_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objDomainRepo.UpdateDomain(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }




        [HttpDelete]
        [Route("DisableDomain")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objDomainRepo.DisableDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableDomain")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objDomainRepo.EnableDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }








        #region ListDomainByPagination (GET)

        [HttpGet]
        [Route("ListDomainByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDomainByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDomainRepo.GetDomainListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion





        [HttpPost]
        [Route("DomainMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DomainMapping(Domain_Mapping_Display objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.Maped_By = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.Parent_CompId = GlobalFunction.getLoggedInUserParentCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDomainRepo.InsertDomainMapping(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("ListDomainForMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListDomainForMapping(long SubCompId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDomainRepo.GetDomainListForMapping(CreatedBy, SubCompId);
            return Ok(new { results = datamodel });
        }






        [HttpDelete]
        [Route("DeleteDomain")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDomainRepo.DeleteDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }


    }
}