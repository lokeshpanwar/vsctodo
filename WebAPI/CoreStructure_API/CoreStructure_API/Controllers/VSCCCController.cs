﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/VSCCC")]
    [ApiController]
    public class VSCCCController : Controller
    {
        VSCCC_Repository objQuery = new VSCCC_Repository();

        [HttpGet]
        [Route("VscccClientlist")]
        [SessionAuthorizeFilter]
        public IActionResult VscccClientlist(int officeid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VscccClientlist(officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCCActivity")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCCActivity(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCActivity(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCCSubACtivity")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCCSubACtivity(int OfficeId, int Activityid)
        {
            if (ModelState.IsValid)
            {
                //objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objQuery.VSCCCSubACtivity(OfficeId, Activityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("UserList")]
        [SessionAuthorizeFilter]
        public IActionResult UserList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.UserList(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Filltimesheet")]
        [SessionAuthorizeFilter]
        public IActionResult Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Filltimesheet(empid, date, time, description, clientid, activityid, subactivityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCCUserData")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCCUserData(int UserId)
        {

            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCUserData(UserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
    }
}
