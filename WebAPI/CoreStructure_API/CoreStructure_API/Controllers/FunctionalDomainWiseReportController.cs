﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/FunctionalDomainWiseReport")]
    [ApiController]
    public class FunctionalDomainWiseReportController : ControllerBase
    {
        FunctionalDomainWiseReport_Repository objFuncRepo = new FunctionalDomainWiseReport_Repository();



        [HttpGet]
        [Route("ListReport")]
        [SessionAuthorizeFilter]
        public ActionResult ListReport(long SubCompId, long DomainId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objFuncRepo.GetReportList(CreatedBy, SubCompId, DomainId);
            return Ok(new { results = datamodel });
        }


    }
}