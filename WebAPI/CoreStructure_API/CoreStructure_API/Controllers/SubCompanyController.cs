﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/SubCompany")]
    [ApiController]
    public class SubCompanyController : ControllerBase
    {
        SubCompany_Repository objSubCompRepo = new SubCompany_Repository();

        //private readonly IHostingEnvironment _hostingEnvironment;

        //public SubCompanyController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}


        [HttpPost]
        [Route("NewSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult NewSubCompany(Sub_Company_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";

            if (ModelState.IsValid)
            {
                if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                {
                    objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                    objreturn = objSubCompRepo.InsertSubCompany(objModel);
                    return Ok(new { results = objreturn });
                }
                else
                {
                    objreturn.ReturnMessage = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE;
                    return Ok(new { results = objreturn });
                }
            }
            else
            {
                return (Ok(new { results = objreturn }));
            }
        }





        [HttpGet]
        [Route("ListSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult ListSubCompany()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objSubCompRepo.GetSubCompanyList(CreatedBy);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult OneSubCompany(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objSubCompRepo.GetOneSubCompany(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult EditSubCompany(Sub_Company_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                {
                    objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                    objreturn = objSubCompRepo.UpdateSubCompany(objModel);
                    return Ok(new { results = objreturn });
                }
                else
                {
                    objreturn.ReturnMessage = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE;
                    return Ok(new { results = objreturn });
                }
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }








        [HttpDelete]
        [Route("DisableSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult DisableSubCompany(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objSubCompRepo.DisableSubCompany(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult EnableSubCompany(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objSubCompRepo.EnableSubCompany(Id, Modified_ById);
            return Ok(new { results = result });

        }






        #region ListSubCompanyByPagination (GET)

        [HttpGet]
        [Route("ListSubCompanyByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListSubCompanyByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objSubCompRepo.GetSubCompanyListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion






        [HttpDelete]
        [Route("DeleteSubCompany")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteSubCompany(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objSubCompRepo.DeleteSubCompany(Id, Modified_ById);
            return Ok(new { results = result });

        }




        [HttpPost]
        [Route("NewSubCompanyBulkUpload")]
        [SessionAuthorizeFilter]
        public ActionResult NewSubCompanyBulkUpload(Sub_Company_table_page_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";

            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                bool dataok = false;
                foreach(var data in objModel.Sub_Company_table_Model_List)
                {
                    if (GlobalFunction.CheckIfValidDomainFromEmail(data.Email, CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                    {
                        dataok = true;
                    }
                    else
                    {
                        dataok = false;
                        objreturn.ReturnMessage = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE;
                        return Ok(new { results = objreturn });
                    }
                }
                if (dataok == true)
                {
                    objreturn = objSubCompRepo.InsertSubCompanyFromBulk(objModel.Sub_Company_table_Model_List, CreatedBy);
                    return Ok(new { results = objreturn });
                }
                else
                {
                    objreturn.ReturnMessage = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE;
                    return Ok(new { results = objreturn });
                }
            }
            else
            {
                return (Ok(new { results = objreturn }));
            }
        }





        #region GetSubCompanyCSV (GET)

        [HttpGet]
        [Route("GetSubCompanyCSV")]
        public IActionResult GetSubCompanyCSV()
        {
            try
            {
                string samplepath = "Content/SampleCSV/subcompany_sample.csv";
                var path = Path.Combine("", samplepath);
                
                var image = System.IO.File.OpenRead(path);
                return File(image, "text/csv", "subcompany_sample.csv");
            }
            catch (Exception ex)
            { }
            return null;
        }

        #endregion


    }
}