﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/FormItem")]
    [ApiController]
    public class FormItemController : ControllerBase
    {
        FormItem_Repository objFormRepo = new FormItem_Repository();


        [HttpPost]
        [Route("NewFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult NewFormItem(Form_Item_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFormRepo.InsertFormItem(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpGet]
        [Route("ListFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult ListFormItem(long FormId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objFormRepo.GetFormItemList(CreatedBy, FormId);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult OneFormItem(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objFormRepo.GetOneFormItem(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult EditFormItem(Form_Item_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFormRepo.UpdateFormItem(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpDelete]
        [Route("DisableFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult DisableFormItem(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFormRepo.DisableFormItem(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableFormItem")]
        [SessionAuthorizeFilter]
        public ActionResult EnableFormItem(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFormRepo.EnableFormItem(Id, Modified_ById);
            return Ok(new { results = result });

        }



                     

        #region ListFormByPagination (GET)

        [HttpGet]
        [Route("ListFormItemByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListFormItemByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objFormRepo.GetFormItemListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

    }
}