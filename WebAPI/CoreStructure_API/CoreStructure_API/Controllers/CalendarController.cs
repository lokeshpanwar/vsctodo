﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/Mastcalendar")]
    [ApiController]
    public class CalendarController : ControllerBase
    {

        Calendar_Repository objEvetMstRep = new Calendar_Repository();


        #region New Calender Master
        [HttpPost]
        [Route("NewMastcalendar")]
        [SessionAuthorizeFilter]
        public ActionResult NewMastcalendar(Mastcalendar_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.Empid = Convert.ToInt32(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()));
                objreturn = objEvetMstRep.InsertMastCalender(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One MastcalendarList
        [HttpGet]
        [Route("OneMastcalendarList")]
        [SessionAuthorizeFilter]

        public ActionResult OneMastcalendarList(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneMastCalender(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit Mastcalendar

        [HttpPut]
        [Route("EditMastcalendar")]
        [SessionAuthorizeFilter]

        public ActionResult EditMastcalendar(Mastcalendar_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.Empid = Convert.ToInt32(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()));
                objreturn = objEvetMstRep.UpdateMastCalender(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion


        #region Disable Mastcalendar

        [HttpDelete]
        [Route("DisableMastcalendar")]
        [SessionAuthorizeFilter]

        public ActionResult DisableMastcalendar(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableMastcalendar(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region Enable Mastcalendar
        [HttpDelete]
        [Route("EnableMastcalendar")]
        [SessionAuthorizeFilter]
        public ActionResult EnableMastcalendar(long Id)
        {
            
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn.ReturnStatus = "Error";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.EnableMastcalendar(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }

            else
            {
                return Ok(new { results = objreturn });

            }
        }
        #endregion


        #region ListMastcalendar By pagination

        [HttpGet]
        [Route("ListMastcalendarBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListMastcalendarBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetMastcalendarByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion


        #region ListMastcalendarByEmpID

        [HttpGet]
        [Route("ListMastcalendarByEmpID")]
        [SessionAuthorizeFilter]

        public ActionResult ListMastcalendarByEmpID()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetMastcalendarByEmpID(CreatedBy);

            return Ok(new { results = result });
        }
        #endregion
    }
}