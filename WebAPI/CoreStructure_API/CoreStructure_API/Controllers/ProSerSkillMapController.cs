﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/ProSerSkillMap")]
    [ApiController]
    public class ProSerSkillMapController : ControllerBase
    {

        ProSerSkillMap_Repository objEvetMstRep = new ProSerSkillMap_Repository();


        #region New ProSerSkillMap
        [HttpPost]
        [Route("NewProSerSkillMap")]
        [SessionAuthorizeFilter]
        public ActionResult NewProSerSkillMap(ProSerSkillMap_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.creationuser = Convert.ToInt64(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First())).ToString();
                objreturn = objEvetMstRep.InsertProSerSkillMap(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One ProSerSkillMap
        [HttpGet]
        [Route("OneProSerSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult OneProSerSkillMap(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneProSerSkillMap(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit ProSerSkillMap

        [HttpPut]
        [Route("EditProSerSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult EditProSerSkillMap(ProSerSkillMap_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.modifactionuser = Convert.ToInt64(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First())).ToString();
                objreturn = objEvetMstRep.UpdateProSerSkillMap(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion


        #region Disable ProSerSkillMap

        [HttpDelete]
        [Route("DisableProSerSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult DisableProSerSkillMap(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableProSerSkillMap(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region Enable ProSerSkillMap
        [HttpDelete]
        [Route("EnableProSerSkillMap")]
        [SessionAuthorizeFilter]
        public ActionResult EnableProSerSkillMap(long Id)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn.ReturnStatus = "Error";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.EnableProSerSkillMap(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }

            else
            {
                return Ok(new { results = objreturn });

            }
        }
        #endregion


        #region ProSerSkillMap By pagination

        [HttpGet]
        [Route("ListProSerSkillMapBypagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListProSerSkillMapBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetProSerSkillMapByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion
    }
}