﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Report")]
    [ApiController]
    public class ReportController : Controller
    {
        Report_Repository objReport = new Report_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;

        public ReportController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}
        #region PolicyReport
        [HttpGet]
        [Route("ViewPolicyReport")]
        [SessionAuthorizeFilter]
        public ActionResult ViewPolicyReport(int OfficeId, int typeId, string FromDate, string ToDate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId)
        {
            if (ModelState.IsValid)
            {
                List<PolicyReport> result = new List<PolicyReport>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objReport.ViewPolicyReport(OfficeId, typeId, FromDate, ToDate, contactPersonId, categoryId, insuranceCompanyId, issuingOfficeId, productId, groupId, policyStatusId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
      
        #region AdminDailyBusiness
        [HttpGet]
        [Route("AdminDailyBusiness")]
        [SessionAuthorizeFilter]
        public ActionResult AdminDailyBusiness(int OfficeId, string fromdate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                DailyBusiness result = new DailyBusiness();
                result = objReport.AdminDailyBusiness(OfficeId, fromdate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
        #region ViewAdminDailyBusinessClientWise
        [HttpGet]
        [Route("ViewAdminDailyBusinessClientWise")]
        [SessionAuthorizeFilter]
        public ActionResult ViewAdminDailyBusinessClientWise(string fromdate, string ToDate, int ClientId, string OrderBy, int OfficeId)
        {
            if (ModelState.IsValid)
            {
                DailyBusiness result = new DailyBusiness();
                result = objReport.ViewAdminDailyBusinessClientWise(fromdate, ToDate, ClientId, OrderBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
        #region ViewAdminDailyBusinessDayWise
        [HttpGet]
        [Route("ViewAdminDailyBusinessDayWise")]
        [SessionAuthorizeFilter]
        public ActionResult ViewAdminDailyBusinessDayWise(int OfficeId, string fromdate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                DailyBusiness result = new DailyBusiness();
                result = objReport.AdminDailyBusinessDayWise(OfficeId, fromdate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
        #region ViewUpcomingPolicy
        [HttpGet]
        [Route("ViewUpcomingPolicy")]
        [SessionAuthorizeFilter]
        public ActionResult ViewUpcomingPolicy(int OfficeId, string fromdate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                List<UpcomingPolicy> result = new List<UpcomingPolicy>();
                result = objReport.ViewUpcomingPolicy(OfficeId, fromdate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Fields!" });
            }
        }
        #endregion
        #region ViewClaimReport
        [HttpGet]
        [Route("ViewClaimReport")]
        [SessionAuthorizeFilter]
        public ActionResult ViewClaimReport(int OfficeId, string policyNo, string contactPersonId, string policyholderId)
        {
            if (ModelState.IsValid)
            {
                List<ClaimReport> result = new List<ClaimReport>();
                if (policyNo == null)
                {
                    policyNo = "";
                }
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objReport.ViewClaimReport(OfficeId, policyNo, contactPersonId, policyholderId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region DashboardReport
        [HttpGet]
        [Route("GetMonthlySalesForGraph")]
        [SessionAuthorizeFilter]
        public ActionResult GetMonthlySalesForGraph()
        {
            if (ModelState.IsValid)
            {
                List<DashboardGraph> result = new List<DashboardGraph>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objReport.GetMonthlySalesForGraph(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region RenewalPolicyReport
        [HttpGet]
        [Route("ViewRenewalReport")]
        [SessionAuthorizeFilter]
        public ActionResult ViewRenewalReport(long OfficeId, string Type, string FromDate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objReport.ViewRenewalReport(OfficeId, Type, FromDate, ToDate, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
    }
}