﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentType")]
    [ApiController]
    public class DocumentTypeController : ControllerBase
    {

        DocumentType_Repository objDocTypeRepo = new DocumentType_Repository();


        [HttpPost]
        [Route("NewDocumentType")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocumentType(DocumentType_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocTypeRepo.InsertDocumentType(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }


        #region ListDocumentTypeByPagination (GET)

        [HttpGet]
        [Route("ListDocumentTypeByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentTypeByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocTypeRepo.GetDocumentTypeListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


        [HttpGet]
        [Route("OneDocumentType")]
        [SessionAuthorizeFilter]
        public ActionResult OneDocumentType(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDocTypeRepo.GetOneDocumentType(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPut]
        [Route("EditDocumentType")]
        [SessionAuthorizeFilter]
        public ActionResult EditDocumentType(DocumentType_Table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objDocTypeRepo.UpdateDocumentType(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }












        [HttpDelete]
        [Route("DisableDocumentType")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocumentType(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocTypeRepo.DisableDocumentType(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }



        [HttpDelete]
        [Route("EnableDocumentType")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocumentType(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocTypeRepo.EnableDocumentType(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }



    }
}