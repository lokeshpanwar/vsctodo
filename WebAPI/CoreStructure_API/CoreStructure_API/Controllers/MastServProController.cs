﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/ServiceProduct")]
    [ApiController]
    public class MastServProController : ControllerBase
    {

        MastServPro_Repository objEvetMstRep = new MastServPro_Repository();

        #region New ServiceProduct Master
        [HttpPost]
        [Route("NewServiceProduct")]
        [SessionAuthorizeFilter]
        public ActionResult NewServiceProduct(MastServPro_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.creationuser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objreturn = objEvetMstRep.InsertSeriveProduct(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion

        #region One OneServiceProductList
        [HttpGet]
        [Route("OneServiceProductList")]
        [SessionAuthorizeFilter]

        public ActionResult OneServiceProductList(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneServiceProduct(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit ServiceProductMaster

        [HttpPut]
        [Route("EditServiceProductMaster")]
        [SessionAuthorizeFilter]

        public ActionResult EditServiceProductMaster(MastServPro_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.modifactionuser = Convert.ToInt64(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First())).ToString();
                objreturn = objEvetMstRep.UpdateServiceProductMaster(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion

        #region Disable ServiceProductMaster

        [HttpDelete]
        [Route("DisableServiceProductMaster")]
        [SessionAuthorizeFilter]

        public ActionResult DisableServiceProductMaster(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableServiceproductMaster(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion

        #region Enable ServiceProductMaster

        [HttpDelete]
        [Route("EnableServiceProductMaster")]
        [SessionAuthorizeFilter]

        public ActionResult EnableServiceProductMaster(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.EnableServiceproductMaster(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region ListServiceProduct By pagination

        [HttpGet]
        [Route("ListServiceProductMasterBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListServiceProductMasterBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetServiceProductMasterByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion


        #region GetMaxServiceProductId
        [HttpGet]
        [Route("GetMaxServiceProductId")]
        [SessionAuthorizeFilter]

        public ActionResult GetMaxServiceProductId()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetMaxServiceProductId(CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion
    }
}