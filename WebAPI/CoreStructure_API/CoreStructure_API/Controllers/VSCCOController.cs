﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/VSCCO")]
    [ApiController]
    public class VSCCOController : Controller
    {
        VSCCO_Repository objQuery = new VSCCO_Repository();

        [HttpGet]
        [Route("VscClientlist")]
        [SessionAuthorizeFilter]
        public IActionResult VscClientlist(int officeid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VscClientlist(officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCOActivity")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCOActivity( int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCOActivity(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCOSubACtivity")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCOSubACtivity(int OfficeId, int Activityid)
        {
            if (ModelState.IsValid)
            {
                //objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objQuery.VSCCOSubACtivity(OfficeId, Activityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("UserList")]
        [SessionAuthorizeFilter]
        public IActionResult UserList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.UserList(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("Filltimesheet")]
        [SessionAuthorizeFilter]
        public IActionResult Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Filltimesheet(empid, date, time, description, clientid, activityid, subactivityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VSCCOUserData")]
        [SessionAuthorizeFilter]
        public IActionResult VSCCOUserData(int UserId)
        {

            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCOUserData(UserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



       
    }
}
