﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentPermission")]
    [ApiController]
    public class DocumentPermissionController : ControllerBase
    {

        DocumentPermission_Repository objDocumentPermissionRepo = new DocumentPermission_Repository();



        #region GetDocumentPermissionFromUserId (GET)

        [HttpGet]
        [Route("GetDocumentPermissionFromUserId")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocumentPermissionFromUserId(long subcompid, long domainid)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocumentPermissionRepo.GetDocumentUserPermissionList(subcompid,domainid, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion






        #region DocumentPermissionMapping (POST)

        [HttpPost]
        [Route("DocumentPermissionMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DocumentPermissionMapping(UserDocMapping_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocumentPermissionRepo.InsertDocumentUserPermission(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



    }
}