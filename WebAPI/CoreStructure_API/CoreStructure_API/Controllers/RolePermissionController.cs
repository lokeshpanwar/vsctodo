﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/RolePermission")]
    [ApiController]
    public class RolePermissionController : ControllerBase
    {
        RolePermission_Repository objRolePermissionRepo = new RolePermission_Repository();


        #region ModuleListBasedOnRole (GET)

        [HttpGet]
        [Route("ModuleListBasedOnRole")]
        [SessionAuthorizeFilter]
        public ActionResult ModuleListBasedOnRole(long RoleID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objRolePermissionRepo.GetModuleListBasedOnRole(RoleID, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion




        #region CreateRoleModulePermission (POST)

        [HttpPost]
        [Route("CreateRoleModulePermission")]
        [SessionAuthorizeFilter]
        public ActionResult CreateRoleModulePermission(Role_Modules_Permission_Display_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objRolePermissionRepo.InsertRoleModulePermission(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


    }
}