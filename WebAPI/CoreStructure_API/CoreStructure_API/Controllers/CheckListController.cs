﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/CheckList")]
    [ApiController]
    public class CheckListController : ControllerBase
    {
        CheckList_Repository objCheckListRepo = new CheckList_Repository();


        [HttpPost]
        [Route("NewCheckList")]
        [SessionAuthorizeFilter]
        public ActionResult NewCheckList(Checklist_Table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objCheckListRepo.InsertCheckList(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }
               

        #region ListCheckListByPagination (GET)

        [HttpGet]
        [Route("ListCheckListByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListCheckListByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objCheckListRepo.GetCheckListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion







        [HttpGet]
        [Route("OneCheckList")]
        [SessionAuthorizeFilter]
        public ActionResult OneCheckList(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objCheckListRepo.GetOneCheckList(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
                     

        [HttpPut]
        [Route("EditCheckList")]
        [SessionAuthorizeFilter]
        public ActionResult EditCheckList(Checklist_Table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objCheckListRepo.UpdateCheckList(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }









        [HttpDelete]
        [Route("DisableCheckList")]
        [SessionAuthorizeFilter]
        public ActionResult DisableCheckList(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objCheckListRepo.DisableCheckList(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }



        [HttpDelete]
        [Route("EnableCheckList")]
        [SessionAuthorizeFilter]
        public ActionResult EnableCheckList(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objCheckListRepo.EnableCheckList(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }


    }
}