﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Document")]
    [ApiController]
    public class DocumentController : ControllerBase
    {



        Document_Repository objDocRepo = new Document_Repository();


        //private readonly IHostingEnvironment _hostingEnvironment;
        //public DocumentController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}




        #region UploadDocument (POST)

        [HttpPost]
        [Route("UploadDocument")]
        [SessionAuthorizeFilter]
        public ActionResult UploadDocument(DocUpload_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocRepo.UploadDocument(objModel, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region ListDocumentListByClassId (GET)

        [HttpGet]
        [Route("ListDocumentListByClassId")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentListByClassId(long ClassID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocRepo.GetDocumentListByClassId(CreatedBy, ClassID);
            return Ok(new { results = result });
        }

        #endregion





        #region ViewDocumentById (GET)

        [HttpGet]
        [Route("ViewDocumentById")]
        [SessionAuthorizeFilter]
        public ActionResult ViewDocumentById(long DocID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocRepo.GetOneDocumentById(CreatedBy, DocID, HttpContext.Request.Headers["Authorization-Token"].First());
            return Ok(new { results = result });
        }

        #endregion






        #region CheckoutDocument (PUT)

        [HttpPut]
        [Route("CheckoutDocument")]
        [SessionAuthorizeFilter]
        public ActionResult CheckoutDocument(DocumentCheckout_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocRepo.CheckoutDocument(CreatedBy, objModel.DocId, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region GetDocumentFile(GET)

        [HttpGet]
        [Route("GetDocumentFile")]
        public IActionResult GetDocumentFile(string path)
        {
            var image = System.IO.File.OpenRead(path);
            var ctype = path.Split('.');
            return File(image, ctype[0].ToString());
        }

        #endregion



        #region GetCheckoutDocumentList (GET)

        [HttpGet]
        [Route("GetCheckoutDocumentList")]
        [SessionAuthorizeFilter]
        public ActionResult GetCheckoutDocumentList(long SubCompId, long DomainId, long UserId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocRepo.GetCheckoutDocumentList(CreatedBy, SubCompId, DomainId, UserId);
            return Ok(new { results = result });
        }

        #endregion



        #region CancelCheckoutDocument (PUT)

        [HttpPut]
        [Route("CancelCheckoutDocument")]
        [SessionAuthorizeFilter]
        public ActionResult CancelCheckoutDocument(DocumentCheckout_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocRepo.CancelCheckoutDocumentFromAdmin(CreatedBy, objModel.DocId, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion





        #region GetOneDocumentClassReport (GET)

        [HttpGet]
        [Route("GetOneDocumentClassReport")]
        [SessionAuthorizeFilter]
        public ActionResult GetOneDocumentClassReport(long ClassId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocRepo.GetOneDocumentClassReport(ClassId, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion






        #region CheckinDocument (PUT)

        [HttpPut]
        [Route("CheckinDocument")]
        [SessionAuthorizeFilter]
        public ActionResult CheckinDocument(DocUpload_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocRepo.CheckinDocument(objModel, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion







        #region DocumentSubmitForApproval (PUT)

        [HttpPut]
        [Route("DocumentSubmitForApproval")]
        [SessionAuthorizeFilter]
        public ActionResult DocumentSubmitForApproval(DocUpload_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocRepo.DocumentSubmitForApproval(objModel, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion

    }
}