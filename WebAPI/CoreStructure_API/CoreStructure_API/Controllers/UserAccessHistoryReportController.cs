﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/UserAccessHistoryReport")]
    [ApiController]
    public class UserAccessHistoryReportController : ControllerBase
    {
        UserAccessHistoryReport_Repository objRoleRepo = new UserAccessHistoryReport_Repository();



        #region ListReport (GET)

        [HttpGet]
        [Route("ListReport")]
        [SessionAuthorizeFilter]
        public ActionResult ListReport(long UserId, DateTime FromDate, DateTime ToDate)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetReport(FromDate, ToDate, UserId, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

    }
}