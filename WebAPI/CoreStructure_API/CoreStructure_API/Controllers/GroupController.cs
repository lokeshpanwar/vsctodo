﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Group")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        Group_Repository objGroupRepo = new Group_Repository();


        [HttpPost]
        [Route("NewGroup")]
        [SessionAuthorizeFilter]
        public ActionResult NewGroup(Group_table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objGroupRepo.InsertGroup(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpGet]
        [Route("ListGroup")]
        [SessionAuthorizeFilter]
        public ActionResult ListGroup()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objGroupRepo.GetGroupList(CreatedBy);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneGroup")]
        [SessionAuthorizeFilter]
        public ActionResult OneGroup(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objGroupRepo.GetOneGroup(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditGroup")]
        [SessionAuthorizeFilter]
        public ActionResult EditGroup(Group_table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objGroupRepo.UpdateGroup(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpDelete]
        [Route("DisableGroup")]
        [SessionAuthorizeFilter]
        public ActionResult DisableGroup(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objGroupRepo.DisableGroup(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableGroup")]
        [SessionAuthorizeFilter]
        public ActionResult EnableGroup(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objGroupRepo.EnableGroup(Id, Modified_ById);
            return Ok(new { results = result });

        }








        #region ListGroupByPagination (GET)

        [HttpGet]
        [Route("ListGroupByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListGroupByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objGroupRepo.GetGroupListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion




        [HttpGet]
        [Route("ListCompanyForGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListCompanyForGroupMapping(long GroupId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objGroupRepo.GetGroupCompanyListForMapping(CreatedBy, GroupId);
            return Ok(new { results = datamodel });
        }

        [HttpGet]
        [Route("ListDomainForGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListDomainForGroupMapping(long GroupId, long SubCompId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objGroupRepo.GetGroupCompanyDomainListForMapping(CreatedBy, GroupId,SubCompId);
            return Ok(new { results = datamodel });
        }


        [HttpGet]
        [Route("ListUserForGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListUserForGroupMapping(long GroupId, long SubCompId, long DomainID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objGroupRepo.GetGroupUserListForMapping(CreatedBy, GroupId, SubCompId, DomainID);
            return Ok(new { results = datamodel });
        }
        

        [HttpGet]
        [Route("ListVendorForGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListVendorForGroupMapping(long GroupId, long SubCompId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objGroupRepo.GetGroupVendorListForMapping(CreatedBy, GroupId, SubCompId);
            return Ok(new { results = datamodel });
        }




        [HttpPost]
        [Route("GroupCompanyMapping")]
        [SessionAuthorizeFilter]
        public ActionResult GroupCompanyMapping(Group_Company_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long Maped_By = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());                
                string result = objGroupRepo.InsertGroupCompanyMapping(objModel, Maped_By);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        
        [HttpPost]
        [Route("GroupDomainMapping")]
        [SessionAuthorizeFilter]
        public ActionResult GroupDomainMapping(Group_Company_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long Maped_By = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objGroupRepo.InsertGroupDomainMapping(objModel, Maped_By);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        
        [HttpPost]
        [Route("GroupUserMapping")]
        [SessionAuthorizeFilter]
        public ActionResult GroupUserMapping(Group_Company_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long Maped_By = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objGroupRepo.InsertGroupUserMapping(objModel, Maped_By);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpPost]
        [Route("GroupVendorMapping")]
        [SessionAuthorizeFilter]
        public ActionResult GroupVendorMapping(Group_Company_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long Maped_By = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objGroupRepo.InsertGroupVendorMapping(objModel, Maped_By);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }






        #region ListGroupMapByPagination (GET)

        [HttpGet]
        [Route("ListGroupMapByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListGroupMapByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objGroupRepo.GetGroupMapListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



    }
}