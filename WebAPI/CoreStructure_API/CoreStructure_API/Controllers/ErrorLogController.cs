﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/ErrorLog")]
    [ApiController]
    public class ErrorLogController : ControllerBase
    {

        ErrorHandler objErrorRepo = new ErrorHandler();


        [HttpPost]
        [Route("InsertErrorLog")]
        [SessionAuthorizeFilter]
        public ActionResult InsertErrorLog(Exception_Error_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.created_by = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                string result = objErrorRepo.InsertLogError(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


    }
}