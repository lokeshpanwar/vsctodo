﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Theme")]
    [ApiController]
    public class ThemeController : ControllerBase
    {

        Theme_Repository objThemeRepo = new Theme_Repository();



        [HttpPost]
        [Route("AddTheme")]
        [SessionAuthorizeFilter]
        public ActionResult AddTheme(Theme_table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objThemeRepo.InsertTheme(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


    }
}