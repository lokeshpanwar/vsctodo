﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Form")]
    [ApiController]
    public class FormController : ControllerBase
    {
        Form_Repository objFormRepo = new Form_Repository();


        [HttpPost]
        [Route("NewForm")]
        [SessionAuthorizeFilter]
        public ActionResult NewForm(Form_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFormRepo.InsertForm(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpGet]
        [Route("ListForm")]
        [SessionAuthorizeFilter]
        public ActionResult ListForm()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objFormRepo.GetFormList(CreatedBy);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneForm")]
        [SessionAuthorizeFilter]
        public ActionResult OneForm(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objFormRepo.GetOneForm(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditForm")]
        [SessionAuthorizeFilter]
        public ActionResult EditForm(Form_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFormRepo.UpdateForm(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpDelete]
        [Route("DisableForm")]
        [SessionAuthorizeFilter]
        public ActionResult DisableForm(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFormRepo.DisableForm(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableForm")]
        [SessionAuthorizeFilter]
        public ActionResult EnableForm(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFormRepo.EnableForm(Id, Modified_ById);
            return Ok(new { results = result });

        }








        #region ListFormByPagination (GET)

        [HttpGet]
        [Route("ListFormByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListFormByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objFormRepo.GetFormListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


    }
}