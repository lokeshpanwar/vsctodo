﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/UserLogReports")]
    [ApiController]
    public class UserLogReportsController : ControllerBase
    {

        UserLogReports_Repository objRoleRepo = new UserLogReports_Repository();



        #region ListUserLogReports (GET)

        [HttpGet]
        [Route("ListUserLogReports")]
        [SessionAuthorizeFilter]
        public ActionResult ListUserLogReports(long UserId, DateTime FromDate, DateTime ToDate)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetUserLogReports(FromDate, ToDate, UserId, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}