﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/RecordFieldItem")]
    [ApiController]
    public class RecordFieldItemController : ControllerBase
    {
        RecordFieldItem_Repository objRecordFieldRepo = new RecordFieldItem_Repository();


        #region NewRecordFieldItem

        [HttpPost]
        [Route("NewRecordFieldItem")]
        [SessionAuthorizeFilter]
        public ActionResult NewRecordFieldItem(List_Item_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objRecordFieldRepo.InsertRecordFieldItem(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


        #region ListRecordFieldItem

        [HttpGet]
        [Route("ListRecordFieldItem")]
        [SessionAuthorizeFilter]
        public ActionResult ListRecordFieldItem(long ListId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objRecordFieldRepo.GetRecordFieldItemList(CreatedBy, ListId);
            return Ok(new { results = datamodel });
        }

        #endregion

        
        #region DeleteRecordFieldItem

        [HttpDelete]
        [Route("DeleteRecordFieldItem")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteRecordFieldItem(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objRecordFieldRepo.DeleteRecordFieldItem(Id, Deleted_ById);
            return Ok(new { results = objreturn });
        }

        #endregion








        #region ListRecordFieldItemFromMappedClassId

        [HttpGet]
        [Route("ListRecordFieldItemFromMappedClassId")]
        [SessionAuthorizeFilter]
        public ActionResult ListRecordFieldItemFromMappedClassId(long ClassId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objRecordFieldRepo.GetRecordFieldItemListFromMappedClassId(CreatedBy, ClassId);
            return Ok(new { results = datamodel });
        }

        #endregion


    }
}