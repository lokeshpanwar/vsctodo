﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Controllers
{
    [Route("api/Announcement")]
    [ApiController]
    public class AnnouncementController : ControllerBase
    {
        Announcement_Repository objAnncementRep = new Announcement_Repository();
        #region Insert Announcement

        [HttpPost]
        [Route("NewAnnouncement")]
        [SessionAuthorizeFilter]

        public ActionResult NewAnnouncement(AnnouncementTable_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnStatus = "Enter Mandatory Fields!";
            if(ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objAnncementRep.InsertAnnouncement(objModel);
                return Ok(new { results=objreturn});
            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion



        #region ListAnnouncementBy Pagination (GET)

        [HttpGet]
        [Route("ListAnnouncementByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListAnnouncementByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objAnncementRep.GetAnnouncementByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


        #region GetOne Announcement

        [HttpGet]
        [Route("GetOneAnnouncement")]
        [SessionAuthorizeFilter]

        public ActionResult GetOneAnnouncement(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objAnncementRep.GetOneAnnouncement(Id, CreatedBy);
                return Ok(new { results=result});
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Edit Announcement

        [HttpPut]
        [Route("EditAnnouncement")]
        [SessionAuthorizeFilter]

        public ActionResult EditAnnouncement(AnnouncementTable_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if(ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].ToString());
                objreturn = objAnncementRep.UpdateAnnouncement(objModel);
                return Ok(new { results=objreturn});
            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion

        #region Disble Announcement

        [HttpDelete]
        [Route("DisbleAnnouncement")]
        [SessionAuthorizeFilter]

        public ActionResult DisbleAnnouncement(long Id)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objAnncementRep.DisableAnnouncement(Id, DeletedBy);

            return Ok(new { results=result});
        }
        #endregion


        #region Enable Announcement

        [HttpDelete]
        [Route("EnableAnnouncement")]
        [SessionAuthorizeFilter]

        public ActionResult EnableAnnouncement(long Id)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";

            long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            objreturn = objAnncementRep.EnableAnnouncement(Id, DeletedBy);
            return Ok(new { results = objreturn });
        }
        #endregion



        #region ListAnnouncement BySubCompAndDomain (GET)

        [HttpGet]
        [Route("ListAnnouncementBySubCompAndDomain")]
        [SessionAuthorizeFilter]
        public ActionResult ListAnnouncementBySubCompAndDomain(long SubCompID, long Domain)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objAnncementRep.GetAnnouncementBySubCompAndDomain(SubCompID, Domain, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


    }
}