﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/MailDomain")]
    [ApiController]
    public class MailDomainController : ControllerBase
    {
        MailDomain_Repository objMailDomainRepo = new MailDomain_Repository();


        [HttpPost]
        [Route("NewMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult NewMailDomain(MailDomain_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMailDomainRepo.InsertMailDomain(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

                                 
        [HttpGet]
        [Route("OneMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult OneMailDomain(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objMailDomainRepo.GetOneMailDomain(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult EditMailDomain(MailDomain_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMailDomainRepo.UpdateMailDomain(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpDelete]
        [Route("DisableMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult DisableMailDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objMailDomainRepo.DisableMailDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult EnableMailDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objMailDomainRepo.EnableMailDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }








        #region ListMailDomainByPagination (GET)

        [HttpGet]
        [Route("ListMailDomainByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListMailDomainByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objMailDomainRepo.GetMailDomainListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion




        [HttpDelete]
        [Route("DeleteMailDomain")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteMailDomain(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objMailDomainRepo.DeleteMailDomain(Id, Modified_ById);
            return Ok(new { results = result });

        }


    }
}