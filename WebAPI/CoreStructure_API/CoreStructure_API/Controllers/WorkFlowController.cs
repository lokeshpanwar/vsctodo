﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/WorkFlow")]
    [ApiController]
    public class WorkFlowController : ControllerBase
    {
        WorkFlow_Repository objWorkFlowRepo = new WorkFlow_Repository();
        WorkFlowStage_Repository objWorkFlowStageRepo = new WorkFlowStage_Repository();


        #region NewWorkFlow (POST)

        [HttpPost]
        [Route("NewWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult NewWorkFlow(workflow_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objWorkFlowRepo.InsertWorkFlow(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion




        #region ListWorkFlowByPagination (GET)

        [HttpGet]
        [Route("ListWorkFlowByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListWorkFlowByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowRepo.GetWorkFlowListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region OneWorkFlow (GET)

        [HttpGet]
        [Route("OneWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult OneWorkFlow(long Id)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowRepo.GetOneWorkFlow(Id, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region EditWorkFlow (PUT)

        [HttpPut]
        [Route("EditWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult EditWorkFlow(workflow_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objWorkFlowRepo.UpdateWorkFlow(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region DisableWorkFlow (DELETE)

        [HttpDelete]
        [Route("DisableWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult DisableWorkFlow(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objWorkFlowRepo.DisableWorkFlow(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion

        #region EnableWorkFlow

        [HttpDelete]
        [Route("EnableWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult EnableWorkFlow(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objWorkFlowRepo.EnableWorkFlow(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion








        #region NewWorkFlowStage (POST)

        [HttpPost]
        [Route("NewWorkFlowStage")]
        [SessionAuthorizeFilter]
        public ActionResult NewWorkFlowStage(workflow_stage_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objReturn = objWorkFlowStageRepo.InsertWorkFlowStage(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region ListWorkFlowStageByWorkFlowId (GET)

        [HttpGet]
        [Route("ListWorkFlowStageByWorkFlowId")]
        [SessionAuthorizeFilter]
        public ActionResult ListWorkFlowStageByWorkFlowId(long WorkFlowId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowStageRepo.GetWorkFlowStageList(CreatedBy,0,WorkFlowId);
            return Ok(new { results = result });
        }

        #endregion

        

        #region DeleteWorkFlowStage (DELETE)

        [HttpDelete]
        [Route("DeleteWorkFlowStage")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteWorkFlowStage(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objWorkFlowStageRepo.DeleteWorkFlowStage(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion





        #region OneWorkFlowStage (GET)

        [HttpGet]
        [Route("OneWorkFlowStage")]
        [SessionAuthorizeFilter]
        public ActionResult OneWorkFlowStage(long Id)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowStageRepo.GetOneWorkFlowStage(CreatedBy, Id);
            return Ok(new { results = result });
        }

        #endregion





        #region UpdateViewRightWorkFlowStage (PUT)

        [HttpPut]
        [Route("UpdateViewRightWorkFlowStage")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateViewRightWorkFlowStage(workflowstage_user_right_List objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objWorkFlowStageRepo.UpdateViewRightForWorkFlowStage(objModel.workflowstage_user_right_list, createdBy);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion






        #region NewWorkFlowStagePriority (POST)

        [HttpPost]
        [Route("NewWorkFlowStagePriority")]
        [SessionAuthorizeFilter]
        public ActionResult NewWorkFlowStagePriority(workflow_priority_map_Model_One objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objWorkFlowStageRepo.InsertWorkFlowPriority(objModel.workflow_priority_map_Model_List, createdBy);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion





        #region ActivateWorkFlow (GET)

        [HttpGet]
        [Route("ActivateWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult ActivateWorkFlow(long Id)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowStageRepo.ActivateWorkFlowStage(Id, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion




        #region ParkWorkFlow (GET)

        [HttpGet]
        [Route("ParkWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult ParkWorkFlow(long Id)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowStageRepo.ParkWorkFlowStage(Id, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


        #region GetMaxProcessIDFromWorkFlow (GET)

        [HttpGet]
        [Route("GetMaxProcessIDFromWorkFlow")]
        [SessionAuthorizeFilter]
        public ActionResult GetMaxProcessIDFromWorkFlow()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objWorkFlowRepo.GetMaxProcessIDFromWorkFlow(CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

    }
}