﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace CoreStructure_API.Controllers
{
    [Route("api/OrganizationChat")]
    [ApiController]
    public class OrganizationChatController : ControllerBase
    {
        OrganizationChat_Repository objrep = new OrganizationChat_Repository();
        #region OrganizationChat (GET)

        [HttpGet]
        [Route("GetOrganizationChat")]
        [SessionAuthorizeFilter]
        public ActionResult GetOrganizationChat(string empnm)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objrep.GetOrganizationChat(CreatedBy, empnm);
            return Ok(new { results = result });
        }

        #endregion
    }
}