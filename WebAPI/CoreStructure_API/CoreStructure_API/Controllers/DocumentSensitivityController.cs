﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentSensitivity")]
    [ApiController]
    public class DocumentSensitivityController : ControllerBase
    {

        DocumentSensitivity_Repository objDocSensitivityRepo = new DocumentSensitivity_Repository();


        #region NewDocumentSensitivity (POST)

        [HttpPost]
        [Route("NewDocumentSensitivity")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocumentSensitivity(DocSensitivity_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objModel.Sub_CompID=GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objReturn = objDocSensitivityRepo.InsertDocumentSensitivity(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


        #region ListDocumentSensitivityByPagination (GET)

        [HttpGet]
        [Route("ListDocumentSensitivityByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentSensitivityByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocSensitivityRepo.GetDocumentSensitivityListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


        [HttpGet]
        [Route("OneDocumentSensitivity")]
        [SessionAuthorizeFilter]
        public ActionResult OneDocumentSensitivity(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDocSensitivityRepo.GetOneDocumentSensitivity(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPut]
        [Route("EditDocumentSensitivity")]
        [SessionAuthorizeFilter]
        public ActionResult EditDocumentSensitivity(DocSensitivity_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objModel.Sub_CompID = GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                objreturn = objDocSensitivityRepo.UpdateDocumentSensitivity(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }









        [HttpDelete]
        [Route("DeleteDocumentSensitivity")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteDocumentSensitivity(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocSensitivityRepo.DeleteDocumentSensitivity(Id, Deleted_ById);
            return Ok(new { results = objreturn });

        }



    }
}