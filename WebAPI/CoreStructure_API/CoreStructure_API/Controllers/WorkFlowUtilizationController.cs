﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/WorkFlowUtilization")]
    [ApiController]
    public class WorkFlowUtilizationController : ControllerBase
    {
        WorkFlowUtilization_Repository objWorkFlowRepo = new WorkFlowUtilization_Repository();

        #region GetWorkFlowThreadDataForReport (GET)

        [HttpGet]
        [Route("GetWorkFlowThreadDataForReport")]
        [SessionAuthorizeFilter]
        public ActionResult GetWorkFlowThreadDataForReport(long WorkFlowID, long ThreadID, long stageID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string UserRole = GlobalFunction.getLoggedInUserRoleName(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objWorkFlowRepo.GetWorkFlowData(CreatedBy, WorkFlowID, ThreadID, stageID, UserRole);
            return Ok(new { results = datamodel });
        }

        #endregion




        #region GetWorkFlowProcessDocumentListByClassId (GET)

        [HttpGet]
        [Route("GetWorkFlowProcessDocumentListByClassId")]
        [SessionAuthorizeFilter]
        public ActionResult GetWorkFlowProcessDocumentListByClassId(long DocClassId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objWorkFlowRepo.GetWorkFlowProcessDocumentListByClassId(CreatedBy, DocClassId);
            return Ok(new { results = datamodel });
        }

        #endregion




        #region GetWorkFlowThreadTracker (GET)

        [HttpGet]
        [Route("GetWorkFlowThreadTracker")]
        [SessionAuthorizeFilter]
        public ActionResult GetWorkFlowThreadTracker()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objWorkFlowRepo.GetWorkFlowThreadTracker(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion




        #region GetOneWorkFlowProgress (GET)

        [HttpGet]
        [Route("GetOneWorkFlowProgress")]
        [SessionAuthorizeFilter]
        public ActionResult GetOneWorkFlowProgress(long wfID, long thrdID, long StgID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objWorkFlowRepo.GetOneWorkFlowProgress(wfID, thrdID, StgID,CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion





        #region AddWorkFlowException (POST)

        [HttpPost]
        [Route("AddWorkFlowException")]
        [SessionAuthorizeFilter]
        public ActionResult AddWorkFlowException(workflow_progress_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objWorkFlowRepo.AddExceptionData(objModel, CreatedBy);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


    }
}