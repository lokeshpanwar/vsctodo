﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using static CoreStructure_API.Model.AuditManagment_Model;

namespace CoreStructure_API.Controllers
{
    [Route("api/AuditManagment")]
    [ApiController]
    public class AuditManagmentController : ControllerBase
    {
        AuditManagement_Repository objFrequencyRepo = new AuditManagement_Repository();

        private readonly IHostingEnvironment _hostingEnvironment;     

        public AuditManagmentController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }


      



        #region NewEmployee (POST)

        [HttpPost]
        [Route("NewFrequency")]
        [SessionAuthorizeFilter]
        public ActionResult NewFrequency(FrequencyModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                
                    //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                    //{
                        string result = objFrequencyRepo.InsertNewFrequency(objModel);
                        return Ok(new { results = result });
                    //}
                    //else
                    //{
                    //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                    //}
               
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("UpdateFrequency")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateFrequency(FrequencyModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateFrequency(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpDelete]
        [Route("DisableFrequency")]
        [SessionAuthorizeFilter]
        public ActionResult DisableFrequency(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableFrequency(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpDelete]
        [Route("DeleteFrequency")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteFrequency(int Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteFrequency(Id, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpGet]
        [Route("Frequencylist")]
        [SessionAuthorizeFilter]
        public ActionResult Frequencylist(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {
                
                List<FrequencyModel> result = new List<FrequencyModel>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                 result = objFrequencyRepo.GetFrequencyList(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        #endregion

        [HttpPost]
        [Route("NewClient")]
        [SessionAuthorizeFilter]
        public ActionResult NewClient(CompanyDetails objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatebyC = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.InsertNewClient(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("Clientlist")]
        [SessionAuthorizeFilter]
        public ActionResult Clientlist(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                List<CompanyDetails> result = new List<CompanyDetails>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetClientlist(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("GetDateCalculation")]
        [SessionAuthorizeFilter]
        public ActionResult GetDateCalculation(int frequencyid,string Datefrom, string Dateto)
        {
            if (ModelState.IsValid)
            {

                List<DatesRange> result = new List<DatesRange>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetDateCalculation(frequencyid, Datefrom, Dateto,CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpPost]
        [Route("UpdateClient")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateClient(CompanyDetails objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatebyC = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateClient(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("DeleteClient")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteClient(int Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteClient(Id, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpDelete]
        [Route("DisableClient")]
        [SessionAuthorizeFilter]
        public ActionResult DisableClient(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableClient(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpPost]
        [Route("NewTeam")]
        [SessionAuthorizeFilter]
        public ActionResult NewTeam(TeamModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.TeamCreatedby = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.NewTeam(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpPost]
        [Route("AddNewGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult AddNewGroupMapping(AuditGroupMapping_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.AuditCreatedby = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{

                string result = objFrequencyRepo.AddNewGroupMapping(objModel);
                return Ok(new { results = result });
                
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("AddAuditUserMapping")]
        [SessionAuthorizeFilter]
        public ActionResult AddAuditUserMapping(AuditUserMapping_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.AddAuditUserMapping(objModel, createdBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("AddAuditScheduling")]
        [SessionAuthorizeFilter]
        public ActionResult AddAuditScheduling(AuditScheduling_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.AddAuditScheduling(objModel, createdBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("AddAuditNotification")]
        [SessionAuthorizeFilter]
        public ActionResult AddAuditNotification(long AuditTransationId, long UserId, string Message)
        {
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.AddAuditNotification( AuditTransationId,  UserId,  Message, createdBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Teamlist")]
        [SessionAuthorizeFilter]
        public ActionResult Teamlist()
        {
            if (ModelState.IsValid)
            {

                List<TeamModel> result = new List<TeamModel>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetTeamlist(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("LocationDetailsList")]
        [SessionAuthorizeFilter]
        public ActionResult LocationDetailsList(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                List<LocationsModel> result = new List<LocationsModel>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.LocationDetailsList(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("UpdateLocation")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateLocation(LocationsModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.createbyclient = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateLocation(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("UpdateTeam")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateTeam(TeamModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.TeamCreatedby = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateTeam(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpDelete]
        [Route("DeleteTeam")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteTeam(int Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteTeam(Id, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpDelete]
        [Route("DisableEnableTeam")]
        [SessionAuthorizeFilter]
        public ActionResult DisableEnableTeam(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableEnableTeam(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpPost]
        [Route("NewLocation")]
        [SessionAuthorizeFilter]
        public ActionResult NewLocation(LocationsModel objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.createbyclient = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.NewLocation(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("DeleteLocation")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteLocation(int Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteLocation(Id, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpDelete]
        [Route("DisableLocation")]
        [SessionAuthorizeFilter]
        public ActionResult DisableLocation(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableLocation(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }




        [HttpPost]
        [Route("AuditTeamSave")]
        [SessionAuthorizeFilter]
        public ActionResult AuditTeamSave(Audit_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.AuditTeamSave(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("AuditTeampDetails")]
        [SessionAuthorizeFilter]
        public ActionResult AuditTeampDetails(Audit_Manage objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.AudCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.AuditTeampDetails(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("UpdateGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateGroupMapping(AuditGroupMapping_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long AuditModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFrequencyRepo.UpdateGroupMapping(objModel, AuditModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("UpdateAuditScheduling")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateAuditScheduling(AuditScheduling_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long AuditModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFrequencyRepo.UpdateAuditScheduling(objModel, AuditModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("UpdateAuditUserMapping")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateAuditUserMapping(AuditUserMapping_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long AuditModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFrequencyRepo.UpdateAuditUserMapping(objModel, AuditModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("UpdateAuditTemplate")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateAuditTemplate(AuditData_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long AuditModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFrequencyRepo.UpdateAuditTemplate(objModel, AuditModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("UpdateAuditTemplatesDetails")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateAuditTemplatesDetails(Audit_Manage objModel)
        {
            if (ModelState.IsValid)
            {
                long AuditModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objFrequencyRepo.UpdateAuditTemplatesDetails(objModel, AuditModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetAuditManagmentList")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditManagmentList(int auditid,int scheduleid,int userid)
        {
            if (ModelState.IsValid)
            {

                List<Audit_Model> result = new List<Audit_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditManagmentList(CreatedBy, auditid, scheduleid,userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAuditTemplateList")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditTemplateList(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                AuditData_Model_Table result = new AuditData_Model_Table();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditTemplateList(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAuditSchedulingList")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditSchedulingList(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                AuditScheduling_Model_Table result = new AuditScheduling_Model_Table();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditSchedulingList(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("AuditRegisterList")]
        [SessionAuthorizeFilter]
        public ActionResult AuditRegisterList(int max, int page,string option,bool isAuditor, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                AuditRegister_Model_Table result = new AuditRegister_Model_Table();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.AuditRegisterList(max, page,option,isAuditor, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetAuditTemplateById")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditTemplateById(int id)
        {
            if (ModelState.IsValid)
            {

                List<AuditData_Model> result = new List<AuditData_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditTemplateById(id);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GroupMappingById")]
        [SessionAuthorizeFilter]
        public ActionResult GroupMappingById(int id)
        {
            if (ModelState.IsValid)
            {

                List<AuditGroupMapping_Model> result = new List<AuditGroupMapping_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.AuditGroupMappingById(id, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAuditUserMappingById")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditUserMappingById(long id)
        {
            if (ModelState.IsValid)
            {

                List<AuditUserMapping_Model> result = new List<AuditUserMapping_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditUserMappingById(id, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAuditSchedulingById")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditSchedulingById(long id)
        {
            if (ModelState.IsValid)
            {

                List<AuditScheduling_Model> result = new List<AuditScheduling_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditSchedulingById(id, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        [HttpGet]
        [Route("GetAuditManageListById")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditManageListById(int Id)
        {
            if (ModelState.IsValid)
            {

                List<Audit_Model> result = new List<Audit_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditManageListById(Id,CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAuditTemplateDetailsListById")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditTemplateDetailsListById(int Id)
        {
            if (ModelState.IsValid)
            {

                List<Audit_Manage> result = new List<Audit_Manage>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditTemplateDetailsListById(Id);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("DeleteAuditClient")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteAuditClient(int Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteAuditClient(Id, Modified_ById);
            return Ok(new { results = result });

        }

        [HttpDelete]
        [Route("DeleteAuditTemplateDetails")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteAuditTemplateDetails(int Id)
        {
            long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
           
            string result = objFrequencyRepo.DeleteAuditTemplateDetails(Id, DeletedBy);
            return Ok(new { results = result });

        }

        [HttpDelete]
        [Route("DeleteGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteGroupMapping(int Id)
        {
            long Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            long Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteGroupMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }

        [HttpDelete]
        [Route("DeleteAuditScheduling")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteAuditScheduling(long Id)
        {
            long Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            long Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DeleteAuditScheduling(Id, Modified_ById);
            return Ok(new { results = result });

        }

        [HttpDelete]
        [Route("DisableEnableAudit")]
        [SessionAuthorizeFilter]
        public ActionResult DisableEnableAudit(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableEnableAudit(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }


        [HttpDelete]
        [Route("DisableGroupMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DisableGroupMapping(int Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableGroupMapping(Id, Type, Modified_ById);
            return Ok(new { results = result });
        }

        [HttpDelete]
        [Route("DisableAuditScheduling")]
        [SessionAuthorizeFilter]
        public ActionResult DisableAuditScheduling(long Id, int Type)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objFrequencyRepo.DisableAuditScheduling(Id, Type, Modified_ById);
            return Ok(new { results = result });

        }

        [HttpGet]
        [Route("GetAuditScheduleByFrequency")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditScheduleByFrequency(long groupMaapingId, long frequencyId)
        {
            if (ModelState.IsValid)
            {

                List<AuditScheduling_Model> result = new List<AuditScheduling_Model>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditScheduleByFrequency(groupMaapingId,frequencyId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GroupMappingdetails")]
        [SessionAuthorizeFilter]
        public ActionResult GroupMappingdetails(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                AuditGroupMapping_Model_table result = new AuditGroupMapping_Model_table();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GroupMappingdetails(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GroupMappingdetailsNew")]
        [SessionAuthorizeFilter]
        public ActionResult GroupMappingdetailsNew(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            if (ModelState.IsValid)
            {

                AuditGroupMapping_Model_table result = new AuditGroupMapping_Model_table();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GroupMappingdetailsNew(max, page, search, sort_col, sort_dir, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpPost]
        [Route("UpdateAuditTransaction")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateAuditTransaction(Audit_Trans objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateAuditTransaction(objModel);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpDelete]
        [Route("UpdateStage1Data")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateStage1Data(int Auditdetailid, int userId, string userName, string st1comment)
        {
            if (ModelState.IsValid)
            {
                int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateStage1Data(Auditdetailid, userId,userName, st1comment, AuditCreatedBy);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpDelete]
        [Route("UpdateStage2Data")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateStage2Data(int Auditdetailid, string ArefactRef, string st2comment, int userId)
        {
            if (ModelState.IsValid)
            {
                int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateStage2Data( Auditdetailid,  ArefactRef,  st2comment,  userId);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpDelete]
        [Route("UpdateStage3Data")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateStage3Data(int Auditdetailid, int NyStatus, string Observation, string Priority, string Recommendation )
        {
            if (ModelState.IsValid)
            {
                int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                //if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                //{
                string result = objFrequencyRepo.UpdateStage3Data(Auditdetailid, NyStatus, Observation, Priority, Recommendation, AuditCreatedBy);
                return Ok(new { results = result });
                //}
                //else
                //{
                //    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                //}

            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpDelete]
        [Route("UpdateStage4NewData")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateStage4NewData(int Auditdetailid, string RefDocClose)
        {
            if (ModelState.IsValid)
            {
                int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.UpdateStage4NewData(Auditdetailid, RefDocClose, AuditCreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("UpdateStage4Data")]
        [SessionAuthorizeFilter]
        public ActionResult UpdateStage4Data(int Auditdetailid, string Response, string EstDateofClosure, int Status)
        {
            if (ModelState.IsValid)
            {
                int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.UpdateStage4Data(Auditdetailid, Response, EstDateofClosure, Status, AuditCreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        //[Route("UpdateStage5Data")]
        //[SessionAuthorizeFilter]
        //public ActionResult UpdateStage5Data(int Auditdetailid, int st5status, string st5comment, int Status)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        int AuditCreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

        //        string result = objFrequencyRepo.UpdateStage5Data(Auditdetailid, st5status, st5comment, AuditCreatedBy);
        //        return Ok(new { results = result });
        //    }
        //    else
        //    {
        //        return Ok(new { results = "Enter Mandatory Fields!" });
        //    }
        //}


        [HttpGet]
        [Route("GetAuditAdminDashboard")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditAdminDashboard()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var datamodel = objFrequencyRepo.GetAuditAdminDashboard(CreatedBy);
            return Ok(new { results = datamodel });
        }

        [HttpGet]
        [Route("GetAuditUserDashboard")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditUserDashboard(bool isAuditor)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var datamodel = objFrequencyRepo.GetAuditUserDashboard(CreatedBy,isAuditor);
            return Ok(new { results = datamodel });
        }

        #region Audit Messages Added By Vijendra

        [HttpPost]
        [Route("SaveAuditMessage")]
        [SessionAuthorizeFilter]
        public ActionResult SaveAuditMessage(Audit_Conversation objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.SenderId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objFrequencyRepo.SaveAuditMessage(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetAuditMessageByAuditId")]
        [SessionAuthorizeFilter]
        public ActionResult GetAuditMessageByAuditId(int AuditId)
        {
            if (ModelState.IsValid)
            {

                List<Audit_Conversation> result = new List<Audit_Conversation>();

                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objFrequencyRepo.GetAuditMessageByAuditId(AuditId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        //private string GetSortCol(string SortCol)
        //{
        //    if (SortCol == null)
        //    {
        //        SortCol = "";
        //    }
        //    if (SortCol != "")
        //    {
        //        ViewData["SortCol"] = SortCol;
        //    }
        //    else
        //    {
        //        if (ViewData["SortCol"] != null)
        //        {
        //            SortCol = TempData["SortCol"].ToString();
        //            ViewData["SortCol"] = SortCol;
        //        }
        //    }

        //    return SortCol;
        //}

        //private string GetSortOrder(string SortOrder)
        //{
        //    if (SortOrder == null)
        //    {
        //        SortOrder = "";
        //    }
        //    if (SortOrder != "")
        //    {
        //        if (TempData["SortOrder"] != null)
        //        {
        //            string oSortOrder = TempData["SortOrder"].ToString();
        //            if (oSortOrder != null)
        //            {
        //                SortOrder = oSortOrder == "asc" ? "desc" : "asc";
        //            }
        //        }
        //        TempData["SortOrder"] = SortOrder;
        //    }
        //    else
        //    {
        //        if (TempData["SortOrder"] != null)
        //        {
        //            SortOrder = TempData["SortOrder"].ToString();
        //            TempData["SortOrder"] = SortOrder;

        //        }
        //        else
        //        {
        //            SortOrder = "asc";
        //            TempData["SortOrder"] = SortOrder;
        //        }
        //    }

        //    return SortOrder;
        //}


    }

}