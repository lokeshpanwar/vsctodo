﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Dashboard")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        Dashboard_Repository objDashboardRepo = new Dashboard_Repository();

        WorkFlow_Repository objWorkFlowRepo = new WorkFlow_Repository();


        //private readonly IHostingEnvironment _hostingEnvironment;

        //public DashboardController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}


        #region GetBPMFormsTriggerList (GET)

        [HttpGet]
        [Route("GetBPMFormsTriggerList")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMFormsTriggerList()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            long DomainID = GlobalFunction.getLoggedInUserDomainId(HttpContext.Request.Headers["Authorization-Token"].First());
            long Sub_CompID = GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());

            var datamodel = objDashboardRepo.GetBPMFormsTriggerList(CreatedBy, DomainID,Sub_CompID);
            return Ok(new { results = datamodel });
        }

        #endregion

        #region GetBPMOpenTabList (GET)

        [HttpGet]
        [Route("GetBPMOpenTabList")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMOpenTabList()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMOpenTabList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion

        #region GetBPMWIPTabList (GET)

        [HttpGet]
        [Route("GetBPMWIPTabList")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMWIPTabList()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMWIPTabList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion


        #region GetBPMClosedTabList (GET)

        [HttpGet]
        [Route("GetBPMClosedTabList")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMClosedTabList()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMClosedTabList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion


        #region GetBPMExceptionTabList (GET)

        [HttpGet]
        [Route("GetBPMExceptionTabList")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMExceptionTabList()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMExceptionTabList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion

        #region InsertProgressTableFromStageTbl (Get)

        [HttpGet]
        [Route("InsertProgressTableFromStageTbl")]
        [SessionAuthorizeFilter]
        public ActionResult InsertProgressTableFromStageTbl(string workFlowIDl, string PriID, long UID, string ThrdRef)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long createdBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objReturn = objDashboardRepo.InsertProgressTableFromStageTbl(workFlowIDl, PriID, UID, ThrdRef, createdBy);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


        #region InsertworkflowProgress
        [HttpPost]
        [Route("InsertworkflowProgress")]
        [SessionAuthorizeFilter]
        public ActionResult InsertworkflowProgress(workflow_progress_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            try
            {

                if (ModelState.IsValid)
                {
                    foreach(var docdata in objModel.workflow_process_docs_Model_List)
                    {
                        docdata.DomainID = GlobalFunction.getLoggedInUserDomainId(HttpContext.Request.Headers["Authorization-Token"].First());
                        docdata.Sub_CompID = GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());                        
                    }

                    objModel.submittedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()).ToString();
                    objreturn = objDashboardRepo.InsertworkflowProgress(objModel);
                    return (Ok(new { results = objreturn }));

                }

                else
                {
                    return (Ok(new { results = objreturn }));
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "Web API", "DashboardController/InsertworkflowProgress");
                objreturn.ReturnMessage = ResponseStatus.SYSTEM_ERROR;
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        // Refcode is inserted to workflow_thread_progress_table Column name is ThrdRef


        #region GetOneworkflowStage (GET)

        [HttpGet]
        [Route("GetOneworkflowStage")]
        [SessionAuthorizeFilter]
        public ActionResult GetOneworkflowStage(long SLNO, long stageOrderID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDashboardRepo.GetOneworkflowStage(SLNO, CreatedBy, stageOrderID);
            result.workflow_table_Model = objWorkFlowRepo.GetOneWorkFlow(result.workFlowID, CreatedBy);

            return Ok(new { results = result });
        }

        #endregion



        #region MakeTabShifter (GET)

        [HttpGet]
        [Route("MakeTabShifter")]
        [SessionAuthorizeFilter]
        public ActionResult MakeTabShifter(string action, long wfid, long thrdID, string order, string orderNext, string key)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDashboardRepo.MakeTabShifter(action, wfid, thrdID, order, orderNext, key);
            //result.workflow_table_Model = objWorkFlowRepo.GetOneWorkFlow(result.workFlowID, CreatedBy);

            return Ok(new { results = result });
        }

        #endregion


        #region GetNextProcessStatus (GET)

        [HttpGet]
        [Route("GetNextProcessStatus")]
        [SessionAuthorizeFilter]
        public ActionResult GetNextProcessStatus(int Order, long BPM_ThrdID, long BPMID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDashboardRepo.GetNextProcessStatus(Order, BPM_ThrdID, BPMID);


            return Ok(new { results = result });
        }

        #endregion




        #region GetOneWorkFlowStageForOneThread (GET)

        [HttpGet]
        [Route("GetOneWorkFlowStageForOneThread")]
        [SessionAuthorizeFilter]
        public ActionResult GetOneWorkFlowStageForOneThread(long SLNO, long ThreadId, long StageOrderId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDashboardRepo.GetOneWorkFlowStageForOneThread(SLNO, ThreadId, StageOrderId, CreatedBy);
            result.workflow_table_Model = objWorkFlowRepo.GetOneWorkFlow(result.workFlowID, CreatedBy);

            return Ok(new { results = result });
        }

        #endregion



        #region GetDashboardPieChartDataForBPM (GET)

        [HttpGet]
        [Route("GetDashboardPieChartDataForBPM")]
        [SessionAuthorizeFilter]
        public ActionResult GetDashboardPieChartDataForBPM()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDashboardRepo.GetDashboardPieChartDataForBPM(CreatedBy);
            //result.workflow_table_Model = objWorkFlowRepo.GetOneWorkFlow(result.workFlowID, CreatedBy);

            return Ok(new { results = result });
        }

        #endregion




        #region GetBPMClosedFormHistory (GET)

        [HttpGet]
        [Route("GetBPMClosedFormHistory")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMClosedFormHistory()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMClosedFormList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion




        #region GetBPMOpenFormHistory (GET)

        [HttpGet]
        [Route("GetBPMOpenFormHistory")]
        [SessionAuthorizeFilter]
        public ActionResult GetBPMOpenFormHistory()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetBPMOpenFormList(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion





        #region GetDashboardWidget (GET)

        [HttpGet]
        [Route("GetDashboardWidget")]
        [SessionAuthorizeFilter]
        public ActionResult GetDashboardWidget(long SubCompId, long DomainID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDashboardRepo.GetDashboardWidgetData(SubCompId, DomainID,CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion






    }
}