﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Attorneys")]
    [ApiController]
    public class AttorneysController : Controller
    {
        Attornyes_Repository objQuery = new Attornyes_Repository();


        [HttpGet]
        [Route("Clientlist")]
        [SessionAuthorizeFilter]
        public IActionResult Clientlist(int officeid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Clientlist(officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Activity")]
        [SessionAuthorizeFilter]
        public IActionResult Activity(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCActivity(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("SubActivity")]
        [SessionAuthorizeFilter]
        public IActionResult SubActivity(int OfficeId, int Activityid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCSubACtivity(OfficeId, Activityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("Userlist")]
        [SessionAuthorizeFilter]
        public IActionResult Userlist(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCActivity(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("Filltimesheet")]
        [SessionAuthorizeFilter]
        public IActionResult Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid, int projectid, int todoid, int StatusId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Filltimesheet(empid, date, time, description, clientid, activityid, subactivityid, projectid, todoid, StatusId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Projectname")]
        [SessionAuthorizeFilter]
        public IActionResult Projectname(int Clientid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Projectname(Clientid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("AttorneysUserData")]
        [SessionAuthorizeFilter]
        public IActionResult AttorneysUserData(int UserId)
        {

            if (ModelState.IsValid)
            {
                var result = objQuery.AttorneysUserData(UserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Category")]
        [SessionAuthorizeFilter]
        public IActionResult Category(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Category(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("SubCategory")]
        [SessionAuthorizeFilter]
        public IActionResult SubCategory(int OfficeId, int Categoryid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.SubCategory(OfficeId, Categoryid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
    }
}
