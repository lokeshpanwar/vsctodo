﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/CompanyHoliday")]
    [ApiController]
    public class CompanyHolidayController : ControllerBase
    {
        CompanyHoliday_Repository objEvetMstRep = new CompanyHoliday_Repository();

        #region New CompanyHoliday
        [HttpPost]
        [Route("NewCompanyHoliday")]
        [SessionAuthorizeFilter]
        public ActionResult NewCompanyHoliday(CompanyHoliday_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                //objModel.Empid = Convert.ToInt32(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()));
                objreturn = objEvetMstRep.InsertCompanyHoliday(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One CompanyHoliday
        [HttpGet]
        [Route("OneCompanyHoliday")]
        [SessionAuthorizeFilter]

        public ActionResult OneCompanyHoliday(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneCompanyHoliday(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit CompanyHoliday

        [HttpPut]
        [Route("EditCompanyHoliday")]
        [SessionAuthorizeFilter]

        public ActionResult EditCompanyHoliday(CompanyHoliday_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
               // objModel.Empid = Convert.ToInt32(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First()));
                objreturn = objEvetMstRep.UpdateCompanyHoliday(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion


        #region Disable CompanyHoliday

        [HttpDelete]
        [Route("DisableCompanyHoliday")]
        [SessionAuthorizeFilter]

        public ActionResult DisableCompanyHoliday(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableCompanyHoliday(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region Enable CompanyHoliday
        [HttpDelete]
        [Route("EnableCompanyHoliday")]
        [SessionAuthorizeFilter]
        public ActionResult EnableCompanyHoliday(long Id)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn.ReturnStatus = "Error";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.EnableCompanyHoliday(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }

            else
            {
                return Ok(new { results = objreturn });

            }
        }
        #endregion


        #region ListCompanyHoliday By pagination

        [HttpGet]
        [Route("ListCompanyHolidayBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListCompanyHolidayBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetCompanyHolidayByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion
    }
}