﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentSet")]
    [ApiController]
    public class DocumentSetController : ControllerBase
    {

        DocumentSet_Repository objDocSetRepo = new DocumentSet_Repository();



        #region ListDocumentClassTypeFile (GET)

        [HttpGet]
        [Route("ListDocumentClassTypeFile")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentClassTypeFile(long SubCompId, long DomainId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocSetRepo.GetDocumentClassTypeFileList(SubCompId, DomainId, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region NewDocumentSet (POST)

        [HttpPost]
        [Route("NewDocumentSet")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocumentSet(Document_Set_master_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocSetRepo.InsertDocumentSet(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion




        #region ListDocumentSetByPagination (GET)

        [HttpGet]
        [Route("ListDocumentSetByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentSetByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocSetRepo.GetDocumentSetListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region OneDocumentSet (GET)

        [HttpGet]
        [Route("OneDocumentSet")]
        [SessionAuthorizeFilter]
        public ActionResult OneDocumentSet(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDocSetRepo.GetOneDocumentSet(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion



        #region DisableDocumentSet

        [HttpDelete]
        [Route("DisableDocumentSet")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocumentSet(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocSetRepo.DisableDocumentSet(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion

        #region EnableDocumentSet

        [HttpDelete]
        [Route("EnableDocumentSet")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocumentSet(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocSetRepo.EnableDocumentSet(Id, Modified_ById);
            return Ok(new { results = result });

        }


        #endregion





        #region ListDocumentSetPermission (GET)

        [HttpGet]
        [Route("ListDocumentSetPermission")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentSetPermission(long SubCompId, long DomainId, long DocSetID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocSetRepo.GetDoucmentSetPermissionList(SubCompId, DomainId, DocSetID, CreatedBy);
            Document_Set_permission_Model objModel = new Document_Set_permission_Model();
            objModel.DocSetPermission_List = result;
            return Ok(new { results = objModel });
        }

        #endregion



        #region NewDocumentSetPermission (POST)

        [HttpPost]
        [Route("NewDocumentSetPermission")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocumentSetPermission(Document_Set_permission_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocSetRepo.InsertDocumentSetPermission(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion





        #region ListDocumentSetForDashboard (GET)

        [HttpGet]
        [Route("ListDocumentSetForDashboard")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentSetForDashboard()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocSetRepo.GetDocumentSetListForDashboard(CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



    }
}