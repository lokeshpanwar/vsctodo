﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/TimeSheet")]
    [ApiController]
    public class TimeSheetController : Controller
    {
        Timesheet_Repository objTimeRepo = new Timesheet_Repository();

        #region FillTimesheet
        [HttpGet]
        [Route("Getfilledtimesheetdata")]
        [SessionAuthorizeFilter]
        public ActionResult Getfilledtimesheetdata()
        {
            if (ModelState.IsValid)
            {
                FilledTimesheetData result = new FilledTimesheetData();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.Getfilledtimesheetdata(LoggedInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetfilledtimesheetdataByDate")]
        [SessionAuthorizeFilter]
        public ActionResult GetfilledtimesheetdataByDate(string Date)
        {
            if (ModelState.IsValid)
            {
                CalFilledTimeSheet result = new CalFilledTimeSheet();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetfilledtimesheetdataByDate(LoggedInUser, Date);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveTimesheet")]
        [SessionAuthorizeFilter]
        public IActionResult SaveTimesheet(Filltimesheet_Model model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.SaveTimesheet(model);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetFillTimesheetGridData")]
        [SessionAuthorizeFilter]
        public ActionResult GetFillTimesheetGridData(string Date)
        {
            if (ModelState.IsValid)
            {
                List<TimesheetGrid> result = new List<TimesheetGrid>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetFillTimesheetGridData(Date, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteSheetData")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteSheetData(long SheetId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.DeleteSheetData(SheetId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("SubmitTimesheetCasual")]
        [SessionAuthorizeFilter]
        public IActionResult SubmitTimesheetCasual(string Date)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objTimeRepo.SubmitTimesheetCasual(Date, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion


        #region View TimeSheet

        [HttpGet]
        [Route("GetTimesheetSelfData")]
        [SessionAuthorizeFilter]
        public ActionResult GetTimesheetSelfData(string FromDate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                SelfTimeSheet result = new SelfTimeSheet();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetTimesheetSelfData(LoggedInUser, FromDate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region View Timesheet User
        [HttpGet]
        [Route("GetAdminTimesheetView")]
        [SessionAuthorizeFilter]
        public ActionResult GetAdminTimesheetView(int OfficeId, string FromDate, string ToDate, int ClientId, int EmployeeId, int ActivityId, int SubActivityId, int RoleId)
        {
            if (ModelState.IsValid)
            {
                AdminTimeSheetOfUsers result = new AdminTimeSheetOfUsers();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetAdminTimesheetView(OfficeId, FromDate, ToDate, ClientId, EmployeeId, ActivityId, SubActivityId, RoleId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Delete TimeSheet

        [HttpGet]
        [Route("GetTimesheetDataTobeDelete")]
        [SessionAuthorizeFilter]
        public ActionResult GetTimesheetDataTobeDelete(long EmployeeId, string Date)
        {
            if (ModelState.IsValid)
            {
                SelfTimeSheet result = new SelfTimeSheet();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetTimesheetDataTobeDelete(LoggedInUser, EmployeeId, Date);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetTimesheetDataDelete")]
        [SessionAuthorizeFilter]
        public IActionResult GetTimesheetDataDelete(long EmployeeId, string Date)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.GetTimesheetDataDelete(logInUser, EmployeeId, Date);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region Apply Leave
        [HttpGet]
        [Route("ApplyLeaveValidation")]
        [SessionAuthorizeFilter]
        public IActionResult ApplyLeaveValidation(string Date)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.ApplyLeaveValidation(logInUser, Date);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetHoldayName")]
        [SessionAuthorizeFilter]
        public IActionResult GetHoldayName(string Date)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.GetHoldayName(logInUser, Date);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAllLeaveApplyDetails")]
        [SessionAuthorizeFilter]
        public ActionResult GetAllLeaveApplyDetails(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<LeaveData> result = new List<LeaveData>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetAllLeaveApplyDetails(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveLeaveRequest")]
        [SessionAuthorizeFilter]
        public IActionResult SaveLeaveRequest(Leave_Model model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.SaveLeaveRequest(model);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetLeaveDetailsForEdit")]
        [SessionAuthorizeFilter]
        public ActionResult GetLeaveDetailsForEdit(int OfficeId, long LId)
        {
            if (ModelState.IsValid)
            {
                List<LeaveData> result = new List<LeaveData>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetLeaveDetailsForEdit(OfficeId, LId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteLeaveRequestByID")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteLeaveRequestByID(long LId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.DeleteLeaveRequestByID(logInUser, LId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Mark Leave
        [HttpGet]
        [Route("GetAllMarkLeaveDetails")]
        [SessionAuthorizeFilter]
        public ActionResult GetAllMarkLeaveDetails(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<LeaveData> result = new List<LeaveData>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetAllMarkLeaveDetails(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveMarkLeave")]
        [SessionAuthorizeFilter]
        public IActionResult SaveMarkLeave(Leave_Model model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.SaveMarkLeave(model);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Verify Leave
        [HttpGet]
        [Route("GetAllLeaveToVerifyDetails")]
        [SessionAuthorizeFilter]
        public ActionResult GetAllLeaveToVerifyDetails(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<LeaveData> result = new List<LeaveData>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetAllLeaveToVerifyDetails(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("ApproveLeaveRequest")]
        [SessionAuthorizeFilter]
        public IActionResult ApproveLeaveRequest(long LId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.ApproveLeaveRequest(logInUser, LId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("RejectLeaveRequest")]
        [SessionAuthorizeFilter]
        public IActionResult RejectLeaveRequest(long LId, string Remarks)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.RejectLeaveRequest(logInUser, LId, Remarks);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region HR Report Sheet
        [HttpGet]
        [Route("GetHrSheet")]
        [SessionAuthorizeFilter]
        public IActionResult GetHrSheet(long ClientId, string FromDate, string ToDate)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objTimeRepo.GetHrSheet(logInUser, ClientId, FromDate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region MIS Report

        [HttpGet]
        [Route("GetHRsheetGraphEmployeewise")]
        [SessionAuthorizeFilter]
        public ActionResult GetHRsheetGraphEmployeewise(int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                MISReport_Model result = new MISReport_Model();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetHRsheetGraphEmployeewise(LoggedInUser, OfficeId, ClientId, FromDate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetHRsheetGraphClientwise")]
        [SessionAuthorizeFilter]
        public ActionResult GetHRsheetGraphClientwise(int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            if (ModelState.IsValid)
            {
                MISReport_Model result = new MISReport_Model();
                long LoggedInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objTimeRepo.GetHRsheetGraphClientwise(LoggedInUser, OfficeId, ClientId, FromDate, ToDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}