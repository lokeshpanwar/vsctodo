﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        ToDo_Repository objDD_Repo = new ToDo_Repository();
        Attornyes_Repository objattorney = new Attornyes_Repository();
        #region GetPriorityList

        [HttpGet]
        [Route("GetPriorityList")]
        [SessionAuthorizeFilter]
        public ActionResult GetPriorityList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetPriorityList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetStatuslist")]
        [SessionAuthorizeFilter]
        public ActionResult GetStatuslist()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetStatuslist(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("VerticalList")]
        [SessionAuthorizeFilter]
        public ActionResult VerticalList(int UserId, int VerticalId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.VerticalList(CreatedBy, UserId, VerticalId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("VerticalListUserwise")]
        [SessionAuthorizeFilter]
        public ActionResult VerticalListUserwise(int UserId, int VerticalId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.VerticalListUserwise(CreatedBy, UserId, VerticalId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        #endregion

        #region SaveTask
        [HttpPost]
        [Route("SaveTask")]
        [SessionAuthorizeFilter]
        public IActionResult SaveTask(Task_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreateBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                string result = objDD_Repo.SaveTask(objModel);
                if (objModel.Verticals == 5)
                {
                    string result1 = objattorney.savequery(objModel,Convert.ToInt32(result));
                }
                
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetViewTaskList")]
        [SessionAuthorizeFilter]
        public ActionResult GetViewTaskList(int UserId)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.GetViewTaskList(UserId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetEditTaskList")]
        [SessionAuthorizeFilter]
        public ActionResult GetEditTaskList(int UserId, int TaskId)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.GetEditTaskList(UserId, CreatedBy, TaskId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetEditAssignTaskList")]
        [SessionAuthorizeFilter]
        public ActionResult GetEditAssignTaskList(int UserId, int TaskId)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.GetEditAssignTaskList(UserId, CreatedBy, TaskId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteTask")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteTask(int TaskId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDD_Repo.DeleteTask(TaskId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion


        #region GetTodoList
        [HttpGet]
        [Route("GetTodoList")]
        [SessionAuthorizeFilter]
        public ActionResult GetTodoList(int UserId, int VerticalId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetTodoList(CreatedBy, UserId, VerticalId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion


        #region GetTodoDetailList
        [HttpGet]
        [Route("GetTodoDetailList")]
        [SessionAuthorizeFilter]
        public ActionResult GetTodoDetailList(int Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetTodoDetailList(CreatedBy, Id);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAlltaskdetails")]
        [SessionAuthorizeFilter]
        public ActionResult GetAlltaskdetails(int Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetAlltaskdetails(CreatedBy, Id);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        #endregion



        #region TODOAction


        [HttpGet]
        [Route("Updatetodo")]
        [SessionAuthorizeFilter]
        public IActionResult Updatetodo(int todoid, string date, string time, int statusId, string description, int empid)
        {
            long logInUser = 0;
            List<Task_Model> objList = new List<Task_Model>();

            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            objList = objDD_Repo.Updatetodo(todoid, date, time, statusId, description, empid, logInUser);

            return Ok(new { results = objList });

        }


        [HttpGet]
        [Route("AcceptTask")]
        [SessionAuthorizeFilter]
        public IActionResult AcceptTask(int todoid, int Clientid, int Activityid, int Subactivityid, int projectid, int moduleid, int vertical)
        {
            long logInUser = 0;
            List<Task_Model> objList = new List<Task_Model>();
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            objList = objDD_Repo.AcceptTask(todoid, Clientid, Activityid, Subactivityid, projectid, moduleid, logInUser);

            if(vertical == 5)
            {
                string date = DateTime.Now.ToString("yyyy-MM-dd");
                string title = "";
                string details = "";
                List<Task_Model> datalist = new List<Task_Model>();
                datalist = objDD_Repo.ViewTotalTaskDetails(vertical,Convert.ToInt32(logInUser), 1).Where(x => x.Id == todoid).ToList();

                title = datalist[0].Title;
                details = datalist[0].Description;

                string result1 = objattorney.savequeryAttorny(date, title, details, Clientid, Activityid, Subactivityid, logInUser, todoid);
            
            }

            return Ok(new { results = objList });
        }



        [HttpGet]
        [Route("RejectTask")]
        [SessionAuthorizeFilter]
        public IActionResult RejectTask(int todoid, string RejectRemark)
        {
            long logInUser = 0;
            List<Task_Model> objList = new List<Task_Model>();
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            objList = objDD_Repo.RejectTask(todoid, RejectRemark, logInUser);
            return Ok(new { results = objList });
        }
        #endregion todoaction

        [HttpGet]
        [Route("GetEmployeelist")]
        [SessionAuthorizeFilter]
        public ActionResult GetEmployeelist(int VerticalId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetEmployeelist(CreatedBy, VerticalId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        #region SaveAssignTask
        [HttpPost]
        [Route("SaveAssignTask")]
        [SessionAuthorizeFilter]
        public IActionResult SaveAssignTask(Task_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreateBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDD_Repo.SaveAssignTask(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("SaveAssignTaskDetails")]
        [SessionAuthorizeFilter]
        public IActionResult SaveAssignTaskDetails(Task_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreateBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDD_Repo.SaveAssignTaskDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpPost]
        [Route("UpdateAssignTaskDetails")]
        [SessionAuthorizeFilter]
        public IActionResult UpdateAssignTaskDetails(Task_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreateBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDD_Repo.UpdateAssignTaskDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        [HttpGet]
        [Route("GetTotalTasks")]
        [SessionAuthorizeFilter]
        public IActionResult GetTotalTasks(int Verticalid, int EmpId)
        {
            long logInUser = 0;
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string result = objDD_Repo.GetTotalTasks(Verticalid, EmpId, logInUser);
            return Ok(new { results = result });
        }



        [HttpGet]
        [Route("ViewTotalTaskDetails")]
        [SessionAuthorizeFilter]
        public ActionResult ViewTotalTaskDetails(int Verticalid, int EmpId)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.ViewTotalTaskDetails(Verticalid, EmpId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        #region GetTodoListFilterWise
        [HttpGet]
        [Route("GetTodoListFilterWise")]
        [SessionAuthorizeFilter]
        public ActionResult GetTodoListFilterWise(int UserId, int VerticalId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetTodoListFilterWise(CreatedBy, UserId, VerticalId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetReassignDataList")]
        [SessionAuthorizeFilter]
        public ActionResult GetReassignDataList(int Id)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.GetReassignDataList(Id, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveReAssignTaskDetails")]
        [SessionAuthorizeFilter]
        public IActionResult SaveReAssignTaskDetails(Task_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreateBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objDD_Repo.SaveReAssignTaskDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        [HttpGet]
        [Route("GetUserDetail")]
        [SessionAuthorizeFilter]
        public ActionResult GetUserDetail(int UserId)
        {
            if (ModelState.IsValid)
            {
                List<Task_Model> result = new List<Task_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objDD_Repo.GetUserDetail(UserId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
    }
}