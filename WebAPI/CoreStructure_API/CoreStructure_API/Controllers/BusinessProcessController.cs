﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
//using static CoreStructure_API.Model.Business_Model;

namespace CoreStructure_API.Controllers
{
    [Route("api/BusinessProcess")]
    [ApiController]
    public class BusinessProcessController : ControllerBase
    {
        LC_Repository objLCRepo = new LC_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        public BusinessProcessController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Route("GetPoDetailsByRefId")]
        [SessionAuthorizeFilter]
        public IActionResult GetPoDetailsByRefId(long refId, long userid)
        {
            if (ModelState.IsValid)
            {
                List<BusinessProcess_Model> result = new List<BusinessProcess_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPoDetailsByRefId(CreatedBy, refId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetPoDetailListByRefId")]
        [SessionAuthorizeFilter]
        public IActionResult GetPoDetailListByRefId(long refId, long userid)
        {
            if (ModelState.IsValid)
            {
                List<PODetails_Model> result = new List<PODetails_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPoDetailListByRefId(CreatedBy, refId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveGenerateTempPrint")]
        [SessionAuthorizeFilter]
        public ActionResult SaveGenerateTempPrint(BusinessProcess_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CurrentUserId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objLCRepo.SaveGenerateTempPrint(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SavePrintPIDetails")]
        [SessionAuthorizeFilter]
        public ActionResult SavePrintPIDetails(BusinessProcess_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CurrentUserId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objLCRepo.SavePrintPIDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetPI_PRINT_TABLE_BySlNo")]
        [SessionAuthorizeFilter]
        public IActionResult GetPI_PRINT_TABLE_BySlNo(int slId, int userid)
        {
            if (ModelState.IsValid)
            {
                List<BusinessProcess_Model> result = new List<BusinessProcess_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPI_PRINT_TABLE_BySlNo(CreatedBy, slId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetPI_PRINT_DETAILS_TABLE_BySlNo")]
        [SessionAuthorizeFilter]
        public IActionResult GetPI_PRINT_DETAILS_TABLE_BySlNo(int slId, int userid)
        {
            if (ModelState.IsValid)
            {
                List<PODetails_Model> result = new List<PODetails_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPI_PRINT_DETAILS_TABLE_BySlNo(CreatedBy, slId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetPI_PRINT_TABLE_BySlNo_Temp")]
        [SessionAuthorizeFilter]
        public IActionResult GetPI_PRINT_TABLE_BySlNo_Temp(int slId, int userid)
        {
            if (ModelState.IsValid)
            {
                List<BusinessProcess_Model> result = new List<BusinessProcess_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPI_PRINT_TABLE_BySlNo_Temp(CreatedBy, slId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetPI_PRINT_DETAILS_TABLE_BySlNo_Temp")]
        [SessionAuthorizeFilter]
        public IActionResult GetPI_PRINT_DETAILS_TABLE_BySlNo_Temp(int slId, int userid)
        {
            if (ModelState.IsValid)
            {
                List<PODetails_Model> result = new List<PODetails_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetPI_PRINT_DETAILS_TABLE_BySlNo_Temp(CreatedBy, slId, userid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("RemovePOsFromPI")]
        [SessionAuthorizeFilter]
        public ActionResult RemovePOsFromPI(long currentUserId, long refId, string pos)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                long CurrentUserId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objLCRepo.RemovePOsFromPI(currentUserId, refId, pos);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetBankDetailsTofillForm")]
        [SessionAuthorizeFilter]
        public IActionResult GetBankDetailsTofillForm(long ref_Id, long userid, long BPMID, long StageId, long ThreadId)
        {
            List<BankFormDetails_Model> result = new List<BankFormDetails_Model>();
            try
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetBankDetailsTofillForm(CreatedBy, ref_Id, BPMID, StageId, ThreadId);
                
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, userid, "API", "LCController/GetBankDetailsTofillForm");
            }
            return Ok(new { results = result });
        }

        [HttpPost]
        [Route("SaveBankFormDetails")]
        [SessionAuthorizeFilter]
        public ActionResult SaveBankFormDetails(BankFormDetails_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long CurrentUserId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objLCRepo.SaveBankFormDetails(objModel, CurrentUserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveBankFormDetailsTemp")]
        [SessionAuthorizeFilter]
        public ActionResult SaveBankFormDetailsTemp(BankFormDetails_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long CurrentUserId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objLCRepo.SaveBankFormDetailsTemp(objModel, CurrentUserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetBankFormDetailsToPrint")]
        [SessionAuthorizeFilter]
        public IActionResult GetBankFormDetailsToPrint(long bankDetail_Id, long userid, long BPMID, long StageId, long ThreadId)
        {
            List<BankFormDetails_Model> result = new List<BankFormDetails_Model>();
            try
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetBankFormDetailsToPrint(CreatedBy, bankDetail_Id, BPMID, StageId, ThreadId);

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, userid, "API", "LCController/GetBankFormDetailsToPrint");
            }
            return Ok(new { results = result });
        }

        [HttpGet]
        [Route("GetBankFormDetailsToPrintTemp")]
        [SessionAuthorizeFilter]
        public IActionResult GetBankFormDetailsToPrintTemp(long bankDetail_Id, long userid)
        {
            List<BankFormDetails_Model> result = new List<BankFormDetails_Model>();
            try
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetBankFormDetailsToPrintTemp(CreatedBy, bankDetail_Id);

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, userid, "API", "LCController/GetBankFormDetailsToPrintTemp");
            }
            return Ok(new { results = result });
        }

        [HttpGet]
        [Route("GetAccountNumberByBankAndCurrency")]
        [SessionAuthorizeFilter]
        public ActionResult GetAccountNumberByBankAndCurrency(string BankName, string Currency)
        {
            if (ModelState.IsValid)
            {

                List<BankAccountNumber> result = new List<BankAccountNumber>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objLCRepo.GetAccountNumberByBankAndCurrency(BankName, Currency).Where(x => x.Bank == BankName.Trim() && x.Currency == Currency).ToList();
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
    }
}