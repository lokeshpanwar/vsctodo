﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/ServerLog")]
    [ApiController]
    public class ServerLogController : ControllerBase
    {

        ServerLog_Repository objRoleRepo = new ServerLog_Repository();



        #region ListServerLog (GET)

        [HttpGet]
        [Route("ListServerLog")]
        [SessionAuthorizeFilter]
        public ActionResult ListServerLog()
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetServerLog( ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}