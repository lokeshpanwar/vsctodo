﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/MailMsg")]
    [ApiController]
    public class MailMsgController : Controller
    {
        MailMsg_Repository objmailMsg = new MailMsg_Repository();

        [HttpGet]
        [Route("GetEmailDataForAdmin")]
        public IActionResult GetEmailDataForAdmin()
        {
            MailMsg_Model result = new MailMsg_Model();

            try
            {
                result = objmailMsg.GetEmailDataForAdmin();
                return Ok(new { results = result });
            }
            catch (Exception ex)
            {
                return Ok(new { results = "Something went wrong ..!" });
            }
        }

        [HttpGet]
        [Route("GetDailyMsgShootData")]
        public IActionResult GetDailyMsgShootData()
        {
            Msg_Model result = new Msg_Model();

            try
            {
                result = objmailMsg.GetDailyMsgShootData();
                return Ok(new { results = result });
            }
            catch (Exception ex)
            {
                return Ok(new { results = "Something went wrong ..!" });
            }
        }

        [HttpPost]
        [Route("SaveSentMsg")]
        public IActionResult SaveSentMsg(Remind_Msg_Model model)
        {
            string result = "";

            try
            {
                result = objmailMsg.SaveSentMsg(model);
            }
            catch (Exception ex)
            {
                return Ok(new { results = result });
            }

            return Ok(new { results = result });
        }
    }
}