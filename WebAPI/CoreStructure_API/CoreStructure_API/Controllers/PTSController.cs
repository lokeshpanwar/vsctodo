﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/PTS")]
    [ApiController]
    public class PTSController : Controller
    {
        PTS_Repository objQuery = new PTS_Repository();


        [HttpGet]
        [Route("Clientlist")]
        [SessionAuthorizeFilter]
        public IActionResult Clientlist(int officeid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Clientlist(officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Activity")]
        [SessionAuthorizeFilter]
        public IActionResult Activity(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.VSCCCActivity(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("SubActivity")]
        [SessionAuthorizeFilter]
        public IActionResult SubActivity(int OfficeId, int Activityid)
        {
            if (ModelState.IsValid)
            {
                //objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objQuery.VSCCCSubACtivity(OfficeId, Activityid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        


                 [HttpGet]
        [Route("Userlist")]
        [SessionAuthorizeFilter]
        public IActionResult Userlist(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Userlist(OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Filltimesheet")]
        [SessionAuthorizeFilter]
        public IActionResult Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid, int Projectid, int moduleid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Filltimesheet(empid, date, time, description, clientid, activityid, subactivityid, Projectid, moduleid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("Projectlist")]
        [SessionAuthorizeFilter]
        public IActionResult Projectlist(int Clientid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Projectlist(Clientid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("Projectmodules")]
        [SessionAuthorizeFilter]
        public IActionResult Projectmodules(int Projectid)
        {
            if (ModelState.IsValid)
            {
                var result = objQuery.Projectmodules(Projectid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("PTSUserData")]
        [SessionAuthorizeFilter]
        public IActionResult PTSUserData(int UserId)
        {

            if (ModelState.IsValid)
            {
                var result = objQuery.PTSUserData(UserId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


    }
}
