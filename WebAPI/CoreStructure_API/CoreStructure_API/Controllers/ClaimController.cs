﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Claim")]
    [ApiController]
    public class ClaimController : Controller
    {
        Claim_Repository objClaim = new Claim_Repository();
        //private readonly IHostingEnvironment _hostingEnvironment;

        //public ClaimController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}

        #region Lodge Claim
        [HttpGet]
        [Route("GetPolicyListForClaim")]
        [SessionAuthorizeFilter]
        public ActionResult GetPolicyListForClaim(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objClaim.GetPolicyListForClaim(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SavePALodgeClaimDetails")]
        [SessionAuthorizeFilter]
        public IActionResult SavePALodgeClaimDetails(LodgeClaim_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objClaim.SavePALodgeClaimDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpPost]
        [Route("SaveMedicalClaimDetails")]
        [SessionAuthorizeFilter]
        public IActionResult SaveMedicalClaimDetails(LodgeClaim_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objClaim.SaveMedicalClaimDetails(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetTPSurveyorForLodgeClaim")]
        [SessionAuthorizeFilter]
        public ActionResult GetTPSurveyorForLodgeClaim(int OfficeId, long PId)
        {
            if (ModelState.IsValid)
            {
                List<TP_Surveyor> result = new List<TP_Surveyor>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objClaim.GetTPSurveyorForLodgeClaim(OfficeId, PId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region View Lodge Claim

        [HttpGet]
        [Route("GetClaimedPolicyList")]
        [SessionAuthorizeFilter]
        public ActionResult GetClaimedPolicyList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<ViewLodgeClaim_Model> result = new List<ViewLodgeClaim_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objClaim.GetClaimedPolicyList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpDelete]
        [Route("DeleteClaim")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteClaim(long ClaimId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objClaim.DeleteClaim(ClaimId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetLodgeClaimDataForEdit")]
        [SessionAuthorizeFilter]
        public ActionResult GetLodgeClaimDataForEdit(int OfficeId, long ClaimId)
        {
            if (ModelState.IsValid)
            {
                List<LodgeClaim_Model> result = new List<LodgeClaim_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objClaim.GetLodgeClaimDataForEdit(OfficeId, ClaimId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("SaveClaimStepsDetails")]
        [SessionAuthorizeFilter]
        public ActionResult SaveClaimStepsDetails(long ClaimId, int StepId, string StepDate, long EmpId, string SettleAmount, string SettleDate, string AppRemark, string RejectRemark, string PendingRemark, int ClStatusId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objClaim.SaveClaimStepsDetails(ClaimId, StepId, StepDate, EmpId, SettleAmount, SettleDate, AppRemark, RejectRemark, PendingRemark, ClStatusId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetClaimStepsDetailList")]
        [SessionAuthorizeFilter]
        public ActionResult GetClaimStepsDetailList(int ClaimId)
        {
            if (ModelState.IsValid)
            {
                List<ClaimStepsDetails_Model> result = new List<ClaimStepsDetails_Model>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objClaim.GetClaimStepsDetailList(ClaimId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
    }


}