﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentClass")]
    [ApiController]
    public class DocumentClassController : ControllerBase
    {
        DocumentClass_Repository objDocClassRepo = new DocumentClass_Repository();


        [HttpPost]
        [Route("NewDocumentClass")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocumentClass(DocumentClass_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocClassRepo.InsertDocumentClass(objModel, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }
                          

        #region ListDocumentClassByPagination (GET)

        [HttpGet]
        [Route("ListDocumentClassByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentClassByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassRepo.GetDocumentClassListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

        
        [HttpGet]
        [Route("OneDocumentClass")]
        [SessionAuthorizeFilter]
        public ActionResult OneDocumentClass(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDocClassRepo.GetOneDocumentClass(Id, ModifiedBy, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        
        [HttpPut]
        [Route("EditDocumentClass")]
        [SessionAuthorizeFilter]
        public ActionResult EditDocumentClass(DocumentClass_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objDocClassRepo.UpdateDocumentClass(objModel, HttpContext.Request.Headers["Authorization-Token"].First());
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }





        #region DisableDocumentClass (DELETE)

        [HttpDelete]
        [Route("DisableDocumentClass")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocumentClass(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocClassRepo.DisableDocumentClass(Id, Modified_ById, HttpContext.Request.Headers["Authorization-Token"].First());
            return Ok(new { results = objreturn });

        }

        #endregion


        #region EnableDocumentClass (DELETE)

        [HttpDelete]
        [Route("EnableDocumentClass")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocumentClass(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocClassRepo.EnableDocumentClass(Id, Modified_ById, HttpContext.Request.Headers["Authorization-Token"].First());
            return Ok(new { results = objreturn });

        }

        #endregion



        #region ListDocumentClassListByUserId (GET)

        [HttpGet]
        [Route("ListDocumentClassListByUserId")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentClassListByUserId()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassRepo.GetDocumentClassListByUserId(CreatedBy);
            return Ok(new { results = result });
        }

        #endregion










    }
}