﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Priority")]
    [ApiController]
    public class PriorityController : ControllerBase
    {
        Priority_Repository objPriorityRepo = new Priority_Repository();


        #region NewPriority (POST)

        [HttpPost]
        [Route("NewPriority")]
        [SessionAuthorizeFilter]
        public ActionResult NewPriority(W_Priority_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.SCoy = GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.FDom = GlobalFunction.getLoggedInUserDomainId(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objPriorityRepo.InsertPriority(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region ListPriorityByPagination (GET)

        [HttpGet]
        [Route("ListPriorityByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListPriorityByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objPriorityRepo.GetPriorityListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region OnePriority (GET)

        [HttpGet]
        [Route("OnePriority")]
        [SessionAuthorizeFilter]
        public ActionResult OnePriority(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objPriorityRepo.GetOnePriority(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion



        #region EditPriority (PUT)

        [HttpPut]
        [Route("EditPriority")]
        [SessionAuthorizeFilter]
        public ActionResult EditPriority(W_Priority_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objPriorityRepo.UpdatePriority(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion



        #region DisablePriority

        [HttpDelete]
        [Route("DisablePriority")]
        [SessionAuthorizeFilter]
        public ActionResult DisablePriority(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Priority_Modified = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objPriorityRepo.DisablePriority(Id, Deleted_ById);
            return Ok(new { results = result });

        }

        #endregion


        #region EnablePriority

        [HttpDelete]
        [Route("EnablePriority")]
        [SessionAuthorizeFilter]
        public ActionResult EnablePriority(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Priority_Modified = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objPriorityRepo.EnablePriority(Id, Deleted_ById);
            return Ok(new { results = result });

        }


        #endregion


    }
}