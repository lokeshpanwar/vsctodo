﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/RoleMapping")]
    [ApiController]
    public class RoleMappingController : ControllerBase
    {
        RoleMapping_Repository objRoleRepo = new RoleMapping_Repository();


        #region NewRoleMapping (POST)

        [HttpPost]
        [Route("NewRoleMapping")]
        [SessionAuthorizeFilter]
        public ActionResult NewRoleMapping(Role_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());                
                objReturn = objRoleRepo.InsertRoleMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion




        #region ListRoleMappingByPagination (GET)

        [HttpGet]
        [Route("ListRoleMappingByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListRoleMappingByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objRoleRepo.GetRoleMappingListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion




        #region DeleteRoleMapping

        [HttpDelete]
        [Route("DeleteRoleMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteRoleMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objRoleRepo.DeleteRoleMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion


    }
}