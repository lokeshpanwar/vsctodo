﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DMS_Reports")]
    [ApiController]
    public class DMS_ReportsController : ControllerBase
    {
        DMS_Reports_Repository objDMSReportsRepo = new DMS_Reports_Repository();



        #region GetDocumentRevisionList (POST)

        [HttpPost]
        [Route("GetDocumentRevisionList")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocumentRevisionList(DMS_Report_DocRevision_Model objModel)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDMSReportsRepo.GetDocumentRevisionList(objModel,CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion




        #region GetDocumentUploadList (POST)

        [HttpPost]
        [Route("GetDocumentUploadList")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocumentUploadList(DMS_Report_DocRevision_Model objModel)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDMSReportsRepo.GetDocumentUploadList(objModel, CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion





        #region GetDocumentClassAccessHistoryList (POST)

        [HttpGet]
        [Route("GetDocumentClassAccessHistoryList")]
        [SessionAuthorizeFilter]
        public ActionResult GetDocumentClassAccessHistoryList(long ClassId, DateTime FromDate, DateTime ToDate)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDMSReportsRepo.GetDocumentClassAccessHistoryList(CreatedBy,ClassId,FromDate,ToDate);
            return Ok(new { results = datamodel });
        }

        #endregion


    }
}