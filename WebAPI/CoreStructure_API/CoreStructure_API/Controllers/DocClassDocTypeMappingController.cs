﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocClassDocTypeMapping")]
    [ApiController]
    public class DocClassDocTypeMappingController : ControllerBase
    {
        DocClassDocTypeMapping_Repository objDocClassDocTypeRepo = new DocClassDocTypeMapping_Repository();


        #region NewDocClassDocTypeMapping (POST)

        [HttpPost]
        [Route("NewDocClassDocTypeMapping")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocClassDocTypeMapping(DocumentType_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocClassDocTypeRepo.InsertDocClassDocTypeMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
                }
        }

        #endregion


        #region ListDocumentTypeForMapping (GET)

        [HttpGet]
        [Route("ListDocumentTypeForMapping")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocumentTypeForMapping()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objDocClassDocTypeRepo.GetDocumentTypeListForMapping(CreatedBy);
            return Ok(new { results = datamodel });
        }

        #endregion

        
        #region ListDocClassDocTypeMappingByPagination (GET)

        [HttpGet]
        [Route("ListDocClassDocTypeMappingByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocClassDocTypeMappingByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassDocTypeRepo.GetDocClassDocTypeMappingListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion





        [HttpDelete]
        [Route("DisableDocClassDocTypeMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocClassDocTypeMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocClassDocTypeRepo.DisableDocClassDocTypeMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableDocClassDocTypeMapping")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocClassDocTypeMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocClassDocTypeRepo.EnableDocClassDocTypeMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }






        [HttpDelete]
        [Route("DeleteDocClassDocTypeMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteDocClassDocTypeMapping(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocClassDocTypeRepo.DeleteDocClassDocTypeMapping(Id, Deleted_ById);
            return Ok(new { results = objreturn });

        }



    }
}