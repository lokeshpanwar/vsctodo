﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Model.Entities;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DropDown")]
    [ApiController]
    public class DropDownController : ControllerBase
    {
        DropDown_Repository objDD_Repo = new DropDown_Repository();
        Dashboard_Repository objDash_Repo = new Dashboard_Repository();

        #region GetThemeList

        [HttpGet]
        [Route("GetThemeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetThemeList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetThemeList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetStateList

        [HttpGet]
        [Route("GetStateList")]
        [SessionAuthorizeFilter]
        public ActionResult GetStateList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetStateList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetOfficelist

        [HttpGet]
        [Route("GetOfficeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetOfficeList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetOfficeList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        [HttpGet]
        [Route("GetOfficeListActive")]
        [SessionAuthorizeFilter]
        public ActionResult GetOfficeListActive()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetOfficeListActive(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }


        


        #endregion

        #region GetRoleList

        [HttpGet]
        [Route("GetRoleList")]
        [SessionAuthorizeFilter]
        public ActionResult GetRoleList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetRoleList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetCityList

        [HttpGet]
        [Route("GetCityListByStateId")]
        [SessionAuthorizeFilter]
        public ActionResult GetCityListByStateId(int StateId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetCityListByStateId(CreatedBy, StateId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetCategoryList

        [HttpGet]
        [Route("GetCategoryList")]
        [SessionAuthorizeFilter]
        public ActionResult GetCategoryList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetCategoryList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetInsuranceCompanyList

        [HttpGet]
        [Route("GetInsuranceCompanyList")]
        [SessionAuthorizeFilter]
        public ActionResult GetInsuranceCompanyList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetInsuranceCompanyList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetContactPersonList

        [HttpGet]
        [Route("GetContactPersonList")]
        [SessionAuthorizeFilter]
        public ActionResult GetContactPersonList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetContactPersonList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetPolicyTypeList

        [HttpGet]
        [Route("GetPolicyTypeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetPolicyTypeList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetPolicyTypeList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetGroupList

        [HttpGet]
        [Route("GetGroupList")]
        [SessionAuthorizeFilter]
        public ActionResult GetGroupList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetGroupList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetIssuingOfficeList

        [HttpGet]
        [Route("GetIssuingOfficeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetIssuingOfficeList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetIssuingOfficeList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetProductList

        [HttpGet]
        [Route("GetProductList")]
        [SessionAuthorizeFilter]
        public ActionResult GetProductList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetProductList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetClientList

        [HttpGet]
        [Route("GetClientList")]
        [SessionAuthorizeFilter]
        public ActionResult GetClientList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetClientList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetIssuingOfcList

        [HttpGet]
        [Route("GetIssuingOfcListbyInsCoId")]
        [SessionAuthorizeFilter]
        public ActionResult GetIssuingOfcListbyInsCoId(int insuranceCompanyId, int officeid)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetIssuingOfcListbyInsCoId(CreatedBy, insuranceCompanyId,officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetCategorylistListbyInsCoId")]
        [SessionAuthorizeFilter]
        public ActionResult GetCategorylistListbyInsCoId(int insuranceCompanyId, int officeid, int Catid)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetCategorylistListbyInsCoId(CreatedBy, insuranceCompanyId, officeid, Catid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



        [HttpGet]
        [Route("GetIssuingofficeByInsco")]
        [SessionAuthorizeFilter]
        public ActionResult GetIssuingofficeByInsco(int insuranceCompanyId, int officeid)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetIssuingofficeByInsco(CreatedBy, insuranceCompanyId, officeid);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        #endregion

        #region GetOfficeListForReport

        [HttpGet]
        [Route("GetOfficeListForReport")]
        [SessionAuthorizeFilter]
        public ActionResult GetOfficeListForReport()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetOfficeListForReport(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetContactPersonListWithMobile

        [HttpGet]
        [Route("GetContactPersonListWithMobile")]
        [SessionAuthorizeFilter]
        public ActionResult GetContactPersonListWithMobile(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetContactPersonListWithMobile(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetUserListByRoleId

        [HttpGet]
        [Route("GetUserListByRoleId")]
        [SessionAuthorizeFilter]
        public ActionResult GetUserListByRoleId(int RoleId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetUserListByRoleId(CreatedBy, RoleId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetTotalPolicyList

        [HttpGet]
        [Route("GetTotalPolicyList")]
        [SessionAuthorizeFilter]
        public ActionResult GetTotalPolicyList()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetTotalPolicyList(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetTotalRenewalPolicy

        [HttpGet]
        [Route("GetTotalRenewalPolicy")]
        [SessionAuthorizeFilter]
        public ActionResult GetTotalRenewalPolicy()
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetTotalRenewalPolicy(CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetAllPolicyNoList

        [HttpGet]
        [Route("GetAllPolicyNoList")]
        [SessionAuthorizeFilter]
        public ActionResult GetAllPolicyNoList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetPolicyNoList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetPolicyholderList

        [HttpGet]
        [Route("GetPolicyholderList")]
        [SessionAuthorizeFilter]
        public ActionResult GetPolicyholderList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetPolicyholderList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetClaimStepList

        [HttpGet]
        [Route("GetClaimStepList")]
        [SessionAuthorizeFilter]
        public ActionResult GetClaimStepList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetClaimStepList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetEmployeeList

        [HttpGet]
        [Route("GetEmployeeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetEmployeeList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetEmployeeList(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetInsuranceCompanyListForTPSurveyor

        [HttpGet]
        [Route("GetInsuranceCompanyListForTPSurveyor")]
        [SessionAuthorizeFilter]
        public ActionResult GetInsuranceCompanyListForTPSurveyor(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetInsuranceCompanyListForTPSurveyor(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetActivityListForDropDown

        [HttpGet]
        [Route("GetActivityListForDropDown")]
        [SessionAuthorizeFilter]
        public ActionResult GetActivityListForDropDown(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetActivityListForDropDown(CreatedBy, OfficeId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion

        #region GetSUbActivityList

        [HttpGet]
        [Route("GetSubActivityListByActivityId")]
        [SessionAuthorizeFilter]
        public ActionResult GetSubActivityListByActivityId(int ActivityId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objDD_Repo.GetSubActivityListByActivityId(CreatedBy, ActivityId);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}   