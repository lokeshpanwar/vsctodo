﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentRevisionReport")]
    [ApiController]
    public class DocumentRevisionReportController : ControllerBase
    {
        DocumentRevisionReport_Repository objRoleRepo = new DocumentRevisionReport_Repository();


        #region List GetDocumentRevisionReport (GET)

        [HttpGet]
        [Route("ListGetDocumentRevisionReport")]
        [SessionAuthorizeFilter]
        public ActionResult ListGetDocumentRevisionReport(long UserId, long DocumentClassID, DateTime FromDate, DateTime ToDate)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetDocumentRevisionReport(FromDate, ToDate, UserId, DocumentClassID, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}