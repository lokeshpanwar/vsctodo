﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Masters")]
    [ApiController]
    public class MastersController : Controller
    {
        Master_Repository objMaster = new Master_Repository();
        private readonly IHostingEnvironment _hostingEnvironment;
        //public MastersController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}

        //public IActionResult Index()
        //{
        //    return View();
        //}

        #region Users
        [HttpPost]
        [Route("AddNewUser")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewUser(Users objModel)
        {
            if (ModelState.IsValid)
            {
                //if (objModel.UserId == 0)
                //{
                    objModel.Password = EncryptDecrypt.Encrypt(objModel.Password);
                //}
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewUser(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetUserList")]
        [SessionAuthorizeFilter]
        public ActionResult GetUserList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Users> result = new List<Users>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetUserList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetUserDataForEdit")]
        [SessionAuthorizeFilter]
        public ActionResult GetUserDataForEdit(int UserId)
        {
            if (ModelState.IsValid)
            {
                List<Users> result = new List<Users>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetUserDataForEdit(UserId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #region GetEmployeeImage (GET)

        [HttpGet]
        [Route("GetEmployeeImage")]
        public IActionResult GetEmployeeImage(string path)
        {
            try
            {
                var image = System.IO.File.OpenRead(path);
                return File(image, "image/jpeg");
            }
            catch (Exception ex)
            { }
            return null;
        }

        #endregion


        [HttpGet]
        [Route("DeleteUser")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteUser(long UserId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteUser(UserId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("CheckUserExistOrNot")]
        [SessionAuthorizeFilter]
        public IActionResult CheckUserExistOrNot(string UserName)
        {
            long logInUser = 0;
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string result = objMaster.CheckUserExistOrNot(UserName, logInUser);
            return Ok(new { results = result });
        }


        [HttpGet]
        [Route("GetLatestPOSCode")]
        [SessionAuthorizeFilter]
        public IActionResult GetLatestPOSCode(int RoleId)
        {
            long logInUser = 0;
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string result = objMaster.GetLatestPOSCode(RoleId, logInUser);
            return Ok(new { results = result });
        }

        [HttpGet]
        [Route("CheckContactPersonExistOrNot")]
        [SessionAuthorizeFilter]
        public IActionResult CheckContactPersonExistOrNot(string Name, string MobileNo)
        {
            long logInUser = 0;
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string result = objMaster.CheckContactPersonExistOrNot(Name, MobileNo, logInUser);
            return Ok(new { results = result });
        }
        #endregion

        #region Groups

        //[HttpPost]
        //[Route("AddNewGroup")]
        //[SessionAuthorizeFilter]
        //public IActionResult AddNewGroup(Group objModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
        //        string result = objMaster.AddNewGroup(objModel);
        //        return Ok(new { results = result });
        //    }
        //    else
        //    {
        //        return Ok(new { results = "Enter Mandatory Fields!" });
        //    }
        //}

        [HttpPost]
        [Route("AddNewGroup")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewGroup([FromBody]Group objModel)
        {
            if (ModelState.IsValid)
            {
                // objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewGroup(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetGroupList")]
        [SessionAuthorizeFilter]
        public ActionResult GetGroupList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Group> result = new List<Group>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetGroupList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteGroup")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteGroup(int GroupId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteGroup(GroupId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Products

        [HttpPost]
        [Route("AddNewProduct")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewProduct(Product objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewProduct(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetProductList")]
        [SessionAuthorizeFilter]
        public ActionResult GetProductList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Product> result = new List<Product>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetProductList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteProduct")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteProduct(int ProductId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteProduct(ProductId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Issuing Office Master

        [HttpGet]
        [Route("GetInsuranceCompanyShortName")]
        [SessionAuthorizeFilter]
        public IActionResult GetInsuranceCompanyShortName(int CompanyId)
        {
            long logInUser = 0;
            logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            string result = objMaster.GetInsuranceCompanyShortName(CompanyId, logInUser);
            return Ok(new { results = result });
        }

        [HttpPost]
        [Route("AddNewIssuingOffice")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewIssuingOffice(IssuingOffice objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewIssuingOffice(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetIssuingOfficeList")]
        [SessionAuthorizeFilter]
        public ActionResult GetIssuingOfficeList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<IssuingOffice> result = new List<IssuingOffice>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetIssuingOfficeList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteIssuingOffice")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteIssuingOffice(int IssuingOfficeId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteIssuingOffice(IssuingOfficeId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Categories
        [HttpPost]
        [Route("AddNewCategory")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewCategory(Category objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewCategory(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        [HttpGet]
        [Route("GetCategoryList")]
        [SessionAuthorizeFilter]
        public ActionResult GetCategoryList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Category> result = new List<Category>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetCategoryList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteCategory")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteCategory(int CategoryId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteCategory(CategoryId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region ContactPerson
        [HttpPost]
        [Route("AddNewContactPerson")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewContactPerson(ContactPerson objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewContactPerson(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetContactPersonList")]
        [SessionAuthorizeFilter]
        public ActionResult GetContactPersonList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<ContactPerson> result = new List<ContactPerson>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetContactPersonList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteContactPerson")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteContactPerson(int ContactPersonId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteContactPerson(ContactPersonId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Profile

        [HttpGet]
        [Route("GetAdminProfileForView")]
        public IActionResult GetAdminProfileForView()
        {
            Users model = new Users();
            long UId = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            model = objMaster.GetAdminProfileForView(UId);
            return Ok(new { results = model });
        }


        #endregion

        #region TP/Surveyor

        [HttpPost]
        [Route("AddNewTPSurveyor")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewTPSurveyor(TP_Surveyor objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewTPSurveyor(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetTPSurveyorList")]
        [SessionAuthorizeFilter]
        public ActionResult GetTPSurveyorList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<TP_Surveyor> result = new List<TP_Surveyor>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetTPSurveyorList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteTP_Surveyor")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteTP_Surveyor(int TPId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteTP_Surveyor(TPId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Activity
        [HttpPost]
        [Route("AddNewActivity")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewActivity(Activity objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewActivity(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        [HttpGet]
        [Route("GetActivityList")]
        [SessionAuthorizeFilter]
        public ActionResult GetActivityList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Activity> result = new List<Activity>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetActivityList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteActivity")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteActivity(int ActivityId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteActivity(ActivityId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Sub Activity

        [HttpPost]
        [Route("AddNewSubActivity")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewSubActivity(SubActivity objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewSubActivity(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetSubActivityList")]
        [SessionAuthorizeFilter]
        public ActionResult GetSubActivityList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<SubActivity> result = new List<SubActivity>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetSubActivityList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteSubActivity")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteSubActivity(int SubActivityId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteSubActivity(SubActivityId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion
        #region Holiday
        [HttpPost]
        [Route("AddNewHoliday")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewHoliday(Holiday objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.AddNewHoliday(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        [HttpGet]
        [Route("GetHolidayList")]
        [SessionAuthorizeFilter]
        public ActionResult GetHolidayList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Holiday> result = new List<Holiday>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objMaster.GetHolidayList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteHoliday")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteHoliday(int HolidayId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objMaster.DeleteHoliday(HolidayId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

    }
}