﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Vendor")]
    [ApiController]
    public class VendorController : ControllerBase
    {

        Vendor_Repository objVendorRepo = new Vendor_Repository();

        private readonly IHostingEnvironment _hostingEnvironment;

        public VendorController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Route("NewVendor")]
        [SessionAuthorizeFilter]
        public ActionResult NewVendor(Vendor_Mapper_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.Employee_table_Model.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objVendorRepo.InsertVendor(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("OneVendor")]
        [SessionAuthorizeFilter]
        public ActionResult OneVendor(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objVendorRepo.GetOneVendor(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #region ListVendorByPagination (GET)

        [HttpGet]
        [Route("ListVendorByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListVendorByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objVendorRepo.GetVendorListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion
                              
        [HttpPut]
        [Route("EditVendor")]
        [SessionAuthorizeFilter]
        public ActionResult EditVendor(Vendor_Mapper_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.Employee_table_Model.UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objVendorRepo.UpdateVendor(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }            

        #region ListAllVendor (GET)

        [HttpGet]
        [Route("ListAllVendor")]
        [SessionAuthorizeFilter]
        public ActionResult ListAllVendor()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objVendorRepo.GetVendorList(CreatedBy);
            return Ok(new { results = result });
        }

        #endregion

    }
}