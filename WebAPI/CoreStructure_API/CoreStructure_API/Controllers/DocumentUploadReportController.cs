﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocumentUploadReport")]
    [ApiController]
    public class DocumentUploadReportController : ControllerBase
    {

        DocumentUploadReport_Repository objRoleRepo = new DocumentUploadReport_Repository();


        #region List GetDocumentUploadReport (GET)

        [HttpGet]
        [Route("ListGetDocumentUploadReport")]
        [SessionAuthorizeFilter]
        public ActionResult ListGetDocumentUploadReport(long UserId,string DocumentClassName, DateTime FromDate, DateTime ToDate)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRoleRepo.GetDocumentUploadReport(FromDate, ToDate, UserId, DocumentClassName, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}