﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        Auth_Repository obAuthRepo = new Auth_Repository();

        //private readonly IHostingEnvironment _hostingEnvironment;

        //public AuthController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}
        [HttpPost]
        [Route("GetOTPForLogIn")]
        public IActionResult GetOTPForLogIn([FromBody]Login_Model model)
        {
            OTP_Return_Value objRetModel = new OTP_Return_Value();

            try
            {

                model.Password = EncryptDecrypt.Encrypt(model.Password);
                objRetModel = obAuthRepo.GetOTPForLogIn(model);

            }
            catch (Exception ex)
            {
                return Ok(new { results = objRetModel });
            }

            return Ok(new { results = objRetModel });
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login([FromBody]Login_Model model)
        {
            LoginReturn_Model objRetModel = new LoginReturn_Model();

            try {

             //model.Password = EncryptDecrypt.Encrypt(model.Password);
             //string dPass = EncryptDecrypt.Decrypt(model.Password);
                objRetModel = obAuthRepo.CheckLogin(model);

            }
            catch(Exception ex)
            {
                objRetModel.EmployeeName = ex.ToString();
                return Ok(new { results = objRetModel });
            }

            return Ok(new { results=objRetModel });
        }



        [HttpGet]
        [Route("CheckIfRoleHavingPermissionForPage")]
        public IActionResult CheckIfRoleHavingPermissionForPage(long RoleId, string FormName)
        {
            var objRetModel = obAuthRepo.CheckIfRoleHavingPermissionForForm(RoleId, FormName);

            return Ok(new { results = objRetModel });
        }



        [HttpGet]
        [Route("GetFormPermissionListFromUserID")]
        public IActionResult GetFormPermissionListFromUserID(long UserID)
        {
            var objRetModel = obAuthRepo.GetRoleMenuListFromUserID(UserID);

            return Ok(new { results = objRetModel });
        }

    }
}