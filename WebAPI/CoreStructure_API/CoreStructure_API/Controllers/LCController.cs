﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/LC")]
    [ApiController]
    public class LCController : ControllerBase
    {
        LC_Repository objLCRepo = new LC_Repository();





        #region ListAllRequestForLC (GET)

        [HttpGet]
        [Route("ListAllRequestForLC")]
        [SessionAuthorizeFilter]
        public ActionResult ListAllRequestForLC(string CompId, string VenEmpId, string ELIKZ, int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objLCRepo.GetRequestForLCList(max, page, search, sort_col, sort_dir, CreatedBy, CompId, VenEmpId, ELIKZ);
            return Ok(new { results = result });
        }

        #endregion




        #region NewLCRequest (POST)

        [HttpPost]
        [Route("NewLCRequest")]
        [SessionAuthorizeFilter]
        public ActionResult NewLCRequest(LC_Request_List_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objLCRepo.InsertLCRequest(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion

        #region Code By Vijendra


        [HttpGet]
        [Route("ListLCSubmission")]
        [SessionAuthorizeFilter]
        public IActionResult ListLCSubmission(string CompId, string VenEmpId, string ELIKZ, int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            List<LC_Process> result = new List<LC_Process>();
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            result = objLCRepo.ListAllLCSubmission(max, page, search, sort_col, sort_dir, CreatedBy, CompId, VenEmpId, ELIKZ);
            return Ok(new { results = result });
        }

        [HttpGet]
        [Route("ListLCProcess")]
        [SessionAuthorizeFilter]
        public IActionResult ListLCProcess(string CompId, string VenEmpId, string ELIKZ, int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            List<LC_Process> result = new List<LC_Process>();
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            result = objLCRepo.ListAllLCProcess(max, page, search, sort_col, sort_dir, CreatedBy, CompId, VenEmpId, ELIKZ);
            return Ok(new { results = result });
        }
        #endregion
    }
}