﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/Discuss")]
    [ApiController]
    public class DiscussController : ControllerBase
    {

        Discuss_Repository objEvetMstRep = new Discuss_Repository();

        #region New Discuss Master
        [HttpPost]
        [Route("NewDiscussMaster")]
        [SessionAuthorizeFilter]
        public ActionResult NewDiscussMaster(Discuss_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.InsertDiscussMaster(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One DiscussMaster
        [HttpGet]
        [Route("OneDiscussMasterList")]
        [SessionAuthorizeFilter]

        public ActionResult OneDiscussMasterList(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneDiscuss(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit DiscussMaster

        [HttpPut]
        [Route("EditDiscussMaster")]
        [SessionAuthorizeFilter]

        public ActionResult EditDiscussMaster(Discuss_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.UpdateDiscussMaster(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion


        #region Disable DiscussMaster

        [HttpDelete]
        [Route("DisableDiscussMaster")]
        [SessionAuthorizeFilter]

        public ActionResult DisableDiscussMaster(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableDiscussMaster(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region Enable DiscussMaster

        [HttpDelete]
        [Route("EnableDiscussMaster")]
        [SessionAuthorizeFilter]

        public ActionResult EnableDiscussMaster(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.EnableDiscussMaster(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region ListDiscuss By pagination

        [HttpGet]
        [Route("ListDiscussMasterBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListDiscussMasterBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetDiscussMasterByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion


        #region Update Interval Value

        [HttpDelete]
        [Route("UpdateIntervalValue")]
        [SessionAuthorizeFilter]

        public ActionResult UpdateIntervalValue(long Id,int interval)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.UpdateIntervalValue(Id, interval, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region ListDiscuss By pagination for user dashboard

        [HttpGet]
        [Route("ListDiscussMasterBypaginationUserDash")]
        [SessionAuthorizeFilter]

        public ActionResult ListDiscussMasterBypaginationUserDash(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetDiscussList(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion



        //Discussion_Message Start

        #region New Discuss Message
        [HttpPost]
        [Route("NewDiscussionMessage")]
        [SessionAuthorizeFilter]
        public ActionResult NewDiscussionMessage(Discussion_Message_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.InsertDiscussMessage(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One DiscussMessage
        [HttpGet]
        [Route("OneDiscussMessageList")]
        [SessionAuthorizeFilter]

        public ActionResult OneDiscussMessageList(long Id,long userid)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneDiscussMessage(Id, CreatedBy, userid);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region One GetemailIdByUser
        [HttpGet]
        [Route("GetemailIdByUser")]
        [SessionAuthorizeFilter]

        public ActionResult GetemailIdByUser(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetemailIdByUser(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion
        //Discussion_Message End

        // Discuss_Vote Start
        #region New Discuss Vote
        [HttpPost]
        [Route("NewDiscussionVote")]
        [SessionAuthorizeFilter]
        public ActionResult NewDiscussionVote(Discuss_Vote_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.UserID = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.InsertDiscuss_Vote(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion
        //Discuss_Vote End
    }
}