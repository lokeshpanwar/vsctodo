﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;


namespace CoreStructure_API.Controllers
{
    [Route("api/EventMaster")]
    [ApiController]
    public class EventMasterController : ControllerBase
    {

        EventMaster_Repository objEvetMstRep = new EventMaster_Repository();

        #region New Event Master
        [HttpPost]
        [Route("NewEventMaster")]
        [SessionAuthorizeFilter]
        public ActionResult NewEventMaster(EventMaster_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            
            

            if (ModelState.IsValid)
            {
               
                    objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                    objreturn = objEvetMstRep.InsertEventMaster(objModel);
                    return (Ok(new { results = objreturn }));
               
            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One EventList
        [HttpGet]
        [Route("OneEventList")]
        [SessionAuthorizeFilter]
       
        public ActionResult OneEventList(long Id)
        {
            if(ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneEvent(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new {results= "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit EventMaster

        [HttpPut]
        [Route("EditEventMaster")]
        [SessionAuthorizeFilter]

        public ActionResult EditEventMaster(EventMaster_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if(ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.UpdateEventMaster(objModel);

                return Ok( new{results=objreturn });

            }

            else
            {
                return Ok(new { results = objreturn});
            }
        }
        #endregion


        #region Disable EventMaster

        [HttpDelete]
        [Route("DisableEventMaster")]
        [SessionAuthorizeFilter]

        public ActionResult DisableEventMaster(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if(ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableEventMaster(Id, DeletedBy);
                return Ok(new { results=objreturn});
            }
            else
            {
                return Ok(new { results =objreturn});
            }

        }
        #endregion


        #region Enable EventMaster
        [HttpDelete]
        [Route("EnableEventMaster")]
        [SessionAuthorizeFilter]
        public ActionResult EnableEventMaster(long Id)
        {
           // string result = "";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn.ReturnStatus = "Error";

            if(ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.EnableEventMaster(Id, DeletedBy);
                return Ok(new { results=objreturn});
            }

            else
            {
                return Ok(new { results=objreturn});

            }
        }
        #endregion


        #region ListEvent By pagination

        [HttpGet]
        [Route("ListEventMasterBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListEventMasterBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetEventMasterByPage( max, page, search, sort_col, sort_dir,CreatedBy);

            return Ok(new { results=result});
        }
        #endregion



        #region EventMasterListFromSubCompAndDomain

        [HttpGet]
        [Route("EventMasterListFromSubCompAndDomain")]
        [SessionAuthorizeFilter]
        public ActionResult EventMasterListFromSubCompAndDomain(long SubCompID, long DomainId)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetEventMasterListFromSubCompAndDomain(SubCompID,DomainId, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion


    }


}