﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/AdminDashboard")]
    [ApiController]
    public class AdminDashboardController : ControllerBase
    {
        AdminDashboard_Repository objAdminRep = new AdminDashboard_Repository();


        #region Total GetTotalallRecord For admin Dashboard

        [HttpGet]
        [Route("GetTotalallRecordForAdminDash")]
        [SessionAuthorizeFilter]
        public ActionResult GetTotalallRecordForAdminDash()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objAdminRep.GetTotalallRecord(CreatedBy);
            return Ok(new { results = datamodel });
        }
        #endregion

        


       
    }
}