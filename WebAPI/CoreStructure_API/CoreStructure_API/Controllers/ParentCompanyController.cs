﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/ParentCompany")]
    [ApiController]
    public class ParentCompanyController : ControllerBase
    {
        ParentCompany_Repository objParentCompanyRepo = new ParentCompany_Repository();


        [HttpPost]
        [Route("NewParentCompany")]
        [SessionAuthorizeFilter]
        public ActionResult NewParentCompany(Parent_Company_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objParentCompanyRepo.InsertParentCompany(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        [HttpGet]
        [Route("ListParentCompany")]
        [SessionAuthorizeFilter]
        public ActionResult ListParentCompany()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var datamodel = objParentCompanyRepo.GetParentCompanyList(CreatedBy);
            return Ok(new { results = datamodel });
        }






        [HttpGet]
        [Route("OneParentCompany")]
        [SessionAuthorizeFilter]
        public ActionResult OneParentCompany(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objParentCompanyRepo.GetOneParentCompany(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpPut]
        [Route("EditParentCompany")]
        [SessionAuthorizeFilter]
        public ActionResult EditParentCompany(Parent_Company_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objParentCompanyRepo.UpdateParentCompany(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }




        #region ListParentCompanyByPagination (GET)

        [HttpGet]
        [Route("ListParentCompanyByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListParentCompanyByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objParentCompanyRepo.GetParentCompanyListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


    }
}