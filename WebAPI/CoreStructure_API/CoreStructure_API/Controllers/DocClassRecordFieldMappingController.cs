﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/DocClassRecordFieldMapping")]
    [ApiController]
    public class DocClassRecordFieldMappingController : ControllerBase
    {

        DocClassRecordFieldMapping_Repository objDocClassRecordFieldRepo = new DocClassRecordFieldMapping_Repository();


        #region NewDocClassRecordFieldMapping (POST)

        [HttpPost]
        [Route("NewDocClassRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult NewDocClassRecordFieldMapping(RecordField_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objDocClassRecordFieldRepo.InsertDocClassRecordFieldMapping(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion




        #region ListDocClassRecordFieldMappingByPagination (GET)

        [HttpGet]
        [Route("ListDocClassRecordFieldMappingByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListDocClassRecordFieldMappingByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objDocClassRecordFieldRepo.GetDocClassRecordFieldMappingListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion






        [HttpDelete]
        [Route("DisableDocClassRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DisableDocClassRecordFieldMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocClassRecordFieldRepo.DisableDocClassRecordFieldMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }



        [HttpDelete]
        [Route("EnableDocClassRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDocClassRecordFieldMapping(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objDocClassRecordFieldRepo.EnableDocClassRecordFieldMapping(Id, Modified_ById);
            return Ok(new { results = result });

        }







        [HttpDelete]
        [Route("DeleteDocClassRecordFieldMapping")]
        [SessionAuthorizeFilter]
        public ActionResult DeleteDocClassRecordFieldMapping(long Id)
        {
            int Deleted_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objDocClassRecordFieldRepo.DeleteDocClassRecordFieldMapping(Id, Deleted_ById);
            return Ok(new { results = objreturn });

        }


    }
}