﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace CoreStructure_API.Controllers
{
    [Route("api/Employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {

        Employee_Repository objEmployeeRepo = new Employee_Repository();

        //private readonly IHostingEnvironment _hostingEnvironment;

        //public EmployeeController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}



        #region NewEmployee (POST)

        [HttpPost]
        [Route("NewEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult NewEmployee(Employee_table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                if (objModel.Email != "")
                {
                    if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email,objModel.CreatedBy)==ResponseStatus.EMAIL_DOMAIN_VALID)
                    {
                        string result = objEmployeeRepo.InsertEmployee(objModel);
                        return Ok(new { results = result });
                    }
                    else
                    {
                        return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                    }
                }
                else
                {
                    return Ok(new { results = "Enter Valid Email ID" });
                }
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion




        #region ListEmployeeByPagination (GET)

        [HttpGet]
        [Route("ListEmployeeByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListEmployeeByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objEmployeeRepo.GetEmployeeListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion



        #region OneEmployee (GET)

        [HttpGet]
        [Route("OneEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult OneEmployee(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.GetOneEmployee(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion



        #region DisableEmployee (DELETE)

        [HttpDelete]
        [Route("DisableEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult DisableEmployee(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objEmployeeRepo.DisableEmployee(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion

        #region EnableEmployee (DELETE)

        [HttpDelete]
        [Route("EnableEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult EnableEmployee(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());

            string result = objEmployeeRepo.EnableEmployee(Id, Modified_ById);
            return Ok(new { results = result });

        }

        #endregion




        #region EditEmployee (PUT)

        [HttpPut]
        [Route("EditEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult EditEmployee(Employee_table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                if (objModel.Email != "")
                {
                    if (GlobalFunction.CheckIfValidDomainFromEmail(objModel.Email, objModel.CreatedBy) == ResponseStatus.EMAIL_DOMAIN_VALID)
                    {
                        string result = objEmployeeRepo.UpdateEmployee(objModel);
                        return Ok(new { results = result });
                    }
                    else
                    {
                        return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                    }
                }
                else
                {
                    return Ok(new { results = "Enter Valid Email ID" });
                }
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion


        #region GetEmployeeImage (GET)

        [HttpGet]
        [Route("GetEmployeeImage")]
        public IActionResult GetEmployeeImage(string path)
        {
            try
            {
                var image = System.IO.File.OpenRead(path);
                return File(image, "image/jpeg");
            }
            catch(Exception ex)
            { }
            return null;
        }

        #endregion




        #region ListUserForApproverLimitation (GET)

        [HttpGet]
        [Route("ListUserForApproverLimitation")]
        [SessionAuthorizeFilter]
        public ActionResult ListUserForApproverLimitation(long SubCompId, long Domain_ID)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            List<Employee_Process_value_Model> datamodel = new List<Employee_Process_value_Model>();
            datamodel = objEmployeeRepo.GetEmployeeListForApproverLimitation(CreatedBy, SubCompId, Domain_ID);
            return Ok(new { results = datamodel });
        }

        #endregion

        #region ListUserForApproverLimitation (POST)

        [HttpPost]
        [Route("ListUserForApproverLimitation")]
        [SessionAuthorizeFilter]
        public ActionResult ListUserForApproverLimitation(Employee_Process_value_list_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objEmployeeRepo.InsertApproverLimitation(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
        #region ListAllEmployee (GET)

        [HttpGet]
        [Route("ListAllEmployee")]
        [SessionAuthorizeFilter]
        public ActionResult ListAllEmployee()
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objEmployeeRepo.GetAllEmployee(CreatedBy);
            return Ok(new { results = result });
        }

        #endregion
        #region EditUserProfile (PUT)

        [HttpPut]
        [Route("EditUserProfile")]
        [SessionAuthorizeFilter]
        public ActionResult EditUserProfile(UserProfile_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.UpdateEmployeeFromUserProfile(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
        #region ChangeUserPassword (PUT)

        //[HttpPut]
        [HttpPost]
        [Route("ChangeUserPassword")]
        [SessionAuthorizeFilter]
        public IActionResult ChangeUserPassword(ResetPassword_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.UpdateEmployeeUserPassword(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
        #region EditUserDelegate (PUT)

        [HttpPut]
        [Route("EditUserDelegate")]
        [SessionAuthorizeFilter]
        public ActionResult EditUserDelegate(User_Delegation_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.parentCoy = GlobalFunction.getLoggedInUserParentCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.subCoy= GlobalFunction.getLoggedInUserSubCompanyId(HttpContext.Request.Headers["Authorization-Token"].First());
                objModel.FD = GlobalFunction.getLoggedInUserDomainId(HttpContext.Request.Headers["Authorization-Token"].First());
                long UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.UpdateUserDelegate(objModel, UpdatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
        #region OneUserDelegation (GET)

        [HttpGet]
        [Route("OneUserDelegation")]
        [SessionAuthorizeFilter]
        public ActionResult OneUserDelegation(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.GetOneUserDelegation(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion



        #region EnableDisableUserDelegate (PUT)

        [HttpPut]
        [Route("EnableDisableUserDelegate")]
        [SessionAuthorizeFilter]
        public ActionResult EnableDisableUserDelegate(User_Delegation_Table_Model objModel)
        {
            if (ModelState.IsValid)
            {
                long UpdatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.EnableDisableUserDelegate(objModel, UpdatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion







        #region ListAllEmployeeReporting (GET)

        [HttpGet]
        [Route("ListAllEmployeeReporting")]
        [SessionAuthorizeFilter]
        public ActionResult ListAllEmployeeReporting(string empnm)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objEmployeeRepo.GetEmployeeReportingList(CreatedBy, empnm);
            return Ok(new { results = result });
        }

        #endregion




        #region OneEmployeeFromEmail (GET)

        [HttpGet]
        [Route("OneEmployeeFromEmail")]
        [SessionAuthorizeFilter]
        public ActionResult OneEmployeeFromEmail(string Email)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEmployeeRepo.GetOneEmployeeFromEmail(Email, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion





        [HttpPost]
        [Route("NewEmployeeBulkUpload")]
        [SessionAuthorizeFilter]
        public ActionResult NewEmployeeBulkUpload(Employee_table_page_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";

            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                bool emailok = false;
                foreach(var data in objModel.Employee_table_Model_List)
                {
                    if (data.Email != "")
                    {
                        if (GlobalFunction.CheckIfValidDomainFromEmail(data.Email, CreatedBy) == ResponseStatus.EMAIL_DOMAIN_INVALID)
                        {
                            emailok = false;
                            return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                        }
                        else
                        {
                            emailok = true;
                        }
                    }
                    else
                    {
                        return Ok(new { results = "Enter Valid Email ID" });
                    }
                }
                if (emailok == true)
                {
                    objreturn = objEmployeeRepo.InsertEmployeeFromBulk(objModel.Employee_table_Model_List, CreatedBy);
                    return Ok(new { results = objreturn });
                }
                else
                {
                    return Ok(new { results = ResponseStatus.EMAIL_DOMAIN_INVALID_MESSAGE });
                }
            }
            else
            {
                return (Ok(new { results = objreturn }));
            }
        }



        #region GetEmployeeCSV (GET)

        [HttpGet]
        [Route("GetEmployeeCSV")]
        public IActionResult GetEmployeeCSV()
        {
            try
            {
                string samplepath = "Content/SampleCSV/employee.csv";
                var path = Path.Combine(samplepath);

                var image = System.IO.File.OpenRead(path);
                return File(image, "text/csv", "employee.csv");
            }
            catch (Exception ex)
            { }
            return null;
        }

        #endregion

    }
}