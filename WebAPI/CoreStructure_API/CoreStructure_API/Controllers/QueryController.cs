﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Query")]
    [ApiController]
    public class QueryController : Controller
    {
        Query_Repository objQuery = new Query_Repository();
        //private readonly IHostingEnvironment _hostingEnvironment;

        //public QueryController(IHostingEnvironment hostingEnvironment)
        //{
        //    _hostingEnvironment = hostingEnvironment;
        //}
        //public IActionResult Index()
        //{
        //    return View();
        //}

        #region Query
        [HttpPost]
        [Route("AddNewQuery")]
        [SessionAuthorizeFilter]
        public IActionResult AddNewQuery(Query objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.AddNewQuery(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetQueryList")]
        [SessionAuthorizeFilter]
        public ActionResult GetQueryList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Query> result = new List<Query>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetQueryList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("DeleteQuery")]
        [SessionAuthorizeFilter]
        public IActionResult DeleteQuery(int QId)
        {
            long logInUser = 0;
            if (ModelState.IsValid)
            {
                logInUser = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.DeleteQuery(QId, logInUser);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("CloseQueryUpdate")]
        [SessionAuthorizeFilter]
        public ActionResult CloseQueryUpdate(long QId, string Remark)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.CloseQueryUpdate(QId, Remark, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("ConvertQueryUpdate")]
        [SessionAuthorizeFilter]
        public ActionResult ConvertQueryUpdate(long QId)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.ConvertQueryUpdate(QId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetClosedQueryList")]
        [SessionAuthorizeFilter]
        public ActionResult GetClosedQueryList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Query> result = new List<Query>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetClosedQueryList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("PendingQueryRemarkUpdate")]
        [SessionAuthorizeFilter]
        public ActionResult PendingQueryRemarkUpdate(long QId, string Remark)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.PendingQueryRemarkUpdate(QId, Remark, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetAllQueryListByUser")]
        [SessionAuthorizeFilter]
        public ActionResult GetAllQueryListByUser(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Query> result = new List<Query>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetAllQueryListByUser(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region UnderWrite
        [HttpPost]
        [Route("SaveUnderWritePolicy")]
        [SessionAuthorizeFilter]
        public IActionResult SaveUnderWritePolicy(UnderWrite objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.SaveUnderWritePolicy(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetQueryDetailsForUnderWrite")]
        [SessionAuthorizeFilter]
        public ActionResult GetQueryDetailsForUnderWrite(int OfficeId, long QId)
        {
            if (ModelState.IsValid)
            {
                List<UnderWrite> result = new List<UnderWrite>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetQueryDetailsForUnderWrite(OfficeId, QId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }
        #endregion

        #region Policy

        [HttpGet]
        [Route("GetPolicyList")]
        [SessionAuthorizeFilter]
        public ActionResult GetPolicyList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetPolicyList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveGeneratePolicy")]
        [SessionAuthorizeFilter]
        public IActionResult SaveGeneratePolicy(Policy objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.SaveGeneratePolicy(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveRenewPolicy")]
        [SessionAuthorizeFilter]
        public IActionResult SaveRenewPolicy(Policy objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.SaveRenewPolicy(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetDeletedPolicyList")]
        [SessionAuthorizeFilter]
        public ActionResult GetDeletedPolicyList(int OfficeId)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetDeletedPolicyList(OfficeId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetMasterPolicyDataList")]
        [SessionAuthorizeFilter]
        public ActionResult GetMasterPolicyDataList(int OfficeId, string fDate, string tDate)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetMasterPolicyDataList(OfficeId, CreatedBy, fDate, tDate);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpGet]
        [Route("GetPolicyDetailsForEdit")]
        [SessionAuthorizeFilter]
        public ActionResult GetPolicyDetailsForEdit(long PId)
        {
            if (ModelState.IsValid)
            {
                List<Policy> result = new List<Policy>();
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                result = objQuery.GetPolicyDetailsForEdit(PId, CreatedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        [HttpPost]
        [Route("SaveUpdatePolicy")]
        [SessionAuthorizeFilter]
        public IActionResult SaveUpdatePolicy(Policy objModel)
        {
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                string result = objQuery.SaveUpdatePolicy(objModel);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion
    }
}