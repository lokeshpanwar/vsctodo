﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/Global")]
    [ApiController]
    public class GlobalController : ControllerBase
    {

        GlobalFunction objGFunc = new GlobalFunction();


        [HttpGet]
        [Route("RandomNumber")]
        [SessionAuthorizeFilter]
        public ActionResult RandomNumber(int Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = GlobalFunction.GetRandomNumber(Id);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }





        [HttpGet]
        [Route("CheckDomain")]
        [SessionAuthorizeFilter]
        public ActionResult CheckDomain(string Email)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = GlobalFunction.CheckIfValidDomainFromEmail(Email, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }



    }
}