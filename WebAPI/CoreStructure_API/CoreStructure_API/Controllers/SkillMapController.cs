﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using CoreStructure_API.Global;

namespace CoreStructure_API.Controllers
{
    [Route("api/SkillMap")]
    [ApiController]
    public class SkillMapController : ControllerBase
    {

        SkillMap_Repository objEvetMstRep = new SkillMap_Repository();

        #region New SkillMap
        [HttpPost]
        [Route("NewSkillMap")]
        [SessionAuthorizeFilter]
        public ActionResult NewSkillMap(SkillMap_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";



            if (ModelState.IsValid)
            {

                objModel.creationuser = Convert.ToInt64(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First())).ToString();
                objreturn = objEvetMstRep.InsertSkillMap(objModel);
                return (Ok(new { results = objreturn }));

            }

            else
            {
                return (Ok(new { results = objreturn }));
            }
        }
        #endregion


        #region One SkillMap
        [HttpGet]
        [Route("OneSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult OneSkillMap(long Id)
        {
            if (ModelState.IsValid)
            {
                long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objEvetMstRep.GetOneSkillMap(Id, CreatedBy);

                return Ok(new { results = result });
            }

            return Ok(new { results = "Enter Mandatory Fields!" });
        }
        #endregion


        #region Edit SkillMap

        [HttpPut]
        [Route("EditSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult EditSkillMap(SkillMap_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.modifactionuser = Convert.ToInt64(GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First())).ToString();
                objreturn = objEvetMstRep.UpdateSkillMap(objModel);

                return Ok(new { results = objreturn });

            }

            else
            {
                return Ok(new { results = objreturn });
            }
        }
        #endregion


        #region Disable SkillMap

        [HttpDelete]
        [Route("DisableSkillMap")]
        [SessionAuthorizeFilter]

        public ActionResult DisableSkillMap(long Id)
        {
            string result = "Enter Mandatory Fields!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = result;
            objreturn.ReturnStatus = "ERROR";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

                objreturn = objEvetMstRep.DisableSkillMap(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }

        }
        #endregion


        #region Enable SkillMap
        [HttpDelete]
        [Route("EnableSkillMap")]
        [SessionAuthorizeFilter]
        public ActionResult EnableSkillMap(long Id)
        {
            
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn.ReturnStatus = "Error";

            if (ModelState.IsValid)
            {
                long DeletedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objEvetMstRep.EnableSkillMap(Id, DeletedBy);
                return Ok(new { results = objreturn });
            }

            else
            {
                return Ok(new { results = objreturn });

            }
        }
        #endregion



        #region SkillMap By pagination

        [HttpGet]
        [Route("ListSkillMapBypagination")]
        [SessionAuthorizeFilter]

        public ActionResult ListSkillMapBypagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
      {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());

            var result = objEvetMstRep.GetSkillMapByPage(max, page, search, sort_col, sort_dir, CreatedBy);

            return Ok(new { results = result });
        }
        #endregion
    }
}