﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Repository;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreStructure_API.Controllers
{
    [Route("api/RecordField")]
    [ApiController]
    public class RecordFieldController : ControllerBase
    {

        RecordField_Repository objRecordFieldRepo = new RecordField_Repository();

        #region NewRecordField (POST)

        [HttpPost]
        [Route("NewRecordField")]
        [SessionAuthorizeFilter]
        public ActionResult NewRecordField(List_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Enter Mandatory Fields!";
            objReturn.ReturnStatus = "ERROR";
            if (ModelState.IsValid)
            {
                objModel.CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objReturn = objRecordFieldRepo.InsertRecordField(objModel);
                return Ok(new { results = objReturn });
            }
            else
            {
                return Ok(new { results = objReturn });
            }
        }

        #endregion


        #region ListRecordFieldByPagination (GET)

        [HttpGet]
        [Route("ListRecordFieldByPagination")]
        [SessionAuthorizeFilter]
        public ActionResult ListRecordFieldByPagination(int max, int page, string search = null, string sort_col = null, string sort_dir = null)
        {
            long CreatedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            var result = objRecordFieldRepo.GetRecordFieldListByPage(max, page, search, sort_col, sort_dir, CreatedBy);
            return Ok(new { results = result });
        }

        #endregion


        #region OneRecordField (GET)

        [HttpGet]
        [Route("OneRecordField")]
        [SessionAuthorizeFilter]
        public ActionResult OneRecordField(long Id)
        {
            if (ModelState.IsValid)
            {
                long ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                var result = objRecordFieldRepo.GetOneRecordField(Id, ModifiedBy);
                return Ok(new { results = result });
            }
            else
            {
                return Ok(new { results = "Enter Mandatory Fields!" });
            }
        }

        #endregion


        #region EditRecordField (PUT)

        [HttpPut]
        [Route("EditRecordField")]
        [SessionAuthorizeFilter]
        public ActionResult EditRecordField(List_table_Model objModel)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            if (ModelState.IsValid)
            {
                objModel.ModifiedBy = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
                objreturn = objRecordFieldRepo.UpdateRecordField(objModel);
                return Ok(new { results = objreturn });
            }
            else
            {
                return Ok(new { results = objreturn });
            }
        }

        #endregion






        #region DisableRecordField (DELETE)

        [HttpDelete]
        [Route("DisableRecordField")]
        [SessionAuthorizeFilter]
        public ActionResult DisableRecordField(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objRecordFieldRepo.DisableRecordField(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }

        #endregion


        #region EnableRecordField (DELETE)

        [HttpDelete]
        [Route("EnableRecordField")]
        [SessionAuthorizeFilter]
        public ActionResult EnableRecordField(long Id)
        {
            int Modified_ById = GlobalFunction.getLoggedInUser(HttpContext.Request.Headers["Authorization-Token"].First());
            int Role_Modified = GlobalFunction.getLoggedInUserRole(HttpContext.Request.Headers["Authorization-Token"].First());
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Enter Mandatory Fields!";
            objreturn = objRecordFieldRepo.EnableRecordField(Id, Modified_ById);
            return Ok(new { results = objreturn });

        }

        #endregion



    }
}