﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class WorkFlow_Repository
    {
        public DBReturnModel InsertWorkFlow(workflow_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Class!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertWorkFlow";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@subCmpID", objModel.subCmpID);
                    cmd.Parameters.AddWithValue("@Fnc_Domain", objModel.Fnc_Domain);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@createdBy", objModel.createdBy);
                    cmd.Parameters.AddWithValue("@createdOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@workFlowID", objModel.workFlowID);
                    cmd.Parameters.AddWithValue("@workFlowName", objModel.workFlowName);
                    cmd.Parameters.AddWithValue("@TotalDays", objModel.TotalDays);
                    cmd.Parameters.AddWithValue("@workFlowLevel", objModel.workFlowLevel);
                    cmd.Parameters.AddWithValue("@bucketing", objModel.bucketing);
                    cmd.Parameters.AddWithValue("@priority", objModel.priority);
                    cmd.Parameters.AddWithValue("@description", objModel.description);
                    cmd.Parameters.AddWithValue("@RASCI", objModel.RASCI);
                    cmd.Parameters.AddWithValue("@DocCompanyID", objModel.DocCompanyID);
                    cmd.Parameters.AddWithValue("@DocDomainID", objModel.DocDomainID);
                    cmd.Parameters.AddWithValue("@DocClassID", objModel.DocClassID);
                    cmd.Parameters.AddWithValue("@activeStatus", false);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.createdBy, "API", "WorkFlow_Repository/InsertWorkFlow");
                    objReturn.ReturnMessage = "System Error on Inserting Work Flow!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.createdBy, "API", "WorkFlow_Repository/InsertWorkFlow");
                objReturn.ReturnMessage = "System Error on Inserting Work Flow!";
            }

            return objReturn;
        }
        

        public workflow_table_page_Model GetWorkFlowListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            workflow_table_page_Model objModel = new workflow_table_page_Model();
            List<workflow_table_Model> objList = new List<workflow_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectWorkFlowByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    workflow_table_Model tempobj = new workflow_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.subCmpID = Convert.ToInt64(sdr["subCmpID"].ToString());
                    tempobj.Fnc_Domain = Convert.ToInt64(sdr["Fnc_Domain"].ToString());
                    tempobj.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.workFlowName = sdr["workFlowName"].ToString();
                    tempobj.TotalDays = Convert.ToInt32(sdr["TotalDays"].ToString());
                    tempobj.workFlowLevel = sdr["workFlowLevel"].ToString();
                    tempobj.bucketing = sdr["bucketing"].ToString();
                    tempobj.priority = sdr["priority"].ToString();
                    tempobj.description = sdr["description"].ToString();
                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.DocCompanyID = Convert.ToInt64(sdr["DocCompanyID"].ToString());
                    tempobj.DocDomainID = Convert.ToInt64(sdr["DocDomainID"].ToString());
                    tempobj.DocClassID = Convert.ToInt64(sdr["DocClassID"].ToString());
                    tempobj.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    tempobj.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    tempobj.createdBy = Convert.ToInt64(sdr["createdBy"].ToString());

                    tempobj.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    tempobj.editedOn = Convert.ToDateTime(sdr["editedOn"].ToString());
                    tempobj.editedBy = Convert.ToInt64(sdr["editedBy"].ToString());
                    tempobj.deletedOn = Convert.ToDateTime(sdr["deletedOn"].ToString());
                    tempobj.BPCompanyName = sdr["BPCompanyName"].ToString();
                    tempobj.BPDomainName = sdr["BPDomainName"].ToString();
                    tempobj.DocCompanyName = sdr["DocCompanyName"].ToString();
                    tempobj.DocDomainName = sdr["DocDomainName"].ToString();
                    tempobj.DocClassName = sdr["DocClassName"].ToString();
                    tempobj.deletedBy = Convert.ToInt64(sdr["deletedBy"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From workflow_table where (Isnull(workFlowName,'') like '%'+@search+'%' "
                + " or Isnull(workFlowLevel,'') like '%'+@search+'%' "
                + " or Isnull(bucketing,'') like '%'+@search+'%')";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.workflow_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "WorkFlow_Repository/GetWorkFlowListByPage");
            }

            return objModel;

        }
        

        public workflow_table_Model GetOneWorkFlow(long SLNO, long CreatedBy)
        {
            workflow_table_Model objModel = new workflow_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectWorkFlowById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.subCmpID = Convert.ToInt64(sdr["subCmpID"].ToString());
                    objModel.Fnc_Domain = Convert.ToInt64(sdr["Fnc_Domain"].ToString());
                    objModel.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    objModel.workFlowName = sdr["workFlowName"].ToString();
                    objModel.TotalDays = Convert.ToInt32(sdr["TotalDays"].ToString());
                    objModel.workFlowLevel = sdr["workFlowLevel"].ToString();
                    objModel.bucketing = sdr["bucketing"].ToString();
                    objModel.priority = sdr["priority"].ToString();
                    objModel.description = sdr["description"].ToString();
                    objModel.RASCI = sdr["RASCI"].ToString();
                    objModel.DocCompanyID = Convert.ToInt64(sdr["DocCompanyID"].ToString());
                    objModel.DocDomainID = Convert.ToInt64(sdr["DocDomainID"].ToString());
                    objModel.DocClassID = Convert.ToInt64(sdr["DocClassID"].ToString());
                    objModel.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    objModel.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    objModel.createdBy = Convert.ToInt64(sdr["createdBy"].ToString());
                    objModel.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    objModel.editedOn = Convert.ToDateTime(sdr["editedOn"].ToString());
                    objModel.editedBy = Convert.ToInt64(sdr["editedBy"].ToString());
                    objModel.deletedOn = Convert.ToDateTime(sdr["deletedOn"].ToString());
                    objModel.BPCompanyName = sdr["BPCompanyName"].ToString();
                    objModel.BPDomainName = sdr["BPDomainName"].ToString();
                    objModel.DocCompanyName = sdr["DocCompanyName"].ToString();
                    objModel.DocDomainName = sdr["DocDomainName"].ToString();
                    objModel.DocClassName = sdr["DocClassName"].ToString();
                    objModel.deletedBy = Convert.ToInt64(sdr["deletedBy"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "WorkFlow_Repository/GetOneWorkFlow");
            }

            return objModel;

        }
                             

        public DBReturnModel UpdateWorkFlow(workflow_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Updating Document Class!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateWorkFlow";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@subCmpID", objModel.subCmpID);
                    cmd.Parameters.AddWithValue("@Fnc_Domain", objModel.Fnc_Domain);
                    cmd.Parameters.AddWithValue("@editedBy", objModel.createdBy);
                    cmd.Parameters.AddWithValue("@editedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@workFlowID", objModel.workFlowID);
                    cmd.Parameters.AddWithValue("@workFlowName", objModel.workFlowName);
                    cmd.Parameters.AddWithValue("@TotalDays", objModel.TotalDays);
                    cmd.Parameters.AddWithValue("@workFlowLevel", objModel.workFlowLevel);
                    cmd.Parameters.AddWithValue("@bucketing", objModel.bucketing);
                    cmd.Parameters.AddWithValue("@priority", objModel.priority);
                    cmd.Parameters.AddWithValue("@description", objModel.description);
                    cmd.Parameters.AddWithValue("@RASCI", objModel.RASCI);
                    cmd.Parameters.AddWithValue("@DocCompanyID", objModel.DocCompanyID);
                    cmd.Parameters.AddWithValue("@DocDomainID", objModel.DocDomainID);
                    cmd.Parameters.AddWithValue("@DocClassID", objModel.DocClassID);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.createdBy, "API", "WorkFlow_Repository/UpdateWorkFlow");
                    objReturn.ReturnMessage = "System Error on Updating Work Flow!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.createdBy, "API", "WorkFlow_Repository/UpdateWorkFlow");
                objReturn.ReturnMessage = "System Error on Updating Work Flow!";
            }

            return objReturn;
        }





        public DBReturnModel DisableWorkFlow(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Work Flow!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update workflow_table set isDeleted=@isDeleted,"
                        + " DeletedBy=@DeletedBy,deletedOn=@deletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@isDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@deletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Work Flow Disabled Successfully!";

                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/DisableWorkFlow");
                    objreturn.ReturnMessage = "System Error on Disabling Work Flow!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/DisableWorkFlow");
                objreturn.ReturnMessage = "System Error on Disabling Work Flow!";

            }

            return objreturn;
        }




        public DBReturnModel EnableWorkFlow(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Work Flow!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update workflow_table set isDeleted=@isDeleted,"
                        + " DeletedBy=@DeletedBy,deletedOn=@deletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@isDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@deletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Work Flow Enabled Successfully!";

                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/EnableWorkFlow");
                    objreturn.ReturnMessage = "System Error on Enabling Work Flow!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/EnableWorkFlow");
                objreturn.ReturnMessage = "System Error on Enabling Work Flow!";

            }

            return objreturn;
        }





        public long GetMaxProcessIDFromWorkFlow(long CreatedBy)
        {
            long Id = 0;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select (Isnull(Max(workFlowId),0)+1) as workFlowId From workflow_table";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Id = Convert.ToInt64(sdr["workFlowId"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "WorkFlow_Repository/GetMaxProcessIDFromWorkFlow");
            }

            return Id;

        }



    }
}
