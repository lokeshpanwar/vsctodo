﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocumentClass_Repository
    {
        public DBReturnModel InsertDocumentClass(DocumentClass_table_Model objModel, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Class!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentClass";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SubCompanyID", objModel.SubCompanyID);
                    cmd.Parameters.AddWithValue("@ClassDomain", objModel.ClassDomain);
                    cmd.Parameters.AddWithValue("@DocumentClassName", objModel.DocumentClassName);
                    cmd.Parameters.AddWithValue("@DocumentClassDesc", objModel.DocumentClassDesc);
                    cmd.Parameters.AddWithValue("@AllowedExtension", objModel.AllowedExtension);
                    cmd.Parameters.AddWithValue("@AccessControlStatus", objModel.AccessControlStatus);
                    cmd.Parameters.AddWithValue("@VersionControlStatus", objModel.VersionControlStatus);
                    cmd.Parameters.AddWithValue("@DocumentAccessBy", objModel.DocumentAccessBy);
                    cmd.Parameters.AddWithValue("@Archieve_Status", objModel.Archieve_Status);
                    cmd.Parameters.AddWithValue("@DocumentType_ID", objModel.DocumentType_ID);
                    cmd.Parameters.AddWithValue("@print_right", objModel.print_right);
                    cmd.Parameters.AddWithValue("@email_right", objModel.email_right);
                    cmd.Parameters.AddWithValue("@checkin_right", objModel.checkin_right);
                    cmd.Parameters.AddWithValue("@checkout_right", objModel.checkout_right);
                    cmd.Parameters.AddWithValue("@download_right", objModel.download_right);
                    cmd.Parameters.AddWithValue("@upload_right", objModel.upload_right);
                    cmd.Parameters.AddWithValue("@selectall_right", objModel.selectall_right);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                    if (AuthorizationToken != "")
                    {
                        if (objReturn.ReturnStatus == "SUCCESS")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.SubCompanyID.ToString(), objModel.CreatedBy.ToString(), LogType.Insert, "Document Class:" + objModel.SLNO.ToString(), "Document Class Created", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), objModel.CreatedBy);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentClass_Repository/InsertDocumentClass");
                    objReturn.ReturnMessage = "System Error on Inserting Document Class!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentClass_Repository/InsertDocumentClass");
            }

            return objReturn;
        }

        public DocumentClass_table_page_Model GetDocumentClassListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            DocumentClass_table_page_Model objModel = new DocumentClass_table_page_Model();
            List<DocumentClass_table_Model> objList = new List<DocumentClass_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentClassByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentClass_table_Model tempobj = new DocumentClass_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.SubCompanyID = Convert.ToInt64(sdr["SubCompanyID"].ToString());
                    tempobj.ClassDomain = Convert.ToInt64(sdr["ClassDomain"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DocumentClassDesc = sdr["DocumentClassDesc"].ToString();
                    tempobj.AllowedExtension = sdr["AllowedExtension"].ToString();
                    tempobj.AccessControlStatus = Convert.ToBoolean(sdr["AccessControlStatus"].ToString());
                    tempobj.VersionControlStatus = Convert.ToBoolean(sdr["VersionControlStatus"].ToString());
                    tempobj.DocumentAccessBy = sdr["DocumentAccessBy"].ToString();
                    tempobj.Archieve_Status = Convert.ToBoolean(sdr["Archieve_Status"].ToString());
                    tempobj.DocumentType_ID = sdr["DocumentType_ID"].ToString();
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCCompanyName = sdr["DCCompanyName"].ToString();
                    tempobj.DCDomainName = sdr["DCDomainName"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From DocumentClass_table where (Isnull(DocumentClassName,'') like '%'+@search+'%' or "
                        + " Isnull(DocumentClassDesc,'') like '%'+@search+'%' or Isnull(AllowedExtension,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.DocumentClass_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentClass_Repository/GetDocumentClassListByPage");
            }

            return objModel;

        }

        public DocumentClass_table_Model GetOneDocumentClass(long SLNO, long CreatedBy, string AuthorizationToken)
        {
            DocumentClass_table_Model objModel = new DocumentClass_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentClassById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.SubCompanyID = Convert.ToInt64(sdr["SubCompanyID"].ToString());
                    objModel.ClassDomain = Convert.ToInt64(sdr["ClassDomain"].ToString());
                    objModel.DocumentClassName = sdr["DocumentClassName"].ToString();
                    objModel.DocumentClassDesc = sdr["DocumentClassDesc"].ToString();
                    objModel.AllowedExtension = sdr["AllowedExtension"].ToString();
                    objModel.AccessControlStatus = Convert.ToBoolean(sdr["AccessControlStatus"].ToString());
                    objModel.VersionControlStatus = Convert.ToBoolean(sdr["VersionControlStatus"].ToString());
                    objModel.DocumentAccessBy = sdr["DocumentAccessBy"].ToString();
                    objModel.Archieve_Status = Convert.ToBoolean(sdr["Archieve_Status"].ToString());
                    objModel.DocumentType_ID = sdr["DocumentType_ID"].ToString();
                    objModel.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    objModel.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    objModel.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    objModel.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    objModel.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    objModel.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    objModel.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    objModel.DCCompanyName = sdr["DCCompanyName"].ToString();
                    objModel.DCDomainName = sdr["DCDomainName"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                }
                connection.Close();

                if (AuthorizationToken != "")
                {
                    Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.SubCompanyID.ToString(), CreatedBy.ToString(), LogType.View, "Document Class:" + objModel.DocumentClassName, "Document Class Viewed", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), CreatedBy);

                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentClass_Repository/GetOneDocumentClass");
            }

            return objModel;

        }

        public DBReturnModel UpdateDocumentClass(DocumentClass_table_Model objModel, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Updating Document Class!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateDocumentClass";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@SubCompanyID", objModel.SubCompanyID);
                    cmd.Parameters.AddWithValue("@ClassDomain", objModel.ClassDomain);
                    cmd.Parameters.AddWithValue("@DocumentClassName", objModel.DocumentClassName);
                    cmd.Parameters.AddWithValue("@DocumentClassDesc", objModel.DocumentClassDesc);
                    cmd.Parameters.AddWithValue("@AllowedExtension", objModel.AllowedExtension);
                    cmd.Parameters.AddWithValue("@AccessControlStatus", objModel.AccessControlStatus);
                    cmd.Parameters.AddWithValue("@VersionControlStatus", objModel.VersionControlStatus);
                    cmd.Parameters.AddWithValue("@DocumentAccessBy", objModel.DocumentAccessBy);
                    cmd.Parameters.AddWithValue("@Archieve_Status", objModel.Archieve_Status);

                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                    if (AuthorizationToken != "")
                    {
                        if (objReturn.ReturnStatus == "SUCCESS")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.SubCompanyID.ToString(), objModel.ModifiedBy.ToString(), LogType.Update, "Document Class:" + objModel.SLNO.ToString(), "Document Class Viewed", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), objModel.ModifiedBy);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "DocumentClass_Repository/UpdateDocumentClass");
                    objReturn.ReturnMessage = "System Error on Updating Document Class!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "DocumentClass_Repository/UpdateDocumentClass");
            }

            return objReturn;
        }








        public DBReturnModel DisableDocumentClass(long SLNO, long DeletedBy, string AuthorizationToken)
        {
            string result = "Error on Disabling Document Class!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update DocumentClass_Table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Document Class Disabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;

                    if (AuthorizationToken != "")
                    {
                        if (objreturn.ReturnStatus == "SUCCESS")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), GlobalFunction.getLoggedInUserSubCompanyId(AuthorizationToken).ToString(), DeletedBy.ToString(), LogType.Enable, "Document Class:" + SLNO.ToString(), "Document Class Viewed", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), DeletedBy);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentClass_Repository/DisableDocumentClass");
                    objreturn.ReturnMessage = "System Error on Disabling Document Class!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentClass_Repository/DisableDocumentClass");
                objreturn.ReturnMessage = "System Error on Disabling Document Class!";
            }

            return objreturn;
        }

        public DBReturnModel EnableDocumentClass(long SLNO, long DeletedBy, string AuthorizationToken)
        {
            string result = "Error on Enabling Document Class!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update DocumentClass_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Document Class Enabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;

                    if (AuthorizationToken != "")
                    {
                        if (objreturn.ReturnStatus == "SUCCESS")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), GlobalFunction.getLoggedInUserSubCompanyId(AuthorizationToken).ToString(), DeletedBy.ToString(), LogType.Disable, "Document Class:" + SLNO.ToString(), "Document Class Viewed", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), DeletedBy);
                        }
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentClass_Repository/EnableDocumentClass");
                    objreturn.ReturnMessage = "System Error on Enabling Document Class!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentClass_Repository/EnableDocumentClass");
            }

            return objreturn;
        }

        public List<DocumentClass_table_Model> GetDocumentClassListByUserId(long CreatedBy)
        {
            List<DocumentClass_table_Model> objList = new List<DocumentClass_table_Model>();

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentClassByUserId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@UserID", CreatedBy);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentClass_table_Model tempobj = new DocumentClass_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.SubCompanyID = Convert.ToInt64(sdr["SubCompanyID"].ToString());
                    tempobj.ClassDomain = Convert.ToInt64(sdr["ClassDomain"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DocumentClassDesc = sdr["DocumentClassDesc"].ToString();
                    tempobj.AllowedExtension = sdr["AllowedExtension"].ToString();
                    tempobj.AccessControlStatus = Convert.ToBoolean(sdr["AccessControlStatus"].ToString());
                    tempobj.VersionControlStatus = Convert.ToBoolean(sdr["VersionControlStatus"].ToString());
                    tempobj.DocumentAccessBy = sdr["DocumentAccessBy"].ToString();
                    tempobj.Archieve_Status = Convert.ToBoolean(sdr["Archieve_Status"].ToString());
                    tempobj.DocumentType_ID = sdr["DocumentType_ID"].ToString();
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.DCCompanyName = sdr["DCCompanyName"].ToString();
                    tempobj.DCDomainName = sdr["DCDomainName"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentClass_Repository/GetDocumentClassListByUserId");
            }

            return objList;

        }











    }
}
