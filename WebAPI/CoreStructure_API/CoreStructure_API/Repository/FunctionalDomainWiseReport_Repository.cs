﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class FunctionalDomainWiseReport_Repository
    {

        public FunctionalDomainWiseReport_Model GetReportList(long CreatedBy, long SubCompId, long DomainId)
        {
            FunctionalDomainWiseReport_Model objModel = new FunctionalDomainWiseReport_Model();
            List<DocumentClass_Model> objDocClassList = new List<DocumentClass_Model>();
            List<Document_Model> objDocList = new List<Document_Model>();
            List<User_Model> objUserList = new List<User_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                // Get Document Class Count
                string sqlstr = "Select Count(*) as Cnt From DocumentClass_table where SubCompanyID=@SubCompanyID "
                    + " and ClassDomain=@ClassDomain and Isnull(IsDeleted,'')!=@IsDeleted ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.DocumentClassCount = Convert.ToInt64(sdr["Cnt"].ToString());
                }
                sdr.Close();

                // Get Document Count
                sqlstr = "Select Count(*) as Cnt From DocUpload_Master where Sub_CompID=@SubCompanyID "
                    + " and DomainID=@ClassDomain ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.DocumentCount = Convert.ToInt64(sdr["Cnt"].ToString());
                }
                sdr.Close();


                // Get Users Count
                sqlstr = "Select Count(*) as Cnt From Employee_table where Sub_Company=@SubCompanyID "
                    + " and Domain_ID=@ClassDomain and user_type='EMPLOYEE' and Isnull(IsDeleted,'')!=@IsDeleted";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.UserCount = Convert.ToInt64(sdr["Cnt"].ToString());
                }
                sdr.Close();
                

                // Get Document Class List 
                sqlstr = "Select (Select Count(*) From DocUpload_Master where DocUpload_Master.Doc_Class_ID=DocumentClass_table.SLNO  "
                + " and DocUpload_Master.Sub_CompID = @SubCompanyID and DocUpload_Master.DomainID = @ClassDomain) as Cnt, *From DocumentClass_table "
                + " where SubCompanyID = @SubCompanyID  and ClassDomain = @ClassDomain and Isnull(IsDeleted,'')!= @IsDeleted";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentClass_Model tempobj = new DocumentClass_Model();
                    tempobj.ClassDomain = Convert.ToInt64(sdr["ClassDomain"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    tempobj.Documents = Convert.ToInt64(sdr["Cnt"].ToString());
                    objDocClassList.Add(tempobj);
                }
                sdr.Close();
                objModel.DocumentClass_Model_List = objDocClassList;


                // Get Document List 
                sqlstr = "Select FileName, VersionID, FileSize From DocUpload_table where Sub_CompID=@SubCompanyID "
                    + " and DomainID=@DomainID and Isnull(IsDeleted,'')!=@IsDeleted";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Model tempobj = new Document_Model();
                    tempobj.DocumentName = sdr["FileName"].ToString();
                    tempobj.DocumentSize = Convert.ToInt64(sdr["FileSize"].ToString());
                    tempobj.DocumentVersion = sdr["VersionID"].ToString();
                    objDocList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Model_List = objDocList;



                // Get Document List 
                sqlstr = "Select Userid, Email,Isnull((Select Isnull(Userid,'') From Employee_table EMP where  "
                + " Cast(EMP.SLNO as varchar(10)) = Employee_table.Reporting_Manager_Id and "
                + " Employee_table.Reporting_Manager_Id != 'NA'),'') as ManagerId From Employee_table where Sub_Company = @SubCompany "
                + " and Domain_ID = @DomainID and user_type='EMPLOYEE' and Isnull(IsDeleted,'')!= @IsDeleted";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompany", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_Model tempobj = new User_Model();
                    tempobj.Email = sdr["Email"].ToString();
                    tempobj.ReportingManagerId = sdr["ManagerId"].ToString();
                    tempobj.UserName = sdr["Userid"].ToString();
                    objUserList.Add(tempobj);
                }
                sdr.Close();
                objModel.User_Model_List = objUserList;

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "FunctionalDomainWiseReport_Repository/GetReportList");
            }

            return objModel;

        }


    }
}
