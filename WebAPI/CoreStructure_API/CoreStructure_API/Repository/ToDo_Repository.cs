﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class ToDo_Repository
    {
        public List<SelectListItem> GetPriorityList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetPriorityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Priority"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/GetPriorityList");
            }
            return objList;
        }

        public List<SelectListItem> GetStatuslist(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_Getstatus";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["StatusName"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/GetStatuslist");
            }
            return objList;
        }

        public List<SelectListItem> VerticalList(long CreatedBy,int Userid,int VerticalId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_Getallverticals";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", Userid);
                cmd.Parameters.AddWithValue("@Vertical", VerticalId);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["VerticalName"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

        

            public List<SelectListItem> VerticalListUserwise(long CreatedBy, int Userid, int VerticalId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetallverticalsUserWise";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", Userid);
                cmd.Parameters.AddWithValue("@Vertical", VerticalId);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["VerticalName"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

       

        protected static String Dateformat(string e)
        {
            string inputFormat = "dd-MM-yyyy";
            string outputFormat = "yyyy-MM-dd";
            string output = string.Empty;
            if (e == null || e == "")
            {
                output = "";
            }
            else
            {
                var dateTime = DateTime.ParseExact(e.ToString(), inputFormat, CultureInfo.InvariantCulture);
                output = dateTime.ToString(outputFormat);
            }
            return output;
        }

     

        public string SaveTask(Task_Model objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                
                    string sqlstr = "[Sp_SaveTask]";
                    
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TaskId", objModel.Id);
                    cmd.Parameters.AddWithValue("@Date", Dateformat(objModel.Date));
                    cmd.Parameters.AddWithValue("@Title", objModel.Title);
                    cmd.Parameters.AddWithValue("@Description", objModel.Description);
                    cmd.Parameters.AddWithValue("@PriorityId", objModel.PriorityId);
                    cmd.Parameters.AddWithValue("@AssignToId", objModel.AssignToId);
                    cmd.Parameters.AddWithValue("@ClientId", objModel.ClientId);
                    cmd.Parameters.AddWithValue("@ActivityId", objModel.ActivityId);
                    cmd.Parameters.AddWithValue("@SubActivityId", objModel.SubActivityId);
                    cmd.Parameters.AddWithValue("@StartDate", Dateformat(objModel.StartDate));
                    cmd.Parameters.AddWithValue("@EndDate", Dateformat(objModel.EndDate));
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreateBy);
                    cmd.Parameters.AddWithValue("@Projectid", objModel.Projectid);
                    cmd.Parameters.AddWithValue("@Moduleid", objModel.Moduleid);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@SubcategoryId", objModel.SubcategoryId);
                    cmd.Parameters.AddWithValue("@VerticalId", objModel.Verticals);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }

        public List<Task_Model> GetViewTaskList(int UserId, long CreatedBy)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetViewTaskList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@UserId", UserId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Dte"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Priority = Convert.ToString(sdr["PriorityName"].ToString()),
                        AssignTo = Convert.ToString(sdr["Name"].ToString()),
                        //StartDate = Convert.ToString(sdr["StartDte"].ToString()),
                        //EndDate = Convert.ToString(sdr["EndDte"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString()),
                        //PriorityId=Convert.ToInt32(sdr["Priority"].ToString()),
                        //AssignToId=Convert.ToInt32(sdr["AssignTo"].ToString()),
                        Frequency = Convert.ToString(sdr["Frequency"].ToString()),
                        FrequencyId = Convert.ToInt32(sdr["FrequencyId"].ToString()),
                        //Verticals = Convert.ToInt32(sdr["Vertical"].ToString()),
                        Selfassign = Convert.ToInt32(sdr["selfassign"].ToString())
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }

        public string DeleteTask(int TaskId, long logInUser)
        {
            string result = "Error on Deleting Task!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteTask]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TaskId", TaskId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "ToDo_Repository/DeleteTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "ToDo_Repository/DeleteTask");
            }
            return result;
        }

       

        public List<Task_Model> GetTodoList(long CreatedBy, int UserId,int VerticalId)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Gettodolist";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@VerticalId", VerticalId);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        AssignTo = Convert.ToString(sdr["Assignlist"].ToString()),
                        Priority = Convert.ToString(sdr["ShowPriority"].ToString()),
                        StatusId = Convert.ToInt32(sdr["Status"].ToString()),
                        AssignBy = Convert.ToString(sdr["Assignby"].ToString()),
                        Isaccepted = Convert.ToInt32(sdr["Isaccepted"].ToString()),
                        IsActiveaction = Convert.ToInt32(sdr["isEditable"].ToString()),
                        IsReassign = Convert.ToInt32(sdr["Isreassign"].ToString())

                    });

                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

       

        public List<Task_Model> GetTodoDetailList(long CreatedBy, int Id)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "GettodoDetails";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@todoid", Id);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        CreateOn = Convert.ToString(sdr["Createon"].ToString()),
                        AssignTo = Convert.ToString(sdr["Assignto"].ToString()),
                        AssignBy = Convert.ToString(sdr["Assignby"].ToString()),
                        StartDate = Convert.ToString(sdr["Startdate"].ToString()),
                        EndDate = Convert.ToString(sdr["Enddate"].ToString()),
                        Priority = Convert.ToString(sdr["Priority"].ToString()),
                        Status = Convert.ToString(sdr["statusname"].ToString()),
                        Isaccepted = Convert.ToInt32(sdr["IsAccepted"].ToString()),
                        StatusId =Convert.ToInt32(sdr["Status"].ToString()),
                        Todoid = Convert.ToInt32(sdr["ToDoId"].ToString()),
                        Frequency = Convert.ToString(sdr["Frequency"].ToString())
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

        public List<Task_Model> GetAlltaskdetails(long CreatedBy, int Id)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "GettodoDetailsForTask";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@todoid", Id);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        CreateOn = Convert.ToString(sdr["Dates"].ToString()),
                        StartDate = Convert.ToString(sdr["Time"].ToString()),
                        Description = Convert.ToString(sdr["details"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString())
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

        

        public List<Task_Model> Updatetodo(int todoid, string date, string time, int statusId, string description, int empid, long logInUser)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_UpdatetaskStatus";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@todoid", todoid);
                cmd.Parameters.AddWithValue("@date", Dateformat(date));
                cmd.Parameters.AddWithValue("@time", time);
                cmd.Parameters.AddWithValue("@statusId", statusId);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@empid", empid);
                cmd.Parameters.AddWithValue("@logInUser", logInUser);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        ClientId = Convert.ToInt32(sdr["clientid"].ToString()),
                        ActivityId = Convert.ToInt32(sdr["activityid"].ToString()),
                        SubActivityId = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        Projectid = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        Moduleid = Convert.ToInt32(sdr["subactivityid"].ToString()),

                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 1, "API", "ToDo_Repository/Updatetodo");
            }
            return objList;
        }

        public List<Task_Model> AcceptTask(int todoid, int Clientid, int Activityid, int Subactivityid, int projectid, int moduleid, long logInUser)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_Accepttask";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@todoid", todoid);
                cmd.Parameters.AddWithValue("@Clientid", Clientid);
                cmd.Parameters.AddWithValue("@Activityid", Activityid);
                cmd.Parameters.AddWithValue("@Subactivityid", Subactivityid);
                cmd.Parameters.AddWithValue("@projectid", projectid);
                cmd.Parameters.AddWithValue("@moduleid", moduleid);
                cmd.Parameters.AddWithValue("@logInUser", logInUser);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        ClientId = Convert.ToInt32(sdr["clientid"].ToString()),
                        ActivityId = Convert.ToInt32(sdr["activityid"].ToString()),
                        SubActivityId = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        Projectid = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        Moduleid = Convert.ToInt32(sdr["subactivityid"].ToString()),


                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 1, "API", "ToDo_Repository/Updatetodo");
            }
            return objList;
        }

        public List<Task_Model> RejectTask(int todoid, string RejectRemark, long logInUser)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_Rejecttask";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@todoid", todoid);
                cmd.Parameters.AddWithValue("@RejectRemark", RejectRemark);
                cmd.Parameters.AddWithValue("@logInUser", logInUser);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        //ClientId = Convert.ToInt32(sdr["clientid"].ToString()),
                        //ActivityId = Convert.ToInt32(sdr["activityid"].ToString()),
                        //SubActivityId = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        //Projectid = Convert.ToInt32(sdr["subactivityid"].ToString()),
                        //Moduleid = Convert.ToInt32(sdr["subactivityid"].ToString()),


                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 1, "API", "ToDo_Repository/Updatetodo");
            }
            return objList;
        }
        public List<SelectListItem> GetEmployeelist(long CreatedBy, int VerticalId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetEmployees";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Vertical", VerticalId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Name"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/GetStatuslist");
            }
            return objList;
        }

        public string SaveAssignTask(Task_Model objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAssignTask]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    cmd.Parameters.AddWithValue("@Date", Dateformat(objModel.Date));
                    cmd.Parameters.AddWithValue("@Title", objModel.Title);
                    cmd.Parameters.AddWithValue("@Description", objModel.Description);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreateBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }

        public string SaveAssignTaskDetails(Task_Model objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAssignTaskDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Todoid", objModel.Todoid);
                    cmd.Parameters.AddWithValue("@AssignTo", objModel.AssignTo);
                    cmd.Parameters.AddWithValue("@StartDate", Dateformat(objModel.StartDate));
                    cmd.Parameters.AddWithValue("@EndDate", Dateformat(objModel.EndDate));
                    cmd.Parameters.AddWithValue("@Status", objModel.Status);
                    cmd.Parameters.AddWithValue("@PriorityId", objModel.PriorityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreateBy);
                    cmd.Parameters.AddWithValue("@vertical", objModel.Verticals);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }

        public string GetTotalTasks(int Verticalid, long EmpId,long logInUser)
        {
            string result = "";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_GetTotalTasks]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Verticalid", Verticalid);
                    cmd.Parameters.AddWithValue("@EmpId", EmpId);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetLatestPOSCode");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetLatestPOSCode");
            }
            return result;
        }

        public List<Task_Model> ViewTotalTaskDetails(int Verticalid,int EmpId, long CreatedBy)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_ViewTotalTaskDetails";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@Verticalid", Verticalid);
                cmd.Parameters.AddWithValue("@EmpId", EmpId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Dte"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Priority = Convert.ToString(sdr["PriorityName"].ToString()),
                        AssignTo = Convert.ToString(sdr["Name"].ToString()),
                        StartDate = Convert.ToString(sdr["StartDte"].ToString()),
                        EndDate = Convert.ToString(sdr["EndDte"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString()),
                        AssignBy = Convert.ToString(sdr["AssignBy"].ToString())
                        //PriorityId = Convert.ToInt32(sdr["Priority"].ToString()),
                        //AssignToId = Convert.ToInt32(sdr["AssignTo"].ToString())

                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }


        public List<Task_Model> GetTodoListFilterWise(long CreatedBy, int UserId, int VerticalId)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Gettodolistfilterwise";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@VerticalId", VerticalId);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        AssignTo = Convert.ToString(sdr["Assignlist"].ToString()),
                        Priority = Convert.ToString(sdr["ShowPriority"].ToString()),
                        StatusId = Convert.ToInt32(sdr["Status"].ToString()),
                        AssignBy = Convert.ToString(sdr["Assignby"].ToString()),

                        Isaccepted = Convert.ToInt32(sdr["Isaccepted"].ToString()),

                    });

                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ToDo_Repository/getverticals");
            }
            return objList;
        }

        public List<Task_Model> GetReassignDataList(int Id, long CreatedBy)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetReassignTaskList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@Id", Id);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Dte"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Priority = Convert.ToString(sdr["PriorityName"].ToString()),
                        AssignTo = Convert.ToString(sdr["Name"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString()),
                        PriorityId = Convert.ToInt32(sdr["PriorityId"].ToString()),
                        AssignToId = Convert.ToInt32(sdr["AssignTo"].ToString()),
                        Frequency = Convert.ToString(sdr["Frequency"].ToString()),
                        FrequencyId = Convert.ToInt32(sdr["FrequencyId"].ToString())
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }
        public string SaveReAssignTaskDetails(Task_Model objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveReAssignTaskDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Todoid", objModel.Todoid);
                    cmd.Parameters.AddWithValue("@AssignTo", objModel.AssignTo);
                    cmd.Parameters.AddWithValue("@StartDate", Dateformat(objModel.StartDate));
                    cmd.Parameters.AddWithValue("@EndDate", Dateformat(objModel.EndDate));
                    cmd.Parameters.AddWithValue("@Status", objModel.Status);
                    cmd.Parameters.AddWithValue("@PriorityId", objModel.PriorityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreateBy);
                    cmd.Parameters.AddWithValue("@vertical", objModel.Verticals);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }


        public List<Task_Model> GetEditTaskList(int UserId, long CreatedBy,int TaskId)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetEditTaskList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@TaskId", TaskId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Dte"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Priority = Convert.ToString(sdr["PriorityName"].ToString()),
                        AssignTo = Convert.ToString(sdr["Name"].ToString()),
                        StartDate = Convert.ToString(sdr["StartDte"].ToString()),
                        EndDate = Convert.ToString(sdr["EndDte"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString()),
                        PriorityId = Convert.ToInt32(sdr["Priority"].ToString()),
                        AssignToId = Convert.ToInt32(sdr["AssignTo"].ToString()),
                        Frequency = Convert.ToString(sdr["Frequency"].ToString()),
                        FrequencyId = Convert.ToInt32(sdr["FrequencyId"].ToString()),
                        Verticals = Convert.ToInt32(sdr["Vertical"].ToString()),
                        PriorityText = Convert.ToString(sdr["PriorityName"].ToString()),
                        VerticalText = Convert.ToString(sdr["VerticalName"].ToString()),
                        EmployeeText = Convert.ToString(sdr["Name"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ActivityId = Convert.ToInt32(sdr["ActivityId"].ToString()),
                        SubActivityId = Convert.ToInt32(sdr["SubActivityId"].ToString()),
                        Projectid = Convert.ToInt32(sdr["ProjectId"].ToString()),
                        Moduleid = Convert.ToInt32(sdr["ModuleId"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["ActivityId"].ToString()),
                        SubcategoryId = Convert.ToInt32(sdr["SubActivityId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }

        public List<Task_Model> GetEditAssignTaskList(int UserId, long CreatedBy, int TaskId)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetEditAssignTaskList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@TaskId", TaskId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {
                        Id = Convert.ToInt32(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Dte"].ToString()),
                        Title = Convert.ToString(sdr["Title"].ToString()),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Priority = Convert.ToString(sdr["PriorityName"].ToString()),
                        AssignTo = Convert.ToString(sdr["Name"].ToString()),
                        StartDate = Convert.ToString(sdr["StartDte"].ToString()),
                        EndDate = Convert.ToString(sdr["EndDte"].ToString()),
                        Status = Convert.ToString(sdr["StatusName"].ToString()),
                        PriorityId = Convert.ToInt32(sdr["Priority"].ToString()),
                        AssignToId = Convert.ToInt32(sdr["AssignTo"].ToString()),
                        Frequency = Convert.ToString(sdr["Frequency"].ToString()),
                        FrequencyId = Convert.ToInt32(sdr["FrequencyId"].ToString()),
                        Verticals = Convert.ToInt32(sdr["Vertical"].ToString()),
                        PriorityText = Convert.ToString(sdr["PriorityName"].ToString()),
                        VerticalText = Convert.ToString(sdr["VerticalName"].ToString()),
                        EmployeeText = Convert.ToString(sdr["Name"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }

        public string UpdateAssignTaskDetails(Task_Model objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_UpdateAssignTaskDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Todoid", objModel.Todoid);
                    cmd.Parameters.AddWithValue("@AssignTo", objModel.AssignTo);
                    cmd.Parameters.AddWithValue("@StartDate", Dateformat(objModel.StartDate));
                    cmd.Parameters.AddWithValue("@EndDate", Dateformat(objModel.EndDate));
                    cmd.Parameters.AddWithValue("@Status", objModel.Status);
                    cmd.Parameters.AddWithValue("@PriorityId", objModel.PriorityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreateBy);
                    cmd.Parameters.AddWithValue("@vertical", objModel.Verticals);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }
        public List<Task_Model> GetUserDetail(int UserId, long CreatedBy)
        {
            List<Task_Model> objList = new List<Task_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetUserDetail";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@UserId", UserId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Task_Model
                    {

                        EmailId = Convert.ToString(sdr["Email"].ToString()),
                        Mobile = Convert.ToString(sdr["Mobile"].ToString()),
                        Name = Convert.ToString(sdr["Name"].ToString())
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }
    }
}
