﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class VSCCO_Repository
    {
        public List<SelectListItem> VscClientlist(int officeid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionVSCCO();
                con.Open();

                string sqlstr = "Sp_GetClientList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", officeid);
                cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["ID"].ToString()),
                        Text = sdr["ClientName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/Getclientlist");
            }
            return objList;
        }

        public List<SelectListItem> VSCCOActivity(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionVSCCO();
                con.Open();

                string sqlstr = "Sp_GetAllActivityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["ID"].ToString()),
                        Text = sdr["ActivityName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/vsccoactivitylist");
            }
            return objList;
        }

        public List<SelectListItem> VSCCOSubACtivity(int officeId, int activityid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionVSCCO();
                con.Open();

                string sqlstr = "Sp_GetAllSubActivtyist";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@ActivityId", activityid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["ID"].ToString()),
                        Text = sdr["SubActivityName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/vsccosubactivitylist");
            }
            return objList;
        }

    

        public List<SelectListItem> UserList(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionVSCCO();
                con.Open();

                string sqlstr = "Sp_GetUsersGridData";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@UserId", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["ID"].ToString()),
                        Text = sdr["FirstName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/vsccoactivitylist");
            }
            return objList;
        }


        public string Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid)
        {
            string result = "Error on Deleting Task!";
            try
            {
                var connection = SqlHelper.ConnectionVSCCO();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_FilldatainSheetByTodo]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empid", empid);
                    cmd.Parameters.AddWithValue("@date", dateFormat(date));
                    cmd.Parameters.AddWithValue("@time", time);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@clientid", clientid);
                    cmd.Parameters.AddWithValue("@activityid", activityid);
                    cmd.Parameters.AddWithValue("@subactivityid", subactivityid);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
            }
            return result;
        }

        public String dateFormat(object e)
        {
            string inputFormat = "dd-MM-yyyy";
            string outputFormat = "yyyy-MM-dd";
            var dateTime = DateTime.ParseExact(e.ToString(), inputFormat, CultureInfo.InvariantCulture);
            string output = dateTime.ToString(outputFormat);
            return output;

        }

        public VSCUserData_Table VSCCOUserData(int Userid)
        {

            VSCUserData_Table objList = new VSCUserData_Table();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetUsersData";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserId", Userid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.results.Name = sdr["Name"].ToString();
                    objList.results.MobileNo = sdr["Mobile"].ToString();
                    objList.results.EmailId = sdr["Email"].ToString();
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Userid, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;

        }

      
    }
}
