﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class CompanyHoliday_Repository
    {
        public DBReturnModel InsertCompanyHoliday(CompanyHoliday_Model objModel)
        {
            string result = "Error on Inserting CompanyHoliday";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                   
                        string sqlstr = "CB_SP_InsertCompanyHoliday";

                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@CompanyId", "KP1018");
                        cmd.Parameters.AddWithValue("@YearStartDate", Convert.ToDateTime(objModel.YearStartDate));
                        cmd.Parameters.AddWithValue("@YearEndDate", objModel.YearEndDate);
                        cmd.Parameters.AddWithValue("@HolidaysDate", objModel.HolidaysDate);
                        cmd.Parameters.AddWithValue("@Workingdays", objModel.Workingdays);
                        cmd.Parameters.AddWithValue("@isdeleted", false);


                        var returncode = new SqlParameter();
                        returncode.ParameterName = "retMessage";
                        returncode.SqlDbType = SqlDbType.VarChar;
                        returncode.Size = 500;
                        returncode.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(returncode);

                        var retStatus = new SqlParameter();
                        retStatus.ParameterName = "retStatus";
                        retStatus.SqlDbType = SqlDbType.VarChar;
                        retStatus.Size = 500;
                        retStatus.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(retStatus);
                        cmd.ExecuteNonQuery();
                        transaction.Commit();

                        objreturn.ReturnMessage = returncode.Value.ToString();
                        objreturn.ReturnStatus = retStatus.Value.ToString();

                        connection.Close();
                    

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, 0, "API", "CompanyHoliday_Repository/InsertCompanyHoliday");
                    objreturn.ReturnMessage = "System Error on Inserting CompanyHoliday";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "CompanyHoliday_Repository/InsertCompanyHoliday");
            }
            return objreturn;

        }


        public CompanyHoliday_Model GetOneCompanyHoliday(long Entryid, long creationuser)
        {
            CompanyHoliday_Model objModel = new CompanyHoliday_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectCompanyHolidayListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Entryid", Entryid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Entryid = Convert.ToInt64(sdr["Entryid"].ToString());
                    objModel.CompanyId = sdr["CompanyId"].ToString();
                    objModel.YearStartDate = Convert.ToDateTime(sdr["YearStartDate"].ToString());
                    objModel.YearEndDate = Convert.ToDateTime(sdr["YearEndDate"].ToString());
                    objModel.HolidaysDate = sdr["HolidaysDate"].ToString();
                    objModel.Workingdays = Convert.ToInt32(sdr["Workingdays"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                   
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "CompanyHoliday_Repository/GetOneCompanyHoliday");
            }
            return objModel;
        }


        public DBReturnModel UpdateCompanyHoliday(CompanyHoliday_Model objModel)
        {
            string result = "Error on Updating CompanyHoliday";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_UpdateCompanyHoliday";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Entryid", objModel.Entryid);
                    cmd.Parameters.AddWithValue("@CompanyId", "KP1018");
                    cmd.Parameters.AddWithValue("@YearStartDate", Convert.ToDateTime(objModel.YearStartDate));
                    cmd.Parameters.AddWithValue("@YearEndDate", objModel.YearEndDate);
                    cmd.Parameters.AddWithValue("@HolidaysDate", objModel.HolidaysDate);
                    cmd.Parameters.AddWithValue("@Workingdays", objModel.Workingdays);
                    cmd.Parameters.AddWithValue("@isdeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, 0, "API", "CompanyHoliday_Repository/UpdateCompanyHoliday");
                    objreturn.ReturnMessage = "System Error on Updating CompanyHoliday";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "CompanyHoliday_Repository/UpdateCompanyHoliday");
            }
            return objreturn;

        }


        public DBReturnModel DisableCompanyHoliday(long Entryid, long modifactionuser)
        {

            string result = "Error on Disabling CompanyHoliday";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update CompanyHoliday set isdeleted=@isdeleted   where Entryid=@Entryid";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Entryid", Entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                   
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "CompanyHoliday Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "CompanyHoliday_Repository/DisableCompanyHoliday");
                    objreturn.ReturnMessage = "System Error on Disabling CompanyHoliday";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "CompanyHoliday_Repository/DisableCompanyHoliday");
            }

            return objreturn;
        }

        public DBReturnModel EnableCompanyHoliday(long Entryid, long modifactionuser)
        {

            string result = "Error on Enabling CompanyHoliday";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update CompanyHoliday set isdeleted=@isdeleted   where Entryid=@Entryid";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Entryid", Entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", false);

                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "CompanyHoliday Enabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "CompanyHoliday_Repository/EnableCompanyHoliday");
                    objreturn.ReturnMessage = "System Error on Enabling CompanyHoliday";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "CompanyHoliday_Repository/EnableCompanyHoliday");
            }

            return objreturn;
        }

        public CompanyHoliday_page_Model GetCompanyHolidayByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            CompanyHoliday_page_Model objModel = new CompanyHoliday_page_Model();
            List<CompanyHoliday_Model> objlist = new List<CompanyHoliday_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Entryid";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectCompanyHolidayByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CompanyHoliday_Model objtemp = new CompanyHoliday_Model();
                    objtemp.Entryid = Convert.ToInt64(sdr["Entryid"].ToString());
                   
                    objtemp.CompanyId = sdr["CompanyId"].ToString();
                    objtemp.YearStartDate = Convert.ToDateTime(sdr["YearStartDate"].ToString());
                    objtemp.YearEndDate = Convert.ToDateTime(sdr["YearEndDate"].ToString());
                    objtemp.HolidaysDate = sdr["HolidaysDate"].ToString();
                    objtemp.Workingdays = Convert.ToInt32(sdr["Workingdays"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From CompanyHoliday where (Isnull(HolidaysDate,'') like '%'+@search+'%' "
                + " or Isnull(Workingdays,'') like '%'+@search+'%') ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.CompanyHoliday_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "CompanyHoliday_Repository/GetCompanyHolidayByPage");

            }

            return objModel;
        }

    }
}
