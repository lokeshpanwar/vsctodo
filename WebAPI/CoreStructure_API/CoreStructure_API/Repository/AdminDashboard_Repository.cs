﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class AdminDashboard_Repository
    {

        public AdminDashboard_Model GetTotalallRecord(long CreatedBy)
        {
            AdminDashboard_Model objModel = new AdminDashboard_Model();
            List<DocumentClassAdminDash_Model> objDocClassList = new List<DocumentClassAdminDash_Model>();

            List<CheckList_Model> objChkList = new List<CheckList_Model>();
            List<Event_Model> objEvtList = new List<Event_Model>();
            List<Emp_CompanyWise_Model> objNoOfEmpList = new List<Emp_CompanyWise_Model>();

            List<Emp_DepartmentWise_Model> objNoOfEmpList2 = new List<Emp_DepartmentWise_Model>();
            List<RecentlyViewDoc_Model> objRecentlyViewDocList = new List<RecentlyViewDoc_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "select count(*) as TotalSubcompany  from Sub_Company_table where IsDeleted=0";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalSubcompany = Convert.ToInt32(sdr["TotalSubcompany"].ToString());
                   
                }
                // get Domain
                sdr.Close();
                     sqlstr = "select count(*) as TotalDomain  from Domain_table where IsDeleted=0";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
               
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalDomain = Convert.ToInt32(sdr["TotalDomain"].ToString());
                }
                sdr.Close();
               
                sqlstr = "select count(*) as TotalRole from Role_table where IsDeleted=0";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRole = Convert.ToInt32(sdr["TotalRole"].ToString());
                }
                sdr.Close();

                sqlstr = "select count(*) as TotalUser from Employee_table where IsDeleted=0";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalUser = Convert.ToInt32(sdr["TotalUser"].ToString());
                }

                sdr.Close();


                sqlstr = "select count(*) as TotalDocument from DocumentClass_table where IsDeleted=0";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalDocument = Convert.ToInt32(sdr["TotalDocument"].ToString());
                }

                sdr.Close();

                // Get Document Class List 
                sqlstr = "select *  from DocumentClass_table where IsDeleted=0 order by SLNO desc";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
        
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentClassAdminDash_Model tempobj = new DocumentClassAdminDash_Model();
                   
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DocumentClassDesc = sdr["DocumentClassDesc"].ToString();
                    tempobj.SLNO = Convert.ToInt32(sdr["SLNO"].ToString());
                    objDocClassList.Add(tempobj);
                }
                sdr.Close();
                objModel.DocumentClassAdminDash_Model_List = objDocClassList;

                //Get Check List

              
                sqlstr = "select *  from Checklist_Table where IsDeleted=0 order by SLNO desc";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    CheckList_Model tempobj = new CheckList_Model();

                    tempobj.Checklist_Name = sdr["Checklist_Name"].ToString();
                    
                    objChkList.Add(tempobj);
                }
                sdr.Close();
                objModel.CheckList_Model_List = objChkList;

                // Get Event list

                sqlstr = "select *  from Event_Table where IsDeleted=0 order by SLNO desc";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Event_Model tempobj = new Event_Model();

                    tempobj.EventTitle = sdr["EventTitle"].ToString();

                    objEvtList.Add(tempobj);
                }
                sdr.Close();
                objModel.Event_Model_List = objEvtList;

                // get No Of employee CompanyWise
                sqlstr = "SELECT SubCompanyName AS SubCompanyName,COUNT(*) AS TotalEmp FROM Sub_Company_table " +
                    "INNER JOIN Employee_table ON Employee_table.Sub_Company = Sub_Company_table.SLNO  " +
                    "GROUP BY Sub_Company_table.SLNO, SubCompanyName ORDER BY SubCompanyName";
                    



                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Emp_CompanyWise_Model tempobj = new Emp_CompanyWise_Model();

                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.TotalEmp = sdr["TotalEmp"].ToString();

                    objNoOfEmpList.Add(tempobj);
                }
                sdr.Close();
                objModel.Emp_CompanyWise_Model_List = objNoOfEmpList;


                // get No Of employee DepartmentWise
                sqlstr = "SELECT Domain_Name AS Domain_Name,COUNT(*) AS TotalEmp FROM Domain_table  " +
                    "INNER JOIN Employee_table ON Employee_table.Domain_ID = Domain_table.SLNO " +
                    "GROUP BY Domain_table.SLNO, Domain_Name ORDER BY Domain_Name";




                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Emp_DepartmentWise_Model tempobj = new Emp_DepartmentWise_Model();

                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.TotalEmp = sdr["TotalEmp"].ToString();

                    objNoOfEmpList2.Add(tempobj);
                }
                sdr.Close();
                objModel.Emp_DepartmentWise_Model_List = objNoOfEmpList2;


                // get recently viewd document

                sqlstr = "SELECT Top 5 Operation_Performed  FROM [Log_Table] WHERE [OperationType]='View' or OperationType='View File'  ORDER BY [SLNO] DESC";
                
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    RecentlyViewDoc_Model tempobj = new RecentlyViewDoc_Model();

                    tempobj.Operation_Performed = sdr["Operation_Performed"].ToString();


                    objRecentlyViewDocList.Add(tempobj);
                }
                sdr.Close();
                objModel.RecentlyViewDoc_Model_List = objRecentlyViewDocList;
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AdminDashboard_Repository/GetTotalallRecord");
            }

            return objModel;

        }

    }
}
