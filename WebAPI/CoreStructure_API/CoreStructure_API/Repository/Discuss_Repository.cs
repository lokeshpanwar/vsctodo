﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Discuss_Repository
    {
        Employee_Repository objEmpRepo = new Employee_Repository();

        public DBReturnModel InsertDiscussMaster(Discuss_Model objModel)
        {
            string result = "Error on Inserting Discuss Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertDiscussMaster";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DiscussionTitle", objModel.DiscussionTitle);
                    cmd.Parameters.AddWithValue("@DiscussionDetail", objModel.DiscussionDetail);
                    cmd.Parameters.AddWithValue("@DiscussionDomain", objModel.DiscussionDomain);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@interval", objModel.interval);
                    cmd.Parameters.AddWithValue("@DisWithUsers", objModel.DisWithUsers);
                    cmd.Parameters.AddWithValue("@ParentID", objModel.ParentID);

                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/InsertDiscussMaster");
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/InsertDiscussMaster");
            }
            return objreturn;

        }



        public Discuss_Model GetOneDiscuss(long SLNO, long CreatedBy)
        {
            Discuss_Model objModel = new Discuss_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectDiscussListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.DiscussionTitle = sdr["DiscussionTitle"].ToString();
                    objModel.DiscussionDetail = sdr["DiscussionDetail"].ToString();
                    objModel.DiscussionDomain = Convert.ToInt32(sdr["DiscussionDomain"].ToString());
                    objModel.interval = Convert.ToInt32(sdr["interval"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objModel.DisWithUsers = sdr["DisWithUsers"].ToString();
                    objModel.ParentID = Convert.ToInt64(sdr["ParentID"].ToString());
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "select *  from Discuss_Table   where ParentID=@ParentID ";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@ParentID", SLNO);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {


                    objModel.ParentID = Convert.ToInt64(sdr["ParentID"].ToString());



                }
                sdr.Close();
                connection.Close();
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Discuss_Repository/GetOneDiscuss");
            }
            return objModel;
        }


        public DBReturnModel UpdateDiscussMaster(Discuss_Model objModel)
        {
            string result = "Error on Updating Discuss Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateDiscussMaster";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                   
                    cmd.Parameters.AddWithValue("@DiscussionTitle", objModel.DiscussionTitle);
                    cmd.Parameters.AddWithValue("@DiscussionDetail", objModel.DiscussionDetail);
                    cmd.Parameters.AddWithValue("@DiscussionDomain", objModel.DiscussionDomain);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@interval", objModel.interval);
                    cmd.Parameters.AddWithValue("@DisWithUsers", objModel.DisWithUsers);
                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/UpdateDiscussMaster");
                    objreturn.ReturnMessage = "System Error on Updating Discuss Master";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/UpdateDiscussMaster");
            }
            return objreturn;
        }


        public DBReturnModel DisableDiscussMaster(long SLNO, long DeletedBy) //for close
        {

            string result = "Error on Closing Discuss ";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Discuss_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Discuss closed Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/DisableDiscussMaster");
                    objreturn.ReturnMessage = "System Error on Closing Discuss !";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/DisableDiscussMaster");
            }

            return objreturn;
        }


        public DBReturnModel EnableDiscussMaster(long SLNO, long DeletedBy)// for open
        {

            string result = "Error on Opening Discuss ";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Discuss_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Discuss Opened Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/EnableDiscussMaster");
                    objreturn.ReturnMessage = "System Error on Opening Discuss";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/EnableDiscussMaster");
            }

            return objreturn;
        }


        public Discuss_page_Model GetDiscussMasterByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Discuss_page_Model objModel = new Discuss_page_Model();
            List<Discuss_Model> objlist = new List<Discuss_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectDiscussMasterByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Discuss_Model objtemp = new Discuss_Model();
                    objtemp.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objtemp.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objtemp.DiscussionTitle = sdr["DiscussionTitle"].ToString();
                    objtemp.DiscussionDetail =sdr["DiscussionDetail"].ToString();
                    objtemp.DiscussionDomain =Convert.ToInt64( sdr["DiscussionDomain"].ToString());
                    objtemp.interval = Convert.ToInt32(sdr["interval"].ToString());
                    objtemp.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objtemp.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objtemp.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objtemp.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objtemp.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                   // objtemp.CompanyName = sdr["CompanyName"].ToString();
                   // objtemp.DomainName = sdr["DomainName"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From Discuss_Table where (Isnull(DiscussionTitle,'') like '%'+@search+'%') ";
               
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.Discuss_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Discuss_Repository/GetDiscussMasterByPage");

            }

            return objModel;
        }

        public DBReturnModel UpdateIntervalValue(long SLNO,int interval, long DeletedBy)
        {

            string result = "Error on Updating Interval";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Discuss_Table set interval=@interval where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@interval", interval);
                   
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Interval Updated Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/UpdateIntervalValue");
                    objreturn.ReturnMessage = "System Error on Updating Interval!";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Discuss_Repository/UpdateIntervalValue");
            }

            return objreturn;
        }

        public Discuss_page_Model GetDiscussList(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Discuss_page_Model objModel = new Discuss_page_Model();
            List<Discuss_Model> objlist = new List<Discuss_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectDiscussList", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Discuss_Model objtemp = new Discuss_Model();
                    objtemp.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                   
                    objtemp.DiscussionTitle = sdr["DiscussionTitle"].ToString();
                    objtemp.DiscussionDetail = sdr["DiscussionDetail"].ToString();
                    objtemp.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objtemp.EmployeeName =sdr["EmployeeName"].ToString();
                    objtemp.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());

                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From Discuss_Table  where Isnull(DiscussionTitle,'') like '%'+@search+'%'  or  "+

                 " (Select EmployeeName From Employee_table where Employee_table.SLNO = Discuss_Table.CreatedBy) like '%'+@search+'%'  ";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.Discuss_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Discuss_Repository/GetDiscussMasterByPage");

            }

            return objModel;
        }


        // Discussion Message  Start
        public DBReturnModel InsertDiscussMessage(Discussion_Message_Model objModel)
        {
            string result = "Error on Submitting Message!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertDiscussMessage";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Discussion_ID", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Userid", objModel.Userid);
                    cmd.Parameters.AddWithValue("@Message", objModel.Message);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                  


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/InsertDiscussMessage");
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Discuss_Repository/InsertDiscussMessage");
            }
            return objreturn;

        }

        public Discussion_Message_page_Model GetOneDiscussMessage(long discussionId, long CreatedBy,long userid)
        {
            Discussion_Message_page_Model objModel = new Discussion_Message_page_Model();
            List<Discussion_Message_Model> objlist = new List<Discussion_Message_Model>();
           
            try
            {

                //Discussion_Message_Model objtemp = new Discussion_Message_Model();
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectDiscussMsgListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Discussion_ID", discussionId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Discussion_Message_Model objtemp = new Discussion_Message_Model();
                    objtemp.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objtemp.Discussion_ID = Convert.ToInt64(sdr["Discussion_ID"].ToString());
                    objtemp.EmployeeName = sdr["EmployeeName"].ToString();
                    objtemp.Message = sdr["Message"].ToString();
                    objtemp.Userid = sdr["Userid"].ToString();
                    objtemp.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objtemp.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                   
                    objlist.Add(objtemp);
                }
                sdr.Close();
               
                cmd.Parameters.Clear();
                cmd.CommandText = "    select SUM(upvote)  as upvote,SUM(downvote) as downvote,Discuss_ID  from vw_discussVote_count where Discuss_ID=@Discuss_ID group by Discuss_ID ";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Discuss_ID", discussionId);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {

                    
                    objModel.upvote = Convert.ToInt32(sdr["upvote"].ToString());
                    objModel.downvote = Convert.ToInt32(sdr["downvote"].ToString());

                    
                }
                sdr.Close();

                //get New Topic

                cmd.Parameters.Clear();
                cmd.CommandText = "SELECT SLNO,DiscussionTitle  from Discuss_Table where ParentID=@ParentID and ParentID >0";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@ParentID", discussionId);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {


                    objModel.newtopic =sdr["DiscussionTitle"].ToString();
                   


                }
                sdr.Close();

                connection.Close();

                objModel.Discussion_Message_Model_List = objlist;

                var onemployee = objEmpRepo.GetOneEmployee(userid, CreatedBy);

                objModel.Userid = onemployee.Userid;
                objModel.SLNO = discussionId;
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Discuss_Repository/GetOneDiscussMessage");
            }
            return objModel;
        }

        public Discuss_Model GetemailIdByUser(long Id, long CreatedBy)
        {
            Discuss_Model objModel = new Discuss_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_GetemailIdByUser", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CreatedBy", Id);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                   
                     objModel.Email = sdr["Email"].ToString();
                }

                connection.Close();
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Discuss_Repository/GetemailIdByUser");
            }
            return objModel;
        }

        // Discussion Message  End


        //Discussion Vote Start
        public DBReturnModel InsertDiscuss_Vote(Discuss_Vote_Model objModel)
        {
            string result = "Error on Submitting Message!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertDiscuss_Vote";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Discuss_ID", objModel.Discuss_ID);
                    cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
                    cmd.Parameters.AddWithValue("@Up", objModel.Up);
                    cmd.Parameters.AddWithValue("@Down", objModel.Down);
                    cmd.Parameters.AddWithValue("@Creadedon", StandardDateTime.GetDateTime());
                   



                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.UserID, "API", "Discuss_Repository/InsertDiscuss_Vote");
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.UserID, "API", "Discuss_Repository/InsertDiscuss_Vote");
            }
            return objreturn;

        }

        // Discussion Vote End


         
    }
}
