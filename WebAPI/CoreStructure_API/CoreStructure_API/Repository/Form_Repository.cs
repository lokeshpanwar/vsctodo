﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Form_Repository
    {

        public string InsertForm(Form_Table_Model objModel)
        {
            string result = "Error on Inserting Form!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertForm";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@FormName", objModel.FormName);
                    cmd.Parameters.AddWithValue("@FormDescription", objModel.FormDescription);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Form_Repository/InsertForm");
                    result = "System Error on Inserting Form";
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Form_Repository/InsertForm");
                result = "System Error on Inserting Form";
            }

            return result;
        }



        public Form_Table_Model GetOneForm(long SLNO, long CreatedBy)
        {
            Form_Table_Model objModel = new Form_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    objModel.FormDescription = sdr["FormDescription"].ToString();
                    objModel.FormName = sdr["FormName"].ToString();

                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Form_Repository/GetOneForm");
            }

            return objModel;

        }



        public List<Form_Table_Model> GetFormList(long CreatedBy)
        {
            List<Form_Table_Model> objModel = new List<Form_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", 0);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Form_Table_Model tempobj = new Form_Table_Model();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.FormDescription = sdr["FormDescription"].ToString();
                    tempobj.FormName = sdr["FormName"].ToString();

                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Form_Repository/GetFormList");
            }

            return objModel;

        }



        public string UpdateForm(Form_Table_Model objModel)
        {
            string result = "Error on Updating Form!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateForm";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@FormName", objModel.FormName);
                    cmd.Parameters.AddWithValue("@FormDescription", objModel.FormDescription);
                    //cmd.Parameters.AddWithValue("@FormEmail", objModel.FormEmail);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Form_Repository/UpdateForm");
                    result = "System Error on updating Form";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Form_Repository/UpdateForm");
                result = "System Error on updating Form";
            }

            return result;
        }



        public Form_table_page_Model GetFormListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Form_table_page_Model objModel = new Form_table_page_Model();
            List<Form_Table_Model> objList = new List<Form_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Form_Table_Model tempobj = new Form_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.FormDescription = sdr["FormDescription"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Form_table where (Isnull(FormName,'') like '%'+@search+'%' or "
                        + " Isnull(FormDescription,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Form_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Form_Repository/GetFormListByPage");
            }

            return objModel;

        }



        public string DisableForm(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Form!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Form_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Form Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Form_Repository/DisableForm");
                    result = "System Error on Disabling Form";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Form_Repository/DisableForm");
                result = "System Error on Disabling Form";
            }

            return result;
        }




        public string EnableForm(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Form!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Form_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Form Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Form_Repository/EnableForm");
                    result = "System Error on Enabling Form";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Form_Repository/EnableForm");
                result = "System Error on Enabling Form";
            }

            return result;
        }


    }

}
