﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocClassUserMapping_Repository
    {


        public DBReturnModel InsertDocClassUserMapping(User_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Class User Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocClassUserMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("Document_ClassID", objModel.Document_ClassID);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("view_right", false);
                    cmd.Parameters.AddWithValue("print_right", false);
                    cmd.Parameters.AddWithValue("email_right", false);
                    cmd.Parameters.AddWithValue("checkin_right", false);
                    cmd.Parameters.AddWithValue("checkout_right", false);
                    cmd.Parameters.AddWithValue("download_right", false);
                    cmd.Parameters.AddWithValue("upload_right", false);
                    cmd.Parameters.AddWithValue("selectall_right", false);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.DocClass_User";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    if (objModel.Emp_UserDet_List != null)
                    {
                        foreach (var dta in objModel.Emp_UserDet_List)
                        {
                            if (dta.IsSelected == true)
                            {
                                dataTable.Rows.Add(dta.User_ID); // Id of '1' is valid for the Person table
                            }
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DocClassUser", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);


                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.DocClass_User";
                    dataTable1.Columns.Add("User_ID", typeof(long));
                    if (objModel.Emp_UserDet_List != null)
                    {
                        if (objModel.Ven_UserDet_List != null)
                        {
                            foreach (var dta in objModel.Ven_UserDet_List)
                            {
                                if (dta.IsSelected == true)
                                {
                                    dataTable1.Rows.Add(dta.User_ID); // Id of '1' is valid for the Person table
                                }
                            }
                        }
                    }
                    SqlParameter parameter1 = new SqlParameter("DocClassVendor", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassUserMapping_Repository/InsertDocClassUserMapping");
                    objReturn.ReturnMessage = "System Error on Inserting Document Class User Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassUserMapping_Repository/InsertDocClassUserMapping");
                objReturn.ReturnMessage = "System Error on Inserting Document Class User Mapping!";
            }

            return objReturn;
        }




        public List<UserDet> GetEmployeeListNotMapped(long SubCompId, long DomainId, long ClassId, long UserId)
        {
            List<UserDet> objList = new List<UserDet>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName "
                + " From Employee_table Emp where Sub_Company = @Sub_Company and Domain_ID = @DomainID"
                + " and User_type=@UserType and SLNO not in (Select User_ID From User_DocClass_Mapping_Table "
                + " where Document_ClassID=@Document_ClassID and Isnull(IsDeleted,'') != @IsDeleted)";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_Company", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@Document_ClassID", ClassId);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDet tempobj = new UserDet();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.User_Name = sdr["EmployeeName"].ToString() + " [" + sdr["EmpId"].ToString() + "]";

                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserId, "API", "DocClassUserMapping_Repository/GetEmployeeListNotMapped");
            }
            return objList;
        }
               
        public List<UserDet> GetVendorListNotMapped(long SubCompId, long DomainId, long ClassId, long UserId)
        {
            List<UserDet> objList = new List<UserDet>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName "
                + " From Employee_table Emp where Sub_Company = @Sub_Company and Domain_ID = @DomainID"
                + " and User_type!=@UserType and SLNO not in (Select User_ID From User_DocClass_Mapping_Table "
                + " where Document_ClassID=@Document_ClassID and Isnull(IsDeleted,'') != @IsDeleted)";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_Company", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@Document_ClassID", ClassId);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDet tempobj = new UserDet();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.User_Name = sdr["EmployeeName"].ToString() + " [" + sdr["EmpId"].ToString() + "]";

                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserId, "API", "DocClassUserMapping_Repository/GetVendorListNotMapped");
            }
            return objList;
        }




        public List<UserDet> GetEmployeeListToUnMap(long SubCompId, long DomainId, long ClassId, long UserId)
        {
            List<UserDet> objList = new List<UserDet>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName "
                + " From Employee_table Emp where Sub_Company = @Sub_Company and Domain_ID = @DomainID "
                + " and User_type=@UserType and SLNO in (Select User_ID From User_DocClass_Mapping_Table "
                + " where Document_ClassID=@Document_ClassID)";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_Company", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@Document_ClassID", ClassId);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDet tempobj = new UserDet();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.User_Name = sdr["EmployeeName"].ToString() + " [" + sdr["EmpId"].ToString() + "]";
                    tempobj.IsSelected = false;
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserId, "API", "DocClassUserMapping_Repository/GetEmployeeListToUnMap");
            }
            return objList;
        }

        public List<UserDet> GetVendorListToUnMap(long SubCompId, long DomainId, long ClassId, long UserId)
        {
            List<UserDet> objList = new List<UserDet>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName "
                + " From Employee_table Emp where Sub_Company = @Sub_Company and Domain_ID = @DomainID"
                + " and User_type!=@UserType and SLNO in (Select User_ID From User_DocClass_Mapping_Table "
                + " where Document_ClassID=@Document_ClassID)";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_Company", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@Document_ClassID", ClassId);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDet tempobj = new UserDet();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.User_Name = sdr["EmployeeName"].ToString() + " [" + sdr["EmpId"].ToString() + "]";
                    tempobj.IsSelected = false;
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserId, "API", "DocClassUserMapping_Repository/GetVendorListToUnMap");
            }
            return objList;
        }




        public User_DocClass_Mapping_Table_page_Model GetDocClassUserMappingListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            User_DocClass_Mapping_Table_page_Model objModel = new User_DocClass_Mapping_Table_page_Model();
            List<User_DocClass_Mapping_Table_Model> objList = new List<User_DocClass_Mapping_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Sub_CompID";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocClassUserMappingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_DocClass_Mapping_Table_Model tempobj = new User_DocClass_Mapping_Table_Model();
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DCClassName = sdr["DCClassName"].ToString();
                    tempobj.DCDomainName = sdr["DCDomainName"].ToString();
                    tempobj.UserName = sdr["UserName"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From vwGetDocClassUserMapping where DCClassName "
                     + " like '%'+@search+'%'  or DCDomainName like '%'+@search+'%'  ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.User_DocClass_Mapping_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocClassUserMapping_Repository/GetDocClassUserMappingListByPage");
            }

            return objModel;

        }




        public DBReturnModel UnMapDocClassUserMapping(User_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on UnMapping Document Class User Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UnMapDocClassUserMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("Document_ClassID", objModel.Document_ClassID);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("DeletedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("DeletedOn", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.DocClass_User";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    foreach (var dta in objModel.Emp_UserDet_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.User_ID); // Id of '1' is valid for the Person table
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DocClassUser", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);


                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.DocClass_User";
                    dataTable1.Columns.Add("User_ID", typeof(long));
                    foreach (var dta in objModel.Ven_UserDet_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable1.Rows.Add(dta.User_ID); // Id of '1' is valid for the Person table
                        }
                    }
                    SqlParameter parameter1 = new SqlParameter("DocClassVendor", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassUserMapping_Repository/UnMapDocClassUserMapping");
                    objReturn.ReturnMessage = "System Error on UnMapping Document Class User Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassUserMapping_Repository/UnMapDocClassUserMapping");
                objReturn.ReturnMessage = "System Error on UnMapping Document Class User Mapping!";
            }

            return objReturn;
        }


    }
}
