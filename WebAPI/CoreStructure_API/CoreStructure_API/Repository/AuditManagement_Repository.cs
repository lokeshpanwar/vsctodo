﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Model.AuditManagment_Model;

namespace CoreStructure_API.Repository
{
    public class AuditManagement_Repository
    {

        public string InsertNewFrequency(FrequencyModel objModel)
        {
            string result = "Error on Inserting Frequency!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "SP_InsertFrequency";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FrequencyName", objModel.FrequencyName);
                    cmd.Parameters.AddWithValue("@FrequencyCode", objModel.FrequencyCode);
                    cmd.Parameters.AddWithValue("@FrequencyDetails", objModel.FrequencyDetails);
                    cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
            }

            return result;
        }

        public string UpdateFrequency(FrequencyModel objModel)
        {
            string result = "Error on Inserting Frequency!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "SP_UpdateFrequency";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FrequencyName", objModel.FrequencyName);
                    cmd.Parameters.AddWithValue("@FrequencyCode", objModel.FrequencyCode);
                    cmd.Parameters.AddWithValue("@FrequencyDetails", objModel.FrequencyDetails);
                    cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);
                    cmd.Parameters.AddWithValue("@Frequencyid", objModel.Frequencyid);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
            }

            return result;
        }





        public List<FrequencyModel> GetFrequencyList(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            List<FrequencyModel> objList = new List<FrequencyModel>();

            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Name";
            }
            if(pageSize == 0)
            {
                pageSize = 10;
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                //string sqlstr = "select id, [Name],[Days],code,[Description], isnull(Isdelete,0) as Isdelete from FrequencyMaster ";
                //SqlCommand cmd = new SqlCommand(sqlstr, con);
                //SqlDataReader sdr = cmd.ExecuteReader();

                string sqlstr = "SP_GetFrequencyByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_col_alias", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new FrequencyModel
                    {
                        FrequencyName = sdr["name"].ToString(),
                        FrequencyCode = sdr["code"].ToString(),
                        FrequencyDetails = sdr["Description"].ToString(),
                        FrequencyDays = sdr["Days"].ToString(),
                        Frequencyid = Convert.ToInt32(sdr["Id"].ToString()),
                        IsDelete = Convert.ToInt32(sdr["isActive"].ToString()),
                    });
                }

                sdr.Close();

                sqlstr = "SELECT Count(id) as Count FROM (id, [Name],[Days],code,[Description], isnull(Isdelete,0) as Isdelete , isnull(isActive,0) as isActive from FrequencyMaster  where isnull(IsDelete,0) = 0 and (Isnull(tm.groupname, '') like '%" + search + "%')) as row; ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList[0].TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetFrequencyList");
            }

            return objList;
        }




        public List<Audit_Model> GetAuditManageListById(int Id, long CreatedBy)
        {
            List<Audit_Model> objList = new List<Audit_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "select Id,ControlArea,Objective,ControlNo,ControlDescription,Artefacts,Remarks,Name,Description from AuditMasterdetails  where Auditid =" + Id + " and isnull(IsDelete,0) = 0 ";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Audit_Model
                    {
                        ControlArea = sdr["ControlArea"].ToString(),
                        ControlObject = sdr["Objective"].ToString(),
                        ControlNo = sdr["ControlNo"].ToString(),
                        ControlDescription = sdr["ControlDescription"].ToString(),
                        Artefacts = sdr["Artefacts"].ToString(),
                        Remarks = sdr["Remarks"].ToString(),
                        Name = sdr["Name"].ToString(),
                        Description = sdr["Description"].ToString(),
                        MainId = Convert.ToInt32(sdr["Id"].ToString())
                    });
                }

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objList;
        }

        public List<AuditGroupMapping_Model> AuditGroupMappingById(int id, long CreatedBy)
        {
            List<AuditGroupMapping_Model> objList = new List<AuditGroupMapping_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();


                string sqlstr = "SP_GroupMappingById";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@id", id);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditGroupMapping_Model
                    {
                        APGroupId = Convert.ToInt32(sdr["APGroupName"].ToString()),
                        ACGroupIds = sdr["ACGroupName"].ToString(),

                        APGroupName = sdr["team"].ToString(),
                        ACGroupName = sdr["location"].ToString(),
                        AuditscopName = sdr["AuditscopName"].ToString(),
                        AuditscopNamelist = sdr["Scopnames"].ToString(),

                        AuditscopCompanyName = sdr["AuditscopCompanyName"].ToString(),
                        AuditscopCompanyNamelist = sdr["Clientnames"].ToString(),

                        ACGroupId = Convert.ToInt32(sdr["id"]),
                        GroupActive = Convert.ToInt32(sdr["Isactive"]),
                        DocumentClass = sdr["DocumentClass"].ToString(),
                        DocumentCompany = Convert.ToInt32(sdr["DocumentCompany"]),
                        FunctionalDomain = Convert.ToInt32(sdr["FunctionalDomain"])
                    });
                }

                sdr.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/AuditGroupMappingById");
            }

            return objList;
        }

        public List<AuditGroupMapping_Model> GetAuditTemplateByIdGroupMappingById(int id, long CreatedBy)
        {
            List<AuditGroupMapping_Model> objList = new List<AuditGroupMapping_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();


                string sqlstr = "SP_GroupMappingById";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@id", id);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditGroupMapping_Model
                    {
                        APGroupId = Convert.ToInt32(sdr["APGroupName"].ToString()),
                        ACGroupIds = sdr["ACGroupName"].ToString(),

                        APGroupName = sdr["team"].ToString(),
                        ACGroupName = sdr["location"].ToString(),
                        AuditscopName = sdr["AuditscopName"].ToString(),
                        AuditscopNamelist = sdr["Scopnames"].ToString(),

                        AuditscopCompanyName = sdr["AuditscopCompanyName"].ToString(),
                        AuditscopCompanyNamelist = sdr["Clientnames"].ToString(),

                        ACGroupId = Convert.ToInt32(sdr["id"]),
                        GroupActive = Convert.ToInt32(sdr["Isactive"]),
                        DocumentClass = sdr["DocumentClass"].ToString(),
                        DocumentCompany = Convert.ToInt32(sdr["DocumentCompany"]),
                        FunctionalDomain = Convert.ToInt32(sdr["FunctionalDomain"])
                    });
                }

                sdr.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GroupMappingById");
            }

            return objList;
        }

        public List<AuditScheduling_Model> GetAuditScheduleByFrequency(long groupMaapingId, long frequencyId, long createdBy)
        {
            //AuditScheduling_Model_Result objModel = new AuditScheduling_Model_Result();
            List<AuditScheduling_Model> objList = new List<AuditScheduling_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_GetAuditScheduleByFrequency";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@GroupMappingId", groupMaapingId);
                cmd.Parameters.AddWithValue("@FrequencyId", frequencyId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditScheduling_Model
                    {
                        FrequencyId = Convert.ToInt64(sdr["FrequencyId"].ToString()),
                        GroupMappingId = Convert.ToInt64(sdr["GroupMappingId"].ToString()),
                        Status = sdr["Status"].ToString(),
                        Days = sdr["Days"].ToString(),
                        Name = sdr["Name"].ToString(),
                        StartDate = Convert.ToDateTime(sdr["StartDate"].ToString()),
                        EndDate = Convert.ToDateTime(sdr["EndDate"].ToString()),
                        Id = Convert.ToInt64(sdr["Id"].ToString())
                    });
                }
                sdr.Close();

                //objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "Audit_Repository/GetAuditScheduleByFrequency");
            }

            return objList;
        }

        public AuditGroupMapping_Model_table GroupMappingdetails(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AuditGroupMapping_Model_table objModel = new AuditGroupMapping_Model_table();
            List<AuditGroupMapping_Model> objList = new List<AuditGroupMapping_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "GroupName";
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                //string sqlstr = "select gm.id,APGroupName,ACGroupName, isnull(gm.isactive,0) as Isactive, tm.name as team,lm.Name as location,gm.AuditscopName, (select dbo.[GetScopSplit](gm.AuditscopName)) as Scopnames, gm.AuditscopCompanyName, (select dbo.[GetClientSplit](gm.AuditscopCompanyName)) as Clientnames from GroupMapping gm inner join TeamMaster tm on gm.APGroupName= tm.id inner join Auditlocationmaster lm on gm.ACGroupName = lm.id where isnull(gm.isdelete,0)=0";


                string sqlstr = "SP_GroupMappingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_col_alias", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new AuditGroupMapping_Model
                    {
                        APGroupId = Convert.ToInt32(sdr["APGroupName"].ToString()),
                        ACGroupIds = sdr["ACGroupName"].ToString(),

                        APGroupName = sdr["team"].ToString(),
                        ACGroupName = sdr["location"].ToString(),
                        AuditscopName = sdr["AuditscopName"].ToString(),
                        AuditscopNamelist = sdr["Scopnames"].ToString(),

                        AuditscopCompanyName = sdr["AuditscopCompanyName"].ToString(),
                        AuditscopCompanyNamelist = sdr["Clientnames"].ToString(),

                        ACGroupId = Convert.ToInt32(sdr["id"]),
                        GroupActive = Convert.ToInt32(sdr["Isactive"]),

                    });
                }

                sdr.Close();

                sqlstr = "SELECT Count(id) as Count FROM (SELECT gm.id, tm.groupname from GroupMapping gm inner join Group_table tm on gm.APGroupName = tm.SLNO where isnull(gm.isdelete, 0) = 0 and (Isnull(tm.groupname, '') like '%" + search + "%')) as row; ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objModel;
        }






        public AuditGroupMapping_Model_table GroupMappingdetailsNew(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AuditGroupMapping_Model_table objModel = new AuditGroupMapping_Model_table();
            List<AuditGroupMapping_Model> objList = new List<AuditGroupMapping_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "GroupName";
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                //string sqlstr = "select gm.id,APGroupName,ACGroupName, isnull(gm.isactive,0) as Isactive, tm.name as team,lm.Name as location,gm.AuditscopName, (select dbo.[GetScopSplit](gm.AuditscopName)) as Scopnames, gm.AuditscopCompanyName, (select dbo.[GetClientSplit](gm.AuditscopCompanyName)) as Clientnames from GroupMapping gm inner join TeamMaster tm on gm.APGroupName= tm.id inner join Auditlocationmaster lm on gm.ACGroupName = lm.id where isnull(gm.isdelete,0)=0";


                string sqlstr = "SP_GroupMappingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", 1);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_col_alias", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new AuditGroupMapping_Model
                    {
                        APGroupId = Convert.ToInt32(sdr["APGroupName"].ToString()),
                        ACGroupIds = sdr["ACGroupName"].ToString(),

                        APGroupName = sdr["team"].ToString(),
                        ACGroupName = sdr["location"].ToString(),
                        AuditscopName = sdr["AuditscopName"].ToString(),
                        AuditscopNamelist = sdr["Scopnames"].ToString(),

                        AuditscopCompanyName = sdr["AuditscopCompanyName"].ToString(),
                        AuditscopCompanyNamelist = sdr["Clientnames"].ToString(),

                        ACGroupId = Convert.ToInt32(sdr["id"]),
                        GroupActive = Convert.ToInt32(sdr["Isactive"]),

                    });
                }

                sdr.Close();

                sqlstr = "SELECT Count(id) as Count FROM (SELECT gm.id, tm.groupname from GroupMapping gm inner join Group_table tm on gm.APGroupName = tm.SLNO where isnull(gm.isdelete, 0) = 0 and(Isnull(tm.groupname, '') like '%" + search + "%')) as row; ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objModel;
        }





        public string DisableFrequency(int Id, int Type, long DeletedBy)
        {
            string result = "Error on Disabling Frequency!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_EnableDisableFrequency";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Frequency_Repository/DisableFrequency");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Frequency_Repository/Disablefrequency");
            }

            return result;
        }

        public string DeleteFrequency(int Id,long DeletedBy)
        {
            string result = "Error on Disabling Frequency!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_DeleteFrequency";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Frequency_Repository/DisableFrequency");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Frequency_Repository/Disablefrequency");
            }

            return result;
        }




        public string InsertNewClient(CompanyDetails objModel)
        {
            string result = "Error on Inserting Frequency!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertClient]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ComapnyName", objModel.ComapnyName);
                    cmd.Parameters.AddWithValue("@ComapnyCode", objModel.ComapnyCode);
                    cmd.Parameters.AddWithValue("@ComapnyDetails", objModel.ComapnyDetails);
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatebyC, "API", "Employee_Repository/InsertClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatebyC, "API", "Employee_Repository/InsertClient");
            }

            return result;
        }



        public List<CompanyDetails> GetClientlist(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            List<CompanyDetails> objList = new List<CompanyDetails>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Name";
            }


            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                //string sqlstr = "select id, [Name],code,Details, isnull(IsDelete,0) as IsDelete, isnull(IsActive,0) as IsActive from clientmaster where isnull(IsDelete,0) = 0";
                //SqlCommand cmd = new SqlCommand(sqlstr, con);
                //SqlDataReader sdr = cmd.ExecuteReader();

                string sqlstr = "SP_GetClientListbyPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_col_alias", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new CompanyDetails
                    {
                        ComapnyName = sdr["name"].ToString(),
                        ComapnyCode = sdr["code"].ToString(),
                        ComapnyDetails = sdr["Details"].ToString(),
                        IsClientDelete = Convert.ToInt32(sdr["IsDelete"].ToString()),
                        IsClientActive = Convert.ToInt32(sdr["IsActive"].ToString()),
                        comapnyid = Convert.ToInt32(sdr["id"].ToString()),
                    });
                }
                sdr.Close();

                sqlstr = "SELECT Count(id) as Count FROM (select id, [Name],code,Details, isnull(IsDelete,0) as IsDelete, isnull(IsActive,0) as IsActive from clientmaster where isnull(IsDelete,0) = 0 and(Isnull(name, '') like '%" + search + "%')) as row; ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
               cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList[0].TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                //objModel.results = objList;



            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetClientList");
            }

            return objList;
        }


        public List<DatesRange> GetDateCalculation(int frequencyid, string Datefrom, string Dateto, long CreatedBy)
        {
            List<DatesRange> objList = new List<DatesRange>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "EXEC [SP_GetFrequencyTwoDates] " + frequencyid + ",'" + Datefrom + "','" + Dateto + "' ";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new DatesRange
                    {
                        Dates = sdr["Dates"].ToString(),
                        Dateid = Convert.ToInt32(sdr["id"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetClientList");
            }

            return objList;
        }



        public string UpdateClient(CompanyDetails objModel)
        {
            string result = "Error on Inserting Client!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "SP_UpdateClient";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ComapnyName", objModel.ComapnyName);
                    cmd.Parameters.AddWithValue("@ComapnyCode", objModel.ComapnyCode);
                    cmd.Parameters.AddWithValue("@ComapnyDetails", objModel.ComapnyDetails);
                    cmd.Parameters.AddWithValue("@comapnyid", objModel.comapnyid);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatebyC, "API", "Audi_Repository/Updateclient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatebyC, "API", "Audit_Repository/Updateclient");
            }

            return result;
        }



        public string DeleteClient(int Id, long DeletedBy)
        {
            string result = "Error on Delete Client!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "Sp_DeleteClient";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteClient");
            }

            return result;
        }


        public string DisableClient(int Id, int Type, long DeletedBy)
        {
            string result = "Error on Disabling Client!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_EnableDisableClient";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "client_Repository/DisableClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "client_Repository/DisableClient");
            }

            return result;
        }



        public string DeleteAuditClient(int Id, long DeletedBy)
        {
            string result = "Error on Delete Audit!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[Sp_DeleteAudit]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteClient");
            }

            return result;
        }


        public string DeleteAuditTemplateDetails(int Id, long DeletedBy)
        {
            string result = "Error on Delete Audit Template Details!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[sp_DeleteAuditTemplateDetails]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteAuditTemplateDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteAuditTemplateDetails");
            }

            return result;
        }

        public string DisableEnableAudit(int Id, int Type, long DeletedBy)
        {
            string result = "Error on Disabling Audit!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_EnableDisableAudit";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "client_Repository/DisableClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "client_Repository/DisableClient");
            }

            return result;
        }


        public string DisableGroupMapping(int Id, int Type, long Modified_ById)
        {
            string result = "Error on Disabling Audit Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_DisableGroupMapping";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", Modified_ById);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DisableGroupMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DisableGroupMapping");
            }

            return result;
        }

        public string DeleteGroupMapping(long Id, long Modified_ById)
        {
            string result = "Error on Delete Audit Group Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[Sp_DeleteGroupMapping]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", Modified_ById);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DeleteGroupMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DeleteGroupMapping");
            }

            return result;
        }

        public string NewTeam(TeamModel objModel)
        {
            string result = "Error on Inserting Team!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertTeam]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TeamName", objModel.TeamName);
                    cmd.Parameters.AddWithValue("@TeamCode", objModel.TeamCode);
                    cmd.Parameters.AddWithValue("@TeamDetails", objModel.TeamDetails);
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.TeamCreatedby, "API", "Employee_Repository/InsertClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.TeamCreatedby, "API", "Employee_Repository/InsertClient");
            }

            return result;
        }


        public string AddNewGroupMapping(AuditGroupMapping_Model objModel)
        {
            string result = "Error on Inserting Group Mapping!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertNewGroupMapping]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@APGroupName", objModel.APGroupName);
                    cmd.Parameters.AddWithValue("@ACGroupName", objModel.ACGroupName.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@AuditscopName", objModel.AuditScope.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@AuditscopCompanyName", objModel.AuditCompany.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@DocumentCompany", objModel.DocumentCompany);
                    cmd.Parameters.AddWithValue("@FunctionalDomain", objModel.FunctionalDomain);
                    cmd.Parameters.AddWithValue("@DocumentClass", objModel.DocumentClass);

                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.AuditCreatedby, "API", "Employee_Repository/InsertGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.AuditCreatedby, "API", "Employee_Repository/InsertGroup");
            }

            return result;
        }

        public string UpdateGroupMapping(AuditGroupMapping_Model objModel, long modifiedBy)
        {
            string result = "Error on Updating Audit Group Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateGroupMapping]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@UpdatedBy", modifiedBy);
                    cmd.Parameters.AddWithValue("@APGroupName", objModel.APGroupName);
                    cmd.Parameters.AddWithValue("@ACGroupName", objModel.ACGroupName.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@AuditscopName", objModel.AuditScope.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@AuditscopCompanyName", objModel.AuditCompany.Trim(new Char[] { '[', '"', ']' }));
                    cmd.Parameters.AddWithValue("@DocumentCompany", objModel.DocumentCompany);
                    cmd.Parameters.AddWithValue("@FunctionalDomain", objModel.FunctionalDomain);
                    cmd.Parameters.AddWithValue("@DocumentClass", objModel.DocumentClass);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateGroupMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateGroupMapping");
            }

            return result;
        }

        #region Audit User Mapping
        public string AddAuditUserMapping(AuditUserMapping_Model objModel, long createdBy)
        {
            string result = "Error on Inserting User Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertAuditUserMapping]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupMappingId ", objModel.GroupMappingId);
                    cmd.Parameters.AddWithValue("@ControlId", objModel.ControlId);
                    cmd.Parameters.AddWithValue("@Type", objModel.Type);
                    cmd.Parameters.AddWithValue("@EmployeeId", objModel.EmployeeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditUserMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditUserMapping");
            }

            return result;
        }

        public string UpdateAuditUserMapping(AuditUserMapping_Model objModel, long modifiedBy)
        {
            string result = "Error on Updating Audit Group Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditUserMapping]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", objModel.Id);
                    cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    cmd.Parameters.AddWithValue("@GroupMappingId ", objModel.GroupMappingId);
                    cmd.Parameters.AddWithValue("@ControlId", objModel.ControlId);
                    cmd.Parameters.AddWithValue("@Type", objModel.Type);
                    cmd.Parameters.AddWithValue("@EmployeeId", objModel.EmployeeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateAuditUserMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateAuditUserMapping");
            }

            return result;
        }

        public List<AuditUserMapping_Model> GetAuditUserMappingById(long id, long CreatedBy)
        {
            List<AuditUserMapping_Model> objList = new List<AuditUserMapping_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();


                string sqlstr = "SP_GetAuditUserMapping";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@GroupMappingId", id);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditUserMapping_Model
                    {
                        ControlId = Convert.ToInt64(sdr["ControlId"].ToString()),
                        GroupMappingId = Convert.ToInt64(sdr["GroupMappingId"].ToString()),
                        Type = sdr["Type"].ToString(),
                        ControlArea = sdr["ControlArea"].ToString(),
                        ControlNo = sdr["ControlNo"].ToString(),
                        EmployeeId = Convert.ToInt64(sdr["EmployeeId"].ToString()),
                        GroupId = Convert.ToInt64(sdr["GroupId"].ToString()),
                        Id = Convert.ToInt64(sdr["Id"].ToString())
                    });
                }

                sdr.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMappingById");
            }

            return objList;
        }
        #endregion

        #region Audit Sheduling
        public string AddAuditScheduling(AuditScheduling_Model objModel, long createdBy)
        {
            string result = "Error on Inserting User Scheduling!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertAuditScheduling]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupMappingId ", objModel.GroupMappingId);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    cmd.Parameters.AddWithValue("@Status", objModel.Status);
                    cmd.Parameters.AddWithValue("@StartDate", objModel.StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditScheduling");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditScheduling");
            }

            return result;
        }

        public string UpdateAuditScheduling(AuditScheduling_Model objModel, long modifiedBy)
        {
            string result = "Error on Updating Audit Scheduling!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditScheduling]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", objModel.Id);
                    cmd.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    cmd.Parameters.AddWithValue("@GroupMappingId ", objModel.GroupMappingId);
                    cmd.Parameters.AddWithValue("@FrequencyId", objModel.FrequencyId);
                    cmd.Parameters.AddWithValue("@StartDate", objModel.StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);
                    cmd.Parameters.AddWithValue("@Status", objModel.Status);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateAuditScheduling");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifiedBy, "API", "AuditManagement_Repository/UpdateAuditScheduling");
            }

            return result;
        }

        public List<AuditScheduling_Model> GetAuditSchedulingById(long id, long CreatedBy)
        {
            List<AuditScheduling_Model> objList = new List<AuditScheduling_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();


                string sqlstr = "SP_GetAuditScheduling";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@GroupMappingId", id);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditScheduling_Model
                    {
                        FrequencyId = Convert.ToInt64(sdr["FrequencyId"].ToString()),
                        GroupMappingId = Convert.ToInt64(sdr["GroupMappingId"].ToString()),
                        Status = sdr["Status"].ToString(),
                        Days = sdr["Days"].ToString(),
                        Name = sdr["Name"].ToString(),
                        StartDate = Convert.ToDateTime(sdr["StartDate"].ToString()),
                        EndDate = Convert.ToDateTime(sdr["EndDate"].ToString()),
                        Id = Convert.ToInt64(sdr["Id"].ToString())
                    });
                }

                sdr.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditScheduling");
            }

            return objList;
        }

        public string DeleteAuditScheduling(long Id, long Modified_ById)
        {
            string result = "Error on Delete Audit Scheduling!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[Sp_DeleteAuditScheduling]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@ModifiedBy", Modified_ById);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DeleteAuditScheduling");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DeleteAuditScheduling");
            }

            return result;
        }

        public string DisableAuditScheduling(long Id, int Type, long Modified_ById)
        {
            string result = "Error on Disabling Audit Scheduling!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "SP_EnableDisableAuditScheduling";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@ModifiedBy", Modified_ById);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DisableAuditScheduling");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Modified_ById, "API", "AuditManagement_Repository/DisableAuditScheduling");
            }

            return result;
        }
        #endregion

        public List<TeamModel> GetTeamlist(long CreatedBy)
        {
            List<TeamModel> objList = new List<TeamModel>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "select id, [Name],code,Details, isnull(IsDelete,0) as IsDelete, isnull(IsActive,0) as IsActive from [TeamMaster] where isnull(IsDelete,0) = 0";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new TeamModel
                    {
                        TeamName = sdr["name"].ToString(),
                        TeamCode = sdr["code"].ToString(),
                        TeamDetails = sdr["Details"].ToString(),
                        IsTeamDelete = Convert.ToInt32(sdr["IsDelete"].ToString()),
                        IsTeamActive = Convert.ToInt32(sdr["IsActive"].ToString()),
                        Teamid = Convert.ToInt32(sdr["id"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetTeamList");
            }

            return objList;
        }


        public List<LocationsModel> LocationDetailsList(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            List<LocationsModel> objList = new List<LocationsModel>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Name";
            }
                if(pageSize == 0)
                {
                    pageSize = 10;
                }


            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                //string sqlstr = "select Id,Name,Code,Details,AuditClientName, (select dbo.[GetClientSplit](AuditClientName) )Auditclientlist, Isnull(isactive,0) as IsActive from Auditlocationmaster where isnull(IsDelete,0) = 0";
                //SqlCommand cmd = new SqlCommand(sqlstr, con);
                //SqlDataReader sdr = cmd.ExecuteReader();

                string sqlstr = "SP_GetAuditlocationByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_col_alias", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new LocationsModel
                    {
                        LocationName = sdr["Name"].ToString(),
                        LocationCode = sdr["Code"].ToString(),
                        LocationDetails = sdr["Details"].ToString(),
                        AuditClientId = sdr["AuditClientName"].ToString(),
                        AuditClientName = sdr["Auditclientlist"].ToString(),
                        //IsTeamDelete = Convert.ToInt32(sdr["IsDelete"].ToString()),
                        IsLocationActive = Convert.ToInt32(sdr["IsActive"].ToString()),
                        Locationid = Convert.ToInt32(sdr["id"].ToString()),
                    });
                }
                sdr.Close();

                sqlstr = "SELECT Count(id) as Count FROM (select Id,Name,Code,Details,AuditClientName, (select dbo.[GetClientSplit](AuditClientName) )Auditclientlist, Isnull(isactive,0) as IsActive from Auditlocationmaster where isnull(IsDelete,0) = 0 and (Isnull(name, '') like '%" + search + "%')) as row; ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList[0].TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetTeamList");
            }

            return objList;
        }


        public string UpdateLocation(LocationsModel objModel)
        {
            string result = "Error on Inserting Location!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateLocation]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Locationid", objModel.Locationid);
                    cmd.Parameters.AddWithValue("@LocationName", objModel.LocationName);
                    cmd.Parameters.AddWithValue("@LocationCode", objModel.LocationCode);
                    cmd.Parameters.AddWithValue("@LocationDetails", objModel.LocationDetails);
                    cmd.Parameters.AddWithValue("@AuditClientName", objModel.AuditClientName.Trim(new Char[] { '[', '"', ']' }));
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.createbyclient, "API", "Employee_Repository/InsertClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.createbyclient, "API", "Employee_Repository/InsertClient");
            }

            return result;
        }


        public string UpdateTeam(TeamModel objModel)
        {
            string result = "Error on Inserting Client!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateTeam]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TeamName", objModel.TeamName);
                    cmd.Parameters.AddWithValue("@TeamCode", objModel.TeamCode);
                    cmd.Parameters.AddWithValue("@TeamDetails", objModel.TeamDetails);
                    cmd.Parameters.AddWithValue("@Teamid", objModel.Teamid);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.TeamCreatedby, "API", "Audi_Repository/UpdateTeam");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.TeamCreatedby, "API", "Audit_Repository/UpdateTeam");
            }

            return result;
        }

        public string DeleteTeam(int Id, long DeletedBy)
        {
            string result = "Error on Delete Team!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[Sp_DeleteTeam]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteTeam");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteTeam");
            }

            return result;
        }

        public string DisableEnableTeam(int Id, int Type, long DeletedBy)
        {
            string result = "Error on Disabling Team!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[SP_EnableDisableTeam]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DisableTeam");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DisableTeam");
            }

            return result;
        }

        public string NewLocation(LocationsModel objModel)
        {
            string result = "Error on Inserting Location!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InserLocation]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LocationName", objModel.LocationName);
                    cmd.Parameters.AddWithValue("@LocationCode", objModel.LocationCode);
                    cmd.Parameters.AddWithValue("@LocationDetails", objModel.LocationDetails);
                    cmd.Parameters.AddWithValue("@AuditClientName", objModel.AuditClientName.Trim(new Char[] { '[', '"', ']' }));
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.createbyclient, "API", "Employee_Repository/InsertClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.createbyclient, "API", "Employee_Repository/InsertClient");
            }

            return result;
        }


        public string AuditTeamSave(Audit_Model objModel)
        {
            string result = "Error on Inserting Audit Template!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InserAuditTemplates]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AuditCode", objModel.AuditCode);
                    cmd.Parameters.AddWithValue("@AuditDescription", objModel.AuditDescription);
                    cmd.Parameters.AddWithValue("@Frequencys", objModel.Frequencys);
                    cmd.Parameters.AddWithValue("@AuditDuration", objModel.AuditDuration);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.AuditCreatedBy, "API", "Employee_Repository/InsertClient");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.AuditCreatedBy, "API", "Employee_Repository/InsertClient");
            }

            return result;
        }


        public string AuditTeampDetails(Audit_Manage objModel)
        {
            string result = "Error on Inserting Audit!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InserAuditTemplatesDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ControlArea", objModel.Audit_ControlArea);
                    cmd.Parameters.AddWithValue("@ControlNo", objModel.Audit_ControlNo);
                    cmd.Parameters.AddWithValue("@ControlObject", objModel.Audit_ControlObject);
                    cmd.Parameters.AddWithValue("@ControlDescription", objModel.Audit__ControlDescription);
                    cmd.Parameters.AddWithValue("@Artefacts", objModel.Audit__Artefacts);
                    cmd.Parameters.AddWithValue("@Remarks", objModel.Audit_Remarks);
                    cmd.Parameters.AddWithValue("@AuditMasterId", objModel.AuditMasterId);
                    cmd.Parameters.AddWithValue("@Description", objModel.Audit_Description);
                    cmd.Parameters.AddWithValue("@Name", objModel.Audit_Name);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.AudCreatedBy, "API", "Audit_Repository/InsertAudit");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.AudCreatedBy, "API", "Audit_Repository/InsertAudit");
            }

            return result;
        }


        public string DeleteLocation(int Id, long DeletedBy)
        {
            string result = "Error on Delete Team!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[Sp_DeleteLocation]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteLocation");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DeleteLocation");
            }

            return result;
        }


        public string DisableLocation(int Id, int Type, long DeletedBy)
        {
            string result = "Error on Disabling Location!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "[SP_EnableDisableLocations]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DisableTeam");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Audit_Repository/DisableTeam");
            }

            return result;
        }


        public string UpdateAuditTransaction(Audit_Trans objModel)
        {
            string result = "Error on Inserting Audit Transaction!";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationSchedule]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransAuditId", objModel.Trans_AuditId);
                    cmd.Parameters.AddWithValue("@Frequencys", objModel.Frequencys);
                    cmd.Parameters.AddWithValue("@ScheduleId", objModel.ScheduleId);
                    cmd.Parameters.AddWithValue("@SchDatefrom", objModel.SchDatefrom);
                    cmd.Parameters.AddWithValue("@SchDateto", objModel.SchDateto);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.AuditCreatedBy, "API", "Employee_Repository/Insert Scheduling");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.AuditCreatedBy, "API", "Employee_Repository/Insert Scheduling");
            }

            return result;
        }

        public string UpdateAuditTemplate(AuditData_Model objModel, long AuditModifiedBy)
        {
            string result = "Error on Updating Audit Template!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTemplates]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", objModel.MainId);
                    cmd.Parameters.AddWithValue("@AuditCode", objModel.AuditCode);
                    cmd.Parameters.AddWithValue("@AuditDescription", objModel.AuditDescription);
                    cmd.Parameters.AddWithValue("@Frequencys", objModel.Frequencys);
                    cmd.Parameters.AddWithValue("@AuditDuration", objModel.AuditDuration);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditModifiedBy, "API", "AuditManagement_Repository/UpdateAuditTemplate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditModifiedBy, "API", "AuditManagement_Repository/UpdateAuditTemplate");
            }

            return result;
        }

        public string UpdateAuditTemplatesDetails(Audit_Manage objModel, long AuditModifiedBy)
        {
            string result = "Error on Updating Audit Template Details!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTemplatesDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", objModel.Id);
                    cmd.Parameters.AddWithValue("@ControlArea", objModel.Audit_ControlArea);
                    cmd.Parameters.AddWithValue("@ControlNo", objModel.Audit_ControlNo);
                    cmd.Parameters.AddWithValue("@ControlObject", objModel.Audit_ControlObject);
                    cmd.Parameters.AddWithValue("@ControlDescription", objModel.Audit__ControlDescription);
                    cmd.Parameters.AddWithValue("@Artefacts", objModel.Audit__Artefacts);
                    cmd.Parameters.AddWithValue("@Remarks", objModel.Audit_Remarks);
                    cmd.Parameters.AddWithValue("@AuditMasterId", objModel.AuditMasterId);
                    cmd.Parameters.AddWithValue("@Description", objModel.Audit_Description);
                    cmd.Parameters.AddWithValue("@Name", objModel.Audit_Name);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditModifiedBy, "API", "AuditManagement_Repository/UpdateAuditTemplatesDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditModifiedBy, "API", "AuditManagement_Repository/UpdateAuditTemplatesDetails");
            }

            return result;
        }

        public string UpdateStage1Data(int Auditdetailid, int userId, string userName, string st1comment, int AuditCreatedBy)
        {
            string result = "Error on Inserting Audit Transaction  stage 1";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage1]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@St1UserId", userId);
                    cmd.Parameters.AddWithValue("@St1UserName", userName);
                    cmd.Parameters.AddWithValue("@st1comment", st1comment);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
            }

            return result;
        }


        public string UpdateStage2Data(int Auditdetailid, string ArefactRef, string st2comment, int userId)
        {
            string result = "Error on Inserting Audit Transaction  stage 2";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage2]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ArefactRef", ArefactRef);
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@st2comment", st2comment);
                    cmd.Parameters.AddWithValue("@AuditCreatedBy", userId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, userId, "API", "Audit_Repository/AuditStage2");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, userId, "API", "Audit_Repository/AuditStage2");
            }

            return result;
        }

        public string UpdateStage3Data(int Auditdetailid, int NyStatus, string Observation, string Priority, string Recommendation, int AuditCreatedBy)
        {
            string result = "Error on Inserting Audit Transaction  stage 3";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage3]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@NyStatus", NyStatus);
                    cmd.Parameters.AddWithValue("@Observation", Observation);
                    cmd.Parameters.AddWithValue("@Priority", Priority);
                    cmd.Parameters.AddWithValue("@Recommendation", Recommendation);
                    cmd.Parameters.AddWithValue("@AuditCreatedBy", AuditCreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Audit_Repository/AuditStage3");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Audit_Repository/AuditStage3");
            }

            return result;
        }


        public string UpdateStage4NewData(int Auditdetailid, string RefDocClose, int AuditCreatedBy)
        {
            string result = "Error on Inserting Audit Transaction  stage 4";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage4New]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@RefDocClose", RefDocClose);

                    cmd.Parameters.AddWithValue("@AuditCreatedBy", AuditCreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Audit_Repository/AuditStage4");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Audit_Repository/AuditStage4");
            }

            return result;
        }

        public string UpdateStage4Data(int Auditdetailid, string Response, string EstDateofClosure, int Status, int AuditCreatedBy)
        {
            string result = "Error on Inserting Audit Transaction  stage 2";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage4]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@Response", Response);
                    cmd.Parameters.AddWithValue("@EstDateofClosure", EstDateofClosure);
                    cmd.Parameters.AddWithValue("@Status", Status);
                    cmd.Parameters.AddWithValue("@AuditCreatedBy", AuditCreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
            }

            return result;
        }

        public string UpdateStage5Data(int Auditdetailid, int st5status, string st5comment, int AuditCreatedBy)
        {
            string result = "Error on Inserting Audit Transaction  stage 5";
            try
            {
                //string empPicName = SaveImage(objModel.EmpPhoto, _hostingEnvironment);
                //if (empPicName != "")
                //{
                //    objModel.EmpPhoto = empPicName;
                //}

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_UpdateAuditTransationStage5]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Auditdetailid", Auditdetailid);
                    cmd.Parameters.AddWithValue("@st5status", st5status);
                    cmd.Parameters.AddWithValue("@st5comment", st5comment);
                    cmd.Parameters.AddWithValue("@AuditCreatedBy", AuditCreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AuditCreatedBy, "API", "Employee_Repository/AuditStage1");
            }

            return result;
        }


        //public List<Audit_Model> GetAuditManagmentList(long CreatedBy)
        //{
        //    List<Audit_Model> objList = new List<Audit_Model>();
        //    try
        //    {
        //        var con = SqlHelper.Connection();
        //        con.Open();

        //        string sqlstr = "select distinct amd.id,am.id as MainId, Auditcode, AuditDiscription, Controlarea, (select  dbo.[GetFrequencySplit](aus.Frequencyid)) as Frequencyname , isnull(amd.IsDelete,0)  as IsDelete,isnull(amd.IsActive,0) as IsActive,amd.Description,amd.name,Objective,ControlNo,ControlDescription,Artefacts,Remarks, convert(varchar(10),amd.createon,105) as CreateDate,isnull(amd.StageUpdate,0) as StageUpdate  ,convert(varchar,  aus.Frequencyid) as Frequencys,am.ScheduleId ,convert(varchar(10), aus.Startdate, 105) as ScheduleDatefrom, convert(varchar(10), ";
        //        sqlstr += "  aus.EndDate, 105) as ScheduleDateto, isnull(amd.PurposeReqUserId,0) as PurposeReqUserId,isnull(amd.AFIDStatus,0) as AFIDStatus,amd.Artefact_Ref,convert(varchar(10), amd.DateOfUpload,105) as DateOfUpload, amd.PurposeCommentsAuditee,isnull(amd.NCStatus,0) as NCStatus,Convert(varchar(10), amd.St3Date,105) as St3Date,isnull(amd.St3Userid,0) as St3Userid, amd.Observation, amd.Priority,amd.Recommendation, isnull( amd.st3Status,0) as st3Status,convert(varchar(10), amd.St4Date,105) as St4Date, isnull(amd.St4Userid,0) as St4Userid,amd.Response,convert(varchar(10), amd.EDC,105) as EDC,    ";
        //        sqlstr += " convert(varchar(10), amd.RefDocClose,105) as RefDocClose,convert(varchar(10), amd.ActualDateofCloser,105) as ActualDateofCloser,isnull(amd.St4Status,0) as St4Status, convert(varchar(10), amd.St5Date,105) as St5Date,isnull( amd.St5Userid,0) as St5Userid,isnull(amd.St5Status,0) as St5Status,amd.St5Comment, isnull(amd.StageUpdate,0) as StageUpdate , gm.DocumentClass  ";
        //        sqlstr += "  from AuditMaster am  inner join AuditMasterDetails amd on am.id = amd.Auditid left join groupmapping gm on gm.ACGroupName = amd.id left join auditscheduling aus on aus.id = gm.id where isnull(amd.IsDelete,0) = 0   ";
        //        SqlCommand cmd = new SqlCommand(sqlstr, con);
        //        SqlDataReader sdr = cmd.ExecuteReader();
        //        while (sdr.Read())
        //        {
        //            objList.Add(new Audit_Model
        //            {
        //                AuditCode = Convert.ToString( sdr["Auditcode"].ToString()),
        //                AuditDescription = Convert.ToString(sdr["AuditDiscription"].ToString()),
        //                ControlArea = Convert.ToString(sdr["Controlarea"].ToString()),
        //                Frequencyname = Convert.ToString(sdr["Frequencyname"].ToString()),
        //                AuditId = Convert.ToInt32(sdr["id"]),
        //                IsDelete = Convert.ToInt32(sdr["IsDelete"]),
        //                AIsActive = Convert.ToInt32(sdr["IsActive"]),
        //                MainId = Convert.ToInt32(sdr["MainId"]),
        //                Description = Convert.ToString(sdr["Description"].ToString()),
        //                Name = Convert.ToString(sdr["name"].ToString()),
        //                ControlObject = Convert.ToString(sdr["Objective"].ToString()),
        //                ControlNo = Convert.ToString(sdr["ControlNo"].ToString()),
        //                ControlDescription = Convert.ToString(sdr["ControlDescription"].ToString()),
        //                Artefacts = Convert.ToString(sdr["Artefacts"].ToString()),
        //                Remarks = Convert.ToString(sdr["Remarks"].ToString()),
        //                AuditCreatedate = Convert.ToString(sdr["CreateDate"].ToString()),
        //                StageUpdate = Convert.ToInt32(sdr["StageUpdate"].ToString()),
        //                HDRowStage = Convert.ToInt32(sdr["StageUpdate"].ToString()),
        //                Frequencys = Convert.ToString(sdr["Frequencys"].ToString()),
        //                ScheduleId = Convert.ToString(sdr["ScheduleId"].ToString()),

        //                SchDatefrom = Convert.ToString(sdr["ScheduleDatefrom"]),
        //                SchDateto = Convert.ToString(sdr["ScheduleDateto"]),
        //                PurposeReqUserId = Convert.ToInt32(sdr["PurposeReqUserId"]),
        //               // AFIDStatus = Convert.ToInt32(sdr["Frequencys"]),
        //                ArefactRef = Convert.ToString(sdr["Artefact_Ref"]),
        //                St1DateofUpload = Convert.ToString(sdr["DateOfUpload"]),
        //                st1comment = Convert.ToString(sdr["PurposeCommentsAuditee"]),
        //                NYStatus = Convert.ToInt32(sdr["NCStatus"]),
        //                st3Date = Convert.ToString(sdr["St3Date"]),
        //                st3CurrentUserName = Convert.ToString(sdr["St3Userid"]),
        //                Observation = Convert.ToString(sdr["Observation"]),
        //                Priority = Convert.ToString(sdr["Priority"]),
        //                Recommendation = Convert.ToString(sdr["Recommendation"]),
        //                st2Status = Convert.ToInt32(sdr["st3Status"].ToString()),
        //                St4Date = Convert.ToString(sdr["St4Date"].ToString()),
        //                St4CurrenUsername = Convert.ToString(sdr["St4Userid"].ToString()),
        //                Response = Convert.ToString(sdr["Response"].ToString()),
        //                EstDateofClosure = Convert.ToString(sdr["EDC"].ToString()),
        //                RefDocClose = Convert.ToString(sdr["RefDocClose"].ToString()),
        //                ActualDateofCloser = Convert.ToString(sdr["ActualDateofCloser"].ToString()),
        //                st4st4status = Convert.ToInt32(sdr["St4Status"].ToString()),
        //                St5Status = Convert.ToInt32(sdr["St5Status"].ToString()),
        //                St4CurrentDate = Convert.ToString(sdr["St5Date"].ToString()),
        //                st4currentusername = Convert.ToString(sdr["St5Userid"].ToString()),
        //                st4comment = Convert.ToString(sdr["St5Comment"].ToString()),
        //                Documentclass = Convert.ToString(sdr["DocumentClass"].ToString()),
        //            });
        //        }
        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
        //    }

        //    return objList;
        //}

        public List<Audit_Model> GetAuditManagmentList(long CreatedBy, int auditid, int scheduleid, int userid)
        {
            List<Audit_Model> objList = new List<Audit_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetAuditdetailsById]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@AuditId", auditid);
                cmd.Parameters.AddWithValue("@ScheduleId", scheduleid);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Audit_Model
                    {
                        AuditCode = Convert.ToString(sdr["Auditcode"].ToString()),
                        AuditDescription = Convert.ToString(sdr["AuditDiscription"].ToString()),
                        ControlArea = Convert.ToString(sdr["Controlarea"].ToString()),
                        Frequencyname = Convert.ToString(sdr["Frequencyname"].ToString()),
                        AuditId = Convert.ToInt32(sdr["id"]),
                        IsDelete = Convert.ToInt32(sdr["IsDelete"]),
                        AIsActive = Convert.ToInt32(sdr["IsActive"]),
                        MainId = Convert.ToInt32(sdr["MainId"]),
                        Description = Convert.ToString(sdr["Description"].ToString()),
                        Name = Convert.ToString(sdr["name"].ToString()),
                        ControlObject = Convert.ToString(sdr["Objective"].ToString()),
                        ControlNo = Convert.ToString(sdr["ControlNo"].ToString()),
                        ControlDescription = Convert.ToString(sdr["ControlDescription"].ToString()),
                        Artefacts = Convert.ToString(sdr["Artefacts"].ToString()),
                        Remarks = Convert.ToString(sdr["Remarks"].ToString()),
                        AuditCreatedate = Convert.ToString(sdr["CreateDate"].ToString()),
                        StageUpdate = Convert.ToInt32(sdr["StageUpdate"].ToString()),
                        HDRowStage = Convert.ToInt32(sdr["StageUpdate"].ToString()),
                        Frequencys = Convert.ToString(sdr["Frequencys"].ToString()),
                        ScheduleId = Convert.ToString(sdr["ScheduleId"].ToString()),

                        SchDatefrom = Convert.ToString(sdr["ScheduleDatefrom"]),
                        SchDateto = Convert.ToString(sdr["ScheduleDateto"]),
                        PurposeReqUserId = Convert.ToInt32(sdr["PurposeReqUserId"]),
                        // AFIDStatus = Convert.ToInt32(sdr["Frequencys"]),
                        ArefactRef = Convert.ToString(sdr["Artefact_Ref"]),
                        st2DateofUpload = Convert.ToString(sdr["DateOfUpload"]),
                        st1comment = Convert.ToString(sdr["St1Comment"]),
                        st1Date = Convert.ToString(sdr["St1Date"]),
                        St1UserName = Convert.ToString(sdr["St1UserName"]),
                        St1UserId = Convert.ToInt32(sdr["St1UserId"]),
                        purposeCommentsAuditee = Convert.ToString(sdr["PurposeCommentsAuditee"]),
                        NYStatus = Convert.ToInt32(sdr["NCStatus"]),
                        st3Date = Convert.ToString(sdr["St3Date"]),
                        st3CurrentUserName = Convert.ToString(sdr["St3UserName"]),
                        st3UserId = Convert.ToInt32(sdr["St3Userid"].ToString()),
                        Observation = Convert.ToString(sdr["Observation"]),
                        Priority = Convert.ToString(sdr["Priority"]),
                        Recommendation = Convert.ToString(sdr["Recommendation"]),
                        st2Status = Convert.ToInt32(sdr["st3Status"].ToString()),
                        St4Date = Convert.ToString(sdr["St4Date"].ToString()),
                        St4CurrenUsername = Convert.ToString(sdr["St4Userid"].ToString()),
                        Response = Convert.ToString(sdr["Response"].ToString()),
                        EstDateofClosure = Convert.ToString(sdr["EDC"].ToString()),
                        RefDocClose = Convert.ToString(sdr["RefDocClose"].ToString()),
                        ActualDateofCloser = Convert.ToString(sdr["ActualDateofCloser"].ToString()),
                        st4st4status = Convert.ToInt32(sdr["St4Status"].ToString()),
                        St5Status = Convert.ToInt32(sdr["St5Status"].ToString()),
                        St4CurrentDate = Convert.ToString(sdr["St5Date"].ToString()),
                        st4currentusername = Convert.ToString(sdr["St5Userid"].ToString()),
                        st4comment = Convert.ToString(sdr["St5Comment"].ToString()),
                        MessageCount = Convert.ToInt32(sdr["MessageCount"]),
                        auditscheduleid = Convert.ToInt32(sdr["auditscheduleid"]),
                        GroupMappingId = Convert.ToInt32(sdr["GroupMappingId"]),
                        AuditControlId = Convert.ToInt32(sdr["AuditControlId"]),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objList;
        }
        public AuditData_Model_Table GetAuditTemplateList(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AuditData_Model_Table objModel = new AuditData_Model_Table();
            List<AuditData_Model> objList = new List<AuditData_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "AuditCode";
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_AuditTemplateByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                //string sqlstr = "select distinct am.id as MainId, AuditCode, AuditDiscription, (select  dbo.[GetFrequencySplit](am.Frequency)) as Frequencyname,am.Frequency as Frequencys,am.ScheduleId ,convert(varchar(10), am.ScheduleDatefrom, 105) as ScheduleDatefrom, convert(varchar(10), am.ScheduleDateto, 105) as ScheduleDateto, isnull(am.IsDelete, 0) as IsDelete,isnull(am.IsActive, 0) as IsActive, convert(varchar(10), am.createon, 105) as Createdate, convert(varchar(10), am.modifyon, 105) as Modifydate,AuditDuration from AuditMaster am where isnull(am.IsDelete,0) = 0  ; ";
                //SqlCommand cmd = new SqlCommand(sqlstr, con);
                //SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new AuditData_Model
                    {
                        AuditCode = Convert.ToString(sdr["AuditCode"].ToString()),
                        AuditDescription = Convert.ToString(sdr["AuditDiscription"].ToString()),
                        Frequencyname = Convert.ToString(sdr["Frequencyname"].ToString()),
                        IsDelete = Convert.ToInt32(sdr["IsDelete"]),
                        IsActive = Convert.ToInt32(sdr["IsActive"]),
                        MainId = Convert.ToInt32(sdr["MainId"]),
                        Frequencys = Convert.ToString(sdr["Frequencys"].ToString()),
                        ScheduleId = Convert.ToString(sdr["ScheduleId"].ToString()),
                        ScheduleDatefrom = Convert.ToString(sdr["ScheduleDatefrom"]),
                        ScheduleDateto = Convert.ToString(sdr["ScheduleDateto"]),
                        Createdate = Convert.ToString(sdr["Createdate"]),
                        Modifydate = Convert.ToString(sdr["Modifydate"]),
                        AuditDuration = Convert.ToInt32(sdr["AuditDuration"])
                    });
                }

                sdr.Close();

                sqlstr = "Select Count(*) As Count From AuditMaster where isnull(IsDelete,0) = 0 and ( Isnull(AuditCode,'') like '%'+@search+'%'  or Isnull(AuditDiscription,'') like '%'+@search+'%' or Isnull(AuditDuration,0) like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objModel;
        }

        public AuditScheduling_Model_Table GetAuditSchedulingList(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AuditScheduling_Model_Table objModel = new AuditScheduling_Model_Table();
            List<AuditScheduling_Model> objList = new List<AuditScheduling_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Id";
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_AuditSchedulingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditScheduling_Model
                    {
                        Id = Convert.ToInt64(sdr["Id"].ToString()),
                        GroupMappingId = Convert.ToInt64(sdr["GroupMappingId"].ToString()),
                        FrequencyId = Convert.ToInt64(sdr["FrequencyId"].ToString()),
                        IsDelete = Convert.ToBoolean(sdr["IsDelete"]),
                        IsActive = Convert.ToBoolean(sdr["IsActive"]),
                        Status = sdr["Status"].ToString(),
                        Client = sdr["Clientnames"].ToString(),
                        Scope = sdr["Scopnames"].ToString(),
                        Location = Convert.ToString(sdr["location"]),
                        Group = Convert.ToString(sdr["groupname"]),
                        StartDate = Convert.ToDateTime(sdr["StartDate"]),
                        EndDate = Convert.ToDateTime(sdr["EndDate"])

                    });
                }

                sdr.Close();

                sqlstr = "select count(*) as Count from (SELECT s.*,(select dbo.[GetClientSplit](gm.AuditscopCompanyName)) as Clientnames,(select dbo.[GetAuditLocationSplit](ACGroupName)) as location,(select dbo.[GetScopSplit](gm.AuditscopName)) as Scopnames,tm.groupname from auditscheduling s inner join GroupMapping gm on s.groupmappingid = gm.id inner join Group_table tm on gm.APGroupName = tm.SLNO where s.isdelete = 0  ) as dataRow where(Isnull(status, '') like '%' + @search + '%' or Isnull(clientnames, '') like '%' + @search + '%' or Isnull(location, '') like '%' + @search + '%' or Isnull(scopnames, '') like '%' + @search + '%' or Isnull(groupname, '') like '%' + @search + '%')";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/GetAuditSchedulingList");
            }

            return objModel;
        }


        public AuditRegister_Model_Table AuditRegisterList(int startRowIndex, int pageSize, string option, bool isAuditor, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AuditRegister_Model_Table objModel = new AuditRegister_Model_Table();
            List<AuditRegister_Model> objList = new List<AuditRegister_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "AuditID";
            }

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = string.Empty;
                // sqlstr = "SP_AuditRegisterByPage";
                if (!isAuditor)
                {
                    if (option == "Ongoing")
                        sqlstr = "SP_AuditRegisterByPageOngoing";
                    if (option == "Upcoming")
                        sqlstr = "SP_AuditRegisterByPageUpcoming";
                    if (option == "Closed")
                        sqlstr = "SP_AuditRegisterByPageClosed";
                }
                else
                {
                    if (option == "Ongoing")
                        sqlstr = "SP_AuditRegisterByPageAOngoing";
                    if (option == "Upcoming")
                        sqlstr = "SP_AuditRegisterByPageAUpcoming";
                    if (option == "Closed")
                        sqlstr = "SP_AuditRegisterByPageAClosed";
                }
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);
                cmd.Parameters.AddWithValue("@userId", CreatedBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new AuditRegister_Model
                    {
                        AuditID = Convert.ToInt64(sdr["AuditID"].ToString()),
                        AuditScheduleId = Convert.ToInt64(sdr["ScheduleId"].ToString()),
                        AuditCode = sdr["AuditCode"].ToString(),
                        //Name = sdr["Name"].ToString(),
                        Description = sdr["Description"].ToString(),
                        StartDate = Convert.ToDateTime(sdr["StartDate"]),
                        EndDate = Convert.ToDateTime(sdr["EndDate"]),
                    });
                }

                sdr.Close();

                if (!isAuditor)
                {
                    if (option == "Ongoing")
                        sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join AuditTransaction at on at.auditscheduleid=ats.id inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where(au.EmployeeId=@userId or gu.User_ID=@userId) and ((cast(GETDATE() as date)>=cast(ats.startDate as date) and cast(GETDATE() as date)<=cast(ats.EndDate as date)) or (cast(GETDATE() as date) >= cast(ats.startDate as date) and(at.ncstatus <> 1 or at.st5status <> 4)))   and ats.isactive=1 and ats.status='enabled' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(am.AuditDiscription, '') like '%' + @search + '%')";
                    if (option == "Upcoming")
                        sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where (au.EmployeeId=@userId or gu.User_ID=@userId) and cast(GETDATE() as date)<cast(ats.startDate as date) and ats.isactive=1 and ats.status='enabled' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(am.AuditDiscription, '') like '%' + @search + '%')";
                    if (option == "Closed")
                        sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where (au.EmployeeId=@userId or gu.User_ID=@userId) and cast(ats.endDate as date) < cast(GETDATE() as date)  and ats.isactive=1 and ats.status='executed' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(am.AuditDiscription, '') like '%' + @search + '%')";
                }
                else
                {
                    if (option == "Ongoing")
                        sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join AuditTransaction at on at.auditscheduleid=ats.id  inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where ((cast(GETDATE() as date)>=cast(ats.startDate as date) and cast(GETDATE() as date)<=cast(ats.EndDate as date)) or (cast(GETDATE() as date) >= cast(ats.startDate as date) and(at.ncstatus <> 1 or at.st5status <> 4)))  and gu.user_id = @userId and ats.status = 'enabled' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(au.AuditDiscription, '') like '%' + @search + '%')";
                    if (option == "Upcoming")
                        sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where cast(GETDATE() as date)<cast(ats.startDate as date) and gu.user_id = @userId and ats.status = 'enabled' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(au.AuditDiscription, '') like '%' + @search + '%')";
                    if (option == "Closed")
                        sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where cast(ats.endDate as date)<cast(GETDATE() as date)  and gu.user_id = @userId and ats.status = 'executed' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1 and (Isnull(StartDate, '') like '%' + @search + '%' or Isnull(au.Id, '') like '%' + @search + '%' or Isnull(AuditCode, '') like '%' + @search + '  %' or Isnull(au.AuditDiscription, '') like '%' + @search + '%')";
                }


                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@userId", CreatedBy);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                con.Close();
                objModel.results = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Audit_Repository/AuditRegisterList");
            }

            return objModel;
        }


        public List<AuditData_Model> GetAuditTemplateById(int id)
        {
            List<AuditData_Model> objList = new List<AuditData_Model>();
            List<Audit_Manage> objManageList = new List<Audit_Manage>();
            AuditData_Model obj = new AuditData_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "select am.id as MainId, AuditCode, AuditDiscription, (select  dbo.[GetFrequencySplit](am.Frequency)) as Frequencyname,am.Frequency as Frequencys,am.ScheduleId ,convert(varchar(10), am.ScheduleDatefrom, 105) as ScheduleDatefrom, convert(varchar(10), am.ScheduleDateto, 105) as ScheduleDateto, isnull(am.IsDelete, 0) as IsDelete,isnull(am.IsActive, 0) as IsActive, convert(varchar(10), am.createon, 105) as Createdate, convert(varchar(10), am.modifyon, 105) as Modifydate,AuditDuration from AuditMaster am where Id=" + id + "  ;";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {

                    obj.AuditCode = Convert.ToString(sdr["AuditCode"].ToString());
                    obj.AuditDescription = Convert.ToString(sdr["AuditDiscription"].ToString());
                    obj.Frequencyname = Convert.ToString(sdr["Frequencyname"].ToString());
                    obj.IsDelete = Convert.ToInt32(sdr["IsDelete"]);
                    obj.IsActive = Convert.ToInt32(sdr["IsActive"]);
                    obj.MainId = Convert.ToInt32(sdr["MainId"]);
                    obj.Frequencys = Convert.ToString(sdr["Frequencys"].ToString());
                    obj.ScheduleId = Convert.ToString(sdr["ScheduleId"].ToString());
                    obj.ScheduleDatefrom = Convert.ToString(sdr["ScheduleDatefrom"]);
                    obj.ScheduleDateto = Convert.ToString(sdr["ScheduleDateto"]);
                    obj.Createdate = Convert.ToString(sdr["Createdate"]);
                    obj.Modifydate = Convert.ToString(sdr["Modifydate"]);
                    obj.AuditDuration = Convert.ToInt32(sdr["AuditDuration"]);
                }
                sdr.Close();
                sqlstr = "SELECT Id,Auditid,ControlArea,Objective,ControlNo,ControlDescription,Artefacts,Remarks,isnull(IsDelete, 0) as IsDelete,isnull(IsActive, 0) as IsActive,convert(varchar(10), createon, 105) as Createdate, convert(varchar(10), modifyon, 105) as Modifydate,Description,Name FROM AuditMasterDetails where isnull(IsDelete,0) = 0 and AuditId=" + id + "  ;";
                cmd = new SqlCommand(sqlstr, con);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {

                    objManageList.Add(new Audit_Manage
                    {
                        Audit__ControlDescription = Convert.ToString(sdr["ControlDescription"].ToString()),
                        Audit_ControlObject = Convert.ToString(sdr["Objective"].ToString()),
                        Audit_ControlNo = Convert.ToString(sdr["ControlNo"].ToString()),
                        Audit_Description = Convert.ToString(sdr["Description"]),
                        Audit_Name = Convert.ToString(sdr["Name"]),
                        Id = Convert.ToInt32(sdr["Id"]),
                        AuditMasterId = Convert.ToInt32(sdr["Auditid"].ToString()),
                        Audit_ControlArea = Convert.ToString(sdr["ControlArea"].ToString()),
                        Audit_Remarks = Convert.ToString(sdr["Remarks"]),
                        Audit__Artefacts = Convert.ToString(sdr["Artefacts"])

                    });
                }
                obj.Audit_Manage_Table = objManageList;
                objList.Add(obj);
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objList;
        }

        public List<Audit_Manage> GetAuditTemplateDetailsListById(int id)
        {
            List<Audit_Manage> objList = new List<Audit_Manage>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SELECT Id,Auditid,ControlArea,Objective,ControlNo,ControlDescription,Artefacts,Remarks,isnull(IsDelete, 0) as IsDelete,isnull(IsActive, 0) as IsActive,convert(varchar(10), createon, 105) as Createdate, convert(varchar(10), modifyon, 105) as Modifydate,Description,Name FROM AuditMasterDetails where isnull(IsDelete,0) = 0 and AuditId=" + id + "  ;";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {

                    objList.Add(new Audit_Manage
                    {
                        Audit__ControlDescription = Convert.ToString(sdr["ControlDescription"].ToString()),
                        Audit_ControlObject = Convert.ToString(sdr["Objective"].ToString()),
                        Audit_ControlNo = Convert.ToString(sdr["ControlNo"].ToString()),
                        Audit_Description = Convert.ToString(sdr["Description"]),
                        Audit_Name = Convert.ToString(sdr["Name"]),
                        Id = Convert.ToInt32(sdr["Id"]),
                        AuditMasterId = Convert.ToInt32(sdr["Auditid"].ToString()),
                        Audit_ControlArea = Convert.ToString(sdr["ControlArea"].ToString()),
                        Audit_Remarks = Convert.ToString(sdr["Remarks"]),
                        Audit__Artefacts = Convert.ToString(sdr["Artefacts"])

                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "Audit_Repository/GetAuditMangaList");
            }

            return objList;
        }

        public AuditAdminDashboard_Model GetAuditUserDashboard(long CreatedBy, bool isAuditor)
        {
            AuditAdminDashboard_Model objModel = new AuditAdminDashboard_Model();
            List<AuditDashboardNotification_Model> objList = new List<AuditDashboardNotification_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand();
                SqlDataReader sdr;
                string sqlstr;
                //pie chart

                List<PieChartReportViewModel> objChart = new List<PieChartReportViewModel>();
                PieChartReportViewModel tempobj = new PieChartReportViewModel();

                if (!isAuditor)
                {
                    sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join AuditTransaction at on at.auditscheduleid=ats.id inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where(au.EmployeeId=@userId or gu.User_ID=@userId) and ((cast(GETDATE() as date)>=cast(ats.startDate as date) and cast(GETDATE() as date)<=cast(ats.EndDate as date)) or (cast(GETDATE() as date) >= cast(ats.startDate as date) and(at.ncstatus <> 1 or at.st5status <> 4)))   and ats.isactive=1 and ats.status='enabled' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1";
                }
                else {
                    sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join AuditTransaction at on at.auditscheduleid=ats.id  inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where ((cast(GETDATE() as date)>=cast(ats.startDate as date) and cast(GETDATE() as date)<=cast(ats.EndDate as date)) or (cast(GETDATE() as date) >= cast(ats.startDate as date) and(at.ncstatus <> 1 or at.st5status <> 4)))  and gu.user_id = @userId and ats.status = 'enabled' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1 ";
                }
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@userId", CreatedBy);
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();
                tempobj.DataName = "Ongoing";
                objChart.Add(tempobj);

                if (!isAuditor)
                {
                    sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where (au.EmployeeId=@userId or gu.User_ID=@userId) and cast(GETDATE() as date)<cast(ats.startDate as date) and ats.isactive=1 and ats.status='enabled' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1";
                }
                else
                {
                    sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where cast(GETDATE() as date)<cast(ats.startDate as date) and gu.user_id = @userId and ats.status = 'enabled' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1";

                }
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@userId", CreatedBy);
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                tempobj = new PieChartReportViewModel();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();
                tempobj.DataName = "Upcoming";
                objChart.Add(tempobj);

                if (!isAuditor)
                {
                    sqlstr = "select count(distinct ats.id) as Count from AuditScheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId inner join AuditUserMapping au on au.GroupMappingId = gm.Id inner join(select* from AuditMaster) am on am.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) left join Employee_table emp on au.EmployeeId = emp.SLNO left join GroupUser gu on au.GroupId = gu.GroupID inner join AuditMasterDetails amd on amd.Id = au.ControlId where (au.EmployeeId=@userId or gu.User_ID=@userId) and cast(ats.endDate as date) < cast(GETDATE() as date)  and ats.isactive=1 and ats.status='executed' and  gm.IsDelete=0 and gm.IsActive=1 and am.IsDelete=0 and  am.IsActive=1";
                }
                else
                {
                    sqlstr = "select count(distinct ats.id) as Count from auditscheduling ats inner join GroupMapping gm on gm.Id = ats.GroupMappingId left join GroupUser gu on gm.APGroupName = gu.GroupID inner join(select * from AuditMaster) au on au.Id in (select item from dbo.SplitStringByComa(gm.AuditscopName, ',')) where cast(ats.endDate as date)<cast(GETDATE() as date)  and gu.user_id = @userId and ats.status = 'executed' and isnull(ats.IsDelete,0)= 0 and gm.IsDelete = 0 and gm.IsActive = 1";
                }
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@userId", CreatedBy);
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                tempobj = new PieChartReportViewModel();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();
                tempobj.DataName = "Closed";
                objChart.Add(tempobj);
                objModel.PieChartReportViewModel_List = objChart;

                //Get Notification List

                sqlstr = "SP_GetAuditNotificationById";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@UserId", CreatedBy);
                cmd.CommandType = CommandType.StoredProcedure;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new AuditDashboardNotification_Model
                    {
                        Id = Convert.ToInt64(sdr["Id"].ToString()),
                        AuditTransactionId = Convert.ToInt64(sdr["AuditTransactionId"].ToString()),
                        Message = sdr["Message"].ToString()
                    });
                }

                sdr.Close();
                objModel.notifications = objList;

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AuditManagement_Repository/GetAuditUserDashboard");
            }

            return objModel;

        }

        public AuditAdminDashboard_Model GetAuditAdminDashboard(long CreatedBy)
        {
            AuditAdminDashboard_Model objModel = new AuditAdminDashboard_Model();
            List<LocationsModel> objList = new List<LocationsModel>();
            List<AuditDashboardClient_Model> objClientList = new List<AuditDashboardClient_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "select count(id) as AuditCount from AuditMaster where isnull(isdelete,0)=0;";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.AuditCount = Convert.ToInt32(sdr["AuditCount"].ToString());

                }

                sdr.Close();
                sqlstr = "select count(id) as EnableAuditCount from AuditMaster where isnull(isdelete,0)=0 and isnull(isactive,0)=1;";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.EnableAuditCount = Convert.ToInt32(sdr["EnableAuditCount"].ToString());
                }
                sdr.Close();

                sqlstr = "select count(id) as DisableAuditCount from AuditMaster where isnull(isdelete,0)=0 and isnull(isactive,0)=0;";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.DisableAuditCount = Convert.ToInt32(sdr["DisableAuditCount"].ToString());
                }
                sdr.Close();


                // Get Location List 
                sqlstr = sqlstr = "select Id,Name,Code,Details,AuditClientName, (select dbo.[GetLocationClientSplit](AuditClientName) )Auditclientlist, Isnull(isactive,0) as IsActive from auditlocationmaster where isnull(IsDelete,0) = 0 ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new LocationsModel
                    {
                        LocationName = sdr["Name"].ToString(),
                        LocationCode = sdr["Code"].ToString(),
                        LocationDetails = sdr["Details"].ToString(),
                        AuditClientId = sdr["AuditClientName"].ToString(),
                        AuditClientName = sdr["Auditclientlist"].ToString(),
                        IsLocationActive = Convert.ToInt32(sdr["IsActive"].ToString()),
                        Locationid = Convert.ToInt32(sdr["id"].ToString()),
                    });
                }
                sdr.Close();
                objModel.locations = objList;

                //Get Client List


                sqlstr = "select  cm.Id,cm.Name,lm.Name as LocationName,lm.Id as LocationId  from clientmaster cm inner join auditlocationmaster lm on cm.Id IN (select item from dbo.SplitStringByComa((select lm.AuditClientName),',')) where isnull(cm.IsDelete,0)=0 and isnull(lm.IsDelete,0)=0;";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objClientList.Add(new AuditDashboardClient_Model
                    {
                        LocationName = sdr["LocationName"].ToString(),
                        Name = sdr["Name"].ToString(),
                        Id = Convert.ToInt64(sdr["Id"].ToString()),
                        LocationId = Convert.ToInt64(sdr["LocationId"].ToString()),
                    });
                }

                sdr.Close();
                objModel.clients = objClientList;

                //pie chart

                List<PieChartReportViewModel> objChart = new List<PieChartReportViewModel>();
                PieChartReportViewModel tempobj = new PieChartReportViewModel();

                sqlstr = "select count(distinct ats.Id) as Count from AuditScheduling ats inner join AuditTransaction at on at.auditscheduleid=ats.id  where status='enabled' and ((cast(GETDATE() as date)>=cast(ats.startDate as date) and cast(GETDATE() as date)<=cast(ats.EndDate as date)) or (cast(GETDATE() as date) >= cast(ats.startDate as date) and isnull(IsDelete,0)= 0 and isnull(IsActive,0)= 1";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();
                tempobj.DataName = "Ongoing";
                objChart.Add(tempobj);

                tempobj = new PieChartReportViewModel();

                sqlstr = "select count(distinct Id) as Count from AuditScheduling where status='enabled' and cast(GETDATE() as date)<cast(startDate as date) and isnull(IsDelete,0)= 0 and isnull(IsActive,0)= 1";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();

                tempobj.DataName = "Upcoming";
                objChart.Add(tempobj);

                tempobj = new PieChartReportViewModel();
                sqlstr = "select count(distinct Id) as Count from AuditScheduling where status='executed' and cast(GETDATE() as date)<cast(startDate as date) and isnull(IsDelete,0)= 0 and isnull(IsActive,0)= 1";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    tempobj.Count = Convert.ToInt32(sdr["Count"].ToString());
                }
                sdr.Close();


                tempobj.DataName = "Closed";
                objChart.Add(tempobj);
                objModel.PieChartReportViewModel_List = objChart;

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AuditManagement_Repository/GetAuditAdminDashboard");
            }

            return objModel;

        }

        #region Audit Message Added By Vijendra

        public string SaveAuditMessage(Audit_Conversation objModel)
        {
            string result = "Error on Inserting Message!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAuditConversation]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AuditId", objModel.MsgAuditId);
                    cmd.Parameters.AddWithValue("@SenderId", objModel.SenderId);
                    cmd.Parameters.AddWithValue("@Message", objModel.Message);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.SenderId, "API", "Audit_Repository/Audit_Transaction");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.SenderId, "API", "Audit_Repository/Audit_Transaction");
            }

            return result;
        }

        public List<Audit_Conversation> GetAuditMessageByAuditId(int AuditId, long createdBy)
        {
            List<Audit_Conversation> objList = new List<Audit_Conversation>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                //SqlCommand cmd = connection.CreateCommand();
                //SqlTransaction transaction;
                //transaction = connection.BeginTransaction();
                //cmd.Transaction = transaction;
                //cmd.Connection = connection;

                string sqlstr = "";
                sqlstr = "[Sp_GetConversationByAuditId]";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@AuditId", AuditId);
                // cmd.Parameters.AddWithValue("@RecieverId", createdBy);

                //  SqlDataReader sdr= cmd.ExecuteReader();
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Audit_Conversation
                    {
                        MsgAuditId = Convert.ToInt32(AuditId),
                        SenderName = Convert.ToString(sdr["Sender"].ToString()),
                        SenderId = Convert.ToInt32(sdr["SenderId"]),
                        RecieverName = Convert.ToString(sdr["Reciver"].ToString()),
                        RecieverId = Convert.ToInt32(sdr["ReciverId"]),
                        IsView = Convert.ToInt32(sdr["IsView"]),
                        Message = Convert.ToString(sdr["TextMessage"].ToString()),
                        CreateDate = Convert.ToString(sdr["Createdate"].ToString())
                    });
                }
                sdr.Close();
                // transaction.Commit();
                //connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "Audit_Repository/GetAuditMessageByAuditId");
            }

            return objList;
        }
        #endregion

        #region
        public string AddAuditNotification(long AuditTransationId, long UserId, string Message, long createdBy)
        {
            string result = "Error on Inserting Audit Notification!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_InsertAuditNotification]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AuditTransationId ", AuditTransationId);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Message", Message);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditNotification");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "AuditManagement/AddAuditNotification");
            }

            return result;
        }
        #endregion
    }
}
