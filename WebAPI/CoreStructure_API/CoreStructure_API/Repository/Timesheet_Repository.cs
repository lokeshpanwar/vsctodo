﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CoreStructure_API.Repository
{
    public class Timesheet_Repository
    {
        #region Fill TimeSheet
        public FilledTimesheetData Getfilledtimesheetdata(long loggedInUser)
        {
            FilledTimesheetData objdata = new FilledTimesheetData();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "sp_GetCalendarTimesheet";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserId", loggedInUser);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.CalFillList = (from DataRow dr in ds.Tables[0].Rows
                                           select new FilledTimesheetCal
                                           {
                                               Dates = Convert.ToString(dr["Dates"].ToString()),
                                               Days = Convert.ToString(dr["dayss"].ToString()),
                                               Months = Convert.ToString(dr["Months"].ToString()),
                                               Years = Convert.ToString(dr["Years"].ToString()),
                                               IsTimesheetfill = Convert.ToString(dr["IsTimesheetfill"].ToString()),
                                               Remarks = Convert.ToString(dr["Remarks"].ToString()),
                                               isholiday = Convert.ToInt32(dr["isholiday"].ToString()),
                                               Newdates = Convert.ToString(dr["newdates"].ToString()),

                                           }).ToList();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    objdata.CrMonthList = (from DataRow dr in ds.Tables[0].Rows
                                           select new CurrentMonth
                                           {
                                               CrMonth = Convert.ToString(dr["CrMonth"].ToString()),
                                           }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public CalFilledTimeSheet GetfilledtimesheetdataByDate(long loggedInUser, string Date)
        {
            CalFilledTimeSheet objdata = new CalFilledTimeSheet();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "sp_GetCalendarTimesheetBydate";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@userid", loggedInUser);
                cmd.Parameters.AddWithValue("@Date", Date);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.CasualTimeList = (from DataRow dr in ds.Tables[0].Rows
                                              select new Casualtimesheet
                                              {
                                                  DateofCal = Convert.ToString(dr["Dates"].ToString()),
                                                  Day = Convert.ToString(dr["dayss"].ToString()),
                                                  Month = Convert.ToString(dr["Months"].ToString()),
                                                  Year = Convert.ToString(dr["Years"].ToString()),
                                                  istimesheetfill = Convert.ToString(dr["IsTimesheetfill"].ToString()),
                                                  Remarks = Convert.ToString(dr["Remarks"].ToString()),

                                              }).ToList();
                }
                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    objdata.CrMonthList = (from DataRow dr in ds.Tables[0].Rows
                //                           select new CurrentMonth
                //                           {
                //                               CrMonth = Convert.ToString(dr["CrMonth"].ToString()),
                //                           }).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public string SaveTimesheet(Filltimesheet_Model model)
        {
            string result = "Error on Saving Timesheet!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SaveTimesheet]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Officeid", model.OfficeId);
                    cmd.Parameters.AddWithValue("@UserId", model.CreatedBy);
                    cmd.Parameters.AddWithValue("@Date", model.Date);
                    cmd.Parameters.AddWithValue("@fromtime", model.FromTime);
                    cmd.Parameters.AddWithValue("@Totime", model.ToTime);
                    cmd.Parameters.AddWithValue("@Totaltime", model.TotalTime);
                    cmd.Parameters.AddWithValue("@clientId", model.ClientId);
                    cmd.Parameters.AddWithValue("@ActivityId", model.ActivityId);
                    cmd.Parameters.AddWithValue("@SubactivityId", model.SubActivityId);
                    cmd.Parameters.AddWithValue("@Balancetobefill", model.Balancetobefill);
                    cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
                    cmd.Parameters.AddWithValue("@NoOfHours", model.NoOfHours);
                    //cmd.Parameters.AddWithValue("@Result", model);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveTimesheet");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveTimesheet");
            }
            return result;
        }

        public List<TimesheetGrid> GetFillTimesheetGridData(string Date, long CreatedBy)
        {
            List<TimesheetGrid> objList = new List<TimesheetGrid>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetFillTimesheetDataGrid";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserId", CreatedBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new TimesheetGrid
                    {
                        SheetId = Convert.ToString(sdr["Id"].ToString()),
                        Date = Convert.ToString(sdr["Date"].ToString()),
                        clientname = Convert.ToString(sdr["ClientName"].ToString()),
                        Activity = Convert.ToString(sdr["ActivityName"].ToString()),
                        SubActivity = Convert.ToString(sdr["SubActivityName"].ToString()),
                        TimeTaken = Convert.ToString(sdr["TotalTime"].ToString()),
                        Remarks = Convert.ToString(sdr["Remarks"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "TimeSheet_Repository/GetFillTimesheetGridData");
            }
            return objList;
        }
        public string DeleteSheetData(long SheetId, long logInUser)
        {
            string result = "Error on Deleting Sheet Data!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteSheetDataByID]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SheetId", SheetId);
                    //cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/DeleteSheetData");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/DeleteSheetData");
            }
            return result;
        }

        public List<SelectListItem> SubmitTimesheetCasual(string Date, long logInUser)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_FinalSubmitTimesheet";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserId", logInUser);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Result"].ToString(),
                        Value = sdr["Id"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "DropDown_Repository/GetSubActivityListByActivityId");
            }
            return objList;
        }
        #endregion

        #region View TimeSheet
        public SelfTimeSheet GetTimesheetSelfData(long LoggedInUser, string FromDate, string ToDate)
        {
            SelfTimeSheet objdata = new SelfTimeSheet();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetSelftTimesheetData";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@datefrom", FromDate);
                cmd.Parameters.AddWithValue("@dateto", ToDate);
                cmd.Parameters.AddWithValue("@UserId", LoggedInUser);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.TimsheetGridData = (from DataRow dr in ds.Tables[0].Rows
                                                select new TimesheetGrid
                                                {
                                                    SheetId = Convert.ToString(dr["Id"].ToString()),
                                                    Date = Convert.ToString(dr["Date"].ToString()),
                                                    clientname = Convert.ToString(dr["ClientName"].ToString()),
                                                    Activity = Convert.ToString(dr["ActivityName"].ToString()),
                                                    SubActivity = Convert.ToString(dr["SubActivityName"].ToString()),
                                                    TimeTaken = Convert.ToString(dr["TotalTime"].ToString()),
                                                    Remarks = Convert.ToString(dr["Remarks"].ToString()),
                                                }).ToList();
                }
                objdata.Totaltime = Convert.ToString(ds.Tables[1].Rows[0]["TotalTime"].ToString());
                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    objdata.ViewPolicy1 = (from DataRow dr in ds.Tables[1].Rows
                //                           select new DailyBusiness
                //                           {
                //                               Category = dr["CategoryName"].ToString(),
                //                               PremiumPayable = dr["PremiumPayable1"].ToString()
                //                           }).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }
        #endregion
        #region View TimeSheet BY Admin
        public AdminTimeSheetOfUsers GetAdminTimesheetView(int OfficeId, string FromDate, string ToDate, int ClientId, int EmployeeId, int ActivityId, int SubActivityId, int RoleId)
        {
            AdminTimeSheetOfUsers objdata = new AdminTimeSheetOfUsers();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetAllCostSheet";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@datefrom", FromDate);
                cmd.Parameters.AddWithValue("@dateto", ToDate);
                cmd.Parameters.AddWithValue("@ClientId", ClientId);
                cmd.Parameters.AddWithValue("@UserId", EmployeeId);
                cmd.Parameters.AddWithValue("@ActivityId", ActivityId);
                cmd.Parameters.AddWithValue("@SubActivityId", SubActivityId);
                cmd.Parameters.AddWithValue("@RoleId", RoleId);
                cmd.Parameters.AddWithValue("@GroupId", 0);
                cmd.Parameters.AddWithValue("@IndustryId", 0);
                cmd.Parameters.AddWithValue("@StatusId", 0);
                cmd.Parameters.AddWithValue("@VerticalId", 0);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.TimeSheetGrid = (from DataRow dr in ds.Tables[0].Rows
                                             select new AdminViewTimesheet
                                             {
                                                 EmployeeName = Convert.ToString(dr["Name"].ToString()),
                                                 Date = Convert.ToString(dr["Date"].ToString()),
                                                 ClientName = Convert.ToString(dr["ClientName"].ToString()),
                                                 ActivityName = Convert.ToString(dr["ActivityName"].ToString()),
                                                 SubActivityName = Convert.ToString(dr["SubActivityName"].ToString()),
                                                 TotalTime = Convert.ToString(dr["TotalTime"].ToString()),
                                                 Remarks = Convert.ToString(dr["Remarks"].ToString()),
                                                 CostHr = Convert.ToString(dr["CostperHr"].ToString()),
                                                 TotalCost = Convert.ToString(dr["TotalCost"].ToString()),
                                             }).ToList();
                }
                objdata.Totaltime = Convert.ToString(ds.Tables[1].Rows[0]["TotalTime"].ToString());
                objdata.TotalCost = Convert.ToString(ds.Tables[2].Rows[0]["TotalCost"].ToString());
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }
        #endregion

        #region Delete TimeSheet
        public SelfTimeSheet GetTimesheetDataTobeDelete(long loggedInUser, long EmployeeId, string Date)
        {
            SelfTimeSheet objdata = new SelfTimeSheet();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_DataToBedeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.Parameters.AddWithValue("@UserId", EmployeeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.TimsheetGridData = (from DataRow dr in ds.Tables[0].Rows
                                                select new TimesheetGrid
                                                {
                                                    SheetId = Convert.ToString(dr["Id"].ToString()),
                                                    Date = Convert.ToString(dr["Date"].ToString()),
                                                    clientname = Convert.ToString(dr["ClientName"].ToString()),
                                                    Activity = Convert.ToString(dr["ActivityName"].ToString()),
                                                    SubActivity = Convert.ToString(dr["SubActivityName"].ToString()),
                                                    TimeTaken = Convert.ToString(dr["TotalTime"].ToString()),
                                                    Remarks = Convert.ToString(dr["Remarks"].ToString()),
                                                }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public string GetTimesheetDataDelete(long logInUser, long EmployeeId, string Date)
        {
            string result = "0";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteTimesheetDataByID]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", Date);
                    cmd.Parameters.AddWithValue("@UserId", EmployeeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/GetTimesheetDataDelete");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/GetTimesheetDataDelete");
            }
            return result;
        }
        #endregion

        #region Apply Leave
        public string ApplyLeaveValidation(long logInUser, string Date)
        {
            string result = "0";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_LeaveValidation]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", Date);
                    cmd.Parameters.AddWithValue("@UserId", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/ApplyLeaveValidation");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "TimeSheet_Repository/ApplyLeaveValidation");
            }
            return result;
        }

        public string GetHoldayName(long logInUser, string Date)
        {
            string result = "0";
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "SP_GetHoldayname";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Date", Date);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    result = Convert.ToString(ds.Tables[0].Rows[0]["HolidayName"].ToString());
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public List<LeaveData> GetAllLeaveApplyDetails(int OfficeId, long CreatedBy)
        {
            List<LeaveData> objList = new List<LeaveData>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_GetAllLeaverequest";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@UserId", CreatedBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new LeaveData
                    {
                        LId = Convert.ToInt64(sdr["LID"].ToString()),
                        Name = Convert.ToString(sdr["Name"].ToString()),
                        FromDate = Convert.ToString(sdr["FDate"].ToString()),
                        ToDate = Convert.ToString(sdr["TDate"].ToString()),
                        DayCount = Convert.ToString(sdr["TotalDay"].ToString()),
                        Remarks = Convert.ToString(sdr["Leavereason"].ToString()),
                        LeaveRequestStatus = Convert.ToString(sdr["LRStatus"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "TimeSheet_Repository/GetAllLeaveApplyDetails");
            }
            return objList;
        }

        public string SaveLeaveRequest(Leave_Model model)
        {
            string result = "Error on Saving Leave!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_Save_LeaveRequest]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Id", model.UpdateID);
                    cmd.Parameters.AddWithValue("@FromDate", model.FromDate);
                    cmd.Parameters.AddWithValue("@ToDate", model.ToDate);
                    cmd.Parameters.AddWithValue("@DayCount", model.NoOfDays);
                    cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
                    cmd.Parameters.AddWithValue("@UserId", model.CreatedBy);
                    cmd.Parameters.AddWithValue("@IPAddress", model.IPAddress);
                    cmd.Parameters.AddWithValue("@OfficeId", model.OfficeId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveTimesheet");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveTimesheet");
            }
            return result;
        }

        public List<LeaveData> GetLeaveDetailsForEdit(int OfficeId, long LId, long CreatedBy)
        {
            List<LeaveData> objList = new List<LeaveData>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_GetLeaveDetailsForEdit";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@UserId", CreatedBy);
                cmd.Parameters.AddWithValue("@LId", LId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new LeaveData
                    {
                        LId = Convert.ToInt64(sdr["Id"].ToString()),
                        UserId = Convert.ToInt64(sdr["UserId"].ToString()),
                        FromDate = Convert.ToString(sdr["Leavefrom"].ToString()),
                        ToDate = Convert.ToString(sdr["Leaveto"].ToString()),
                        DayCount = Convert.ToString(sdr["TotalDay"].ToString()),
                        Remarks = Convert.ToString(sdr["Leavereason"].ToString()),
                        LeaveStatus = Convert.ToInt32(sdr["LeaveStatus"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "TimeSheet_Repository/GetLeaveDetailsForEdit");
            }
            return objList;
        }

        public string DeleteLeaveRequestByID(long LogInUser, long LId)
        {
            string result = "Error on Deleting Leave!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteLeaveRequestByID]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LId", LId);
                    cmd.Parameters.AddWithValue("@LogInUser", LogInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/DeleteLeaveRequestByID");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/DeleteLeaveRequestByID");
            }
            return result;
        }

        #endregion

        #region Mark Leave

        public List<LeaveData> GetAllMarkLeaveDetails(int OfficeId, long CreatedBy)
        {
            List<LeaveData> objList = new List<LeaveData>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_GetAllMarkleave";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@UserId", CreatedBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new LeaveData
                    {
                        LId = Convert.ToInt64(sdr["LID"].ToString()),
                        Name = Convert.ToString(sdr["Name"].ToString()),
                        FromDate = Convert.ToString(sdr["FDate"].ToString()),
                        ToDate = Convert.ToString(sdr["TDate"].ToString()),
                        DayCount = Convert.ToString(sdr["TotalDay"].ToString()),
                        Remarks = Convert.ToString(sdr["Leavereason"].ToString()),
                        LeaveRequestStatus = Convert.ToString(sdr["LRStatus"].ToString()),
                        LeaveStatus = Convert.ToInt32(sdr["LeaveStatus"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "TimeSheet_Repository/GetAllMarkLeaveDetails");
            }
            return objList;
        }

        public string SaveMarkLeave(Leave_Model model)
        {
            string result = "Error on Saving Leave!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_Save_MarkLeave]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", model.UserId);
                    cmd.Parameters.AddWithValue("@FromDate", model.FromDate);
                    cmd.Parameters.AddWithValue("@ToDate", model.ToDate);
                    cmd.Parameters.AddWithValue("@DayCount", model.NoOfDays);
                    cmd.Parameters.AddWithValue("@Remarks", model.Remarks);
                    cmd.Parameters.AddWithValue("@LogInUser", model.CreatedBy);
                    cmd.Parameters.AddWithValue("@IPAddress", model.IPAddress);
                    cmd.Parameters.AddWithValue("@OfficeId", model.OfficeId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveMarkLeave");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, model.CreatedBy, "API", "TimeSheet_Repository/SaveMarkLeave");
            }
            return result;
        }
        #endregion

        #region Verify Leave
        public List<LeaveData> GetAllLeaveToVerifyDetails(int OfficeId, long CreatedBy)
        {
            List<LeaveData> objList = new List<LeaveData>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "SP_GetAllLeaveToVerify";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserId", CreatedBy);

                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new LeaveData
                    {
                        LId = Convert.ToInt64(sdr["LID"].ToString()),
                        Name = Convert.ToString(sdr["Name"].ToString()),
                        FromDate = Convert.ToString(sdr["FDate"].ToString()),
                        ToDate = Convert.ToString(sdr["TDate"].ToString()),
                        DayCount = Convert.ToString(sdr["TotalDay"].ToString()),
                        Remarks = Convert.ToString(sdr["Leavereason"].ToString()),
                        LeaveRequestStatus = Convert.ToString(sdr["LRStatus"].ToString()),
                        LeaveStatus = Convert.ToInt32(sdr["LeaveStatus"].ToString()),
                        TotalLeaves = Convert.ToString(sdr["TotalleaveTaken"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "TimeSheet_Repository/GetAllLeaveToVerifyDetails");
            }
            return objList;
        }

        public string ApproveLeaveRequest(long LogInUser, long LId)
        {
            string result = "Error on Approving Leave!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_Approve_LeaveRequest]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LeaveRequestId", LId);
                    //cmd.Parameters.AddWithValue("@LogInUser", LogInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/ApproveLeaveRequest");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/ApproveLeaveRequest");
            }
            return result;
        }

        public string RejectLeaveRequest(long LogInUser, long LId, string Remarks)
        {
            string result = "Error on Reject Leave!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_Reject_LeaveRequest]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LeaveRequestId", LId);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@Result";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/RejectLeaveRequest");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, LogInUser, "API", "TimeSheet_Repository/RejectLeaveRequest");
            }
            return result;
        }

        #endregion


        #region HR Sheet
        public string GetHrSheet(long LogInUser, long ClientId, string FromDate, string ToDate)
        {
            string header = string.Empty;
            string Query1 = string.Empty;
            try
            {
                string[] Main = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,".ToString().Split(',');
                string[] Alp = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF".ToString().Split(',');
                string[] Alps = "I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII,XIV,XV,XVI,XVII,XVIII,XIX,XX".ToString().Split(',');
                int x = 0;
                int z = 0;
                int m = 1;

                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetHrSheet";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@DateFrom", FromDate);
                cmd.Parameters.AddWithValue("@DateTo", ToDate);
                cmd.Parameters.AddWithValue("@ClientID", ClientId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet dset = new DataSet();
                da.Fill(dset);
                if (dset.Tables[0].Rows.Count > 0)
                {
                    header = dset.Tables[0].Rows[0]["header"].ToString() + " <tbody>";
                }

                if (dset.Tables[1].Rows.Count > 0)
                {


                    for (int i = 0; i < dset.Tables[1].Rows.Count; i++)
                    {
                        if (dset.Tables[1].Rows[i][1].ToString().Contains("<b>"))
                        {

                            Query1 = Query1 + "<tr class='success'>";

                            for (int j = 0; j < dset.Tables[1].Columns.Count; j++)
                            {
                                x = 0;
                                z = 0;
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td>" + m + "</td>";
                                    m = m + 1;
                                }
                                else
                                {
                                    Query1 = Query1 + "<td>" + dset.Tables[1].Rows[i][j].ToString() + "</td>";
                                }
                            }
                        }
                        else
                        {
                            Query1 = Query1 + "<tr class='activity' style='display:none'>";
                            z = 0;
                            for (int j = 0; j < dset.Tables[1].Columns.Count; j++)
                            {
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td>" + Alp[x].ToString() + "</td>";
                                    x = x + 1;
                                }
                                else
                                {
                                    Query1 = Query1 + "<td>" + dset.Tables[1].Rows[i][j].ToString() + "</td>";
                                }
                            }
                        }
                    }
                }
                Query1 = Query1 + "</tr>";
                header += Query1 + "</tbody></table>";
            }
            catch (Exception ex)
            {

            }
            return header;
        }
        #endregion

        #region MISReport
        public MISReport_Model GetHRsheetGraphEmployeewise(long LoggedInUser, int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            MISReport_Model objdata = new MISReport_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetHRsheetGraphEmployeewise";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@DateFrom", FromDate);
                cmd.Parameters.AddWithValue("@DateTo", ToDate);
                cmd.Parameters.AddWithValue("@ClientID", ClientId);
                cmd.Parameters.AddWithValue("@OfficeID", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.ReportGridData = (from DataRow dr in ds.Tables[0].Rows
                                                select new MISReportGrid
                                                {
                                                    Name = Convert.ToString(dr["Name"].ToString()),
                                                    Hours = Convert.ToString(dr["Hours"].ToString()),
                                                }).ToList();
                }
                objdata.Totaltime = Convert.ToString(ds.Tables[1].Rows[0]["Totaltime"].ToString());
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public MISReport_Model GetHRsheetGraphClientwise(long LoggedInUser, int OfficeId, int ClientId, string FromDate, string ToDate)
        {
            MISReport_Model objdata = new MISReport_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetHRsheetGraphClientwise";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@DateFrom", FromDate);
                cmd.Parameters.AddWithValue("@DateTo", ToDate);
                cmd.Parameters.AddWithValue("@ClientID", ClientId);
                cmd.Parameters.AddWithValue("@OfficeID", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.ReportGridData = (from DataRow dr in ds.Tables[0].Rows
                                              select new MISReportGrid
                                              {
                                                  Name = Convert.ToString(dr["Name"].ToString()),
                                                  Hours = Convert.ToString(dr["Hours"].ToString()),
                                              }).ToList();
                }
                objdata.Totaltime = Convert.ToString(ds.Tables[1].Rows[0]["Totaltime"].ToString());
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }
        #endregion
    }
}
