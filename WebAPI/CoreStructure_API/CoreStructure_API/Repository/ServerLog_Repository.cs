﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class ServerLog_Repository
    {

       


        public ServerLog_Model GetServerLog(long CreatedBy)
        {
            ServerLog_Model objModel = new ServerLog_Model();
            List<Log_Table_Model> objList = new List<Log_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "select Employee_table.EmployeeName as EmployeeName,Log_Table.SLNO,Log_Table.OperationON,Log_Table.Ramarks,Log_Table.IP_Address  from Log_Table inner join Employee_table on Log_Table.UserID=Employee_table.SLNO  order by Log_Table.SLNO desc";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Log_Table_Model tempobj = new Log_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());

                    tempobj.UserID = sdr["EmployeeName"].ToString();

                    tempobj.OperationON = Convert.ToDateTime(sdr["OperationON"].ToString());
                    tempobj.Remarks = sdr["Ramarks"].ToString();
                    tempobj.IP_Address = sdr["IP_Address"].ToString();

                    objList.Add(tempobj);
                }
                sdr.Close();


                connection.Close();
                objModel.Log_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ServerLog_Repository/GetServerLog");
            }

            return objModel;
        }
    }
}
