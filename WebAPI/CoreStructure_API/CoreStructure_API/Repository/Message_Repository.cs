﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Message_Repository
    {


        public DBReturnModel InsertMessageFromDocumentShare(Message_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Message!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertMessage";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("AdminID", objModel.AdminID);
                    cmd.Parameters.AddWithValue("From_user", objModel.AdminID);
                    cmd.Parameters.AddWithValue("FileLink", objModel.FileLink);
                    cmd.Parameters.AddWithValue("Status", false);
                    cmd.Parameters.AddWithValue("Doc_ID", objModel.Doc_ID);
                    cmd.Parameters.AddWithValue("Date", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("Section", "");
                    cmd.Parameters.AddWithValue("Message", objModel.Message);
                    cmd.Parameters.AddWithValue("Exparein", objModel.Expirein);
                    cmd.Parameters.AddWithValue("isReaded", false);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.EmpData";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    dataTable.Columns.Add("Email_ID", typeof(string));
                    foreach (var dta in objModel.EmployeeList)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.SLNO,dta.Email); 
                        }
                    }
                    SqlParameter parameter = new SqlParameter("EmpData", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);


                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.EmpData";
                    dataTable1.Columns.Add("User_ID", typeof(long));
                    dataTable1.Columns.Add("Email_ID", typeof(string));
                    foreach (var dta in objModel.VendorList)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable1.Rows.Add(dta.SLNO, dta.Email);
                        }
                    }
                    SqlParameter parameter1 = new SqlParameter("VenData", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);
                                       

                    var dataTable2 = new DataTable();
                    dataTable2.TableName = "dbo.GroupData";
                    dataTable2.Columns.Add("Group_ID", typeof(long));
                    foreach (var dta in objModel.GroupList)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable2.Rows.Add(dta.SLNO);
                        }
                    }
                    SqlParameter parameter2 = new SqlParameter("GroupData", SqlDbType.Structured);
                    parameter2.TypeName = dataTable2.TableName;
                    parameter2.Value = dataTable2;
                    cmd.Parameters.Add(parameter2);




                    var dataTable3 = new DataTable();
                    dataTable3.TableName = "dbo.DomainData";
                    dataTable3.Columns.Add("Domain_ID", typeof(long));
                    foreach (var dta in objModel.DomainList)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable3.Rows.Add(dta.SLNO);
                        }
                    }
                    SqlParameter parameter3 = new SqlParameter("DomainData", SqlDbType.Structured);
                    parameter3.TypeName = dataTable3.TableName;
                    parameter3.Value = dataTable3;
                    cmd.Parameters.Add(parameter3);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.AdminID, "API", "Message_Repository/InsertMessageFromDocumentShare");
                    objReturn.ReturnMessage = "System Error on Inserting Document Share!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.AdminID, "API", "Message_Repository/InsertMessageFromDocumentShare");
                objReturn.ReturnMessage = "System Error on Inserting Document Share!";
            }

            return objReturn;
        }
    }
}
