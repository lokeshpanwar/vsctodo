﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class MastServPro_Repository
    {

        public DBReturnModel InsertSeriveProduct(MastServPro_Model objModel)
        {
            string result = "Error on Inserting Service/Product Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertMastServPro";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@ProSerid", objModel.ProSerid);
                    cmd.Parameters.AddWithValue("@SerProductname", objModel.SerProductname);
                    cmd.Parameters.AddWithValue("@serDescription", objModel.serDescription);
                    cmd.Parameters.AddWithValue("@MastProSerTypeID", objModel.MastProSerTypeID);
                    cmd.Parameters.AddWithValue("@creationdate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@creationuser", objModel.creationuser);
                    cmd.Parameters.AddWithValue("@isdeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastServPro_Repository/InsertSeriveProduct");
                    objreturn.ReturnMessage = "System Error on Inserting Service/Product Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastServPro_Repository/InsertSeriveProduct");
            }
            return objreturn;

        }

        public MastServPro_Model GetOneServiceProduct(long EntryId, long creationuser)
        {
            MastServPro_Model objModel = new MastServPro_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectMastServProListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EntryId", EntryId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.EntryId = Convert.ToInt64(sdr["EntryId"].ToString());
                    objModel.ProSerid = Convert.ToInt64(sdr["ProSerid"].ToString());
                    objModel.SerProductname = sdr["SerProductname"].ToString();
                    objModel.serDescription = sdr["serDescription"].ToString();
                    objModel.MastProSerTypeID = Convert.ToInt32(sdr["MastProSerTypeID"].ToString());
                    objModel.creationuser = sdr["creationuser"].ToString();
                    objModel.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objModel.modifactionuser = sdr["modifactionuser"].ToString();
                    objModel.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "MastServPro_Repository/GetOneServiceProduct");
            }
            return objModel;
        }


        public DBReturnModel UpdateServiceProductMaster(MastServPro_Model objModel)
        {
            string result = "Error on Updating ServiceProduct Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateServiceProduct";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryId", objModel.EntryId);
                    cmd.Parameters.AddWithValue("@ProSerid", objModel.ProSerid);
                    cmd.Parameters.AddWithValue("@SerProductname", objModel.SerProductname);
                    cmd.Parameters.AddWithValue("@serDescription", objModel.serDescription);
                    cmd.Parameters.AddWithValue("@MastProSerTypeID", objModel.MastProSerTypeID);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);
                    //cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);

                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastServPro_Repository/UpdateServiceProductMaster");
                    objreturn.ReturnMessage = "System Error on Updating ServiceProduct Master";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastServPro_Repository/UpdateServiceProductMaster");
            }
            return objreturn;
        }


        public DBReturnModel DisableServiceproductMaster(long EntryId, long modifactionuser)
        {

            string result = "Error on Disabling Service/product Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update MastServPro set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where EntryId=@EntryId";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryId", EntryId);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Service/product Master Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "MastServPro_Repository/DisableServiceproductMaster");
                    objreturn.ReturnMessage = "System Error on Disabling Service/product Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "MastServPro_Repository/DisableServiceproductMaster");
            }

            return objreturn;
        }


        public DBReturnModel EnableServiceproductMaster(long EntryId, long modifactionuser)
        {

            string result = "Error on Disabling Service/product Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update MastServPro set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where EntryId=@EntryId";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryId", EntryId);
                    cmd.Parameters.AddWithValue("@isdeleted", false);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Service/product Master Enable Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "MastServPro_Repository/EnableServiceproductMaster");
                    objreturn.ReturnMessage = "System Error on Enabling Service/product Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "MastServPro_Repository/EnableServiceproductMaster");
            }

            return objreturn;
        }
        
        public MastServPro_page_Model GetServiceProductMasterByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            MastServPro_page_Model objModel = new MastServPro_page_Model();
            List<MastServPro_Model> objlist = new List<MastServPro_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "EntryId";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectServerProductMasterByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    MastServPro_Model objtemp = new MastServPro_Model();
                    objtemp.EntryId = Convert.ToInt64(sdr["EntryId"].ToString());
                    objtemp.ProSerid = Convert.ToInt64(sdr["ProSerid"].ToString());
                    objtemp.SerProductname = sdr["SerProductname"].ToString();
                    objtemp.serDescription = sdr["serDescription"].ToString();
                    objtemp.MastProSerTypeID = Convert.ToInt32(sdr["MastProSerTypeID"].ToString());
                    objtemp.creationuser = sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    objtemp.ServiceProductType = sdr["ServiceProductType"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From MastServPro where (Isnull(ProSerid,'') like '%'+@search+'%' "
                + " or Isnull(SerProductname,'') like '%'+@search+'%') ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.MastServPro_Model_List= objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "MastServPro_Repository/GetServiceProductMasterByPage");

            }

            return objModel;
        }



        public int GetMaxServiceProductId(long creationuser)
        {
            int objModel =0;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("select ISNULL((MAX(ProSerid)),0)+1 as Id from MastServPro", connection);
                cmd.CommandType = CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel = Convert.ToInt32(sdr["Id"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "MastServPro_Repository/GetMaxServiceProductId");
            }
            return objModel;
        }
    }
}
