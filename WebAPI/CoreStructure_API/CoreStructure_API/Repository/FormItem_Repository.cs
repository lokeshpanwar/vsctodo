﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class FormItem_Repository
    {

        public string InsertFormItem(Form_Item_Table_Model objModel)
        {
            string result = "Error on Inserting FormItem!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertFormItem";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Form_ID", objModel.Form_ID);
                    cmd.Parameters.AddWithValue("@Form_ItemType", objModel.Form_ItemType);
                    cmd.Parameters.AddWithValue("@Form_ItemValue", objModel.Form_ItemValue);
                    cmd.Parameters.AddWithValue("@isMulti", objModel.isMulti);
                    cmd.Parameters.AddWithValue("@isMandatory", objModel.isMandatory);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@IsHorizAllign", objModel.IsHorizAllign);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "FormItem_Repository/InsertFormItem");
                    result = "System Error on Inserting FormItem";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "FormItem_Repository/InsertFormItem");
                result = "System Error on Inserting FormItem";
            }

            return result;
        }



        public Form_Item_Table_Model GetOneFormItem(long SLNO, long CreatedBy)
        {
            Form_Item_Table_Model objModel = new Form_Item_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormItemById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                cmd.Parameters.AddWithValue("@Form_ID", 0);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    objModel.FormName = sdr["FormName"].ToString();
                    objModel.Form_ID = Convert.ToInt64(sdr["Form_ID"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objModel.Form_ItemType =sdr["Form_ItemType"].ToString();
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Form_ItemValue = sdr["Form_ItemValue"].ToString();
                    objModel.IsHorizAllign = Convert.ToBoolean(sdr["IsHorizAllign"].ToString());
                    objModel.isMandatory = Convert.ToBoolean(sdr["isMandatory"].ToString());
                    objModel.isMulti = Convert.ToBoolean(sdr["isMulti"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "FormItem_Repository/GetOneFormItem");
            }

            return objModel;

        }



        public List<Form_Item_Table_Model> GetFormItemList(long CreatedBy, long FormId)
        {
            List<Form_Item_Table_Model> objModel = new List<Form_Item_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormItemById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", 0);
                cmd.Parameters.AddWithValue("@Form_ID", FormId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Form_Item_Table_Model tempobj = new Form_Item_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.Form_ID = Convert.ToInt64(sdr["Form_ID"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.Form_ItemType = sdr["Form_ItemType"].ToString();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Form_ItemValue = sdr["Form_ItemValue"].ToString();
                    tempobj.IsHorizAllign = Convert.ToBoolean(sdr["IsHorizAllign"].ToString());
                    tempobj.isMandatory = Convert.ToBoolean(sdr["isMandatory"].ToString());
                    tempobj.isMulti = Convert.ToBoolean(sdr["isMulti"].ToString());

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "FormItem_Repository/GetFormItemList");
            }

            return objModel;

        }



        public string UpdateFormItem(Form_Item_Table_Model objModel)
        {
            string result = "Error on Updating FormItem!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateFormItem";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);                    
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@Form_ID", objModel.Form_ID);
                    cmd.Parameters.AddWithValue("@Form_ItemType", objModel.Form_ItemType);
                    cmd.Parameters.AddWithValue("@Form_ItemValue", objModel.Form_ItemValue);
                    cmd.Parameters.AddWithValue("@isMulti", objModel.isMulti);
                    cmd.Parameters.AddWithValue("@isMandatory", objModel.isMandatory);
                    cmd.Parameters.AddWithValue("@IsHorizAllign", objModel.IsHorizAllign);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "FormItem_Repository/UpdateFormItem");
                    result = "System Error on updating FormItem";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "FormItem_Repository/UpdateFormItem");
                result = "System Error on updating FormItem";
            }

            return result;
        }



        public Form_Item_table_page_Model GetFormItemListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Form_Item_table_page_Model objModel = new Form_Item_table_page_Model();
            List<Form_Item_Table_Model> objList = new List<Form_Item_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectFormItemByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Form_Item_Table_Model tempobj = new Form_Item_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.Form_ID = Convert.ToInt64(sdr["Form_ID"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.Form_ItemType = sdr["Form_ItemType"].ToString();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Form_ItemValue = sdr["Form_ItemValue"].ToString();
                    tempobj.IsHorizAllign = Convert.ToBoolean(sdr["IsHorizAllign"].ToString());
                    tempobj.isMandatory = Convert.ToBoolean(sdr["isMandatory"].ToString());
                    tempobj.isMulti = Convert.ToBoolean(sdr["isMulti"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From FormItem_table where (Isnull(Form_ItemValue,'') like '%'+@search+'%' or "
                        + " Isnull(Form_ItemType,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Form_Item_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "FormItem_Repository/GetFormItemListByPage");
            }

            return objModel;

        }



        public string DisableFormItem(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Form Item!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Form_Item_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Form Item Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "FormItem_Repository/DisableFormItem");
                    result = "System Error on Disabling Form Item";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "FormItem_Repository/DisableFormItem");
                result = "System Error on Disabling FormItem";
            }

            return result;
        }




        public string EnableFormItem(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Form Item!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Form_Item_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Form Item Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "FormItem_Repository/EnableFormItem");
                    result = "System Error on Enabling Form Item";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "FormItem_Repository/EnableFormItem");
                result = "System Error on Enabling Form Item";
            }

            return result;
        }

    }
}
