﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class SubCompany_Repository
    {


        public DBReturnModel InsertSubCompany(Sub_Company_table_Model objModel)
        {
            string result = "Error on Inserting Sub Company!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var emailsplit = objModel.Email.Split("@");

                objModel = NullToBlank(objModel);
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";
                    sqlstr = "CB_SP_InsertSubCompany";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Parent_compID", objModel.Parent_compID);
                    cmd.Parameters.AddWithValue("@Parent_compName", objModel.Parent_compName);
                    cmd.Parameters.AddWithValue("@SubCompanyID", objModel.SubCompanyID);
                    cmd.Parameters.AddWithValue("@SubCompanyName", objModel.SubCompanyName);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Contact_No);
                    cmd.Parameters.AddWithValue("@ContactPerson", objModel.ContactPerson);
                    cmd.Parameters.AddWithValue("@Email", objModel.Email);
                    cmd.Parameters.AddWithValue("@IsParentCompany", objModel.IsParentCompany);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);

                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());
                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();


                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "SubCompany_Repository/InsertSubCompany");
                    objreturn.ReturnMessage = "System Error on Inserting Sub Company";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "SubCompany_Repository/InsertSubCompany");
            }

            return objreturn;
        }



        public List<Sub_Company_table_Model> GetSubCompanyList(long CreatedBy)
        {
            List<Sub_Company_table_Model> objModel = new List<Sub_Company_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO,Isnull(Parent_compID,0) as Parent_compID, Isnull(Parent_compName,'') as Parent_compName,"
                    + " isnull(SubCompanyID,'') as SubCompanyID, isnull(SubCompanyName,'') as SubCompanyName, "
                + " isnull(Address,'') as Address, isnull(Contact_No,0) as Contact_No, isnull(ContactPerson,'') as ContactPerson,"
                + " isnull(Email,'') as Email,CreatedDate,CreatedBy,isnull(IsParentCompany,'') as IsParentCompany, IsDeleted From Sub_Company_table";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Sub_Company_table_Model tempobj = new Sub_Company_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Parent_compID = Convert.ToInt64(sdr["Parent_compID"].ToString());
                    tempobj.Parent_compName = sdr["Parent_compName"].ToString();
                    tempobj.SubCompanyID = sdr["SubCompanyID"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.Address = sdr["Address"].ToString();
                    tempobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempobj.ContactPerson = sdr["ContactPerson"].ToString();
                    tempobj.Email = sdr["Email"].ToString();
                    tempobj.IsParentCompany = Convert.ToBoolean(sdr["IsParentCompany"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "SubCompany_Repository/GetSubCompanyList");
            }

            return objModel;

        }



        public Sub_Company_table_Model GetOneSubCompany(long SLNO, long CreatedBy)
        {
            Sub_Company_table_Model objModel = new Sub_Company_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO,Isnull(Parent_compID,0) as Parent_compID, Isnull(Parent_compName,'') as Parent_compName,"
                    + " isnull(SubCompanyID,'') as SubCompanyID, isnull(SubCompanyName,'') as SubCompanyName, "
                + " isnull(Address,'') as Address, isnull(Contact_No,0) as Contact_No, isnull(ContactPerson,'') as ContactPerson,"
                + " isnull(Email,'') as Email,CreatedDate,CreatedBy,isnull(IsParentCompany,'') as IsParentCompany, IsDeleted From Sub_Company_table"
                + " where SLNO=@SLNO";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Parent_compID = Convert.ToInt64(sdr["Parent_compID"].ToString());
                    objModel.Parent_compName = sdr["Parent_compName"].ToString();
                    objModel.SubCompanyID = sdr["SubCompanyID"].ToString();
                    objModel.SubCompanyName = sdr["SubCompanyName"].ToString();
                    objModel.Address = sdr["Address"].ToString();
                    objModel.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    objModel.ContactPerson = sdr["ContactPerson"].ToString();
                    objModel.Email = sdr["Email"].ToString();
                    objModel.IsParentCompany = Convert.ToBoolean(sdr["IsParentCompany"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "SubCompany_Repository/GetOneSubCompany");
            }

            return objModel;

        }



        public DBReturnModel UpdateSubCompany(Sub_Company_table_Model objModel)
        {
            string result = "Error on Updating Sub Company!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                objModel = NullToBlank(objModel);
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_UpdateSubCompany";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Parent_compID", objModel.Parent_compID);
                    cmd.Parameters.AddWithValue("@Parent_compName", objModel.Parent_compName);
                    cmd.Parameters.AddWithValue("@SubCompanyID", objModel.SubCompanyID);
                    cmd.Parameters.AddWithValue("@SubCompanyName", objModel.SubCompanyName);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Contact_No);
                    cmd.Parameters.AddWithValue("@ContactPerson", objModel.ContactPerson);
                    cmd.Parameters.AddWithValue("@Email", objModel.Email);
                    cmd.Parameters.AddWithValue("@IsParentCompany", objModel.IsParentCompany);

                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedDate", StandardDateTime.GetDateTime());

                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "SubCompany_Repository/UpdateSubCompany");
                    objreturn.ReturnMessage = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "SubCompany_Repository/UpdateSubCompany");
            }

            return objreturn;
        }



        public string DisableSubCompany(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Sub Company!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Sub_Company_table set IsDeleted=@IsDeleted,"
                         + " DeletedOn=@DeletedOn,DeletedBy=@DeletedBy "
                         + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Sub Company Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/DisableSubCompany");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/DisableSubCompany");
            }

            return result;
        }



        public string EnableSubCompany(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Sub Company!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Sub_Company_table set IsDeleted=@IsDeleted,"
                         + " DeletedOn=@DeletedOn,DeletedBy=@DeletedBy "
                         + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Sub Company Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/EnableSubCompany");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/EnableSubCompany");
            }

            return result;
        }



        public Sub_Company_table_page_Model GetSubCompanyListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Sub_Company_table_page_Model objModel = new Sub_Company_table_page_Model();
            List<Sub_Company_table_Model> objList = new List<Sub_Company_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectSubCompanyByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Sub_Company_table_Model tempobj = new Sub_Company_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Parent_compID = Convert.ToInt64(sdr["Parent_compID"].ToString());
                    tempobj.Parent_compName = sdr["Parent_compName"].ToString();
                    tempobj.SubCompanyID = sdr["SubCompanyID"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.Address = sdr["Address"].ToString();
                    tempobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempobj.ContactPerson = sdr["ContactPerson"].ToString();
                    tempobj.Email = sdr["Email"].ToString();
                    tempobj.IsParentCompany = Convert.ToBoolean(sdr["IsParentCompany"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Sub_Company_Table where (Isnull(Parent_compID,0) like '%'+@search+'%' or "
                        + " Isnull(Parent_compName,'') like '%'+@search+'%' or Isnull(SubCompanyID,0) like '%'+@search+'%' or "
                        + " Isnull(SubCompanyName,'') like '%'+@search+'%' or Isnull(Address,'') like '%'+@search+'%' or "
                        + " Isnull(Contact_No,0) like '%'+@search+'%' or Isnull(ContactPerson,'') like '%'+@search+'%' or "
                        + " Isnull(Email,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Sub_Company_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "SubCompany_Repository/GetSubCompanyListByPage");
            }

            return objModel;

        }


        private Sub_Company_table_Model NullToBlank(Sub_Company_table_Model objModel)
        {
            if (objModel.Parent_compName == null)
            {
                objModel.Parent_compName = "";
            }
            if (objModel.SubCompanyID == null)
            {
                objModel.SubCompanyID = "";
            }
            if (objModel.SubCompanyName == null)
            {
                objModel.SubCompanyName = "";
            }
            return objModel;
        }



        private List<Sub_Company_table_Model> NullToBlank(List<Sub_Company_table_Model> objModel)
        {
            foreach (var data in objModel)
            {
                if (data.Parent_compName == null)
                {
                    data.Parent_compName = "";
                }
                if (data.SubCompanyID == null)
                {
                    data.SubCompanyID = "";
                }
                if (data.SubCompanyName == null)
                {
                    data.SubCompanyName = "";
                }
            }
            return objModel;
        }




        public DBReturnModel InsertSubCompanyFromBulk(List<Sub_Company_table_Model> objModel, long CreatedBy)
        {
            string result = "Error on Inserting Sub Company!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                objModel = NullToBlank(objModel);
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";
                    sqlstr = "CB_SP_InsertSub_Company_table_Bulk";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    
                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Sub_Company_table_Type";
                    dataTable.Columns.Add("Parent_compID", typeof(long));
                    dataTable.Columns.Add("Parent_compName", typeof(string));
                    dataTable.Columns.Add("SubCompanyID", typeof(string));
                    dataTable.Columns.Add("SubCompanyName", typeof(string));
                    dataTable.Columns.Add("Address", typeof(string));
                    dataTable.Columns.Add("Contact_No", typeof(long));
                    dataTable.Columns.Add("ContactPerson", typeof(string));
                    dataTable.Columns.Add("Email", typeof(string));
                    dataTable.Columns.Add("CountryCode", typeof(string));
                    foreach (var dta in objModel)
                    {
                        dataTable.Rows.Add(dta.Parent_compID, dta.Parent_compName, dta.SubCompanyID, dta.SubCompanyName,
                            dta.Address, dta.Contact_No, dta.ContactPerson, dta.Email, dta.CountryCode); // Id of '1' is valid for the Person table
                    }
                    SqlParameter parameter = new SqlParameter("@Sub_Company_table_Type", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);
                                       
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());
                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "SubCompany_Repository/InsertSubCompanyFromBulk");
                    objreturn.ReturnMessage = "System Error on Inserting Sub Company";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "SubCompany_Repository/InsertSubCompanyFromBulk");
            }

            return objreturn;
        }



        public DBReturnModel DeleteSubCompany(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Sub Company!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteSubCompany";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var ret = new SqlParameter();
                    ret.ParameterName = "ret";
                    ret.SqlDbType = SqlDbType.VarChar;
                    ret.Size = 500;
                    ret.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(ret);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = ret.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/DeleteSubCompany");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "SubCompany_Repository/DeleteSubCompany");
            }

            return objreturn;
        }


    }
}
