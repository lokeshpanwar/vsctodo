﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class DocumentRevisionReport_Repository
    {
        public DocumentRevisionReport_Model GetDocumentRevisionReport(DateTime FromDate, DateTime ToDate, long UserId,long DocumentClassID, long CreatedBy)
        {
            DocumentRevisionReport_Model objModel = new DocumentRevisionReport_Model();
            List<Document_Revision_Log_Table_Model> objList = new List<Document_Revision_Log_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "SELECT * FROM Document_Revision_Log_Table WHERE UserID=@UserID and DocumentClassID=@DocumentClassID and" +
                    "  Cast(Datetime as Date) Between Cast(@FromDate as Date) and Cast(@ToDate as Date) ";

               
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@UserID", UserId);
                cmd.Parameters.AddWithValue("@DocumentClassID", DocumentClassID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Revision_Log_Table_Model tempobj = new Document_Revision_Log_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                  
                    //tempobj.UserID = sdr["UserID"].ToString();
                    tempobj.RevisionID = sdr["RevisionID"].ToString();
                    tempobj.Action = sdr["Action"].ToString();
                    tempobj.Datetime = Convert.ToDateTime(sdr["Datetime"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    tempobj.Filename = sdr["Filename"].ToString();
                    tempobj.Comments = sdr["Comments"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();


                connection.Close();
                objModel.Document_Revision_Log_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentRevisionReport_Repository/GetDocumentRevisionReport");
            }

            return objModel;
        }

    }
}
