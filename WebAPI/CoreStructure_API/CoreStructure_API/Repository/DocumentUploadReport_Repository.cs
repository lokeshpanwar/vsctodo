﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class DocumentUploadReport_Repository
    {

        public DocumentUploadReport_Model GetDocumentUploadReport(DateTime FromDate, DateTime ToDate, long UserId,string DocumentClassName, long CreatedBy)
         {
            DocumentUploadReport_Model objModel = new DocumentUploadReport_Model();
            List<Log_Table_Model> objList = new List<Log_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO,Parent_CompID, Sub_CompID, OperationType, Operation_Performed,  "
                + " OperationON, Ramarks, IP_Address, user_id,  (Select Userid From Employee_table where  "
                + " Employee_table.SLNO=Log_Table.User_id) as UserID from Log_Table where UserID = @UserID  "
                + " and (OperationType='UPLOAD' and Log_Table.Ramarks=' Document Uploaded to "+DocumentClassName+"')"
                + " and Cast(OperationON as Date) Between Cast(@FromDate as Date) and Cast(@ToDate as Date) ";
                 
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@UserID", UserId);
               
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Log_Table_Model tempobj = new Log_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Parent_CompID = sdr["Parent_CompID"].ToString();
                    tempobj.Sub_CompID = sdr["Sub_CompID"].ToString();
                    tempobj.UserID = sdr["UserID"].ToString();
                    tempobj.OperationType = sdr["OperationType"].ToString();
                    tempobj.Operation_Performed = sdr["Operation_Performed"].ToString();
                    tempobj.OperationON = Convert.ToDateTime(sdr["OperationON"].ToString());
                    tempobj.Remarks = sdr["Ramarks"].ToString();
                    tempobj.IP_Address = sdr["IP_Address"].ToString();
                    tempobj.user_id = Convert.ToInt64(sdr["user_id"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();


                connection.Close();
                objModel.Log_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentUploadReport_Repository/GetDocumentUploadReport");
            }

            return objModel;
        }

    }
}
