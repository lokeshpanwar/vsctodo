﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Priority_Repository
    {

        public DBReturnModel InsertPriority(W_Priority_Table_Model objModel)
        {
            string result = "Error on Inserting Priority!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertPriority";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SCoy", objModel.SCoy);
                    cmd.Parameters.AddWithValue("@FDom", objModel.FDom);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@PriName", objModel.PriName);
                    cmd.Parameters.AddWithValue("@PriDesc", objModel.PriDesc);
                    cmd.Parameters.AddWithValue("@PriPer", objModel.PriPer);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 100;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Priority_Repository/InsertPriority");
                    objReturn.ReturnMessage = "System Error on Inserting Priority!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Priority_Repository/InsertPriority");
            }

            return objReturn;
        }


        public W_Priority_Table_Page_Model GetPriorityListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            W_Priority_Table_Page_Model objModel = new W_Priority_Table_Page_Model();
            List<W_Priority_Table_Model> objList = new List<W_Priority_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectPriorityByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    W_Priority_Table_Model tempobj = new W_Priority_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.PriName = sdr["PriName"].ToString();
                    tempobj.PriDesc = sdr["PriDesc"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.PriPer = sdr["PriPer"].ToString();
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From W_Priority_Table where (Isnull(PriName,'') like '%'+@search+'%' or "
                        + " Isnull(PriDesc,'') like '%'+@search+'%' or Isnull(PriPer,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.W_Priority_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Priority_Repository/GetPriorityListByPage");
            }

            return objModel;

        }


        public DBReturnModel UpdatePriority(W_Priority_Table_Model objModel)
        {
            string result = "Error on Updating Priority!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdatePriority";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@PriName", objModel.PriName);
                    cmd.Parameters.AddWithValue("@PriDesc", objModel.PriDesc);
                    cmd.Parameters.AddWithValue("@PriPer", objModel.PriPer);
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@IsDeleted", false);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 100;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Priority_Repository/UpdatePriority");
                    objReturn.ReturnMessage = "System Error on updating Priority";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Priority_Repository/UpdatePriority");
                objReturn.ReturnMessage = "System Error on updating Priority";
            }

            return objReturn;
        }


        public W_Priority_Table_Model GetOnePriority(long SLNO, long CreatedBy)
        {
            W_Priority_Table_Model objModel = new W_Priority_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectPriorityById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.PriName = sdr["PriName"].ToString();
                    objModel.PriDesc = sdr["PriDesc"].ToString();
                    objModel.PriPer = sdr["PriPer"].ToString();

                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objModel.SCoy = Convert.ToInt64(sdr["SCoy"].ToString());
                    objModel.FDom = Convert.ToInt64(sdr["FDom"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Priority_Repository/GetOnePriority");
            }

            return objModel;

        }






        public string DisablePriority(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Priority!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update W_Priority_Table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Priority Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Priority_Repository/DisablePriority");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Priority_Repository/DisablePriority");
            }

            return result;
        }

        public string EnablePriority(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Priority!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update W_Priority_Table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Priority Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Priority_Repository/EnablePriority");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Priority_Repository/EnablePriority");
            }

            return result;
        }

    }
}
