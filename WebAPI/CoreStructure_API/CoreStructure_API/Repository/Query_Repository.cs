﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class Query_Repository
    {
        #region Query
        public string AddNewQuery(Query objModel)
        {
            string result = "Error on Inserting Query!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateQuery]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", objModel.QId);
                    cmd.Parameters.AddWithValue("@ClientName", objModel.ClientName);
                    cmd.Parameters.AddWithValue("@ContactPersonId", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@QueryDate", objModel.QueryDate);
                    cmd.Parameters.AddWithValue("@FatherName", objModel.FatherName);
                    cmd.Parameters.AddWithValue("@MobileNo", objModel.MobileNo);
                    cmd.Parameters.AddWithValue("@EmailId", objModel.EmailId);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@StateId", objModel.StateId);
                    cmd.Parameters.AddWithValue("@CityId", objModel.CityId);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@PolicyExpiryDate", objModel.PolicyExpiryDate);
                    cmd.Parameters.AddWithValue("@QuatationAmount", objModel.QuatationAmount);
                    cmd.Parameters.AddWithValue("@Remarks", objModel.Remarks);
                    cmd.Parameters.AddWithValue("@ReferencePerson", objModel.ReferencePerson);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/AddNewQuery");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/AddNewQuery");
            }
            return result;
        }
        public List<Query> GetQueryList(int OfficeId, long CreatedBy)
        {
            List<Query> objList = new List<Query>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetQueryList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Query
                    {
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientName = sdr["ClientName"].ToString(),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = sdr["Name"].ToString(),
                        QueryDate = sdr["QDate"].ToString(),
                        FatherName = sdr["FatherName"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        EmailId = sdr["EmailID"].ToString(),
                        Address = sdr["Address"].ToString(),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = sdr["StateName"].ToString(),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = sdr["CityName"].ToString(),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = sdr["CategoryName"].ToString(),
                        PolicyExpiryDate = sdr["ExpiryDate"].ToString(),
                        QuatationAmount = Convert.ToDecimal(sdr["QuatationAmount"].ToString()),
                        Remarks = sdr["Remarks"].ToString(),
                        QStatus = Convert.ToInt32(sdr["QStatus"].ToString()),
                        QStatusName = sdr["QStatusName"].ToString(),
                        ReferencePerson = Convert.ToString(sdr["ReferencePerson"].ToString()),
                        PendingRemark = Convert.ToString(sdr["PendingRemark"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetQueryList");
            }
            return objList;
        }
        public List<Query> GetClosedQueryList(int OfficeId, long CreatedBy)
        {
            List<Query> objList = new List<Query>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetClosedQueryList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Query
                    {
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientName = sdr["ClientName"].ToString(),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = sdr["Name"].ToString(),
                        QueryDate = sdr["QDate"].ToString(),
                        FatherName = sdr["FatherName"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        EmailId = sdr["EmailID"].ToString(),
                        Address = sdr["Address"].ToString(),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = sdr["StateName"].ToString(),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = sdr["CityName"].ToString(),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = sdr["CategoryName"].ToString(),
                        PolicyExpiryDate = sdr["ExpiryDate"].ToString(),
                        QuatationAmount = Convert.ToDecimal(sdr["QuatationAmount"].ToString()),
                        Remarks = sdr["Remarks"].ToString(),
                        QStatus = Convert.ToInt32(sdr["QStatus"].ToString()),
                        QStatusName = sdr["QStatusName"].ToString(),
                        ReferencePerson = Convert.ToString(sdr["ReferencePerson"].ToString()),
                        PendingRemark = Convert.ToString(sdr["PendingRemark"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetClosedQueryList");
            }
            return objList;
        }

        public string DeleteQuery(long QId, long logInUser)
        {
            string result = "Error on Deleting Query!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteQuery]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", QId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Query_Repository/DeleteQuery");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Query_Repository/DeleteQuery");
            }
            return result;
        }

        public string CloseQueryUpdate(long QId, string Remark, long CreatedBy)
        {
            string result = "Error on Updating Query Status!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_CloseQueryUpdate]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", QId);
                    cmd.Parameters.AddWithValue("@Remarks", Remark);
                    cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/CloseQueryUpdate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/CloseQueryUpdate");
            }
            return result;
        }
        public string ConvertQueryUpdate(long QId, long CreatedBy)
        {
            string result = "Error on Convert Query!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_ConvertQueryUpdate]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", QId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/ConvertQueryUpdate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/ConvertQueryUpdate");
            }
            return result;
        }

        public string PendingQueryRemarkUpdate(long QId, string Remark, long CreatedBy)
        {
            string result = "Error on Updating Query Status!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_PendingQueryRemarkUpdate]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", QId);
                    cmd.Parameters.AddWithValue("@Remarks", Remark);
                    cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/PendingQueryRemarkUpdate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/PendingQueryRemarkUpdate");
            }
            return result;
        }

        public List<Query> GetAllQueryListByUser(int OfficeId, long CreatedBy)
        {
            List<Query> objList = new List<Query>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetAllQueryListById";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Query
                    {
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientName = sdr["ClientName"].ToString(),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = sdr["Name"].ToString(),
                        QueryDate = sdr["QDate"].ToString(),
                        FatherName = sdr["FatherName"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        EmailId = sdr["EmailID"].ToString(),
                        Address = sdr["Address"].ToString(),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = sdr["StateName"].ToString(),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = sdr["CityName"].ToString(),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = sdr["CategoryName"].ToString(),
                        PolicyExpiryDate = sdr["ExpiryDate"].ToString(),
                        QuatationAmount = Convert.ToDecimal(sdr["QuatationAmount"].ToString()),
                        Remarks = sdr["Remarks"].ToString(),
                        QStatus = Convert.ToInt32(sdr["QStatus"].ToString()),
                        QStatusName = sdr["QStatusName"].ToString(),
                        ReferencePerson = Convert.ToString(sdr["ReferencePerson"].ToString()),
                        PendingRemark = Convert.ToString(sdr["PendingRemark"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetAllQueryListByUser");
            }
            return objList;
        }

        #endregion

        #region UnderWrite
        public string SaveUnderWritePolicy(UnderWrite objModel)
        {
            string result = "Error on UnderWrite Query!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveUnderWritePolicy]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QId", objModel.QId);
                    cmd.Parameters.AddWithValue("@TypeId", objModel.TypeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@ContactPersonId", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@PolicyHolderId", objModel.PolicyHolderId);
                    cmd.Parameters.AddWithValue("@UnderWriteDate", objModel.UnderWriteDate);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", objModel.IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@ProductId", objModel.ProductId);
                    cmd.Parameters.AddWithValue("@SubjectInsured", objModel.SubjectInsured);
                    cmd.Parameters.AddWithValue("@CostCenter", objModel.CostCenter);
                    cmd.Parameters.AddWithValue("@CommissionRate", objModel.CommissionRate);
                    cmd.Parameters.AddWithValue("@Premium", objModel.Premium);
                    cmd.Parameters.AddWithValue("@CGST", objModel.CGST);
                    cmd.Parameters.AddWithValue("@SGST", objModel.SGST);
                    cmd.Parameters.AddWithValue("@PremiumPayable", objModel.PremiumPayable);
                    cmd.Parameters.AddWithValue("@DiscountProvided", objModel.DiscountProvided);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@fileName", objModel.fileName);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveUnderWritePolicy");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveUnderWritePolicy");
            }
            return result;
        }

        public List<UnderWrite> GetQueryDetailsForUnderWrite(int OfficeId, long QId, long CreatedBy)
        {
            List<UnderWrite> objList = new List<UnderWrite>();

            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetQueryDetailsForUnderWrite";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@QId", QId);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new UnderWrite
                    {
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPerson"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetQueryDetailsForUnderWrite");
            }
            return objList;
        }

        #endregion

        #region Policy
        public List<Policy> GetPolicyList(int OfficeId, long CreatedBy)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetPolicyList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                        ContactNo = Convert.ToString(sdr["MobileNo"].ToString()),
                        UW_FileName = Convert.ToString(sdr["UW_FileName"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetPolicyList");
            }
            return objList;
        }
        public List<Policy> GetDeletedPolicyList(int OfficeId, long CreatedBy)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetDeletedPolicyList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetDeletedPolicyList");
            }
            return objList;
        }
        public string SaveGeneratePolicy(Policy objModel)
        {
            string result = "Error on Generating Policy!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveGeneratePolicy]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PId", objModel.PId);
                    cmd.Parameters.AddWithValue("@QId", objModel.QId);
                    cmd.Parameters.AddWithValue("@TypeId", objModel.TypeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@ContactPersonId", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@PolicyHolderId", objModel.PolicyHolderId);
                    cmd.Parameters.AddWithValue("@UnderWriteDate", objModel.UnderWriteDate);

                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@VehicleNo", objModel.VehicleNo);
                    cmd.Parameters.AddWithValue("@OnDamage", objModel.OnDamage);
                    cmd.Parameters.AddWithValue("@PersonalAccident", objModel.PersonalAccident);
                    cmd.Parameters.AddWithValue("@ThirdParty", objModel.ThirdParty);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", objModel.IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@ProductId", objModel.ProductId);

                    cmd.Parameters.AddWithValue("@SubjectInsured", objModel.SubjectInsured);
                    cmd.Parameters.AddWithValue("@CostCenter", objModel.CostCenter);
                    cmd.Parameters.AddWithValue("@CommissionRate", objModel.CommissionRate);
                    cmd.Parameters.AddWithValue("@Premium", objModel.Premium);
                    cmd.Parameters.AddWithValue("@CGST", objModel.CGST);
                    cmd.Parameters.AddWithValue("@SGST", objModel.SGST);
                    cmd.Parameters.AddWithValue("@PremiumPayable", objModel.PremiumPayable);
                    cmd.Parameters.AddWithValue("@IDV", objModel.IDV);
                    cmd.Parameters.AddWithValue("@PolicyDate", objModel.PolicyDate);
                    cmd.Parameters.AddWithValue("@ExpiryDate", objModel.ExpiryDate);
                    cmd.Parameters.AddWithValue("@PolicyNo", objModel.PolicyNo);
                    cmd.Parameters.AddWithValue("@PreviousPolicyNo", objModel.PreviousPolicyNo);
                    cmd.Parameters.AddWithValue("@UploadFileName", objModel.fileName);
                    cmd.Parameters.AddWithValue("@UploadFileExt", objModel.fileExt);
                    cmd.Parameters.AddWithValue("@UploadFileData", objModel.fileData);

                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@inceptiondate", objModel.inceptiondate);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveGeneratePolicy");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveGeneratePolicy");
            }
            return result;
        }
        public string SaveRenewPolicy(Policy objModel)
        {
            string result = "Error on Renew Policy!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveRenewPolicy]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PId", objModel.PId);
                    cmd.Parameters.AddWithValue("@QId", objModel.QId);
                    cmd.Parameters.AddWithValue("@TypeId", objModel.TypeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@ContactPersonId", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@PolicyHolderId", objModel.PolicyHolderId);
                    cmd.Parameters.AddWithValue("@UnderWriteDate", objModel.UnderWriteDate);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", objModel.IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@ProductId", objModel.ProductId);
                    cmd.Parameters.AddWithValue("@SubjectInsured", objModel.SubjectInsured);
                    cmd.Parameters.AddWithValue("@CostCenter", objModel.CostCenter);
                    cmd.Parameters.AddWithValue("@CommissionRate", objModel.CommissionRate);
                    cmd.Parameters.AddWithValue("@Premium", objModel.Premium);
                    cmd.Parameters.AddWithValue("@CGST", objModel.CGST);
                    cmd.Parameters.AddWithValue("@SGST", objModel.SGST);
                    cmd.Parameters.AddWithValue("@PremiumPayable", objModel.PremiumPayable);
                    cmd.Parameters.AddWithValue("@DiscountProvided", objModel.DiscountProvided);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveRenewPolicy");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveRenewPolicy");
            }
            return result;
        }  
        public List<Policy> GetMasterPolicyDataList(int OfficeId, long CreatedBy, string fDate, string tDate)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetMasterPolicyDataList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@fDate", fDate);
                cmd.Parameters.AddWithValue("@tDate", tDate);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                        ContactNo = Convert.ToString(sdr["MobileNo"].ToString()),
                        ProductRate = Convert.ToString(sdr["ProductRate"].ToString()),
                        ODRate = Convert.ToString(sdr["ODCommissionRate"].ToString()),
                        BrokerageAmount=Convert.ToString(sdr["BrokerageAmount"].ToString()),
                        BrokerAmount=Convert.ToString(sdr["BrokerAmount"].ToString()),
                        GSTTotal=Convert.ToString(sdr["GSTTotal"].ToString()),
                        totalBorkerageamt=Convert.ToString(sdr["totalBorkerageamt"].ToString())
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetMasterPolicyDataList");
            }
            return objList;
        }
        public List<Policy> GetPolicyDetailsForEdit(long PId, long CreatedBy)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetPolicyDetailsForEdit";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@PId", PId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        //ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        //TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        //GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        //ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        //PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        //CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        //InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        //IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        //ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                        DiscountProvided = Convert.ToString(sdr["DiscountProvided"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetPolicyDetailsForEdit");
            }
            return objList;
        }
        public string SaveUpdatePolicy(Policy objModel)
        {
            string result = "Error on Updating Policy!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveUpdatePolicy]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PId", objModel.PId);
                    cmd.Parameters.AddWithValue("@QId", objModel.QId);
                    cmd.Parameters.AddWithValue("@TypeId", objModel.TypeId);
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@ContactPersonId", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@PolicyHolderId", objModel.PolicyHolderId);
                    cmd.Parameters.AddWithValue("@UnderWriteDate", objModel.UnderWriteDate);

                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@VehicleNo", objModel.VehicleNo);
                    cmd.Parameters.AddWithValue("@OnDamage", objModel.OnDamage);
                    cmd.Parameters.AddWithValue("@PersonalAccident", objModel.PersonalAccident);
                    cmd.Parameters.AddWithValue("@ThirdParty", objModel.ThirdParty);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", objModel.IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@ProductId", objModel.ProductId);

                    cmd.Parameters.AddWithValue("@SubjectInsured", objModel.SubjectInsured);
                    cmd.Parameters.AddWithValue("@CostCenter", objModel.CostCenter);
                    cmd.Parameters.AddWithValue("@CommissionRate", objModel.CommissionRate);
                    cmd.Parameters.AddWithValue("@Premium", objModel.Premium);
                    cmd.Parameters.AddWithValue("@CGST", objModel.CGST);
                    cmd.Parameters.AddWithValue("@SGST", objModel.SGST);
                    cmd.Parameters.AddWithValue("@PremiumPayable", objModel.PremiumPayable);
                    cmd.Parameters.AddWithValue("@IDV", objModel.IDV);
                    cmd.Parameters.AddWithValue("@PolicyDate", objModel.PolicyDate);
                    cmd.Parameters.AddWithValue("@ExpiryDate", objModel.ExpiryDate);
                    cmd.Parameters.AddWithValue("@PolicyNo", objModel.PolicyNo);
                    cmd.Parameters.AddWithValue("@PreviousPolicyNo", objModel.PreviousPolicyNo);
                    cmd.Parameters.AddWithValue("@UploadFileName", objModel.fileName);
                    cmd.Parameters.AddWithValue("@UploadFileExt", objModel.fileExt);
                    cmd.Parameters.AddWithValue("@UploadFileData", objModel.fileData);

                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@inceptiondate", objModel.inceptiondate);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveUpdatePolicy");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Query_Repository/SaveUpdatePolicy");
            }
            return result;
        }

        #endregion
    }
}
