﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class MastSkill_Repository
    {

        public DBReturnModel InsertMastSkill(MastSkill_Model objModel)
        {
            string result = "Error on Inserting Skill Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertMastSkill";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SkillID", objModel.SkillID);
                    cmd.Parameters.AddWithValue("@SkillName", objModel.SkillName);
                    cmd.Parameters.AddWithValue("@SkillDescritpion", objModel.SkillDescritpion);
                    cmd.Parameters.AddWithValue("@creationdate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@creationuser", objModel.creationuser);
                    cmd.Parameters.AddWithValue("@isdeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex,Convert.ToInt64( objModel.creationuser), "API", "MastSkill_Repository/InsertMastSkill");
                    objreturn.ReturnMessage = "System Error on Inserting SkillMaster";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastSkill_Repository/InsertMastSkill");
            }
            return objreturn;

        }


        public MastSkill_Model GetOneSkill(long entryid, long creationuser)
        {
            MastSkill_Model objModel = new MastSkill_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectMastSkillListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@entryid", entryid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.entryid = Convert.ToInt64(sdr["entryid"].ToString());
                    objModel.SkillID = Convert.ToInt64(sdr["SkillID"].ToString());
                    objModel.SkillName = sdr["SkillName"].ToString();
                    objModel.SkillDescritpion = sdr["SkillDescritpion"].ToString();
                    objModel.creationuser = sdr["creationuser"].ToString();
                    objModel.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objModel.modifactionuser = sdr["modifactionuser"].ToString();
                    objModel.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "MastSkill_Repository/GetOneSkill");
            }
            return objModel;
        }


        public DBReturnModel UpdateSkillMaster(MastSkill_Model objModel)
        {
            string result = "Error on Updating Skill Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateMastSkill";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", objModel.entryid);
                    cmd.Parameters.AddWithValue("@SkillID", objModel.SkillID);
                    cmd.Parameters.AddWithValue("@SkillName", objModel.SkillName);
                    cmd.Parameters.AddWithValue("@SkillDescritpion", objModel.SkillDescritpion);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);

                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex,Convert.ToInt64 (objModel.creationuser), "API", "MastSkill_Repository/UpdateSkillMaster");
                    objreturn.ReturnMessage = "System Error on Updating SkillMaster";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "MastSkill_Repository/UpdateSkillMaster");
            }
            return objreturn;
        }

        public DBReturnModel DisableSkillMaster(long entryid,long modifactionuser)
        {

            string result = "Error on Disabling Skill Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update MastSkill set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where entryid=@entryid";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Skill Master Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "MastSkill_Repository/DisableSkillMaster");
                    objreturn.ReturnMessage = "System Error on Disabling Skill Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "MastSkill_Repository/DisableSkillMaster");
            }

            return objreturn;
        }


        public DBReturnModel EnableSkillMaster(long entryid, long modifactionuser)
        {
            string result = "Error on Enabling Skill Master!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                  string  sqlstr = "update MastSkill set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where entryid=@entryid";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", false);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = "Skill Master Enabled Successfully!";


                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "MastSkill_Repository/EnableSkillMaster");
                    objreturn.ReturnMessage = "System Error on Enabling SkillMaster!";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "MastSkill_Repository/EnableSkillMaster");
            }

            return objreturn;
        }

        
        public MastSkill_page_Model GetSkillMasterByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            MastSkill_page_Model objModel = new MastSkill_page_Model();
            List<MastSkill_Model> objlist = new List<MastSkill_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "entryid";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectskillMasterByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    MastSkill_Model objtemp = new MastSkill_Model();
                    objtemp.entryid = Convert.ToInt64(sdr["entryid"].ToString());
                    objtemp.SkillID = Convert.ToInt64(sdr["SkillID"].ToString());
                    objtemp.SkillName = sdr["SkillName"].ToString();
                    objtemp.SkillDescritpion = sdr["SkillDescritpion"].ToString();
                    objtemp.creationuser =sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From MastSkill where (Isnull(SkillID,'') like '%'+@search+'%' "
                + " or Isnull(SkillName,'') like '%'+@search+'%') ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.MastSkill_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "MastSkill_Repository/GetSkillMasterByPage");

            }

            return objModel;
        }


        public int GetMaxMastSkillId(long creationuser)
        {
            int objModel = 0;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("select ISNULL((MAX(SkillID)),0)+1 as Id from MastSkill", connection);
                cmd.CommandType = CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel = Convert.ToInt32(sdr["Id"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "MastSkill_Repository/GetMaxMastSkillId");
            }
            return objModel;
        }

    }
}
