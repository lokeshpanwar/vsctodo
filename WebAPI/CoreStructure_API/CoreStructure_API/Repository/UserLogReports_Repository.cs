﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class UserLogReports_Repository
    {

        public UserLogReports_Model GetUserLogReports(DateTime FromDate, DateTime ToDate, long UserId, long CreatedBy)
        {
            UserLogReports_Model objModel = new UserLogReports_Model();
            List<Log_Table_Model> objList = new List<Log_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string qry = "  SELECT e.EmployeeName, c.SubCompanyName, d.Domain_Name,log.SLNO, log.OperationType, log.Operation_Performed, log.OperationON, log.Ramarks, log.IP_Address";
                qry += "  FROM  Employee_table e INNER JOIN  Log_Table log ON e.SLNO = log.user_id INNER JOIN  Domain_table d ON e.Domain_ID = d.SLNO INNER JOIN Sub_Company_table c ON e.Sub_Company = c.SLNO";
                qry += " Where e.slno=@UserID and log.OperationON BETWEEN Cast(@FromDate as Date) and Cast(@ToDate as Date)";
               
                SqlCommand cmd = new SqlCommand(qry, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@UserID", UserId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Log_Table_Model tempobj = new Log_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                  
                    tempobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.OperationType = sdr["OperationType"].ToString();
                    tempobj.Operation_Performed = sdr["Operation_Performed"].ToString();
                    tempobj.OperationON = Convert.ToDateTime(sdr["OperationON"].ToString());
                    tempobj.Remarks = sdr["Ramarks"].ToString();
                    tempobj.IP_Address = sdr["IP_Address"].ToString();
                    
                    objList.Add(tempobj);
                }
                sdr.Close();


                connection.Close();
                objModel.Log_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "UserLogReports_Repository/GetUserLogReports");
            }

            return objModel;
        }

    }
}
