﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Domain_Repository
    {

        public DBReturnModel InsertDomain(Domain_table_Model objModel)
        {
            string result = "Error on Inserting Domain Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertDomain";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Domain_Name", objModel.Domain_Name);
                    cmd.Parameters.AddWithValue("@Domain_Description", objModel.Domain_Description);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CenterCode", objModel.CenterCode);

                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Domain_Repository/InsertDomain");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Domain_Repository/InsertDomain");
            }

            return objreturn;
        }




        public List<Domain_table_Model> GetDomainList(long CreatedBy)
        {
            List<Domain_table_Model> objModel = new List<Domain_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(SLNO,0) as SLNO, isnull(Domain_Name,'') as Domain_Name, isnull(Domain_Description,'') as Domain_Description,"
                    + " isnull(CreatedBy,0) as CreatedBy, CreatedOn, isnull(CenterCode, '') as CenterCode, isnull(IsDeleted,'') as IsDeleted From Domain_table ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Domain_table_Model tempobj = new Domain_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.Domain_Description = sdr["Domain_Description"].ToString();
                    tempobj.Domain_Description = sdr["Domain_Description"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.CenterCode = sdr["CenterCode"].ToString();
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Domain_Repository/GetDomainList");
            }

            return objModel;

        }



        public Domain_table_Model GetOneDomain(long SLNO, long CreatedBy)
        {
            Domain_table_Model objModel = new Domain_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(SLNO,0) as SLNO, isnull(Domain_Name,'') as Domain_Name, isnull(Domain_Description,'') as Domain_Description,"
                    + " isnull(CreatedBy,0) as CreatedBy, CreatedOn, isnull(CenterCode, '') as CenterCode From Domain_table where SLNO=@SLNO ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Domain_Name = sdr["Domain_Name"].ToString();
                    objModel.Domain_Description = sdr["Domain_Description"].ToString();
                    objModel.Domain_Description = sdr["Domain_Description"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.CenterCode = sdr["CenterCode"].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Domain_Repository/GetOneDomain");
            }

            return objModel;

        }



        public DBReturnModel UpdateDomain(Domain_table_Model objModel)
        {
            string result = "Error on Updating Domain Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_UpdateDomain";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Domain_Name", objModel.Domain_Name);
                    cmd.Parameters.AddWithValue("@Domain_Description", objModel.Domain_Description);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CenterCode", objModel.CenterCode);
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);

                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Domain_Repository/UpdateDomain");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Domain_Repository/UpdateDomain");
            }

            return objreturn;
        }


        public string DisableDomain(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Domain!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Domain_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Domain Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/DisableDomain");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/DisableDomain");
            }

            return result;
        }




        public string EnableDomain(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Domain!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Domain_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Domain Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/EnableDomain");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/EnableDomain");
            }

            return result;
        }


        public Domain_table_page_Model GetDomainListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Domain_table_page_Model objModel = new Domain_table_page_Model();
            List<Domain_table_Model> objList = new List<Domain_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDomainByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Domain_table_Model tempobj = new Domain_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.Domain_Description = sdr["Domain_Description"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.CenterCode = sdr["CenterCode"].ToString();
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Domain_table where (Isnull(Domain_Name,'') like '%'+@search+'%' or "
                        + " Isnull(Domain_Description,'') like '%'+@search+'%' or Isnull(CenterCode,'') like '%'+@search+'%')";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Domain_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Domain_Repository/GetDomainListByPage");
            }

            return objModel;

        }






        public DBReturnModel DeleteDomain(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Domain!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteDomain";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var ret = new SqlParameter();
                    ret.ParameterName = "ret";
                    ret.SqlDbType = SqlDbType.VarChar;
                    ret.Size = 500;
                    ret.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(ret);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    //cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = ret.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/DeleteDomain");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Domain_Repository/DeleteDomain");
            }

            return objreturn;
        }




        public string InsertDomainMapping(Domain_Mapping_Display objModel)
        {
            string result = "Error on Inserting Domain Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDomainMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Parent_CompId", objModel.Parent_CompId);
                    cmd.Parameters.AddWithValue("Maped_CompID", objModel.Maped_CompId);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("Maped_By", objModel.Maped_By);
                    cmd.Parameters.AddWithValue("Maped_On", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Domain_Mapping_Type";
                    dataTable.Columns.Add("Maped_DomainID", typeof(long));
                    foreach (var dta in objModel.Domain_table_Model_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.SLNO); // Id of '1' is valid for the Person table
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DomainMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 50;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();

                    result = returnCode.Value.ToString();

                    connection.Close();


                    //result = "Domain Mapping Inserted Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.Maped_By, "API", "Domain_Repository/InsertDomainMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.Maped_By, "API", "Domain_Repository/InsertDomainMapping");
            }

            return result;
        }


        public List<Domain_table_Model_for_mapping> GetDomainListForMapping(long CreatedBy, long SubCompId)
        {
            List<Domain_table_Model_for_mapping> objModel = new List<Domain_table_Model_for_mapping>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(D.SLNO,0) as SLNO, isnull(Domain_Name,'') as Domain_Name, "
                + " Isnull(M.Maped_CompID, 0) as Maped_CompID From Domain_table D Left Outer Join Domain_Mapping_table M "
                + " on D.SLNO = M.Maped_DomainID ";
                sqlstr = sqlstr + " and Maped_CompID = @Maped_CompID where D.IsDeleted!=@IsDeleted ";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.Parameters.AddWithValue("@Maped_CompID", SubCompId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Domain_table_Model_for_mapping tempobj = new Domain_table_Model_for_mapping();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    if (Convert.ToInt32(sdr["Maped_CompID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Domain_Repository/GetDomainListForMapping");
            }

            return objModel;

        }


    }
}
