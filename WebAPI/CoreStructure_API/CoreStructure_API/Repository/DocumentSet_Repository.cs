﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocumentSet_Repository
    {


        public Document_Set_master_Model GetDocumentClassTypeFileList(long SubCompId, long DomainId, long CreatedBy)
        {
            Document_Set_master_Model objModel = new Document_Set_master_Model();

            List<Document_Set_Doc_Class_Model> objDocClassList = new List<Document_Set_Doc_Class_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, SubCompanyID, ClassDomain,DocumentClassName From DocumentClass_table where "
                    + " Isnull(IsDeleted,'')!=@IsDeleted and SubCompanyID = @SubCompanyID and ClassDomain = @ClassDomain";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);

                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Doc_Class_Model tempobj = new Document_Set_Doc_Class_Model();
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    objDocClassList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Doc_Class_Model_List = objDocClassList;



                List<Document_Set_Doc_Types_Model> objDocTypeList = new List<Document_Set_Doc_Types_Model>();
                sqlstr = "Select DT.SLNO, DT.DocumentType_Name, DTMap.Document_ClassID  From DocumentType_DocClass_Mapping_Table DTMap, "
                + " DocumentType_Table DT Where DTMap.DocumentType_ID = DT.SLNO and DTMap.IsDeleted != @IsDeleted and DT.IsDeleted != @IsDeleted "
                + " and DTMap.Document_ClassID in (Select SLNO From DocumentClass_table where SubCompanyID = @SubCompID and ClassDomain = @DomainID)";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.Parameters.AddWithValue("@SubCompID", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Doc_Types_Model tempobj = new Document_Set_Doc_Types_Model();
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.DocumentTypeName = sdr["DocumentType_Name"].ToString();
                    tempobj.DocumentType_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    objDocTypeList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Doc_Types_Model_List = objDocTypeList;




                List<Document_Set_Files> objDocList = new List<Document_Set_Files>();
                sqlstr = "Select DocumentID,Doc_Class_ID,FileName From DocUpload_Master where Sub_CompID=@SubCompanyID and DomainID=@DomainID";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Files tempobj = new Document_Set_Files();
                    tempobj.Doc_ID = Convert.ToInt64(sdr["DocumentID"].ToString());
                    tempobj.DocumentName = sdr["FileName"].ToString();
                    objDocList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Files_Class_List = objDocList;


                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSet_Repository/GetDocumentClassTypeFileList");
            }













            //List<Document_Set_Doc_Class_Model> objDocClassList = new List<Document_Set_Doc_Class_Model>();
            //try
            //{
            //    var connection = SqlHelper.Connection();
            //    connection.Open();

            //    string sqlstr = "Select SLNO, SubCompanyID, ClassDomain,DocumentClassName From DocumentClass_table where "
            //        + " Isnull(IsDeleted,'')!=@IsDeleted and SubCompanyID = @SubCompanyID and ClassDomain = @ClassDomain";
            //    SqlCommand cmd = new SqlCommand(sqlstr, connection);
            //    cmd.Parameters.AddWithValue("@SubCompanyID", SubCompId);
            //    cmd.Parameters.AddWithValue("@ClassDomain", DomainId);
            //    cmd.Parameters.AddWithValue("@IsDeleted", true);

            //    cmd.CommandType = System.Data.CommandType.Text;
            //    SqlDataReader sdr = cmd.ExecuteReader();
            //    while (sdr.Read())
            //    {
            //        Document_Set_Doc_Class_Model tempobj = new Document_Set_Doc_Class_Model();
            //        tempobj.Document_ClassID = Convert.ToInt64(sdr["SLNO"].ToString());
            //        tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
            //        objDocClassList.Add(tempobj);
            //    }
            //    sdr.Close();
            //    objModel.Document_Set_Doc_Class_Model_List = objDocClassList;


            //    for(int i=0; i< objModel.Document_Set_Doc_Class_Model_List.Count; i++)
            //    {
            //        List<Document_Set_Doc_Types_Model> objDocTypeList = new List<Document_Set_Doc_Types_Model>();
            //        sqlstr = "Select DT.SLNO, DT.DocumentType_Name, DTMap.Document_ClassID  From DocumentType_DocClass_Mapping_Table DTMap, "
            //        + " DocumentType_Table DT Where DTMap.DocumentType_ID = DT.SLNO and DTMap.IsDeleted != @IsDeleted and DT.IsDeleted != @IsDeleted "
            //        + " and DTMap.Document_ClassID = @ClassID";
            //        cmd.Parameters.Clear();
            //        cmd.CommandText = sqlstr;
            //        cmd.Connection = connection;
            //        cmd.Parameters.AddWithValue("@IsDeleted", true);
            //        cmd.Parameters.AddWithValue("@ClassID", objModel.Document_Set_Doc_Class_Model_List[i].Document_ClassID);
            //        cmd.CommandType = System.Data.CommandType.Text;
            //        sdr = cmd.ExecuteReader();
            //        while (sdr.Read())
            //        {
            //            Document_Set_Doc_Types_Model tempobj = new Document_Set_Doc_Types_Model();
            //            tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
            //            tempobj.DocumentTypeName = sdr["DocumentType_Name"].ToString();
            //            tempobj.DocumentType_ID = Convert.ToInt64(sdr["SLNO"].ToString());
            //            objDocTypeList.Add(tempobj);
            //        }
            //        sdr.Close();
            //        objModel.Document_Set_Doc_Class_Model_List[i].Document_Set_Doc_Types_Model_List = objDocTypeList;


            //        List<Document_Set_Files> objDocList = new List<Document_Set_Files>();
            //        sqlstr = "Select DocumentID,Doc_Class_ID,FileName From DocUpload_Master where Doc_Class_ID=@ClassId and DocType_ID=0";
            //        cmd.Parameters.Clear();
            //        cmd.CommandText = sqlstr;
            //        cmd.Connection = connection;
            //        cmd.Parameters.AddWithValue("@ClassID", objModel.Document_Set_Doc_Class_Model_List[i].Document_ClassID);
            //        cmd.CommandType = System.Data.CommandType.Text;
            //        sdr = cmd.ExecuteReader();
            //        while (sdr.Read())
            //        {
            //            Document_Set_Files tempobj = new Document_Set_Files();
            //            tempobj.Doc_ID = Convert.ToInt64(sdr["DocumentID"].ToString());
            //            tempobj.DocumentName = sdr["FileName"].ToString();
            //            objDocList.Add(tempobj);
            //        }
            //        sdr.Close();
            //        objModel.Document_Set_Doc_Class_Model_List[i].Document_Set_Files_Class_List = objDocList;
            //    }


            //    for (int i = 0; i < objModel.Document_Set_Doc_Class_Model_List.Count; i++)
            //    {
            //        for (int j = 0; j < objModel.Document_Set_Doc_Class_Model_List[i].Document_Set_Doc_Types_Model_List.Count(); j++)
            //        {
            //            List<Document_Set_Files> objDocList = new List<Document_Set_Files>();
            //            sqlstr = "Select DocumentID,Doc_Class_ID,FileName From DocUpload_Master where Doc_Class_ID=@ClassId and DocType_ID=@DocTypeId";
            //            cmd.Parameters.Clear();
            //            cmd.CommandText = sqlstr;
            //            cmd.Connection = connection;
            //            cmd.Parameters.AddWithValue("@ClassID", objModel.Document_Set_Doc_Class_Model_List[i].Document_ClassID);
            //            cmd.Parameters.AddWithValue("@DocTypeId", objModel.Document_Set_Doc_Class_Model_List[i].Document_Set_Doc_Types_Model_List[j].DocumentType_ID);
            //            cmd.CommandType = System.Data.CommandType.Text;
            //            sdr = cmd.ExecuteReader();
            //            while (sdr.Read())
            //            {
            //                Document_Set_Files tempobj = new Document_Set_Files();
            //                tempobj.Doc_ID = Convert.ToInt64(sdr["DocumentID"].ToString());
            //                tempobj.DocumentName = sdr["FileName"].ToString();
            //                objDocList.Add(tempobj);
            //            }
            //            sdr.Close();

            //            objModel.Document_Set_Doc_Class_Model_List[i].Document_Set_Doc_Types_Model_List[j].Document_Set_Files_Type_List = objDocList;
            //        }
            //    }

            //    connection.Close();
            //}
            //catch(Exception ex)
            //{
            //    ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSet_Repository/GetDocumentClassTypeFileList");
            //}

            return objModel;
        }



        public Document_Set_master_page_Model GetDocumentSetListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Document_Set_master_page_Model objModel = new Document_Set_master_page_Model();
            List<Document_Set_master_Model> objList = new List<Document_Set_master_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentSetByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_master_Model tempobj = new Document_Set_master_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DocumentSetName = sdr["DocumentSetName"].ToString();
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());

                    tempobj.IsExpire = Convert.ToBoolean(sdr["IsExpire"].ToString());
                    //tempobj.IsExpireDate = Convert.ToDateTime(sdr["IsExpireDate"].ToString());

                    if (tempobj.IsExpire == true)
                    {
                        tempobj.IsExpireDate = Convert.ToDateTime(sdr["IsExpireDate"].ToString());
                    }

                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());

                    tempobj.SubCompName = sdr["SubCompanyName"].ToString();
                    tempobj.DomainName = sdr["DomainName"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Document_Set_master  where (Isnull(DocumentSetName,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Document_Set_master_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSet_Repository/GetDocumentSetListByPage");
            }

            return objModel;

        }




        public DBReturnModel InsertDocumentSet(Document_Set_master_Model objModel)
        {
            string result = "Error on Inserting Document Set!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentSet";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Document_Set_Doc_Class_Type";
                    dataTable.Columns.Add("Document_ClassID", typeof(long));
                    foreach (var dta in objModel.Document_Set_Doc_Class_Model_List)
                    {
                        if (dta != null)
                        {
                            if (dta.IsSelected == true)
                            {
                                dataTable.Rows.Add(dta.Document_ClassID);
                            }
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DocumentSetDocClassType", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);


                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.Document_Set_Doc_Types_Type";
                    dataTable1.Columns.Add("Document_ClassID", typeof(long));
                    dataTable1.Columns.Add("DocumentType_ID", typeof(long));
                    foreach (var dta in objModel.Document_Set_Doc_Types_Model_List)
                    {
                        if (dta != null)
                        {
                            if (dta.IsSelected == true)
                            {
                                dataTable1.Rows.Add(dta.Document_ClassID, dta.DocumentType_ID);
                            }
                        }
                    }
                    SqlParameter parameter1 = new SqlParameter("DocumentSetDocTypesType", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);


                    var dataTable2 = new DataTable();
                    dataTable2.TableName = "dbo.Document_Set_Files_Type";
                    dataTable2.Columns.Add("Doc_ID", typeof(long));
                    foreach (var dta in objModel.Document_Set_Files_Class_List)
                    {
                        if (dta != null)
                        {
                            if (dta.IsSelected == true)
                            {
                                dataTable2.Rows.Add(dta.Doc_ID);
                            }
                        }
                    }
                    SqlParameter parameter2 = new SqlParameter("DocumentSetFilesType", SqlDbType.Structured);
                    parameter2.TypeName = dataTable2.TableName;
                    parameter2.Value = dataTable2;
                    cmd.Parameters.Add(parameter2);

                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DomainID", objModel.DomainID);
                    cmd.Parameters.AddWithValue("@DocumentSetName", objModel.DocumentSetName);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@IsExpire", objModel.IsExpire);
                    cmd.Parameters.AddWithValue("@IsExpireDate", objModel.IsExpireDate);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSet_Repository/InsertDocumentSet");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSet_Repository/InsertDocumentSet");
            }

            return objReturn;
        }




        public DBReturnModel DisableDocumentSet(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturnNodel = new DBReturnModel();
            string result = "Error on Disabling Document Set!";
            objReturnNodel.ReturnStatus = "ERROR";
            objReturnNodel.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Document_Set_master set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@DeletedBy,ModifiedOn=@ModifiedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturnNodel.ReturnMessage = "Document Set Disabled Successfully!";
                    objReturnNodel.ReturnStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSet_Repository/DisableDocumentSet");
                    objReturnNodel.ReturnMessage = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSet_Repository/DisableDocumentSet");
            }

            return objReturnNodel;
        }

        public DBReturnModel EnableDocumentSet(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturnNodel = new DBReturnModel();
            string result = "Error on Enabling Document Set!";
            objReturnNodel.ReturnStatus = "ERROR";
            objReturnNodel.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Document_Set_master set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@DeletedBy,ModifiedOn=@ModifiedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturnNodel.ReturnMessage = "Document Set Enabled Successfully!";
                    objReturnNodel.ReturnStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSet_Repository/EnableDocumentSet");
                    objReturnNodel.ReturnMessage = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSet_Repository/EnableDocumentSet");
            }

            return objReturnNodel;
        }




        public Document_Set_master_Model GetOneDocumentSet(long DocSetId, long CreatedBy)
        {
            Document_Set_master_Model objModel = new Document_Set_master_Model();

            List<Document_Set_Doc_Class_Model> objDocClassList = new List<Document_Set_Doc_Class_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, SubCompanyID, ClassDomain,DocumentClassName, Isnull(Set_ID,0) as Set_ID From DocumentClass_table DocClass "
                + " LEFT OUTER JOIN Document_Set_Doc_Class DocSet on DocClass.SLNO = DocSet.Document_ClassID where Isnull(Set_ID,0)=@Set_ID ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Set_ID", DocSetId);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Doc_Class_Model tempobj = new Document_Set_Doc_Class_Model();
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    if (Convert.ToInt64(sdr["Set_ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objDocClassList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Doc_Class_Model_List = objDocClassList;



                List<Document_Set_Doc_Types_Model> objDocTypeList = new List<Document_Set_Doc_Types_Model>();
                sqlstr = " Select SLNO, DocumentType_Name, Isnull(Set_ID,0) as Set_ID,Isnull(Document_ClassID,0) as Document_ClassID  "
                + "  From DocumentType_Table DT LEFT OUTER JOIN Document_Set_Doc_Types DocSet on DT.SLNO = DocSet.DocumentType_ID "
                + " where Isnull(Set_ID,0)= @Set_ID";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Set_ID", DocSetId);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Doc_Types_Model tempobj = new Document_Set_Doc_Types_Model();
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.DocumentTypeName = sdr["DocumentType_Name"].ToString();
                    tempobj.DocumentType_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    if (Convert.ToInt64(sdr["Set_ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objDocTypeList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Doc_Types_Model_List = objDocTypeList;




                List<Document_Set_Files> objDocList = new List<Document_Set_Files>();
                sqlstr = "Select DocumentID,Doc_Class_ID,FileName,Isnull(Set_ID,0) as Set_ID From DocUpload_Master DUM "
                    + " Left Outer Join Document_Set_Files DocSet on DUM.DocumentID = DocSet.Doc_ID where Set_ID = @Set_ID";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Set_ID", DocSetId);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Document_Set_Files tempobj = new Document_Set_Files();
                    tempobj.Doc_ID = Convert.ToInt64(sdr["DocumentID"].ToString());
                    tempobj.DocumentName = sdr["FileName"].ToString();
                    if (Convert.ToInt64(sdr["Set_ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objDocList.Add(tempobj);
                }
                sdr.Close();
                objModel.Document_Set_Files_Class_List = objDocList;



                sqlstr = "Select SLNO, Sub_CompID, DomainID, DocumentSetName, CreatedBy, CreatedOn, "
                + " Isnull(IsExpire, '') as IsExpire,IsExpireDate From Document_Set_master "
                + " where SLNO = @SLNO";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@SLNO", DocSetId);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    objModel.DocumentSetName = sdr["DocumentSetName"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsExpire = Convert.ToBoolean(sdr["IsExpire"].ToString());
                    //objModel.IsExpireDate = Convert.ToDateTime(sdr["IsExpireDate"].ToString());
                    if (objModel.IsExpire == true)
                    {
                        objModel.IsExpireDate = Convert.ToDateTime(sdr["IsExpireDate"].ToString());
                    }
                }
                sdr.Close();


                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSet_Repository/GetOneDocumentSet");
            }



            return objModel;
        }




        public List<DocSetPermission> GetDoucmentSetPermissionList(long SubCompId, long DomainId, long DocSetId, long CreatedBy)
        {
            List<DocSetPermission> objModel = new List<DocSetPermission>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Isnull(DSM.SLNO,0) as DocSetId, isnull(DocumentSetName,'') as DocumentSetName,isnull(EMP.SLNO,0) as User_ID, "
                + " Isnull(Set_ID, 0) as Set_ID,Isnull(view_right, '') as view_right, Isnull(print_right, '') as print_right,"
                + " isnull(email_right, '') as email_right, isnull(checkin_right, '') as checkin_right, isnull(checkout_right, '') as checkout_right, "
                + " isnull(download_right, '') as download_right, isnull(upload_right, '') as upload_right, isnull(selectall_right, '') as selectall_right, "
                + " EmployeeName from Document_Set_master DSM Left Join Document_Set_permission DSP on DSM.SLNO = DSP.Set_ID "
                + " Right Outer Join Employee_table EMP on DSP.User_ID = EMP.SLNO and Isnull(DSM.SLNO,0)=@Set_ID where Isnull(DSM.IsDeleted,'')!= @IsDeleted and "
                + " Isnull(EMP.IsDeleted, '') != @IsDeleted and EMP.Sub_Company = @SubCompany and EMP.Domain_ID = @DomainID ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.Parameters.AddWithValue("@SubCompany", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                cmd.Parameters.AddWithValue("@Set_ID", DocSetId);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocSetPermission tempobj = new DocSetPermission();
                    tempobj.Set_ID= Convert.ToInt64(sdr["DocSetId"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.UserName = sdr["EmployeeName"].ToString();
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());

                    objModel.Add(tempobj);
                }
                sdr.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSet_Repository/GetDoucmentSetPermissionList");
            }

            return objModel;
        }






        public DBReturnModel InsertDocumentSetPermission(Document_Set_permission_Model objModel)
        {
            string result = "Error on Inserting Document Set Permission!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentSetPermission";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Document_Set_permission_Type";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    dataTable.Columns.Add("Set_ID", typeof(long));
                    dataTable.Columns.Add("view_right", typeof(bool));
                    dataTable.Columns.Add("print_right", typeof(bool));
                    dataTable.Columns.Add("email_right", typeof(bool));
                    dataTable.Columns.Add("checkin_right", typeof(bool));
                    dataTable.Columns.Add("checkout_right", typeof(bool));
                    dataTable.Columns.Add("download_right", typeof(bool));
                    dataTable.Columns.Add("upload_right", typeof(bool));
                    dataTable.Columns.Add("selectall_right", typeof(bool));
                    foreach (var dta in objModel.DocSetPermission_List)
                    {
                        if (dta != null)
                        {
                            dataTable.Rows.Add(dta.User_ID,dta.Set_ID, dta.view_right, dta.print_right, dta.email_right, dta.checkin_right, dta.checkout_right,
                                dta.download_right, dta.upload_right, dta.selectall_right);
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DocumentSetpermissionType", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);



                    cmd.Parameters.AddWithValue("@Set_ID", objModel.Set_ID);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSet_Repository/InsertDocumentSetPermission");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSet_Repository/InsertDocumentSetPermission");
            }

            return objReturn;
        }




        public List<DocSetUserDashboard> GetDocumentSetListForDashboard(long CreatedBy)
        {
            List<DocSetUserDashboard> objReturn = new List<DocSetUserDashboard>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "SELECT DISTINCT ds.SLNO, ds.DocumentSetName, e.EmpId, ds.DomainID, ds.Sub_CompID "
                + " FROM dbo.Document_Set_master AS ds INNER JOIN dbo.Document_Set_permission AS p ON ds.SLNO = p.Set_ID "
                + " INNER JOIN dbo.Employee_table AS e ON p.User_ID = e.SLNO WHERE e.SLNO = @id";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@id", CreatedBy);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocSetUserDashboard tempobj = new DocSetUserDashboard();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentSetName = sdr["DocumentSetName"].ToString();
                    tempobj.EmpId = sdr["EmpId"].ToString();
                    tempobj.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objReturn.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocClassDocTypeMapping_Repository/GetDocumentSetListForDashboard");
            }

            return objReturn;
        }




    }
}
