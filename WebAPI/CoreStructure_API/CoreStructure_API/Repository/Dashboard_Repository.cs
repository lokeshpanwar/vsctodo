﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;
using System.Collections;
using System.IO;

namespace CoreStructure_API.Repository
{
    public class Dashboard_Repository
    {

        DocumentClass_Repository objDocClassRepo = new DocumentClass_Repository();


        public List<DashBPMFormsTrigger_Model> GetBPMFormsTriggerList(long CreatedBy,long DomainId,long SubCompanyId)
        {
            List<DashBPMFormsTrigger_Model> objList = new List<DashBPMFormsTrigger_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";
               

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype.ToUpper() == "VENDOR" || usertype.ToUpper() == "CUSTOMER")
                {
                    peopleType = "External";
                }
                //get Work flow records
                cmd.Parameters.Clear();

                sqlstr = "CB_SP_SelectBPMFormsTriggerList";
                cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@activeStatus", true);
                //cmd.Parameters.AddWithValue("@stageOrderID", 1);
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                cmd.Parameters.AddWithValue("@DomainId", DomainId);
                cmd.Parameters.AddWithValue("@SubCompanyId", SubCompanyId);

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMFormsTrigger_Model tempobj = new DashBPMFormsTrigger_Model();
                    tempobj.SLNO = Convert.ToInt32(sdr["SLNO"].ToString());
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowId"].ToString());
                    tempobj.Days = Convert.ToInt64(sdr["stageDays"].ToString());
                    tempobj.Description = sdr["StageDescription"].ToString();
                    tempobj.Name = sdr["StageName"].ToString();
                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.stageOrderID = sdr["stageOrderID"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMFormsTriggerList");
            }
            return objList;
        }


        public List<DashBPMWipOrClosedTab_Model> GetBPMOpenTabList(long CreatedBy)
        {
           
            List<DashBPMWipOrClosedTab_Model> objlist2 = new List<DashBPMWipOrClosedTab_Model>();

           
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }
                ////get Work flow records
                //cmd.Parameters.Clear();

                //sqlstr = "CB_SP_SelectBPMOpenTabHeader";
                //cmd = new SqlCommand(sqlstr, connection);

                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Connection = connection;
                //cmd.Parameters.AddWithValue("@peopleType", peopleType);
                //cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                //cmd.Parameters.AddWithValue("@complitionStatus", false);
                //cmd.Parameters.AddWithValue("@isClosed", false);
                //cmd.Parameters.AddWithValue("@isOpen", true);

                //sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    DashBPMOpenTab_Model tempobj = new DashBPMOpenTab_Model();
                //    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                //    tempobj.workFlowName = sdr["workFlowName"].ToString();
                //    tempobj.TotalDays = Convert.ToInt32(sdr["TotalDays"].ToString());
                //    tempobj.priority = sdr["priority"].ToString();
                //    tempobj.description = sdr["description"].ToString();

                //    objlist.Add(tempobj);
                //    //sdr.Close();
                //}
                //sdr.Close();
                ////get workflow_thread_progress Records

                
                    cmd.Parameters.Clear();
                    sqlstr = "CB_SP_SelectBPMOpenTabDetails";
                    SqlCommand cmd1 = new SqlCommand(sqlstr, connection);

                    cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd1.Connection = connection;
                    cmd1.Parameters.AddWithValue("@peopleType", peopleType);
                    cmd1.Parameters.AddWithValue("@peopleID", CreatedBy);                   

                    cmd1.Parameters.AddWithValue("@complitionStatus", false);
                    cmd1.Parameters.AddWithValue("@isClosed", false);
                    cmd1.Parameters.AddWithValue("@isOpen", true);

                    SqlDataReader sdr1 = cmd1.ExecuteReader();
                    objlist2 = new List<DashBPMWipOrClosedTab_Model>();
                    while (sdr1.Read())
                    {
                        DashBPMWipOrClosedTab_Model tempobj2 = new DashBPMWipOrClosedTab_Model();
                        tempobj2.BPMID = Convert.ToInt64(sdr1["workflowid"].ToString());
                        tempobj2.ThrdRef = sdr1["ThrdRef"].ToString();
                        tempobj2.BPM_Name = sdr1["WorkflowName"].ToString();
                        tempobj2.threadID = Convert.ToInt64(sdr1["threadID"].ToString());
                        tempobj2.stageName = sdr1["stageName"].ToString();
                        tempobj2.stageDescription = sdr1["stageDescription"].ToString();
                        tempobj2.stageDays = Convert.ToInt64(sdr1["stageDays"].ToString());
                        tempobj2.RASCI = sdr1["RASCI"].ToString();
                        tempobj2.AdditionalSearchableColumn = "2,3,4,6".Contains(sdr1["workflowid"].ToString()) ? GetAdditionalSearchableValue(sdr1["workflowid"].ToString(), sdr1["threadID"].ToString()) : "";
                        tempobj2.stageOrderId = Convert.ToInt64(sdr1["stageOrderID"].ToString());

                        objlist2.Add(tempobj2);


                    }
                    sdr1.Close();
                  
     
              




                connection.Close();

              
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMOpenTabList");
            }

            return objlist2;
        }

        public string GetAdditionalSearchableValue(string strBPMID, string strThreadId)
        {
            string Values = "";
            var connection = SqlHelper.Connection();
            connection.Open();
                using (SqlCommand cmd = new SqlCommand("sp_BPMAdvanceSearch", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BPMID", strBPMID);
                    cmd.Parameters.AddWithValue("@ThreadId", strThreadId);
                    SqlDataReader rd = cmd.ExecuteReader();

                    if (rd.HasRows)
                    {
                        while (rd.Read())
                            Values = rd["SAPPO"].ToString();
                    }
                }
            connection.Close();
            

            return Values;
        }
        public List<DashBPMWipOrClosedTab_Model> GetBPMWIPTabList(long CreatedBy)
        {
            List<DashBPMWipOrClosedTab_Model> objList = new List<DashBPMWipOrClosedTab_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }

                cmd.Parameters.Clear();//Get WIP tabl List
                sqlstr = "CB_SP_SelectBPMWIPTabList";
                cmd = new SqlCommand(sqlstr, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                cmd.Parameters.AddWithValue("@complitionStatus", false);
                cmd.Parameters.AddWithValue("@isClosed", false);
                cmd.Parameters.AddWithValue("@isOpen", false);
                cmd.Parameters.AddWithValue("@isWip", true);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMWipOrClosedTab_Model tempobj = new DashBPMWipOrClosedTab_Model();
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.BPM_Name = sdr["WorkFlowName"].ToString();
                    tempobj.ThrdRef = sdr["ThrdRef"].ToString();
                    tempobj.threadID = Convert.ToInt64(sdr["threadID"].ToString());
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageDays = Convert.ToInt64(sdr["stageDays"].ToString());
                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.stageOrderId = Convert.ToInt64(sdr["stageOrderId"].ToString());
                    tempobj.AdditionalSearchableColumn = "2,3,4,6".Contains(sdr["workflowid"].ToString()) ? GetAdditionalSearchableValue(sdr["workflowid"].ToString(), sdr["threadID"].ToString()) : "";
                    objList.Add(tempobj);
                }
                sdr.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMWIPTabList");
            }
            return objList;
        }

        public List<DashBPMWipOrClosedTab_Model> GetBPMClosedTabList(long CreatedBy)
        {
            List<DashBPMWipOrClosedTab_Model> objList = new List<DashBPMWipOrClosedTab_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }


                //Get Closed tabl List

                cmd.Parameters.Clear();
                sqlstr = "CB_SP_SelectBPMClosedTabList";
                cmd = new SqlCommand(sqlstr, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                cmd.Parameters.AddWithValue("@complitionStatus", true);
                cmd.Parameters.AddWithValue("@isClosed", true);
                //cmd.Parameters.AddWithValue("@isOpen", true);

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMWipOrClosedTab_Model tempobj = new DashBPMWipOrClosedTab_Model();
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.BPM_Name = sdr["WorkFlowName"].ToString();
                    tempobj.ThrdRef = sdr["ThrdRef"].ToString();
                    tempobj.threadID = Convert.ToInt64(sdr["threadID"].ToString());
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageDays = Convert.ToInt64(sdr["stageDays"].ToString());
                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.stageOrderId = Convert.ToInt64(sdr["stageOrderId"].ToString());
                    tempobj.AdditionalSearchableColumn = "2,3,4,6".Contains(sdr["workflowid"].ToString()) ? GetAdditionalSearchableValue(sdr["workflowid"].ToString(), sdr["threadID"].ToString()) : "";
                    objList.Add(tempobj);
                }
                sdr.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMClosedTabList");
            }
            return objList;
        }

        public List<DashBPMExceptionTab_Model> GetBPMExceptionTabList(long CreatedBy)
        {
            List<DashBPMExceptionTab_Model> objList = new List<DashBPMExceptionTab_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }


                //Get Exception tabl List

                cmd.Parameters.Clear();
                sqlstr = "CB_SP_SelectBPMExceptionTabList";
                cmd = new SqlCommand(sqlstr, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMExceptionTab_Model tempobj = new DashBPMExceptionTab_Model();
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.AdditionalSearchableColumn = "2,3,4,6".Contains(sdr["workflowid"].ToString()) ? GetAdditionalSearchableValue(sdr["workflowid"].ToString(), sdr["threadID"].ToString()) : "";
                    tempobj.WorkflowName = sdr["workflowname"].ToString();
                    tempobj.thrdID = Convert.ToInt64(sdr["threadID"].ToString());
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageOrderId = Convert.ToInt64(sdr["threadID"].ToString());
                    string stageDays = sdr["stageDays"].ToString();
                    if (stageDays != null)
                    {
                        tempobj.stageDays = Convert.ToInt64(sdr["stageDays"].ToString());
                    }
                    else
                    {
                        tempobj.stageDays = 0;
                    }
                    string raisedBy = sdr["raisedBy"].ToString();
                    if (raisedBy != "")
                    {
                        tempobj.raisedBy = Convert.ToInt64(sdr["raisedBy"].ToString());
                    }
                    else
                    {
                        tempobj.raisedBy = 0;
                    }

                    tempobj.status = sdr["status"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();
                connection.Close();



            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMExceptionTabList");
            }
            return objList;
        }

        //insert into workflow_thread_progress_table from workflow_stage_table Start



        //public DBReturnModel InsertProgressTableFromStageTbl(long workFlowID, long createdBy)
        //{
        //    string result = "Error on Inserting";
        //    DBReturnModel objReturn = new DBReturnModel();
        //    objReturn.ReturnMessage = result;
        //    objReturn.ReturnStatus = "ERROR";
        //    try
        //    {
        //        var connection = SqlHelper.Connection();
        //        connection.Open();
        //        SqlCommand cmd = connection.CreateCommand();
        //        SqlTransaction transaction;
        //        transaction = connection.BeginTransaction();
        //        cmd.Transaction = transaction;
        //        cmd.Connection = connection;
        //        try
        //        {
        //            string sqlstr = "CB_SP_InsertProgressTblFromStageTbl";
        //            cmd.Parameters.Clear();
        //            cmd.CommandText = sqlstr;
        //            cmd.Connection = connection;
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@workFlowID", workFlowID);
        //            cmd.Parameters.AddWithValue("@threadStatus", true);
        //            cmd.Parameters.AddWithValue("@complitionStatus", true);
        //            //cmd.Parameters.AddWithValue("@isOpen", true);
        //            //cmd.Parameters.AddWithValue("@isWip", createdBy);
        //            cmd.Parameters.AddWithValue("@start_date", StandardDateTime.GetDateTime());
        //            cmd.Parameters.AddWithValue("@end_date", StandardDateTime.GetDateTime());
        //            cmd.Parameters.AddWithValue("@recv_date", StandardDateTime.GetDateTime());
        //            cmd.Parameters.AddWithValue("@submission_date", StandardDateTime.GetDateTime());
        //            cmd.Parameters.AddWithValue("@createdOn", StandardDateTime.GetDateTime());


        //            var returnCode = new SqlParameter();
        //            returnCode.ParameterName = "@ret";
        //            returnCode.SqlDbType = SqlDbType.VarChar;
        //            returnCode.Size = 500;
        //            returnCode.Direction = ParameterDirection.Output;
        //            returnCode.Value = "";
        //            cmd.Parameters.Add(returnCode);

        //            var returnStatus = new SqlParameter();
        //            returnStatus.ParameterName = "@retStatus";
        //            returnStatus.SqlDbType = SqlDbType.VarChar;
        //            returnStatus.Size = 50;
        //            returnStatus.Direction = ParameterDirection.Output;
        //            returnStatus.Value = "";
        //            cmd.Parameters.Add(returnStatus);

        //            cmd.ExecuteNonQuery();

        //            transaction.Commit();
        //            connection.Close();
        //            objReturn.ReturnMessage = returnCode.Value.ToString();
        //            objReturn.ReturnStatus = returnStatus.Value.ToString();
        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            connection.Close();
        //            ErrorHandler.LogError(ex, Convert.ToInt64(createdBy), "API", "Dashboard_Repository/InsertProgressTableFromStageTbl");
        //            result = ex.Message;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandler.LogError(ex, Convert.ToInt64(createdBy), "API", "Dashboard_Repository/InsertProgressTableFromStageTbl");
        //    }

        //    return objReturn;
        //}

        public DBReturnModel InsertworkflowProgress(workflow_progress_Model objModel)
        {
            string result = "Error on Inserting workflow Progress!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                foreach (var docdata in objModel.workflow_process_docs_Model_List)
                {
                    string folderPath = "";
                    if (docdata.Doc_Class_ID > 0)
                    {
                        var docclass = objDocClassRepo.GetOneDocumentClass(docdata.Doc_Class_ID, Convert.ToInt64(objModel.submittedBy), "");
                        docdata.FilePath = SaveDocument(docdata.FilePath,  docdata.FileName, docclass.DocumentClassName, objModel.thrdID.ToString(), objModel.wfID.ToString(), docdata.Stage_name);
                    }
                    else
                    {
                        docdata.FilePath = SaveDocument(docdata.FilePath,  docdata.FileName, "", objModel.thrdID.ToString(), objModel.wfID.ToString(), docdata.Stage_name);
                    }
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertworkflowProgress";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@thrdID", objModel.thrdID);
                    cmd.Parameters.AddWithValue("@wfID", objModel.wfID);
                    cmd.Parameters.AddWithValue("@stgID", objModel.stgID);
                    cmd.Parameters.AddWithValue("@peopleType", objModel.peopleType);
                    cmd.Parameters.AddWithValue("@personID", objModel.personID);
                    cmd.Parameters.AddWithValue("@formID", objModel.formID);
                    cmd.Parameters.AddWithValue("@formData", objModel.formData);
                    cmd.Parameters.AddWithValue("@chkID", objModel.chkID);
                    cmd.Parameters.AddWithValue("@chkData", objModel.chkData);
                    if (objModel.processStatus == null)
                    {
                        cmd.Parameters.AddWithValue("@processStatus", "Yes");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@processStatus", objModel.processStatus);
                    }
                    cmd.Parameters.AddWithValue("@comments", objModel.comments);
                    cmd.Parameters.AddWithValue("@complitionStatus", true);
                    cmd.Parameters.AddWithValue("@threadStatus", true);
                    cmd.Parameters.AddWithValue("@recv_date", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@start_date", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@end_date", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@submission_date", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@submissionStatus", objModel.submissionStatus);

                    cmd.Parameters.AddWithValue("@submittedBy", objModel.submittedBy);
                    cmd.Parameters.AddWithValue("@communicateTo", objModel.communicateTo);
                    cmd.Parameters.AddWithValue("@defaultFile", objModel.defaultFile);



                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.workflow_process_docs_Type";
                    dataTable.Columns.Add("Sub_CompID", typeof(long));
                    dataTable.Columns.Add("DomainID", typeof(long));
                    dataTable.Columns.Add("Doc_Class_ID", typeof(long));
                    dataTable.Columns.Add("WFID", typeof(long));
                    dataTable.Columns.Add("ThrdID", typeof(long));
                    dataTable.Columns.Add("Stage_name", typeof(string));
                    dataTable.Columns.Add("Stage_order", typeof(long));
                    dataTable.Columns.Add("status", typeof(string));
                    dataTable.Columns.Add("UploadedBy", typeof(long));
                    dataTable.Columns.Add("UploadedOn", typeof(DateTime));
                    dataTable.Columns.Add("FileName", typeof(string));
                    dataTable.Columns.Add("FilePath", typeof(string));
                    dataTable.Columns.Add("FileSize", typeof(string));
                    dataTable.Columns.Add("cmt", typeof(string));
                    dataTable.Columns.Add("verID", typeof(string));
                    foreach (var dta in objModel.workflow_process_docs_Model_List)
                    {
                        dataTable.Rows.Add(dta.Sub_CompID, dta.DomainID, dta.Doc_Class_ID, objModel.wfID, objModel.thrdID,
                            dta.Stage_name, dta.Stage_order, dta.status, Convert.ToInt64(objModel.submittedBy), Standard.StandardDateTime.GetDateTime(), dta.FileName,
                            dta.FilePath, dta.FileSize, dta.cmt, dta.verID); // Id of '1' is valid for the Person table

                    }
                    SqlParameter parameter = new SqlParameter("workflow_process_docs_Type", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.submittedBy), "API", "Dashboard_Repository/InsertworkflowProgress");
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.submittedBy), "API", "Dashboard_Repository/InsertworkflowProgress");
            }
            return objreturn;

        }

        public workflow_thread_progress_table GetOneworkflowStage(long SLNO, long CreatedBy, long stageOrderID)// Get StageName and Description
        {
            workflow_thread_progress_table objModel = new workflow_thread_progress_table();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                //string sqlstr = "select * from workflow_stage_table where SLNO=@SLNO";
                string sqlstr = "CB_SP_SelectWorkFlowStageByWorkFlowIDAndStageId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@SLNO", SLNO);
                cmd.Parameters.AddWithValue("@workFlowID", SLNO);
                cmd.Parameters.AddWithValue("@stageOrderID", stageOrderID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.stageName = sdr["stageName"].ToString();
                    objModel.stageDescription = sdr["stageDescription"].ToString();
                    objModel.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    objModel.stageOrderID = sdr["stageOrderID"].ToString();
                    objModel.PeopleID = Convert.ToInt64(sdr["PeopleID"].ToString());
                    objModel.processYes = sdr["processYes"].ToString();
                    objModel.processNo = sdr["processNo"].ToString();
                    //objModel.workFlowName = sdr["workFlowName"].ToString();
                    //objModel.TotalDays = Convert.ToInt32(sdr["TotalDays"].ToString());
                    //objModel.workFlowLevel = sdr["workFlowLevel"].ToString();
                    //objModel.bucketing = sdr["bucketing"].ToString();
                    //objModel.priority = sdr["priority"].ToString();
                    //objModel.description = sdr["description"].ToString();
                    //objModel.RASCI = sdr["RASCI"].ToString();
                    //objModel.DocCompanyID = Convert.ToInt64(sdr["DocCompanyID"].ToString());
                    //objModel.DocDomainID = Convert.ToInt64(sdr["DocDomainID"].ToString());
                    //objModel.DocClassID = Convert.ToInt64(sdr["DocClassID"].ToString());
                    //objModel.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    //objModel.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    //objModel.createdBy = Convert.ToInt64(sdr["createdBy"].ToString());
                    //objModel.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    //objModel.editedOn = Convert.ToDateTime(sdr["editedOn"].ToString());
                    //objModel.editedBy = Convert.ToInt64(sdr["editedBy"].ToString());
                    //objModel.deletedOn = Convert.ToDateTime(sdr["deletedOn"].ToString());
                    //objModel.BPCompanyName = sdr["BPCompanyName"].ToString();
                    //objModel.BPDomainName = sdr["BPDomainName"].ToString();
                    //objModel.DocCompanyName = sdr["DocCompanyName"].ToString();
                    //objModel.DocDomainName = sdr["DocDomainName"].ToString();
                    //objModel.DocClassName = sdr["DocClassName"].ToString();
                    //objModel.deletedBy = Convert.ToInt64(sdr["deletedBy"].ToString());
                }

                sdr.Close();
                //get PriID from workflow_priority_map

                cmd.Parameters.Clear();
                sqlstr = "CB_SP_SelectWorkFlowPriorityByWorkFlowId";
                cmd = new SqlCommand(sqlstr, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@workFlowID", objModel.workFlowID);

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.PriID = sdr["PriID"].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetOneworkflowStage");
            }

            return objModel;

        }



        // update Query Start for workflow_thread_progress_table

        public DBReturnModel InsertProgressTableFromStageTbl(string WFID, string PriID, long UID, string ThrdRef, long createdBy)
        {

            string result = "Error on Inserting";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            bool status = false;
            bool threadStatus = true;
            bool CmpStatus = false;

            bool open = false;
            bool wip = false;
            bool close = false;
            bool hold = false;

            string thrdID = "0";
            if (!WFID.Equals(""))
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                try
                {

                    workflow_thread_progress_table objModel = new workflow_thread_progress_table();
                    DateTime dt = DateTime.Now;
                    thrdID = createTheradID2(WFID).ToString();
                    // con.Open();
                    //string qry = "INSERT INTO [workflow_thread_progress_table]([threadID],[workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],[createdOn],[threadStatus],[complitionStatus]) SELECT @threadID,[workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],@createdOn,@threadStatus,@CmpStatus FROM [workflow_stage_table]";
                    //string qry = "INSERT INTO [workflow_thread_progress_table]([threadID],[workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[peopleID],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],[createdOn],[threadStatus],[complitionStatus],[isOpen],[isWip],[isClosed],[isOnHold],[communicateTo],[IsDelegated],[DelegatedTo],Viewright,[DynRef],[sapForm],[ThrdRef]) SELECT @threadID,[workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[peopleID],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],@createdOn,@threadStatus,@CmpStatus,@open,@wip,@close,@hold,[communicateTo],[IsDelegated],[DelegatedTo],Viewright,[DynRef],[sapForm],@ThrdRef FROM [workflow_stage_table] WHERE [workFlowID] = @wfid AND [isDeleted]='FALSE'";
                    //[threadID],[workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],[createdOn]

                    string qry = "CB_SP_InsertThreadProgressFromStage";
                    SqlCommand cmd = new SqlCommand(qry, connection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@threadID", thrdID);
                    cmd.Parameters.AddWithValue("@createdOn", dt);
                    cmd.Parameters.AddWithValue("@threadStatus", threadStatus);
                    cmd.Parameters.AddWithValue("@CmpStatus", CmpStatus);
                    cmd.Parameters.AddWithValue("@wfid", WFID);
                    cmd.Parameters.AddWithValue("@ThrdRef", ThrdRef);
                    cmd.Parameters.AddWithValue("@open", open);
                    cmd.Parameters.AddWithValue("@wip", wip);
                    cmd.Parameters.AddWithValue("@close", close);
                    cmd.Parameters.AddWithValue("@hold", hold);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    int row = cmd.ExecuteNonQuery();
                    if (row > 0)
                    {
                        //status = true;
                        status = DateAssigner2(WFID, thrdID, PriID);
                        AssignTriggerInitiator(thrdID, WFID, createdBy);
                        objReturn.ReturnMessage = "Created Successfully";
                        objReturn.ReturnStatus = "SUCCESS";
                        objReturn.Id = Convert.ToInt64(thrdID);


                    }
                    else
                    {
                        objReturn.ReturnMessage = "Error on Creating";
                        objReturn.ReturnStatus = "ERROR";
                        objReturn.Id = Convert.ToInt64(thrdID);
                        status = false;
                    }
                }
                catch (Exception ex)
                {
                    string Optype = "Exception";
                    string OpPerform = "createProcessThread 959";
                    string remark = "Exception while creating thread:" + ex;
                    string OperationOn = DateTime.Today.Date.ToString();
                    //  loghistory.logInsert(adminid, adminid, userslid, Optype, OpPerform, OperationOn, remark);

                    // Response.Redirect("ErrorModel.aspx");
                }
                finally
                {
                    connection.Close();
                }
            }
            return objReturn;
            //return  status + "|" + thrdID;
        }

        protected long createTheradID2(string WFID)
        {
            long thrdID = 0;
            long nwfid = 0;
            nwfid = Convert.ToInt64(WFID);
            if (!WFID.Equals(""))
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                try
                {
                    ////.Open();
                    //string qryMain = "SELECT * FROM [workflow_thread_progress_table] WHERE [workFlowID]=@wfid";
                    //SqlCommand cmd1 = new SqlCommand(qryMain, connection);
                    //cmd1.Parameters.AddWithValue("@wfid", WFID);
                    //SqlDataReader read = cmd1.ExecuteReader();
                    //if (read.HasRows)
                    //{
                    //    read.Close();
                    //    read.Dispose();
                    //    string qry = "SELECT TOP 1 [threadID] FROM [workflow_thread_progress_table] WHERE [workFlowID]=@wfid ORDER BY [SLNO] DESC";
                    //    SqlCommand cmd = new SqlCommand(qry, connection);
                    //    cmd.Parameters.AddWithValue("@wfid", WFID);
                    //    thrdID = Convert.ToInt32(cmd.ExecuteScalar().ToString().Trim()) + 1;
                    //}
                    //else
                    //{
                    //    thrdID = 1;
                    //}
                    //read.Close();
                    //read.Dispose();

                    string sqlstr = "CB_SP_SelectMaxThreadId";                    
                    SqlCommand cmd1 = new SqlCommand(sqlstr, connection);
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@wfid", nwfid);
                    SqlDataReader sdr1 = cmd1.ExecuteReader();
                    while (sdr1.Read())
                    {
                        thrdID = Convert.ToInt64(sdr1["threadID"].ToString());
                    }
                    sdr1.Close();
                }
                catch (Exception ex)
                {
                    string Optype = "Exception";
                    string OpPerform = "creation of Therad ID";
                    string remark = "Exception while creating thread ID :" + ex;
                    string OperationOn = DateTime.Today.Date.ToString();
                    // loghistory.logInsert(adminid, adminid, userslid, Optype, OpPerform, OperationOn, remark);

                    //Response.Redirect("ErrorModel.aspx");
                }
                finally
                {
                    connection.Close();
                }
            }
            return thrdID;
        }

        protected bool DateAssigner2(string wfid, string thrid, string priID)
        {
            bool status = false;
            SqlTransaction trans = null;
            try
            {
                ArrayList list = new ArrayList();
                //  string  connection = SqlHelper.Connection().ToString();
                //using (SqlConnection con = new SqlConnection(connection))
                //{
                var connection = SqlHelper.Connection();
                bool IsPriVisible = false;
                connection.Open();
                string qry1 = "Select * from [workflow_thread_progress_table] where [workFlowID]=@wfid and [threadID]=@thrid";
                if (priID != null)
                {
                    qry1 = "Select pri.BPMID,pri.PriID,pri.StgID,pri.Day,th.* from [workflow_thread_progress_table] as th inner join [workflow_priority_map] as pri on th.workFlowID=pri.BPMID and th.stageOrderID=pri.StgID where [workFlowID]=@wfid and [threadID]=@thrid and pri.PriID=@PriID and pri.IsDeleted=0;";
                    IsPriVisible = true;
                }
                else
                {
                    priID = "";
                }
                SqlCommand cmd = new SqlCommand(qry1, connection);
                cmd.Parameters.AddWithValue("@wfid", wfid);
                cmd.Parameters.AddWithValue("@thrid", thrid);
                cmd.Parameters.AddWithValue("@PriID", priID);
                int totalRow = 0;
                int InsertRow = 0;
                using (SqlDataReader read = cmd.ExecuteReader())
                {
                    if (read.HasRows)
                    {
                        while (read.Read())
                        {
                            totalRow++;
                            string slno = read["SLNO"].ToString();
                            string stgord = read["stageOrderID"].ToString();
                            string createdOn = read["createdOn"].ToString();

                            string stgDays = "0";// change this parameter based on the priority
                            if (IsPriVisible)
                            {
                                stgDays = read["Day"].ToString();
                            }
                            else
                            {
                                stgDays = read["stageDays"].ToString();
                            }

                            list.Add(slno + "|" + stgord + "|" + stgDays + "|" + createdOn);
                        }
                    }
                }

                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        string slno = list[i].ToString().Split('|')[0];
                        string stgord = list[i].ToString().Split('|')[1];
                        string stgday = list[i].ToString().Split('|')[2];
                        string createdOn = list[i].ToString().Split('|')[3];
                        int order = Convert.ToInt32(stgord);
                        int days = Convert.ToInt32(stgday);
                        DateTime createON = DateTime.Parse(createdOn);

                        DateTime sDate = createON;
                        DateTime eDate = createON;

                        if (order > 1)
                        {
                            string stgdayPrevious;
                            int daysPrevious = 0;
                            if (i != 0)
                            {
                                for (int j = 0; j < i; j++)
                                {
                                    //stgdayPrevious = list[i - 1].ToString().Split('|')[2];
                                    stgdayPrevious = list[j].ToString().Split('|')[2];
                                    daysPrevious += Convert.ToInt32(stgdayPrevious);
                                }
                            }
                            sDate = sDate.AddDays(daysPrevious);
                            eDate = eDate.AddDays(daysPrevious + days - 1);
                        }
                        else
                        {
                            sDate = createON;
                            if (days == 0)
                            {
                                eDate = createON;
                            }
                            else
                            {
                                eDate = eDate.AddDays(days - 1);
                            }

                        }

                        trans = connection.BeginTransaction();
                        string qry = "UPDATE [workflow_thread_progress_table] SET [start_date]=@sDate ,[end_date]=@eDate where [SLNO]=@slno";
                        if (IsPriVisible)
                        {
                            qry = "UPDATE [workflow_thread_progress_table] SET [stageDays]=@stageDays,[start_date]=@sDate ,[end_date]=@eDate where [SLNO]=@slno";
                        }
                        SqlCommand cmd1 = new SqlCommand(qry, connection, trans);
                        cmd1.Parameters.AddWithValue("@sDate", sDate);
                        cmd1.Parameters.AddWithValue("@eDate", eDate);
                        cmd1.Parameters.AddWithValue("@slno", slno);
                        cmd1.Parameters.AddWithValue("@stageDays", days.ToString());
                        int row = cmd1.ExecuteNonQuery();
                        if (row == 1)
                        {
                            trans.Commit();
                            InsertRow++;
                        }
                        else
                        {
                            trans.Rollback();
                        }
                    }
                }
                if (totalRow != 0 && InsertRow != 0 && totalRow == InsertRow)
                {
                    status = true;
                }
                // }
            }
            catch (Exception ex)
            {
                status = false;
                trans.Rollback();
            }
            return status;
        }

        public bool AssignTriggerInitiator(string thrdID, string BPMID, long uid)
        {
            bool status = false;
            var connection = SqlHelper.Connection();
            connection.Open();
            // SqlConnection con = new SqlConnection(connection);
            SqlTransaction trans = null;
            try
            {
                // con.Open();
                trans = connection.BeginTransaction();
                string qry = "update [workflow_thread_progress_table] set [peopleID]=@peopleID where [threadID]=@thrdID and [workFlowID]=@wfid and [stageOrderID]='1' and [complitionStatus]='FALSE'";
                //string qry = "update [workflow_thread_progress_table] set [peopleInfo]=(Select [SubCompanyName]+'|'+[Domain_Name]+'|'+[EmployeeName] AS EMP FROM [Employee_table] as a inner join [Sub_Company_table] as b on a.[Sub_Company]=b.[SLNO] inner join [Domain_table] as c on a.[Domain_ID]=c.[SLNO] WHERE a.[Sub_Company] =@coy AND a.[Domain_ID] =@dom AND A.[SLNO]=@peopleID AND a.[IsDeleted]='FALSE'),[peopleID]=@peopleID where [threadID]=@thrdID and [workFlowID]=@wfid and [stageOrderID]='1' and [people]=@peopleType and [complitionStatus]='FALSE'";
                /*if (Type == "External")
                {
                    qry = "update [workflow_thread_progress_table] set [peopleInfo]=(SELECT b.SubCompanyName + '|' + c.Domain_Name + '|' + v.CompanyName AS EMP FROM dbo.Employee_table AS a INNER JOIN dbo.Sub_Company_table AS b ON a.Sub_Company = b.SLNO INNER JOIN dbo.Domain_table AS c ON a.Domain_ID = c.SLNO INNER JOIN dbo.ven_Table AS v ON a.SLNO = v.User_id WHERE a.[Sub_Company] =@coy AND a.[Domain_ID] =@dom AND A.[SLNO]=@peopleID AND a.[IsDeleted]='FALSE'),[peopleID]=@peopleID where [threadID]=@thrdID and [workFlowID]=@wfid and [people]=@peopleType and [complitionStatus]='FALSE' and ([peopleInfo]=@old_peopleInfo or [peopleInfo]='Initiator')";
                }*/

                SqlCommand cmd = new SqlCommand(qry, connection, trans);
                cmd.Parameters.AddWithValue("@peopleID", uid);
                cmd.Parameters.AddWithValue("@thrdID", thrdID);
                cmd.Parameters.AddWithValue("@wfid", BPMID);
                //cmd.Parameters.AddWithValue("@peopleType", Type);
                //cmd.Parameters.AddWithValue("@coy", subcompID);
                //cmd.Parameters.AddWithValue("@dom", dom);
                int row = cmd.ExecuteNonQuery();
                if (row > 0)
                {
                    trans.Commit();
                    status = true;
                }
                else
                {
                    trans.Rollback();
                    status = false;
                }
            }
            catch (Exception ex)
            {
                status = false;
                trans.Rollback();
            }
            finally
            {
                connection.Close();
            }
            return status;
        }

        public bool MakeTabShifter(string action, long wfid, long thrdID, string order, string orderNext, string key)
        {
            bool status = false;
            if (wfid != 0 && thrdID != 0 && !order.Equals(""))
            {
                if (key == null)
                {
                    key = "";
                }
                SqlTransaction trans = null;
                try
                {
                    int order1 = 0;
                    string sqlQuerry = "";
                    string stageName = "";
                    if (action.Equals("open"))
                    {
                        //sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@on,[isWip]=@off,[isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order AND [isOnHold]=@off;UPDATE [workflow_thread_progress_table] SET [isWip]=@off,[isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [isOnHold]=@off";
                        sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@on,[isWip]=@off,[isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order AND [isOnHold]=@off";
                    }
                    else if (action.Equals("wip"))
                    {
                        sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@off,[isWip]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order";
                    }
                    else if (action.Equals("close"))
                    {
                        int OrderID = Convert.ToInt32((string.IsNullOrEmpty(order) ? "0" : order)),
                            OrderIDNext = Convert.ToInt32((string.IsNullOrEmpty(orderNext) ? "0" : orderNext));
                        string OperatorQry = "[stageOrderID] >= @order1 ORDER BY [stageOrderID] ASC";

                        if (OrderID > OrderIDNext)
                            OperatorQry = "[stageOrderID] <= @order1 ORDER BY [stageOrderID] DESC";

                        string QryStageOrderID = string.Format("SELECT TOP 1 [stageOrderID] FROM [workflow_thread_progress_table] WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND processYes <> 'skip' AND {0}", OperatorQry);

                        if (key.Contains("Old Stages"))
                        {
                            stageName = key.Split('|')[1];
                            sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@off, [isWip]=@off, [isClosed]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order; UPDATE [workflow_thread_progress_table] SET [isOpen]=@on, [isWip]=@off, [isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageName]=@stageName and processYes <> 'skip'";


                            sqlQuerry += ";UPDATE [workflow_thread_progress_table] SET [isOpen]=@on, [isWip]=@off, [isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid  and processYes <> 'skip' AND  [stageName]=@stageName";

                        }
                        else
                        {
                            sqlQuerry = string.Format("UPDATE [workflow_thread_progress_table] SET [isOpen]=@off, [isWip]=@off, [isClosed]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order; UPDATE [workflow_thread_progress_table] SET [isOpen]=@on, [isWip]=@off, [isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=({0})", QryStageOrderID);

                            //sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@off, [isWip]=@off, [isClosed]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=@order; UPDATE [workflow_thread_progress_table] SET [isOpen]=@on, [isWip]=@off, [isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [stageOrderID]=(Select Top 1 StageOrderId from [workflow_thread_progress_table] where [workFlowID]=@wfid AND [threadID]=@thrdid and processYes <> 'skip' and StageOrderId < @order order by SLNO DESC)";

                            OperatorQry = "[stageOrderID] = @order1";
                            sqlQuerry += string.Format(";UPDATE [workflow_thread_progress_table] SET [isOpen]=@on, [isWip]=@off, [isClosed]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid  and processYes = 'skip' AND {0}", OperatorQry);
                        }



                    }
                    else if (action.Equals("hold"))
                    {
                        sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOnHold]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [isClosed]=@off";
                    }
                    else if (action.Equals("unhold"))
                    {
                        sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOnHold]=@off WHERE [workFlowID]=@wfid AND [threadID]=@thrdid AND [isClosed]=@off";
                    }
                    else if (action.Equals("closeAll"))
                    {
                        sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [isOpen]=@off, [isWip]=@off, [isClosed]=@on WHERE [workFlowID]=@wfid AND [threadID]=@thrdid";
                    }

                    if (!sqlQuerry.Equals(""))
                    {
                        bool on = true;
                        bool off = false;
                        //using (SqlConnection con = new SqlConnection(connection))
                        //{
                        var connection = SqlHelper.Connection();
                        connection.Open();
                        //con.Open();
                        trans = connection.BeginTransaction();
                        SqlCommand cmd = new SqlCommand(sqlQuerry, connection, trans);
                        cmd.Parameters.AddWithValue("@wfid", wfid);
                        cmd.Parameters.AddWithValue("@thrdid", thrdID);
                        cmd.Parameters.AddWithValue("@order", order);
                        if (orderNext == null || orderNext == "")
                        {
                            cmd.Parameters.AddWithValue("@order1", "");
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@order1", orderNext);
                        }

                        cmd.Parameters.AddWithValue("@on", on);
                        cmd.Parameters.AddWithValue("@off", off);
                        cmd.Parameters.AddWithValue("@stageName", stageName);

                        //cmd.Transaction = trans;
                        int row = cmd.ExecuteNonQuery();
                        if ((row == 1) || ((action.Equals("hold") || action.Equals("unhold") || action.Equals("close") || action.Equals("closeAll")) && (row > 1)))
                        {
                            status = true;
                            trans.Commit();
                        }
                        else
                        {
                            status = false;
                            trans.Rollback();
                        }
                        // }
                    }
                    else
                    {
                        status = false;
                    }


                    if (action.Equals("closeAll"))
                    {
                        closeThread(wfid.ToString(), thrdID.ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (trans != null)
                    {
                        trans.Rollback();
                    }
                }
            }
            return status;
        }




        public string GetNextProcessStatus(int Order, long BPM_ThrdID, long BPMID)
        {
            string processYes = "";
            var connection = SqlHelper.Connection();
            connection.Open();
            //SqlConnection con = new SqlConnection(connection);
            try
            {
                //con.Open();

                string qry = "";
                if (Order == 1 && BPM_ThrdID == 0)
                {
                    qry = "Select * from [workflow_stage_table] where [workFlowID]=@wfid and [stageOrderID]=@order and [isDeleted]='FALSE' and [activeStatus]='TRUE'";
                }
                else
                {
                    qry = "Select * from [workflow_thread_progress_table] where [threadID]=@thrdID and [workFlowID]=@wfid and [stageOrderID]=@order and [complitionStatus]='FALSE'";
                }

                SqlCommand cmd = new SqlCommand(qry, connection);
                cmd.Parameters.AddWithValue("@thrdID", BPM_ThrdID);
                cmd.Parameters.AddWithValue("@wfid", BPMID);
                cmd.Parameters.AddWithValue("@order", Order + 1);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        processYes = dr["processYes"].ToString().Trim();
                    }
                }
                dr.Dispose();
                dr.Close();
            }
            catch (Exception ex)
            {
                //Response.Redirect("ErrorModel.aspx");
            }
            finally
            {
                connection.Close();
            }

            return processYes;
        }


        public workflow_thread_progress_table GetOneWorkFlowStageForOneThread(long SLNO, long ThreadId, long StageOrderId, long CreatedBy)// Get StageName and Description
        {
            workflow_thread_progress_table objModel = new workflow_thread_progress_table();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, threadID, workFlowID, stageOrderID, stageName, Isnull(stageDescription,'') as stageDescription, "
                + " Isnull(stageDays, 0) as stageDays, Isnull(people, '') as people, Isnull(peopleInfo, '') as peopleInfo, Isnull(RASCI, '') as RASCI, "
                + " Isnull(attachment, '') as attachment, Isnull(url, '') as url, Isnull(form, '') as form, Isnull(checklist, '') as checklist, "
                + " Isnull(approverWorkFlow, '') as approverWorkFlow, Isnull(processYes, '') as processYes, Isnull(processNo, '') as processNo, "
                + " Isnull(createdOn, '') as createdOn, Isnull(threadStatus, '') as threadStatus, Isnull(complitionStatus, '') as complitionStatus, "
                + " Isnull(isOpen, '') as isOpen, ISnull(isWip, '') as isWip, Isnull(isClosed, '') as isClosed, Isnull(isOnHold, '') as isOnHold, "
                + " Isnull(start_date, '') as start_date, Isnull(end_date, '') as end_date, Isnull(recv_date, '') as recv_date, ISnull(submission_date, '') as submission_date, "
                + " Isnull(Comment, '') as Comment, Isnull(communicateTo, '') as communicateTo, Isnull(IsDelegated, '') as IsDelegated, "
                + " Isnull(DelegatedTo, '') as DelegatedTo, Isnull(peopleID, '') as peopleID, Isnull(Viewright, '') as Viewright, "
                + " Isnull(DynRef, '') as DynRef, Isnull(sapForm, '') as sapForm, Isnull(ThrdRef, '') as ThrdRef, ISnull(DynGroupId, 0) as DynGroupId, "
                + " Isnull(formTemplate, '') as formTemplate, Isnull(CPSimulationRefNumber, '') as CPSimulationRefNumber, Isnull(BPMTriggerId, 0) as BPMTriggerId, "
                + "( Select Top 1 submission_date from [workflow_progress_table] where [thrdID]=@thrdID and [wfID]=@wfid and [stgID]=@order order by [SLNO] desc) as progress_submission_date"
                + " From workflow_thread_progress_table where threadID = @thrdID and workFlowID = @wfid and stageOrderID = @order";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@thrdID", ThreadId);
                cmd.Parameters.AddWithValue("@wfid", SLNO);
                cmd.Parameters.AddWithValue("@order", StageOrderId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.threadID = Convert.ToInt64(sdr["threadID"].ToString());
                    objModel.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    objModel.stageOrderID = sdr["stageOrderID"].ToString();
                    objModel.stageName = sdr["stageName"].ToString();
                    objModel.stageDescription = sdr["stageDescription"].ToString();
                    objModel.stageDays = sdr["stageDays"].ToString();
                    objModel.people = sdr["people"].ToString();
                    objModel.peopleInfo = sdr["peopleInfo"].ToString();
                    objModel.RASCI = sdr["RASCI"].ToString();
                    objModel.attachment = sdr["attachment"].ToString();
                    objModel.url = sdr["url"].ToString();
                    objModel.form = sdr["form"].ToString();
                    objModel.checklist = sdr["checklist"].ToString();
                    objModel.approverWorkFlow = sdr["approverWorkFlow"].ToString();
                    objModel.processYes = sdr["processYes"].ToString();
                    objModel.processNo = sdr["processNo"].ToString();
                    objModel.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    objModel.threadStatus = Convert.ToBoolean(sdr["threadStatus"].ToString());
                    objModel.complitionStatus = Convert.ToBoolean(sdr["complitionStatus"].ToString());
                    objModel.isOpen = Convert.ToBoolean(sdr["isOpen"].ToString());
                    objModel.isWip = Convert.ToBoolean(sdr["isWip"].ToString());
                    objModel.isClosed = Convert.ToBoolean(sdr["isClosed"].ToString());
                    objModel.isOnHold = Convert.ToBoolean(sdr["isOnHold"].ToString());
                    objModel.start_date = Convert.ToDateTime(sdr["start_date"].ToString());
                    objModel.end_date = Convert.ToDateTime(sdr["end_date"].ToString());
                    objModel.recv_date = Convert.ToDateTime(sdr["recv_date"].ToString());
                    objModel.submission_date = Convert.ToDateTime(sdr["submission_date"].ToString());
                    objModel.Comment = sdr["Comment"].ToString();
                    objModel.communicateTo = sdr["communicateTo"].ToString();
                    objModel.IsDelegated = Convert.ToBoolean(sdr["IsDelegated"].ToString());
                    objModel.DelegatedTo = sdr["DelegatedTo"].ToString();
                    objModel.PeopleID = Convert.ToInt64(sdr["PeopleID"].ToString());
                    objModel.Viewright = sdr["Viewright"].ToString();
                    objModel.DynRef = sdr["DynRef"].ToString();
                    objModel.sapForm = sdr["sapForm"].ToString();
                    objModel.ThrdRef = sdr["ThrdRef"].ToString();
                    objModel.DynGroupId = Convert.ToInt64(sdr["DynGroupId"].ToString());
                    objModel.formTemplate = sdr["formTemplate"].ToString();
                    objModel.CPSimulationRefNumber = sdr["CPSimulationRefNumber"].ToString();
                    objModel.BPMTriggerId = Convert.ToInt64(sdr["BPMTriggerId"].ToString());
                    if (sdr["progress_submission_date"] != null)
                        objModel.progress_submission_date = Convert.ToDateTime(sdr["progress_submission_date"]);  
                }

                sdr.Close();
                //get PriID from workflow_priority_map

                cmd.Parameters.Clear();
                sqlstr = "";
                sqlstr = "select PriID  from workflow_priority_map inner join workflow_stage_table on workflow_priority_map.BPMID=workflow_stage_table.workFlowID" +
                    "  where workflow_stage_table.workFlowID=@workFlowID and workflow_stage_table.isDeleted=0";
                cmd = new SqlCommand(sqlstr, connection);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@workFlowID", objModel.workFlowID);

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.PriID = sdr["PriID"].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetOneworkflowStage");
            }

            return objModel;

        }

        // call this after submit stage
        // bool stat = TabSwitcher(processStatus, wfID, thrdID, stageOrder);
        // update Query End for workflow_thread_progress_table



        private string SaveDocument(string EmpPhoto, string nFileName, string docClass, string thrdID, string wfid, string stageName)
        {
            string result = "";

            try
            {
                var guid = Guid.NewGuid().ToString();
                string folderPath = "";
                if (!docClass.Equals("") && !thrdID.Equals(""))
                {
                    folderPath = Path.Combine("" + "/Content/Documents/" + docClass + "/BPM_ID " + wfid + "/Thread " + thrdID + "/" + stageName);
                }
                else
                {
                    folderPath = Path.Combine("" + "/Content/Documents/" + "DocCLass/WORKFLOW/THREAD" + "/" + stageName);
                }

                if (!Directory.Exists(folderPath))
                {
                    if (folderPath.Contains(" "))
                    {
                        string folderPath1 = folderPath.Replace(" ", "\\ ");
                    }
                    //If Directory (Folder) does not exists. Create it.
                    Directory.CreateDirectory(folderPath);
                }


                byte[] imageBytes = Convert.FromBase64String(EmpPhoto);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                //System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                string filepath = Path.Combine(folderPath, nFileName);
                File.WriteAllBytes(filepath, imageBytes);


                result = filepath;
            }
            catch (Exception ex)
            {
                //result = ex.Message;
            }

            return result;




        }




        protected bool closeThread(string Wfid, string thrdId)
        {
            bool status = false;
            bool thrdStatus = false;
            bool cmpStatus = true;
            var con = SqlHelper.Connection();
            try
            {
                //int slno = Convert.ToInt32(Wfid);
                DateTime cdt = StandardDateTime.GetDateTime();
                con.Open();
                //string qry = "UPDATE [workflow_stage_table] SET [workFlowID],[stageOrderID],[stageName],[stageDescription],[stageDays],[people],[peopleInfo],[RASCI],[attachment],[url],[form],[checklist],[approverWorkFlow],[processYes],[processNo],[createdBy],[createdOn],[editedBy],[editedOn],[isDeleted],[deletedBy],[deletedOn] WHERE ";
                string sqlQuerry = "UPDATE [workflow_thread_progress_table] SET [threadStatus]=@thrdStatus,[complitionStatus]=@cmpStatus WHERE [workFlowID]=@wfid AND [threadID]=@thrdiD";

                SqlCommand cmd = new SqlCommand(sqlQuerry, con);
                cmd.Parameters.AddWithValue("@wfid", Wfid);
                cmd.Parameters.AddWithValue("@thrdStatus", thrdStatus);
                cmd.Parameters.AddWithValue("@cmpStatus", cmpStatus);
                cmd.Parameters.AddWithValue("@thrdiD", thrdId);

                int row = cmd.ExecuteNonQuery();
                if (row > 0)
                {

                    string sqlQuerry1 = "UPDATE [workflow_progress_table] SET [threadStatus]=@thrdStatus WHERE [wfID]=@wfid AND [thrdID]=@thrdiD";
                    SqlCommand cmd1 = new SqlCommand(sqlQuerry1, con);
                    cmd1.Parameters.AddWithValue("@wfid", Wfid);
                    cmd1.Parameters.AddWithValue("@thrdStatus", thrdStatus);
                    cmd1.Parameters.AddWithValue("@thrdiD", thrdId);

                    int row1 = cmd1.ExecuteNonQuery();
                    if (row1 > 0)
                    {
                        //Call CP method  if wfid belongs to  CP

                        sqlQuerry1 = "SELECT CPSimulationRefNumber  From [workflow_thread_progress_table]   WHERE [workflowId]=@wfid AND [threadId]=@thrdiD and IsNull(CPSimulationRefNumber,'') <> ''";
                        cmd1 = new SqlCommand(sqlQuerry1, con);
                        cmd1.Parameters.AddWithValue("@wfid", Wfid);
                        cmd1.Parameters.AddWithValue("@thrdiD", thrdId);
                        SqlDataReader oreader = cmd1.ExecuteReader();
                        if (oreader.Read())
                        {
                            //activateCPThread(Wfid, oreader["CPSimulationRefNumber"].ToString());
                        }
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }
                else
                {
                    status = false;
                }
            }
            catch (Exception ex)
            {
                string Optype = "Exception";
                string OpPerform = "closeThread P:1260 ";
                string remark = "Exception while closing thread :" + ex;
                string OperationOn = DateTime.Today.Date.ToString();
                //loghistory.logInsert(adminid, adminid, userslid, Optype, OpPerform, OperationOn, remark);

                //Response.Redirect("ErrorModel.aspx");
            }
            finally
            {
                con.Close();
            }
            return status;
        }


        public List<PieChartReportViewModel> GetDashboardPieChartDataForBPM(long CreatedBy)
        {
            List<PieChartReportViewModel> objList = new List<PieChartReportViewModel>();

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                // For OPEN
                //string sqlstr = "SELECT COUNT (*) FROM [workflow_stage_table] WHERE [people]=@peopleType AND [activeStatus]=@activeStatus " +
                //    "AND [stageOrderID]=@stageOrderID AND [peopleInfo] LIKE '%" + user + "'";

                string sqlstr = "CB_SP_SelectDashboardPieChartDataForBPM_OpenStage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    PieChartReportViewModel tempobj = new PieChartReportViewModel();
                    tempobj.Count = Convert.ToInt32(sdr["CNT"].ToString());
                    tempobj.DataName = "Open";
                    objList.Add(tempobj);
                }
                sdr.Close();


                // For PROGRESS
                cmd.Parameters.Clear();
                //sqlstr = "SELECT COUNT (*) FROM [workflow_thread_progress_table] WHERE [people]=@peopleType AND " +
                //    "[threadStatus]=@activeStatus AND [complitionStatus]=@cmpStatus AND [peopleInfo] LIKE '%" + user + "'";
                sqlstr = "CB_SP_SelectDashboardPieChartDataForBPM_ProgressStage";
                cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@activeStatus", true);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    PieChartReportViewModel tempobj = new PieChartReportViewModel();
                    tempobj.Count = Convert.ToInt32(sdr["CNT"].ToString());
                    tempobj.DataName = "Progress";
                    objList.Add(tempobj);
                }
                sdr.Close();



                // For CLOSE
                cmd.Parameters.Clear();
                //sqlstr = "SELECT COUNT (*) FROM [workflow_thread_progress_table] WHERE [people]=@peopleType AND [threadStatus]=@activeStatus
                // AND [peopleInfo] LIKE '%" + user + "' OR [peopleInfo]=@AllUser";
                sqlstr = "CB_SP_SelectDashboardPieChartDataForBPM_CloseStage";
                cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@activeStatus", false);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    PieChartReportViewModel tempobj = new PieChartReportViewModel();
                    tempobj.Count = Convert.ToInt32(sdr["CNT"].ToString());
                    tempobj.DataName = "Close";
                    objList.Add(tempobj);
                }
                sdr.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetDashboardPieChartDataForBPM");
            }

            return objList;
        }




        public List<DashBPMWipOrClosedTab_Model> GetBPMClosedFormList(long CreatedBy)
        {
            List<DashBPMWipOrClosedTab_Model> objList = new List<DashBPMWipOrClosedTab_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }


                //Get Closed tabl List

                cmd.Parameters.Clear();
                sqlstr = "";
                string query = "CB_SP_SelectBPMClosedFormList";
                cmd = new SqlCommand(query, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                cmd.Parameters.AddWithValue("@complitionStatus", true);
                cmd.Parameters.AddWithValue("@isClosed", true);

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMWipOrClosedTab_Model tempobj = new DashBPMWipOrClosedTab_Model();
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.ThrdRef = sdr["ThrdRef"].ToString();
                    tempobj.threadID = Convert.ToInt64(sdr["threadID"].ToString());
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageDays = Convert.ToInt64(sdr["stageDays"].ToString());
                    tempobj.RASCI = sdr["RASCI"].ToString();

                    tempobj.BPM_Name = sdr["workFlowName"].ToString();
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();

                    objList.Add(tempobj);
                }
                sdr.Close();
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMClosedFormList");
            }
            return objList;
        }

        public List<DashBPMWipOrClosedTab_Model> GetBPMOpenFormList(long CreatedBy)
        {
            List<DashBPMWipOrClosedTab_Model> objModel = new List<DashBPMWipOrClosedTab_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "";
                string peopleType = "Internal";
                string usertype = "";

                sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", CreatedBy);
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    usertype = sdr["User_type"].ToString();
                }
                sdr.Close();

                if (usertype == "VENDOR" || usertype == "CUSTOMER")
                {
                    peopleType = "External";
                }
                //get Work flow records
                cmd.Parameters.Clear();
                //string query = "SELECT st.*,wf.workFlowName,wf.owner,coy.SubCompanyName,dom.Domain_Name FROM [workflow_stage_table] as st Inner join workflow_table as wf on wf.SLNO = st.workFlowID Inner join Sub_Company_table as coy on coy.SLNO = wf.subCmpID Inner join Domain_table as dom on dom.SLNO = wf.Fnc_Domain";

                //query += " where (st.[people] = 'Internal' AND st.[activeStatus] = 1 AND st.[stageOrderID] = 1 AND st.[isDeleted] = 0 AND (st.[peopleID] = @peopleID or st.[peopleInfo] = 'All People'))";
                //query += " OR ([DelegatedTo] = @peopleID AND [IsDelegated] = 'TRUE' AND st.[people] = 'Internal' AND st.[activeStatus] = 1 AND st.[stageOrderID] = 1 AND st.[isDeleted] = 0)";
                //query += " OR ([people] = 'Group' AND st.[activeStatus] = 1 AND st.[stageOrderID] = 1 AND st.[isDeleted] = 0 AND st.[PeopleID] in (Select gc.[GroupID] from [Group_Company] as gc inner join Group_table as gt on gc.GroupID = gt.SLNO where gc.[CompanyID] = 2 and gt.IsDeleted= 0))";
                //query += " OR ([people] = 'Group' AND st.[activeStatus] = 1 AND st.[stageOrderID] = 1 AND st.[isDeleted] = 0 AND st.[PeopleID] in (Select gc.[GroupID] from [Group_Domain] as gc inner join Group_table as gt on gc.GroupID = gt.SLNO where gc.[CompanyID] = 2 and gc.[DomainID] = 1 and gt.IsDeleted= 0))";
                //query += " OR ([people] = 'Group' AND st.[activeStatus] = 1 AND st.[stageOrderID] = 1 AND st.[isDeleted] = 0 AND st.[PeopleID] in (Select gc.[GroupID] from [GroupUser] as gc inner join Group_table as gt on gc.GroupID = gt.SLNO where gc.[User_ID] = @peopleID  and gt.IsDeleted= 0))";

                string query = "CB_SP_SelectBPMOpenFormList";
                cmd = new SqlCommand(query, connection);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@peopleType", peopleType);
                cmd.Parameters.AddWithValue("@peopleID", CreatedBy);
                cmd.Parameters.AddWithValue("@peopleInfo", "All People");

                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DashBPMWipOrClosedTab_Model tempobj = new DashBPMWipOrClosedTab_Model();
                    tempobj.BPMID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.ThrdRef = sdr["workFlowName"].ToString();
                    //tempobj.threadID = Convert.ToInt32(sdr["TotalDays"].ToString());
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();

                    tempobj.stageDays = Convert.ToInt64(sdr["stageDays"].ToString());
                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.stageOrderId = Convert.ToInt64(sdr["stageOrderID"].ToString());
                    tempobj.BPM_Name = sdr["workFlowName"].ToString();
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();

                    objModel.Add(tempobj);
                }
                sdr.Close();

                connection.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Dashboard_Repository/GetBPMOpenFormList");
            }

            return objModel;
        }



        public Dashboard_Widget GetDashboardWidgetData(long SubCompany, long Domain, long AdminId)
        {
            Dashboard_Widget objModel = new Dashboard_Widget();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select count(*) as cnt From DocUpload_table where Sub_CompID=@SubCompID and DomainID=@DomainID and IsDeleted!=@IsDeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompID", SubCompany);
                cmd.Parameters.AddWithValue("@DomainID", Domain);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Document = Convert.ToInt64(sdr["cnt"].ToString());                    
                }
                sdr.Close();


                cmd.Parameters.Clear();
                sqlstr = "Select count(*) as cnt From Event_Table where Sub_CompID=@SubCompID and EventDomain=@DomainID and IsDeleted!=@IsDeleted";
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompID", SubCompany);
                cmd.Parameters.AddWithValue("@DomainID", Domain);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Event = Convert.ToInt64(sdr["cnt"].ToString());
                }
                sdr.Close();


                cmd.Parameters.Clear();
                sqlstr = "Select count(*) as cnt From Announcement_Table where Sub_CompID=@SubCompID and AnnouncemnetDomain=@DomainID and IsDeleted!=@IsDeleted";
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompID", SubCompany);
                cmd.Parameters.AddWithValue("@DomainID", Domain);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Announcement = Convert.ToInt64(sdr["cnt"].ToString());
                }
                sdr.Close();

                connection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, AdminId, "Web API", "Dashboard_Repository/GetDashboardWidgetData");
            }

            return objModel;
        }

    }
}
