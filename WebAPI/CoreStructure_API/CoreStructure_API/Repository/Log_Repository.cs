﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public static class Log_Repository
    {

        public static DBReturnModel InsertLog(string ParentCompId, string SubcompId, string UserId, string OperationType,string Operation_Performed, string Remarks, string IP_Address, long user_id)
        {
            Log_Table_Model objModel = new Log_Table_Model();
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Log!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                objModel.Parent_CompID = ParentCompId;
                objModel.Sub_CompID = SubcompId;
                objModel.UserID = UserId;
                objModel.OperationType = OperationType;
                objModel.Operation_Performed = Operation_Performed;
                objModel.Remarks = Remarks;
                objModel.IP_Address = IP_Address;
                objModel.user_id = user_id;

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertLog";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Parent_CompID", objModel.Parent_CompID);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@UserID", objModel.UserID);
                    cmd.Parameters.AddWithValue("@OperationType", objModel.OperationType);
                    cmd.Parameters.AddWithValue("@Operation_Performed", objModel.Operation_Performed);
                    cmd.Parameters.AddWithValue("@Ramarks", objModel.Remarks);

                    cmd.Parameters.AddWithValue("@IP_Address", objModel.IP_Address);
                    cmd.Parameters.AddWithValue("@user_id", objModel.user_id);
                    cmd.Parameters.AddWithValue("@OperationON", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.user_id), "API", "Log_Repository/InsertLog");
                    objReturn.ReturnMessage = "System Error on Inserting Log Table!";
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.user_id), "API", "Log_Repository/InsertLog");
            }
            return objReturn;
        }
    }
}
