﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Employee_Repository
    {


        public string InsertEmployee(Employee_table_Model objModel)
        {
            string result = "Error on Inserting Employee!";
            try
            {
                string empPicName = SaveImage(objModel.EmpPhoto);
                if (empPicName != "")
                {
                    objModel.EmpPhoto = empPicName;
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertEmployee";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmpId", objModel.EmpId);
                    cmd.Parameters.AddWithValue("@User_type", objModel.User_type);
                    cmd.Parameters.AddWithValue("@EmployeeName", objModel.EmployeeName);
                    cmd.Parameters.AddWithValue("@Designation", objModel.Designation);
                    cmd.Parameters.AddWithValue("@Sub_Company", objModel.Sub_Company);
                    cmd.Parameters.AddWithValue("@Department", objModel.Department);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("@Location", objModel.Location);
                    cmd.Parameters.AddWithValue("@Reporting_Manager_Id", objModel.Reporting_Manager_Id);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Contact_No);
                    cmd.Parameters.AddWithValue("@Email", objModel.Email);
                    cmd.Parameters.AddWithValue("@CountryCode", objModel.CountryCode);
                    cmd.Parameters.AddWithValue("@Doj", objModel.Doj);
                    cmd.Parameters.AddWithValue("@Dob", objModel.Dob);
                    cmd.Parameters.AddWithValue("@Userid", objModel.Userid);
                    cmd.Parameters.AddWithValue("@Password", objModel.Password);
                    cmd.Parameters.AddWithValue("@isManager", objModel.isManager);
                    cmd.Parameters.AddWithValue("@isUserFp", objModel.isUserFp);
                    cmd.Parameters.AddWithValue("@EmpPhoto", objModel.EmpPhoto);
                    cmd.Parameters.AddWithValue("@Gender", objModel.Gender);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertEmployee");
            }

            return result;
        }



        public Employee_table_Model GetOneEmployee(long SLNO, long CreatedBy)
        {
            Employee_table_Model objModel = new Employee_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectOneEmployee";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SubCompanyName = sdr["SubCompName"].ToString();
                    objModel.DomainName = sdr["DomainName"].ToString();

                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.EmpId = sdr["EmpId"].ToString();
                    objModel.User_type = sdr["User_type"].ToString();
                    objModel.EmployeeName = sdr["EmployeeName"].ToString();
                    objModel.Designation = sdr["Designation"].ToString();
                    objModel.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    objModel.Department = sdr["Department"].ToString();
                    objModel.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    objModel.Location = sdr["Location"].ToString();
                    objModel.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    objModel.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    objModel.Email = sdr["Email"].ToString();
                    objModel.CountryCode = sdr["CountryCode"].ToString();
                    objModel.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    objModel.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    objModel.Userid = sdr["Userid"].ToString();
                    objModel.EmpPhoto = "Content/CustomImages/Employee/" + sdr["EmpPhoto"].ToString();
                    objModel.Gender = sdr["Gender"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());

                    var path = Path.Combine("", objModel.EmpPhoto);
                    objModel.EmpPhoto = GlobalFunction.getImageURL() + "/api/Employee/GetEmployeeImage?path=" + path;

                    objModel.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    objModel.isManager = Convert.ToBoolean(sdr["isManager"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetOneEmployee");
            }

            return objModel;

        }



        public string UpdateEmployee(Employee_table_Model objModel)
        {
            string result = "Error on Updating Employee!";
            try
            {
                string empPicName = SaveImage(objModel.EmpPhoto);
                if (empPicName != "")
                {
                    objModel.EmpPhoto = empPicName;
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateEmployee";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@EmpId", objModel.EmpId);
                    cmd.Parameters.AddWithValue("@User_type", objModel.User_type);
                    cmd.Parameters.AddWithValue("@EmployeeName", objModel.EmployeeName);
                    cmd.Parameters.AddWithValue("@Designation", objModel.Designation);
                    cmd.Parameters.AddWithValue("@Sub_Company", objModel.Sub_Company);
                    cmd.Parameters.AddWithValue("@Department", objModel.Department);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("@Location", objModel.Location);
                    cmd.Parameters.AddWithValue("@Reporting_Manager_Id", objModel.Reporting_Manager_Id);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Contact_No);
                    cmd.Parameters.AddWithValue("@Email", objModel.Email);
                    cmd.Parameters.AddWithValue("@CountryCode", objModel.CountryCode);
                    cmd.Parameters.AddWithValue("@Doj", objModel.Doj);
                    cmd.Parameters.AddWithValue("@Dob", objModel.Dob);
                    cmd.Parameters.AddWithValue("@Userid", objModel.Userid);
                    cmd.Parameters.AddWithValue("@Password", objModel.Password);
                    cmd.Parameters.AddWithValue("@isManager", objModel.isManager);
                    cmd.Parameters.AddWithValue("@isUserFp", objModel.isUserFp);
                    cmd.Parameters.AddWithValue("@EmpPhoto", objModel.EmpPhoto);
                    cmd.Parameters.AddWithValue("@Gender", objModel.Gender);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployee");
            }

            return result;
        }



        public string DisableEmployee(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Employee!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "CB_SP_EnableDisableEmployee";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Employee_Repository/DisableEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Employee_Repository/DisableEmployee");
            }

            return result;
        }



        public string EnableEmployee(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Employee!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "CB_SP_EnableDisableEmployee";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@UpdatedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Employee_Repository/EnableEmployee");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Employee_Repository/EnableEmployee");
            }

            return result;
        }



        public Employee_table_page_Model GetEmployeeListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Employee_table_page_Model objModel = new Employee_table_page_Model();
            List<Employee_table_Model> objList = new List<Employee_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectEmployeeByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Employee_table_Model tempobj = new Employee_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.EmpId = sdr["EmpId"].ToString();
                    tempobj.User_type = sdr["User_type"].ToString();
                    tempobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempobj.Designation = sdr["Designation"].ToString();
                    tempobj.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    tempobj.Department = sdr["Department"].ToString();
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Location = sdr["Location"].ToString();
                    tempobj.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    tempobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempobj.Email = sdr["Email"].ToString();
                    tempobj.CountryCode = sdr["CountryCode"].ToString();
                    tempobj.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    tempobj.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    tempobj.Userid = sdr["Userid"].ToString();
                    tempobj.EmpPhoto = sdr["EmpPhoto"].ToString();
                    tempobj.Gender = sdr["Gender"].ToString();
                    if (!string.IsNullOrEmpty(sdr["CreatedBy"].ToString())) tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Employee_table where User_type in ('User') and (Isnull(EmpId,'') like '%'+@search+'%'     "
                + " or Isnull(User_type,'') like '%'+@search+'%'"
                + " or Isnull(EmployeeName,'') like '%'+@search+'%' or Isnull(Designation,'') like '%'+@search+'%' "
                + " or Isnull(Sub_Company,'') like '%'+@search+'%' or Isnull(Department,'') like '%'+@search+'%' "
                + " or Isnull(Domain_ID,'') like '%'+@search+'%' or Isnull(Location,'') like '%'+@search+'%' "
                + " or Isnull(Reporting_Manager_Id,'') like '%'+@search+'%' or Isnull(Reporting_Manager_Id,'') like '%'+@search+'%' "
                + " or Isnull(Contact_No,0) like '%'+@search+'%' or Isnull(Email,'') like '%'+@search+'%' "
                + " or Isnull(CountryCode,'') like '%'+@search+'%' or Isnull(Userid,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Employee_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetEmployeeListByPage");
            }

            return objModel;

        }



        private string SaveImage(string EmpPhoto)
        {
            string result = "";

            try
            {
                var guid = Guid.NewGuid().ToString();

                byte[] imageBytes = Convert.FromBase64String(EmpPhoto);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);


                string newFile = Guid.NewGuid().ToString() + ".png";
                string filepath = Path.Combine("" + "/Content/CustomImages/Employee", newFile);
                //string filePath = Path.Combine(Microsoft.AspNetCore.Server.MapPath("~/Assets/") + Request.QueryString["id"] + "/", newFile);
                image.Save(filepath, ImageFormat.Png);
                result = newFile;
            }
            catch (Exception ex)
            {
                //result = ex.Message;
                ErrorHandler.LogError(ex, 0, "Web API", "Employee_Repository/SaveImage");
            }

            return result;




        }







        public string InsertApproverLimitation(Employee_Process_value_list_Model objModel)
        {
            string result = "Error on Inserting Approver Limitation!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertApproverLimitation";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Employee_Process_value_Type";
                    dataTable.Columns.Add("UserID", typeof(long));
                    dataTable.Columns.Add("MaxValue", typeof(long));
                    dataTable.Columns.Add("MinValue", typeof(long));
                    dataTable.Columns.Add("IsDeleted", typeof(long));
                    dataTable.Columns.Add("isApprover", typeof(long));
                    foreach (var dta in objModel.Employee_Process_value_list)
                    {
                        dataTable.Rows.Add(dta.UserID, dta.MaxValue, dta.MinValue, false, false); // Id of '1' is valid for the Person table

                    }
                    SqlParameter parameter = new SqlParameter("ApproveLimit", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();

                    result = returnCode.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertApproverLimitation");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Employee_Repository/InsertApproverLimitation");
            }

            return result;
        }


        public List<Employee_Process_value_Model> GetEmployeeListForApproverLimitation(long CreatedBy, long SubCompId, long DomainId)
        {
            List<Employee_Process_value_Model> objModel = new List<Employee_Process_value_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                //string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName,Sub_Company, Domain_ID,Reporting_Manager_Id, "
                //+ " Isnull((Select EmpId From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_ID, "
                //+ " Isnull((Select EmployeeName From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_Name, "
                //+ " Isnull((Select Domain_ID From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_Domain, "
                //+ " Isnull(EmpProc.MinValue, 0) as MinValue, Isnull(EmpProc.MaxValue, 0) as MaxValue, Isnull(EmpProc.UserID, 0) as UserID "
                //+ " From Employee_table Emp Left outer join Employee_Process_value EmpProc on Emp.SLNO = EmpProc.SLNO where Sub_Company = @Sub_Company and Domain_ID = @DomainID";

                string sqlstr = "Select Emp.SLNO, EmpId, EmployeeName,Sub_Company, Domain_ID,Reporting_Manager_Id, "
                + " Isnull((Select EmpId From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_ID, "
                + " Isnull((Select EmployeeName From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_Name, "
                + " Isnull((Select Domain_ID From Employee_table where Cast(Employee_table.SLNO as varchar(100)) = Emp.Reporting_Manager_Id),'') as Manager_Domain, "
                + " Isnull(EmpProc.MinValue, 0) as MinValue, Isnull(EmpProc.MaxValue, 0) as MaxValue, Isnull(EmpProc.UserID, 0) as UserID "
                + " From Employee_table Emp Left outer join Employee_Process_value EmpProc on Emp.SLNO = EmpProc.USERID where" +
                " Sub_Company = @Sub_Company and Domain_ID = @DomainID and Emp.User_type='User'";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_Company", SubCompId);
                cmd.Parameters.AddWithValue("@DomainID", DomainId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Employee_Process_value_Model tempobj = new Employee_Process_value_Model();
                    tempobj.UserID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Employee_Company_Name = sdr["Sub_Company"].ToString();
                    tempobj.Employee_ID = sdr["EmpId"].ToString();
                    tempobj.Employee_Name = sdr["EmployeeName"].ToString();
                    tempobj.Employee_Company_Name = sdr["Sub_Company"].ToString();
                    tempobj.Employee_Domain = sdr["Domain_ID"].ToString();
                    tempobj.Manager_ID = sdr["Reporting_Manager_Id"].ToString();
                    tempobj.Manager_ID = sdr["Manager_ID"].ToString();
                    tempobj.Manager_Name = sdr["Manager_Name"].ToString();
                    tempobj.Manager_Domain = sdr["Manager_Domain"].ToString();

                    tempobj.MinValue = Convert.ToInt32(sdr["MinValue"].ToString());
                    tempobj.MaxValue = Convert.ToInt32(sdr["MaxValue"].ToString());

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetEmployeeListForApproverLimitation");
            }

            return objModel;

        }





        public List<Employee_table_Model> GetAllEmployee(long CreatedBy)
        {
            List<Employee_table_Model> objModel = new List<Employee_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, isnull(EmpId,'') as EmpId, isnull(User_type,'') as User_type, isnull(EmployeeName,'') as EmployeeName, "
                + " isnull(Designation, '') as Designation, isnull(Sub_Company, '') as Sub_Company, isnull(Department, '') as Department, "
                + " isnull(Domain_ID, '') as Domain_ID, isnull(Location, '') as Location, isnull(Reporting_Manager_Id, '') as Reporting_Manager_Id, "
                + " isnull(Contact_No, 0) as Contact_No, isnull(Email, '') as Email, isnull(CountryCode, '') as CountryCode, "
                + " isnull(Doj, '') as Doj, isnull(Dob, '') as Dob, isnull(Userid, '') as Userid,Password,isnull(IsDeleted, '') as IsDeleted, "
                + " isnull(isManager, '') as isManager, isnull(isUserFp, '') as isUserFp, "
                + " CreatedDate,CreatedBy,isnull(EmpPhoto, '') as EmpPhoto,isnull(Gender, '') as Gender "
                + " From Employee_table  where Isnull(user_type,'')=@UserType and Isnull(IsDeleted,'')=@IsDeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                cmd.Parameters.AddWithValue("@UserType", "User");
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Employee_table_Model tempobj = new Employee_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.EmpId = sdr["EmpId"].ToString();
                    tempobj.User_type = sdr["User_type"].ToString();
                    tempobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempobj.Designation = sdr["Designation"].ToString();
                    tempobj.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    tempobj.Department = sdr["Department"].ToString();
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Location = sdr["Location"].ToString();
                    tempobj.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    tempobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempobj.Email = sdr["Email"].ToString();
                    tempobj.CountryCode = sdr["CountryCode"].ToString();
                    tempobj.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    tempobj.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    tempobj.Userid = sdr["Userid"].ToString();
                    tempobj.EmpPhoto = "Content/CustomImages/Employee/" + sdr["EmpPhoto"].ToString();
                    tempobj.Gender = sdr["Gender"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());

                    var path = Path.Combine("", tempobj.EmpPhoto);
                    tempobj.EmpPhoto = GlobalFunction.getImageURL() + "/api/Employee/GetEmployeeImage?path=" + path;

                    tempobj.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    tempobj.isManager = Convert.ToBoolean(sdr["isManager"].ToString());

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetAllEmployee");
            }

            return objModel;

        }


        public DBReturnModel UpdateEmployeeFromUserProfile(UserProfile_Model objModel)
        {
            DBReturnModel objRetModel = new DBReturnModel();
            objRetModel.ReturnStatus = "ERROR";
            string result = "Error on Updating Employee!";
            objRetModel.ReturnMessage = result;
            try
            {
                string empPicName = SaveImage(objModel.EmpPhoto);
                if (empPicName != "")
                {
                    objModel.EmpPhoto = empPicName;
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateEmployeeFromProfile";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Designation", objModel.Designation);
                    cmd.Parameters.AddWithValue("@Location", objModel.Location);
                    cmd.Parameters.AddWithValue("@Dob", objModel.Dob);
                    cmd.Parameters.AddWithValue("@EmpPhoto", objModel.EmpPhoto);
                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objRetModel.ReturnMessage = returnCode.Value.ToString();
                    objRetModel.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployeeFromUserProfile");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployeeFromUserProfile");
            }

            return objRetModel;
        }


        public string UpdateEmployeeUserPassword(ResetPassword_Model objModel)
        {
            DBReturnModel objRetModel = new DBReturnModel();
            objRetModel.ReturnStatus = "ERROR";
            string result = "Error on Updating User Password!";
            objRetModel.ReturnMessage = result;
            objModel.Current_Password = EncryptDecrypt.Encrypt(objModel.Current_Password);
            objModel.New_Password = EncryptDecrypt.Encrypt(objModel.New_Password);
            objModel.Confirm_Password = EncryptDecrypt.Encrypt(objModel.Confirm_Password);
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "Sp_ChangePassword";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@CurrentPassword", objModel.Current_Password);
                    cmd.Parameters.AddWithValue("@NewPassword", objModel.New_Password);
                    cmd.Parameters.AddWithValue("@ConfirmPassword", objModel.Confirm_Password);
                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    //var returnStatus = new SqlParameter();
                    //returnStatus.ParameterName = "@retStatus";
                    //returnStatus.SqlDbType = SqlDbType.VarChar;
                    //returnStatus.Size = 50;
                    //returnStatus.Direction = ParameterDirection.Output;
                    //returnStatus.Value = "";
                    //cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    //objRetModel.ReturnMessage = returnCode.Value.ToString();
                    result= returnCode.Value.ToString();
                    //objRetModel.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployeeUserPassword");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.UpdatedBy, "API", "Employee_Repository/UpdateEmployeeUserPassword");
            }

            return result;
        }


        public User_Delegation_Table_Model GetOneUserDelegation(long SLNO, long CreatedBy)
        {
            User_Delegation_Table_Model objModel = new User_Delegation_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, parentCoy, subCoy , FD, delegatedBy, del_from, del_to, delegatedTo, comment, IsEnabled From User_Delegation_Table "
                    + " where delegatedBy = @delegatedBy ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@delegatedBy", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.parentCoy = Convert.ToInt64(sdr["parentCoy"].ToString());
                    objModel.subCoy = Convert.ToInt64(sdr["subCoy"].ToString());
                    objModel.FD = Convert.ToInt64(sdr["FD"].ToString());
                    objModel.delegatedBy = Convert.ToInt64(sdr["delegatedBy"].ToString());
                    objModel.del_from = sdr["del_from"].ToString();
                    objModel.del_to = sdr["del_to"].ToString();
                    objModel.delegatedTo = Convert.ToInt64(sdr["delegatedTo"].ToString());
                    objModel.comment = sdr["comment"].ToString();
                    objModel.IsEnabled = Convert.ToBoolean(sdr["IsEnabled"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetOneUserDelegation");
            }

            return objModel;

        }




        public DBReturnModel UpdateUserDelegate(User_Delegation_Table_Model objModel, long UpdatedBy)
        {
            DBReturnModel objRetModel = new DBReturnModel();
            objRetModel.ReturnStatus = "ERROR";
            string result = "Error on Updating User Delegate!";
            objRetModel.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateUserDelegate";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@parentCoy", objModel.parentCoy);
                    cmd.Parameters.AddWithValue("@subCoy", objModel.subCoy);
                    cmd.Parameters.AddWithValue("@FD", objModel.FD);
                    cmd.Parameters.AddWithValue("@delegatedBy", objModel.delegatedBy);
                    cmd.Parameters.AddWithValue("@del_from", objModel.del_from);
                    cmd.Parameters.AddWithValue("@del_to", objModel.del_to);
                    cmd.Parameters.AddWithValue("@delegatedTo", objModel.delegatedTo);
                    cmd.Parameters.AddWithValue("@comment", objModel.comment);
                    cmd.Parameters.AddWithValue("@IsEnabled", objModel.IsEnabled);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objRetModel.ReturnMessage = returnCode.Value.ToString();
                    objRetModel.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, UpdatedBy, "API", "Employee_Repository/UpdateUserDelegate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UpdatedBy, "API", "Employee_Repository/UpdateUserDelegate");
            }

            return objRetModel;
        }


        public DBReturnModel EnableDisableUserDelegate(User_Delegation_Table_Model objModel, long UpdatedBy)
        {
            DBReturnModel objRetModel = new DBReturnModel();
            objRetModel.ReturnStatus = "ERROR";
            string result = "Error on Updating User Delegate!";
            objRetModel.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_EnableDisableUserDelegate";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@delegatedBy", objModel.delegatedBy);
                    cmd.Parameters.AddWithValue("@IsEnabled", objModel.IsEnabled);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objRetModel.ReturnMessage = returnCode.Value.ToString();
                    objRetModel.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, UpdatedBy, "API", "Employee_Repository/EnableDisableUserDelegate");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UpdatedBy, "API", "Employee_Repository/EnableDisableUserDelegate");
            }

            return objRetModel;
        }



        public List<Employees_Reporting> GetEmployeeReportingList(long CreatedBy, string empnm)
        {
            List<Employees_Reporting> objModel = new List<Employees_Reporting>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select 1 as Level, SLNO,EmpId,EmployeeName, Userid, Reporting_Manager_Id, isManager from Employee_table "
                    + " where User_type='User' and isnull(isDeleted,'')!=@IsDeleted";

                if (empnm == null)
                {
                    empnm = "";
                }

                if (empnm != "")
                {
                    sqlstr = sqlstr + " and EmployeeName like @EmployeeName";
                }
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                if (empnm != "")
                {
                    cmd.Parameters.AddWithValue("@EmployeeName", "%" + empnm + "%");
                }
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Employees_Reporting tempobj = new Employees_Reporting();
                    tempobj.Employee_ID = sdr["SLNO"].ToString();
                    tempobj.Employee_Name = sdr["EmployeeName"].ToString();
                    tempobj.ReportTo = sdr["Reporting_Manager_Id"].ToString();

                    if (tempobj.ReportTo == "0")
                    {
                        tempobj.ReportTo = tempobj.Employee_ID;
                    }


                    if (Convert.ToBoolean(sdr["isManager"].ToString()) == true)
                    {
                        tempobj.Title = "MANAGER";
                    }
                    else
                    {
                        tempobj.Title = "User";
                    }

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetEmployeeReportingList");
            }

            return objModel;
        }





        public Employee_table_Model GetOneEmployeeFromEmail(string Email, long CreatedBy)
        {
            Employee_table_Model objModel = new Employee_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, isnull(EmpId,'') as EmpId, isnull(User_type,'') as User_type, isnull(EmployeeName,'') as EmployeeName, "
                + " isnull(Designation, '') as Designation, isnull(Sub_Company, '') as Sub_Company, isnull(Department, '') as Department, "
                + " isnull(Domain_ID, '') as Domain_ID, isnull(Location, '') as Location, isnull(Reporting_Manager_Id, '') as Reporting_Manager_Id, "
                + " isnull(Contact_No, 0) as Contact_No, isnull(Email, '') as Email, isnull(CountryCode, '') as CountryCode, "
                + " isnull(Doj, '') as Doj, isnull(Dob, '') as Dob, isnull(Userid, '') as Userid,Password,isnull(IsDeleted, '') as IsDeleted, "
                + " isnull(isManager, '') as isManager, isnull(isUserFp, '') as isUserFp, "
                + " CreatedDate,CreatedBy,isnull(EmpPhoto, '') as EmpPhoto,isnull(Gender, '') as Gender, "
                + " (Select SubCompanyName from Sub_Company_table where SLNO=Employee_table.Sub_Company ) as SubCompName,"
                + " (Select Domain_Name from Domain_table where SLNO=Employee_table.Domain_ID ) as DomainName "
                + " From Employee_table where Email=@Email ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SubCompanyName = sdr["SubCompName"].ToString();
                    objModel.DomainName = sdr["DomainName"].ToString();

                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.EmpId = sdr["EmpId"].ToString();
                    objModel.User_type = sdr["User_type"].ToString();
                    objModel.EmployeeName = sdr["EmployeeName"].ToString();
                    objModel.Designation = sdr["Designation"].ToString();
                    objModel.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    objModel.Department = sdr["Department"].ToString();
                    objModel.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    objModel.Location = sdr["Location"].ToString();
                    objModel.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    objModel.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    objModel.Email = sdr["Email"].ToString();
                    objModel.CountryCode = sdr["CountryCode"].ToString();
                    objModel.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    objModel.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    objModel.Userid = sdr["Userid"].ToString();
                    objModel.EmpPhoto = "Content/CustomImages/Employee/" + sdr["EmpPhoto"].ToString();
                    objModel.Gender = sdr["Gender"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());

                    var path = Path.Combine("", objModel.EmpPhoto);
                    objModel.EmpPhoto = GlobalFunction.getImageURL() + "/api/Employee/GetEmployeeImage?path=" + path;

                    objModel.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    objModel.isManager = Convert.ToBoolean(sdr["isManager"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/GetOneEmployeeFromEmail");
            }

            return objModel;

        }




        public DBReturnModel InsertEmployeeFromBulk(List<Employee_table_Model> objModel, long CreatedBy)
        {
            string result = "Error on Inserting Employee!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                //objModel = NullToBlank(objModel);
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";
                    sqlstr = "CB_SP_InsertEmployeeBulk";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Employee_table_Type";
                    dataTable.Columns.Add("EmpId", typeof(string));
                    dataTable.Columns.Add("EmployeeName", typeof(string));
                    dataTable.Columns.Add("Gender", typeof(string));
                    dataTable.Columns.Add("Designation", typeof(string));
                    dataTable.Columns.Add("SubCompanyName", typeof(string));
                    dataTable.Columns.Add("Department", typeof(string));
                    dataTable.Columns.Add("Location", typeof(string));
                    dataTable.Columns.Add("Reporting_Manager_Id", typeof(string));
                    dataTable.Columns.Add("CountryCode", typeof(string));
                    dataTable.Columns.Add("Contact_No", typeof(long));
                    dataTable.Columns.Add("Email", typeof(string));
                    dataTable.Columns.Add("Doj", typeof(DateTime));
                    dataTable.Columns.Add("Dob", typeof(DateTime));
                    foreach (var dta in objModel)
                    {
                        dataTable.Rows.Add(dta.EmpId, dta.EmployeeName, dta.Gender, dta.Designation,
                            dta.SubCompanyName, dta.Department, dta.Location, dta.Reporting_Manager_Id, dta.CountryCode,
                            dta.Contact_No,dta.Email,dta.Doj,dta.Dob); // Id of '1' is valid for the Person table
                    }
                    SqlParameter parameter = new SqlParameter("@Employee_table_Type", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());
                    var returncode = new SqlParameter();
                    returncode.ParameterName = "ret";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/InsertEmployeeFromBulk");
                    objreturn.ReturnMessage = "System Error on Inserting Employee";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Employee_Repository/InsertEmployeeFromBulk");
            }

            return objreturn;
        }



    }
}
