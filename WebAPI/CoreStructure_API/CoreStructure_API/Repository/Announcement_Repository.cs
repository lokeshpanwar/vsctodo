﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using static CoreStructure_API.Utility.Standard;
using System.Data;
using System.Data.SqlClient;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class Announcement_Repository
    {
        public DBReturnModel InsertAnnouncement(AnnouncementTable_Model objModel)
        {
          
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnMessage = "Error on Inserting Announcement";
            objreturn.ReturnStatus = "Error";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                


                try
                {
                    string sqlstr = "CB_SP_InsertAnnouncement";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@AnnouncementTitle", objModel.AnnouncementTitle);
                    cmd.Parameters.AddWithValue("@AnnouncementDetail", objModel.AnnouncementDetail);
                    cmd.Parameters.AddWithValue("@ExpiryDate", objModel.ExpiryDate);
                    cmd.Parameters.AddWithValue("@AnnouncemnetDomain", objModel.AnnouncemnetDomain);
                    cmd.Parameters.AddWithValue("@CreatedOn",StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);

                    
                    
                    var retstatus = new SqlParameter();
                    retstatus.ParameterName = "retStatus";
                    retstatus.Size = 50;
                    retstatus.SqlDbType = SqlDbType.VarChar;
                    retstatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retstatus);


                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.Size = 500;
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    objreturn.ReturnStatus = retstatus.Value.ToString();
                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    connection.Close();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "AnnouncementTable_Repository/InsertAnnouncement");
                    objreturn.ReturnMessage = "System Error on Inserting Announcement";
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex,objModel.CreatedBy,"API", "AnnouncementTable_Repository/InsertAnnouncement");
            }

            return objreturn;
        }


        public AnnouncementTable_Model GetOneAnnouncement(long SLNO, long CreatedBy)
        {
            AnnouncementTable_Model objModel = new AnnouncementTable_Model();
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectAnnouncementById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.AnnouncementTitle = sdr["AnnouncementTitle"].ToString();
                    objModel.AnnouncementDetail = sdr["AnnouncementDetail"].ToString();
                    objModel.ExpiryDate = Convert.ToDateTime(sdr["ExpiryDate"].ToString());
                    objModel.AnnouncemnetDomain = Convert.ToInt64(sdr["AnnouncemnetDomain"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                }

                connection.Close();

            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AnnouncementTable_Repository/GetOneAnnouncement");
            }

            return objModel;
        }


        public DBReturnModel UpdateAnnouncement(AnnouncementTable_Model objModel)
        {

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Error on Updating Announcement";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                cmd.Transaction = connection.BeginTransaction();
                transaction = cmd.Transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "CB_SP_UpdateAnnouncement";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@AnnouncementTitle", objModel.AnnouncementTitle);
                    cmd.Parameters.AddWithValue("@AnnouncementDetail", objModel.AnnouncementDetail);
                    cmd.Parameters.AddWithValue("@ExpiryDate", objModel.ExpiryDate);
                    cmd.Parameters.AddWithValue("@AnnouncemnetDomain", objModel.AnnouncemnetDomain);
                    cmd.Parameters.AddWithValue("@ModifiedOn",StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                   // cmd.Parameters.AddWithValue("@IsDeleted", false);

                    var retmsg = new SqlParameter();
                    retmsg.ParameterName= "retMessage";
                    retmsg.Size = 500;
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);

                    var retstatus = new SqlParameter();
                    retstatus.ParameterName = "retStatus";
                    retstatus.Size = 50;
                    retstatus.SqlDbType = SqlDbType.VarChar;
                    retstatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retstatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnStatus = retstatus.Value.ToString();
                    objreturn.ReturnMessage = retmsg.Value.ToString();

                    connection.Close();

                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "AnnouncementTable_Repository/UpdateAnnouncement");
                    objreturn.ReturnMessage = "System Error on Updating Announcement";
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "AnnouncementTable_Repository/UpdateAnnouncement");
            }

              return  objreturn;
        }


        public DBReturnModel DisableAnnouncement(long SLNO,long DeletedBy)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Error on Disabling Announcement!";

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "update Announcement_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO ";
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = "Announcement Disabled Successfully!";



                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "AnnouncementTable_Repository/DisableAnnouncement");
                    objreturn.ReturnMessage = "System Error on Disabling Announcement!";
                    
                }

            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "AnnouncementTable_Repository/DisableAnnouncement");
            }
            return objreturn;
        }

        public DBReturnModel EnableAnnouncement(long SLNO,long DeletedBy)
        {
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = "Error Enabling on Announcement";

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "update Announcement_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO ";
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted",false);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = "Announcement Enabled Successfully!";
                    connection.Close();
                }

                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "AnnouncementTable_Repository/EnableAnnouncement");
                    objreturn.ReturnMessage = "System Error on Enabling Announcement!";
                }


            }

            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "AnnouncementTable_Repository/EnableAnnouncement");
            }
            return objreturn;
        }



        public AnnouncementTable_page_Model GetAnnouncementByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            AnnouncementTable_page_Model objModel = new AnnouncementTable_page_Model();
            List<AnnouncementTable_Model> objList = new List<AnnouncementTable_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectAnnouncementByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    AnnouncementTable_Model tempobj = new AnnouncementTable_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.AnnouncementTitle = sdr["AnnouncementTitle"].ToString();
                    tempobj.AnnouncementDetail = sdr["AnnouncementDetail"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.CompanyName = sdr["CompanyName"].ToString();
                    tempobj.DomainName = sdr["DomainName"].ToString();
                    tempobj.ExpiryDate = Convert.ToDateTime(sdr["ExpiryDate"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Announcement_Table where (Isnull(AnnouncementTitle,'') like '%'+@search+'%' "
                + " or Isnull(AnnouncementDetail,'') like '%'+@search+'%') ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.AnnouncementTable_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AnnouncementTable_Repository/GetAnnouncementByPage");
            }

            return objModel;

        }




        public AnnouncementTable_page_Model GetAnnouncementBySubCompAndDomain(long SubCompID, long DomainID, long CreatedBy)
        {
            AnnouncementTable_page_Model objModel = new AnnouncementTable_page_Model();
            List<AnnouncementTable_Model> objList = new List<AnnouncementTable_Model>();
            
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, Sub_CompID, Isnull(AnnouncementTitle,'') as AnnouncementTitle, "
                    + " isnull(AnnouncementDetail,'') as AnnouncementDetail,     CreatedBy,CreatedOn,ExpiryDate,  "
                    + " isnull(DeletedBy, 0) as DeletedBy,isnull(AnnouncemnetDomain, 0) as AnnouncemnetDomain,   "
                    + " isnull(IsDeleted, 0) as IsDeleted, isnull(ModifiedBy, 0) as ModifiedBy, isnull(ModifiedOn, '') as ModifiedOn , "
                    + " CompanyName, DomainName From Vw_AnnouncementTable where Sub_CompID=@Sub_CompID and AnnouncemnetDomain=@AnnouncemnetDomain"
                    + " and IsDeleted!=@IsDeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.Parameters.AddWithValue("@AnnouncemnetDomain", DomainID);
                cmd.Parameters.AddWithValue("@Sub_CompID", SubCompID);

                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    AnnouncementTable_Model tempobj = new AnnouncementTable_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.AnnouncementTitle = sdr["AnnouncementTitle"].ToString();
                    tempobj.AnnouncementDetail = sdr["AnnouncementDetail"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.CompanyName = sdr["CompanyName"].ToString();
                    tempobj.DomainName = sdr["DomainName"].ToString();
                    tempobj.ExpiryDate = Convert.ToDateTime(sdr["ExpiryDate"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
                objModel.AnnouncementTable_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "AnnouncementTable_Repository/GetAnnouncementBySubCompAndDomain");
            }

            return objModel;

        }


    }


}
