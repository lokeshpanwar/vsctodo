﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Vendor_Repository
    {

        public string InsertVendor(Vendor_Mapper_Model objModel)
        {
            string result = "Error on Inserting Vendor!";
            try
            {
                string empPicName = SaveImage(objModel.Employee_table_Model.EmpPhoto);
                if (empPicName != "")
                {
                    objModel.Employee_table_Model.EmpPhoto = empPicName;
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertVendor";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmpId", objModel.Employee_table_Model.EmpId);
                    cmd.Parameters.AddWithValue("@User_type", objModel.Employee_table_Model.User_type);
                    cmd.Parameters.AddWithValue("@EmployeeName", objModel.Employee_table_Model.EmployeeName);
                    cmd.Parameters.AddWithValue("@Designation", objModel.Employee_table_Model.Designation);
                    cmd.Parameters.AddWithValue("@Sub_Company", objModel.Employee_table_Model.Sub_Company);
                    cmd.Parameters.AddWithValue("@Department", objModel.Employee_table_Model.Department);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Employee_table_Model.Domain_ID);
                    cmd.Parameters.AddWithValue("@Location", objModel.Employee_table_Model.Location);
                    cmd.Parameters.AddWithValue("@Reporting_Manager_Id", objModel.Employee_table_Model.Reporting_Manager_Id);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Employee_table_Model.Contact_No);
                    cmd.Parameters.AddWithValue("@Email", objModel.Employee_table_Model.Email);
                    cmd.Parameters.AddWithValue("@CountryCode", objModel.Employee_table_Model.CountryCode);
                    cmd.Parameters.AddWithValue("@Doj", objModel.Employee_table_Model.Doj);
                    cmd.Parameters.AddWithValue("@Dob", objModel.Employee_table_Model.Dob);
                    cmd.Parameters.AddWithValue("@Userid", objModel.Employee_table_Model.Userid);
                    cmd.Parameters.AddWithValue("@Password", objModel.Employee_table_Model.Password);
                    cmd.Parameters.AddWithValue("@isManager", objModel.Employee_table_Model.isManager);
                    cmd.Parameters.AddWithValue("@isUserFp", objModel.Employee_table_Model.isUserFp);
                    cmd.Parameters.AddWithValue("@EmpPhoto", objModel.Employee_table_Model.EmpPhoto);
                    cmd.Parameters.AddWithValue("@Gender", objModel.Employee_table_Model.Gender);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.Employee_table_Model.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("@CompanyName", objModel.ven_Table_Model.CompanyName);
                    cmd.Parameters.AddWithValue("@Url", objModel.ven_Table_Model.Url);
                    cmd.Parameters.AddWithValue("@Address", objModel.ven_Table_Model.Address);
                    cmd.Parameters.AddWithValue("@LegalStatus", objModel.ven_Table_Model.LegalStatus);
                    cmd.Parameters.AddWithValue("@Remarks", objModel.ven_Table_Model.Remarks);
                    cmd.Parameters.AddWithValue("@BCountry", objModel.ven_Table_Model.BCountry);
                    cmd.Parameters.AddWithValue("@SwiftCode", objModel.ven_Table_Model.SwiftCode);
                    cmd.Parameters.AddWithValue("@ExtUserBankAcNo", objModel.ven_Table_Model.ExtUserBankAcNo);
                    cmd.Parameters.AddWithValue("@AcHolderName", objModel.ven_Table_Model.AcHolderName);
                    cmd.Parameters.AddWithValue("@IBANNO", objModel.ven_Table_Model.IBANNO);
                    cmd.Parameters.AddWithValue("@BankName", objModel.ven_Table_Model.BankName);
                    cmd.Parameters.AddWithValue("@IFSC_Code", objModel.ven_Table_Model.IFSC_Code);
                    cmd.Parameters.AddWithValue("@BANK_BRANCH", objModel.ven_Table_Model.BANK_BRANCH);
                    cmd.Parameters.AddWithValue("@HOUSE_No_BANK", objModel.ven_Table_Model.HOUSE_No_BANK);
                    cmd.Parameters.AddWithValue("@STREET_NAME", objModel.ven_Table_Model.STREET_NAME);
                    cmd.Parameters.AddWithValue("@CITY_NAME", objModel.ven_Table_Model.CITY_NAME);
                    cmd.Parameters.AddWithValue("@CITY_POSTAL_CODE", objModel.ven_Table_Model.CITY_POSTAL_CODE);
                    cmd.Parameters.AddWithValue("@REGION_STATE", objModel.ven_Table_Model.REGION_STATE);
                    cmd.Parameters.AddWithValue("@IsDashboard", objModel.ven_Table_Model.IsDashboard);
                    cmd.Parameters.AddWithValue("@BankTelNo", objModel.ven_Table_Model.BankTelNo);
                    cmd.Parameters.AddWithValue("@BANKMOBILENo", objModel.ven_Table_Model.BANKMOBILENo);
                    cmd.Parameters.AddWithValue("@BANKFAXNo", objModel.ven_Table_Model.BANKFAXNo);
                    cmd.Parameters.AddWithValue("@BANKEMAIL", objModel.ven_Table_Model.BANKEMAIL);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.Employee_table_Model.CreatedBy, "API", "Vendor_Repository/InsertVendor");
                    result = "System Error on Inserting Vendor!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.Employee_table_Model.CreatedBy, "API", "Vendor_Repository/InsertVendor");
                result = "System Error on Inserting Vendor!";
            }

            return result;
        }

        

        public Vendor_Mapper_Model GetOneVendor(long SLNO, long CreatedBy)
        {
            Vendor_Mapper_Model objModel = new Vendor_Mapper_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select * From Vw_EmployeeVendorDetails where SLNO=@SLNO ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Vendor_Mapper_Model objMapperModel = new Vendor_Mapper_Model();

                    Employee_table_Model tempemoobj = new Employee_table_Model();
                    tempemoobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempemoobj.EmpId = sdr["EmpId"].ToString();
                    tempemoobj.User_type = sdr["User_type"].ToString();
                    tempemoobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempemoobj.Designation = sdr["Designation"].ToString();
                    tempemoobj.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    tempemoobj.Department = sdr["Department"].ToString();
                    tempemoobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempemoobj.Location = sdr["Location"].ToString();
                    if (!string.IsNullOrEmpty(sdr["Reporting_Manager_Id"].ToString())) tempemoobj.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    tempemoobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempemoobj.Email = sdr["Email"].ToString();
                    tempemoobj.CountryCode = sdr["CountryCode"].ToString();
                    tempemoobj.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    if (!string.IsNullOrEmpty(sdr["Dob"].ToString())) tempemoobj.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    tempemoobj.Userid = sdr["Userid"].ToString();
                    tempemoobj.EmpPhoto = sdr["EmpPhoto"].ToString();
                    tempemoobj.Gender = sdr["Gender"].ToString();
                    if (!string.IsNullOrEmpty(sdr["CreatedBy"].ToString())) tempemoobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempemoobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempemoobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    if (!string.IsNullOrEmpty(sdr["isManager"].ToString())) tempemoobj.isManager = Convert.ToBoolean(sdr["isManager"].ToString());
                    if (!string.IsNullOrEmpty(sdr["isUserFp"].ToString())) tempemoobj.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    tempemoobj.Password = sdr["Password"].ToString();

                    if (tempemoobj.EmpPhoto != "")
                    {
                        var path = Path.Combine("", "Content/CustomImages/Employee/" + tempemoobj.EmpPhoto);
                        tempemoobj.EmpPhoto = GlobalFunction.getImageURL() + "/api/Employee/GetEmployeeImage?path=" + path;
                    }
                    objMapperModel.Employee_table_Model = tempemoobj;

                    ven_Table_Model tempvenobj = new ven_Table_Model();
                    tempvenobj.SLNO = Convert.ToInt64(sdr["VenSLNO"].ToString());
                    tempvenobj.User_id = Convert.ToInt64(sdr["User_id"].ToString());
                    tempvenobj.CompanyName = sdr["CompanyName"].ToString();
                    tempvenobj.Url = sdr["Url"].ToString();
                    tempvenobj.Address = sdr["Address"].ToString();
                    tempvenobj.LegalStatus = sdr["LegalStatus"].ToString();
                    tempvenobj.Remarks = sdr["Remarks"].ToString();
                    tempvenobj.BCountry = sdr["BCountry"].ToString();
                    tempvenobj.SwiftCode = sdr["SwiftCode"].ToString();
                    tempvenobj.ExtUserBankAcNo = sdr["ExtUserBankAcNo"].ToString();
                    tempvenobj.AcHolderName = sdr["AcHolderName"].ToString();
                    tempvenobj.IBANNO = sdr["IBANNO"].ToString();
                    tempvenobj.BankName = sdr["BankName"].ToString();
                    tempvenobj.IFSC_Code = sdr["IFSC_Code"].ToString();
                    tempvenobj.BANK_BRANCH = sdr["BANK_BRANCH"].ToString();
                    tempvenobj.HOUSE_No_BANK = sdr["HOUSE_No_BANK"].ToString();
                    tempvenobj.STREET_NAME = sdr["STREET_NAME"].ToString();
                    tempvenobj.CITY_NAME = sdr["CITY_NAME"].ToString();

                    tempvenobj.CITY_POSTAL_CODE = sdr["CITY_POSTAL_CODE"].ToString();
                    tempvenobj.REGION_STATE = sdr["REGION_STATE"].ToString();
                    tempvenobj.IsDashboard = Convert.ToBoolean(sdr["IsDashboard"].ToString());
                    tempvenobj.BankTelNo = sdr["BankTelNo"].ToString();
                    tempvenobj.BANKMOBILENo = sdr["BANKMOBILENo"].ToString();
                    tempvenobj.BANKFAXNo = sdr["BANKFAXNo"].ToString();
                    tempvenobj.BANKEMAIL = sdr["BANKEMAIL"].ToString();


                    objModel.Employee_table_Model = tempemoobj;
                    objModel.ven_Table_Model = tempvenobj;
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Vendor_Repository/GetOneVendor");
            }

            return objModel;

        }



        public string UpdateVendor(Vendor_Mapper_Model objModel)
        {
            string result = "Error on Updating Vendor!";
            try
            {
                string empPicName = SaveImage(objModel.Employee_table_Model.EmpPhoto);
                if (empPicName != "")
                {
                    objModel.Employee_table_Model.EmpPhoto = empPicName;
                }

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateVendor";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.Employee_table_Model.SLNO);
                    cmd.Parameters.AddWithValue("@EmpId", objModel.Employee_table_Model.EmpId);
                    cmd.Parameters.AddWithValue("@User_type", objModel.Employee_table_Model.User_type);
                    cmd.Parameters.AddWithValue("@EmployeeName", objModel.Employee_table_Model.EmployeeName);
                    cmd.Parameters.AddWithValue("@Designation", objModel.Employee_table_Model.Designation);
                    cmd.Parameters.AddWithValue("@Sub_Company", objModel.Employee_table_Model.Sub_Company);
                    cmd.Parameters.AddWithValue("@Department", objModel.Employee_table_Model.Department);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Employee_table_Model.Domain_ID);
                    cmd.Parameters.AddWithValue("@Location", objModel.Employee_table_Model.Location);
                    cmd.Parameters.AddWithValue("@Reporting_Manager_Id", objModel.Employee_table_Model.Reporting_Manager_Id);
                    cmd.Parameters.AddWithValue("@Contact_No", objModel.Employee_table_Model.Contact_No);
                    cmd.Parameters.AddWithValue("@Email", objModel.Employee_table_Model.Email);
                    cmd.Parameters.AddWithValue("@CountryCode", objModel.Employee_table_Model.CountryCode);
                    cmd.Parameters.AddWithValue("@Doj", objModel.Employee_table_Model.Doj);
                    cmd.Parameters.AddWithValue("@Dob", objModel.Employee_table_Model.Dob);
                    cmd.Parameters.AddWithValue("@Userid", objModel.Employee_table_Model.Userid);
                    cmd.Parameters.AddWithValue("@Password", objModel.Employee_table_Model.Password);
                    cmd.Parameters.AddWithValue("@isManager", objModel.Employee_table_Model.isManager);
                    cmd.Parameters.AddWithValue("@isUserFp", objModel.Employee_table_Model.isUserFp);
                    cmd.Parameters.AddWithValue("@EmpPhoto", objModel.Employee_table_Model.EmpPhoto);
                    cmd.Parameters.AddWithValue("@Gender", objModel.Employee_table_Model.Gender);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.Employee_table_Model.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("@CompanyName", objModel.ven_Table_Model.CompanyName);
                    cmd.Parameters.AddWithValue("@Url", objModel.ven_Table_Model.Url);
                    cmd.Parameters.AddWithValue("@Address", objModel.ven_Table_Model.Address);
                    cmd.Parameters.AddWithValue("@LegalStatus", objModel.ven_Table_Model.LegalStatus);
                    cmd.Parameters.AddWithValue("@Remarks", objModel.ven_Table_Model.Remarks);
                    cmd.Parameters.AddWithValue("@BCountry", objModel.ven_Table_Model.BCountry);
                    cmd.Parameters.AddWithValue("@SwiftCode", objModel.ven_Table_Model.SwiftCode);
                    cmd.Parameters.AddWithValue("@ExtUserBankAcNo", objModel.ven_Table_Model.ExtUserBankAcNo);
                    cmd.Parameters.AddWithValue("@AcHolderName", objModel.ven_Table_Model.AcHolderName);
                    cmd.Parameters.AddWithValue("@IBANNO", objModel.ven_Table_Model.IBANNO);
                    cmd.Parameters.AddWithValue("@BankName", objModel.ven_Table_Model.BankName);
                    cmd.Parameters.AddWithValue("@IFSC_Code", objModel.ven_Table_Model.IFSC_Code);
                    cmd.Parameters.AddWithValue("@BANK_BRANCH", objModel.ven_Table_Model.BANK_BRANCH);
                    cmd.Parameters.AddWithValue("@HOUSE_No_BANK", objModel.ven_Table_Model.HOUSE_No_BANK);
                    cmd.Parameters.AddWithValue("@STREET_NAME", objModel.ven_Table_Model.STREET_NAME);
                    cmd.Parameters.AddWithValue("@CITY_NAME", objModel.ven_Table_Model.CITY_NAME);
                    cmd.Parameters.AddWithValue("@CITY_POSTAL_CODE", objModel.ven_Table_Model.CITY_POSTAL_CODE);
                    cmd.Parameters.AddWithValue("@REGION_STATE", objModel.ven_Table_Model.REGION_STATE);
                    cmd.Parameters.AddWithValue("@IsDashboard", objModel.ven_Table_Model.IsDashboard);
                    cmd.Parameters.AddWithValue("@BankTelNo", objModel.ven_Table_Model.BankTelNo);
                    cmd.Parameters.AddWithValue("@BANKMOBILENo", objModel.ven_Table_Model.BANKMOBILENo);
                    cmd.Parameters.AddWithValue("@BANKFAXNo", objModel.ven_Table_Model.BANKFAXNo);
                    cmd.Parameters.AddWithValue("@BANKEMAIL", objModel.ven_Table_Model.BANKEMAIL);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.Employee_table_Model.CreatedBy, "API", "Vendor_Repository/UpdatedVendor");
                    result = "System Error on Updating Vendor!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.Employee_table_Model.CreatedBy, "API", "Vendor_Repository/UpdatedVendor");
                result = "System Error on Updating Vendor!";
            }

            return result;
        }




        public ven_Mapper_page_Model GetVendorListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            ven_Mapper_page_Model objModel = new ven_Mapper_page_Model();

            List<Vendor_Mapper_Model> objList = new List<Vendor_Mapper_Model>();

            //ven_Table_Model objVenModel = new ven_Table_Model();
            //Employee_table_Model objEmpModel = new Employee_table_Model();

            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectVendorByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

               

                while (sdr.Read())
                {
                    Vendor_Mapper_Model objMapperModel = new Vendor_Mapper_Model();

                    Employee_table_Model tempemoobj = new Employee_table_Model();
                    tempemoobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempemoobj.EmpId = sdr["EmpId"].ToString();
                    tempemoobj.User_type = sdr["User_type"].ToString();
                    tempemoobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempemoobj.Designation = sdr["Designation"].ToString();
                    if (!string.IsNullOrEmpty(sdr["Sub_Company"].ToString())) tempemoobj.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    tempemoobj.Department = sdr["Department"].ToString();
                    if (!string.IsNullOrEmpty(sdr["Domain_ID"].ToString())) tempemoobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempemoobj.Location = sdr["Location"].ToString();
                    tempemoobj.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    if (!string.IsNullOrEmpty(sdr["Contact_No"].ToString())) tempemoobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempemoobj.Email = sdr["Email"].ToString();
                    tempemoobj.CountryCode = sdr["CountryCode"].ToString();
                    if (!string.IsNullOrEmpty(sdr["Doj"].ToString())) tempemoobj.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    if (!string.IsNullOrEmpty(sdr["Dob"].ToString())) tempemoobj.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    tempemoobj.Userid = sdr["Userid"].ToString();
                    tempemoobj.EmpPhoto = sdr["EmpPhoto"].ToString();
                    tempemoobj.Gender = sdr["Gender"].ToString();
                    if (!string.IsNullOrEmpty(sdr["CreatedBy"].ToString())) tempemoobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempemoobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempemoobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    if (!string.IsNullOrEmpty(sdr["isManager"].ToString())) tempemoobj.isManager = Convert.ToBoolean(sdr["isManager"].ToString());
                    if (!string.IsNullOrEmpty(sdr["isUserFp"].ToString())) tempemoobj.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    objMapperModel.Employee_table_Model = tempemoobj;

                    ven_Table_Model tempvenobj = new ven_Table_Model();
                    tempvenobj.SLNO = Convert.ToInt64(sdr["VenSLNO"].ToString());
                    tempvenobj.User_id = Convert.ToInt64(sdr["User_id"].ToString());
                    tempvenobj.CompanyName = sdr["CompanyName"].ToString();
                    tempvenobj.Url = sdr["Url"].ToString();
                    tempvenobj.Address = sdr["Address"].ToString();
                    tempvenobj.LegalStatus = sdr["LegalStatus"].ToString();
                    tempvenobj.Remarks = sdr["Remarks"].ToString();
                    tempvenobj.BCountry = sdr["BCountry"].ToString();
                    tempvenobj.SwiftCode = sdr["SwiftCode"].ToString();
                    tempvenobj.ExtUserBankAcNo = sdr["ExtUserBankAcNo"].ToString();
                    tempvenobj.AcHolderName = sdr["AcHolderName"].ToString();
                    tempvenobj.IBANNO = sdr["IBANNO"].ToString();
                    tempvenobj.BankName = sdr["BankName"].ToString();
                    tempvenobj.IFSC_Code = sdr["IFSC_Code"].ToString();
                    tempvenobj.BANK_BRANCH = sdr["BANK_BRANCH"].ToString();
                    tempvenobj.HOUSE_No_BANK = sdr["HOUSE_No_BANK"].ToString();
                    tempvenobj.STREET_NAME = sdr["STREET_NAME"].ToString();
                    tempvenobj.CITY_NAME = sdr["CITY_NAME"].ToString();

                    tempvenobj.CITY_POSTAL_CODE = sdr["CITY_POSTAL_CODE"].ToString();
                    tempvenobj.REGION_STATE = sdr["REGION_STATE"].ToString();
                    tempvenobj.IsDashboard = Convert.ToBoolean(sdr["IsDashboard"].ToString());
                    tempvenobj.BankTelNo = sdr["BankTelNo"].ToString();
                    tempvenobj.BANKMOBILENo = sdr["BANKMOBILENo"].ToString();
                    tempvenobj.BANKFAXNo = sdr["BANKFAXNo"].ToString();
                    tempvenobj.BANKEMAIL = sdr["BANKEMAIL"].ToString();


                    objMapperModel.Employee_table_Model = tempemoobj;
                    objMapperModel.ven_Table_Model = tempvenobj;

                    objList.Add(objMapperModel);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Vw_EmployeeVendorDetails where (Isnull(EmpId,'') like '%'+@search+'%'     "
                + " or Isnull(User_type,'') like '%'+@search+'%'"
                + " or Isnull(EmployeeName,'') like '%'+@search+'%' or Isnull(Designation,'') like '%'+@search+'%' "
                + " or Isnull(Sub_Company,'') like '%'+@search+'%' or Isnull(Department,'') like '%'+@search+'%' "
                + " or Isnull(Domain_ID,'') like '%'+@search+'%' or Isnull(Location,'') like '%'+@search+'%' "
                + " or Isnull(Reporting_Manager_Id,'') like '%'+@search+'%' or Isnull(Reporting_Manager_Id,'') like '%'+@search+'%' "
                + " or Isnull(Contact_No,0) like '%'+@search+'%' or Isnull(Email,'') like '%'+@search+'%' "
                + " or Isnull(CountryCode,'') like '%'+@search+'%' or Isnull(Userid,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Vendor_Mapper_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Vendor_Repository/GetVendorListByPage");
            }

            return objModel;

        }



        public List<Vendor_Mapper_Model> GetVendorList(long CreatedBy)
        {
            List<Vendor_Mapper_Model> objModel = new List<Vendor_Mapper_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select * From Vw_EmployeeVendorDetails where Isnull(IsDeleted,'')=@IsDeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", false);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Vendor_Mapper_Model objMapperModel = new Vendor_Mapper_Model();

                    Employee_table_Model tempemoobj = new Employee_table_Model();
                    tempemoobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempemoobj.EmpId = sdr["EmpId"].ToString();
                    tempemoobj.User_type = sdr["User_type"].ToString();
                    tempemoobj.EmployeeName = sdr["EmployeeName"].ToString();
                    tempemoobj.Designation = sdr["Designation"].ToString();
                    tempemoobj.Sub_Company = Convert.ToInt64(sdr["Sub_Company"].ToString());
                    tempemoobj.Department = sdr["Department"].ToString();
                    tempemoobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempemoobj.Location = sdr["Location"].ToString();
                    tempemoobj.Reporting_Manager_Id = sdr["Reporting_Manager_Id"].ToString();
                    tempemoobj.Contact_No = Convert.ToInt64(sdr["Contact_No"].ToString());
                    tempemoobj.Email = sdr["Email"].ToString();
                    tempemoobj.CountryCode = sdr["CountryCode"].ToString();
                    tempemoobj.Doj = Convert.ToDateTime(sdr["Doj"].ToString());
                    tempemoobj.Dob = Convert.ToDateTime(sdr["Dob"].ToString());
                    tempemoobj.Userid = sdr["Userid"].ToString();
                    tempemoobj.EmpPhoto = sdr["EmpPhoto"].ToString();
                    tempemoobj.Gender = sdr["Gender"].ToString();
                    tempemoobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempemoobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempemoobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempemoobj.isManager = Convert.ToBoolean(sdr["isManager"].ToString());
                    tempemoobj.isUserFp = Convert.ToBoolean(sdr["isUserFp"].ToString());
                    objMapperModel.Employee_table_Model = tempemoobj;

                    ven_Table_Model tempvenobj = new ven_Table_Model();
                    tempvenobj.SLNO = Convert.ToInt64(sdr["VenSLNO"].ToString());
                    tempvenobj.User_id = Convert.ToInt64(sdr["User_id"].ToString());
                    tempvenobj.CompanyName = sdr["CompanyName"].ToString();
                    tempvenobj.Url = sdr["Url"].ToString();
                    tempvenobj.Address = sdr["Address"].ToString();
                    tempvenobj.LegalStatus = sdr["LegalStatus"].ToString();
                    tempvenobj.Remarks = sdr["Remarks"].ToString();
                    tempvenobj.BCountry = sdr["BCountry"].ToString();
                    tempvenobj.SwiftCode = sdr["SwiftCode"].ToString();
                    tempvenobj.ExtUserBankAcNo = sdr["ExtUserBankAcNo"].ToString();
                    tempvenobj.AcHolderName = sdr["AcHolderName"].ToString();
                    tempvenobj.IBANNO = sdr["IBANNO"].ToString();
                    tempvenobj.BankName = sdr["BankName"].ToString();
                    tempvenobj.IFSC_Code = sdr["IFSC_Code"].ToString();
                    tempvenobj.BANK_BRANCH = sdr["BANK_BRANCH"].ToString();
                    tempvenobj.HOUSE_No_BANK = sdr["HOUSE_No_BANK"].ToString();
                    tempvenobj.STREET_NAME = sdr["STREET_NAME"].ToString();
                    tempvenobj.CITY_NAME = sdr["CITY_NAME"].ToString();

                    tempvenobj.CITY_POSTAL_CODE = sdr["CITY_POSTAL_CODE"].ToString();
                    tempvenobj.REGION_STATE = sdr["REGION_STATE"].ToString();
                    tempvenobj.IsDashboard = Convert.ToBoolean(sdr["IsDashboard"].ToString());
                    tempvenobj.BankTelNo = sdr["BankTelNo"].ToString();
                    tempvenobj.BANKMOBILENo = sdr["BANKMOBILENo"].ToString();
                    tempvenobj.BANKFAXNo = sdr["BANKFAXNo"].ToString();
                    tempvenobj.BANKEMAIL = sdr["BANKEMAIL"].ToString();
                    objMapperModel.ven_Table_Model = tempvenobj;

                    objModel.Add(objMapperModel);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Vendor_Repository/GetVendorList");
            }

            return objModel;

        }


        private string SaveImage(string EmpPhoto)
        {
            string result = "";

            try
            {
                var guid = Guid.NewGuid().ToString();

                byte[] imageBytes = Convert.FromBase64String(EmpPhoto);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);


                string newFile = Guid.NewGuid().ToString() + ".png";
                string filepath = Path.Combine("" + "/Content/CustomImages/Employee", newFile);
                //string filePath = Path.Combine(Microsoft.AspNetCore.Server.MapPath("~/Assets/") + Request.QueryString["id"] + "/", newFile);
                image.Save(filepath, ImageFormat.Png);
                result = newFile;
            }
            catch (Exception ex)
            {
                //result = ex.Message;
                ErrorHandler.LogError(ex, 0, "Web API", "Vendor_Repository/SaveImage");
            }

            return result;




        }



    }
}
