﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class RecordFieldItem_Repository
    {
        public DBReturnModel InsertRecordFieldItem(List_Item_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Record Field Item!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertRecordFieldItem";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@List_ID", objModel.List_ID);
                    cmd.Parameters.AddWithValue("@List_ItemType", objModel.List_ItemType);
                    cmd.Parameters.AddWithValue("@List_ItemValue", objModel.List_ItemValue);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("@isMulti", objModel.isMulti);
                    cmd.Parameters.AddWithValue("@isMandatory", objModel.isMandatory);
                    cmd.Parameters.AddWithValue("@IsHorizAllign", objModel.IsHorizAllign);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RecordFieldItem_Repository/InsertRecordFieldItem");
                    objReturn.ReturnMessage = "System Error on Inserting Record Field Item";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RecordFieldItem_Repository/InsertRecordFieldItem");
                objReturn.ReturnMessage = "System Error on Inserting Record Field Item";
            }

            return objReturn;
        }



        public List<List_Item_Table_Model> GetRecordFieldItemList(long CreatedBy, long ListId)
        {
            List<List_Item_Table_Model> objModel = new List<List_Item_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectRecordFieldItemById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", 0);
                cmd.Parameters.AddWithValue("@List_ID", ListId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    List_Item_Table_Model tempobj = new List_Item_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.ListName = sdr["ListName"].ToString();
                    tempobj.List_ID = Convert.ToInt64(sdr["List_ID"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.List_ItemType = sdr["List_ItemType"].ToString();
                    tempobj.List_ItemValue = sdr["List_ItemValue"].ToString();
                    tempobj.isMandatory = Convert.ToBoolean(sdr["isMandatory"].ToString());
                    tempobj.IsHorizAllign = Convert.ToBoolean(sdr["IsHorizAllign"].ToString());
                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "RecordFieldItem_Repository/GetRecordFieldItemList");
            }

            return objModel;

        }
        


        public DBReturnModel DeleteRecordFieldItem(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Record Field Item!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteRecordFieldItem";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "RecordFieldItem_Repository/DeleteRecordFieldItem");
                    objreturn.ReturnMessage = "System Error on Deleting Record Field Item!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "RecordFieldItem_Repository/DeleteRecordFieldItem");
                objreturn.ReturnMessage = "System Error on Deleting Record Field Item!";
            }

            return objreturn;
        }




        public List<List_Item_Table_Model> GetRecordFieldItemListFromMappedClassId(long CreatedBy, long ClassId)
        {
            List<List_Item_Table_Model> objModel = new List<List_Item_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, List_ID, List_ItemType,List_ItemValue From List_Item_Table where IsDeleted!=1 and List_ID in "
                + " (Select RecordField_ID From RecordField_DocClass_Mapping_Table where IsDeleted != 1 and Document_ClassID = @Document_ClassID) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Document_ClassID", ClassId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    List_Item_Table_Model tempobj = new List_Item_Table_Model();
                    //tempobj.ListName = sdr["ListName"].ToString();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.List_ID = Convert.ToInt64(sdr["List_ID"].ToString());
                    tempobj.List_ItemType = sdr["List_ItemType"].ToString();
                    tempobj.List_ItemValue = sdr["List_ItemValue"].ToString();

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "RecordFieldItem_Repository/GetRecordFieldItemListFromMappedClassId");
            }

            return objModel;

        }


    }
}
