﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class PTS_Repository
    {

        public List<SelectListItem> Clientlist(int officeid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();
                string sqlstr = "Sp_GetAllClientList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
               // cmd.Parameters.AddWithValue("@OfficeId", officeid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["Name"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/Getclientlist");
            }
            return objList;
        }

        public List<SelectListItem> VSCCCActivity(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();

                string sqlstr = "Sp_GetAllActivityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
               // cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["Activity"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/activitylist");
            }
            return objList;
        }

        public List<SelectListItem> VSCCCSubACtivity(int officeId, int activityid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();

                string sqlstr = "Sp_GetAllSubActivityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@ActivityId", activityid);
                //cmd.Parameters.AddWithValue("@OfficeId", officeId);
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["SubActivity"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/subactivitylist");
            }
            return objList;
        }

     

        public List<SelectListItem> Userlist(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();

                string sqlstr = "Getusers";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                // cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Userid"].ToString()),
                        Text = sdr["name"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/userlist");
            }
            return objList;
        }

       

        public object Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid, int Projectid, int moduleid)
        {
            string result = "Error on Deleting Task!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_FilldatainSheetByTodo]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empid", empid);
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@time", time);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@clientid", clientid);
                    cmd.Parameters.AddWithValue("@activityid", activityid);
                    cmd.Parameters.AddWithValue("@subactivityid", subactivityid);
                    cmd.Parameters.AddWithValue("@Projectid", Projectid);
                    cmd.Parameters.AddWithValue("@moduleid", moduleid);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
            }
            return result;
        }



        public List<SelectListItem> Projectlist(int Clientid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();

                string sqlstr = "Sp_GetProjectBymodules";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@clientid", Clientid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["Projectname"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "PTS/activitylist");
            }
            return objList;
        }

        public List<SelectListItem> Projectmodules(int Projectid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionPTS();
                con.Open();

                string sqlstr = "Sp_GetModulesByProject";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@projectid", Projectid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["modulename"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "PTS/modules");
            }
            return objList;
        }

        public VSCUserData_Table PTSUserData(int Userid)
        {
            VSCUserData_Table objList = new VSCUserData_Table();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetUsersData";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserId", Userid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.results.Name = sdr["Name"].ToString();
                    objList.results.MobileNo = sdr["Mobile"].ToString();
                    objList.results.EmailId = sdr["Email"].ToString();
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Userid, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }
    }
}
