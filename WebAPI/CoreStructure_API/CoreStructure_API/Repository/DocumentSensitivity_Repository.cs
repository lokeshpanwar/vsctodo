﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocumentSensitivity_Repository
    {

        public DBReturnModel InsertDocumentSensitivity(DocSensitivity_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Sensitivity!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentSensitivity";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@sensiName", objModel.sensiName);
                    cmd.Parameters.AddWithValue("@sensiCode", objModel.sensiCode);
                    cmd.Parameters.AddWithValue("@sensiDescrip", objModel.sensiDescrip);
                    cmd.Parameters.AddWithValue("@sensiType", objModel.sensiType);
                    cmd.Parameters.AddWithValue("@sensiPercent", objModel.sensiPercent);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSensitivity_Repository/InsertDocumentSensitivity");
                    objReturn.ReturnMessage = "System Error on Inserting Document Sensitivity!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocumentSensitivity_Repository/InsertDocumentSensitivity");
            }

            return objReturn;
        }


        
        public DocSensitivity_table_page_Model GetDocumentSensitivityListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            DocSensitivity_table_page_Model objModel = new DocSensitivity_table_page_Model();
            List<DocSensitivity_table_Model> objList = new List<DocSensitivity_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentSensitivityByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocSensitivity_table_Model tempobj = new DocSensitivity_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.sensiCode = sdr["sensiCode"].ToString();
                    tempobj.Sub_CompID = sdr["Sub_CompID"].ToString();
                    tempobj.sensiName = sdr["sensiName"].ToString();
                    tempobj.sensiDescrip = sdr["sensiDescrip"].ToString();
                    tempobj.sensiType = sdr["sensiType"].ToString();
                    tempobj.CreatedBy = sdr["CreatedBy"].ToString();
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.sensiPercent = Convert.ToInt32(sdr["sensiPercent"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From DocSensitivity_table where (Isnull(sensiName,'') like '%'+@search+'%'    "       
                    + " or Isnull(sensiCode,'') like '%'+@search+'%' or Isnull(sensiDescrip,'') like '%'+@search+'%' "
                    + " or Isnull(sensiType,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.DocSensitivity_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSensitivity_Repository/GetDocumentSensitivityListByPage");
            }

            return objModel;

        }



        public DocSensitivity_table_Model GetOneDocumentSensitivity(long SLNO, long CreatedBy)
        {
            DocSensitivity_table_Model objModel = new DocSensitivity_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentSensitivityById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.sensiCode = sdr["sensiCode"].ToString();
                    objModel.Sub_CompID = sdr["Sub_CompID"].ToString();
                    objModel.sensiName = sdr["sensiName"].ToString();
                    objModel.sensiDescrip = sdr["sensiDescrip"].ToString();
                    objModel.sensiType = sdr["sensiType"].ToString();
                    objModel.CreatedBy = sdr["CreatedBy"].ToString();
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.sensiPercent = Convert.ToInt32(sdr["sensiPercent"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentSensitivity_Repository/GetOneDocumentSensitivity");
            }

            return objModel;

        }

                

        public DBReturnModel UpdateDocumentSensitivity(DocSensitivity_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Updating Document Sensitivity!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateDocumentSensitivity";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@sensiName", objModel.sensiName);
                    cmd.Parameters.AddWithValue("@sensiCode", objModel.sensiCode);
                    cmd.Parameters.AddWithValue("@sensiDescrip", objModel.sensiDescrip);
                    cmd.Parameters.AddWithValue("@sensiType", objModel.sensiType);
                    cmd.Parameters.AddWithValue("@sensiPercent", objModel.sensiPercent);

                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.ModifiedBy), "API", "DocumentSensitivity_Repository/UpdateDocumentSensitivity");
                    objReturn.ReturnMessage = "System Error on Updating Document Type!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.ModifiedBy), "API", "DocumentSensitivity_Repository/UpdateDocumentSensitivity");
            }

            return objReturn;
        }





        public DBReturnModel DeleteDocumentSensitivity(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Document Sensitivity!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteDocumentSensitivity";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@ModifiedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSensitivity_Repository/DeleteDocumentSensitivity");
                    objreturn.ReturnMessage = "System Error on Deleting Document Sensitivity!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentSensitivity_Repository/DeleteDocumentSensitivity");
                objreturn.ReturnMessage = "System Error on Deleting Document Sensitivity!";
            }

            return objreturn;
        }



    }
}
