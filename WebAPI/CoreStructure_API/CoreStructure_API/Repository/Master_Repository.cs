﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class Master_Repository
    {
        #region Users
        public string AddNewUser(Users objModel)
        {
            string result = "Error on Inserting User!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateUsers]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", objModel.UserId);
                    cmd.Parameters.AddWithValue("@RoleId", objModel.RoleId);
                    cmd.Parameters.AddWithValue("@UserName", objModel.UserName);
                    cmd.Parameters.AddWithValue("@Password", objModel.Password);
                    cmd.Parameters.AddWithValue("@Name", objModel.Name);
                    cmd.Parameters.AddWithValue("@FatherName", objModel.FatherName);
                    cmd.Parameters.AddWithValue("@MotherName", objModel.MotherName);
                    cmd.Parameters.AddWithValue("@DOB", objModel.DOB);
                    cmd.Parameters.AddWithValue("@DOJ", objModel.DOJ);
                    cmd.Parameters.AddWithValue("@EmailId", objModel.EmailId);
                    cmd.Parameters.AddWithValue("@MobileNo", objModel.MobileNo);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@StateId", objModel.StateId);
                    cmd.Parameters.AddWithValue("@CityId", objModel.CityId);
                    cmd.Parameters.AddWithValue("@Town", objModel.Town);
                    cmd.Parameters.AddWithValue("@EmergencyContactNo", objModel.EmergencyContactNo);
                    cmd.Parameters.AddWithValue("@NameRelation", objModel.NameRelation);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@UploadFileName", objModel.fileName);
                    cmd.Parameters.AddWithValue("@UploadFileExt", objModel.fileExt);
                    cmd.Parameters.AddWithValue("@UploadFileData", objModel.fileData);
                    cmd.Parameters.AddWithValue("@POSCode", objModel.POSCode);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewUser");
                    //result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewUser");
            }
            return result;
        }

        public List<Users> GetUserDataForEdit(int userId, long createdBy)
        {
            List<Users> objList = new List<Users>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetUserDataForEdit";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", createdBy);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Users
                    {
                        UserId = Convert.ToInt64(sdr["UId"].ToString()),
                        RoleId = Convert.ToInt32(sdr["RoleId"].ToString()),
                        RoleName = sdr["RoleName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        UserName = sdr["UserName"].ToString(),
                        Password = EncryptDecrypt.Decrypt(sdr["Password"].ToString()),
                        Name = sdr["Name"].ToString(),
                        FatherName = sdr["FatherName"].ToString(),
                        MotherName = sdr["MotherName"].ToString(),
                        DOB = sdr["DateOfBirth"].ToString(),
                        DOJ = sdr["DateOfJoin"].ToString(),
                        EmailId = sdr["EmailId"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        Address = sdr["Address"].ToString(),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = sdr["StateName"].ToString(),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = sdr["CityName"].ToString(),
                        Town = sdr["Town"].ToString(),
                        EmergencyContactNo = sdr["EmergencyContactNo"].ToString(),
                        NameRelation = sdr["NameRelation"].ToString(),
                        POSCode = sdr["POSCode"].ToString(),
                        fileName = sdr["ImageName"].ToString(),
                        OfficeName = sdr["OfficeName"].ToString(),
                   //     photo = "http://localhost:1202/api/Employee/GetEmployeeImage?path=" +  Path.Combine(webpath, "http://localhost:1202/Content/CustomImages/Employee/" + sdr["Photo"].ToString())

                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "Master_Repository/GetUserList");
            }
            return objList;
        }

        public List<Users> GetUserList(int OfficeId, long CreatedBy)
        {
            List<Users> objList = new List<Users>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetUserList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Users
                    {
                        UserId = Convert.ToInt64(sdr["UId"].ToString()),
                        RoleId = Convert.ToInt32(sdr["RoleId"].ToString()),
                        RoleName = sdr["RoleName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        UserName = sdr["UserName"].ToString(),
                        Password = sdr["Password"].ToString(),
                        Name = sdr["Name"].ToString(),
                        FatherName = sdr["FatherName"].ToString(),
                        MotherName = sdr["MotherName"].ToString(),
                        DOB = sdr["DateOfBirth"].ToString(),
                        DOJ = sdr["DateOfJoin"].ToString(),
                        EmailId = sdr["EmailId"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        Address = sdr["Address"].ToString(),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = sdr["StateName"].ToString(),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = sdr["CityName"].ToString(),
                        Town = sdr["Town"].ToString(),
                        EmergencyContactNo = sdr["EmergencyContactNo"].ToString(),
                        NameRelation = sdr["NameRelation"].ToString(),
                        POSCode = sdr["POSCode"].ToString(),
                        fileName = sdr["ImageName"].ToString(),
                        OfficeName = sdr["OfficeName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetUserList");
            }
            return objList;
        }
        public string DeleteUser(long userId, long logInUser)
        {
            string result = "Error on Deleting User!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteUser]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteUser");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteUser");
            }
            return result;
        }
        public string CheckUserExistOrNot(string userName, long logInUser)
        {
            string result = "0";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_CheckUserExistOrNot]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserName", userName);
                    //cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/CheckUserExistOrNot");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/CheckUserExistOrNot");
            }
            return result;
        }
        public string GetLatestPOSCode(int RoleId, long logInUser)
        {
            string result = "";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_GetLatestPOSCode]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RoleId", RoleId);
                    //cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetLatestPOSCode");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetLatestPOSCode");
            }
            return result;
        }
        public string CheckContactPersonExistOrNot(string Name, string MobileNo, long logInUser)
        {
            string result = "0";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_CheckContactPersonExistOrNot]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Name", Name);
                    cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                    cmd.Parameters.AddWithValue("@userid", logInUser);
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/CheckContactPersonExistOrNot");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/CheckContactPersonExistOrNot");
            }
            return result;
        }


        #endregion

        #region Groups
        public string AddNewGroup(Group objModel)
        {
            string result = "Error on Inserting Group!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateGroups]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", objModel.GroupId);
                    cmd.Parameters.AddWithValue("@GroupName", objModel.GroupName);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewGroup");
            }
            return result;
        }

        public List<Group> GetGroupList(int OfficeId, long CreatedBy)
        {
            List<Group> objList = new List<Group>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetGroupList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new Group
                    {
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = sdr["GroupName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetGroupList");
            }

            return objList;
        }

        public string DeleteGroup(int groupId, long logInUser)
        {
            string result = "Error on Deleting Group!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteGroup]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GroupId", groupId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteGroup");
            }
            return result;
        }


        #endregion

        #region Products

        public string AddNewProduct(Product objModel)
        {
            string result = "Error on Inserting Product!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateProducts]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProductId", objModel.ProductId);
                    cmd.Parameters.AddWithValue("@ProductName", objModel.ProductName);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@ProductRate", objModel.Rate);
                    cmd.Parameters.AddWithValue("@ODRate", objModel.ODRate);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewProduct");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewProduct");
            }
            return result;
        }



        public List<Product> GetProductList(int OfficeId, long CreatedBy)
        {
            List<Product> objList = new List<Product>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetProductList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Product
                    {
                        InsuranceCompanyId = Convert.ToInt32(sdr["Inscoid"].ToString()),
                        InsuranceCompany = sdr["CompanyName"].ToString(),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = sdr["ProductName"].ToString(),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = sdr["CategoryName"].ToString(),
                        Rate = Convert.ToString(sdr["ProductRate"].ToString()),
                        ODRate = Convert.ToString(sdr["ODCommissionRate"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        totalrate = Convert.ToDecimal(sdr["totalrate"].ToString()),

                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetProductList");
            }
            return objList;
        }

      

        public string DeleteProduct(int ProductId, long logInUser)
        {
            string result = "Error on Deleting Product!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteProduct]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProductId", ProductId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteProduct");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteProduct");
            }
            return result;
        }

        #endregion

        #region Issuing Office Master

        public string GetInsuranceCompanyShortName(int CompanyId, long logInUser)
        {
            string result = "";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_GetInsuranceCompanyShortName]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
                    //cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetInsuranceCompanyShortName");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/GetInsuranceCompanyShortName");
            }
            return result;
        }

        public string AddNewIssuingOffice(IssuingOffice objModel)
        {
            string result = "Error on Inserting Issuing Office!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateIssuingOffice]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", objModel.IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@IssuingOfficeName", objModel.IssuingOfficeName);
                    cmd.Parameters.AddWithValue("@InsuranceCompanyId", objModel.InsuranceCompanyId);
                    cmd.Parameters.AddWithValue("@ShortName", objModel.ShortName);
                    cmd.Parameters.AddWithValue("@ManagerName", objModel.ManagerName);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@City", objModel.City);
                    cmd.Parameters.AddWithValue("@MobileNo", objModel.MobileNo);
                    cmd.Parameters.AddWithValue("@EmailID", objModel.EmailID);
                    cmd.Parameters.AddWithValue("@Code", objModel.Code);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@SalesManagerName", objModel.SalesManagerName);
                    cmd.Parameters.AddWithValue("@SMContactNo", objModel.SMContactNo);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewIssuingOffice");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewIssuingOffice");
            }
            return result;
        }
        public List<IssuingOffice> GetIssuingOfficeList(int OfficeId, long CreatedBy)
        {
            List<IssuingOffice> objList = new List<IssuingOffice>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetIssuingOfficeList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new IssuingOffice
                    {
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = sdr["IssuingOfficeName"].ToString(),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = sdr["CompanyName"].ToString(),
                        ShortName = sdr["CompanyShortName"].ToString(),
                        ManagerName = sdr["ManagerName"].ToString(),
                        Address = sdr["Address"].ToString(),
                        City = sdr["City"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        EmailID = sdr["EmailID"].ToString(),
                        Code = sdr["Code"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        SalesManagerName = Convert.ToString(sdr["SalesManagerName"].ToString()),
                        SMContactNo = Convert.ToString(sdr["SMContactNo"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetIssuingOfficeList");
            }
            return objList;
        }
        public string DeleteIssuingOffice(long IssuingOfficeId, long logInUser)
        {
            string result = "Error on Deleting IssuingOffice!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteIssuingOffice]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IssuingOfficeId", IssuingOfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteIssuingOffice");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteIssuingOffice");
            }
            return result;
        }

        #endregion

        #region Category
        public string AddNewCategory(Category objModel)
        {
            string result = "Error on Inserting Product!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateCategories]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@CategoryName", objModel.CategoryName);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewCategory");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewProduct");
            }
            return result;
        }

        public List<Category> GetCategoryList(int OfficeId, long CreatedBy)
        {
            List<Category> objList = new List<Category>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetCategoryListbyOfficeId";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Category
                    {
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = sdr["CategoryName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetCategoryList");
            }
            return objList;
        }

        public string DeleteCategory(int categoryId, long logInUser)
        {
            string result = "Error on Deleting Product!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteCategory]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CategoryId", categoryId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteCategory");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteCategory");
            }
            return result;
        }
        #endregion

        #region ContactPerson
        public string AddNewContactPerson(ContactPerson objModel)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateContactPerson]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CPID", objModel.ContactPersonId);
                    cmd.Parameters.AddWithValue("@Name", objModel.Name);
                    cmd.Parameters.AddWithValue("@FatherName", objModel.FatherName);
                    cmd.Parameters.AddWithValue("@MobileNo", objModel.MobileNo);
                    cmd.Parameters.AddWithValue("@EmailId", objModel.EmailId);
                    cmd.Parameters.AddWithValue("@Address", objModel.Address);
                    cmd.Parameters.AddWithValue("@DOB", objModel.DOB);
                    cmd.Parameters.AddWithValue("@StateId", objModel.StateId);
                    cmd.Parameters.AddWithValue("@CityId", objModel.CityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewContactPerson");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewContactPerson");
            }
            return result;
        }
        public List<ContactPerson> GetContactPersonList(int OfficeId, long CreatedBy)
        {
            List<ContactPerson> objList = new List<ContactPerson>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetContactPersonList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new ContactPerson
                    {
                        ContactPersonId = Convert.ToInt32(sdr["CPId"].ToString()),
                        Name = Convert.ToString(sdr["Name"].ToString()),
                        FatherName = Convert.ToString(sdr["FatherName"].ToString()),
                        MobileNo = Convert.ToString(sdr["MobileNo"].ToString()),
                        EmailId = Convert.ToString(sdr["EmailId"].ToString()),
                        Address = Convert.ToString(sdr["Address"].ToString()),
                        DOB = Convert.ToString(sdr["DateOfBirth"].ToString()),
                        StateId = Convert.ToInt32(sdr["StateId"].ToString()),
                        StateName = Convert.ToString(sdr["StateName"].ToString()),
                        CityId = Convert.ToInt32(sdr["CityId"].ToString()),
                        CityName = Convert.ToString(sdr["CityName"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }
        public string DeleteContactPerson(int ContactPersonId, long logInUser)
        {
            string result = "Error on Deleting Contact Person!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteContactPerson]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ContactPersonId", ContactPersonId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteContactPerson");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteContactPerson");
            }
            return result;
        }

        #endregion

        #region Profile
        public Users GetAdminProfileForView(long UId)
        {
            Users data = new Users();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetAdminProfileForView";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", UId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    //objList.Add(new Users
                    //{
                    data.UserId = Convert.ToInt64(sdr["UId"].ToString());
                    data.RoleId = Convert.ToInt32(sdr["RoleId"].ToString());
                    data.RoleName = sdr["RoleName"].ToString();
                    data.OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString());
                    data.UserName = sdr["UserName"].ToString();
                    data.Password = sdr["Password"].ToString();
                    data.Name = sdr["Name"].ToString();
                    data.FatherName = sdr["FatherName"].ToString();
                    data.MotherName = sdr["MotherName"].ToString();
                    data.DOB = sdr["DateOfBirth"].ToString();
                    data.DOJ = sdr["DateOfJoin"].ToString();
                    data.EmailId = sdr["EmailId"].ToString();
                    data.MobileNo = sdr["MobileNo"].ToString();
                    data.Address = sdr["Address"].ToString();
                    data.StateId = Convert.ToInt32(sdr["StateId"].ToString());
                    data.StateName = sdr["StateName"].ToString();
                    data.CityId = Convert.ToInt32(sdr["CityId"].ToString());
                    data.CityName = sdr["CityName"].ToString();
                    data.Town = sdr["Town"].ToString();
                    data.EmergencyContactNo = sdr["EmergencyContactNo"].ToString();
                    data.NameRelation = sdr["NameRelation"].ToString();
                    data.fileName = sdr["ImageName"].ToString();
                    //});
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UId, "API", "Master_Repository/GetAdminProfileForView");
            }
            return data;
        }

        #endregion

        #region TP/Surveyor
        public string AddNewTPSurveyor(TP_Surveyor objModel)
        {
            string result = "Error on Inserting TP Surveyor!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateTPSurveyor]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TPS_Id", objModel.TPS_Id);
                    cmd.Parameters.AddWithValue("@Ins_Comp", objModel.Ins_Comp);
                    cmd.Parameters.AddWithValue("@TP_Name", objModel.TP_Name);
                    cmd.Parameters.AddWithValue("@TP_Address", objModel.TP_Address);
                    cmd.Parameters.AddWithValue("@TP_ContactNo", objModel.TP_ContactNo);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewTPSurveyor");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewTPSurveyor");
            }
            return result;
        }
        public List<TP_Surveyor> GetTPSurveyorList(int OfficeId, long CreatedBy)
        {
            List<TP_Surveyor> objList = new List<TP_Surveyor>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetTPSurveyorList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new TP_Surveyor
                    {
                        TPS_Id = Convert.ToInt32(sdr["TPId"].ToString()),
                        Ins_Comp = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        TP_Name = Convert.ToString(sdr["TPName"].ToString()),
                        TP_Address = Convert.ToString(sdr["TPAddress"].ToString()),
                        TP_ContactNo = Convert.ToString(sdr["TPMobileNo"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetTPSurveyorList");
            }
            return objList;
        }
        public string DeleteTP_Surveyor(int TPId, long logInUser)
        {
            string result = "Error on Deleting TP Surveyor!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteTPSurveyor]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TPId", TPId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteTP_Surveyor");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteTP_Surveyor");
            }
            return result;
        }

        #endregion

        #region Activity
        public string AddNewActivity(Activity objModel)
        {
            string result = "Error on Inserting Activity!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateActivity]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ActivityId", objModel.ActivityId);
                    cmd.Parameters.AddWithValue("@ActivityName", objModel.ActivityName);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewActivity");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewActivity");
            }
            return result;
        }
        public List<Activity> GetActivityList(int OfficeId, long CreatedBy)
        {
            List<Activity> objList = new List<Activity>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetActivityListbyOfficeId";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Activity
                    {
                        ActivityId = Convert.ToInt32(sdr["ActivityId"].ToString()),
                        ActivityName = sdr["ActivityName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetActivityList");
            }
            return objList;
        }
        public string DeleteActivity(int ActivityId, long logInUser)
        {
            string result = "Error on Deleting Activity!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteActivity]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ActivityId", ActivityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteActivity");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteActivity");
            }
            return result;
        }

        #endregion

        #region Sub Activity
        public string AddNewSubActivity(SubActivity objModel)
        {
            string result = "Error on Inserting SubActivity!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateSubActivity]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SubActivityId", objModel.SubActivityId);
                    cmd.Parameters.AddWithValue("@SubActivityName", objModel.SubActivityName);
                    cmd.Parameters.AddWithValue("@ActivityId", objModel.ActivityId);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewSubActivity");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewSubActivity");
            }
            return result;
        }

        public List<SubActivity> GetSubActivityList(int OfficeId, long CreatedBy)
        {
            List<SubActivity> objList = new List<SubActivity>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetSubActivityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new SubActivity
                    {
                        SubActivityId = Convert.ToInt32(sdr["SubActivityId"].ToString()),
                        SubActivityName = sdr["SubActivityName"].ToString(),
                        ActivityId = Convert.ToInt32(sdr["ActivityId"].ToString()),
                        ActivityName = sdr["ActivityName"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetSubActivityList");
            }
            return objList;
        }

        public string DeleteSubActivity(int SubActivityId, long logInUser)
        {
            string result = "Error on Deleting SubActivity!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteSubActivity]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SubActivityId", SubActivityId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteSubActivity");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteSubActivity");
            }
            return result;
        }
        #endregion

        #region Holiday
        public string AddNewHoliday(Holiday objModel)
        {
            string result = "Error on Inserting Activity!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveAndUpdateHoliday]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HolidayId", objModel.HolidayId);
                    cmd.Parameters.AddWithValue("@HolidayName", objModel.HolidayName);
                    cmd.Parameters.AddWithValue("@Date", objModel.Date);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewHoliday");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Master_Repository/AddNewHoliday");
            }
            return result;
        }

        public List<Holiday> GetHolidayList(int OfficeId, long CreatedBy)
        {
            List<Holiday> objList = new List<Holiday>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetHolidayListbyOfficeId";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new Holiday
                    {
                        HolidayId = Convert.ToInt32(sdr["Id"].ToString()),
                        HolidayName = sdr["HolidayName"].ToString(),
                        Date = sdr["Date1"].ToString(),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetHolidayList");
            }
            return objList;
        }


        public string DeleteHoliday(int holidayId, long logInUser)
        {
            string result = "Error on Deleting Holiday!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteHoliday]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@holidayId", holidayId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteActivity");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Master_Repository/DeleteActivity");
            }
            return result;
        }
        #endregion
    }
}
