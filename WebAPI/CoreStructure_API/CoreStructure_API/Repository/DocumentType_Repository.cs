﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocumentType_Repository
    {

        public DBReturnModel InsertDocumentType(DocumentType_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Type!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentType";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Parent_CompID", objModel.Parent_CompID);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DocumentType_Name", objModel.DocumentType_Name);
                    cmd.Parameters.AddWithValue("@DocumenType_desc", objModel.DocumenType_desc);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentType_Repository/InsertDocumentType");
                    objReturn.ReturnMessage = "System Error on Inserting Document Type!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentType_Repository/InsertDocumentType");
            }

            return objReturn;
        }



        public DocumentType_Table_page_Model GetDocumentTypeListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            DocumentType_Table_page_Model objModel = new DocumentType_Table_page_Model();
            List<DocumentType_Table_Model> objList = new List<DocumentType_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentTypeByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentType_Table_Model tempobj = new DocumentType_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Parent_CompID = Convert.ToInt64(sdr["Parent_CompID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DocumentType_Name = sdr["DocumentType_Name"].ToString();
                    tempobj.DocumenType_desc = sdr["DocumenType_desc"].ToString();                    
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From DocumentType_table where (Isnull(DocumentType_Name,'') like '%'+@search+'%'    "       
                + " or Isnull(DocumenType_Desc,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.DocumentType_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentType_Repository/GetDocumentTypeListByPage");
            }

            return objModel;

        }



        public DocumentType_Table_Model GetOneDocumentType(long SLNO, long CreatedBy)
        {
            DocumentType_Table_Model objModel = new DocumentType_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentTypeById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Parent_CompID = Convert.ToInt64(sdr["Parent_CompID"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.DocumentType_Name = sdr["DocumentType_Name"].ToString();
                    objModel.DocumenType_desc = sdr["DocumenType_desc"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentType_Repository/GetOneDocumentType");
            }

            return objModel;

        }

        

        public DBReturnModel UpdateDocumentType(DocumentType_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Updating Document Type!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateDocumentType";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Parent_CompID", objModel.Parent_CompID);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DocumentType_Name", objModel.DocumentType_Name);
                    cmd.Parameters.AddWithValue("@DocumenType_desc", objModel.DocumenType_desc);

                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "DocumentType_Repository/UpdateDocumentType");
                    objReturn.ReturnMessage = "System Error on Updating Document Type!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "DocumentType_Repository/UpdateDocumentType");
            }

            return objReturn;
        }










        public DBReturnModel DisableDocumentType(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Document Type!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update DocumentType_Table set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Document Type Disabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentType_Repository/DisableDocumentType");
                    objreturn.ReturnMessage = "System Error on Disabling Document Type!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentType_Repository/DisableDocumentType");
                objreturn.ReturnMessage = "System Error on Disabling Document Type!";
            }

            return objreturn;
        }




        public DBReturnModel EnableDocumentType(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Document Type!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update DocumentType_table set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Document Type Enabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentType_Repository/EnableDocumentType");
                    objreturn.ReturnMessage = "System Error on Enabling Document Type!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocumentType_Repository/EnableDocumentType");
                objreturn.ReturnMessage = "System Error on Enabling Document Type!";
            }

            return objreturn;
        }




    }
}
