﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class ParentCompany_Repository
    {
        public string InsertParentCompany(Parent_Company_Table_Model objModel)
        {
            string result = "Error on Inserting Parent Company!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "Insert into Parent_Company_Table(Company_Name,CreatedBy,CreatedOn,KPID)"
                        + " values (@Company_Name,@CreatedBy,@CreatedOn,@KPID) ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Company_Name", objModel.Company_Name);
                    cmd.Parameters.AddWithValue("@KPID", objModel.KPID);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Parent Company Inserted Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "ParentCompany_Repository/InsertParentCompany");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "ParentCompany_Repository/InsertParentCompany");
            }

            return result;
        }

        

        public List<Parent_Company_Table_Model> GetParentCompanyList(long CreatedBy)
        {
            List<Parent_Company_Table_Model> objModel = new List<Parent_Company_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(SLNO,0) as SLNO, isnull(Company_Name,'') as Company_Name, isnull(CreatedBy,0) as CreatedBy, "
                    + " CreatedOn, isnull(KPID, '') as KPID From Parent_Company_Table";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Parent_Company_Table_Model tempobj = new Parent_Company_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Company_Name = sdr["Company_Name"].ToString();
                    tempobj.KPID = sdr["KPID"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ParentCompany_Repository/GetCompanyList");
            }

            return objModel;

        }



        public Parent_Company_Table_Model GetOneParentCompany(long SLNO, long CreatedBy)
        {
            Parent_Company_Table_Model objModel = new Parent_Company_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(SLNO,0) as SLNO, isnull(Company_Name,'') as Company_Name, isnull(CreatedBy,0) as CreatedBy, "
                    + " CreatedOn, isnull(KPID, '') as KPID From Parent_Company_Table where SLNO=@SLNO";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Company_Name = sdr["Company_Name"].ToString();
                    objModel.KPID = sdr["KPID"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ParentCompany_Repository/GetOneCompany");
            }

            return objModel;

        }

               

        public string UpdateParentCompany(Parent_Company_Table_Model objModel)
        {
            string result = "Error on Updating Parent Company!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "Update Parent_Company_Table set Company_Name=@Company_Name,UpdatedBy=@UpdatedBy,UpdatedOn=@UpdatedOn,KPID=@KPID "
                        + " where SLNO=@SLNO ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Company_Name", objModel.Company_Name);
                    cmd.Parameters.AddWithValue("@KPID", objModel.KPID);
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@UpdatedBy", objModel.UpdatedBy);
                    cmd.Parameters.AddWithValue("@UpdatedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Parent Company Updated Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "ParentCompany_Repository/UpdateCompany");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "ParentCompany_Repository/UpdateCompany");
            }

            return result;
        }



        public Parent_Company_Table_page_Model GetParentCompanyListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Parent_Company_Table_page_Model objModel = new Parent_Company_Table_page_Model();
            List<Parent_Company_Table_Model> objList = new List<Parent_Company_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectParentCompanyByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Parent_Company_Table_Model tempobj = new Parent_Company_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Company_Name = sdr["Company_Name"].ToString();
                    tempobj.KPID = sdr["KPID"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Parent_Company_Table where (Isnull(Company_Name,'') like '%'+@search+'%' or "
                        + " Isnull(KPID,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Parent_Company_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ParentCompany_Repository/GetParentCompanyListByPage");
            }

            return objModel;

        }


    }
}
