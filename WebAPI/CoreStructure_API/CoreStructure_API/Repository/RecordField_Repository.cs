﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class RecordField_Repository
    {

        public DBReturnModel InsertRecordField(List_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Record Field!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertRecordField";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DomainID", objModel.DomainID);
                    cmd.Parameters.AddWithValue("@ListName", objModel.ListName);
                    cmd.Parameters.AddWithValue("@ListDescription", objModel.ListDescription);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RecordField_Repository/InsertRecordField");
                    objReturn.ReturnMessage = "System Error on Inserting Record Field!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RecordField_Repository/InsertRecordField");
            }

            return objReturn;
        }
               

        public List_table_page_Model GetRecordFieldListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            List_table_page_Model objModel = new List_table_page_Model();
            List<List_table_Model> objList = new List<List_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectRecordFieldByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    List_table_Model tempobj = new List_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    tempobj.ListName = sdr["ListName"].ToString();
                    tempobj.ListDescription = sdr["ListDescription"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From List_table where (Isnull(ListName,'') like '%'+@search+'%' or "
                        + " Isnull(ListDescription,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.List_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "RecordField_Repository/GetRecordFieldListByPage");
            }

            return objModel;

        }

               
        public List_table_Model GetOneRecordField(long SLNO, long CreatedBy)
        {
            List_table_Model objModel = new List_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectRecordFieldById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    objModel.ListName = sdr["ListName"].ToString();
                    objModel.ListDescription = sdr["ListDescription"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "RecordField_Repository/GetOneRecordField");
            }

            return objModel;

        }
                     

        public DBReturnModel UpdateRecordField(List_table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Updating Record Field!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateRecordField";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@DomainID", objModel.DomainID);
                    cmd.Parameters.AddWithValue("@ListName", objModel.ListName);
                    cmd.Parameters.AddWithValue("@ListDescription", objModel.ListDescription);

                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "RecordField_Repository/UpdateRecordField");
                    objReturn.ReturnMessage = "System Error on Updating Record Field!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "RecordField_Repository/UpdateRecordField");
            }

            return objReturn;
        }







        public DBReturnModel DisableRecordField(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Record Field!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update List_Table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Record Field Disabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "RecordField_Repository/DisableRecordField");
                    objreturn.ReturnMessage = "System Error on Disabling Record Field!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "RecordField_Repository/DisableRecordField");
                objreturn.ReturnMessage = "System Error on Disabling Record Field!";
            }

            return objreturn;
        }




        public DBReturnModel EnableRecordField(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Record Field!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update List_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Record Field Enabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "RecordField_Repository/EnableRecordField");
                    objreturn.ReturnMessage = "System Error on Enabling Record Field!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "RecordField_Repository/EnableRecordField");
            }

            return objreturn;
        }


    }
}
