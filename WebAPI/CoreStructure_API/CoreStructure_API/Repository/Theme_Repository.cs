﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Theme_Repository
    {
        
        public Theme_table_Model GetOneTheme(long CreatedBy, string IPAddress)
        {
            Theme_table_Model objModel = new Theme_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select isnull(Theme_Name,'') as Theme_Name, isnull(CreatedBy,0) as CreatedBy, "
                    + " CreatedOn From Theme_table";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Theme_Name = sdr["Theme_Name"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Theme_Repository/GetOneTheme");
            }
            return objModel;
        }



        public string InsertTheme(Theme_table_Model objModel)
        {
            string result = "Error on Inserting Theme!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "Delete From Theme_table";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();



                    sqlstr = "Insert into Theme_table(Theme_Name,CreatedBy,CreatedOn)"
                        + " values (@Theme_Name,@CreatedBy,@CreatedOn) ";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Theme_Name", objModel.Theme_Name);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = "Theme Inserted Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy,"API", "Theme_Repository/InsertTheme");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Theme_Repository/InsertTheme");
            }

            return result;
        }


    }
}
