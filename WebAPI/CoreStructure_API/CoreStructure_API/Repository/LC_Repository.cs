﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
//using static CoreStructure_API.Model.Business_Model;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class LC_Repository
    {

        public LC_RequestLC_Pagination GetRequestForLCList(int pageSize, int startRowIndex, string search, string sort_col, string sort_dir, long CreatedBy, string CompId, string VenEmpId, string ELIKZ)
        {
            LC_RequestLC_Pagination objModel = new LC_RequestLC_Pagination();
            List<LC_REQUEST_TABLE_Model> objList = new List<LC_REQUEST_TABLE_Model>();
            try
            {
                if (ELIKZ == null)
                {
                    ELIKZ = "";
                }
                if (search == null)
                {
                    search = "";
                }
                if (sort_dir == null)
                {
                    sort_dir = "ASC";
                }
                if (sort_col == null)
                {
                    sort_col = "C_NAME1";
                }
                if (VenEmpId == "" || VenEmpId == null)
                {
                    VenEmpId = "404691";
                }
                string sort_col_alias = string.Empty;
                try
                {
                    //var con = SqlHelper.getNewConnectionString(CompId);  // Provided by Manish Sir
                    var con = SqlHelper.Connection();  // New Line Added By Vijendra
                    con.Open();
                    string sqlstr = "sp_GetLCList";
                    SqlCommand cmd = new SqlCommand(sqlstr, con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LIFNR", VenEmpId);
                    cmd.Parameters.AddWithValue("@ELIKZ", ELIKZ);
                    cmd.Parameters.AddWithValue("@search", search);
                    cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                    cmd.Parameters.AddWithValue("@pageSize", pageSize);
                    cmd.Parameters.AddWithValue("@sort_col", sort_col);
                    cmd.Parameters.AddWithValue("@sort_dir", sort_dir);
                    cmd.Parameters.AddWithValue("@sort_col_alias", sort_col_alias);

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        LC_REQUEST_TABLE_Model tempobj = new LC_REQUEST_TABLE_Model();
                        tempobj.EBELN = sdr["EBELN"].ToString();
                        //tempobj.LIFNR = sdr["LIFNR"].ToString();
                        tempobj.PO_R_DATE = Convert.ToDateTime(sdr["PO_R_DATE"].ToString());
                        tempobj.C_NAME1 = sdr["C_NAME1"].ToString();
                        tempobj.VBELN = sdr["VBELN"].ToString();
                        tempobj.V_NAME1 = sdr["V_NAME1"].ToString();
                        //tempobj.SAP_PO_VER = sdr["SAP_PO_VER"].ToString();
                        //tempobj.HID_PO_VER = sdr["HID_PO_VER"].ToString();
                        //tempobj.PO_STATUS = sdr["PO_STATUS"].ToString();
                        tempobj.MTART = sdr["MTART"].ToString();
                        tempobj.T_ZTERM = sdr["T_ZTERM"].ToString();

                        tempobj.WAERS = sdr["WAERS"].ToString();
                        tempobj.T_AMOUNT = sdr["T_AMOUNT"].ToString();

                        objList.Add(tempobj);
                    }
                    sdr.Close();


                    sqlstr = "sp_GetLCAllCount";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@LIFNR", VenEmpId);
                    cmd.Parameters.AddWithValue("@ELIKZ", ELIKZ);
                    cmd.Parameters.AddWithValue("@SearchText", search);
                    sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        objModel.TotalRows = Convert.ToInt32(sdr["LCCount"].ToString());
                    }

                    con.Close();

                    objModel.LC_REQUEST_TABLE_Model_List = objList;

                }
                catch (Exception ex)
                {
                    ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/GetRequestForLCList");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/GetRequestForLCList");
            }
            return objModel;
        }
        
        public DBReturnModel InsertLCRequest(LC_Request_List_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting LC Request!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertLCRequest";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.LC_EBELN";
                    dataTable.Columns.Add("EBELN", typeof(long));
                    foreach (var dta in objModel.LC_Request_Model)
                    {
                        dataTable.Rows.Add(dta.EBELN); // Id of '1' is valid for the Person table                        
                    }
                    SqlParameter parameter = new SqlParameter("EBELNList", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "LC_Repository/InsertLCRequest");
                    objReturn.ReturnMessage = "System Error on Inserting LC Request!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "LC_Repository/InsertLCRequest");
                objReturn.ReturnMessage = "System Error on Inserting LC Request!";
            }

            return objReturn;
        }

        #region Code By Vijendra
        public List<PODetails_Model> GetPoDetailListByRefId(long createdBy, long refId, long userid)
        {
            List<PODetails_Model> objList = new List<PODetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailListByRefId]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@refId", refId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new PODetails_Model
                    {
                        SrNo = Convert.ToInt64(sdr["SLNO"]),
                        SAP_PO = Convert.ToInt64(sdr["EBELN"].ToString()),
                        SAP_PO_Line_Item = Convert.ToString(sdr["EBELP"].ToString()),
                        Material_SAP_Style_Number = Convert.ToString(sdr["MATNR"].ToString()),
                        Style_Description = Convert.ToString(sdr["TXZ01"].ToString()),
                        Buyer_PO_Line = Convert.ToString(sdr["BSTKD"].ToString()),
                        Colour = Convert.ToString(sdr["DIV1"].ToString()),
                        UOM_SAP = Convert.ToString(sdr["MEINS"].ToString()),
                        Qty_In_Pcs = Convert.ToString(sdr["MENGE"].ToString()),
                        Price_Per_Pc = Convert.ToString(sdr["VNETPR"].ToString()),
                        Value_In_USD = Convert.ToDecimal(sdr["ValueInUSD"].ToString()),
                        Shipment_Date = Convert.ToString(sdr["EINDT"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPoDetailListByRefId");
            }

            return objList;
        }

        public List<BusinessProcess_Model> GetPoDetailsByRefId(long createdBy, long refId, long userid)
        {
            //BusinessProcess_Model objData = new BusinessProcess_Model();
            List<BusinessProcess_Model> objList = new List<BusinessProcess_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                #region PoDeatils
                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailsByRefId]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@refId", refId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BusinessProcess_Model
                    {
                        Buyer = Convert.ToString(sdr["Buyer"].ToString()),
                        Manufacturing_Factory = Convert.ToString(sdr["Manuf_Factory"].ToString()),
                        Manufacturing_Address = Convert.ToString(sdr["Manuf_Address"].ToString()),
                        Shipment_Terms = Convert.ToString(sdr["Shipment_Term"].ToString()),
                        LC_Terms = Convert.ToString(sdr["LC_Term"].ToString()),
                        Notify_Party = Convert.ToString(sdr["Notify_Party"].ToString()),
                        Shipment_From_Country = Convert.ToString(sdr["Ship_From_Country"].ToString()),
                        Shipment_From_Port = Convert.ToString(sdr["Ship_From_Port"].ToString()),
                        Shipment_To_Country = Convert.ToString(sdr["Ship_To_Country"].ToString()),
                        Shipment_To_Port = Convert.ToString(sdr["Ship_To_Port"].ToString()),
                        Variation = Convert.ToString(sdr["Varation"].ToString()),
                        Expiry_Date = Convert.ToString(sdr["ExpiryDate"].ToString()),
                        Advising_Bank_Details = Convert.ToString(sdr["Bank_Details"].ToString()),
                    });
                }
                //sdr.Close();
                #endregion

                #region PoDetailList
                ////string sqlstr = "";
                //sqlstr = "[Sp_GetPoDetailListByRefId]";
                ////SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.Clear();
                //cmd.CommandText = sqlstr;
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Connection = con;
                //cmd.Parameters.AddWithValue("@refId", refId);
                //cmd.Parameters.AddWithValue("@userid", userid);
                // sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    objData.Buyer = Convert.ToString(sdr["Buyer"].ToString());
                //    objData.Manufacturing_Factory = Convert.ToString(sdr["Manuf_Factory"].ToString());
                //    objData.Shipment_Terms = Convert.ToString(sdr["Shipment_Term"].ToString());
                //    objData.LC_Terms = Convert.ToString(sdr["LC_Term"].ToString());
                //    objData.Notify_Party = Convert.ToString(sdr["Notify_Party"].ToString());
                //    objData.Shipment_From_Country = Convert.ToString(sdr["Ship_From_Country"].ToString());
                //    objData.Shipment_From_Port = Convert.ToString(sdr["Ship_From_Port"].ToString());
                //    objData.Shipment_To_Country = Convert.ToString(sdr["Ship_To_Country"].ToString());
                //    objData.Shipment_To_Port = Convert.ToString(sdr["Ship_To_Port"].ToString());
                //    objData.Variation = Convert.ToString(sdr["Varation"].ToString());
                //    objData.Expiry_Date = Convert.ToString(sdr["ExpiryDate"].ToString());
                //}

                #endregion
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPoDetailsByRefId");
            }
            return objList;
        }
        public string SaveGenerateTempPrint(BusinessProcess_Model objModel)
        {
            string result = "Error on Inserting PI Details..!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_SavePIDetailsTemp]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", objModel.CurrentUserId);
                    cmd.Parameters.AddWithValue("@ref_Id", objModel.ref_Id);
                    cmd.Parameters.AddWithValue("@Performa_Invoice_number", objModel.Performa_Invoice_number);
                    cmd.Parameters.AddWithValue("@Buyer", objModel.Buyer);
                    cmd.Parameters.AddWithValue("@Manufacturing_Factory", objModel.Manufacturing_Factory);
                    cmd.Parameters.AddWithValue("@Manufacturing_Address", objModel.Manufacturing_Address);
                    cmd.Parameters.AddWithValue("@Variation", objModel.Variation);
                    cmd.Parameters.AddWithValue("@Transhipment", objModel.Transhipment);
                    cmd.Parameters.AddWithValue("@Partial_Shipment", objModel.Partial_Shipment);
                    cmd.Parameters.AddWithValue("@Third_Party_Documnet", objModel.Third_Party_Documnet);
                    cmd.Parameters.AddWithValue("@Export_License", objModel.Export_License);
                    cmd.Parameters.AddWithValue("@Trims_Free_Of_Cost", objModel.Trims_Free_Of_Cost);
                    cmd.Parameters.AddWithValue("@Trims_Included_In_Factory_Cost", objModel.Trims_Included_In_Factory_Cost);
                    cmd.Parameters.AddWithValue("@Shipment_Terms", objModel.Shipment_Terms);
                    cmd.Parameters.AddWithValue("@LC_Terms", objModel.LC_Terms);
                    cmd.Parameters.AddWithValue("@Packing_Mode", objModel.Packing_Mode);
                    cmd.Parameters.AddWithValue("@Mode_Of_Transport", objModel.Mode_Of_Transport);
                    cmd.Parameters.AddWithValue("@Advising_Bank_Details", objModel.Advising_Bank_Details);
                    cmd.Parameters.AddWithValue("@Inspection_Certificate_Signed_By", objModel.Inspection_Certificate_Signed_By);
                    cmd.Parameters.AddWithValue("@Notify_Party", objModel.Notify_Party);
                    cmd.Parameters.AddWithValue("@GSP", objModel.GSP);
                    cmd.Parameters.AddWithValue("@Expiry_Date", objModel.Expiry_Date);
                    cmd.Parameters.AddWithValue("@Presentation_Period", objModel.Presentation_Period);
                    cmd.Parameters.AddWithValue("@Shipment_From_Country", objModel.Shipment_From_Country);
                    cmd.Parameters.AddWithValue("@Shipment_From_Port", objModel.Shipment_From_Port);
                    cmd.Parameters.AddWithValue("@Shipment_To_Country", objModel.Shipment_To_Country);
                    cmd.Parameters.AddWithValue("@Shipment_To_Port", objModel.Shipment_To_Port);
                    cmd.Parameters.AddWithValue("@BPMID", objModel.BPMID);
                    cmd.Parameters.AddWithValue("@StageID", objModel.StageId);
                    cmd.Parameters.AddWithValue("@ThreadId", objModel.StageId);


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CurrentUserId, "API", "LC_Repository/SaveGenerateTempPrint");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CurrentUserId, "API", "LC_Repository/SaveGenerateTempPrint");
            }

            return result;
        }

        public string SavePrintPIDetails(BusinessProcess_Model objModel)
        {
            string result = "Error on Inserting PI Details..!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_SavePIDetailsWhenPrint]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", objModel.CurrentUserId);
                    cmd.Parameters.AddWithValue("@ref_Id", objModel.ref_Id);
                    cmd.Parameters.AddWithValue("@Performa_Invoice_number", objModel.Performa_Invoice_number);
                    cmd.Parameters.AddWithValue("@Buyer", objModel.Buyer);
                    cmd.Parameters.AddWithValue("@Manufacturing_Factory", objModel.Manufacturing_Factory);
                    cmd.Parameters.AddWithValue("@Manufacturing_Address", objModel.Manufacturing_Address);
                    cmd.Parameters.AddWithValue("@Variation", objModel.Variation);
                    cmd.Parameters.AddWithValue("@Transhipment", objModel.Transhipment);
                    cmd.Parameters.AddWithValue("@Partial_Shipment", objModel.Partial_Shipment);
                    cmd.Parameters.AddWithValue("@Third_Party_Documnet", objModel.Third_Party_Documnet);
                    cmd.Parameters.AddWithValue("@Export_License", objModel.Export_License);
                    cmd.Parameters.AddWithValue("@Trims_Free_Of_Cost", objModel.Trims_Free_Of_Cost);
                    cmd.Parameters.AddWithValue("@Trims_Included_In_Factory_Cost", objModel.Trims_Included_In_Factory_Cost);
                    cmd.Parameters.AddWithValue("@Shipment_Terms", objModel.Shipment_Terms);
                    cmd.Parameters.AddWithValue("@LC_Terms", objModel.LC_Terms);
                    cmd.Parameters.AddWithValue("@Packing_Mode", objModel.Packing_Mode);
                    cmd.Parameters.AddWithValue("@Mode_Of_Transport", objModel.Mode_Of_Transport);
                    cmd.Parameters.AddWithValue("@Advising_Bank_Details", objModel.Advising_Bank_Details);
                    cmd.Parameters.AddWithValue("@Inspection_Certificate_Signed_By", objModel.Inspection_Certificate_Signed_By);
                    cmd.Parameters.AddWithValue("@Notify_Party", objModel.Notify_Party);
                    cmd.Parameters.AddWithValue("@GSP", objModel.GSP);
                    cmd.Parameters.AddWithValue("@Expiry_Date", objModel.Expiry_Date);
                    cmd.Parameters.AddWithValue("@Presentation_Period", objModel.Presentation_Period);
                    cmd.Parameters.AddWithValue("@Shipment_From_Country", objModel.Shipment_From_Country);
                    cmd.Parameters.AddWithValue("@Shipment_From_Port", objModel.Shipment_From_Port);
                    cmd.Parameters.AddWithValue("@Shipment_To_Country", objModel.Shipment_To_Country);
                    cmd.Parameters.AddWithValue("@Shipment_To_Port", objModel.Shipment_To_Port);
                    cmd.Parameters.AddWithValue("@BPMID", objModel.BPMID);
                    cmd.Parameters.AddWithValue("@StageID", objModel.StageId);

                    //cmd.Parameters.AddWithValue("@AuditClientName", objModel.AuditClientName.Trim(new Char[] { '[', '"', ']' }));
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CurrentUserId, "API", "LC_Repository/SavePrintPIDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CurrentUserId, "API", "LC_Repository/SavePrintPIDetails");
            }

            return result;
        }


        public List<PODetails_Model> GetPI_PRINT_DETAILS_TABLE_BySlNo(long createdBy, int slId, int userid)
        {
            List<PODetails_Model> objList = new List<PODetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailListByslIdForPrint]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@slId", slId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new PODetails_Model
                    {
                        SrNo = Convert.ToInt64(sdr["SLNO"]),
                        SAP_PO = Convert.ToInt64(sdr["SAP_PO"].ToString()),
                        SAP_PO_Line_Item = Convert.ToString(sdr["PO_LINEITEM"].ToString()),
                        Material_SAP_Style_Number = Convert.ToString(sdr["SAP_STYLE_NUMBER"].ToString()),
                        Style_Description = Convert.ToString(sdr["STYLE_DESCRIPTION"].ToString()),
                        Buyer_PO_Line = Convert.ToString(sdr["BUYER_PO_LINE"].ToString()),
                        Colour = Convert.ToString(sdr["COLOUR"].ToString()),
                        UOM_SAP = Convert.ToString(sdr["UOM_SAP"].ToString()),
                        Qty_In_Pcs = Convert.ToString(sdr["QTY_INPCS"].ToString()),
                        Price_Per_Pc = Convert.ToString(sdr["PRICE_INUSD"].ToString()),
                        Value_In_USD = Convert.ToDecimal(sdr["VALUE_INUSD"].ToString()),
                        Shipment_Date = Convert.ToString(sdr["SHIPMENT_DATE"].ToString()),
                        Remarks= Convert.ToString(sdr["REMARKS"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPI_PRINT_DETAILS_TABLE_BySlNo");
            }
            return objList;
        }

        public List<BusinessProcess_Model> GetPI_PRINT_TABLE_BySlNo(long createdBy, int slId, int userid)
        {
            List<BusinessProcess_Model> objList = new List<BusinessProcess_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailsBySlIdForPrint]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@slId", slId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BusinessProcess_Model
                    {
                        Performa_Invoice_number = Convert.ToString(sdr["P_INVOICE_NO"].ToString()),
                        Buyer = Convert.ToString(sdr["BUYER"].ToString()),
                        Manufacturing_Factory = Convert.ToString(sdr["MANUFACTURING_FACTORY"].ToString()),
                        Manufacturing_Address = Convert.ToString(sdr["MANUFACTURING_ADDRESS"].ToString()),
                        Shipment_Terms = Convert.ToString(sdr["SHIPMENT_TERMS"].ToString()),
                        LC_Terms = Convert.ToString(sdr["LC_TERMS"].ToString()),
                        Notify_Party = Convert.ToString(sdr["NOTIFY_PARTY"].ToString()),
                        Shipment_From_Country = Convert.ToString(sdr["SHIP_FROM_COUNTRY"].ToString()),
                        Shipment_From_Port = Convert.ToString(sdr["SHIP_FROM_PORT"].ToString()),
                        Shipment_To_Country = Convert.ToString(sdr["SHIP_TO_COUNTRY"].ToString()),
                        Shipment_To_Port = Convert.ToString(sdr["SHIP_TO_PORT"].ToString()),
                        Variation = Convert.ToString(sdr["VARIATION"].ToString()),
                        Expiry_Date = Convert.ToString(sdr["EXPIRY_DATE"].ToString()),
                        Advising_Bank_Details = Convert.ToString(sdr["ADVISING_BANK_DETAILS"].ToString()),
                        Transhipment = Convert.ToInt32(sdr["TRANSHIPMENT"]),
                        Partial_Shipment = Convert.ToInt32(sdr["PARTIAL_SHIPMENT"]),
                        Third_Party_Documnet = Convert.ToInt32(sdr["THIRED_PARTY_DOC"]),
                        Export_License = Convert.ToInt32(sdr["EXPORT_LICENSE"]),
                        Trims_Free_Of_Cost = Convert.ToInt32(sdr["T_FREE_OF_COST"]),
                        Trims_Included_In_Factory_Cost = Convert.ToInt32(sdr["T_INCLUDED_FACTORY_COST"]),
                        Presentation_Period = Convert.ToString(sdr["PRESENTATION_PERIOD"].ToString()),
                        Packing_Mode = Convert.ToString(sdr["PACKING_MODE"].ToString()),
                        Mode_Of_Transport = Convert.ToString(sdr["MODE_OF_TRANSPORT"].ToString()),
                        Inspection_Certificate_Signed_By = Convert.ToString(sdr["INSPECTION_SIGNEDBY"].ToString()),
                        GSP = Convert.ToString(sdr["GSP"].ToString()),
                        Total_Qty = Convert.ToString(sdr["TotalQty"].ToString()),
                        Value_InUSD = Convert.ToString(sdr["Total_ValueInUSD"].ToString()),
                        Value_InWords = Convert.ToString(sdr["AmountInWord"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPI_PRINT_TABLE_BySlNo");
            }
            return objList;
        }

        public List<PODetails_Model> GetPI_PRINT_DETAILS_TABLE_BySlNo_Temp(long createdBy, int slId, int userid)
        {
            List<PODetails_Model> objList = new List<PODetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailListByslIdForPrint_Temp]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@slId", slId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new PODetails_Model
                    {
                        SrNo = Convert.ToInt64(sdr["SLNO"]),
                        SAP_PO = Convert.ToInt64(sdr["SAP_PO"].ToString()),
                        SAP_PO_Line_Item = Convert.ToString(sdr["PO_LINEITEM"].ToString()),
                        Material_SAP_Style_Number = Convert.ToString(sdr["SAP_STYLE_NUMBER"].ToString()),
                        Style_Description = Convert.ToString(sdr["STYLE_DESCRIPTION"].ToString()),
                        Buyer_PO_Line = Convert.ToString(sdr["BUYER_PO_LINE"].ToString()),
                        Colour = Convert.ToString(sdr["COLOUR"].ToString()),
                        UOM_SAP = Convert.ToString(sdr["UOM_SAP"].ToString()),
                        Qty_In_Pcs = Convert.ToString(sdr["QTY_INPCS"].ToString()),
                        Price_Per_Pc = Convert.ToString(sdr["PRICE_INUSD"].ToString()),
                        Value_In_USD = Convert.ToDecimal(sdr["VALUE_INUSD"].ToString()),
                        Shipment_Date = Convert.ToString(sdr["SHIPMENT_DATE"].ToString()),
                        Remarks = Convert.ToString(sdr["REMARKS"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPI_PRINT_DETAILS_TABLE_BySlNo");
            }
            return objList;
        }

        public List<BusinessProcess_Model> GetPI_PRINT_TABLE_BySlNo_Temp(long createdBy, int slId, int userid)
        {
            List<BusinessProcess_Model> objList = new List<BusinessProcess_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "";
                sqlstr = "[Sp_GetPoDetailsBySlIdForPrint_Temp]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@slId", slId);
                cmd.Parameters.AddWithValue("@userid", userid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BusinessProcess_Model
                    {
                        Performa_Invoice_number = Convert.ToString(sdr["P_INVOICE_NO"].ToString()),
                        Buyer = Convert.ToString(sdr["BUYER"].ToString()),
                        Manufacturing_Factory = Convert.ToString(sdr["MANUFACTURING_FACTORY"].ToString()),
                        Manufacturing_Address = Convert.ToString(sdr["MANUFACTURING_ADDRESS"].ToString()),
                        Shipment_Terms = Convert.ToString(sdr["SHIPMENT_TERMS"].ToString()),
                        LC_Terms = Convert.ToString(sdr["LC_TERMS"].ToString()),
                        Notify_Party = Convert.ToString(sdr["NOTIFY_PARTY"].ToString()),
                        Shipment_From_Country = Convert.ToString(sdr["SHIP_FROM_COUNTRY"].ToString()),
                        Shipment_From_Port = Convert.ToString(sdr["SHIP_FROM_PORT"].ToString()),
                        Shipment_To_Country = Convert.ToString(sdr["SHIP_TO_COUNTRY"].ToString()),
                        Shipment_To_Port = Convert.ToString(sdr["SHIP_TO_PORT"].ToString()),
                        Variation = Convert.ToString(sdr["VARIATION"].ToString()),
                        Expiry_Date = Convert.ToString(sdr["EXPIRY_DATE"].ToString()),
                        Advising_Bank_Details = Convert.ToString(sdr["ADVISING_BANK_DETAILS"].ToString()),
                        Transhipment = Convert.ToInt32(sdr["TRANSHIPMENT"]),
                        Partial_Shipment = Convert.ToInt32(sdr["PARTIAL_SHIPMENT"]),
                        Third_Party_Documnet = Convert.ToInt32(sdr["THIRED_PARTY_DOC"]),
                        Export_License = Convert.ToInt32(sdr["EXPORT_LICENSE"]),
                        Trims_Free_Of_Cost = Convert.ToInt32(sdr["T_FREE_OF_COST"]),
                        Trims_Included_In_Factory_Cost = Convert.ToInt32(sdr["T_INCLUDED_FACTORY_COST"]),
                        Presentation_Period = Convert.ToString(sdr["PRESENTATION_PERIOD"].ToString()),
                        Packing_Mode = Convert.ToString(sdr["PACKING_MODE"].ToString()),
                        Mode_Of_Transport = Convert.ToString(sdr["MODE_OF_TRANSPORT"].ToString()),
                        Inspection_Certificate_Signed_By = Convert.ToString(sdr["INSPECTION_SIGNEDBY"].ToString()),
                        GSP = Convert.ToString(sdr["GSP"].ToString()),
                        Total_Qty = Convert.ToString(sdr["TotalQty"].ToString()),
                        Value_InUSD = Convert.ToString(sdr["Total_ValueInUSD"].ToString()),
                        Value_InWords = Convert.ToString(sdr["AmountInWord"].ToString()),
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetPI_PRINT_TABLE_BySlNo");
            }
            return objList;
        }

        public List<LC_Process> ListAllLCSubmission(int pageSize, int startRowIndex, string search, string sort_col, string sort_dir, long CreatedBy, string CompId, string VenEmpId, string ELIKZ)
        {
            LC_RequestLC_Pagination objModel = new LC_RequestLC_Pagination();
            List<LC_Process> objList = new List<LC_Process>();
            try
            {
                if (ELIKZ == null)
                {
                    ELIKZ = "";
                }
                if (search == null)
                {
                    search = "";
                }
                if (sort_dir == null)
                {
                    sort_dir = "ASC";
                }
                if (sort_col == null)
                {
                    sort_col = "entryid";
                }
                if (VenEmpId == "" || VenEmpId == null)
                {
                    VenEmpId = "404691";
                }
                try
                {
                    var con = SqlHelper.Connection();
                    con.Open();
                    string sqlstr = "sp_ListAllLCSubmission";
                    SqlCommand cmd = new SqlCommand(sqlstr, con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LIFNR", VenEmpId);
                    cmd.Parameters.AddWithValue("@ELIKZ", ELIKZ);
                    cmd.Parameters.AddWithValue("@SearchText", search);
                    cmd.Parameters.AddWithValue("@Page", startRowIndex);
                    cmd.Parameters.AddWithValue("@PageData", pageSize);

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        LC_Process tempobj = new LC_Process();
                        tempobj.LC_REF_ID = Convert.ToInt64(sdr["LC_REF_ID"].ToString());
                        tempobj.EBELN = sdr["EBELN"].ToString();
                        tempobj.C_NAME1 = sdr["C_NAME1"].ToString();
                        tempobj.WAERS = sdr["WAERS"].ToString();
                        tempobj.T_AMOUNT = sdr["T_AMOUNT"].ToString();

                        objList.Add(tempobj);
                    }
                    sdr.Close();

                    con.Close();

                    objModel.LC_Process_List = objList;

                }
                catch (Exception ex)
                {
                    ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/ListAllLCProcess");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/ListAllLCProcess");
            }
            return objList;
        }

        public List<LC_Process> ListAllLCProcess(int pageSize, int startRowIndex, string search, string sort_col, string sort_dir, long CreatedBy, string CompId, string VenEmpId, string ELIKZ)
        {
            LC_RequestLC_Pagination objModel = new LC_RequestLC_Pagination();
            List<LC_Process> objList = new List<LC_Process>();
            try
            {
                if (ELIKZ == null)
                {
                    ELIKZ = "";
                }
                if (search == null)
                {
                    search = "";
                }
                if (sort_dir == null)
                {
                    sort_dir = "ASC";
                }
                if (sort_col == null)
                {
                    sort_col = "entryid";
                }
                if (VenEmpId == "" || VenEmpId == null)
                {
                    VenEmpId = "404691";
                }
                try
                {
                    var con = SqlHelper.Connection();
                    con.Open();
                    string sqlstr = "sp_ListAllLCProcess";
                    SqlCommand cmd = new SqlCommand(sqlstr, con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@LIFNR", VenEmpId);
                    cmd.Parameters.AddWithValue("@ELIKZ", ELIKZ);
                    cmd.Parameters.AddWithValue("@SearchText", search);
                    cmd.Parameters.AddWithValue("@Page", startRowIndex);
                    cmd.Parameters.AddWithValue("@PageData", pageSize);

                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        LC_Process tempobj = new LC_Process();
                        tempobj.LC_REF_ID = Convert.ToInt64(sdr["LC_REF_ID"].ToString());
                        tempobj.EBELN = sdr["EBELN"].ToString();
                        tempobj.C_NAME1 = sdr["C_NAME1"].ToString();
                        tempobj.WAERS = sdr["WAERS"].ToString();
                        tempobj.T_AMOUNT = sdr["T_AMOUNT"].ToString();

                        objList.Add(tempobj);
                    }
                    sdr.Close();                   

                    con.Close();

                    objModel.LC_Process_List = objList;

                }
                catch (Exception ex)
                {
                    ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/ListAllLCProcess");
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "LC_Repository/ListAllLCProcess");
            }
            return objList;
        }
        
        public DBReturnModel RemovePOsFromPI(long currentUserId, long refId, string pos)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting LC Request!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    List<string> poNos = pos.Split(',').ToList();
                    string sqlstr = "SP_RemovePOsFromPI";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@currentUserId", currentUserId);
                    cmd.Parameters.AddWithValue("@refId", refId);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.LC_EBELN";
                    dataTable.Columns.Add("EBELN", typeof(string)); 
                    foreach (var dta in poNos)
                    {
                        dataTable.Rows.Add(dta); // Id of '1' is valid for the Person table                        
                    }
                    SqlParameter parameter = new SqlParameter("EBELNList", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, currentUserId, "API", "LC_Repository/RemovePOsFromPI");
                    objReturn.ReturnMessage = "System Error on Removing PO..!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, currentUserId, "API", "LC_Repository/RemovePOsFromPI");
                objReturn.ReturnMessage = "System Error on Removing PO..!";
            }

            return objReturn;
        }

        public List<BankFormDetails_Model> GetBankDetailsTofillForm(long createdBy, long ref_Id, long bPMID, long stageId, long threadId)
        {
            List<BankFormDetails_Model> objList = new List<BankFormDetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                #region PoDeatils
                string sqlstr = "";
                sqlstr = "[GetBankFormDetails]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@ref_Id", ref_Id);
                cmd.Parameters.AddWithValue("@CurrentUserId", createdBy);
                cmd.Parameters.AddWithValue("@BPMID", bPMID);
                cmd.Parameters.AddWithValue("@StageId", stageId);
                cmd.Parameters.AddWithValue("@ThreadId", threadId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BankFormDetails_Model
                    {
                        ref_Id = Convert.ToInt64(sdr["LC_REF_NO"]),
                        ApplicantName = Convert.ToString(sdr["ApplicantName"].ToString()),
                        ApplicantAddress = Convert.ToString(sdr["ApplicantAddress"].ToString()),
                        BeneficiaryName = Convert.ToString(sdr["BenName"].ToString()),
                        BeneficiaryAddress = Convert.ToString(sdr["BenAddress"].ToString()),
                        DCCurrency = Convert.ToString(sdr["Currency"].ToString()),
                        DCAmount = Convert.ToString(sdr["Amount"].ToString()),
                        EXPIRY_DATE = Convert.ToString(sdr["EXPIRY_DATE"].ToString()),
                        PARTIAL_SHIPMENT = Convert.ToString(sdr["PARTIAL_SHIPMENT"].ToString().Trim()),
                        AllowanceInQty = Convert.ToString(sdr["AllowanceInQty"].ToString()),
                        TRANSHIPMENT = Convert.ToString(sdr["TRANSHIPMENT"].ToString().Trim()),
                        DCTenorDays = Convert.ToString(sdr["DCTenor"].ToString()),
                        AdvisingSwiftCode = Convert.ToString(sdr["SwiftCode"].ToString()),
                        AdvisingBankName = Convert.ToString(sdr["BankName"].ToString()),
                        AdvisingBankAddress = Convert.ToString(sdr["BankAddress"].ToString()),
                        SHIP_FROM_COUNTRY = Convert.ToString(sdr["SHIP_FROM_COUNTRY"].ToString()),
                        SHIP_FROM_PORT = Convert.ToString(sdr["SHIP_FROM_PORT"].ToString()),
                        SHIP_TO_COUNTRY = Convert.ToString(sdr["SHIP_TO_COUNTRY"].ToString()),
                        SHIP_TO_PORT = Convert.ToString(sdr["SHIP_TO_PORT"].ToString()),
                        LastShipmentDate = Convert.ToString(sdr["LastShipmentDate"].ToString()),
                        Presentation_Period = Convert.ToInt32(sdr["PRESENTATION_PERIOD"]),
                    });
                }
                #endregion

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetBankDetailsTofillForm");
            }
            return objList;
        }

        public string SaveBankFormDetails(BankFormDetails_Model objModel, long CurrentUserId)
        {
            string result = "Error on Inserting PI Details..!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_SaveBankFormDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CurrentUserId", CurrentUserId);
                    cmd.Parameters.AddWithValue("@ref_Id", objModel.ref_Id);
                    cmd.Parameters.AddWithValue("@BPMID", objModel.BPMID);
                    cmd.Parameters.AddWithValue("@StageId", objModel.StageId);
                    cmd.Parameters.AddWithValue("@ThreadId", objModel.ThreadId);
                    cmd.Parameters.AddWithValue("@BankName", objModel.BankName);
                    cmd.Parameters.AddWithValue("@DateOfApplication", objModel.DateOfApplication);
                    cmd.Parameters.AddWithValue("@ApplicantName", objModel.ApplicantName);
                    cmd.Parameters.AddWithValue("@ApplicantAddress", objModel.ApplicantAddress);
                    cmd.Parameters.AddWithValue("@ApplicantContactPerson", objModel.ApplicantContactPerson);
                    cmd.Parameters.AddWithValue("@ApplicantTel", objModel.ApplicantTel);
                    cmd.Parameters.AddWithValue("@ImportAccountNo", objModel.ImportAccountNo);
                    cmd.Parameters.AddWithValue("@BeneficiaryName", objModel.BeneficiaryName);
                    cmd.Parameters.AddWithValue("@BeneficiaryAddress", objModel.BeneficiaryAddress);
                    cmd.Parameters.AddWithValue("@BeneficiaryContactPerson", objModel.BeneficiaryContactPerson);
                    cmd.Parameters.AddWithValue("@BenificiaryTel", objModel.BenificiaryTel);
                    cmd.Parameters.AddWithValue("@BenificiaryFax", objModel.BenificiaryFax);
                    cmd.Parameters.AddWithValue("@DCDispatchBy", objModel.DCDispatchBy);
                    cmd.Parameters.AddWithValue("@DCNumber", objModel.DCNumber);
                    cmd.Parameters.AddWithValue("@DCCurrency", objModel.DCCurrency);
                    cmd.Parameters.AddWithValue("@DCAmount", objModel.DCAmount);
                    cmd.Parameters.AddWithValue("@DCAmount_Currency", objModel.DCAmount_Currency);
                    cmd.Parameters.AddWithValue("@EXPIRY_DATE", objModel.EXPIRY_DATE);
                    cmd.Parameters.AddWithValue("@Expiry_Place", objModel.Expiry_Place);
                    cmd.Parameters.AddWithValue("@PARTIAL_SHIPMENT", objModel.PARTIAL_SHIPMENT);
                    cmd.Parameters.AddWithValue("@AllowanceInDCAmount", objModel.AllowanceInDCAmount);
                    cmd.Parameters.AddWithValue("@AllowanceInQty", objModel.AllowanceInQty);
                    cmd.Parameters.AddWithValue("@Confirmation", objModel.Confirmation);
                    cmd.Parameters.AddWithValue("@DCAvailableWith", objModel.DCAvailableWith);
                    cmd.Parameters.AddWithValue("@TRANSHIPMENT", objModel.TRANSHIPMENT);
                    cmd.Parameters.AddWithValue("@DraftRequired", objModel.DraftRequired);
                    cmd.Parameters.AddWithValue("@DCTenor", objModel.DCTenor);
                    cmd.Parameters.AddWithValue("@DCTenorDays", objModel.DCTenorDays);
                    cmd.Parameters.AddWithValue("@DCAvailableBy", objModel.DCAvailableBy);
                    cmd.Parameters.AddWithValue("@Transferable", objModel.Transferable);
                    cmd.Parameters.AddWithValue("@Presentation_Period", objModel.Presentation_Period);
                    cmd.Parameters.AddWithValue("@AdvisingSwiftCode", objModel.AdvisingSwiftCode);
                    cmd.Parameters.AddWithValue("@AdvisingBankName", objModel.AdvisingBankName);
                    cmd.Parameters.AddWithValue("@AdvisingBankAddress", objModel.AdvisingBankAddress);
                    cmd.Parameters.AddWithValue("@DescriptionOfGoods", objModel.DescriptionOfGoods);
                    cmd.Parameters.AddWithValue("@SHIP_FROM_COUNTRY", objModel.SHIP_FROM_COUNTRY);
                    cmd.Parameters.AddWithValue("@SHIP_FROM_PORT", objModel.SHIP_FROM_PORT);
                    cmd.Parameters.AddWithValue("@SHIP_TO_PORT", objModel.SHIP_TO_PORT);
                    cmd.Parameters.AddWithValue("@SHIP_TO_COUNTRY", objModel.SHIP_TO_COUNTRY);
                    cmd.Parameters.AddWithValue("@LastShipmentDate", objModel.LastShipmentDate);
                    cmd.Parameters.AddWithValue("@InsuranceCoveredBy", objModel.InsuranceCoveredBy);
                    cmd.Parameters.AddWithValue("@Incoterms", objModel.Incoterms);
                    cmd.Parameters.AddWithValue("@IncotermsOther", objModel.IncotermsOther);
                    cmd.Parameters.AddWithValue("@AccountNumber", objModel.AccountNumber);
                    cmd.Parameters.AddWithValue("@SignedCommercialOriginals", objModel.SignedCommercialOriginals);
                    cmd.Parameters.AddWithValue("@SignedCommercialCopies", objModel.SignedCommercialCopies);
                    cmd.Parameters.AddWithValue("@PackingListOriginals", objModel.PackingListOriginals);
                    cmd.Parameters.AddWithValue("@PackingListCopies", objModel.PackingListCopies);
                    cmd.Parameters.AddWithValue("@ShipmentBySea", objModel.ShipmentBySea);
                    cmd.Parameters.AddWithValue("@ShipmentByAir", objModel.ShipmentByAir);
                    cmd.Parameters.AddWithValue("@CargoReceipt", objModel.CargoReceipt);
                    cmd.Parameters.AddWithValue("@InsurancePolicy", objModel.InsurancePolicy);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificate", objModel.BeneficiaryCertificate);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificateDays", objModel.BeneficiaryCertificateDays);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificateType", objModel.BeneficiaryCertificateType);
                    cmd.Parameters.AddWithValue("@AdditionalDocuments", objModel.AdditionalDocuments);
                    cmd.Parameters.AddWithValue("@TTReimbursement", objModel.TTReimbursement);
                    cmd.Parameters.AddWithValue("@DCOpeningCommission", objModel.DCOpeningCommission);
                    cmd.Parameters.AddWithValue("@IssuingBankOtherCharges", objModel.IssuingBankOtherCharges);
                    cmd.Parameters.AddWithValue("@CorrespondentBankCharges", objModel.CorrespondentBankCharges);
                    cmd.Parameters.AddWithValue("@DCConfirmationCharges", objModel.DCConfirmationCharges);
                    cmd.Parameters.AddWithValue("@TransitInterestCharges", objModel.TransitInterestCharges);
                    cmd.Parameters.AddWithValue("@DelayedReimbursement", objModel.DelayedReimbursement);
                    cmd.Parameters.AddWithValue("@HKDBillCommission", objModel.HKDBillCommission);
                    cmd.Parameters.AddWithValue("@AccountNoCharges", objModel.AccountNoCharges);
                    cmd.Parameters.AddWithValue("@CurrencyAccount", objModel.CurrencyAccount);
                    cmd.Parameters.AddWithValue("@MasterCreditNumber", objModel.MasterCreditNumber);
                    cmd.Parameters.AddWithValue("@MasterCreditNumberIssuedBy", objModel.MasterCreditNumberIssuedBy);
                    cmd.Parameters.AddWithValue("@MasterDCBy", objModel.MasterDCBy);
                    cmd.Parameters.AddWithValue("@CollateralCash", objModel.CollateralCash);
                    cmd.Parameters.AddWithValue("@CollateralAmount", objModel.CollateralAmount);
                    cmd.Parameters.AddWithValue("@CollateralAccountNumber", objModel.CollateralAccountNumber);
                    cmd.Parameters.AddWithValue("@CollateralCurrency", objModel.CollateralCurrency);
                    cmd.Parameters.AddWithValue("@AdditionalInformation", objModel.AdditionalInformation);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);
                    //cmd.Parameters.AddWithValue("@", objModel.);

                    //cmd.Parameters.AddWithValue("@AuditClientName", objModel.AuditClientName.Trim(new Char[] { '[', '"', ']' }));
                    //cmd.Parameters.AddWithValue("@FrequencyDays", objModel.FrequencyDays);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CurrentUserId, "API", "LC_Repository/SaveBankFormDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CurrentUserId, "API", "LC_Repository/SaveBankFormDetails");
            }

            return result;
        }

        public string SaveBankFormDetailsTemp(BankFormDetails_Model objModel, long CurrentUserId)
        {
            string result = "Error on Inserting PI Details..!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[SP_SaveBankFormDetails_Temp]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CurrentUserId", CurrentUserId);
                    cmd.Parameters.AddWithValue("@ref_Id", objModel.ref_Id);
                    cmd.Parameters.AddWithValue("@BPMID", objModel.BPMID);
                    cmd.Parameters.AddWithValue("@StageId", objModel.StageId);
                    cmd.Parameters.AddWithValue("@ThreadId", objModel.ThreadId);
                    cmd.Parameters.AddWithValue("@BankName", objModel.BankName);
                    cmd.Parameters.AddWithValue("@DateOfApplication", objModel.DateOfApplication);
                    cmd.Parameters.AddWithValue("@ApplicantName", objModel.ApplicantName);
                    cmd.Parameters.AddWithValue("@ApplicantAddress", objModel.ApplicantAddress);
                    cmd.Parameters.AddWithValue("@ApplicantContactPerson", objModel.ApplicantContactPerson);
                    cmd.Parameters.AddWithValue("@ApplicantTel", objModel.ApplicantTel);
                    cmd.Parameters.AddWithValue("@ImportAccountNo", objModel.ImportAccountNo);
                    cmd.Parameters.AddWithValue("@BeneficiaryName", objModel.BeneficiaryName);
                    cmd.Parameters.AddWithValue("@BeneficiaryAddress", objModel.BeneficiaryAddress);
                    cmd.Parameters.AddWithValue("@BeneficiaryContactPerson", objModel.BeneficiaryContactPerson);
                    cmd.Parameters.AddWithValue("@BenificiaryTel", objModel.BenificiaryTel);
                    cmd.Parameters.AddWithValue("@BenificiaryFax", objModel.BenificiaryFax);
                    cmd.Parameters.AddWithValue("@DCDispatchBy", objModel.DCDispatchBy);
                    cmd.Parameters.AddWithValue("@DCNumber", objModel.DCNumber);
                    cmd.Parameters.AddWithValue("@DCCurrency", objModel.DCCurrency);
                    cmd.Parameters.AddWithValue("@DCAmount", objModel.DCAmount);
                    cmd.Parameters.AddWithValue("@DCAmount_Currency", objModel.DCAmount_Currency);
                    cmd.Parameters.AddWithValue("@EXPIRY_DATE", objModel.EXPIRY_DATE);
                    cmd.Parameters.AddWithValue("@Expiry_Place", objModel.Expiry_Place);
                    cmd.Parameters.AddWithValue("@PARTIAL_SHIPMENT", objModel.PARTIAL_SHIPMENT);
                    cmd.Parameters.AddWithValue("@AllowanceInDCAmount", objModel.AllowanceInDCAmount);
                    cmd.Parameters.AddWithValue("@AllowanceInQty", objModel.AllowanceInQty);
                    cmd.Parameters.AddWithValue("@Confirmation", objModel.Confirmation);
                    cmd.Parameters.AddWithValue("@DCAvailableWith", objModel.DCAvailableWith);
                    cmd.Parameters.AddWithValue("@TRANSHIPMENT", objModel.TRANSHIPMENT);
                    cmd.Parameters.AddWithValue("@DraftRequired", objModel.DraftRequired);
                    cmd.Parameters.AddWithValue("@DCTenor", objModel.DCTenor);
                    cmd.Parameters.AddWithValue("@DCTenorDays", objModel.DCTenorDays);
                    cmd.Parameters.AddWithValue("@DCAvailableBy", objModel.DCAvailableBy);
                    cmd.Parameters.AddWithValue("@Transferable", objModel.Transferable);
                    cmd.Parameters.AddWithValue("@Presentation_Period", objModel.Presentation_Period);
                    cmd.Parameters.AddWithValue("@AdvisingSwiftCode", objModel.AdvisingSwiftCode);
                    cmd.Parameters.AddWithValue("@AdvisingBankName", objModel.AdvisingBankName);
                    cmd.Parameters.AddWithValue("@AdvisingBankAddress", objModel.AdvisingBankAddress);
                    cmd.Parameters.AddWithValue("@DescriptionOfGoods", objModel.DescriptionOfGoods);
                    cmd.Parameters.AddWithValue("@SHIP_FROM_COUNTRY", objModel.SHIP_FROM_COUNTRY);
                    cmd.Parameters.AddWithValue("@SHIP_FROM_PORT", objModel.SHIP_FROM_PORT);
                    cmd.Parameters.AddWithValue("@SHIP_TO_PORT", objModel.SHIP_TO_PORT);
                    cmd.Parameters.AddWithValue("@SHIP_TO_COUNTRY", objModel.SHIP_TO_COUNTRY);
                    cmd.Parameters.AddWithValue("@LastShipmentDate", objModel.LastShipmentDate);
                    cmd.Parameters.AddWithValue("@InsuranceCoveredBy", objModel.InsuranceCoveredBy);
                    cmd.Parameters.AddWithValue("@Incoterms", objModel.Incoterms);
                    cmd.Parameters.AddWithValue("@IncotermsOther", objModel.IncotermsOther);
                    cmd.Parameters.AddWithValue("@AccountNumber", objModel.AccountNumber);
                    cmd.Parameters.AddWithValue("@SignedCommercialOriginals", objModel.SignedCommercialOriginals);
                    cmd.Parameters.AddWithValue("@SignedCommercialCopies", objModel.SignedCommercialCopies);
                    cmd.Parameters.AddWithValue("@PackingListOriginals", objModel.PackingListOriginals);
                    cmd.Parameters.AddWithValue("@PackingListCopies", objModel.PackingListCopies);
                    cmd.Parameters.AddWithValue("@ShipmentBySea", objModel.ShipmentBySea);
                    cmd.Parameters.AddWithValue("@ShipmentByAir", objModel.ShipmentByAir);
                    cmd.Parameters.AddWithValue("@CargoReceipt", objModel.CargoReceipt);
                    cmd.Parameters.AddWithValue("@InsurancePolicy", objModel.InsurancePolicy);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificate", objModel.BeneficiaryCertificate);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificateDays", objModel.BeneficiaryCertificateDays);
                    cmd.Parameters.AddWithValue("@BeneficiaryCertificateType", objModel.BeneficiaryCertificateType);
                    cmd.Parameters.AddWithValue("@AdditionalDocuments", objModel.AdditionalDocuments);
                    cmd.Parameters.AddWithValue("@TTReimbursement", objModel.TTReimbursement);
                    cmd.Parameters.AddWithValue("@DCOpeningCommission", objModel.DCOpeningCommission);
                    cmd.Parameters.AddWithValue("@IssuingBankOtherCharges", objModel.IssuingBankOtherCharges);
                    cmd.Parameters.AddWithValue("@CorrespondentBankCharges", objModel.CorrespondentBankCharges);
                    cmd.Parameters.AddWithValue("@DCConfirmationCharges", objModel.DCConfirmationCharges);
                    cmd.Parameters.AddWithValue("@TransitInterestCharges", objModel.TransitInterestCharges);
                    cmd.Parameters.AddWithValue("@DelayedReimbursement", objModel.DelayedReimbursement);
                    cmd.Parameters.AddWithValue("@HKDBillCommission", objModel.HKDBillCommission);
                    cmd.Parameters.AddWithValue("@AccountNoCharges", objModel.AccountNoCharges);
                    cmd.Parameters.AddWithValue("@CurrencyAccount", objModel.CurrencyAccount);
                    cmd.Parameters.AddWithValue("@MasterCreditNumber", objModel.MasterCreditNumber);
                    cmd.Parameters.AddWithValue("@MasterCreditNumberIssuedBy", objModel.MasterCreditNumberIssuedBy);
                    cmd.Parameters.AddWithValue("@MasterDCBy", objModel.MasterDCBy);
                    cmd.Parameters.AddWithValue("@CollateralCash", objModel.CollateralCash);
                    cmd.Parameters.AddWithValue("@CollateralAmount", objModel.CollateralAmount);
                    cmd.Parameters.AddWithValue("@CollateralAccountNumber", objModel.CollateralAccountNumber);
                    cmd.Parameters.AddWithValue("@CollateralCurrency", objModel.CollateralCurrency);
                    cmd.Parameters.AddWithValue("@AdditionalInformation", objModel.AdditionalInformation);
                    

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CurrentUserId, "API", "LC_Repository/SaveBankFormDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CurrentUserId, "API", "LC_Repository/SaveBankFormDetails");
            }

            return result;
        }

        public List<BankFormDetails_Model> GetBankFormDetailsToPrint(long createdBy, long bankDetail_Id, long bPMID, long stageId, long threadId)
        {
            List<BankFormDetails_Model> objList = new List<BankFormDetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                #region PoDeatils
                string sqlstr = "";
                sqlstr = "[Sp_GetBankFormDetailsToPrint]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@bankDetail_Id", bankDetail_Id);
                cmd.Parameters.AddWithValue("@CurrentUserId", createdBy);
                cmd.Parameters.AddWithValue("@BPMID", bPMID);
                cmd.Parameters.AddWithValue("@StageId", stageId);
                cmd.Parameters.AddWithValue("@ThreadId", threadId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BankFormDetails_Model
                    {
                        bDetail_Id = Convert.ToInt64(sdr["SLNO"]),
                        ref_Id = Convert.ToInt64(sdr["LC_REF_NO"]),
                        BPMID = Convert.ToInt64(sdr["BPMID"]),
                        StageId = Convert.ToInt64(sdr["StageId"]),
                        ThreadId = Convert.ToInt64(sdr["ThreadId"]),
                        BankName = Convert.ToString(sdr["BankName"].ToString()),
                        DateOfApplication = Convert.ToString(sdr["DateOfApplication"].ToString()),
                        ApplicantName = Convert.ToString(sdr["ApplicantName"].ToString()),
                        ApplicantAddress = Convert.ToString(sdr["ApplicantAddress"].ToString()),
                        ApplicantContactPerson = Convert.ToString(sdr["ApplicantContactPerson"].ToString()),
                        ApplicantTel = Convert.ToString(sdr["ApplicantTel"].ToString()),
                        ImportAccountNo = Convert.ToString(sdr["ImportAccountNo"].ToString()),
                        BeneficiaryName = Convert.ToString(sdr["BeneficiaryName"].ToString()),
                        BeneficiaryAddress = Convert.ToString(sdr["BeneficiaryAddress"].ToString()),
                        BeneficiaryContactPerson = Convert.ToString(sdr["BeneficiaryContactPerson"].ToString()),
                        BenificiaryTel = Convert.ToString(sdr["BenificiaryTel"].ToString()),
                        BenificiaryFax = Convert.ToString(sdr["BenificiaryFax"].ToString()),
                        DCDispatchBy = Convert.ToString(sdr["DCDispatchBy"].ToString().Trim()),
                        DCNumber = Convert.ToString(sdr["DCNumber"].ToString()),
                        DCCurrency = Convert.ToString(sdr["DCCurrency"].ToString().Trim()),
                        DCAmount = Convert.ToString(sdr["DCAmount"].ToString()),
                        DCAmount_Currency = Convert.ToString(sdr["DCAmount_Currency"].ToString()),
                        EXPIRY_DATE = Convert.ToString(sdr["EXPIRY_DATE"].ToString()),
                        Expiry_Place = Convert.ToString(sdr["Expiry_Place"].ToString()),
                        PARTIAL_SHIPMENT = Convert.ToString(sdr["PARTIAL_SHIPMENT"].ToString()),
                        AllowanceInDCAmount = Convert.ToString(sdr["AllowanceInDCAmount"].ToString()),
                        AllowanceInQty = Convert.ToString(sdr["AllowanceInQty"].ToString()),
                        Confirmation = Convert.ToString(sdr["Confirmation"].ToString()),
                        DCAvailableWith = Convert.ToString(sdr["DCAvailableWith"].ToString()),
                        TRANSHIPMENT = Convert.ToString(sdr["TRANSHIPMENT"].ToString()),
                        DraftRequired = Convert.ToString(sdr["DraftRequired"].ToString()),
                        DCTenor = Convert.ToString(sdr["DCTenor"].ToString()),
                        DCTenorDays = Convert.ToString(sdr["DCTenorDays"].ToString()),
                        DCAvailableBy = Convert.ToString(sdr["DCAvailableBy"].ToString()),
                        Transferable = Convert.ToString(sdr["Transferable"].ToString()),
                        Presentation_Period = Convert.ToInt32(sdr["Presentation_Period"].ToString()),
                        AdvisingSwiftCode = Convert.ToString(sdr["AdvisingSwiftCode"].ToString()),
                        AdvisingBankName = Convert.ToString(sdr["AdvisingBankName"].ToString()),
                        AdvisingBankAddress = Convert.ToString(sdr["AdvisingBankAddress"].ToString()),
                        DescriptionOfGoods = Convert.ToString(sdr["DescriptionOfGoods"].ToString()),
                        SHIP_FROM_COUNTRY = Convert.ToString(sdr["SHIP_FROM_COUNTRY"].ToString()),
                        SHIP_FROM_PORT = Convert.ToString(sdr["SHIP_FROM_PORT"].ToString()),
                        SHIP_TO_PORT = Convert.ToString(sdr["SHIP_TO_PORT"].ToString()),
                        SHIP_TO_COUNTRY = Convert.ToString(sdr["SHIP_TO_COUNTRY"].ToString()),
                        LastShipmentDate = Convert.ToString(sdr["LastShipmentDate"].ToString()),
                        InsuranceCoveredBy = Convert.ToString(sdr["InsuranceCoveredBy"].ToString()),
                        Incoterms = Convert.ToString(sdr["Incoterms"].ToString()),
                        IncotermsOther = Convert.ToString(sdr["IncotermsOther"].ToString()),
                        AccountNumber = Convert.ToString(sdr["AccountNumber"].ToString()),
                        SignedCommercialOriginals = Convert.ToString(sdr["SignedCommercialOriginals"].ToString()),
                        SignedCommercialCopies = Convert.ToString(sdr["SignedCommercialCopies"].ToString()),
                        PackingListOriginals = Convert.ToString(sdr["PackingListOriginals"].ToString()),
                        PackingListCopies = Convert.ToString(sdr["PackingListCopies"].ToString()),
                        ShipmentBySea = Convert.ToString(sdr["ShipmentBySea"].ToString()),
                        ShipmentByAir = Convert.ToString(sdr["ShipmentByAir"].ToString()),
                        CargoReceipt = Convert.ToString(sdr["CargoReceipt"].ToString()),
                        InsurancePolicy = Convert.ToString(sdr["InsurancePolicy"].ToString()),
                        BeneficiaryCertificate = Convert.ToString(sdr["BeneficiaryCertificate"].ToString()),
                        BeneficiaryCertificateDays = Convert.ToString(sdr["BeneficiaryCertificateDays"].ToString()),
                        BeneficiaryCertificateType = Convert.ToString(sdr["BeneficiaryCertificateType"].ToString()),
                        AdditionalDocuments = Convert.ToString(sdr["AdditionalDocuments"].ToString()),
                        TTReimbursement = Convert.ToString(sdr["TTReimbursement"].ToString()),
                        DCOpeningCommission = Convert.ToString(sdr["DCOpeningCommission"].ToString()),
                        IssuingBankOtherCharges = Convert.ToString(sdr["IssuingBankOtherCharges"].ToString()),
                        CorrespondentBankCharges = Convert.ToString(sdr["CorrespondentBankCharges"].ToString()),
                        DCConfirmationCharges = Convert.ToString(sdr["DCConfirmationCharges"].ToString()),
                        TransitInterestCharges = Convert.ToString(sdr["TransitInterestCharges"].ToString()),
                        DelayedReimbursement = Convert.ToString(sdr["DelayedReimbursement"].ToString()),
                        HKDBillCommission = Convert.ToString(sdr["HKDBillCommission"].ToString()),
                        AccountNoCharges = Convert.ToString(sdr["AccountNoCharges"].ToString()),
                        CurrencyAccount = Convert.ToString(sdr["CurrencyAccount"].ToString()),
                        MasterCreditNumber = Convert.ToString(sdr["MasterCreditNumber"].ToString()),
                        MasterCreditNumberIssuedBy = Convert.ToString(sdr["MasterCreditNumberIssuedBy"].ToString()),
                        MasterDCBy = Convert.ToString(sdr["MasterDCBy"].ToString()),
                        CollateralCash = Convert.ToString(sdr["CollateralCash"].ToString()),
                        CollateralAmount = Convert.ToString(sdr["CollateralAmount"].ToString()),
                        CollateralAccountNumber = Convert.ToString(sdr["CollateralAccountNumber"].ToString()),
                        CollateralCurrency = Convert.ToString(sdr["CollateralCurrency"].ToString()),
                        AdditionalInformation = Convert.ToString(sdr["AdditionalInformation"].ToString()),
                        Inspection_SignedBY = Convert.ToString(sdr["INSPECTION_SIGNEDBY"].ToString())
                    });
                }
                #endregion

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetBankFormDetailsToPrint");
            }
            return objList;
        }

        public List<BankFormDetails_Model> GetBankFormDetailsToPrintTemp(long createdBy, long bankDetail_Id)
        {
            List<BankFormDetails_Model> objList = new List<BankFormDetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                #region PoDeatils
                string sqlstr = "";
                sqlstr = "[Sp_GetBankFormDetailsToPrint_Temp]";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@bankDetail_Id", bankDetail_Id);
                cmd.Parameters.AddWithValue("@CurrentUserId", createdBy);
                //cmd.Parameters.AddWithValue("@BPMID", bPMID);
                //cmd.Parameters.AddWithValue("@StageId", stageId);
                //cmd.Parameters.AddWithValue("@ThreadId", threadId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new BankFormDetails_Model
                    {
                        bDetail_Id = Convert.ToInt64(sdr["SLNO"]),
                        ref_Id = Convert.ToInt64(sdr["LC_REF_NO"]),
                        BPMID = Convert.ToInt64(sdr["BPMID"]),
                        StageId = Convert.ToInt64(sdr["StageId"]),
                        ThreadId = Convert.ToInt64(sdr["ThreadId"]),
                        BankName = Convert.ToString(sdr["BankName"].ToString()),
                        DateOfApplication = Convert.ToString(sdr["DateOfApplication"].ToString()),
                        ApplicantName = Convert.ToString(sdr["ApplicantName"].ToString()),
                        ApplicantAddress = Convert.ToString(sdr["ApplicantAddress"].ToString()),
                        ApplicantContactPerson = Convert.ToString(sdr["ApplicantContactPerson"].ToString()),
                        ApplicantTel = Convert.ToString(sdr["ApplicantTel"].ToString()),
                        ImportAccountNo = Convert.ToString(sdr["ImportAccountNo"].ToString()),
                        BeneficiaryName = Convert.ToString(sdr["BeneficiaryName"].ToString()),
                        BeneficiaryAddress = Convert.ToString(sdr["BeneficiaryAddress"].ToString()),
                        BeneficiaryContactPerson = Convert.ToString(sdr["BeneficiaryContactPerson"].ToString()),
                        BenificiaryTel = Convert.ToString(sdr["BenificiaryTel"].ToString()),
                        BenificiaryFax = Convert.ToString(sdr["BenificiaryFax"].ToString()),
                        DCDispatchBy = Convert.ToString(sdr["DCDispatchBy"].ToString().Trim()),
                        DCNumber = Convert.ToString(sdr["DCNumber"].ToString()),
                        DCCurrency = Convert.ToString(sdr["DCCurrency"].ToString().Trim()),
                        DCAmount = Convert.ToString(sdr["DCAmount"].ToString()),
                        DCAmount_Currency = Convert.ToString(sdr["DCAmount_Currency"].ToString()),
                        EXPIRY_DATE = Convert.ToString(sdr["EXPIRY_DATE"].ToString()),
                        Expiry_Place = Convert.ToString(sdr["Expiry_Place"].ToString()),
                        PARTIAL_SHIPMENT = Convert.ToString(sdr["PARTIAL_SHIPMENT"].ToString()),
                        AllowanceInDCAmount = Convert.ToString(sdr["AllowanceInDCAmount"].ToString()),
                        AllowanceInQty = Convert.ToString(sdr["AllowanceInQty"].ToString()),
                        Confirmation = Convert.ToString(sdr["Confirmation"].ToString()),
                        DCAvailableWith = Convert.ToString(sdr["DCAvailableWith"].ToString()),
                        TRANSHIPMENT = Convert.ToString(sdr["TRANSHIPMENT"].ToString()),
                        DraftRequired = Convert.ToString(sdr["DraftRequired"].ToString()),
                        DCTenor = Convert.ToString(sdr["DCTenor"].ToString()),
                        DCTenorDays = Convert.ToString(sdr["DCTenorDays"].ToString()),
                        DCAvailableBy = Convert.ToString(sdr["DCAvailableBy"].ToString()),
                        Transferable = Convert.ToString(sdr["Transferable"].ToString()),
                        Presentation_Period = Convert.ToInt32(sdr["Presentation_Period"].ToString()),
                        AdvisingSwiftCode = Convert.ToString(sdr["AdvisingSwiftCode"].ToString()),
                        AdvisingBankName = Convert.ToString(sdr["AdvisingBankName"].ToString()),
                        AdvisingBankAddress = Convert.ToString(sdr["AdvisingBankAddress"].ToString()),
                        DescriptionOfGoods = Convert.ToString(sdr["DescriptionOfGoods"].ToString()),
                        SHIP_FROM_COUNTRY = Convert.ToString(sdr["SHIP_FROM_COUNTRY"].ToString()),
                        SHIP_FROM_PORT = Convert.ToString(sdr["SHIP_FROM_PORT"].ToString()),
                        SHIP_TO_PORT = Convert.ToString(sdr["SHIP_TO_PORT"].ToString()),
                        SHIP_TO_COUNTRY = Convert.ToString(sdr["SHIP_TO_COUNTRY"].ToString()),
                        LastShipmentDate = Convert.ToString(sdr["LastShipmentDate"].ToString()),
                        InsuranceCoveredBy = Convert.ToString(sdr["InsuranceCoveredBy"].ToString()),
                        Incoterms = Convert.ToString(sdr["Incoterms"].ToString()),
                        IncotermsOther = Convert.ToString(sdr["IncotermsOther"].ToString()),
                        AccountNumber = Convert.ToString(sdr["AccountNumber"].ToString()),
                        SignedCommercialOriginals = Convert.ToString(sdr["SignedCommercialOriginals"].ToString()),
                        SignedCommercialCopies = Convert.ToString(sdr["SignedCommercialCopies"].ToString()),
                        PackingListOriginals = Convert.ToString(sdr["PackingListOriginals"].ToString()),
                        PackingListCopies = Convert.ToString(sdr["PackingListCopies"].ToString()),
                        ShipmentBySea = Convert.ToString(sdr["ShipmentBySea"].ToString()),
                        ShipmentByAir = Convert.ToString(sdr["ShipmentByAir"].ToString()),
                        CargoReceipt = Convert.ToString(sdr["CargoReceipt"].ToString()),
                        InsurancePolicy = Convert.ToString(sdr["InsurancePolicy"].ToString()),
                        BeneficiaryCertificate = Convert.ToString(sdr["BeneficiaryCertificate"].ToString()),
                        BeneficiaryCertificateDays = Convert.ToString(sdr["BeneficiaryCertificateDays"].ToString()),
                        BeneficiaryCertificateType = Convert.ToString(sdr["BeneficiaryCertificateType"].ToString()),
                        AdditionalDocuments = Convert.ToString(sdr["AdditionalDocuments"].ToString()),
                        TTReimbursement = Convert.ToString(sdr["TTReimbursement"].ToString()),
                        DCOpeningCommission = Convert.ToString(sdr["DCOpeningCommission"].ToString()),
                        IssuingBankOtherCharges = Convert.ToString(sdr["IssuingBankOtherCharges"].ToString()),
                        CorrespondentBankCharges = Convert.ToString(sdr["CorrespondentBankCharges"].ToString()),
                        DCConfirmationCharges = Convert.ToString(sdr["DCConfirmationCharges"].ToString()),
                        TransitInterestCharges = Convert.ToString(sdr["TransitInterestCharges"].ToString()),
                        DelayedReimbursement = Convert.ToString(sdr["DelayedReimbursement"].ToString()),
                        HKDBillCommission = Convert.ToString(sdr["HKDBillCommission"].ToString()),
                        AccountNoCharges = Convert.ToString(sdr["AccountNoCharges"].ToString()),
                        CurrencyAccount = Convert.ToString(sdr["CurrencyAccount"].ToString()),
                        MasterCreditNumber = Convert.ToString(sdr["MasterCreditNumber"].ToString()),
                        MasterCreditNumberIssuedBy = Convert.ToString(sdr["MasterCreditNumberIssuedBy"].ToString()),
                        MasterDCBy = Convert.ToString(sdr["MasterDCBy"].ToString()),
                        CollateralCash = Convert.ToString(sdr["CollateralCash"].ToString()),
                        CollateralAmount = Convert.ToString(sdr["CollateralAmount"].ToString()),
                        CollateralAccountNumber = Convert.ToString(sdr["CollateralAccountNumber"].ToString()),
                        CollateralCurrency = Convert.ToString(sdr["CollateralCurrency"].ToString()),
                        AdditionalInformation = Convert.ToString(sdr["AdditionalInformation"].ToString()),
                        Inspection_SignedBY = Convert.ToString(sdr["INSPECTION_SIGNEDBY"].ToString())
                    });
                }
                #endregion

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "LC_Repository/GetBankFormDetailsToPrintTemp");
            }
            return objList;
        }

        public List<BankAccountNumber> GetAccountNumberByBankAndCurrency(string bankName, string currency)
        {
            List<BankAccountNumber> objRowsList = new List<BankAccountNumber>();
            #region BCI
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BCI",
                Currency = "HKD",
                AccountNumber= "08240-001-0001"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BCI",
                Currency = "USD",
                AccountNumber = "08240-001-0002"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BCI",
                Currency = "EUR",
                AccountNumber = "08240-001-0003"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BCI",
                Currency = "GBP",
                AccountNumber = "08240-001-0004"
            });
                        #endregion


            #region ICICI
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "ICICI",
                Currency = "HKD",
                AccountNumber = "852028457"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "ICICI",
                Currency = "USD",
                AccountNumber = "852028456"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "ICICI",
                Currency = "EUR",
                AccountNumber = "852028459"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "ICICI",
                Currency = "GBP",
                AccountNumber = "852028458"
            });

            #endregion

            #region BNP Current

            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "HKD",
                AccountNumber = "00001-208440-001-95"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "USD",
                AccountNumber = "00001-208440-001-95"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "EUR",
                AccountNumber = "00001-208440-001-95"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "GBP",
                AccountNumber = "00001-208440-001-95"
            });

            #endregion

            #region BNP Call Deposite
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "HKD",
                AccountNumber = "00001-208440-008-74"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "USD",
                AccountNumber = "00001-208440-008-74"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "EUR",
                AccountNumber = "00001-208440-008-74"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "BNP",
                Currency = "GBP",
                AccountNumber = "00001-208440-008-74"
            });

            #endregion

            #region HSBC
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "HSBC",
                Currency = "HKD",
                AccountNumber = "848-773537-001"
            });
            objRowsList.Add(new BankAccountNumber
            {
                Bank = "HSBC",
                Currency = "USD",
                AccountNumber = "848-773537-274"
            });
            #endregion
            return objRowsList;
        }























        #endregion
    }
}
