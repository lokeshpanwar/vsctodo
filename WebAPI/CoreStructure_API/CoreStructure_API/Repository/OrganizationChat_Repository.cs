﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class OrganizationChat_Repository
    {

        public List<OrganizationChat_Model> GetOrganizationChat(long CreatedBy, string empnm)
        {
            List<OrganizationChat_Model> objModel = new List<OrganizationChat_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                if (empnm == null)
                {
                    empnm = "";
                }


                string sqlstr = "Select 0 as Level,Company_Name,'P'+convert(varchar(10),SLNO)as SLNO,'0' as Parent_compID ,'Parent Company' as Title from Parent_Company_Table   ";
                if (empnm != "")
                {
                    sqlstr = sqlstr + "where SLNO in (Select Parent_CompID From Sub_Company_table where SLNO in  ";
                    sqlstr = sqlstr + "(Select Sub_Company From Employee_table where EmployeeName like @EmployeeName and isnull(Employee_table.isDeleted, '') != 1)   ) ";
                }
                sqlstr = sqlstr + "  union all  ";

                sqlstr = sqlstr + "   Select 1 as Level, Sub_Company_table.SubCompanyName as  "
                + "   Company_Name,'S'+convert(varchar(10),SLNO)as SLNO,'P'+convert(varchar(10),Parent_compID) as Parent_compID  ,'Sub Company' as Title "
                  + "   from Sub_Company_table where isnull(Sub_Company_table.isDeleted, '') != 1 ";
                if (empnm != "")
                {
                    sqlstr = sqlstr + " and SLNO in (Select Sub_Company From Employee_table where EmployeeName like @EmployeeName and isnull(Employee_table.isDeleted, '') != 1)";
                }
                sqlstr = sqlstr + " union all"


                   + " select 2 as Level,Domain_Name as Company_Name"

                  + " ,'D'+convert(varchar(10),Domain_table.SLNO)as SLNO,'S'+convert(varchar(10),Maped_CompID) as Parent_compID ,'Domain' as Title  "
                + "    from Domain_table inner join Domain_Mapping_table on Domain_table.SLNO=Domain_Mapping_table.Maped_DomainID  where isnull(Domain_table.isDeleted, '') != 1  ";
                if (empnm != "")
                {
                    sqlstr = sqlstr + " and Domain_table.SLNO in (Select Domain_ID From Employee_table where EmployeeName like @EmployeeName and isnull(Employee_table.isDeleted, '') != 1)";
                }
                sqlstr = sqlstr + " union all "
                    + " Select 3 as Level, Employee_table.EmployeeName as Company_Name "
                    + " ,'E'+convert(varchar(10),SLNO)as SLNO,'D'+convert(varchar(10),Sub_Company) as Parent_compID ,'Employee' as Title  "
                    + " from Employee_table where isnull(Employee_table.isDeleted, '') != 1  ";


                if (empnm != "")
                {
                    sqlstr = sqlstr + " and EmployeeName like @EmployeeName";
                }
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                if (empnm != "")
                {
                    cmd.Parameters.AddWithValue("@EmployeeName", "%" + empnm + "%");
                }
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    OrganizationChat_Model tempobj = new OrganizationChat_Model();
                    tempobj.Id = sdr["SLNO"].ToString();
                    tempobj.Name = sdr["Company_Name"].ToString();
                    tempobj.ReportTo = sdr["Parent_compID"].ToString();
                    tempobj.Title = sdr["Title"].ToString();

                    if (tempobj.ReportTo == "0")
                    {
                        tempobj.ReportTo = tempobj.Id;
                    }



                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "OrganizationChat_Repository/GetOrganizationChat");
            }

            return objModel;
        }

    }
}
