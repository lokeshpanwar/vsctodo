﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class MailMsg_Repository
    {
        public MailMsg_Model GetEmailDataForAdmin()
        {
            MailMsg_Model objdata = new MailMsg_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetEmailDataForAdmin";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                // Delhi Office Data
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.CompReportList_Jaipur = (from DataRow dr in ds.Tables[0].Rows
                                                     select new CompanyWiseReport
                                                     {
                                                         InsCompany = dr["CompanyShortName"].ToString(),
                                                         PremiumPayable = dr["PremiumPayable1"].ToString()

                                                     }).ToList();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    objdata.CatReportList_Jaipur = (from DataRow dr in ds.Tables[1].Rows
                                                    select new CategoryWiseReport
                                                    {
                                                        Category = dr["CategoryName"].ToString(),
                                                        PremiumPayable = dr["PremiumPayable1"].ToString()
                                                    }).ToList();
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    objdata.ReportCount_Jaipur = (from DataRow dr in ds.Tables[2].Rows
                                                  select new CountReports
                                                  {
                                                      PolicyUW = dr["PolicyUW"].ToString(),
                                                      PolicyGenenrate = dr["PolicyGenenrate"].ToString(),
                                                      PolicyScan = dr["PolicyScan"].ToString(),
                                                      PolicyDeliver = dr["PolicyDeliver"].ToString(),
                                                  }).ToList();
                }
                // Delhi Office Data
                if (ds.Tables[3].Rows.Count > 0)
                {
                    objdata.CompReportList_Delhi = (from DataRow dr in ds.Tables[3].Rows
                                                    select new CompanyWiseReport
                                                    {
                                                        InsCompany = dr["CompanyShortName"].ToString(),
                                                        PremiumPayable = dr["PremiumPayable1"].ToString()

                                                    }).ToList();
                }
                if (ds.Tables[4].Rows.Count > 0)
                {
                    objdata.CatReportList_Delhi = (from DataRow dr in ds.Tables[4].Rows
                                                   select new CategoryWiseReport
                                                   {
                                                       Category = dr["CategoryName"].ToString(),
                                                       PremiumPayable = dr["PremiumPayable1"].ToString()
                                                   }).ToList();
                }
                if (ds.Tables[5].Rows.Count > 0)
                {
                    objdata.ReportCount_Delhi = (from DataRow dr in ds.Tables[5].Rows
                                                 select new CountReports
                                                 {
                                                     PolicyUW = dr["PolicyUW"].ToString(),
                                                     PolicyGenenrate = dr["PolicyGenenrate"].ToString(),
                                                     PolicyScan = dr["PolicyScan"].ToString(),
                                                     PolicyDeliver = dr["PolicyDeliver"].ToString(),
                                                 }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }
        public Msg_Model GetDailyMsgShootData()
        {
            Msg_Model objdata = new Msg_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_SMSPolicyDetail";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.RemindMsgList = (from DataRow dr in ds.Tables[0].Rows
                                             select new Remind_Msg_Model
                                             {
                                                 PolicyId = Convert.ToInt64(dr["PId"].ToString()),
                                                 MobileNo = Convert.ToString(dr["Mobileno"].ToString()),
                                                 MsgText = Convert.ToString(dr["MSGText"].ToString()),
                                                 SmsDays = Convert.ToInt32(dr["SmsDays"].ToString()),
                                             }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }
        public string SaveSentMsg(Remind_Msg_Model model)
        {
            string result = "Error";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveSMSLog]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", model.PolicyId);
                    cmd.Parameters.AddWithValue("@Mobile", model.MobileNo);
                    cmd.Parameters.AddWithValue("@SMStext", model.MsgText);
                    cmd.Parameters.AddWithValue("@Day", model.SmsDays);
                    cmd.Parameters.AddWithValue("@result", model.Error);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
