﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class DMS_Reports_Repository
    {


        public DMS_Report_DocRevision_Model GetDocumentRevisionList(DMS_Report_DocRevision_Model objModel, long CreatedBy)
        {
            DMS_Report_DocRevision_Model objReturnModel = new DMS_Report_DocRevision_Model();
            List<DocRevision> objRetList = new List<DocRevision>();
            try
            {
                objReturnModel.DocClassId = objModel.DocClassId;
                objReturnModel.FromDate = objModel.FromDate;
                objReturnModel.ToDate = objModel.ToDate;
                objReturnModel.UserId = objModel.UserId;
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO,Parent_CompID,Sub_CompID,DocumentID,RevisionID,Action,UserID,Datetime,DocumentClassID, "
                    + " DocumentClassName,FileName,Comments from Document_Revision_Log_Table "
                + " where UserID = @UploadedBy and DocumentClassID = @Doc_Class_ID and "
                + " (Cast(Datetime as Date) between Cast(@FromDate as Date) and Cast(@ToDate as Date)) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@UploadedBy", objModel.UserId);
                cmd.Parameters.AddWithValue("@Doc_Class_ID", objModel.DocClassId);
                cmd.Parameters.AddWithValue("@FromDate", objModel.FromDate);
                cmd.Parameters.AddWithValue("@ToDate", objModel.ToDate);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocRevision tempobj = new DocRevision();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentId = Convert.ToInt64(sdr["DocumentID"].ToString());
                    tempobj.RevisionId = sdr["RevisionID"].ToString();
                    tempobj.Action = sdr["Action"].ToString();
                    tempobj.UserId = sdr["UserID"].ToString();
                    tempobj.DateTime1 = Convert.ToDateTime(sdr["Datetime"].ToString());
                    tempobj.DocumentClassId = Convert.ToInt64(sdr["DocumentClassID"].ToString());
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.Comments = sdr["Comments"].ToString();
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    objRetList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
                objReturnModel.DocRevision_List = objRetList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DMS_Reports_Repository/GetDocumentRevisionList");
            }

            return objReturnModel;
        }




        public DMS_Report_DocRevision_Model GetDocumentUploadList(DMS_Report_DocRevision_Model objModel, long CreatedBy)
        {
            DMS_Report_DocRevision_Model objReturnModel = new DMS_Report_DocRevision_Model();
            List<DocRevision> objRetList = new List<DocRevision>();
            try
            {
                objReturnModel.DocClassId = objModel.DocClassId;
                objReturnModel.FromDate = objModel.FromDate;
                objReturnModel.ToDate = objModel.ToDate;
                objReturnModel.UserId = objModel.UserId;
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Doc_ID,SLNO,Doc_Class_ID,FileName,Isnull(UploadedBy,0) as UploadedBy, UploadedOn,VersionID, "
                + " (Select DocumentClassName From DocumentClass_table where DocumentClass_table.SLNO = DocUpload_table.Doc_Class_ID) as DocumentClassName, "
                + " Isnull((Select Userid from Employee_table where Employee_table.SLNO = DocUpload_table.UploadedBy),'') as UserName "
                + " From DocUpload_table where Doc_Class_ID = @DocClassId and Isnull(UploadedBy,0) = @UploadedBy and  "
                + " (Cast(UploadedOn as Date) between Cast(@FromDate as Date) and Cast(@ToDate as Date)) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@UploadedBy", objModel.UserId);
                cmd.Parameters.AddWithValue("@DocClassId", objModel.DocClassId);
                cmd.Parameters.AddWithValue("@FromDate", objModel.FromDate);
                cmd.Parameters.AddWithValue("@ToDate", objModel.ToDate);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocRevision tempobj = new DocRevision();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentId = Convert.ToInt64(sdr["Doc_ID"].ToString());
                    tempobj.Action = "UPLOAD";
                    tempobj.UserId = sdr["UserName"].ToString();
                    tempobj.DateTime1 = Convert.ToDateTime(sdr["UploadedOn"].ToString());
                    tempobj.DocumentClassId = Convert.ToInt64(sdr["Doc_Class_ID"].ToString());
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.DocumentClassName = sdr["DocumentClassName"].ToString();
                    objRetList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
                objReturnModel.DocRevision_List = objRetList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DMS_Reports_Repository/GetDocumentUploadList");
            }

            return objReturnModel;
        }




        public DocumentClassAccessHistoryReport_Model GetDocumentClassAccessHistoryList(long CreatedBy, long ClassId, DateTime FromDate, DateTime ToDate)
        {
            DocumentClassAccessHistoryReport_Model objModel = new DocumentClassAccessHistoryReport_Model();
            List<Log_Table_Model> objList = new List<Log_Table_Model>();
            try
            {
                DocumentClass_Repository objDocClassRepo = new DocumentClass_Repository();
                var docclassdetails = objDocClassRepo.GetOneDocumentClass(ClassId, CreatedBy, "");
                string docclassname = "Document Class:"+ docclassdetails.DocumentClassName;

                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO,Parent_CompID, Sub_CompID, OperationType, Operation_Performed,  "
                + " OperationON, Ramarks, IP_Address, user_id,  (Select Userid From Employee_table where  "
                + " Employee_table.SLNO=Log_Table.User_id) as UserID from Log_Table where Operation_Performed = @Operation_Performed and "
                + " Cast(OperationON as Date) Between Cast(@FromDate as Date) and Cast(@ToDate as Date) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@Operation_Performed", docclassname);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Log_Table_Model tempobj = new Log_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Parent_CompID = sdr["Parent_CompID"].ToString();
                    tempobj.Sub_CompID = sdr["Sub_CompID"].ToString();
                    tempobj.UserID = sdr["UserID"].ToString();
                    tempobj.OperationType = sdr["OperationType"].ToString();
                    tempobj.Operation_Performed = sdr["Operation_Performed"].ToString();
                    tempobj.OperationON = Convert.ToDateTime(sdr["OperationON"].ToString());
                    tempobj.Remarks = sdr["Ramarks"].ToString();
                    tempobj.IP_Address = sdr["IP_Address"].ToString();
                    tempobj.user_id = Convert.ToInt64(sdr["user_id"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
                objModel.Log_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DMS_Reports_Repository/GetDocumentClassAccessHistoryList");
            }

            return objModel;
        }

    }
}
