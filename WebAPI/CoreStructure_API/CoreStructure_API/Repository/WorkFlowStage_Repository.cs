﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class WorkFlowStage_Repository
    {

        public DBReturnModel InsertWorkFlowStage(workflow_stage_table_Model objModel)
        {
            string result = "Error on Inserting Work Flow Stage!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertWorkFlowStage";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@workFlowID", objModel.workFlowID);
                    cmd.Parameters.AddWithValue("@stageOrderID", objModel.stageOrderID);
                    cmd.Parameters.AddWithValue("@stageName", objModel.stageName);
                    cmd.Parameters.AddWithValue("@stageDescription", objModel.stageDescription);
                    cmd.Parameters.AddWithValue("@stageDays", objModel.stageDays);
                    cmd.Parameters.AddWithValue("@people", objModel.people);
                    cmd.Parameters.AddWithValue("@peopleInfo", objModel.peopleInfo);
                    cmd.Parameters.AddWithValue("@RASCI", objModel.RASCI);
                    cmd.Parameters.AddWithValue("@attachment", objModel.attachment);
                    cmd.Parameters.AddWithValue("@url", objModel.url);
                    cmd.Parameters.AddWithValue("@form", objModel.form);
                    cmd.Parameters.AddWithValue("@checklist", objModel.checklist);
                    cmd.Parameters.AddWithValue("@approverWorkFlow", objModel.approverWorkFlow);
                    cmd.Parameters.AddWithValue("@processYes", objModel.processYes);
                    cmd.Parameters.AddWithValue("@processNo", objModel.processNo);
                    cmd.Parameters.AddWithValue("@activeStatus", objModel.activeStatus);
                    cmd.Parameters.AddWithValue("@IsAllpeople", objModel.IsAllpeople);
                    cmd.Parameters.AddWithValue("@PeopleID", objModel.PeopleID);
                    cmd.Parameters.AddWithValue("@Viewright", objModel.Viewright);
                    cmd.Parameters.AddWithValue("@communicateTo", objModel.communicateTo);
                    cmd.Parameters.AddWithValue("@IsDelegated", objModel.IsDelegated);
                    cmd.Parameters.AddWithValue("@DelegatedTo", objModel.DelegatedTo);
                    cmd.Parameters.AddWithValue("@DynRef", objModel.DynRef);
                    cmd.Parameters.AddWithValue("@sapForm", objModel.sapForm);
                    cmd.Parameters.AddWithValue("@DynGroupId", objModel.DynGroupId);
                    cmd.Parameters.AddWithValue("@formTemplate", objModel.formTemplate);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@createdBy", objModel.createdBy);
                    cmd.Parameters.AddWithValue("@createdOn", StandardDateTime.GetDateTime());


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.createdBy), "API", "WorkFlowStage_Repository/InsertWorkFlowStage");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.createdBy), "API", "WorkFlowStage_Repository/InsertWorkFlowStage");
            }

            return objReturn;
        }


        public List<workflow_stage_table_Model> GetWorkFlowStageList(long AdminId, long SLNO, long WorkFlowId)
        {
            List<workflow_stage_table_Model> objList = new List<workflow_stage_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectWorkFlowStage ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                cmd.Parameters.AddWithValue("@WorkFlowId", WorkFlowId);
                cmd.Parameters.AddWithValue("@isDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    workflow_stage_table_Model tempobj = new workflow_stage_table_Model();
                    tempobj.createdBy = sdr["createdBy"].ToString();
                    tempobj.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    tempobj.deletedBy = sdr["deletedBy"].ToString();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.stageOrderID = sdr["stageOrderID"].ToString();
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageDays = sdr["stageDays"].ToString();
                    tempobj.people = sdr["people"].ToString();
                    tempobj.peopleInfo = sdr["peopleInfo"].ToString();


                    tempobj.RASCI = sdr["RASCI"].ToString();
                    tempobj.attachment = sdr["attachment"].ToString();
                    tempobj.url = sdr["url"].ToString();
                    tempobj.form = sdr["form"].ToString();
                    tempobj.checklist = sdr["checklist"].ToString();
                    tempobj.approverWorkFlow = sdr["approverWorkFlow"].ToString();
                    tempobj.processYes = sdr["processYes"].ToString();
                    tempobj.processNo = sdr["processNo"].ToString();
                    tempobj.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    tempobj.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    tempobj.IsAllpeople = Convert.ToBoolean(sdr["IsAllpeople"].ToString());
                    tempobj.PeopleID = Convert.ToInt64(sdr["PeopleID"].ToString());
                    tempobj.Viewright = sdr["Viewright"].ToString();
                    tempobj.communicateTo = sdr["communicateTo"].ToString();
                    tempobj.IsDelegated = Convert.ToBoolean(sdr["IsDelegated"].ToString());
                    tempobj.DelegatedTo = sdr["DelegatedTo"].ToString();
                    tempobj.DynRef = sdr["DynRef"].ToString();
                    tempobj.sapForm = sdr["sapForm"].ToString();
                    tempobj.DynGroupId = Convert.ToInt64(sdr["DynGroupId"].ToString());
                    tempobj.formTemplate = sdr["formTemplate"].ToString();

                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AdminId, "WebAPI", "WorkFlowStage_Repository/GetWorkFlowStageList");
            }

            return objList;
        }



        public DBReturnModel DeleteWorkFlowStage(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Work Flow Stage!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update workflow_stage_table set isDeleted=@isDeleted,"
                        + " DeletedBy=@DeletedBy,deletedOn=@deletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@isDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@deletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Work Flow Stage Deleted Successfully!";

                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/DeleteWorkFlowStage");
                    objreturn.ReturnMessage = "System Error on Deleting Work Flow!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "WorkFlow_Repository/DeleteWorkFlowStage");
                objreturn.ReturnMessage = "System Error on Deleting Work Flow!";

            }

            return objreturn;
        }





        public workflow_stage_table_Model GetOneWorkFlowStage(long AdminId, long SLNO)
        {
            workflow_stage_table_Model objModel = new workflow_stage_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectWorkFlowStage ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                cmd.Parameters.AddWithValue("@WorkFlowId", 0);
                cmd.Parameters.AddWithValue("@isDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    workflow_stage_table_Model tempobj = new workflow_stage_table_Model();
                    objModel.createdBy = sdr["createdBy"].ToString();
                    objModel.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    objModel.deletedBy = sdr["deletedBy"].ToString();
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    objModel.stageOrderID = sdr["stageOrderID"].ToString();
                    objModel.stageName = sdr["stageName"].ToString();
                    objModel.stageDescription = sdr["stageDescription"].ToString();
                    objModel.stageDays = sdr["stageDays"].ToString();
                    objModel.people = sdr["people"].ToString();
                    objModel.peopleInfo = sdr["peopleInfo"].ToString();
                    objModel.RASCI = sdr["RASCI"].ToString();
                    objModel.attachment = sdr["attachment"].ToString();
                    objModel.url = sdr["url"].ToString();
                    objModel.form = sdr["form"].ToString();
                    objModel.checklist = sdr["checklist"].ToString();
                    objModel.approverWorkFlow = sdr["approverWorkFlow"].ToString();
                    objModel.processYes = sdr["processYes"].ToString();
                    objModel.processNo = sdr["processNo"].ToString();
                    objModel.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    objModel.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    objModel.IsAllpeople = Convert.ToBoolean(sdr["IsAllpeople"].ToString());
                    objModel.PeopleID = Convert.ToInt64(sdr["PeopleID"].ToString());
                    objModel.Viewright = sdr["Viewright"].ToString();
                    objModel.communicateTo = sdr["communicateTo"].ToString();
                    objModel.IsDelegated = Convert.ToBoolean(sdr["IsDelegated"].ToString());
                    objModel.DelegatedTo = sdr["DelegatedTo"].ToString();
                    objModel.DynRef = sdr["DynRef"].ToString();
                    objModel.sapForm = sdr["sapForm"].ToString();
                    objModel.DynGroupId = Convert.ToInt64(sdr["DynGroupId"].ToString());
                    objModel.formTemplate = sdr["formTemplate"].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AdminId, "WebAPI", "WorkFlowStage_Repository/GetOneWorkFlowStage");
            }

            return objModel;
        }



        public DBReturnModel UpdateViewRightForWorkFlowStage(List<workflowstage_user_right> objModel, long Modified_By)
        {
            string result = "Error on Updating View Right For Work Flow Stage!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateViewRightForWorkFlowStage";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.WorkFlowStageViewRight";
                    dataTable.Columns.Add("SLNO", typeof(long));
                    dataTable.Columns.Add("ViewRight", typeof(string));
                    foreach (var dta in objModel)
                    {
                        dataTable.Rows.Add(dta.SLNO, dta.Viewright);

                    }
                    SqlParameter parameter = new SqlParameter("WFStageVR", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    cmd.Parameters.AddWithValue("@Modified_By", Modified_By);
                    cmd.Parameters.AddWithValue("@Modified_On", StandardDateTime.GetDateTime());


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/UpdateViewRightForWorkFlowStage");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/UpdateViewRightForWorkFlowStage");
            }

            return objReturn;
        }







        public DBReturnModel InsertWorkFlowPriority(List<workflow_priority_map_Model> objModel, long createdBy)
        {
            string result = "Error on Inserting Work Flow Priority!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertWorkFlowPriority";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.workflow_priority_map_Type";
                    dataTable.Columns.Add("BPMID", typeof(string));
                    dataTable.Columns.Add("StgID", typeof(string));
                    dataTable.Columns.Add("PriID", typeof(string));
                    dataTable.Columns.Add("Day", typeof(string));
                    foreach (var dta in objModel)
                    {
                        if (dta != null)
                        {
                            dataTable.Rows.Add(dta.BPMID, dta.StgID, dta.PriID, dta.Day);
                        }
                    }
                    SqlParameter parameter = new SqlParameter("workflowpriority", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);




                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", createdBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(createdBy), "API", "WorkFlowStage_Repository/InsertWorkFlowPriority");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(createdBy), "API", "WorkFlowStage_Repository/InsertWorkFlowPriority");
            }

            return objReturn;
        }




        public DBReturnModel ActivateWorkFlowStage(long WorkFlowId, long Modified_By)
        {
            string result = "Error on Activating Work Flow!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_ActivateWorkFlow";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    cmd.Parameters.AddWithValue("@WorkFlowId", WorkFlowId);
                    cmd.Parameters.AddWithValue("@activeStatus", true);
                    cmd.Parameters.AddWithValue("@Modified_By", Modified_By);
                    cmd.Parameters.AddWithValue("@Modified_On", StandardDateTime.GetDateTime());


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/ActivateWorkFlowStage");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/ActivateWorkFlowStage");
            }

            return objReturn;
        }


        public DBReturnModel ParkWorkFlowStage(long WorkFlowId, long Modified_By)
        {
            string result = "Error on Parking Work Flow!";
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = result;
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_ParkWorkFlow";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    cmd.Parameters.AddWithValue("@WorkFlowId", WorkFlowId);
                    cmd.Parameters.AddWithValue("@activeStatus", false);
                    cmd.Parameters.AddWithValue("@Modified_By", Modified_By);
                    cmd.Parameters.AddWithValue("@Modified_On", StandardDateTime.GetDateTime());


                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/ParkWorkFlowStage");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(Modified_By), "API", "WorkFlowStage_Repository/ParkWorkFlowStage");
            }

            return objReturn;
        }


        
    }
}
