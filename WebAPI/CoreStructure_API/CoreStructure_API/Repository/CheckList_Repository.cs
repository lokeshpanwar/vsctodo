﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class CheckList_Repository
    {


        public DBReturnModel InsertCheckList(Checklist_Table_Model objModel)
        {
            string result = "Error on Inserting Check List!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertCheckList";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("SubComp_ID", objModel.SubComp_ID);
                    cmd.Parameters.AddWithValue("Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("Checklist_Name", objModel.Checklist_Name);
                    cmd.Parameters.AddWithValue("Checklist_Items", objModel.Checklist_Items);
                    cmd.Parameters.AddWithValue("Checklist_Itemcount", objModel.Checklist_Itemcount);
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("IsMaped", false);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "retMessage";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);
                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "CheckList_Repository/InsertCheckList");
                    objreturn.ReturnMessage = "System Error on Inserting Check List";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "CheckList_Repository/InsertCheckList");
            }

            return objreturn;
        }

               
        public Checklist_Table_page_Model GetCheckListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Checklist_Table_page_Model objModel = new Checklist_Table_page_Model();
            List<Checklist_Table_Model> objList = new List<Checklist_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectCheckListByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Checklist_Table_Model tempobj = new Checklist_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Checklist_Itemcount = Convert.ToInt64(sdr["Checklist_Itemcount"].ToString());
                    tempobj.Checklist_Name = sdr["Checklist_Name"].ToString();
                    tempobj.Checklist_Items = sdr["Checklist_Items"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.IsMaped = Convert.ToBoolean(sdr["IsMaped"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.SubComp_ID = Convert.ToInt64(sdr["SubComp_ID"].ToString());

                    tempobj.CLCompanyName = sdr["CLCompanyName"].ToString();
                    tempobj.CLDomainName = sdr["CLDomainName"].ToString();

                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Checklist_Table where (Isnull(Checklist_Items,'') like '%'+@search+'%' "
                + " or Isnull(Checklist_Name,'') like '%'+@search+'%') ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Checklist_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "CheckList_Repository/GetCheckListByPage");
            }

            return objModel;

        }

                     
        public Checklist_Table_Model GetOneCheckList(long SLNO, long CreatedBy)
        {
            Checklist_Table_Model objModel = new Checklist_Table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectCheckListById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Checklist_Itemcount = Convert.ToInt64(sdr["Checklist_Itemcount"].ToString());
                    objModel.Checklist_Name = sdr["Checklist_Name"].ToString();
                    objModel.Checklist_Items = sdr["Checklist_Items"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    objModel.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.IsMaped = Convert.ToBoolean(sdr["IsMaped"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objModel.SubComp_ID = Convert.ToInt64(sdr["SubComp_ID"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "CheckList_Repository/GetOneCheckList");
            }

            return objModel;

        }

                     
        public DBReturnModel UpdateCheckList(Checklist_Table_Model objModel)
        {
            string result = "Error on Updating Check List!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateCheckList";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("SubComp_ID", objModel.SubComp_ID);
                    cmd.Parameters.AddWithValue("SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("Domain_ID", objModel.Domain_ID);
                    cmd.Parameters.AddWithValue("Checklist_Name", objModel.Checklist_Name);
                    cmd.Parameters.AddWithValue("Checklist_Items", objModel.Checklist_Items);
                    cmd.Parameters.AddWithValue("Checklist_Itemcount", objModel.Checklist_Itemcount);
                    cmd.Parameters.AddWithValue("ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("IsMaped", false);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "retMessage";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);
                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "CheckList_Repository/UpdateCheckList");
                    objreturn.ReturnMessage = "System Error on Updating Check List";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "CheckList_Repository/UpdateCheckList");
            }

            return objreturn;
        }







        public DBReturnModel DisableCheckList(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Check List!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Checklist_Table set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@ModifiedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@ModifiedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Check List Disabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "CheckList_Repository/DisableCheckList");
                    objreturn.ReturnMessage = "System Error on Disabling Check List!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "CheckList_Repository/DisableCheckList");
            }

            return objreturn;
        }




        public DBReturnModel EnableCheckList(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Check List!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update CheckList_table set IsDeleted=@IsDeleted,"
                        + " ModifiedBy=@ModifiedBy,DeletedOn=@DeletedOn "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@ModifiedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Check List Enabled Successfully!";
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = result;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "CheckList_Repository/EnableCheckList");
                    objreturn.ReturnMessage = "System Error on Enabling Check List!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "CheckList_Repository/EnableCheckList");
            }

            return objreturn;
        }


    }
}
