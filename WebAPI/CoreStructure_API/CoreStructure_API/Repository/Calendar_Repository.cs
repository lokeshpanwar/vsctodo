﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Calendar_Repository
    {

        public DBReturnModel InsertMastCalender(Mastcalendar_Model objModel)
        {
            string result = "Error on Inserting Calender Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    DateTime currdate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
                    DateTime Leavedt = Convert.ToDateTime(objModel.leave_date.ToString("dd/MM/yyyy"));
                    if (Leavedt < currdate)
                    {
                        objreturn.ReturnStatus = "ERROR";
                        objreturn.ReturnMessage = "The date must not be less than today";
                    }

                    else
                    {
                        string sqlstr = "CB_SP_InsertMastcalendar";

                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@Empid", objModel.Empid);
                        cmd.Parameters.AddWithValue("@leave_date", Convert.ToDateTime(objModel.leave_date));
                        cmd.Parameters.AddWithValue("@Cl_description", objModel.Cl_description);
                        cmd.Parameters.AddWithValue("@creationdate", StandardDateTime.GetDateTime());
                        cmd.Parameters.AddWithValue("@creationuser", objModel.creationuser);
                        cmd.Parameters.AddWithValue("@isdeleted", false);


                        var returncode = new SqlParameter();
                        returncode.ParameterName = "retMessage";
                        returncode.SqlDbType = SqlDbType.VarChar;
                        returncode.Size = 500;
                        returncode.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(returncode);

                        var retStatus = new SqlParameter();
                        retStatus.ParameterName = "retStatus";
                        retStatus.SqlDbType = SqlDbType.VarChar;
                        retStatus.Size = 500;
                        retStatus.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(retStatus);
                        cmd.ExecuteNonQuery();
                        transaction.Commit();

                        objreturn.ReturnMessage = returncode.Value.ToString();
                        objreturn.ReturnStatus = retStatus.Value.ToString();

                        connection.Close();
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "Calendar_Repository/InsertMastCalender");
                    objreturn.ReturnMessage = "System Error on Inserting Calender Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "Calendar_Repository/InsertMastCalender");
            }
            return objreturn;

        }



        public Mastcalendar_Model GetOneMastCalender(long Cl_ID, long creationuser)
        {
            Mastcalendar_Model objModel = new Mastcalendar_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectMastcalendarListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Cl_ID", Cl_ID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.Cl_ID = Convert.ToInt64(sdr["Cl_ID"].ToString());
                    objModel.Empid = Convert.ToInt64(sdr["Empid"].ToString());
                    objModel.leave_date = Convert.ToDateTime(sdr["leave_date"].ToString());
                    objModel.Cl_description = sdr["Cl_description"].ToString();
                    objModel.creationuser = sdr["creationuser"].ToString();
                    objModel.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objModel.modifactionuser = sdr["modifactionuser"].ToString();
                    objModel.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "Calendar_Repository/GetOneMastCalender");
            }
            return objModel;
        }


        public DBReturnModel UpdateMastCalender(Mastcalendar_Model objModel)
        {
            string result = "Error on Updating Skill Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    DateTime currdate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
                    DateTime Leavedt = Convert.ToDateTime(objModel.leave_date.ToString("dd/MM/yyyy"));
                    if (Leavedt < currdate)
                    {
                        objreturn.ReturnStatus = "ERROR";
                        objreturn.ReturnMessage = "The date must not be less than today";
                    }
                    else
                    {
                        string sqlstr = "CB_SP_UpdateMastcalendar";
                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = connection;
                        cmd.Parameters.AddWithValue("@Cl_ID", objModel.Cl_ID);
                        cmd.Parameters.AddWithValue("@Empid", objModel.Empid);
                        cmd.Parameters.AddWithValue("@leave_date", Convert.ToDateTime(objModel.leave_date));
                        cmd.Parameters.AddWithValue("@Cl_description", objModel.Cl_description);
                        cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                        cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);

                        var retmsg = new SqlParameter();
                        retmsg.ParameterName = "retMessage";
                        retmsg.SqlDbType = SqlDbType.VarChar;
                        retmsg.Size = 500;
                        retmsg.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(retmsg);


                        var retStatus = new SqlParameter();
                        retStatus.ParameterName = "retStatus";
                        retStatus.SqlDbType = SqlDbType.VarChar;
                        retStatus.Size = 50;
                        retStatus.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(retStatus);
                        cmd.ExecuteNonQuery();

                        transaction.Commit();

                        objreturn.ReturnMessage = retmsg.Value.ToString();
                        objreturn.ReturnStatus = retStatus.Value.ToString();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "Calendar_Repository/UpdateMastCalender");
                    objreturn.ReturnMessage = "System Error on Updating CalenderMaster";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "Calendar_Repository/UpdateMastCalender");
            }
            return objreturn;
        }


        public DBReturnModel DisableMastcalendar(long Cl_ID, long modifactionuser)
        {

            string result = "Error on Disabling Calender Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Mastcalendar set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where Cl_ID=@Cl_ID";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Cl_ID", Cl_ID);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Calender Master Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "Calendar_Repository/DisableMastcalendar");
                    objreturn.ReturnMessage = "System Error on Disabling Calender Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "Calendar_Repository/DisableMastcalendar");
            }

            return objreturn;
        }


        public DBReturnModel EnableMastcalendar(long Cl_ID, long modifactionuser)
        {

            string result = "Error on Enabling Calender Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Mastcalendar set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where Cl_ID=@Cl_ID";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Cl_ID", Cl_ID);
                    cmd.Parameters.AddWithValue("@isdeleted", false);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Calender Master Enabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "Calendar_Repository/EnableMastcalendar");
                    objreturn.ReturnMessage = "System Error on Enabling Calender Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "Calendar_Repository/EnableMastcalendar");
            }

            return objreturn;
        }


        public Mastcalendar_page_Model GetMastcalendarByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Mastcalendar_page_Model objModel = new Mastcalendar_page_Model();
            List<Mastcalendar_Model> objlist = new List<Mastcalendar_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "Cl_ID";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectMastcalendarByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Mastcalendar_Model objtemp = new Mastcalendar_Model();
                    objtemp.Cl_ID = Convert.ToInt64(sdr["Cl_ID"].ToString());
                    objtemp.Empid = Convert.ToInt64(sdr["Empid"].ToString());
                    objtemp.leave_date = Convert.ToDateTime(sdr["leave_date"].ToString());
                    objtemp.Cl_description = sdr["Cl_description"].ToString();
                    objtemp.creationuser = sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From Mastcalendar where (Isnull(Empid,'') like '%'+@search+'%' "
                + " or Isnull(leave_date,'') like '%'+@search+'%') ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.Mastcalendar_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Calendar_Repository/GetMastcalendarByPage");

            }

            return objModel;
        }



        public Mastcalendar_page_Model GetMastcalendarByEmpID(long CreatedBy)
        {
            Mastcalendar_page_Model objModel = new Mastcalendar_page_Model();
            List<Mastcalendar_Model> objlist = new List<Mastcalendar_Model>();
            
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                string sqlstr = "Select  Cl_ID, Empid, Isnull(leave_date,'') as leave_date, isnull(Cl_description,'') as Cl_description, "
                + " isnull(isdeleted, 0) as isdeleted, isnull(creationdate, '') as creationdate,isnull(modifactiondate, '') as modifactiondate, "
                + " isnull(creationuser, '') as creationuser,isnull(modifactionuser, '') as modifactionuser "
                + " From Mastcalendar  where Isnull(Empid, '') = @EmpId and isnull(isdeleted,0)!= @isdeleted "
                + " union all "
                + " Select 0 as Cl_ID, 0 as Empid, Isnull(HolidaysDate, '') as leave_date, 'Holiday' as Cl_description,  "
                + " isnull(isdeleted, 0) as isdeleted, '' as creationdate,'' as modifactiondate, "
                + " '' as creationuser,'' as modifactionuser "
                + " From CompanyHoliday where isnull(isdeleted,0)!= @isdeleted";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@EmpId", CreatedBy);
                cmd.Parameters.AddWithValue("@isdeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Mastcalendar_Model objtemp = new Mastcalendar_Model();
                    objtemp.Cl_ID = Convert.ToInt64(sdr["Cl_ID"].ToString());
                    objtemp.Empid = Convert.ToInt64(sdr["Empid"].ToString());
                    objtemp.leave_date = Convert.ToDateTime(sdr["leave_date"].ToString());
                    objtemp.Cl_description = sdr["Cl_description"].ToString();
                    objtemp.creationuser = sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    if (sdr["Cl_description"].ToString() == "Holiday")
                    {
                        objtemp.backgroundcolor = "orange";
                    }
                    else
                    {
                        objtemp.backgroundcolor = "skyblue";
                    }

                    objlist.Add(objtemp);
                }
                sdr.Close();


                connection.Close();
                objModel.Mastcalendar_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Calendar_Repository/GetMastcalendarByEmpID");

            }

            return objModel;
        }


    }
}
