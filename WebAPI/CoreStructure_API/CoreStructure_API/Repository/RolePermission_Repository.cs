﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class RolePermission_Repository
    {

        public List<Role_Modules_Permission_Model> GetModuleListBasedOnRole(long Role_ID, long CreatedBy)
        {
            List<Role_Modules_Permission_Model> objList = new List<Role_Modules_Permission_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectRolePermissionModuleListByRoleId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@RoleID", Role_ID);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Role_Modules_Permission_Model tempobj = new Role_Modules_Permission_Model();
                    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                    tempobj.ModuleID = Convert.ToInt32(sdr["ModuleID"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.ModuleName = sdr["ModuleName"].ToString();
                    tempobj.DisplayName = sdr["DisplayName"].ToString();
                    tempobj.GroupName = sdr["GroupName"].ToString();
                    tempobj.RoleID = Convert.ToInt64(sdr["RoleID"].ToString());
                    tempobj.IsRight = Convert.ToBoolean(sdr["IsRight"].ToString());
                    tempobj.IsEdit = Convert.ToBoolean(sdr["IsEdit"].ToString());
                    tempobj.IsWrite = Convert.ToBoolean(sdr["IsWrite"].ToString());
                    tempobj.IsView = Convert.ToBoolean(sdr["IsView"].ToString());
                    tempobj.IsDelete = Convert.ToBoolean(sdr["IsDelete"].ToString());
                    if(tempobj.IsRight == true)
                    {
                        tempobj.IsSelected = true;
                    }
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "RolePermission_Repository/GetModuleListBasedOnRole");
            }

            return objList;
        }



        public DBReturnModel InsertRoleModulePermission(Role_Modules_Permission_Display_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Role Module Permission";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertRoleModulePermission";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Role_ID", objModel.Role_ID);
                    cmd.Parameters.AddWithValue("IsDeleted", false);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Role_Modules_Permission";
                    dataTable.Columns.Add("ID", typeof(int));
                    dataTable.Columns.Add("ModuleID", typeof(int));
                    dataTable.Columns.Add("FormName", typeof(string));
                    dataTable.Columns.Add("IsRight", typeof(bool));
                    if (objModel.Role_Modules_Permission_Model_List != null)
                    {
                        foreach (var dta in objModel.Role_Modules_Permission_Model_List)
                        {
                            if (dta.IsSelected == true)
                            {
                                dataTable.Rows.Add(dta.ID,dta.ModuleID, dta.FormName,dta.IsSelected); // Id of '1' is valid for the Person table
                            }
                        }
                    }
                    SqlParameter parameter = new SqlParameter("Role_Modules_Permission", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);
                    
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RolePermission_Repository/InsertRoleModulePermission");
                    objReturn.ReturnMessage = "System Error on Inserting Role Module Permission!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "RolePermission_Repository/InsertRoleModulePermission");
                objReturn.ReturnMessage = "System Error on Inserting Role Module Permission!";
            }

            return objReturn;
        }




    }
}
