﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocClassDocTypeMapping_Repository
    {

        public DBReturnModel InsertDocClassDocTypeMapping(DocumentType_DocClass_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Class Document Type Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocTypeDocClassMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("Document_ClassID", objModel.Document_ClassID);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.DocumentType_DocClass_Mapping_Type";
                    dataTable.Columns.Add("DocumentType_ID", typeof(long));
                    foreach (var dta in objModel.DocType_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.DocType_Id); // Id of '1' is valid for the Person table
                        }
                    }
                    SqlParameter parameter = new SqlParameter("DocTypeMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassDocTypeMapping_Repository/InsertDocClassDocTypeMapping");
                    objReturn.ReturnMessage = "System Error on Inserting Document Class Document Type Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocClassDocTypeMapping_Repository/InsertDocClassDocTypeMapping");
                objReturn.ReturnMessage = "System Error on Inserting Document Class Document Type Mapping!";
            }

            return objReturn;
        }
        
        public List<DocType> GetDocumentTypeListForMapping(long CreatedBy)
        {
            List<DocType> objReturn = new List<DocType>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, DocumentType_Name From DocumentType_Table where Isnull(IsDeleted,'') != @IsDeleted and SLNO Not In "
                + " (Select  DocumentType_ID From  DocumentType_DocClass_Mapping_Table where Isnull(IsDeleted,'') !=@IsDeleted) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocType tempobj = new DocType();
                    tempobj.DocType_Id = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocType_Name = sdr["DocumentType_Name"].ToString();
                    tempobj.IsSelected = false;
                    objReturn.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocClassDocTypeMapping_Repository/GetDocumentTypeListForMapping");
            }

            return objReturn;
        }
                     
        public DocumentType_DocClass_Mapping_Table_page_Model GetDocClassDocTypeMappingListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            DocumentType_DocClass_Mapping_Table_page_Model objModel = new DocumentType_DocClass_Mapping_Table_page_Model();
            List<DocumentType_DocClass_Mapping_Table_Model> objList = new List<DocumentType_DocClass_Mapping_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocTypeDocClassMappingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentType_DocClass_Mapping_Table_Model tempobj = new DocumentType_DocClass_Mapping_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.DocumentType_ID = Convert.ToInt64(sdr["DocumentType_ID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DCClassName = sdr["DCClassName"].ToString();
                    tempobj.DCDocumentTypeName = sdr["DCDocumentTypeName"].ToString();
                    tempobj.DCDomainName = sdr["DCDomainName"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From DocumentType_DocClass_Mapping_Table where "
                + " (Select Isnull(DocumentClassName, '') From DocumentClass_table where "
                + " DocumentClass_table.SLNO = DocumentType_DocClass_Mapping_Table.Document_ClassID) like '%'+@search+'%' "
                + " or(Select Isnull(DocumentType_Name, '') From DocumentType_Table where "
                + " DocumentType_Table.SLNO = DocumentType_DocClass_Mapping_Table.DocumentType_ID) like '%'+@search+'%' ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.DocumentType_DocClass_Mapping_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocClassDocTypeMapping_Repository/GetDocClassDocTypeMappingListByPage");
            }

            return objModel;

        }






        public DBReturnModel DisableDocClassDocTypeMapping(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Disabling Document Class Document Type Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_EnableDisableDocTypeDocClassMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/DisableDocClassDocTypeMapping");
                    objReturn.ReturnMessage = "System Error on Disabling Document Class Document Type Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/DisableDocClassDocTypeMapping");
                objReturn.ReturnMessage = "System Error on Disabling Document Class Document Type Mapping!";
            }

            return objReturn;
        }


        public DBReturnModel EnableDocClassDocTypeMapping(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Enabling Document Class Document Type Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_EnableDisableDocTypeDocClassMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/EnableDocClassDocTypeMapping");
                    objReturn.ReturnMessage = "System Error on Enabling Document Class Document Type Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/EnableDocClassDocTypeMapping");
                objReturn.ReturnMessage = "System Error on Enabling Document Class Document Type Mapping!";
            }

            return objReturn;
        }





        public DBReturnModel DeleteDocClassDocTypeMapping(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Record Field Mapping With Document Class!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteDocTypeDocClassMapping";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/DeleteDocClassDocTypeMapping");
                    objreturn.ReturnMessage = "System Error on Deleting Document Type Mapping With Document Class!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocClassDocTypeMapping_Repository/DeleteDocClassDocTypeMapping");
                objreturn.ReturnMessage = "System Error on Deleting Document Type Mapping With Document Class!";
            }

            return objreturn;
        }






    }
}
