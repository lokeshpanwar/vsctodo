﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class SkillMap_Repository
    {

        public DBReturnModel InsertSkillMap(SkillMap_Model objModel)
        {
            string result = "Error on Inserting SkillMap Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertSkillMap";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Skillid", objModel.Skillid);
                    cmd.Parameters.AddWithValue("@Empid", objModel.Empid);
                    cmd.Parameters.AddWithValue("@Rating", objModel.Rating);
                    cmd.Parameters.AddWithValue("@creationdate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@creationuser", objModel.creationuser);
                    cmd.Parameters.AddWithValue("@isdeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "SkillMap_Repository/InsertSkillMap");
                    objreturn.ReturnMessage = "System Error on Inserting SkillMap Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "SkillMap_Repository/InsertSkillMap");
            }
            return objreturn;

        }


        public SkillMap_Model GetOneSkillMap(long entryid, long creationuser)
        {
            SkillMap_Model objModel = new SkillMap_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectSkillMapListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@entryid", entryid);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.entryid = Convert.ToInt64(sdr["entryid"].ToString());
                    objModel.Skillid = Convert.ToInt64(sdr["Skillid"].ToString());
                    objModel.Empid = sdr["Empid"].ToString();
                    objModel.Rating = Convert.ToSingle(sdr["Rating"].ToString());
                    objModel.creationuser = sdr["creationuser"].ToString();
                    objModel.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objModel.modifactionuser = sdr["modifactionuser"].ToString();
                    objModel.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "SkillMap_Repository/GetOneSkillMap");
            }
            return objModel;
        }


        public DBReturnModel UpdateSkillMap(SkillMap_Model objModel)
        {
            string result = "Error on Updating SkillMap Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateSkillMap";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", objModel.entryid);
                    cmd.Parameters.AddWithValue("@Skillid", objModel.Skillid);
                    cmd.Parameters.AddWithValue("@Empid", objModel.Empid);
                    cmd.Parameters.AddWithValue("@Rating", objModel.Rating);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);
                   

                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "SkillMap_Repository/UpdateSkillMap");
                    objreturn.ReturnMessage = "System Error on Updating SkillMap Master";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "SkillMap_Repository/UpdateSkillMap");
            }
            return objreturn;
        }

        public DBReturnModel DisableSkillMap(long entryid, long modifactionuser)
        {

            string result = "Error on Disabling SkillMap Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update SkillMap set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where entryid=@entryid";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "SkillMap Master Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "SkillMap_Repository/DisableSkillMap");
                    objreturn.ReturnMessage = "System Error on Disabling SkillMap Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "SkillMap_Repository/DisableSkillMap");
            }

            return objreturn;
        }


        public DBReturnModel EnableSkillMap(long entryid, long modifactionuser)
        {

            string result = "Error on Enabling SkillMap Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update SkillMap set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where entryid=@entryid";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@entryid", entryid);
                    cmd.Parameters.AddWithValue("@isdeleted", false);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "SkillMap Master Enabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "SkillMap_Repository/EnableSkillMap");
                    objreturn.ReturnMessage = "System Error on Enabling SkillMap Master";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "SkillMap_Repository/EnableSkillMap");
            }

            return objreturn;
        }


        public SkillMap_page_Model GetSkillMapByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            SkillMap_page_Model objModel = new SkillMap_page_Model();
            List<SkillMap_Model> objlist = new List<SkillMap_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "entryid";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectSkillMapByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    SkillMap_Model objtemp = new SkillMap_Model();
                    objtemp.entryid = Convert.ToInt64(sdr["entryid"].ToString());
                    objtemp.Skillid = Convert.ToInt64(sdr["Skillid"].ToString());
                    objtemp.Empid = sdr["Empid"].ToString();
                    objtemp.Rating = Convert.ToInt32(sdr["Rating"].ToString());
                    objtemp.creationuser = sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    objtemp.EmployeeName = sdr["EmployeeName"].ToString();
                    objtemp.SkillName = sdr["SkillName"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = " Select Count(*) As Count From SkillMap  where (select SkillName from MastSkill " +    
                "where MastSkill.entryid = SkillMap.Skillid) like '%'+@search+'%' "+
                "or(select EmployeeName from Employee_table where User_type in ('EMPLOYEE') and ISNULL(IsDeleted,'')= 0 "+
                "and Employee_table.SLNO = SkillMap.Empid) like '%'+@search+'%'";
               
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.SkillMap_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "SkillMap_Repository/GetSkillMapByPage");

            }

            return objModel;
        }

    }
}
