﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocTypeRecordFieldMapping_Repository
    {

        public DBReturnModel InsertDocTypeRecordFieldMapping(RecordField_DocType_Mapping_Table_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Document Type Record Field Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocTypeRecordFieldMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("Document_ClassID", objModel.Document_ClassID);
                    cmd.Parameters.AddWithValue("DocumentType_ID", objModel.DocumentType_ID);
                    cmd.Parameters.AddWithValue("RecordField_ID", objModel.RecordField_ID);
                    cmd.Parameters.AddWithValue("IsDeleted", false);
                    cmd.Parameters.AddWithValue("CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("CreatedOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocTypeRecordFieldMapping_Repository/InsertDocTypeRecordFieldMapping");
                    objReturn.ReturnMessage = "System Error on Inserting Document Type Record Field Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.CreatedBy), "API", "DocTypeRecordFieldMapping_Repository/InsertDocTypeRecordFieldMapping");
                objReturn.ReturnMessage = "System Error on Inserting Document Type Record Field Mapping!";
            }

            return objReturn;
        }




        public RecordField_DocType_Mapping_Table_page_Model GetDocTypeRecordFieldMappingListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            RecordField_DocType_Mapping_Table_page_Model objModel = new RecordField_DocType_Mapping_Table_page_Model();
            List<RecordField_DocType_Mapping_Table_Model> objList = new List<RecordField_DocType_Mapping_Table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocTypeRecordFieldMappingByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    RecordField_DocType_Mapping_Table_Model tempobj = new RecordField_DocType_Mapping_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.RecordField_ID = Convert.ToInt64(sdr["RecordField_ID"].ToString());
                    tempobj.DocumentType_ID = Convert.ToInt64(sdr["DocumentType_ID"].ToString());
                    tempobj.DCClassName = sdr["DCClassName"].ToString();
                    tempobj.DCRecordFieldName = sdr["DCRecordFieldName"].ToString();
                    tempobj.DCDocumentTypeName = sdr["DCDocumentTypeName"].ToString();
                    tempobj.CreatedBy = sdr["CreatedBy"].ToString();
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From RecordField_DocType_Mapping_Table where "
                + " (Select Isnull(DocumentClassName, '') From DocumentClass_table where "
                + " DocumentClass_table.SLNO = RecordField_DocType_Mapping_Table.Document_ClassID) like '%'+@search+'%' "
                + " or(Select Isnull(DocumentType_Name, '') From DocumentType_Table where "
                + " DocumentType_Table.SLNO = RecordField_DocType_Mapping_Table.RecordField_ID) like '%'+@search+'%' "
                + " or(Select Isnull(DocumentType_Name, '') From DocumentType_Table where "
                + " DocumentType_Table.SLNO = RecordField_DocType_Mapping_Table.DocumentType_ID)  like '%'+@search+'%'  ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.RecordField_DocType_Mapping_Table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocTypeRecordFieldMapping_Repository/GetDocTypeRecordFieldMappingListByPage");
            }

            return objModel;

        }







        public DBReturnModel DisableDocTypeRecordFieldMapping(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Disabling Document Type Record Field Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_EnableDisableDocTypeRecordFieldMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/DisableDocTypeRecordFieldMapping");
                    objReturn.ReturnMessage = "System Error on Disabling Document Type Record Field Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/DisableDocTypeRecordFieldMapping");
                objReturn.ReturnMessage = "System Error on Disabling Document Type Record Field Mapping!";
            }

            return objReturn;
        }


        public DBReturnModel EnableDocTypeRecordFieldMapping(long SLNO, long DeletedBy)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Enabling Document Type Record Field Mapping!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_EnableDisableDocTypeRecordFieldMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/EnableDocTypeRecordFieldMapping");
                    objReturn.ReturnMessage = "System Error on Enabling Document Type Record Field Mapping!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/EnableDocTypeRecordFieldMapping");
                objReturn.ReturnMessage = "System Error on Enabling Document Type Record Field Mapping!";
            }

            return objReturn;
        }

               
        public DBReturnModel DeleteDocTypeRecordFieldMapping(long SLNO, long DeletedBy)
        {
            string result = "Error on Deleting Record Field Mapping With Document Type!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_DeleteDocTypeRecordFieldMapping";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnMessage = returnCode.Value.ToString();
                    objreturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/DeleteDocTypeRecordFieldMapping");
                    objreturn.ReturnMessage = "System Error on Deleting Document Type Mapping With Record Field!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "DocTypeRecordFieldMapping_Repository/DeleteDocTypeRecordFieldMapping");
                objreturn.ReturnMessage = "System Error on Deleting Document Type Mapping With Record Field!";
            }

            return objreturn;
        }



    }
}
