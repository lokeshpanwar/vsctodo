﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class WorkFlowUtilization_Repository
    {
        public WorkFlowUserReport_Model GetWorkFlowData(long CreatedBy_Id, long WorkFlowId, long ThreadId, long stageID, string UserRole)
        {
            WorkFlowUserReport_Model objModel = new WorkFlowUserReport_Model();
            List<WorkFlowUserReportStage_Model> objList = new List<WorkFlowUserReportStage_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, workFlowID, Isnull(workFlowName,'') as workFlowName, Isnull(TotalDays,0) as TotalDays, Isnull(workFlowLevel,'') as workFlowLevel, "
                + " Isnull(bucketing, '') as bucketing, Isnull(priority, '') as priority, Isnull(description, '') as description, Isnull(RASCI, '') as RASCI, "
                + " Isnull(DocCompanyID, 0) as DocCompanyID, Isnull(DocDomainID, 0) as DocDomainID, Isnull(DocClassID, 0) as DocClassID, "
                + " Isnull(activeStatus, '') as activeStatus, Isnull(createdOn, '') as createdOn, Isnull(createdBy, 0) as createdBy, "
                + " Isnull(editedOn, '') as editedOn, Isnull(editedBy, 0) as editedBy, Isnull(isDeleted, '') as isDeleted, Isnull(deletedOn, '') as deletedOn, "
                + " Isnull(deletedBy, 0) as deletedBy From workflow_table where workFlowID = @workFlowID";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@workFlowID", WorkFlowId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    objModel.workFlowName = sdr["workFlowName"].ToString();
                    objModel.TotalDays = Convert.ToInt64(sdr["TotalDays"].ToString());
                    objModel.workFlowLevel = sdr["workFlowLevel"].ToString();
                    objModel.bucketing = sdr["bucketing"].ToString();
                    objModel.priority = sdr["priority"].ToString();
                    objModel.description = sdr["description"].ToString();
                    objModel.RASCI = sdr["RASCI"].ToString();
                    objModel.DocCompanyID = Convert.ToInt64(sdr["DocCompanyID"].ToString());
                    objModel.DocDomainID = Convert.ToInt64(sdr["DocDomainID"].ToString());
                    objModel.DocClassID = Convert.ToInt64(sdr["DocClassID"].ToString());
                    objModel.activeStatus = Convert.ToBoolean(sdr["activeStatus"].ToString());
                    objModel.createdOn = Convert.ToDateTime(sdr["createdOn"].ToString());
                    objModel.createdBy = Convert.ToInt64(sdr["createdBy"].ToString());
                    objModel.editedOn = Convert.ToDateTime(sdr["editedOn"].ToString());
                    objModel.editedBy = Convert.ToInt64(sdr["editedBy"].ToString());
                    objModel.isDeleted = Convert.ToBoolean(sdr["isDeleted"].ToString());
                    objModel.deletedOn = Convert.ToDateTime(sdr["deletedOn"].ToString());
                    objModel.deletedBy = Convert.ToInt64(sdr["deletedBy"].ToString());
                    
                }
                sdr.Close();

                sqlstr = "Select stageOrderID From workflow_thread_progress_table WT where workFlowID = @workFlowID and threadID = @threadID";
                sqlstr += " and stageOrderID in (select stageOrderID from [dbo].[workflow_stage_table] where [workFlowID]=@workFlowID AND "
                    + " [threadID]=@threadID and concat(',', Replace(viewright,' ','') , ',') like ',%"+ stageID + "%' and viewright <> '' ) Order by stageOrderID";

                cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@workFlowID", WorkFlowId);
                cmd.Parameters.AddWithValue("@threadID", ThreadId);
                sdr = cmd.ExecuteReader();
                string viewrights = stageID.ToString();

                while (sdr.Read())
                    viewrights += " ," + sdr["stageOrderID"].ToString();
                sdr.Close();

                if (UserRole == "ADMIN")
                    sqlstr = "SELECT * FROM [workflow_thread_progress_table] where [workFlowID]=@workFlowID AND [threadID]=@threadID order by workFlowID, threadID,stageOrderID";
                else
                    sqlstr = string.Format("SELECT * FROM [workflow_thread_progress_table] where [workFlowID]=@workFlowID AND [threadID]=@threadID AND stageOrderID in ({0})  order by stageOrderID ", viewrights);

                cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@workFlowID", WorkFlowId);
                cmd.Parameters.AddWithValue("@threadID", ThreadId);
                sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    WorkFlowUserReportStage_Model tempobj = new WorkFlowUserReportStage_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.threadID = Convert.ToInt64(sdr["threadID"].ToString());
                    tempobj.workFlowID = Convert.ToInt64(sdr["workFlowID"].ToString());
                    tempobj.stageOrderID = sdr["stageOrderID"].ToString();
                    tempobj.stageName = sdr["stageName"].ToString();
                    tempobj.stageDescription = sdr["stageDescription"].ToString();
                    tempobj.stageDays = sdr["stageDays"].ToString();
                    tempobj.people = sdr["people"].ToString();
                    tempobj.peopleInfo = sdr["peopleInfo"].ToString();
                    tempobj.RASCI = sdr["rasci"].ToString();
                    tempobj.attachment = sdr["attachment"].ToString();
                    tempobj.form = sdr["form"].ToString();
                    tempobj.checklist = sdr["checklist"].ToString();
                    tempobj.approverWorkFlow = sdr["approverWorkFlow"].ToString();
                    tempobj.isOpen = Convert.ToBoolean(sdr["isOpen"].ToString());
                    tempobj.isClosed = Convert.ToBoolean(sdr["isClosed"].ToString());
                    tempobj.isWip = Convert.ToBoolean(sdr["isWip"].ToString());
                    objList.Add(tempobj);
                }
                connection.Close();

                objModel.WorkFlowUserReportStage_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy_Id, "Web API", "WorkFlowUtilization_Repository/GetWorkFlowData");
            }
            return objModel;
        }



        public DBReturnModel AddExceptionData(workflow_progress_Model objModel, long CreatedBy_Id)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Inserting Exception Data!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                if(objModel.raisedBy == null)
                {
                    objModel.raisedBy = CreatedBy_Id.ToString();
                }
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "INSERT INTO [workflow_progress_table]([thrdID],[wfID],[stgID],[peopleType],[personID],[processStatus]," +
                        "[comments],[complitionStatus],[threadStatus],[submission_date],[submissionStatus],[submittedBy],[communicateTo])" +
                        "VALUES(@thrdID,@workFlowID,@stageOrder,@peopleType,@raisedBy,@processStatus,@comment,@complitionStatus,@threadStatus," +
                        "@raisedOn,@sub_status,@raisedBy,@communicateTo)";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@workFlowID", objModel.wfID);
                    cmd.Parameters.AddWithValue("@thrdID", objModel.thrdID);
                    cmd.Parameters.AddWithValue("@stageOrder", objModel.stgID);
                    cmd.Parameters.AddWithValue("@raisedTo", objModel.raisedTo);
                    cmd.Parameters.AddWithValue("@raisedMsg", objModel.raisedMsg);
                    cmd.Parameters.AddWithValue("@raisedBy", objModel.raisedBy);
                    cmd.Parameters.AddWithValue("@raisedOn", Standard.StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@status", "NA");
                    cmd.Parameters.AddWithValue("@complitionStatus", false);
                    cmd.Parameters.AddWithValue("@threadStatus", true);
                    cmd.Parameters.AddWithValue("@processStatus", "Exception");
                    cmd.Parameters.AddWithValue("@sub_status", true);
                    cmd.Parameters.AddWithValue("@peopleType", objModel.peopleType);
                    cmd.Parameters.AddWithValue("@comment", objModel.comments);
                    cmd.Parameters.AddWithValue("@communicateTo", objModel.communicateTo);

                    cmd.ExecuteNonQuery();



                    sqlstr = "INSERT INTO [workflow_process_exception] ([workFlowID],[thrdID],[stageOrder],[raisedTo],[raisedMsg],[raisedBy]," +
                        "[raisedOn],[status],[IsClosed]) values(@workFlowID,@thrdID,@stageOrder,@raisedTo,@raisedMsg,@raisedBy,@raisedOn,@status,@IsClosed)";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@workFlowID", objModel.wfID);
                    cmd.Parameters.AddWithValue("@thrdID", objModel.thrdID);
                    cmd.Parameters.AddWithValue("@stageOrder", objModel.stgID);
                    cmd.Parameters.AddWithValue("@raisedTo", objModel.raisedTo);
                    cmd.Parameters.AddWithValue("@raisedMsg", objModel.raisedMsg);
                    cmd.Parameters.AddWithValue("@raisedBy", objModel.raisedBy);
                    cmd.Parameters.AddWithValue("@raisedOn", Standard.StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@status", "NA");
                    cmd.Parameters.AddWithValue("@IsClosed", false);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = "Exception Data Added Successfully!";
                    objReturn.ReturnStatus = "SUCCESS";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy_Id, "API", "WorkFlowStage_Repository/AddExceptionData");
                    objReturn.ReturnMessage = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy_Id, "Web API", "WorkFlowUtilization_Repository/AddExceptionData");
            }
            return objReturn;
        }




        public List<workflow_process_docs_Model> GetWorkFlowProcessDocumentListByClassId(long CreatedBy_Id, long ClassID)
        {
            List<workflow_process_docs_Model> objList = new List<workflow_process_docs_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, Isnull(Sub_CompID,0) as Sub_CompID, Isnull(DomainID,0) as DomainID, Isnull(Doc_Class_ID,0) as Doc_Class_ID, "
                + " Isnull(WFID, 0) as WFID, Isnull(ThrdID, 0) as ThrdID, Isnull(Stage_name, '') as Stage_name, Isnull(Stage_order, 0) as Stage_order, "
                + " Isnull(status, '') as status, Isnull(UploadedBy, 0) as UploadedBy, Isnull(UploadedOn, '') as UploadedOn, Isnull(FileName, '') as FileName, "
                + " Isnull(FilePath, '') as FilePath, Isnull(FileSize, '') as FileSize, Isnull(cmt, '') as cmt, Isnull(verID, '') as verID "
                + " From workflow_process_docs where Isnull(Doc_Class_ID,0)= @Doc_Class_ID";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Doc_Class_ID", ClassID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    workflow_process_docs_Model tempobj = new workflow_process_docs_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Doc_Class_ID = Convert.ToInt64(sdr["Doc_Class_ID"].ToString());
                    tempobj.WFID = Convert.ToInt64(sdr["WFID"].ToString());
                    tempobj.ThrdID = Convert.ToInt64(sdr["ThrdID"].ToString());
                    tempobj.Stage_name = sdr["Stage_name"].ToString();
                    tempobj.Stage_order = Convert.ToInt64(sdr["Stage_order"].ToString());
                    tempobj.status = sdr["status"].ToString();
                    tempobj.UploadedBy = Convert.ToInt64(sdr["UploadedBy"].ToString());
                    tempobj.UploadedOn = Convert.ToDateTime(sdr["UploadedOn"].ToString());
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.FilePath = sdr["FilePath"].ToString();
                    tempobj.FileSize = sdr["FileSize"].ToString();
                    tempobj.cmt = sdr["cmt"].ToString();
                    tempobj.verID = sdr["verID"].ToString();
                    objList.Add(tempobj);
                }
                
                connection.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy_Id, "Web API", "WorkFlowUtilization_Repository/GetWorkFlowProcessDocumentListByClassId");
            }
            return objList;
        }


        public List<workflow_thread_tracker_Model> GetWorkFlowThreadTracker(long CreatedBy_Id)
        {
            List<workflow_thread_tracker_Model> objList = new List<workflow_thread_tracker_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "SELECT SLNO, workFlowName, '0' as killed, "
                + " (SELECT COUNT(workFlowID) AS c FROM dbo.workflow_thread_progress_table AS wtp WHERE(workFlowID = wf.SLNO) AND(stageOrderID = 1)) AS TotalProcess, "
                + " (select COUNT(distinct threadID)  AS c FROM dbo.workflow_thread_progress_table  where workflowid = wf.SLNO and isOnHold = 0 and iswip = 1 and threadStatus = 1 and complitionStatus = 0 and DATEDIFF(DAY, end_date, getdate()) < 0) as wip_lag, "
                + " (select COUNT(distinct threadID)  AS c FROM dbo.workflow_thread_progress_table where workflowid = wf.SLNO and isOnHold = 0 and iswip = 1 and threadStatus = 1 and complitionStatus = 0 and DATEDIFF(DAY, end_date, getdate())>= 0) as wip_lead, "
                + " (select COUNT(distinct threadID)  AS c FROM dbo.workflow_thread_progress_table where workflowid = wf.SLNO  and isOnHold = 0 and isopen = 1 and threadStatus = 1 and complitionStatus = 0 and DATEDIFF(DAY, end_date, getdate())< 0) as open_lag, "
                + " (select COUNT(distinct threadID)  AS c FROM dbo.workflow_thread_progress_table where workflowid = wf.SLNO  and isOnHold = 0 and isopen = 1 and threadStatus = 1 and complitionStatus = 0 and DATEDIFF(DAY, end_date, getdate())>= 0) as open_lead, "
                + " (select COUNT(distinct threadID)  AS c FROM dbo.workflow_thread_progress_table where workflowid = wf.SLNO   and isOnHold = 1 and threadStatus = 1 and complitionStatus = 0 ) as hold, "
                + " (SELECT   COUNT(distinct wt.threadID) AS c "
                + " FROM dbo.workflow_progress_table AS wp INNER JOIN dbo.workflow_thread_progress_table AS wt ON wp.thrdID = wt.threadID AND wp.wfID = wt.workFlowID AND wp.stgID = wt.stageOrderID "
                + " where wt.workflowid = wf.SLNO and wt.stageOrderID = (select top(1) stgID from dbo.workflow_progress_table where wfID = wt.workflowid and thrdID = wt.threadID order by slno desc) "
                + " and wt.threadStatus = 0 and wt.complitionStatus = 1 and DATEDIFF(DAY, wt.end_date, wp.submission_date)< 0) as close_lag, "
                + " (SELECT   COUNT(distinct wt.threadID) FROM dbo.workflow_progress_table AS wp INNER JOIN "
                + " dbo.workflow_thread_progress_table AS wt ON wp.thrdID = wt.threadID AND wp.wfID = wt.workFlowID AND wp.stgID = wt.stageOrderID "
                + " where wt.workflowid = wf.SLNO and wt.stageOrderID = (select top(1) stgID from dbo.workflow_progress_table where wfID = wt.workflowid and thrdID = wt.threadID order by slno desc) "
                + " and wt.threadStatus = 0 and wt.complitionStatus = 1 and DATEDIFF(DAY, wt.end_date, wp.submission_date)>= 0) as close_lead "
                + " FROM dbo.workflow_table AS wf where 1 = 1 and isDeleted = 0 ";
                //+ " and owner = '" + owner + "' "
                //+ " and owner LIKE '%"+ cnameX + "%' "
                //+ " and slno in (select distinct workflowid from workflow_thread_progress_table  where peopleID = " + userslid + ")";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@workFlowID", WorkFlowId);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    workflow_thread_tracker_Model tempobj = new workflow_thread_tracker_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.killed = sdr["killed"].ToString();
                    tempobj.workFlowName = sdr["workFlowName"].ToString();
                    tempobj.TotalProcess = Convert.ToInt64(sdr["TotalProcess"].ToString());
                    tempobj.wip_lag = Convert.ToInt64(sdr["wip_lag"].ToString());
                    tempobj.wip_lead = Convert.ToInt64(sdr["wip_lead"].ToString());
                    tempobj.open_lag = Convert.ToInt64(sdr["open_lag"].ToString());
                    tempobj.open_lead = Convert.ToInt64(sdr["open_lead"].ToString());
                    tempobj.hold = Convert.ToInt64(sdr["hold"].ToString());
                    tempobj.close_lag = Convert.ToInt64(sdr["close_lag"].ToString());
                    tempobj.close_lead = Convert.ToInt64(sdr["close_lead"].ToString());
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy_Id, "Web API", "WorkFlowUtilization_Repository/GetWorkFlowThreadTracker");
            }
            return objList;
        }


        public workflow_progress_table_Model GetOneWorkFlowProgress(long wfID, long thrdID, long StgID, long CreatedBy)
        {
            workflow_progress_table_Model objModel = new workflow_progress_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectOneWorkFlowThreadProgress";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@thrdID", thrdID);
                cmd.Parameters.AddWithValue("@wfID", wfID);
                cmd.Parameters.AddWithValue("@stgID", StgID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.thrdID = Convert.ToInt64(sdr["thrdID"].ToString());
                    objModel.wfID = Convert.ToInt64(sdr["wfID"].ToString());
                    objModel.stgID = Convert.ToInt64(sdr["stgID"].ToString());
                    objModel.peopleType = sdr["peopleType"].ToString();
                    objModel.personID = sdr["personID"].ToString();
                    objModel.formID = sdr["formID"].ToString();
                    objModel.formData = sdr["formData"].ToString();
                    objModel.chkID = sdr["chkID"].ToString();
                    objModel.chkData = sdr["chkData"].ToString();
                    objModel.processStatus = sdr["processStatus"].ToString();
                    objModel.comments = sdr["comments"].ToString();
                    objModel.complitionStatus = Convert.ToBoolean(sdr["complitionStatus"].ToString());
                    objModel.threadStatus = Convert.ToBoolean(sdr["threadStatus"].ToString());
                    objModel.recv_date = Convert.ToDateTime(sdr["recv_date"].ToString());

                    objModel.start_date = Convert.ToDateTime(sdr["start_date"].ToString());
                    objModel.end_date = Convert.ToDateTime(sdr["end_date"].ToString());
                    objModel.submission_date = Convert.ToDateTime(sdr["submission_date"].ToString());
                    objModel.submissionStatus = Convert.ToBoolean(sdr["submissionStatus"].ToString());
                    objModel.submittedBy = sdr["submittedBy"].ToString();
                    objModel.communicateTo = sdr["communicateTo"].ToString();
                    objModel.defaultFile = sdr["defaultFile"].ToString();
                }

                connection.Close();
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "Web API", "WorkFlowUtilization_Repository/GetOneWorkFlowProgress");
            }

            return objModel;
        }

    }
}
