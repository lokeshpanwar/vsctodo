﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Auth_Repository
    {
        Theme_Repository objThemeVM = new Theme_Repository();


        public OTP_Return_Value GetOTPForLogIn(Login_Model model)
        {
            OTP_Return_Value objModel = new OTP_Return_Value();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_Generate_OTP";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserName", model.UserName);
                cmd.Parameters.AddWithValue("@Password", model.Password);
                cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);
                cmd.Parameters.AddWithValue("@MacAddress", model.MacAddress);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objModel.UserId = Convert.ToInt64(sdr["UserId"].ToString());
                    objModel.return_OTP = Convert.ToString(sdr["OTP"].ToString());
                    objModel.LoginStatus = Convert.ToString(sdr["Status"].ToString());
                    objModel.LoginStatusMessage = Convert.ToString(sdr["StatusMessage"].ToString());
                    objModel.MobileNo = Convert.ToString(sdr["MobileNo"].ToString());
                    objModel.IsSameDevice = Convert.ToInt32(sdr["IsSameDevice"].ToString());
                }

                sdr.Close();
                if (objModel.UserId > 0)
                {
                    objModel.LoginStatus = ResponseStatus.SUCCESS;
                }
                else
                {
                    objModel.LoginStatus = ResponseStatus.FAILED;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/GetOTPForLogIn");
            }
            return objModel;
        }

        public LoginReturn_Model CheckLogin(Login_Model model)
        {
            LoginReturn_Model objModel = new LoginReturn_Model();
            try
            {
                objModel = ValidateLogin(model);

                if (objModel.LoginStatus == ResponseStatus.SUCCESS)
                {
                    objModel.Token = CreateToken(objModel.UserId, objModel.UserName, objModel.RoleId, objModel.EmployeeName, objModel.UserMobile, model.IPAddress, objModel.OfficeId);
                    objModel.Theme = "~/Views/Shared/_Layout1.cshtml";
                    //if (objModel.RoleId == 1)
                    //{
                    //    objModel.UserType = StandardUserType.SUPERADMIN;
                    //    objModel.UserRole = StandardUserType.SUPERADMIN;
                    //}
                    objModel.UserDivision = "";
                    objModel.UserSubCompId = 0;
                    objModel.UserDomainId = 0;
                    objModel.UserParentCompId = 0;
                }

                #region Previous Code
                //if (model.UserName == "SuperAdmin")
                //{
                //    if (model.Password == "123")
                //    {
                //        objModel.UserId = 2;
                //        objModel.Token = WriteCookie(model.UserName, 1, "SuperAdmin", objModel.UserId, 0, 0, model.IPAddress,"");
                //        objModel.UserSubCompId = 0;
                //        objModel.UserDomainId = 0;
                //        objModel.UserParentCompId = 0;
                //        objModel.Theme = "";
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        objModel.UserType = "SuperAdmin";

                //        objModel.UserEmail = "SuperAdmin@testmail.com";
                //        objModel.UserName = "SuperAdmin";
                //        objModel.UserRole= "SuperAdmin";
                //        objModel.EmployeeName = "Super Admin";
                //        objModel.UserDivision = "";

                //        objModel.Theme = "~/Views/Shared/_Layout1.cshtml";
                //    }
                //    else
                //    {
                //        objModel.Token = "";
                //        objModel.LoginStatus = "Enter Valid Password!!!";
                //    }
                //}
                //#region New Code By Vijendra
                //else if (model.UserName == "Auditor")
                //{
                //    if (model.Password == "123")
                //    {
                //        objModel.UserId = 10;
                //        objModel.Token = WriteCookie(model.UserName, 1, "Auditor", objModel.UserId, 0, 0, model.IPAddress, "");
                //        objModel.UserSubCompId = 0;
                //        objModel.UserDomainId = 0;
                //        objModel.UserParentCompId = 0;
                //        objModel.Theme = "";
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        objModel.UserType = "Auditor";

                //        objModel.UserEmail = "Auditor@testmail.com";
                //        objModel.UserName = "Auditor";
                //        objModel.UserRole = "Auditor";
                //        objModel.EmployeeName = "Auditor Name";
                //        objModel.UserDivision = "";
                //    }
                //    else
                //    {
                //        objModel.Token = "";
                //        objModel.LoginStatus = "Enter Valid Password!!!";
                //    }
                //}
                //else if (model.UserName == "Auditee")
                //{
                //    if (model.Password == "123")
                //    {
                //        objModel.UserId = 20;
                //        objModel.Token = WriteCookie(model.UserName, 1, "Auditee", objModel.UserId, 0, 0, model.IPAddress, "");
                //        objModel.UserSubCompId = 0;
                //        objModel.UserDomainId = 0;
                //        objModel.UserParentCompId = 0;
                //        objModel.Theme = "";
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        objModel.UserType = "Auditee";

                //        objModel.UserEmail = "Auditee@testmail.com";
                //        objModel.UserName = "Auditee";
                //        objModel.UserRole = "Auditee";
                //        objModel.EmployeeName = "Auditee Name";
                //        objModel.UserDivision = "";
                //    }
                //    else
                //    {
                //        objModel.Token = "";
                //        objModel.LoginStatus = "Enter Valid Password!!!";
                //    }
                //}
                //#endregion
                //else if (model.UserName == "Public")
                //{
                //    if (model.Password == "123")
                //    {
                //        objModel.UserId = 3;
                //        objModel.Token = WriteCookie(model.UserName, 1, "Public", objModel.UserId, 0, 0, model.IPAddress, "");
                //        objModel.UserSubCompId = 0;
                //        objModel.UserDomainId = 0;
                //        objModel.UserParentCompId = 0;
                //        objModel.Theme = "";
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        objModel.UserType = "Public";

                //        objModel.UserEmail = "public@testmail.com";
                //        objModel.UserName = "Public";
                //        objModel.UserRole = "Public";
                //        objModel.EmployeeName = "Public";
                //        objModel.UserDivision = "";
                //    }
                //    else
                //    {
                //        objModel.Token = "";
                //        objModel.LoginStatus = "Enter Valid Password!!!";
                //    }
                //}
                //else if (objModel.LoginStatus == ResponseStatus.SUCCESS)
                //{
                //    if (objModel.UserType == StandardUserType.ADMIN)
                //    {
                //        objModel.Token = WriteCookie(model.UserName, 1, StandardUserType.ADMIN, objModel.UserId, 0, 0, model.IPAddress,"");
                //        objModel.UserSubCompId = 0;
                //        objModel.UserDomainId = 0;
                //        objModel.UserParentCompId = 1;
                //        objModel.Theme = "";
                //        var data = objThemeVM.GetOneTheme(1, model.IPAddress);
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        if (data.Theme_Name != "")
                //        {
                //            objModel.Theme = data.Theme_Name;
                //        }
                //        else
                //        {
                //            objModel.Theme = "~/Views/Shared/_Layout1.cshtml";
                //        }
                //        objModel.UserDivision = "";
                //    }
                //    //else if ((objModel.UserType.ToUpper() == "EMPLOYEE") || (objModel.UserType.ToUpper() == "USER"))
                //    //{
                //    //    objModel.Token = WriteCookie(model.UserName, 1, "EMPLOYEE", objModel.UserId, objModel.UserSubCompId, objModel.UserDomainId, model.IPAddress);
                //    //    objModel.UserParentCompId = 1;
                //    //    objModel.Theme = "";
                //    //    objModel.LoginStatus = ResponseStatus.SUCCESS;
                //    //    var data = objThemeVM.GetOneTheme(1, model.IPAddress);
                //    //    objModel.LoginStatus = ResponseStatus.SUCCESS;
                //    //    if (data.Theme_Name != "")
                //    //    {
                //    //        objModel.Theme = data.Theme_Name;
                //    //    }
                //    //    else
                //    //    {
                //    //        objModel.Theme = "~/Views/Shared/_Layout1.cshtml";
                //    //    }
                //    //}
                //    else
                //    {
                //        objModel.Token = WriteCookie(model.UserName, 1, StandardUserType.EMPLOYEE, objModel.UserId, objModel.UserSubCompId, objModel.UserDomainId, model.IPAddress, objModel.UserType.ToString());
                //         if (objModel.UserType == StandardUserType.VENDOR)
                //        {
                //            objModel.POZone = GetPOCompany(Convert.ToInt64(objModel.EmpId));
                //        }
                //        objModel.UserParentCompId = 1;
                //        objModel.Theme = "";
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        var data = objThemeVM.GetOneTheme(1, model.IPAddress);
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        if (data.Theme_Name != "")
                //        {
                //            objModel.Theme = data.Theme_Name;
                //        }
                //        else
                //        {
                //            objModel.Theme = "~/Views/Shared/_Layout1.cshtml";
                //        }
                //        objModel.UserDivision = objModel.UserType.ToString();
                //    }
                //}
                #endregion
                else
                {
                    objModel.Theme = "Enter Valid Credentials!!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/CheckLogin");
            }
            return objModel;
        }

        public static string CreateToken(long userId, string userName, int roleId, string employeeName, string userMobile, string iPAddress, int officeId)
        {
            string token = userId + "," + userName + "," + roleId + "," + employeeName + "," + userMobile + "," + iPAddress + "," + officeId + "," + StandardDateTime.GetDateTime();
            string strCookieValue = CryptorEngine.Encrypt(token, true);
            return strCookieValue;
        }

        public static string WriteCookie(string email, int uType, string Name, Int64 id, long subcompid, long domainid, string ipaddress,string uDivision)
        {
            //string Token = id + "," + uType + "," + email + "," + Name + "," + StandardDateTime.GetDateTime();

            //string Token = id + "," + uType + "," + email + "," + Name + "," + subcompid + "," + domainid + "," + StandardDateTime.GetDateTime();

            string Token = id + "," + uType + "," + email + "," + Name + "," + subcompid + "," + domainid + "," + ipaddress + ","  + uDivision + "," +  StandardDateTime.GetDateTime() ;

            string strCookieValue = CryptorEngine.Encrypt(Token, true);// + " " + CryptorEngine.Encrypt(DateTime.Now.ToShortDateString(), true);

            return strCookieValue;
        }


        private LoginReturn_Model ValidateLogin(Login_Model model)
        {
            LoginReturn_Model objModel = new LoginReturn_Model();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_UserLogIn";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserId", model.UserId);
                cmd.Parameters.AddWithValue("@UserName", model.UserName);
                cmd.Parameters.AddWithValue("@Vertical", model.Vertical);
                //cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);
                //cmd.Parameters.AddWithValue("@OTP", model.OTP);
                //cmd.Parameters.AddWithValue("@MacAddress", model.MacAddress);
                //cmd.Parameters.AddWithValue("@IsSameMac", model.IsSameMac);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objModel.UserId = Convert.ToInt64(sdr["Id"].ToString());
                    objModel.EmpId = Convert.ToString(sdr["Userid"].ToString());  
                    objModel.Name = Convert.ToString(sdr["Name"].ToString());
                    objModel.UserName = Convert.ToString(sdr["Username"].ToString());
                    objModel.Vertical = Convert.ToInt32(sdr["Vertical"].ToString());
                    objModel.Role = Convert.ToInt32(sdr["Role"].ToString());
                    objModel.OfficeId = Convert.ToInt32(sdr["Officeid"].ToString());
                    objModel.UserMobile = Convert.ToString(sdr["Mobile"].ToString());
                    objModel.UserEmail = Convert.ToString(sdr["Email"].ToString());
                }

                sdr.Close();
                if (objModel.UserId > 0)
                {
                    objModel.LoginStatus = ResponseStatus.SUCCESS;
                }
                else
                {
                    objModel.LoginStatus = ResponseStatus.FAILED;
                }


                #region Previous Code

                //long UID = 0;
                //var connection = SqlHelper.Connection();
                //connection.Open();
                //string sqlstr = " Select * from Users where UserName=@UserId and Password = @Password and ISNULL(IsDeleted,0)=0 and IsActive = 1 ";

                //SqlCommand cmd = new SqlCommand(sqlstr, connection);
                //cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@UserId", model.UserName);
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //SqlDataReader sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    UID = Convert.ToInt64(sdr["UId"].ToString());
                //}
                //sdr.Close();




                //if (UID>0)
                //{
                //    sqlstr = "Select EmployeeName, Isnull(SLNO,0) as SLNO, Isnull(Sub_Company,0) as Sub_Company,Isnull(Domain_ID,0) as Domain_ID, "
                //   + " Isnull(User_type,'') as User_type,Isnull(Userid,'') as Userid, Isnull(Email,'') as Email, "
                //   + " Isnull(EmpPhoto,'') as EmpPhoto, isnull(EmpId,'') as EmpId From Employee_table where SLNO=@UID";
                //    cmd.Parameters.Clear();
                //    cmd.CommandText = sqlstr;
                //    cmd.Connection = connection;
                //    cmd.Parameters.AddWithValue("@UID", UID);
                //    sdr = cmd.ExecuteReader();
                //    while (sdr.Read())
                //    {
                //        objModel.UserId = Convert.ToInt64(sdr["SLNO"].ToString());
                //        objModel.UserParentCompId = Convert.ToInt64(sdr["Sub_Company"].ToString());
                //        objModel.UserSubCompId = Convert.ToInt64(sdr["Sub_Company"].ToString());
                //        objModel.UserDomainId = Convert.ToInt64(sdr["Domain_ID"].ToString());
                //        objModel.LoginStatus = ResponseStatus.SUCCESS;
                //        objModel.UserType = sdr["User_type"].ToString().ToUpper();
                //        objModel.EmployeeName = sdr["EmployeeName"].ToString();
                //        //objModel.UserName = sdr["EmployeeName"].ToString();
                //        objModel.UserName = sdr["Userid"].ToString();
                //        objModel.UserEmail = sdr["Email"].ToString();
                //        if (sdr["EmpPhoto"].ToString() != "")
                //        {
                //            //objModel.UserImage = "Content/CustomImages/Employee/" + sdr["EmpPhoto"].ToString();
                //            objModel.UserImage = "Content/CustomImages/Employee/" + sdr["EmpPhoto"].ToString();
                //            var path = Path.Combine(webpath, objModel.UserImage);
                //            objModel.UserImage = GlobalFunction.getImageURL() + "/api/Employee/GetEmployeeImage?path=" + path;
                //        }
                //        else
                //        {
                //            objModel.UserImage = "";
                //        }

                //        objModel.UserRoleId = 0;
                //        objModel.UserRole = "";
                //        objModel.EmpId = sdr["EmpId"].ToString();
                //    }
                //    sdr.Close();
                //}


                //sqlstr = "Select SLNO,RoleName From Role_table where SLNO in (Select Role_ID From Role_Mapping_Table "
                //+ " where User_ID = @User_ID)";
                //cmd.Parameters.Clear();
                //cmd.CommandText = sqlstr;
                //cmd.Connection = con;
                //cmd.Parameters.AddWithValue("@User_ID", objModel.UserId);
                //sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    objModel.UserRoleId = Convert.ToInt64(sdr["SLNO"].ToString());
                //    objModel.UserRole = sdr["RoleName"].ToString();
                //}
                //sdr.Close();



                //List<Role_Modules_Permission_Model> objList = new List<Role_Modules_Permission_Model>();
                //sqlstr = "Select * From Role_Modules_Permission "
                //+ " where RoleID in (Select SLNO From Role_table "
                //+ " where SLNO in (Select Role_ID from Role_Mapping_Table where User_ID = @User_ID))";
                //cmd.Parameters.Clear();
                //cmd.CommandText = sqlstr;
                //cmd.Connection = connection;
                //cmd.Parameters.AddWithValue("@User_ID", objModel.UserId);
                //sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    Role_Modules_Permission_Model tempobj = new Role_Modules_Permission_Model();
                //    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                //    tempobj.ModuleID = Convert.ToInt32(sdr["ModuleID"].ToString());
                //    tempobj.RoleID = Convert.ToInt64(sdr["RoleID"].ToString());
                //    tempobj.FormName = sdr["FormName"].ToString();
                //    tempobj.IsRight = Convert.ToBoolean(sdr["IsRight"].ToString());
                //    objList.Add(tempobj);
                //}
                //sdr.Close();
                //objModel.Role_Modules_Permission_Model_List = objList;

                //List<RoleFormList> objRoleFormList = new List<RoleFormList>();
                //sqlstr = "Select Distinct GroupName From tblModulesForm where FormName in " 
                //+ " (Select FormName From Role_Modules_Permission "
                //+ " where RoleID in (Select SLNO From Role_table "
                //+ " where SLNO in (Select Role_ID from Role_Mapping_Table where User_ID = @User_ID))) ";
                //cmd.Parameters.Clear();
                //cmd.CommandText = sqlstr;
                //cmd.Connection = con;
                //cmd.Parameters.AddWithValue("@User_ID", objModel.UserId);
                //sdr = cmd.ExecuteReader();
                //while (sdr.Read())
                //{
                //    RoleFormList tempobj = new RoleFormList();
                //    tempobj.GroupName = sdr["GroupName"].ToString();
                //    objRoleFormList.Add(tempobj);
                //}
                //sdr.Close();
                //objModel.GroupNameList = objRoleFormList;

                //con.Close();
                //if (objModel.UserRoleId > 0)
                //{
                //    objModel.Role_Modules_Permission_Model_List = GetModulePermissionForRole(objModel.UserRoleId);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/ValidateLogin");
            }
            return objModel;
        }


        private List<Role_Modules_Permission_Model> GetModulePermissionForRole(long RoleID)
        {
            List<Role_Modules_Permission_Model> objList = new List<Role_Modules_Permission_Model>();

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectRoleModulePermissionInLoginByRoleId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Role_Modules_Permission_Model tempobj = new Role_Modules_Permission_Model();
                    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.ModuleName = sdr["ModuleName"].ToString();
                    tempobj.GroupName = sdr["GroupName"].ToString();
                    tempobj.IsRight = Convert.ToBoolean(sdr["IsRight"].ToString());

                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/GetModulePermissionForRole");
            }
            return objList;
        }





        public string CheckIfRoleHavingPermissionForForm(long RoleID, string FormName)
        {
            string result = "User's Role Is Not having Permission For This Form!";

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select Isnull(IsRight,'') as IsRight From Role_Modules_Permission "
                    + " where RoleID=@RoleID and FormName=@FormName";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@RoleID", RoleID);
                cmd.Parameters.AddWithValue("@FormName", FormName);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    if(Convert.ToBoolean(sdr["IsRight"].ToString()) == true)
                    {
                        result = "";
                    }
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/GetModulePermissionForRole");
            }
            return result;
        }




        public RoleFormList GetRoleMenuListFromUserID(long UserID)
        {
            RoleFormList objModel = new RoleFormList();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                List<Role_Modules_Permission_Model> objList = new List<Role_Modules_Permission_Model>();

                string sqlstr = "Select * From Role_Modules_Permission "
                + " where RoleID in (Select SLNO From Role_table "
                + " where SLNO in (Select Role_ID from Role_Mapping_Table where User_ID = @User_ID))";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                SqlDataReader sdr = cmd.ExecuteReader();
                                
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Role_Modules_Permission_Model tempobj = new Role_Modules_Permission_Model();
                    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                    tempobj.ModuleID = Convert.ToInt32(sdr["ModuleID"].ToString());
                    tempobj.RoleID = Convert.ToInt64(sdr["RoleID"].ToString());
                    tempobj.FormName = sdr["FormName"].ToString();
                    tempobj.IsRight = Convert.ToBoolean(sdr["IsRight"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();
                //objModel.Role_Modules_Permission_Model_List = objList;


                sqlstr = "Select Distinct GroupName From tblModulesForm where FormName in (Select FormName From Role_Modules_Permission "
                + " where RoleID in (Select SLNO From Role_table "
                + " where SLNO in (Select Role_ID from Role_Mapping_Table where User_ID = @User_ID))) ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.GroupName = sdr["GroupName"].ToString();
                }
                sdr.Close();


                connection.Close();
                
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/GetRoleMenuListFromUserID");
            }
            return objModel;
        }



        private string GetPOCompany(long vendorid)
        {
            string Company = "";

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                List<Role_Modules_Permission_Model> objList = new List<Role_Modules_Permission_Model>();

                string sqlstr = "Select vendor_id,isnull(zone,'') as zone from vendor_zone_selection where vendor_id=@vendor_id";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@vendor_id", vendorid);
                SqlDataReader sdr = cmd.ExecuteReader();
                //sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Company = sdr["zone"].ToString();
                }
                sdr.Close();
                //objModel.Role_Modules_Permission_Model_List = objList;



                connection.Close();

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "AuthRepository/GetRoleMenuListFromUserID");
            }

            return Company;
        }

    }
}

