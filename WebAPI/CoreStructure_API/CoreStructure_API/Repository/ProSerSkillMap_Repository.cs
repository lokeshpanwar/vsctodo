﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class ProSerSkillMap_Repository
    {

        public DBReturnModel InsertProSerSkillMap(ProSerSkillMap_Model objModel)
        {
            string result = "Error on Inserting Service/Product Map!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertProSerSkillMap";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@ProSerId", objModel.ProSerId);
                    cmd.Parameters.AddWithValue("@VendorId", objModel.VendorId);
                    cmd.Parameters.AddWithValue("@Rating", objModel.Rating);
                    cmd.Parameters.AddWithValue("@creationdate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@creationuser", objModel.creationuser);
                    cmd.Parameters.AddWithValue("@isdeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();

                    connection.Close();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "ProSerSkillMap_Repository/InsertProSerSkillMap");
                    objreturn.ReturnMessage = "System Error on Inserting Service/Product";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "ProSerSkillMap_Repository/InsertProSerSkillMap");
            }
            return objreturn;

        }


        public ProSerSkillMap_Model GetOneProSerSkillMap(long EntryID, long creationuser)
        {
            ProSerSkillMap_Model objModel = new ProSerSkillMap_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectProSerSkillMapListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EntryID", EntryID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.EntryID = Convert.ToInt64(sdr["EntryID"].ToString());
                    objModel.ProSerId = Convert.ToInt64(sdr["ProSerId"].ToString());
                    objModel.VendorId = Convert.ToInt64(sdr["VendorId"].ToString());
                    objModel.Rating = Convert.ToSingle(sdr["Rating"].ToString());
                    objModel.creationuser = sdr["creationuser"].ToString();
                    objModel.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objModel.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objModel.modifactionuser = sdr["modifactionuser"].ToString();
                    objModel.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, creationuser, "API", "ProSerSkillMap_Repository/GetOneProSerSkillMap");
            }
            return objModel;
        }


        public DBReturnModel UpdateProSerSkillMap(ProSerSkillMap_Model objModel)
        {
            string result = "Error on Updating Service/Product Map";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateProSerSkillMap";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryID", objModel.EntryID);
                    cmd.Parameters.AddWithValue("@ProSerId", objModel.ProSerId);
                    cmd.Parameters.AddWithValue("@VendorId", objModel.VendorId);
                    cmd.Parameters.AddWithValue("@Rating", objModel.Rating);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", objModel.modifactionuser);


                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "ProSerSkillMap_Repository/UpdateProSerSkillMap");
                    objreturn.ReturnMessage = "System Error on Updating Service/Product Map";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Convert.ToInt64(objModel.creationuser), "API", "ProSerSkillMap_Repository/UpdateProSerSkillMap");
            }
            return objreturn;
        }


        public DBReturnModel DisableProSerSkillMap(long EntryID, long modifactionuser)
        {

            string result = "Error on Disabling Service/Product Map";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update ProSerSkillMap set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where EntryID=@EntryID";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryID", EntryID);
                    cmd.Parameters.AddWithValue("@isdeleted", true);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Service/Product Map Disabled Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "ProSerSkillMap_Repository/DisableProSerSkillMap");
                    objreturn.ReturnMessage = "System Error on Disabling Service/Product Map";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "ProSerSkillMap_Repository/DisableProSerSkillMap");
            }

            return objreturn;
        }


        public DBReturnModel EnableProSerSkillMap(long EntryID, long modifactionuser)
        {

            string result = "Error on Enabling Service/Product Map";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update ProSerSkillMap set isdeleted=@isdeleted,modifactiondate=@modifactiondate,modifactionuser=@modifactionuser  where EntryID=@EntryID";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@EntryID", EntryID);
                    cmd.Parameters.AddWithValue("@isdeleted", false);
                    cmd.Parameters.AddWithValue("@modifactiondate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@modifactionuser", modifactionuser);
                    x = cmd.ExecuteNonQuery();
                    if (x > 0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Service/Product Map Enable Successfully!";
                    }

                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, modifactionuser, "API", "ProSerSkillMap_Repository/EnableProSerSkillMap");
                    objreturn.ReturnMessage = "System Error on Enabling Service/Product Map";
                }
            }

            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, modifactionuser, "API", "ProSerSkillMap_Repository/EnableProSerSkillMap");
            }

            return objreturn;
        }

        public ProSerSkillMap_page_Model GetProSerSkillMapByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            ProSerSkillMap_page_Model objModel = new ProSerSkillMap_page_Model();
            List<ProSerSkillMap_Model> objlist = new List<ProSerSkillMap_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "EntryID";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectProSerSkillMapByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ProSerSkillMap_Model objtemp = new ProSerSkillMap_Model();
                    objtemp.EntryID = Convert.ToInt64(sdr["EntryID"].ToString());
                    objtemp.ProSerId = Convert.ToInt64(sdr["ProSerId"].ToString());
                    objtemp.VendorId = Convert.ToInt64(sdr["VendorId"].ToString());
                    objtemp.Rating = Convert.ToInt32(sdr["Rating"].ToString());
                    objtemp.creationuser = sdr["creationuser"].ToString();
                    objtemp.creationdate = Convert.ToDateTime(sdr["creationdate"].ToString());
                    objtemp.isdeleted = Convert.ToBoolean(sdr["isdeleted"].ToString());
                    objtemp.modifactionuser = sdr["modifactionuser"].ToString();
                    objtemp.modifactiondate = Convert.ToDateTime(sdr["modifactiondate"].ToString());
                    objtemp.EmployeeName = sdr["EmployeeName"].ToString();
                    objtemp.ServiceProductName = sdr["ServiceProductName"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = " Select Count(*) As Count From ProSerSkillMap  where (select SerProductname from MastServPro where MastServPro.ProSerid=ProSerSkillMap.ProSerId) like '%'+@search+'%' " +
                "or(select EmployeeName from Employee_table where User_type in ( 'VENDOR','CUSTOMER') and ISNULL(IsDeleted,0)=0  and Employee_table.SLNO = ProSerSkillMap.VendorId) like '%'+@search+'%'";

                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }


                connection.Close();
                objModel.ProSerSkillMap_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "ProSerSkillMap_Repository/GetProSerSkillMapByPage");

            }

            return objModel;
        }

    }
}
