﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class Claim_Repository
    {
        public List<Policy> GetPolicyListForClaim(int OfficeId, long CreatedBy)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetPolicyListForClaim";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                        IsClaimed = Convert.ToInt32(sdr["IsClaimed"].ToString()),
                        MobileNo=Convert.ToString(sdr["MobileNo"].ToString())
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/GetPolicyListForClaim");
            }
            return objList;
        }

        public string SavePALodgeClaimDetails(LodgeClaim_Model objModel)
        {
            string result = "Error on Claim Lodge!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SavePALodgeClaimDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClaimId", objModel.ClaimId);
                    cmd.Parameters.AddWithValue("@PolicyId", objModel.PolicyId);
                    cmd.Parameters.AddWithValue("@ClaimOfficer", objModel.ClaimOfficer);
                    cmd.Parameters.AddWithValue("@ClaimAmount", objModel.ClaimAmount);
                    cmd.Parameters.AddWithValue("@ClaimDate", objModel.ClaimDate);
                    cmd.Parameters.AddWithValue("@ClaimNo", objModel.ClaimNo);
                    cmd.Parameters.AddWithValue("@TPName", objModel.TPName);
                    cmd.Parameters.AddWithValue("@Mobile", objModel.Mobile);
                    cmd.Parameters.AddWithValue("@ClaimFileName", objModel.ClaimFileName);
                    cmd.Parameters.AddWithValue("@fileExt", objModel.fileExt);
                    cmd.Parameters.AddWithValue("@fileData", objModel.fileData);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@TPSurveyor", objModel.TPSurveyor);
                    cmd.Parameters.AddWithValue("@TPSurveyorContactNo", objModel.TPSurveyorContactNo);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Claim_Repository/SavePALodgeClaimDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Claim_Repository/SavePALodgeClaimDetails");
            }
            return result;
        }

        public string SaveMedicalClaimDetails(LodgeClaim_Model objModel)
        {
            string result = "Error on Claim Lodge!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveMedicalClaimDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClaimId", objModel.ClaimId);
                    cmd.Parameters.AddWithValue("@PolicyId", objModel.PolicyId);
                    cmd.Parameters.AddWithValue("@ClaimOfficer", objModel.ClaimOfficer);
                    cmd.Parameters.AddWithValue("@ClaimAmount", objModel.ClaimAmount);
                    cmd.Parameters.AddWithValue("@ClaimDate", objModel.ClaimDate);
                    cmd.Parameters.AddWithValue("@ClaimNo", objModel.ClaimNo);
                    cmd.Parameters.AddWithValue("@TPName", objModel.TPName);
                    cmd.Parameters.AddWithValue("@PatientName", objModel.PatientName);
                    cmd.Parameters.AddWithValue("@Mobile", objModel.Mobile);
                    cmd.Parameters.AddWithValue("@ClaimFileName", objModel.ClaimFileName);
                    cmd.Parameters.AddWithValue("@fileExt", objModel.fileExt);
                    cmd.Parameters.AddWithValue("@fileData", objModel.fileData);
                    cmd.Parameters.AddWithValue("@OfficeId", objModel.OfficeId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", objModel.CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Claim_Repository/SavePALodgeClaimDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Claim_Repository/SavePALodgeClaimDetails");
            }
            return result;
        }

        public List<ViewLodgeClaim_Model> GetClaimedPolicyList(int OfficeId, long CreatedBy)
        {
            List<ViewLodgeClaim_Model> objList = new List<ViewLodgeClaim_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetClaimedPolicyList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new ViewLodgeClaim_Model
                    {
                        ClaimId = Convert.ToInt64(sdr["ClaimId"].ToString()),
                        PolicyId = Convert.ToInt64(sdr["PolicyId"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["ExpiryDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        ClaimFileName = Convert.ToString(sdr["ClaimFileName"].ToString()),
                        ClaimStatus = Convert.ToInt32(sdr["ClaimStatus"].ToString()),
                        ClaimType = Convert.ToInt32(sdr["ClaimType"].ToString()),
                        ClaimDate = Convert.ToString(sdr["ClaimDate"].ToString()),
                        ClaimStepName = Convert.ToString(sdr["ClaimStepName"].ToString()),
                        TPSurveyor = Convert.ToString(sdr["TPSurveyorName"].ToString()),
                        TPSurveyorContactNo = Convert.ToString(sdr["TPSurveyorContactNo"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/GetClaimedPolicyList");
            }
            return objList;
        }

        public string DeleteClaim(long ClaimId, long logInUser)
        {
            string result = "Error on Deleting Claim!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_DeleteClaim]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClaimId", ClaimId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", logInUser);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, logInUser, "API", "Claim_Repository/DeleteClaim");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, logInUser, "API", "Claim_Repository/DeleteClaim");
            }
            return result;
        }

        public List<LodgeClaim_Model> GetLodgeClaimDataForEdit(int OfficeId, long ClaimId, long CreatedBy)
        {
            List<LodgeClaim_Model> objList = new List<LodgeClaim_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetLodgeClaimDataForEdit";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@ClaimId", ClaimId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new LodgeClaim_Model
                    {
                        ClaimId = Convert.ToInt64(sdr["ClaimId"].ToString()),
                        PolicyId = Convert.ToInt64(sdr["PolicyId"].ToString()),
                        ClaimOfficer = Convert.ToString(sdr["ClaimOfficer"].ToString()),
                        ClaimAmount = Convert.ToString(sdr["ClaimAmount"].ToString()),
                        ClaimDate = Convert.ToString(sdr["ClaimDate"].ToString()),
                        ClaimNo = Convert.ToString(sdr["ClaimNo"].ToString()),
                        TPName = Convert.ToString(sdr["TPName"].ToString()),
                        PatientName = Convert.ToString(sdr["PatientName"].ToString()),
                        Mobile = Convert.ToString(sdr["Mobile"].ToString()),
                        ClaimFileName = Convert.ToString(sdr["ClaimFileName"].ToString()),
                        TPSurveyor = Convert.ToString(sdr["TPSurveyorName"].ToString()),
                        TPSurveyorContactNo = Convert.ToString(sdr["TPSurveyorContactNo"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/GetLodgeClaimDataForEdit");
            }
            return objList;
        }

        public string SaveClaimStepsDetails(long ClaimId, int StepId, string StepDate, long EmpId, string SettleAmount, string SettleDate, string AppRemark, string RejectRemark, string PendingRemark, int ClStatusId, long CreatedBy)
        {
            string result = "Error !";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_SaveClaimStepsDetails]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ClaimId", ClaimId);
                    cmd.Parameters.AddWithValue("@StepId", StepId);
                    cmd.Parameters.AddWithValue("@StepDate", StepDate);
                    cmd.Parameters.AddWithValue("@EmpId", EmpId);
                    cmd.Parameters.AddWithValue("@SettleAmount", SettleAmount);
                    cmd.Parameters.AddWithValue("@SettleDate", SettleDate);
                    cmd.Parameters.AddWithValue("@AppRemark", AppRemark);
                    cmd.Parameters.AddWithValue("@RejectRemark", RejectRemark);
                    cmd.Parameters.AddWithValue("@PendingRemark", PendingRemark);
                    cmd.Parameters.AddWithValue("@ClStatusId", ClStatusId);
                    cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/SaveClaimStepsDetails");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/SaveClaimStepsDetails");
            }
            return result;
        }

        public List<ClaimStepsDetails_Model> GetClaimStepsDetailList(int ClaimId, long CreatedBy)
        {
            List<ClaimStepsDetails_Model> objList = new List<ClaimStepsDetails_Model>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetClaimStepsDetailList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@ClaimId", ClaimId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new ClaimStepsDetails_Model
                    {
                        Id = Convert.ToInt64(sdr["ID"].ToString()),
                        ClaimId = Convert.ToInt64(sdr["CLId"].ToString()),
                        StepId = Convert.ToInt32(sdr["StepId"].ToString()),
                        StepDate = Convert.ToString(sdr["StepDate"].ToString()),
                        Remark = Convert.ToString(sdr["Remark"].ToString()),
                        StepName = Convert.ToString(sdr["StepName"].ToString()),
                        UserName = Convert.ToString(sdr["UserName"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Claim_Repository/GetClaimStepsDetailList");
            }
            return objList;
        }

        public List<TP_Surveyor> GetTPSurveyorForLodgeClaim(int OfficeId, long PId, long CreatedBy)
        {
            List<TP_Surveyor> objList = new List<TP_Surveyor>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetTPSurveyorForLodgeClaim";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@PId", PId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new TP_Surveyor
                    {
                        TPS_Id = Convert.ToInt32(sdr["TPId"].ToString()),
                        Ins_Comp = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        TP_Name = Convert.ToString(sdr["TPName"].ToString()),
                        TP_Address = Convert.ToString(sdr["TPAddress"].ToString()),
                        TP_ContactNo = Convert.ToString(sdr["TPMobileNo"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Master_Repository/GetTPSurveyorForLodgeClaim");
            }
            return objList;
        }
    }
}
