﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class DocumentPermission_Repository
    {

        public UserDocMapping_Model GetDocumentUserPermissionList(long Sub_CompID, long Domain_ID, long CreatedBy)
        {
            UserDocMapping_Model objRetModel = new UserDocMapping_Model();
            List<UserDocMapping> objModelList = new List<UserDocMapping>();
            UserDocMapping objModel = new UserDocMapping();
            List<User_DocClass_Mapping_Table_Model> objList = new List<User_DocClass_Mapping_Table_Model>();
            List<User_DocType_Mapping_Table_Model> objListDocType = new List<User_DocType_Mapping_Table_Model>();
            List<User_Document_Mapping_Table_Model> objListDocument = new List<User_Document_Mapping_Table_Model>();

            List<User_DocClass_Mapping_Table_Model> objListRet = new List<User_DocClass_Mapping_Table_Model>();
            List<User_DocType_Mapping_Table_Model> objListDocTypeRet = new List<User_DocType_Mapping_Table_Model>();
            List<User_Document_Mapping_Table_Model> objListDocumentRet = new List<User_Document_Mapping_Table_Model>();
            long userid = 0;
            string username = "";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentClassFromSubCompDomain";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_DocClass_Mapping_Table_Model tempobj = new User_DocClass_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    //if (userid != Convert.ToInt64(sdr["User_ID"].ToString()))
                    //{
                    //    if (userid > 0)
                    //    {
                    //        objModel.User_ID = userid;
                    //        objModel.UserName = username;
                    //        objModel.User_DocClass_Mapping_Table_Model_List = objList;
                    //        objModelList.Add(objModel);
                    //        objList = new List<User_DocClass_Mapping_Table_Model>();
                    //        objModel = new UserDocMapping();
                    //    }
                    //}
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "EMPLOYEE";
                    objList.Add(tempobj);
                }
                //objModel.User_ID = userid;
                //objModel.UserName = username;
                //objModel.User_DocClass_Mapping_Table_Model_List = objList;
                //objModelList.Add(objModel);
                //objList = new List<User_DocClass_Mapping_Table_Model>();
                //objModel = new UserDocMapping();
                sdr.Close();
                //objRetModel.UserDocMapping_List = objModelList;


                //objModelList = new List<UserDocMapping>();
                userid = 0;
                username = "";
                sqlstr = "CB_SP_SelectDocumentClassFromSubCompDomain";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "VENDOR");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_DocClass_Mapping_Table_Model tempobj = new User_DocClass_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    //if (userid != Convert.ToInt64(sdr["User_ID"].ToString()))
                    //{
                    //    if (userid > 0)
                    //    {
                    //        objModel.User_ID = userid;
                    //        objModel.UserName = username;
                    //        objModel.User_DocClass_Mapping_Table_Model_List = objList;
                    //        objModelList.Add(objModel);
                    //        objList = new List<User_DocClass_Mapping_Table_Model>();
                    //        objModel = new UserDocMapping();
                    //    }
                    //}
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "VENDOR";
                    objList.Add(tempobj);
                }
                //objModel.User_ID = userid;
                //objModel.UserName = username;
                //objModel.User_DocClass_Mapping_Table_Model_List = objList;
                //objModelList.Add(objModel);
                //objList = new List<User_DocClass_Mapping_Table_Model>();
                //objModel = new UserDocMapping();
                sdr.Close();











                userid = 0;
                username = "";
                sqlstr = "CB_SP_SelectDocumentTypeFromSubCompDomain";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_DocType_Mapping_Table_Model tempobj = new User_DocType_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DCTypeName = sdr["DocumentTypeName"].ToString();
                    tempobj.Document_Type = Convert.ToInt64(sdr["Document_Type"].ToString());
                    //if (userid != Convert.ToInt64(sdr["User_ID"].ToString()))
                    //{
                    //    if (userid > 0)
                    //    {
                    //        objModel.User_ID = userid;
                    //        objModel.UserName = username;
                    //        objModel.User_DocClass_Mapping_Table_Model_List = objList;
                    //        objModelList.Add(objModel);
                    //        objList = new List<User_DocClass_Mapping_Table_Model>();
                    //        objModel = new UserDocMapping();
                    //    }
                    //}
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "EMPLOYEE";
                    objListDocType.Add(tempobj);
                }
                //objModel.User_ID = userid;
                //objModel.UserName = username;
                //objModel.User_DocClass_Mapping_Table_Model_List = objList;
                //objModelList.Add(objModel);
                //objList = new List<User_DocClass_Mapping_Table_Model>();
                //objModel = new UserDocMapping();
                sdr.Close();

                userid = 0;
                username = "";
                sqlstr = "CB_SP_SelectDocumentTypeFromSubCompDomain";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "VENDOR");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_DocType_Mapping_Table_Model tempobj = new User_DocType_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DCTypeName = sdr["DocumentTypeName"].ToString();
                    tempobj.Document_Type = Convert.ToInt64(sdr["Document_Type"].ToString());
                    //if (userid != Convert.ToInt64(sdr["User_ID"].ToString()))
                    //{
                    //    if (userid > 0)
                    //    {
                    //        objModel.User_ID = userid;
                    //        objModel.UserName = username;
                    //        objModel.User_DocClass_Mapping_Table_Model_List = objList;
                    //        objModelList.Add(objModel);
                    //        objList = new List<User_DocClass_Mapping_Table_Model>();
                    //        objModel = new UserDocMapping();
                    //    }
                    //}
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "VENDOR";
                    objListDocType.Add(tempobj);
                }
                //objModel.User_ID = userid;
                //objModel.UserName = username;
                //objModel.User_DocClass_Mapping_Table_Model_List = objList;
                //objModelList.Add(objModel);
                //objList = new List<User_DocClass_Mapping_Table_Model>();
                //objModel = new UserDocMapping();
                sdr.Close();












                userid = 0;
                username = "";
                sqlstr = "CB_SP_SelectDocumentFromSubCompDomain";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "EMPLOYEE");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_Document_Mapping_Table_Model tempobj = new User_Document_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DCTypeName = sdr["DocumentTypeName"].ToString();
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.Document_Type = Convert.ToInt64(sdr["Document_Type"].ToString());                    
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "EMPLOYEE";
                    objListDocument.Add(tempobj);
                }
                
                sdr.Close();

                userid = 0;
                username = "";
                sqlstr = "CB_SP_SelectDocumentFromSubCompDomain";
                cmd.CommandText = sqlstr;
                cmd.Parameters.Clear();
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Sub_CompID", Sub_CompID);
                cmd.Parameters.AddWithValue("@Domain_ID", Domain_ID);
                cmd.Parameters.AddWithValue("@UserType", "VENDOR");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_Document_Mapping_Table_Model tempobj = new User_Document_Mapping_Table_Model();

                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.DCClassName = sdr["DocumentClassName"].ToString();
                    tempobj.DCTypeName = sdr["DocumentTypeName"].ToString();
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.Document_Type = Convert.ToInt64(sdr["Document_Type"].ToString());                    
                    userid = Convert.ToInt64(sdr["User_ID"].ToString());
                    username = sdr["UserName"].ToString();
                    tempobj.UserName = username;
                    tempobj.User_ID = userid;
                    tempobj.UserType = "VENDOR";
                    objListDocument.Add(tempobj);
                }
                sdr.Close();



















                // Mapping Main
                userid = 0;
                objListRet = new List<User_DocClass_Mapping_Table_Model>();
                for (int i = 0; i < objList.Count(); i++)
                {
                    if (objList[i].UserType == "EMPLOYEE")
                    {
                        objModel = new UserDocMapping();
                        objListRet = new List<User_DocClass_Mapping_Table_Model>();
                        objModel.UserName = objList[i].UserName;
                        objModel.User_ID = objList[i].User_ID;
                        userid = objList[i].User_ID;

                        foreach (var data in objList)
                        {
                            bool mexists = false;
                            //if (objModel.User_DocClass_Mapping_Table_Model_List != null)
                            //{
                            //    mexists = objModel.User_DocClass_Mapping_Table_Model_List.Exists(element => element.SLNO == data.SLNO);
                            //}
                            if (objModelList != null)
                            {
                                foreach (var testdata in objModelList)
                                {
                                    mexists = testdata.User_DocClass_Mapping_Table_Model_List.Exists(element => element.SLNO == data.SLNO);
                                }
                                //mexists = objModelList.User_DocClass_Mapping_Table_Model_List.Exists(element => element.SLNO == data.SLNO);
                            }
                            if (mexists != true)
                            {
                                bool exists = objListRet.Exists(element => element.SLNO == data.SLNO);
                                if (exists != true)
                                {
                                    if (userid == data.User_ID)
                                    {
                                        User_DocClass_Mapping_Table_Model tempobj = new User_DocClass_Mapping_Table_Model();
                                        tempobj.SLNO = data.SLNO;
                                        tempobj.Sub_CompID = data.Sub_CompID;
                                        tempobj.Domain_ID = data.Domain_ID;
                                        tempobj.Document_ClassID = data.Document_ClassID;
                                        tempobj.User_ID = data.User_ID;
                                        tempobj.CreatedBy = data.CreatedBy;
                                        tempobj.CreatedOn = data.CreatedOn;
                                        tempobj.ModifiedBy = data.ModifiedBy;
                                        tempobj.ModifiedOn = data.ModifiedOn;
                                        tempobj.IsDeleted = data.IsDeleted;
                                        tempobj.DeletedBy = data.DeletedBy;
                                        tempobj.DeletedOn = data.DeletedOn;
                                        tempobj.view_right = data.view_right;
                                        tempobj.print_right = data.print_right;
                                        tempobj.email_right = data.email_right;
                                        tempobj.checkin_right = data.checkin_right;
                                        tempobj.checkout_right = data.checkout_right;
                                        tempobj.download_right = data.download_right;
                                        tempobj.upload_right = data.upload_right;
                                        tempobj.selectall_right = data.selectall_right;
                                        tempobj.DCClassName = data.DCClassName;
                                        tempobj.UserName = data.UserName;
                                        tempobj.User_ID = data.User_ID;
                                        tempobj.UserType = data.UserType;
                                        objListRet.Add(tempobj);
                                    }
                                }
                            }

                        }
                        objModel.User_DocClass_Mapping_Table_Model_List = objListRet;
                    }
                    if (objList[i].UserType == "EMPLOYEE")
                    {
                        objListDocTypeRet = new List<User_DocType_Mapping_Table_Model>();
                        foreach (var data in objListDocType)
                        {
                            bool exists = objListDocTypeRet.Exists(element => element.SLNO == data.SLNO);
                            if (exists != true)
                            {
                                if (userid == data.User_ID)
                                {
                                    User_DocType_Mapping_Table_Model tempobj = new User_DocType_Mapping_Table_Model();
                                    tempobj.SLNO = data.SLNO;
                                    tempobj.Sub_CompID = data.Sub_CompID;
                                    tempobj.Domain_ID = data.Domain_ID;
                                    tempobj.Document_ClassID = data.Document_ClassID;
                                    tempobj.User_ID = data.User_ID;
                                    tempobj.CreatedBy = data.CreatedBy;
                                    tempobj.CreatedOn = data.CreatedOn;
                                    tempobj.ModifiedBy = data.ModifiedBy;
                                    tempobj.ModifiedOn = data.ModifiedOn;
                                    tempobj.IsDeleted = data.IsDeleted;
                                    tempobj.DeletedBy = data.DeletedBy;
                                    tempobj.DeletedOn = data.DeletedOn;
                                    tempobj.view_right = data.view_right;
                                    tempobj.print_right = data.print_right;
                                    tempobj.email_right = data.email_right;
                                    tempobj.checkin_right = data.checkin_right;
                                    tempobj.checkout_right = data.checkout_right;
                                    tempobj.download_right = data.download_right;
                                    tempobj.upload_right = data.upload_right;
                                    tempobj.selectall_right = data.selectall_right;
                                    tempobj.DCClassName = data.DCClassName;
                                    tempobj.DCTypeName = data.DCTypeName;
                                    tempobj.UserName = data.UserName;
                                    tempobj.User_ID = data.User_ID;
                                    tempobj.UserType = data.UserType;
                                    tempobj.Document_Type = data.Document_Type;
                                    objListDocTypeRet.Add(tempobj);
                                    userid = tempobj.User_ID;
                                }
                            }
                        }
                        objModel.User_DocType_Mapping_Table_Model_List = objListDocTypeRet;
                        
                    }
                    if (objList[i].UserType == "EMPLOYEE")
                    {
                        objListDocumentRet = new List<User_Document_Mapping_Table_Model>();
                        foreach (var data in objListDocument)
                        {
                            bool exists = objListDocumentRet.Exists(element => element.SLNO == data.SLNO);
                            if (exists != true)
                            {
                                if (userid == data.User_ID)
                                {
                                    User_Document_Mapping_Table_Model tempobj = new User_Document_Mapping_Table_Model();
                                    tempobj.SLNO = data.SLNO;
                                    tempobj.Sub_CompID = data.Sub_CompID;
                                    tempobj.Domain_ID = data.Domain_ID;
                                    tempobj.Document_ClassID = data.Document_ClassID;
                                    tempobj.User_ID = data.User_ID;
                                    tempobj.CreatedBy = data.CreatedBy;
                                    tempobj.CreatedOn = data.CreatedOn;
                                    tempobj.ModifiedBy = data.ModifiedBy;
                                    tempobj.ModifiedOn = data.ModifiedOn;
                                    tempobj.IsDeleted = data.IsDeleted;
                                    tempobj.DeletedBy = data.DeletedBy;
                                    tempobj.DeletedOn = data.DeletedOn;
                                    tempobj.view_right = data.view_right;
                                    tempobj.print_right = data.print_right;
                                    tempobj.email_right = data.email_right;
                                    tempobj.checkin_right = data.checkin_right;
                                    tempobj.checkout_right = data.checkout_right;
                                    tempobj.download_right = data.download_right;
                                    tempobj.upload_right = data.upload_right;
                                    tempobj.selectall_right = data.selectall_right;
                                    tempobj.DCClassName = data.DCClassName;
                                    tempobj.DCTypeName = data.DCTypeName;
                                    tempobj.UserName = data.UserName;
                                    tempobj.User_ID = data.User_ID;
                                    tempobj.UserType = data.UserType;
                                    tempobj.Document_Type = data.Document_Type;
                                    tempobj.FileName = data.FileName;
                                    objListDocumentRet.Add(tempobj);
                                    userid = tempobj.User_ID;
                                }
                            }
                        }
                        objModel.User_Document_Mapping_Table_Model_List = objListDocumentRet;

                    }



                    if (objModel.User_DocClass_Mapping_Table_Model_List != null)
                    {
                        if (objModel.User_DocClass_Mapping_Table_Model_List.Count() > 0)
                        {
                            objModelList.Add(objModel);
                        }
                    }
                    else if (objModel.User_DocType_Mapping_Table_Model_List != null)
                    {
                        if (objModel.User_DocType_Mapping_Table_Model_List.Count() > 0)
                        {
                            objModelList.Add(objModel);
                        }
                    }
                    else if (objModel.User_Document_Mapping_Table_Model_List != null)
                    {
                        if (objModel.User_Document_Mapping_Table_Model_List.Count() > 0)
                        {
                            objModelList.Add(objModel);
                        }
                    }
                }
                objRetModel.UserDocMapping_List = objModelList;

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DocumentPermission_Repository/GetDocumentUserPermissionList");
            }

            return objRetModel;

        }



        public DBReturnModel InsertDocumentUserPermission(UserDocMapping_Model objModel)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Giving Permission For User Document!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDocumentUserPermission";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@Domain_ID", objModel.Domain_ID);

                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.User_DocClass_Mapping_Type";
                    dataTable.Columns.Add("Document_ClassID", typeof(long));
                    dataTable.Columns.Add("User_ID", typeof(long));
                    dataTable.Columns.Add("view_right", typeof(bool));
                    dataTable.Columns.Add("print_right", typeof(bool));
                    dataTable.Columns.Add("email_right", typeof(bool));
                    dataTable.Columns.Add("checkin_right", typeof(bool));
                    dataTable.Columns.Add("checkout_right", typeof(bool));
                    dataTable.Columns.Add("download_right", typeof(bool));
                    dataTable.Columns.Add("upload_right", typeof(bool));
                    dataTable.Columns.Add("selectall_right", typeof(bool));
                    foreach (var dta in objModel.UserDocMapping_List)
                    {
                        if (dta.User_DocClass_Mapping_Table_Model_List != null)
                        {
                            foreach (var ndta in dta.User_DocClass_Mapping_Table_Model_List)
                            {
                                dataTable.Rows.Add(ndta.Document_ClassID, dta.User_ID, ndta.view_right, ndta.print_right,
                                    ndta.email_right, ndta.checkin_right, ndta.checkout_right, ndta.download_right, ndta.upload_right,
                                    ndta.selectall_right); // Id of '1' is valid for the Person table
                            }
                        }
                    }
                    SqlParameter parameter = new SqlParameter("UserDocClassMapping", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);






                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.User_DocType_Mapping_Type";
                    dataTable1.Columns.Add("Document_Type", typeof(long));
                    dataTable1.Columns.Add("User_ID", typeof(long));
                    dataTable1.Columns.Add("view_right", typeof(bool));
                    dataTable1.Columns.Add("print_right", typeof(bool));
                    dataTable1.Columns.Add("email_right", typeof(bool));
                    dataTable1.Columns.Add("checkin_right", typeof(bool));
                    dataTable1.Columns.Add("checkout_right", typeof(bool));
                    dataTable1.Columns.Add("download_right", typeof(bool));
                    dataTable1.Columns.Add("upload_right", typeof(bool));
                    dataTable1.Columns.Add("selectall_right", typeof(bool));
                    foreach (var dta in objModel.UserDocMapping_List)
                    {
                        if (dta.User_DocType_Mapping_Table_Model_List != null)
                        {
                            foreach (var ndta in dta.User_DocType_Mapping_Table_Model_List)
                            {
                                dataTable1.Rows.Add(ndta.Document_Type, dta.User_ID, ndta.view_right, ndta.print_right,
                                    ndta.email_right, ndta.checkin_right, ndta.checkout_right, ndta.download_right, ndta.upload_right,
                                    ndta.selectall_right); // Id of '1' is valid for the Person table
                            }
                        }
                    }
                    SqlParameter parameter1 = new SqlParameter("UserDocTypeMapping", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);



                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentPermission_Repository/InsertDocumentUserPermission");
                    objReturn.ReturnMessage = "System Error on Inserting Document Permission!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "DocumentPermission_Repository/InsertDocumentUserPermission");
            }

            return objReturn;
        }


    }
}
