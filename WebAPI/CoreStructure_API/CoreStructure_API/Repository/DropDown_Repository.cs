﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Model.Entities;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class DropDown_Repository
    {
        public List<SelectListItem> GetParentCompanyList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "select SLNO, Company_Name from Parent_Company_Table";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Company_Name"].ToString(),
                        Value = sdr["SLNO"].ToString()
                    });
                }

                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetParentCompanyList");
            }

            return objList;
        }
        public List<SelectListItem> GetThemeList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                objList.Add(new SelectListItem
                {
                    Text = "Theme 1",
                    Value = "~/Views/Shared/_Layout1.cshtml"
                });
                objList.Add(new SelectListItem
                {
                    Text = "Theme 2",
                    Value = "~/Views/Shared/_Layout2.cshtml"
                });
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetThemeList");
            }

            return objList;
        }
        public List<SelectListItem> GetStateList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetStateList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.AddWithValue("@UserName", model.UserName);
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["StateName"].ToString(),
                        Value = sdr["StateId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetStateList");
            }
            return objList;
        }
        public List<SelectListItem> GetOfficeList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetOfficelist";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.AddWithValue("@UserName", model.UserName);
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["OfficeName"].ToString(),
                        Value = sdr["OfficeID"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetOfficeList");
            }
            return objList;
        }
        public List<SelectListItem> GetOfficeListActive(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetOfficelistActive";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.AddWithValue("@UserName", model.UserName);
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["OfficeName"].ToString(),
                        Value = sdr["OfficeID"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetOfficeListActive");
            }
            return objList;
        }
        public List<SelectListItem> GetRoleList(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetRoleList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                //cmd.Parameters.AddWithValue("@UserName", model.UserName);
                //cmd.Parameters.AddWithValue("@Password", model.Password);
                //cmd.Parameters.AddWithValue("@IpAddress", model.IPAddress);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["RoleName"].ToString(),
                        Value = sdr["RoleId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetRoleList");
            }
            return objList;
        }
        public List<SelectListItem> GetCityListByStateId(long CreatedBy, int StateId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetCityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@StateId", StateId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["CityName"].ToString(),
                        Value = sdr["CityId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetCityListByStateId");
            }
            return objList;
        }
        public List<SelectListItem> GetCategoryList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetCategoryList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["CategoryName"].ToString(),
                        Value = sdr["CategoryId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetCategoryList");
            }
            return objList;
        }
        public List<SelectListItem> GetInsuranceCompanyList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "GetInsuranceCompanyList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["CompanyName"].ToString(),
                        Value = sdr["CompanyId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetInsuranceCompanyList");
            }
            return objList;
        }
        public List<SelectListItem> GetContactPersonList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetContactPersonListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Name"].ToString(),
                        Value = sdr["CPId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetContactPersonList");
            }
            return objList;
        }
        public List<SelectListItem> GetPolicyTypeList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetPolicyTypeListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["TypeName"].ToString(),
                        Value = sdr["TypeId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetPolicyTypeList");
            }
            return objList;
        }      
        public List<SelectListItem> GetIssuingOfficeList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetIssuingOfficeListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["IssuingOfficeName"].ToString(),
                        Value = sdr["IssuingOfficeId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetIssuingOfficeList");
            }
            return objList;
        }    
        public List<SelectListItem> GetGroupList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetGroupListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["GroupName"].ToString(),
                        Value = sdr["GroupId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetGroupList");
            }
            return objList;
        }     
        public List<SelectListItem> GetProductList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetProductListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["ProductName"].ToString(),
                        Value = sdr["ProductId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetProductList");
            }
            return objList;
        }
        public List<SelectListItem> GetClientList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetClientListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["ClientName"].ToString(),
                        Value = sdr["CId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetClientList");
            }
            return objList;
        }
        public List<SelectListItem> GetIssuingOfcListbyInsCoId(long CreatedBy, int insuranceCompanyId, int officeid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetIssuingOfcList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@insuranceCompanyId", insuranceCompanyId);
                cmd.Parameters.AddWithValue("@officeid", officeid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["IssuingOfficeName"].ToString(),
                        Value = sdr["IssuingOfficeId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetIssuingOfcListbyInsCoId");
            }
            return objList;
        }
        public List<SelectListItem> GetCategorylistListbyInsCoId(long CreatedBy, int insuranceCompanyId, int officeid, int Catid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetCategorylistListbyInsCoId";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@insuranceCompanyId", insuranceCompanyId);
                cmd.Parameters.AddWithValue("@officeid", officeid);
                cmd.Parameters.AddWithValue("@Catid", Catid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["ProductName"].ToString(),
                        Value = sdr["ProductId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetIssuingOfcListbyInsCoId");
            }
            return objList;
        }
        public List<SelectListItem> GetIssuingofficeByInsco(long CreatedBy, int insuranceCompanyId, int officeid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetIssuingOfficeListDropDownBYInsco";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@insuranceCompanyId", insuranceCompanyId);
                cmd.Parameters.AddWithValue("@officeid", officeid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["IssuingOfficeName"].ToString(),
                        Value = sdr["IssuingOfficeId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetIssuingOfcListbyInsCoId");
            }
            return objList;
        }
        public List<SelectListItem> GetOfficeListForReport(long CreatedBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetOfficeListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["OfficeName"].ToString(),
                        Value = sdr["OfficeID"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetOfficeListForReport");
            }
            return objList;
        }
        public object GetContactPersonListWithMobile(long createdBy, int officeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetContactPersonListDropDownWithMobile";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", officeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["ContactPersonName"].ToString(),
                        Value = sdr["ContactPersonId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "DropDown_Repository/GetContactPersonList");
            }
            return objList;
        }
        public List<SelectListItem> GetUserListByRoleId(long CreatedBy, int RoleId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetUserListByRoleId";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@RoleId", RoleId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Name"].ToString(),
                        Value = sdr["UId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetUserListByRoleId");
            }
            return objList;
        }
        public List<SelectListItem> GetTotalPolicyList(long createdBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetTotalCountpolicy";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["totalgen"].ToString(),
                        Value = sdr["totalun"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "DropDown_Repository/GetTotalPolicyList");
            }
            return objList;
        }
        public List<SelectListItem> GetTotalRenewalPolicy(long createdBy)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetTotalRenewalPolicy";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["totalren"].ToString(),
                        Value = sdr["Processed"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "DropDown_Repository/GetTotalRenewalPolicy");
            }
            return objList;
        }
        public object GetPolicyNoList(long createdBy, int officeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetPolicyNoList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", officeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["PolicyNo"].ToString(),
                        Value = sdr["PolicyNo"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "DropDown_Repository/GetContactPersonList");
            }
            return objList;
        }
        public object GetPolicyholderList(long createdBy, int officeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetPolicyholderList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", officeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["PHName"].ToString(),
                        Value = sdr["Pid"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, createdBy, "API", "DropDown_Repository/GetPolicyholderList");
            }
            return objList;
        }
        public List<SelectListItem> GetClaimStepList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetClaimStepListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["StepName"].ToString(),
                        Value = sdr["StepId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetClaimStepList");
            }
            return objList;
        }
        public List<SelectListItem> GetEmployeeList(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetEmployeeListDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["Name"].ToString(),
                        Value = sdr["UId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetEmployeeList");
            }
            return objList;
        }
        public List<SelectListItem> GetInsuranceCompanyListForTPSurveyor(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetInsuranceCompanyListForTPSurveyor";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["CompanyName"].ToString(),
                        Value = sdr["CompanyId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetInsuranceCompanyListForTPSurveyor");
            }
            return objList;
        }
        public List<SelectListItem> GetActivityListForDropDown(long CreatedBy, int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetActivityListForDropDown";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["ActivityName"].ToString(),
                        Value = sdr["ActivityId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetActivityListForDropDown");
            }
            return objList;
        }
        public List<SelectListItem> GetSubActivityListByActivityId(long CreatedBy, int ActivityId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetSubActivityListByActivity";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@ActivityId", ActivityId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Text = sdr["SubActivityName"].ToString(),
                        Value = sdr["SubActivityId"].ToString()
                    });
                }
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "DropDown_Repository/GetSubActivityListByActivityId");
            }
            return objList;
        }

    }
}
