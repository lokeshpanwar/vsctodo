﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;

namespace CoreStructure_API.Repository
{
    public class Report_Repository
    {
        public List<PolicyReport> ViewPolicyReport(int officeId, int typeId, string fromdate, string todate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId, long createdBy)
        {
            List<PolicyReport> objList = new List<PolicyReport>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_ViewPolicyReport";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@officeId", officeId);
                cmd.Parameters.AddWithValue("@typeId", typeId);
                cmd.Parameters.AddWithValue("@fromdate", fromdate);
                cmd.Parameters.AddWithValue("@ToDate", todate);
                cmd.Parameters.AddWithValue("@contactPersonId", contactPersonId);
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.Parameters.AddWithValue("@insuranceCompanyId", insuranceCompanyId);
                cmd.Parameters.AddWithValue("@issuingOfficeId", issuingOfficeId);
                cmd.Parameters.AddWithValue("@productId", productId);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@policyStatusId", policyStatusId); 
                cmd.Parameters.AddWithValue("@LoggedInUser", createdBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new PolicyReport
                    {
                        PolicyHolderName = Convert.ToString(sdr["Policyholdername"].ToString()),
                        MobileNo = sdr["MobileNo"].ToString(),
                        ContactPersonName = sdr["ContactPersonName"].ToString(),
                        ProductName = sdr["ProductName"].ToString(),
                        InsuranceCompanyName = Convert.ToString(sdr["InsCo"]),
                        PolicyNo = sdr["PolicyNo"].ToString(),
                        ExpiryDate = sdr["ExpiryDate1"].ToString(),
                        Premium = Convert.ToString(sdr["PremiumPayable"]),
                        GST = Convert.ToDecimal(sdr["GST"]),
                        PolicyStatusName = sdr["PolicyStatusName"].ToString()
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                //ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetQueryList");
            }
            return objList;
        }



        public DailyBusiness AdminDailyBusiness(int officeId, string fromDate, string toDate)
        {
            DailyBusiness objdata = new DailyBusiness();
            List<DailyBusiness> objList = new List<DailyBusiness>();
            List<DailyBusiness> ViewPolicy = new List<DailyBusiness>();
            List<DailyBusiness> ViewPolicy1 = new List<DailyBusiness>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetDailyBusinessReprort";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Dateform", fromDate);
                cmd.Parameters.AddWithValue("@DateTo", toDate);
                cmd.Parameters.AddWithValue("@office", officeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.ViewPolicy = (from DataRow dr in ds.Tables[0].Rows
                                          select new DailyBusiness
                                          {
                                              InsCo = dr["CompanyShortName"].ToString(),
                                              PremiumPayable = dr["PremiumPayable1"].ToString(),
                                              Percentage=dr["Percentt"].ToString()

                                          }).ToList();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    objdata.ViewPolicy1 = (from DataRow dr in ds.Tables[1].Rows
                                           select new DailyBusiness
                                           {
                                               Category = dr["CategoryName"].ToString(),
                                               PremiumPayable = dr["PremiumPayable1"].ToString(),
                                               Percentage = dr["Percentt"].ToString()
                                           }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }



        public DailyBusiness ViewAdminDailyBusinessClientWise(string fromdate, string toDate, int clientId, string orderBy, int OfficeId)
        {
            DailyBusiness objdata = new DailyBusiness();
            List<DailyBusiness> objList = new List<DailyBusiness>();
            List<DailyBusiness> ViewPolicy = new List<DailyBusiness>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetDailyBusinessReprortClientWise";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Dateform", fromdate);
                cmd.Parameters.AddWithValue("@DateTo", toDate);
                cmd.Parameters.AddWithValue("@ContactPerson", clientId);
                cmd.Parameters.AddWithValue("@Sortype", orderBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objdata.ViewPolicy = (from DataRow dr in ds.Tables[0].Rows
                                          select new DailyBusiness
                                          {
                                              ClientName = dr["ContactPersonName"].ToString(),
                                              PremiumPayable = dr["PremiumPayable"].ToString()
                                          }).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public DailyBusiness AdminDailyBusinessDayWise(int officeId, string fromdate, string toDate)
        {
            string header = "";
            String Query1 = string.Empty;
            String Query2 = string.Empty;
            DailyBusiness objdata = new DailyBusiness();
            List<DailyBusiness> objList = new List<DailyBusiness>();
            List<DailyBusiness> ViewPolicy = new List<DailyBusiness>();
            List<DailyBusiness> ViewPolicy1 = new List<DailyBusiness>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetbusinessReport";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@DateFrom", fromdate);
                cmd.Parameters.AddWithValue("@DateTo", toDate);
                cmd.Parameters.AddWithValue("@OfficeId", officeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                string QueryAct = string.Empty;
               string[] main = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,".ToString().Split(',');
              //  string[] Alp = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ,BA,BB,BC,BD,BE,BF,BG,BH,BI,BJ,BK,BL,BM,BN,BO,BP,BQ,BR,BS,BT,BU,BV,BW,BX,BY,BZ,CA,CB,CC,CD,CE,CF,CG,CH,CI,CJ,CK,CL,CM,CN,CO,CP,CQ,CR,CS,CT,CU,CV,CW,CX,CY,CZ,DA,DB,DC,DD,DE,DF,DG,DH,DI,DJ,DK,DL,DM,DN,DO,DP,DQ,DR,DS,DT,DU,DV,DW,DX,DY,DZ,EA,EB,EC,ED,EE,EF,EG,EH,EI,EJ,EK,EL,EM,EN,EO,EP,EQ,ER,ES,ET,EU,EV,EW,EX,EY,EZ,FA,FB,FC,FD,FE,FF,FG,FH,FI,FJ,FK,FL,FM,FN,FO,FP,FQ,FR,FS,FT,FU,FV,FW,FX,FY,FZ,GA,GB,GC,GD,GE,GF,GG,GH,GI,GJ,GK,GL,GM,GN,GO,GP,GQ,GR,GS,GT,GU,GV,GW,GX,GY,GZ,HA,HB,HC,HD,HE,HF,HG,HH,HI,HJ,HK,HL,HM,HN,HO,HP,HQ,HR,HS,HT,HU,HV,HW,HX,HY,HZ,IA,IB,IC,ID,IE,IF,IG,IH,II,IJ,IK,IL,IM,IN,IO,IP,IQ,IR,IS,IT,IU,IV,IW,IX,IY,IZ".ToString().Split('-');
              //  string[] Alps = "I,II,III,IV,V,VI,VII,VIII,IX,X,XI,XII,XIII,XIV,XV,XVI,XVII,XVIII,XIX,XX".ToString().Split('-');
                int x = 0;
                int z = 0;
                int m = 1;
                if (ds.Tables[1].Rows.Count > 0)
                {
                    header = "<table class='table table-striped table-bordered table-hover table-checkable order-column' id='sample_1' style='width:100px'><thead><tr><td>Sr. No.</td><td>Date</td><td>Total Premium</td><td class='InsCo' style='display:none'>Insurance Company</td><td class='InsCo' style='display:none'>Insurance Premium</td><td class='Category cat1' style='display:none'>Category</td><td class='Category cat1' style='display:none'>Category Premium</td></tr></thead><tbody>";
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        if (ds.Tables[1].Rows[i][1].ToString().Contains("<b>"))
                        {
                            Query1 = Query1 + "<tr class='success'>";
                            for (int j = 0; j < ds.Tables[1].Columns.Count; j++)
                            {
                                x = 1;
                                z = 0;
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td>" + main[m - 1].ToString() + "</td>";
                                    m = m + 1;
                                }
                                else
                                {
                                    if (j == 3 || j == 4)
                                    {
                                        Query1 = Query1 + "<td class='InsCo cat1' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else if (j == 5 || j == 6)
                                    {
                                        Query1 = Query1 + "<td class='Category cat1' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else
                                    {
                                        Query1 = Query1 + "<td>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                }
                            }
                            Query1 = Query1 + "</tr>";
                        }
                        else if (ds.Tables[1].Rows[i][1].ToString().Contains("<InsComp>"))
                        {
                            Query1 = Query1 + "<tr class='InsCo' style='display:none'>";
                            for (int j = 0; j < ds.Tables[1].Columns.Count; j++)
                            {
                                x = 1;
                                z = 0;
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td></td>";
                                }
                                else
                                {
                                    if (j == 3 || j == 4)
                                    {
                                        Query1 = Query1 + "<td class='InsCo' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else if (j == 5 || j == 6)
                                    {
                                        Query1 = Query1 + "<td class='Category' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else
                                    {
                                        Query1 = Query1 + "<td>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                }
                            }
                            Query1 = Query1 + "</tr>";
                        }
                        else if (ds.Tables[1].Rows[i][1].ToString().Contains("<Cat>"))
                        {
                            Query1 = Query1 + "<tr class='Category' style='display:none'>";
                            for (int j = 0; j < ds.Tables[1].Columns.Count; j++)
                            {
                                x = 1;
                                z = 0;
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td></td>";
                                }
                                else
                                {
                                    Query1 = Query1 + "<td>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                }
                            }
                            Query1 = Query1 + "</tr>";
                        }
                        else
                        {
                            Query1 = Query1 + "<tr class='Catwise' style='display:none'>";
                            z = 0;
                            for (int j = 0; j < ds.Tables[1].Columns.Count; j++)
                            {
                                if (j == 0)
                                {
                                    Query1 = Query1 + "<td></td>";
                                }
                                else
                                {
                                    if (j == 3 || j == 4)
                                    {
                                        Query1 = Query1 + "<td class='Catwise' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else if (j == 5 || j == 6)
                                    {
                                        Query1 = Query1 + "<td class='Category' style='display:none'>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                    else
                                    {
                                        Query1 = Query1 + "<td>" + ds.Tables[1].Rows[i][j].ToString() + "</td>";
                                    }
                                }
                            }
                            Query1 = Query1 + "</tr>";
                        }
                    }
                    Query1 = Query1 + "</tbody></table>";

                    Query1 = header + Query1;
                    //DayWiseData listdata = new DayWiseData();
                    //listdata.QueryData = Query1;
                    //listdata.TotalAmt = ds.Tables[0].Rows[0]["TotalAmount"].ToString();
                    objdata.QueryData = Query1;
                    objdata.TotalAmt = ds.Tables[0].Rows[0]["TotalAmount"].ToString();
                }
            }
            catch (Exception ex)
            {

            }
            return objdata;
        }

        public List<UpcomingPolicy> ViewUpcomingPolicy(int officeId, string fromdate, string toDate)
        {
            UpcomingPolicy objdata = new UpcomingPolicy();
            List<UpcomingPolicy> objList = new List<UpcomingPolicy>();
            List<UpcomingPolicy> ViewUpcomingPolicy = new List<UpcomingPolicy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();
                string sqlstr = "Sp_GetUpcomingPolicyReprort";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Dateform", fromdate);
                cmd.Parameters.AddWithValue("@DateTo", toDate);
                cmd.Parameters.AddWithValue("@office", officeId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objList = (from DataRow dr in ds.Tables[0].Rows
                               select new UpcomingPolicy
                               {
                                   PolicyNo = dr["PolicyNo"].ToString(),
                                   ExpiryDate = dr["E_Date"].ToString(),
                                   Policyholdername = dr["PHName"].ToString(),
                                   ContactPersonName = dr["CPName"].ToString(),
                                   InsCompany = dr["InsCo"].ToString(),
                                   IssuingOfc = dr["IssuingOfc"].ToString(),
                                   PremiumPayable = Convert.ToDecimal(dr["Premium"]),
                                   CategoryName = dr["Category"].ToString(),
                                   ProductName = dr["ProductName"].ToString(),
                                   SubjectInsured = dr["SubjectInsured"].ToString(),
                                   CostCenterName = dr["CostCenter"].ToString(),
                                   VehicleNo=dr["VehicleNo"].ToString(),
                                   MobileNo=dr["MobileNo"].ToString()

                               }).ToList();
                }
                //if (ds.Tables[1].Rows.Count > 0)
                //{
                //    objdata.ViewPolicy1 = (from DataRow dr in ds.Tables[1].Rows
                //                           select new DailyBusiness
                //                           {
                //                               Category = dr["CategoryName"].ToString(),
                //                               PremiumPayable = dr["PremiumPayable1"].ToString()
                //                           }).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return objList;
        }

        public List<PolicyReport> AdminViewPolicyReport(int officeId, int typeId, string fromDate, string toDate, int contactPersonId, int categoryId, int insuranceCompanyId, int issuingOfficeId, int productId, int groupId, int policyStatusId, long createdBy)
        {
            List<PolicyReport> objList = new List<PolicyReport>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_ViewPolicyReport";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@officeId", officeId);
                cmd.Parameters.AddWithValue("@typeId", typeId);
                cmd.Parameters.AddWithValue("@fromdate", fromDate);
                cmd.Parameters.AddWithValue("@ToDate", toDate);
                cmd.Parameters.AddWithValue("@contactPersonId", contactPersonId);
                cmd.Parameters.AddWithValue("@categoryId", categoryId);
                cmd.Parameters.AddWithValue("@insuranceCompanyId", insuranceCompanyId);
                cmd.Parameters.AddWithValue("@issuingOfficeId", issuingOfficeId);
                cmd.Parameters.AddWithValue("@productId", productId);
                cmd.Parameters.AddWithValue("@groupId", groupId);
                cmd.Parameters.AddWithValue("@policyStatusId", policyStatusId);
                cmd.Parameters.AddWithValue("@LoggedInUser", createdBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.Add(new PolicyReport
                    {
                        PolicyHolderName = Convert.ToString(sdr["Policyholdername"].ToString()),
                        MobileNo = sdr["MobileNo"].ToString(),
                        ContactPersonName = sdr["ContactPersonName"].ToString(),
                        ProductName = sdr["ProductName"].ToString(),
                        InsuranceCompanyName = Convert.ToString(sdr["InsCo"]),
                        PolicyNo = sdr["PolicyNo"].ToString(),
                        ExpiryDate = sdr["ExpiryDate1"].ToString(),
                        Premium = Convert.ToString(sdr["PremiumPayable"]),
                        GST = Convert.ToDecimal(sdr["GST"]),
                        PolicyStatusName = sdr["PolicyStatusName"].ToString()
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                //ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetQueryList");
            }
            return objList;
        }


        public List<ClaimReport> ViewClaimReport(int officeId, string policyNo, string contactPersonId, string policyholderId)
        {
            List<ClaimReport> objList = new List<ClaimReport>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_ViewClaimReport";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@officeId", officeId);
                cmd.Parameters.AddWithValue("@policyNo", policyNo);
                cmd.Parameters.AddWithValue("@contactPersonId", contactPersonId);
                cmd.Parameters.AddWithValue("@policyholderId", policyholderId);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new ClaimReport
                    {
                        Pid = Convert.ToInt32(sdr["Pid"]),
                        UnderwriteDate = sdr["UdpDate"].ToString(),
                        PolicyholderName = sdr["ClientName"].ToString(),
                        ContactPerson = sdr["CPName"].ToString(),
                        PolicyNo=sdr["PolicyNo"].ToString(),
                        ProductName = sdr["ProductName"].ToString(),
                        PolicyDate = sdr["P_Date"].ToString(),
                        ClaimDate = sdr["ClaimDate"].ToString(),
                        TPName = sdr["TPName"].ToString(),
                        CategoryName = sdr["CategoryName"].ToString(),
                        MobileNo = sdr["MobileNo"].ToString(),
                        InsCo = Convert.ToString(sdr["InsCo"]),
                        ClaimAmount = sdr["ClaimAmount"].ToString(),
                        SettleAmount = sdr["SetteledAmmount"].ToString(),
                        SubjectInsured = sdr["SubjectInsured"].ToString(),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"]),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"]),
                        IssuingOffice = sdr["IssuingOfficeName"].ToString(),
                        ExpiryDate = sdr["E_Date"].ToString(),
                        PolicyStatus = sdr["PolicyStatusName"].ToString(),
                        ClaimStatus = sdr["ClaimStatus"].ToString(),
                        PatientName = sdr["PatientName"].ToString()
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                //ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/GetQueryList");
            }
            return objList;
        }


        public List<DashboardGraph> GetMonthlySalesForGraph(long CreatedBy)
        {
            List<DashboardGraph> objList = new List<DashboardGraph>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetMonthlySalesForGraph";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new DashboardGraph
                    {
                        Id = Convert.ToInt32(sdr["MonthInt"].ToString()),  
                        MonthYear = sdr["MonthYear"].ToString(),
                        SaleAmount = sdr["Amount"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Report_Repository/GetMonthlySalesForGraph");
            }
            return objList;
        }

        public List<Policy> ViewRenewalReport(long OfficeId, string Type, string FromDate, string ToDate, long CreatedBy)
        {
            List<Policy> objList = new List<Policy>();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_ViewRenewalReport";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@LoggedInUser", CreatedBy);
                cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@FromDate", FromDate);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                //cmd.Parameters.AddWithValue("@ContactPersonId", ContactPersonId);
                //cmd.Parameters.AddWithValue("@PolicyNo", PolicyNo);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();

                while (sdr.Read())
                {
                    objList.Add(new Policy
                    {
                        PId = Convert.ToInt64(sdr["PId"].ToString()),
                        QId = Convert.ToInt64(sdr["QId"].ToString()),
                        ClientId = Convert.ToInt32(sdr["ClientId"].ToString()),
                        ClientName = Convert.ToString(sdr["ClientName"].ToString()),
                        TypeId = Convert.ToInt32(sdr["TypeId"].ToString()),
                        TypeName = Convert.ToString(sdr["TypeName"].ToString()),
                        GroupId = Convert.ToInt32(sdr["GroupId"].ToString()),
                        GroupName = Convert.ToString(sdr["GroupName"].ToString()),
                        ContactPersonId = Convert.ToInt32(sdr["ContactPersonId"].ToString()),
                        ContactPersonName = Convert.ToString(sdr["CPName"].ToString()),
                        PolicyHolderId = Convert.ToInt32(sdr["PolicyHolderId"].ToString()),
                        PolicyHolderName = Convert.ToString(sdr["ClientName"].ToString()),
                        UnderWriteDate = Convert.ToString(sdr["URDate"].ToString()),
                        CategoryId = Convert.ToInt32(sdr["CategoryId"].ToString()),
                        CategoryName = Convert.ToString(sdr["CategoryName"].ToString()),
                        InsuranceCompanyId = Convert.ToInt32(sdr["InsuranceCompanyId"].ToString()),
                        InsuranceCompanyName = Convert.ToString(sdr["CompanyName"].ToString()),
                        IssuingOfficeId = Convert.ToInt32(sdr["IssuingOfficeId"].ToString()),
                        IssuingOfficeName = Convert.ToString(sdr["IssuingOfficeName"].ToString()),
                        ProductId = Convert.ToInt32(sdr["ProductId"].ToString()),
                        ProductName = Convert.ToString(sdr["ProductName"].ToString()),
                        SubjectInsured = Convert.ToString(sdr["SubjectInsured"].ToString()),
                        CostCenter = Convert.ToString(sdr["CostCenter"].ToString()),
                        CommissionRate = Convert.ToString(sdr["CommissionRate"].ToString()),
                        Premium = Convert.ToString(sdr["Premium"].ToString()),
                        CGST = Convert.ToString(sdr["CGST"].ToString()),
                        SGST = Convert.ToString(sdr["SGST"].ToString()),
                        PremiumPayable = Convert.ToString(sdr["PremiumPayable"].ToString()),
                        OfficeId = Convert.ToInt32(sdr["OfficeId"].ToString()),
                        PolicyStatus = Convert.ToInt32(sdr["PolicyStatus"].ToString()),
                        PolicyStatusName = Convert.ToString(sdr["PolicyStatusName"].ToString()),
                        VehicleNo = Convert.ToString(sdr["VehicleNo"].ToString()),
                        OnDamage = Convert.ToString(sdr["OnDamage"].ToString()),
                        PersonalAccident = Convert.ToString(sdr["PersonalAccident"].ToString()),
                        ThirdParty = Convert.ToString(sdr["ThirdParty"].ToString()),
                        IDV = Convert.ToString(sdr["IDV"].ToString()),
                        PolicyDate = Convert.ToString(sdr["PDate"].ToString()),
                        ExpiryDate = Convert.ToString(sdr["EDate"].ToString()),
                        PolicyNo = Convert.ToString(sdr["PolicyNo"].ToString()),
                        PreviousPolicyNo = Convert.ToString(sdr["PreviousPolicyNo"].ToString()),
                        UploadFileName = Convert.ToString(sdr["UploadFileName"].ToString()),
                        IsRenewal = Convert.ToInt32(sdr["IsRenewal"].ToString()),
                        ContactNo = Convert.ToString(sdr["MobileNo"].ToString()),
                        UW_FileName = Convert.ToString(sdr["UW_FileName"].ToString()),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Query_Repository/ViewRenewalReport ");
            }
            return objList;
        }
    }
}
