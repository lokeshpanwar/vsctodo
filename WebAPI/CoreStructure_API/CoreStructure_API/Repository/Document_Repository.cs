﻿using CoreStructure_API.Global;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Document_Repository
    {


        public DBReturnModel UploadDocument(DocUpload_table_Model objModel, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Uploading Document!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                long recordfieldid = 0;
                string recordfielditems = "";
                if (objModel.List_Item_Table_Model != null)
                {
                    foreach (var dataitem in objModel.List_Item_Table_Model)
                    {
                        recordfieldid = dataitem.List_ID;
                        recordfielditems = recordfielditems + dataitem.SLNO + "_" + dataitem.List_ItemType + "^" + dataitem.List_ItemValue + "^" + dataitem.ListItemUserEnterValue + "|";
                    }
                }

                if (CheckIfFileExists(objModel.CreatedBy, objModel.FileName) == "")
                {
                    string empPicName = SaveDocument(objModel.FilePath,  objModel.FileName);
                    if (empPicName != "")
                    {
                        objModel.FilePath = empPicName;

                        var connection = SqlHelper.Connection();
                        connection.Open();
                        SqlCommand cmd = connection.CreateCommand();
                        SqlTransaction transaction;
                        transaction = connection.BeginTransaction();
                        cmd.Transaction = transaction;
                        cmd.Connection = connection;
                        try
                        {
                            string sqlstr = "CB_SP_InsertDocument";

                            cmd.Parameters.Clear();
                            cmd.CommandText = sqlstr;
                            cmd.Connection = connection;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                            cmd.Parameters.AddWithValue("@DomainID", objModel.DomainID);
                            cmd.Parameters.AddWithValue("@Doc_Class_ID", objModel.Doc_Class_ID);
                            cmd.Parameters.AddWithValue("@DocType_ID", objModel.DocType_ID);
                            cmd.Parameters.AddWithValue("@Doc_Version", objModel.Doc_Version);
                            cmd.Parameters.AddWithValue("@FileName", objModel.FileName);
                            cmd.Parameters.AddWithValue("@FilePath", objModel.FilePath);
                            cmd.Parameters.AddWithValue("@VersionID", objModel.VersionID);
                            cmd.Parameters.AddWithValue("@FileSize", objModel.FileSize);
                            cmd.Parameters.AddWithValue("@CheckoutStatus", objModel.CheckoutStatus);
                            cmd.Parameters.AddWithValue("@CheckoutRight", objModel.CheckoutRight);
                            cmd.Parameters.AddWithValue("@Checkout_Available", objModel.Checkout_Available);
                            cmd.Parameters.AddWithValue("@IsCopied", objModel.IsCopied);
                            cmd.Parameters.AddWithValue("@CopiedTo", objModel.CopiedTo);
                            cmd.Parameters.AddWithValue("@IsMoved", objModel.IsMoved);
                            cmd.Parameters.AddWithValue("@Comments", objModel.Comments);
                            cmd.Parameters.AddWithValue("@sensitivity", objModel.sensitivity);
                            cmd.Parameters.AddWithValue("@IsDeleted", false);
                            cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                            cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                            cmd.Parameters.AddWithValue("@UploadedOn", StandardDateTime.GetDateTime());

                            cmd.Parameters.AddWithValue("@view_right", true);
                            cmd.Parameters.AddWithValue("@print_right", true);
                            cmd.Parameters.AddWithValue("@email_right", true);
                            cmd.Parameters.AddWithValue("@checkin_right", true);
                            cmd.Parameters.AddWithValue("@checkout_right", true);
                            cmd.Parameters.AddWithValue("@download_right", true);
                            cmd.Parameters.AddWithValue("@upload_right", true);
                            cmd.Parameters.AddWithValue("@selectall_right", true);

                            cmd.Parameters.AddWithValue("@RecordField_ID", recordfieldid);
                            cmd.Parameters.AddWithValue("@RecordField_Items", recordfielditems);

                            var returnCode = new SqlParameter();
                            returnCode.ParameterName = "@ret";
                            returnCode.SqlDbType = SqlDbType.VarChar;
                            returnCode.Size = 500;
                            returnCode.Direction = ParameterDirection.Output;
                            returnCode.Value = "";
                            cmd.Parameters.Add(returnCode);

                            var returnStatus = new SqlParameter();
                            returnStatus.ParameterName = "@retStatus";
                            returnStatus.SqlDbType = SqlDbType.VarChar;
                            returnStatus.Size = 50;
                            returnStatus.Direction = ParameterDirection.Output;
                            returnStatus.Value = "";
                            cmd.Parameters.Add(returnStatus);

                            //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                            //returnParameter.Direction = ParameterDirection.ReturnValue;

                            cmd.ExecuteNonQuery();

                            transaction.Commit();
                            connection.Close();
                            objReturn.ReturnMessage = returnCode.Value.ToString();
                            objReturn.ReturnStatus = returnStatus.Value.ToString();

                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.Sub_CompID.ToString(), objModel.CreatedBy.ToString(), LogType.Upload, "Document:" + objModel.Doc_Class_ID, "Document Uploaded", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), objModel.CreatedBy);
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            connection.Close();
                            ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/UploadDocument");
                            objReturn.ReturnMessage = ex.Message;
                        }
                    }
                }
                else
                {
                    objReturn.ReturnMessage = "File Already Exists!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/UploadDocument");
            }

            return objReturn;
        }


        private string SaveDocument(string EmpPhoto, string nFileName)
        {
            string result = "";

            try
            {
                var guid = Guid.NewGuid().ToString();


                byte[] imageBytes = Convert.FromBase64String(EmpPhoto);
                MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                //System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                string filepath = Path.Combine("" + "/Content/Documents", nFileName);
                File.WriteAllBytes(filepath, imageBytes);



                ////string filePath = Path.Combine(Microsoft.AspNetCore.Server.MapPath("~/Assets/") + Request.QueryString["id"] + "/", newFile);
                //image.Save(filepath);
                result = filepath;
            }
            catch (Exception ex)
            {
                //result = ex.Message;
            }

            return result;




        }


        private string CheckIfFileExists(long CreatedBy, string FileName)
        {
            string results = "";

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select * from DocUpload_Master where FileName=@FileName";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@FileName", FileName);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    results = "File Already Exists!";
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/CheckIfFileExists");
            }

            return results;

        }





        public List<DocUpload_table_Model> GetDocumentListByClassId(long CreatedBy, long ClassId)
        {
            List<DocUpload_table_Model> objList = new List<DocUpload_table_Model>();

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentByClassId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@DocClassID", ClassId);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocUpload_table_Model tempobj = new DocUpload_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Doc_ID = Convert.ToInt64(sdr["Doc_ID"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DomainID = Convert.ToInt32(sdr["DomainID"].ToString());
                    tempobj.Doc_Class_ID = Convert.ToInt32(sdr["Doc_Class_ID"].ToString());
                    tempobj.DocType_ID = Convert.ToInt64(sdr["DocType_ID"].ToString());
                    tempobj.Doc_Version = Convert.ToInt32(sdr["Doc_Version"].ToString());
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.FilePath = sdr["FilePath"].ToString();
                    tempobj.VersionID = sdr["VersionID"].ToString();
                    tempobj.FileSize = sdr["FileSize"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.UploadedOn = Convert.ToDateTime(sdr["UploadedOn"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.UploadedBy = Convert.ToInt64(sdr["UploadedBy"].ToString());
                    tempobj.CheckoutStatus = Convert.ToBoolean(sdr["CheckoutStatus"].ToString());
                    tempobj.CheckoutBy = Convert.ToInt64(sdr["CheckoutBy"].ToString());
                    tempobj.CheckoutOn = Convert.ToDateTime(sdr["CheckoutOn"].ToString());
                    tempobj.CheckoutRight = Convert.ToBoolean(sdr["CheckoutRight"].ToString());
                    tempobj.Checkout_Available = Convert.ToBoolean(sdr["Checkout_Available"].ToString());
                    tempobj.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.IsCopied = Convert.ToBoolean(sdr["IsCopied"].ToString());
                    tempobj.CopiedBy = Convert.ToInt64(sdr["CopiedBy"].ToString());
                    tempobj.CopiedOn = Convert.ToDateTime(sdr["CopiedOn"].ToString());
                    tempobj.CopiedTo = sdr["CopiedTo"].ToString();
                    tempobj.IsMoved = Convert.ToBoolean(sdr["IsMoved"].ToString());
                    tempobj.MovedOn = Convert.ToDateTime(sdr["MovedOn"].ToString());
                    tempobj.MovedBy = Convert.ToInt64(sdr["MovedBy"].ToString());
                    tempobj.MovedTo = sdr["MovedTo"].ToString();
                    tempobj.Comments = sdr["Comments"].ToString();
                    tempobj.sensitivity = Convert.ToInt64(sdr["sensitivity"].ToString());
                    tempobj.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/GetDocumentListByClassId");
            }

            return objList;

        }


        public DocUpload_table_Model GetOneDocumentById(long CreatedBy, long Id, string AuthorizationToken)
        {
            DocUpload_table_Model objModel = new DocUpload_table_Model();
            List<OldDocumentData> objOldDocList = new List<OldDocumentData>();
            List<DocumentComent_Table_Model> objDocCommList = new List<DocumentComent_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentById";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@DocID", Id);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Doc_ID = Convert.ToInt64(sdr["Doc_ID"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.DomainID = Convert.ToInt32(sdr["DomainID"].ToString());
                    objModel.Doc_Class_ID = Convert.ToInt32(sdr["Doc_Class_ID"].ToString());
                    objModel.DocType_ID = Convert.ToInt64(sdr["DocType_ID"].ToString());
                    objModel.Doc_Version = Convert.ToInt32(sdr["Doc_Version"].ToString());
                    objModel.FileName = sdr["FileName"].ToString();
                    objModel.FilePath = sdr["FilePath"].ToString();
                    objModel.VersionID = sdr["VersionID"].ToString();
                    objModel.FileSize = sdr["FileSize"].ToString();
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.UploadedOn = Convert.ToDateTime(sdr["UploadedOn"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.UploadedBy = Convert.ToInt64(sdr["UploadedBy"].ToString());
                    objModel.CheckoutStatus = Convert.ToBoolean(sdr["CheckoutStatus"].ToString());
                    objModel.CheckoutBy = Convert.ToInt64(sdr["CheckoutBy"].ToString());
                    objModel.CheckoutOn = Convert.ToDateTime(sdr["CheckoutOn"].ToString());
                    objModel.CheckoutRight = Convert.ToBoolean(sdr["CheckoutRight"].ToString());
                    objModel.Checkout_Available = Convert.ToBoolean(sdr["Checkout_Available"].ToString());
                    objModel.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());
                    objModel.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    objModel.IsCopied = Convert.ToBoolean(sdr["IsCopied"].ToString());
                    objModel.CopiedBy = Convert.ToInt64(sdr["CopiedBy"].ToString());
                    objModel.CopiedOn = Convert.ToDateTime(sdr["CopiedOn"].ToString());
                    objModel.CopiedTo = sdr["CopiedTo"].ToString();
                    objModel.IsMoved = Convert.ToBoolean(sdr["IsMoved"].ToString());
                    objModel.MovedOn = Convert.ToDateTime(sdr["MovedOn"].ToString());
                    objModel.MovedBy = Convert.ToInt64(sdr["MovedBy"].ToString());
                    objModel.MovedTo = sdr["MovedTo"].ToString();
                    objModel.Comments = sdr["Comments"].ToString();
                    objModel.sensitivity = Convert.ToInt64(sdr["sensitivity"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());

                    objModel.DocumentClassName = sdr["DocumentClassName"].ToString();
                    objModel.DocumentTypeName = sdr["DocumentTypeName"].ToString();
                }
                sdr.Close();



                sqlstr = "CB_SP_SelectOldDocumentById";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@DocID", objModel.Doc_ID);
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    OldDocumentData tempobj = new OldDocumentData();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.VersionID = sdr["VersionID"].ToString();
                    tempobj.FileSize = sdr["FileSize"].ToString();
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.FilePath = sdr["FilePath"].ToString();
                    objOldDocList.Add(tempobj);
                }
                sdr.Close();


                sqlstr = "Select SLNO, UserID, Isnull(DocumentID,'') as DocumentID, Isnull(VersionID,'') as VersionID, "
                + " Comments, Isnull(Status, 0) as Status,Date From DocumentComent_Table where DocumentID = @DocumentID";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@DocumentID", objModel.Doc_ID);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocumentComent_Table_Model tempobj = new DocumentComent_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.VersionID = sdr["VersionID"].ToString();
                    tempobj.UserID = Convert.ToInt64(sdr["UserID"].ToString());
                    tempobj.DocumentID = Convert.ToInt64(sdr["DocumentID"].ToString());
                    tempobj.Status = sdr["Status"].ToString();
                    tempobj.Comments = sdr["Comments"].ToString();
                    tempobj.Date = Convert.ToDateTime(sdr["Date"].ToString());
                    objDocCommList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
                objModel.OldDocumentData_List = objOldDocList;
                objModel.User_Document_Mapping_Table_Model = GetUserListFromDocumentId(CreatedBy, objModel.Doc_ID);
                objModel.DocumentComent_Table_Model_List = objDocCommList;
                if (AuthorizationToken != "")
                {
                    Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.Sub_CompID.ToString(), objModel.CreatedBy.ToString(), LogType.View, "Document:" + objModel.Doc_Class_ID, "Document Uploaded", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), objModel.CreatedBy);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/GetDocumentById");
            }

            return objModel;

        }


        public List<User_Document_Mapping_Table_Model> GetUserListFromDocumentId(long CreatedBy, long Id)
        {
            List<User_Document_Mapping_Table_Model> objList = new List<User_Document_Mapping_Table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectDocumentByUserId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@DocumentID", Id);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    User_Document_Mapping_Table_Model tempobj = new User_Document_Mapping_Table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.Domain_ID = Convert.ToInt64(sdr["Domain_ID"].ToString());
                    tempobj.Document_ClassID = Convert.ToInt64(sdr["Document_ClassID"].ToString());
                    tempobj.Document_Type = Convert.ToInt64(sdr["Document_Type"].ToString());
                    tempobj.Document_ID = Convert.ToInt64(sdr["Document_ID"].ToString());
                    tempobj.User_ID = Convert.ToInt64(sdr["User_ID"].ToString());
                    //tempobj.DocType_ID = sdr["DocType_ID"].ToString();
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.print_right = Convert.ToBoolean(sdr["print_right"].ToString());
                    tempobj.email_right = Convert.ToBoolean(sdr["email_right"].ToString());
                    tempobj.checkin_right = Convert.ToBoolean(sdr["checkin_right"].ToString());
                    tempobj.checkout_right = Convert.ToBoolean(sdr["checkout_right"].ToString());
                    tempobj.download_right = Convert.ToBoolean(sdr["download_right"].ToString());
                    tempobj.upload_right = Convert.ToBoolean(sdr["upload_right"].ToString());
                    tempobj.selectall_right = Convert.ToBoolean(sdr["selectall_right"].ToString());
                    tempobj.view_right = Convert.ToBoolean(sdr["view_right"].ToString());

                    tempobj.DCCompanyName = sdr["DCCompanyName"].ToString();
                    tempobj.DCDomainName = sdr["DCDomainName"].ToString();
                    tempobj.DCUserName = sdr["DCUserName"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/GetUserListFromDocumentId");
            }

            return objList;
        }




        public DBReturnModel CheckoutDocument(long CreatedBy, long DocId, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Checkout Document!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var docdata = GetOneDocumentById(CreatedBy, DocId, "");

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_CheckoutDocument";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", DocId);
                    cmd.Parameters.AddWithValue("@CheckoutStatus", true);
                    cmd.Parameters.AddWithValue("@CheckoutBy", CreatedBy);
                    cmd.Parameters.AddWithValue("@CheckoutOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;

                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                    if (objReturn.ReturnStatus == "SUCCESS")
                    {
                        //webpath = webpath + "\\Content\\Documents";
                        var path = Path.Combine("", objReturn.ReturnMessage);
                        //objReturn.ReturnMessage = GlobalFunction.getImageURL() + "/api/Document/GetDocumentFile?path=" + path;
                        objReturn.ReturnMessage = path;
                        if (AuthorizationToken != "")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), docdata.Sub_CompID.ToString(), CreatedBy.ToString(), LogType.CheckOut, "Document:" + docdata.Doc_Class_ID, "Document Uploaded", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), CreatedBy);
                        }
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/CheckoutDocument");
                    objReturn.ReturnMessage = ex.Message;
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/CheckoutDocument");
            }

            return objReturn;
        }



        public List<DocUpload_table_Model> GetCheckoutDocumentList(long CreatedBy, long SubCompId, long DomainId, long UserId)
        {
            List<DocUpload_table_Model> objList = new List<DocUpload_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectCheckedOutDocument";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@SubCompId", SubCompId);
                cmd.Parameters.AddWithValue("@DomainId", DomainId);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@CheckoutStatus", true);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    DocUpload_table_Model tempobj = new DocUpload_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.FileName = sdr["FileName"].ToString();
                    tempobj.VersionID = sdr["VersionID"].ToString();
                    tempobj.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    tempobj.DomainID = Convert.ToInt64(sdr["DomainID"].ToString());
                    tempobj.Doc_Class_ID = Convert.ToInt64(sdr["Doc_Class_ID"].ToString());
                    tempobj.DocType_ID = Convert.ToInt64(sdr["DocType_ID"].ToString());
                    tempobj.Doc_ID = Convert.ToInt64(sdr["DocumentID"].ToString());
                    tempobj.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.DocumentClassName = sdr["DCClassName"].ToString();
                    //tempobj.dcc = sdr["DCCompanyName"].ToString();
                    //tempobj.dom = sdr["DCDomainName"].ToString();
                    tempobj.DCUserName = sdr["DCUserName"].ToString();
                    tempobj.CheckoutOn = Convert.ToDateTime(sdr["CheckoutOn"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/GetCheckoutDocumentList");
            }
            return objList;
        }




        public DBReturnModel CancelCheckoutDocumentFromAdmin(long CreatedBy, long DocId, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Checkout Cancel Document!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                var docdata = GetOneDocumentById(CreatedBy, DocId, "");
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_CheckoutDocument";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", DocId);
                    cmd.Parameters.AddWithValue("@CheckoutStatus", false);
                    cmd.Parameters.AddWithValue("@CheckoutBy", CreatedBy);
                    cmd.Parameters.AddWithValue("@CheckoutOn", StandardDateTime.GetDateTime());

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;

                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                    if (objReturn.ReturnStatus == "SUCCESS")
                    {
                        objReturn.ReturnMessage = "Checkout Cancelled Successfully!";

                        if (AuthorizationToken != "")
                        {
                            Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), docdata.Sub_CompID.ToString(), CreatedBy.ToString(), LogType.CheckOutCancel, "Document:" + docdata.Doc_Class_ID, "Document Uploaded", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), CreatedBy);
                        }
                    }


                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/CancelCheckoutDocumentFromAdmin");
                    objReturn.ReturnMessage = ex.Message;
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Document_Repository/CancelCheckoutDocumentFromAdmin");
            }

            return objReturn;
        }



        public User_docClass_Report_Model GetOneDocumentClassReport(long Id, long AdminId)
        {
            User_docClass_Report_Model objRetModel = new User_docClass_Report_Model();
            List<UserDocType> objUserDocTypeList = new List<UserDocType>();
            List<UserDoc> objUserDocList = new List<UserDoc>();
            try
            {
                DocumentClass_Repository objDocClassRepo = new DocumentClass_Repository();
                var docclass = objDocClassRepo.GetOneDocumentClass(Id, AdminId, "");
                if (docclass != null)
                {
                    objRetModel.ClassId = docclass.SLNO;
                    objRetModel.ClassName = docclass.DocumentClassName;
                    objRetModel.CreatedBy = docclass.CreatedBy.ToString();
                    if (docclass.IsDeleted == true)
                    {
                        objRetModel.Status = "True";
                    }
                    else
                    {
                        objRetModel.Status = "False";
                    }
                    objRetModel.Type = docclass.DocumentAccessBy;
                    if (docclass.VersionControlStatus == true)
                    {
                        objRetModel.Version = "True";
                    }
                    else
                    {
                        objRetModel.Version = "False";
                    }
                }


                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select DTYPE.SLNO, DTYPE.DocumentType_Name, DTYPE.documenType_desc,DTYPE.IsDeleted, "
                + " (Select Count(*) From DocUpload_table where DocUpload_table.DocType_ID = DTYPE.SLNO) as NoOfDocuments, "
                + " (Select Count(*) From User_DocType_Mapping_Table where User_DocType_Mapping_Table.Document_Type = DTYPE.SLNO) as NoOfUsers "
                + " From DocumentType_Table DTYPE where DTYPE.SLNO "
                + " in (Select DocumentType_ID from DocumentType_DocClass_Mapping_Table where Document_ClassId = @Document_ClassId) ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@Document_ClassId", Id);
                cmd.CommandType = System.Data.CommandType.Text;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDocType tempobj = new UserDocType();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.DocumentTypeName = sdr["DocumentType_Name"].ToString();
                    if (Convert.ToBoolean(sdr["IsDeleted"].ToString()) == true)
                    {
                        tempobj.Status = "True";
                    }
                    else
                    {
                        tempobj.Status = "False";
                    }
                    tempobj.NoOfDocuments = Convert.ToInt32(sdr["NoOfDocuments"].ToString());
                    tempobj.NoOfUsers = Convert.ToInt32(sdr["NoOfUsers"].ToString());

                    objUserDocTypeList.Add(tempobj);
                }
                sdr.Close();
                objRetModel.UserDocTypeList = objUserDocTypeList;




                int i = 0;
                sqlstr = "Select Count(*) as NoOfDocUploaded, CreatedBy,IsDeleted , "
                + " (Select Userid from Employee_table where Employee_table.SLNO = DocUpload_table.CreatedBy) as UserID "
                + " From DocUpload_table where Doc_Class_Id = 23 Group By CreatedBy,IsDeleted";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@Document_ClassId", Id);
                cmd.CommandType = System.Data.CommandType.Text;
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    UserDoc tempobj = new UserDoc();
                    tempobj.SLNO = i + 1;
                    tempobj.NoOfDocumentsUploaded = Convert.ToInt32(sdr["NoOfDocUploaded"].ToString());
                    if (Convert.ToBoolean(sdr["IsDeleted"].ToString()) == true)
                    {
                        tempobj.Status = "True";
                    }
                    else
                    {
                        tempobj.Status = "False";
                    }
                    tempobj.UserName = sdr["UserID"].ToString();

                    objUserDocList.Add(tempobj);
                }
                sdr.Close();
                objRetModel.UserDocList = objUserDocList;

                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, AdminId, "Document_Repository/GetOneDocumentClassReport", "API");
            }

            return objRetModel;
        }




        public DBReturnModel CheckinDocument(DocUpload_table_Model objModel, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Checkin Document!";
            objReturn.ReturnStatus = "ERROR";
            try
            {
                objModel.VersionID = (Convert.ToDecimal(objModel.VersionID) + Convert.ToDecimal(0.1)).ToString();
                long recordfieldid = 0;
                string recordfielditems = "";
                foreach (var dataitem in objModel.List_Item_Table_Model)
                {
                    recordfieldid = dataitem.List_ID;
                    recordfielditems = recordfielditems + dataitem.SLNO + "_" + dataitem.List_ItemType + "^" + dataitem.List_ItemValue + "^" + dataitem.ListItemUserEnterValue + "|";
                }
                var fnm = objModel.FileName.Split('.');
                objModel.FileName = fnm[0].ToString() + "_" + objModel.VersionID;
                objModel.FileName = objModel.FileName + "." + fnm[1].ToString();
                string empPicName = SaveDocument(objModel.FilePath, objModel.FileName);
                if (empPicName != "")
                {
                    objModel.FilePath = empPicName;

                    var connection = SqlHelper.Connection();
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction();
                    cmd.Transaction = transaction;
                    cmd.Connection = connection;
                    try
                    {
                        string sqlstr = "CB_SP_CheckinDocument";

                        cmd.Parameters.Clear();
                        cmd.CommandText = sqlstr;
                        cmd.Connection = connection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Doc_ID", objModel.Doc_ID);
                        cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                        cmd.Parameters.AddWithValue("@DomainID", objModel.DomainID);
                        cmd.Parameters.AddWithValue("@Doc_Class_ID", objModel.Doc_Class_ID);
                        cmd.Parameters.AddWithValue("@DocType_ID", objModel.DocType_ID);
                        cmd.Parameters.AddWithValue("@Doc_Version", objModel.Doc_Version);
                        cmd.Parameters.AddWithValue("@FileName", objModel.FileName);
                        cmd.Parameters.AddWithValue("@FilePath", objModel.FilePath);
                        cmd.Parameters.AddWithValue("@VersionID", objModel.VersionID);
                        cmd.Parameters.AddWithValue("@FileSize", objModel.FileSize);
                        cmd.Parameters.AddWithValue("@CheckoutStatus", false);
                        cmd.Parameters.AddWithValue("@CheckoutRight", objModel.CheckoutRight);
                        cmd.Parameters.AddWithValue("@Checkout_Available", objModel.Checkout_Available);
                        cmd.Parameters.AddWithValue("@IsCopied", objModel.IsCopied);
                        cmd.Parameters.AddWithValue("@CopiedTo", objModel.CopiedTo);
                        cmd.Parameters.AddWithValue("@IsMoved", objModel.IsMoved);
                        cmd.Parameters.AddWithValue("@Comments", objModel.Comments);
                        cmd.Parameters.AddWithValue("@sensitivity", objModel.sensitivity);
                        cmd.Parameters.AddWithValue("@IsDeleted", false);
                        cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                        cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());
                        cmd.Parameters.AddWithValue("@UploadedOn", StandardDateTime.GetDateTime());

                        cmd.Parameters.AddWithValue("@view_right", true);
                        cmd.Parameters.AddWithValue("@print_right", true);
                        cmd.Parameters.AddWithValue("@email_right", true);
                        cmd.Parameters.AddWithValue("@checkin_right", true);
                        cmd.Parameters.AddWithValue("@checkout_right", true);
                        cmd.Parameters.AddWithValue("@download_right", true);
                        cmd.Parameters.AddWithValue("@upload_right", true);
                        cmd.Parameters.AddWithValue("@selectall_right", true);

                        cmd.Parameters.AddWithValue("@RecordField_ID", recordfieldid);
                        cmd.Parameters.AddWithValue("@RecordField_Items", recordfielditems);

                        var returnCode = new SqlParameter();
                        returnCode.ParameterName = "@ret";
                        returnCode.SqlDbType = SqlDbType.VarChar;
                        returnCode.Size = 500;
                        returnCode.Direction = ParameterDirection.Output;
                        returnCode.Value = "";
                        cmd.Parameters.Add(returnCode);

                        var returnStatus = new SqlParameter();
                        returnStatus.ParameterName = "@retStatus";
                        returnStatus.SqlDbType = SqlDbType.VarChar;
                        returnStatus.Size = 50;
                        returnStatus.Direction = ParameterDirection.Output;
                        returnStatus.Value = "";
                        cmd.Parameters.Add(returnStatus);

                        //SqlParameter returnParameter = cmd.Parameters.Add("@ret", SqlDbType.VarChar,500);
                        //returnParameter.Direction = ParameterDirection.ReturnValue;

                        cmd.ExecuteNonQuery();

                        transaction.Commit();
                        connection.Close();
                        objReturn.ReturnMessage = returnCode.Value.ToString();
                        objReturn.ReturnStatus = returnStatus.Value.ToString();

                        if (objReturn.ReturnStatus == "SUCCESS")
                        {
                            if (AuthorizationToken != "")
                            {
                                Log_Repository.InsertLog(GlobalFunction.getLoggedInUserParentCompanyId(AuthorizationToken).ToString(), objModel.Sub_CompID.ToString(), objModel.CreatedBy.ToString(), LogType.CheckIn, "Document:" + objModel.Doc_Class_ID, "Document Uploaded", GlobalFunction.getLoggedInUserIPAddress(AuthorizationToken), objModel.CreatedBy);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/CheckinDocument");
                        objReturn.ReturnMessage = ex.Message;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/CheckinDocument");
            }

            return objReturn;
        }




        public DBReturnModel DocumentSubmitForApproval(DocUpload_table_Model objModel, string AuthorizationToken)
        {
            DBReturnModel objReturn = new DBReturnModel();
            objReturn.ReturnMessage = "Error on Submitting Document For Approval!";
            objReturn.ReturnStatus = "ERROR";
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertDMSSubmitForApproval";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;



                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.DMSProcess_Approvers_Type";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    dataTable.Columns.Add("Action", typeof(string));
                    dataTable.Columns.Add("Comments", typeof(string));
                    foreach (var dta in objModel.DMSProcess_Approvers_Model)
                    {
                        dataTable.Rows.Add(dta.User_ID, dta.Action, objModel.DocumentComent_Table_Model.Comments);
                    }
                    SqlParameter parameter = new SqlParameter("DMSProcess_Approvers", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);



                    var dataTable1 = new DataTable();
                    dataTable1.TableName = "dbo.DMSProcess_Checkers_Type";
                    dataTable1.Columns.Add("User_ID", typeof(long));
                    dataTable1.Columns.Add("Action", typeof(string));
                    dataTable1.Columns.Add("Comments", typeof(string));
                    foreach (var dta in objModel.DMSProcess_Checkers_Model)
                    {
                        dataTable1.Rows.Add(dta.User_ID, dta.Action, objModel.DocumentComent_Table_Model.Comments);
                    }
                    SqlParameter parameter1 = new SqlParameter("DMSProcess_Checkers", SqlDbType.Structured);
                    parameter1.TypeName = dataTable1.TableName;
                    parameter1.Value = dataTable1;
                    cmd.Parameters.Add(parameter1);


                    cmd.Parameters.AddWithValue("@DocumentID", objModel.DocumentComent_Table_Model.DocumentID);
                    cmd.Parameters.AddWithValue("@VersionID", objModel.DocumentComent_Table_Model.VersionID);
                    //cmd.Parameters.AddWithValue("@DMSProceesID", objModel.DMSProcess_Checkers_Model.DMSProceesID);
                    //cmd.Parameters.AddWithValue("@Chk_User_ID", objModel.DMSProcess_Checkers_Model.User_ID);
                    //cmd.Parameters.AddWithValue("@Chk_Action", objModel.DMSProcess_Checkers_Model.Action);
                    //cmd.Parameters.AddWithValue("@Apr_User_ID", objModel.DMSProcess_Approvers_Model.User_ID);
                    //cmd.Parameters.AddWithValue("@Apr_Action", objModel.DMSProcess_Approvers_Model.Action);
                    cmd.Parameters.AddWithValue("@Comments", objModel.DocumentComent_Table_Model.Comments);
                    cmd.Parameters.AddWithValue("@Status", objModel.DocumentComent_Table_Model.Status);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedOn", StandardDateTime.GetDateTime());

                    cmd.Parameters.AddWithValue("@ProcessValue", objModel.DMSProcess_Model.ProcessValue);
                    cmd.Parameters.AddWithValue("@PageUrl", objModel.DMSProcess_Model.PageUrl);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    var returnStatus = new SqlParameter();
                    returnStatus.ParameterName = "@retStatus";
                    returnStatus.SqlDbType = SqlDbType.VarChar;
                    returnStatus.Size = 50;
                    returnStatus.Direction = ParameterDirection.Output;
                    returnStatus.Value = "";
                    cmd.Parameters.Add(returnStatus);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    objReturn.ReturnMessage = returnCode.Value.ToString();
                    objReturn.ReturnStatus = returnStatus.Value.ToString();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/DocumentSubmitForApproval");
                    objReturn.ReturnMessage = ex.Message;
                }


            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Document_Repository/DocumentSubmitForApproval");
            }

            return objReturn;
        }


    }
}
