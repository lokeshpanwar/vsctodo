﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using static CoreStructure_API.Utility.Standard;

namespace CoreStructure_API.Repository
{
    public class Group_Repository
    {

        public string InsertGroup(Group_table_Model objModel)
        {
            string result = "Error on Inserting Group!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertGroup";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Parent_compID", objModel.Parent_compID);
                    cmd.Parameters.AddWithValue("@Sub_compID", objModel.Sub_compID);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@CreatedDate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@GroupName", objModel.GroupName);
                    cmd.Parameters.AddWithValue("@GroupDescription", objModel.GroupDescription);
                    cmd.Parameters.AddWithValue("@GroupEmail", objModel.GroupEmail);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Group_Repository/InsertGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "Group_Repository/InsertGroup");
            }

            return result;
        }

        public Group_table_Model GetOneGroup(long SLNO, long CreatedBy)
        {
            Group_table_Model objModel = new Group_table_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectGroupById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    objModel.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    objModel.GroupDescription = sdr["GroupDescription"].ToString();
                    objModel.GroupEmail = sdr["GroupEmail"].ToString();
                    objModel.GroupName = sdr["GroupName"].ToString();

                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedDate = Convert.ToDateTime(sdr["ModifiedDate"].ToString());
                    objModel.Parent_compID = Convert.ToInt64(sdr["Parent_compID"].ToString());
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_compID = Convert.ToInt64(sdr["Sub_compID"].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Group_Repository/GetOneGroup");
            }

            return objModel;

        }

        public List<Group_table_Model> GetGroupList(long CreatedBy)
        {
            List<Group_table_Model> objModel = new List<Group_table_Model>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectGroupById ";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", 0);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Group_table_Model tempobj = new Group_table_Model();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.DeletedBy = Convert.ToInt64(sdr["DeletedBy"].ToString());
                    tempobj.GroupDescription = sdr["GroupDescription"].ToString();
                    tempobj.GroupEmail = sdr["GroupEmail"].ToString();
                    tempobj.GroupName = sdr["GroupName"].ToString();

                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    tempobj.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    tempobj.ModifiedDate = Convert.ToDateTime(sdr["ModifiedDate"].ToString());
                    tempobj.Parent_compID = Convert.ToInt64(sdr["Parent_compID"].ToString());
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Sub_compID = Convert.ToInt64(sdr["Sub_compID"].ToString());

                    objModel.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Group_Repository/GetGroupList");
            }

            return objModel;

        }

        public string UpdateGroup(Group_table_Model objModel)
        {
            string result = "Error on Updating Group!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateGroup";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Parent_compID", objModel.Parent_compID);
                    cmd.Parameters.AddWithValue("@Sub_compID", objModel.Sub_compID);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);
                    cmd.Parameters.AddWithValue("@ModifiedDate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@GroupName", objModel.GroupName);
                    cmd.Parameters.AddWithValue("@GroupDescription", objModel.GroupDescription);
                    cmd.Parameters.AddWithValue("@GroupEmail", objModel.GroupEmail);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();


                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Group_Repository/UpdateGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.ModifiedBy, "API", "Group_Repository/UpdateGroup");
            }

            return result;
        }

        public Group_table_page_Model GetGroupListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            Group_table_page_Model objModel = new Group_table_page_Model();
            List<Group_table_Model> objList = new List<Group_table_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectGroupByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Group_table_Model tempobj = new Group_table_Model();
                    tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.GroupName = sdr["GroupName"].ToString();
                    tempobj.GroupDescription = sdr["GroupDescription"].ToString();
                    tempobj.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    tempobj.CreatedDate = Convert.ToDateTime(sdr["CreatedDate"].ToString());
                    tempobj.GroupEmail = sdr["GroupEmail"].ToString();
                    tempobj.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From Group_table where (Isnull(GroupName,'') like '%'+@search+'%' or "
                        + " Isnull(GroupDescription,'') like '%'+@search+'%' or Isnull(GroupEmail,'') like '%'+@search+'%' )";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.Group_table_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Group_Repository/GetGroupListByPage");
            }

            return objModel;

        }

        public string DisableGroup(long SLNO, long DeletedBy)
        {
            string result = "Error on Disabling Group!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Group_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,ModifiedDate=@ModifiedDate "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", true);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@ModifiedDate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Group Disabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Group_Repository/DisableGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Group_Repository/DisableGroup");
            }

            return result;
        }

        public string EnableGroup(long SLNO, long DeletedBy)
        {
            string result = "Error on Enabling Group!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "";

                    sqlstr = "update Group_table set IsDeleted=@IsDeleted,"
                        + " DeletedBy=@DeletedBy,ModifiedDate=@ModifiedDate "
                        + " where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.Parameters.AddWithValue("@ModifiedDate", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    connection.Close();
                    result = "Group Enabled Successfully!";
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "Group_Repository/EnableGroup");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "Group_Repository/EnableGroup");
            }

            return result;
        }





        public List<Group_Company_Model_Mapping> GetGroupCompanyListForMapping(long UserID, long GroupID)
        {
            List<Group_Company_Model_Mapping> objList = new List<Group_Company_Model_Mapping>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SC.SLNO, SC.SubCompanyName, Isnull(ID,0) as ID, Isnull(GroupID,0) as GroupID "
                + " From Sub_Company_table SC Left outer join Group_Company GC on SC.SLNO = GC.CompanyID And GC.GroupID=@GroupID";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@GroupID", GroupID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Group_Company_Model_Mapping tempobj = new Group_Company_Model_Mapping();
                    tempobj.CompanyID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.CompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                    if (Convert.ToInt64(sdr["ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserID, "API", "Group_Repository/GetGroupCompanyListForMapping");
            }
            return objList;
        }


        public string InsertGroupCompanyMapping(Group_Company_Model objModel, long Maped_By)
        {
            string result = "Error on Inserting Group Company Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertGroupCompanyMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("GroupID", objModel.GroupID);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Group_Company_Type";
                    dataTable.Columns.Add("Maped_DomainID", typeof(long));
                    foreach (var dta in objModel.Group_Company_Model_Mapping_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.CompanyID); // Id of '1' is valid for the Person table
                        }
                    }
                    SqlParameter parameter = new SqlParameter("GroupCompMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 50;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    result = returnCode.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupCompanyMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupCompanyMapping");
            }

            return result;
        }


        public List<Group_Domain_Model_Mapping> GetGroupCompanyDomainListForMapping(long UserID, long GroupID, long SubCompID)
        {
            List<Group_Domain_Model_Mapping> objList = new List<Group_Domain_Model_Mapping>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = " Select Maped_DomainID, Isnull(ID,0) as ID, (Select Domain_Name From Domain_table Where Domain_table.SLNO = DM.Maped_DomainID) "
                + " as Domain_Name  From Domain_Mapping_table DM Left join Group_Domain GD on DM.Maped_DomainID = GD.DomainID  and GD.GroupID = @GroupID"
                + " where DM.Maped_CompID = @SubCompId";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@GroupID", GroupID);
                cmd.Parameters.AddWithValue("@SubCompId", SubCompID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    Group_Domain_Model_Mapping tempobj = new Group_Domain_Model_Mapping();
                    tempobj.DomainID = Convert.ToInt64(sdr["Maped_DomainID"].ToString());
                    tempobj.DomainName = sdr["Domain_Name"].ToString();
                    tempobj.ID = Convert.ToInt64(sdr["ID"].ToString());
                    if (Convert.ToInt64(sdr["ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserID, "API", "Group_Repository/GetGroupCompanyListForMapping");
            }
            return objList;
        }


        public string InsertGroupDomainMapping(Group_Company_Model objModel, long Maped_By)
        {
            string result = "Error on Inserting Group Domain Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertGroupDomainMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("GroupID", objModel.GroupID);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.Group_Domain_Type";
                    dataTable.Columns.Add("CompanyID", typeof(long));
                    dataTable.Columns.Add("DomainID", typeof(long));
                    foreach (var dta in objModel.Group_Domain_Model_Mapping_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(objModel.CompanyID, dta.DomainID);
                        }
                    }
                    SqlParameter parameter = new SqlParameter("GroupDomainMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    result = returnCode.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupDomainMapping");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupDomainMapping");
            }

            return result;
        }


        public List<GroupUser_Model_Mapping> GetGroupUserListForMapping(long UserID, long GroupID, long SubCompID, long DomainID)
        {
            List<GroupUser_Model_Mapping> objList = new List<GroupUser_Model_Mapping>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = " Select SLNO, Isnull(EmpId,'') as EmpId, EmployeeName,Isnull(GU.User_ID,0) as User_ID "
                + "  From Employee_table EM Left join GroupUser GU on EM.SLNO = GU.User_ID  and GU.GroupID = @GroupID "
                + " where EM.Sub_Company = @SubCompId and EM.Domain_ID = @DomainId and EM.User_type='EMPLOYEE'";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@DomainId", DomainID);
                cmd.Parameters.AddWithValue("@SubCompId", SubCompID);
                cmd.Parameters.AddWithValue("@GroupID", GroupID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    GroupUser_Model_Mapping tempobj = new GroupUser_Model_Mapping();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.UserName = sdr["EmployeeName"].ToString() + " (" + sdr["EmpId"].ToString() + ")";
                    tempobj.ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    if (Convert.ToInt64(sdr["User_ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserID, "API", "Group_Repository/GetGroupUserListForMapping");
            }
            return objList;
        }


        public string InsertGroupUserMapping(Group_Company_Model objModel, long Maped_By)
        {
            string result = "Error on Inserting Group User Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertGroupUserMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("GroupID", objModel.GroupID);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.GroupUser_Type";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    foreach (var dta in objModel.GroupUser_Model_Mapping_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.User_ID);
                        }
                    }
                    SqlParameter parameter = new SqlParameter("GroupUserMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    result = returnCode.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupUserMapping");
                    result = "System Error on Inserting Group User!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupUserMapping");
            }

            return result;
        }



        public List<GroupUser_Model_Mapping> GetGroupVendorListForMapping(long UserID, long GroupID, long SubCompID)
        {
            List<GroupUser_Model_Mapping> objList = new List<GroupUser_Model_Mapping>();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = " Select SLNO, Isnull(EmpId,'') as EmpId, EmployeeName,Isnull(GU.User_ID,0) as User_ID "
                + "  From Employee_table EM Left join GroupUser GU on EM.SLNO = GU.User_ID  and GU.GroupID = @GroupID "
                + " where EM.Sub_Company = @SubCompId and EM.User_type='VENDOR'";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@SubCompId", SubCompID);
                cmd.Parameters.AddWithValue("@GroupID", GroupID);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    GroupUser_Model_Mapping tempobj = new GroupUser_Model_Mapping();
                    tempobj.User_ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.UserName = sdr["EmployeeName"].ToString() + " (" + sdr["EmpId"].ToString() + ")";
                    tempobj.ID = Convert.ToInt64(sdr["SLNO"].ToString());
                    if (Convert.ToInt64(sdr["User_ID"].ToString()) > 0)
                    {
                        tempobj.IsSelected = true;
                    }
                    else
                    {
                        tempobj.IsSelected = false;
                    }
                    objList.Add(tempobj);
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, UserID, "API", "Group_Repository/GetGroupVendorListForMapping");
            }
            return objList;
        }


        public string InsertGroupVendorMapping(Group_Company_Model objModel, long Maped_By)
        {
            string result = "Error on Inserting Group Vendor Mapping!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_InsertGroupUserMapping";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("GroupID", objModel.GroupID);

                    var dataTable = new DataTable();
                    dataTable.TableName = "dbo.GroupUser_Type";
                    dataTable.Columns.Add("User_ID", typeof(long));
                    foreach (var dta in objModel.GroupVendor_Model_Mapping_List)
                    {
                        if (dta.IsSelected == true)
                        {
                            dataTable.Rows.Add(dta.User_ID);
                        }
                    }
                    SqlParameter parameter = new SqlParameter("GroupUserMap", SqlDbType.Structured);
                    parameter.TypeName = dataTable.TableName;
                    parameter.Value = dataTable;
                    cmd.Parameters.Add(parameter);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    result = returnCode.Value.ToString();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupVendorMapping");
                    result = "System Error on Inserting Group User!";
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Maped_By, "API", "Group_Repository/InsertGroupVendorMapping");
            }

            return result;
        }





        public GroupMapping_page_Model GetGroupMapListByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            GroupMapping_page_Model objModel = new GroupMapping_page_Model();
            List<GroupMapping_Model> objList = new List<GroupMapping_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SubCompanyName";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "CB_SP_SelectGroupMapByPage";
                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    GroupMapping_Model tempobj = new GroupMapping_Model();
                    // tempobj.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    tempobj.Domain_Name = sdr["Domain_Name"].ToString();
                    tempobj.SubCompanyName = sdr["SubCompanyName"].ToString();
                    tempobj.UserName = sdr["UserName"].ToString();
                    tempobj.Vendor = sdr["Vendor"].ToString();
                    tempobj.GroupID = Convert.ToInt64(sdr["GroupID"].ToString());
                    tempobj.GroupName = sdr["GroupName"].ToString();
                    objList.Add(tempobj);
                }
                sdr.Close();

                sqlstr = "Select Count(*) As Count From  View_GroupMapping " +
               "where  (Domain_Name like '%'+@search+'%'" +
              "or SubCompanyName  like '%'+@search+'%'" +
              "or UserName  like '%'+@search+'%' or Vendor like '%'+@search+'%') ";
                cmd.Parameters.Clear();
                cmd.CommandText = sqlstr;
                cmd.Connection = connection;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

                connection.Close();
                objModel.GroupMapping_Model_List = objList;
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "Group_Repository/GetGroupMapListByPage");
            }

            return objModel;

        }


    }
}
