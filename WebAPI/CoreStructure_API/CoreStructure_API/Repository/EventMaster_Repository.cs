﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using System.Data.SqlClient;
using System.Data;
using static CoreStructure_API.Utility.Standard;
namespace CoreStructure_API.Repository
{
    public class EventMaster_Repository
    {

        public DBReturnModel InsertEventMaster(EventMaster_Model objModel)
        {
            string result = "Error on Inserting Event Master!";

            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;
            try
            {

                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "CB_SP_InsertEventMaster";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@EventTitle", objModel.EventTitle);
                    cmd.Parameters.AddWithValue("@StartDate", objModel.StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);
                    cmd.Parameters.AddWithValue("@EventDetail", objModel.EventDetail);
                    cmd.Parameters.AddWithValue("@EventDomain", objModel.EventDomain);
                    cmd.Parameters.AddWithValue("@CreatedOn",StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@CreatedBy", objModel.CreatedBy);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);


                    var returncode = new SqlParameter();
                    returncode.ParameterName = "retMessage";
                    returncode.SqlDbType = SqlDbType.VarChar;
                    returncode.Size = 500;
                    returncode.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(returncode);

                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 500;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    
                    objreturn.ReturnMessage = returncode.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                   
                    connection.Close();

                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "EventMaster_Repository/InsertEventMaster");
                }
            }

            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "EventMaster_Repository/InsertEventMaster");
            }
            return objreturn;

        }

        public EventMaster_Model GetOneEvent(long SLNO,long CreatedBy)
        {
            EventMaster_Model objModel = new EventMaster_Model();
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = new SqlCommand("CB_SP_SelectEventListById", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SLNO", SLNO);
                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objModel.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objModel.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objModel.EventTitle = sdr["EventTitle"].ToString();
                    objModel.StartDate = Convert.ToDateTime(sdr["StartDate"].ToString());
                    objModel.EndDate = Convert.ToDateTime(sdr["EndDate"].ToString());
                    objModel.EventDetail = sdr["EventDetail"].ToString();
                    objModel.EventDomain = Convert.ToInt64(sdr["EventDomain"].ToString());
                    objModel.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objModel.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objModel.DeletedOn = Convert.ToDateTime(sdr["DeletedOn"].ToString());                  
                    objModel.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());                  
                    objModel.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objModel.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());

                    objModel.CompanyName = sdr["Sub_Company_Name"].ToString();
                    objModel.DomainName = sdr["Domain_Name"].ToString();
                }
            }

            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "EventMaster_Repository/GetOneEvent");
            }
            return objModel;
        }

        public DBReturnModel UpdateEventMaster(EventMaster_Model objModel)
        {
            string result = "Error on Updating Event Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction=transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "CB_SP_UpdateEventMaster";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", objModel.SLNO);
                    cmd.Parameters.AddWithValue("@Sub_CompID", objModel.Sub_CompID);
                    cmd.Parameters.AddWithValue("@EventTitle", objModel.EventTitle);
                    cmd.Parameters.AddWithValue("@StartDate", objModel.StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", objModel.EndDate);
                    cmd.Parameters.AddWithValue("@EventDetail", objModel.EventDetail);
                    cmd.Parameters.AddWithValue("@EventDomain", objModel.EventDomain);
                    cmd.Parameters.AddWithValue("@ModifiedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@ModifiedBy", objModel.ModifiedBy);

                    var retmsg = new SqlParameter();
                    retmsg.ParameterName = "retMessage";
                    retmsg.SqlDbType = SqlDbType.VarChar;
                    retmsg.Size = 500;
                    retmsg.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retmsg);


                    var retStatus = new SqlParameter();
                    retStatus.ParameterName = "retStatus";
                    retStatus.SqlDbType = SqlDbType.VarChar;
                    retStatus.Size = 50;
                    retStatus.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(retStatus);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();

                    objreturn.ReturnMessage = retmsg.Value.ToString();
                    objreturn.ReturnStatus = retStatus.Value.ToString();
                    connection.Close();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "EventMaster_Repository/UpdateEventMaster");
                    objreturn.ReturnMessage = "System Error on Updating EventMaster List";
                }
            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreatedBy, "API", "EventMaster_Repository/UpdateEventMaster");
            }
            return objreturn;
        }


        public DBReturnModel DisableEventMaster(long SLNO,long DeletedBy)
        {

            string result = "Error on Disabling Event Master";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "Error";
            objreturn.ReturnMessage =result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;

                try
                {
                    string sqlstr = "";
                    int x = 0;
                    sqlstr = "update Event_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted",true);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    x= cmd.ExecuteNonQuery();
                    if (x>0)
                    {

                        transaction.Commit();
                        connection.Close();
                        objreturn.ReturnStatus = "SUCCESS";
                        objreturn.ReturnMessage = "Event Master Disabled Successfully!";
                    }

                }

                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, DeletedBy, "API", "EventMaster_Repository/DisableEventMaster");
                    objreturn.ReturnMessage = "System Error on Disabling Event Master!";
                }
            }

            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, DeletedBy, "API", "EventMaster_Repository/DisableEventMaster");
            }

            return objreturn;
        }


        public DBReturnModel EnableEventMaster(long SLNO,long DeletedBy)
        {
            string result = "Error on Enabling Event Master!";
            DBReturnModel objreturn = new DBReturnModel();
            objreturn.ReturnStatus = "ERROR";
            objreturn.ReturnMessage = result;

            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "update Event_Table set IsDeleted=@IsDeleted,DeletedOn=@DeletedOn,DeletedBy=@DeletedBy where SLNO=@SLNO";
                    
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@SLNO", SLNO);
                    cmd.Parameters.AddWithValue("@IsDeleted", false);
                    cmd.Parameters.AddWithValue("@DeletedOn", StandardDateTime.GetDateTime());
                    cmd.Parameters.AddWithValue("@DeletedBy", DeletedBy);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    objreturn.ReturnStatus = "SUCCESS";
                    objreturn.ReturnMessage = "Event Master Enabled Successfully!";
                   

                }

                catch(Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex,DeletedBy,"API", "EventMaster_Repository/EnableEventMaster");
                    objreturn.ReturnMessage = "System Error on Enabling EventMaster!";
                }
            }

            catch(Exception ex)
            {
                ErrorHandler.LogError(ex,DeletedBy,"API", "EventMaster_Repository/EnableEventMaster");
            }

            return objreturn;
        }


        public EventMaster_page_Model GetEventMasterByPage(int startRowIndex, int pageSize, string search, string sort_col, string sort_dir, long CreatedBy)
        {
            EventMaster_page_Model objModel = new EventMaster_page_Model();
            List<EventMaster_Model> objlist = new List<EventMaster_Model>();
            if (search == null)
            {
                search = "";
            }
            if (sort_dir == null)
            {
                sort_dir = "ASC";
            }
            if (sort_col == null)
            {
                sort_col = "SLNO";
            }
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                SqlCommand cmd = new SqlCommand("CB_SP_SelectEventTabletByPage", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@startRowIndex", startRowIndex);
                cmd.Parameters.AddWithValue("@pageSize", pageSize);
                cmd.Parameters.AddWithValue("@search", search);
                cmd.Parameters.AddWithValue("@sort_col", sort_col);
                cmd.Parameters.AddWithValue("@sort_dir", sort_dir);

                SqlDataReader sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    EventMaster_Model objtemp = new EventMaster_Model();
                    objtemp.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objtemp.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objtemp.EventTitle = sdr["EventTitle"].ToString();
                    objtemp.StartDate = Convert.ToDateTime(sdr["StartDate"].ToString());
                    objtemp.EndDate = Convert.ToDateTime(sdr["EndDate"].ToString());
                    objtemp.EventDetail = sdr["EventDetail"].ToString();
                    objtemp.EventDomain = Convert.ToInt64(sdr["EventDomain"].ToString());
                    objtemp.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objtemp.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objtemp.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objtemp.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objtemp.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objtemp.CompanyName = sdr["CompanyName"].ToString();
                    objtemp.DomainName = sdr["DomainName"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();

                cmd.Parameters.Clear();
                cmd.CommandText = "Select Count(*) As Count From Event_Table where (Isnull(EventTitle,'') like '%'+@search+'%' "
                + " or Isnull(EventDetail,'') like '%'+@search+'%') ";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                cmd.Parameters.AddWithValue("@search", search);
                sdr = cmd.ExecuteReader();
                while(sdr.Read())
                {
                    objModel.TotalRows = Convert.ToInt32(sdr["Count"].ToString());
                }

               
                connection.Close();
                objModel.EventMaster_Model_List = objlist;

            }
            catch(Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "EventMaster_Repository/GetEventMasterByPage");

            }

            return objModel;
        }



        public EventMaster_page_Model GetEventMasterListFromSubCompAndDomain(long subCompID, long DomainID, long CreatedBy)
        {
            EventMaster_page_Model objModel = new EventMaster_page_Model();
            List<EventMaster_Model> objlist = new List<EventMaster_Model>();
            
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();

                string sqlstr = "Select SLNO, Sub_CompID, Isnull(EventTitle,'') as EventTitle, isnull(StartDate,'') as StartDate,EndDate, "
                    + " isnull(EventDetail,'') as EventDetail,  CreatedBy,CreatedOn, isnull(DeletedBy, 0) as DeletedBy, "
                    + " isnull(EventDomain, 0) as EventDomain,  isnull(IsDeleted, 0) as IsDeleted, isnull(ModifiedBy, 0) as ModifiedBy, "
                    + " isnull(ModifiedOn, '') as ModifiedOn  , CompanyName,DomainName From Vw_EventTable where Sub_CompID=@Sub_CompID "
                    + " And EventDomain=@EventDomain and isnull(IsDeleted, 0)!=@IsDeleted";

                SqlCommand cmd = new SqlCommand(sqlstr, connection);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IsDeleted", true);
                cmd.Parameters.AddWithValue("@EventDomain", DomainID);
                cmd.Parameters.AddWithValue("@Sub_CompID", subCompID);

                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    EventMaster_Model objtemp = new EventMaster_Model();
                    objtemp.SLNO = Convert.ToInt64(sdr["SLNO"].ToString());
                    objtemp.Sub_CompID = Convert.ToInt64(sdr["Sub_CompID"].ToString());
                    objtemp.EventTitle = sdr["EventTitle"].ToString();
                    objtemp.StartDate = Convert.ToDateTime(sdr["StartDate"].ToString());
                    objtemp.EndDate = Convert.ToDateTime(sdr["EndDate"].ToString());
                    objtemp.EventDetail = sdr["EventDetail"].ToString();
                    objtemp.EventDomain = Convert.ToInt64(sdr["EventDomain"].ToString());
                    objtemp.CreatedBy = Convert.ToInt64(sdr["CreatedBy"].ToString());
                    objtemp.CreatedOn = Convert.ToDateTime(sdr["CreatedOn"].ToString());
                    objtemp.IsDeleted = Convert.ToBoolean(sdr["IsDeleted"].ToString());
                    objtemp.ModifiedBy = Convert.ToInt64(sdr["ModifiedBy"].ToString());
                    objtemp.ModifiedOn = Convert.ToDateTime(sdr["ModifiedOn"].ToString());
                    objtemp.CompanyName = sdr["CompanyName"].ToString();
                    objtemp.DomainName = sdr["DomainName"].ToString();
                    objlist.Add(objtemp);
                }
                sdr.Close();
                
                connection.Close();
                objModel.EventMaster_Model_List = objlist;

            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, CreatedBy, "API", "EventMaster_Repository/GetEventMasterListFromSubCompAndDomain");

            }

            return objModel;
        }


    }
}
