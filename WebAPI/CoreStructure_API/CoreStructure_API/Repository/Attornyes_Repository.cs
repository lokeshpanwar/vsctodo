﻿using CoreStructure_API.Model;
using CoreStructure_API.Utility;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStructure_API.Repository
{
    public class Attornyes_Repository
    {

        public List<SelectListItem> Clientlist(int officeid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();
                string sqlstr = "Sp_GetAllClientList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                // cmd.Parameters.AddWithValue("@OfficeId", officeid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["ClientName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/Getclientlist");
            }
            return objList;
        }

        public List<SelectListItem> VSCCCActivity(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "Sp_GetAllActivityList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                // cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["ActivityName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/activitylist");
            }
            return objList;
        }

        

        public List<SelectListItem> VSCCCSubACtivity(int officeId, int activityid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "Sp_GetAllSubActivitList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@ActivityId", activityid);
                //cmd.Parameters.AddWithValue("@OfficeId", officeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["SubActivityName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/subactivitylist");
            }
            return objList;
        }

        

        public List<SelectListItem> Userlist(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "Sp_GetUsers";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                // cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["Name"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/userlist");
            }
            return objList;
        }

       

        public string Filltimesheet(int empid, string date, string time, string description, int clientid, int activityid, int subactivityid, int projectid,int todoid, int StatusId)
        {
            string result = "Error on Deleting Task!";
            try
            {
                var connection = SqlHelper.Connection();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_FilldatainSheetByTodo]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empid", empid);
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@time", time);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@clientid", clientid);
                    cmd.Parameters.AddWithValue("@activityid", activityid);
                    cmd.Parameters.AddWithValue("@subactivityid", subactivityid);
                    cmd.Parameters.AddWithValue("@projectid", projectid);
                    cmd.Parameters.AddWithValue("@todoid", todoid);
                    cmd.Parameters.AddWithValue("@StatusId", StatusId);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, empid, "API", "ToDo_Repository/DeleteTask");
            }
            return result;
        }


        public List<SelectListItem> Projectname(int Clientid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "GetProjectname";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@clientid", Clientid);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = sdr["Id"].ToString(),
                        Text = sdr["CaseTitle"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/activitylist");
            }
            return objList;
        }


       
        public VSCUserData_Table AttorneysUserData(int Userid)
        {
            VSCUserData_Table objList = new VSCUserData_Table();
            try
            {
                var con = SqlHelper.Connection();
                con.Open();

                string sqlstr = "Sp_GetUsersData";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@UserId", Userid);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();


                while (sdr.Read())
                {
                    objList.results.Name = sdr["Name"].ToString();
                    objList.results.MobileNo = sdr["Mobile"].ToString();
                    objList.results.EmailId = sdr["Email"].ToString();
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, Userid, "API", "Master_Repository/GetContactPersonList");
            }
            return objList;
        }


        public List<SelectListItem> Category(int OfficeId)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "Sp_GetAllCategoryList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                // cmd.Parameters.AddWithValue("@OfficeId", OfficeId);
                //cmd.Parameters.AddWithValue("@UserID", 1);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["CategoryName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/activitylist");
            }
            return objList;
        }

        public List<SelectListItem> SubCategory(int OfficeId, int Categoryid)
        {
            List<SelectListItem> objList = new List<SelectListItem>();
            try
            {
                var con = SqlHelper.ConnectionAttorneys();
                con.Open();

                string sqlstr = "Sp_GetAllSubCategoryList";
                SqlCommand cmd = new SqlCommand(sqlstr, con);
                cmd.Parameters.AddWithValue("@Categoryid", Categoryid);
                //cmd.Parameters.AddWithValue("@OfficeId", officeId);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    objList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(sdr["Id"].ToString()),
                        Text = sdr["SubCategoryName"].ToString(),
                    });
                }
                sdr.Close();
                con.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 0, "API", "VSCCO/SubCategory");
            }
            return objList;
        }

        public string savequery(Task_Model objModel, int todoid)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {

                var connection = SqlHelper.ConnectionAttorneys();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {

                    string sqlstr = "[Sp_CreateQuery]";

                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", Dateformat(objModel.Date));
                    cmd.Parameters.AddWithValue("@Title", objModel.Title);
                    cmd.Parameters.AddWithValue("@Description", objModel.Description);
                    cmd.Parameters.AddWithValue("@ClientId", objModel.ClientId);
                    cmd.Parameters.AddWithValue("@CategoryId", objModel.CategoryId);
                    cmd.Parameters.AddWithValue("@SubcategoryId", objModel.SubcategoryId);
                    cmd.Parameters.AddWithValue("@Userid", objModel.AssignToId);
                    cmd.Parameters.AddWithValue("@todoid", todoid);
                    
                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, objModel.CreateBy, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }


        public string savequeryAttorny(string date,string title,string details,int Clientid,int Activityid,int Subactivityid,long logInUser, int todoid)
        {
            string result = "Error on Inserting Contact Person!";
            try
            {
                var connection = SqlHelper.ConnectionAttorneys();
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;
                transaction = connection.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = connection;
                try
                {
                    string sqlstr = "[Sp_CreateQuery]";
                    cmd.Parameters.Clear();
                    cmd.CommandText = sqlstr;
                    cmd.Connection = connection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", date);
                    cmd.Parameters.AddWithValue("@Title", title);
                    cmd.Parameters.AddWithValue("@Description", details);
                    cmd.Parameters.AddWithValue("@ClientId", Clientid);
                    cmd.Parameters.AddWithValue("@CategoryId", Activityid);
                    cmd.Parameters.AddWithValue("@SubcategoryId", Subactivityid);
                    cmd.Parameters.AddWithValue("@Userid", logInUser);
                    cmd.Parameters.AddWithValue("@todoid", todoid);

                    var returnCode = new SqlParameter();
                    returnCode.ParameterName = "@ret";
                    returnCode.SqlDbType = SqlDbType.VarChar;
                    returnCode.Size = 500;
                    returnCode.Direction = ParameterDirection.Output;
                    returnCode.Value = "";
                    cmd.Parameters.Add(returnCode);
                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    connection.Close();
                    result = returnCode.Value.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    connection.Close();
                    ErrorHandler.LogError(ex, 1, "API", "ToDo_Repository/SaveTask");
                    result = ex.Message;
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.LogError(ex, 1, "API", "ToDo_Repository/SaveTask");
            }
            return result;
        }




        protected static String Dateformat(string e)
        {
            string inputFormat = "dd-MM-yyyy";
            string outputFormat = "yyyy-MM-dd";
            string output = string.Empty;
            if (e == null || e == "")
            {
                output = "";
            }
            else
            {
                var dateTime = DateTime.ParseExact(e.ToString(), inputFormat, CultureInfo.InvariantCulture);
                output = dateTime.ToString(outputFormat);
            }
            return output;
        }



    }
}
